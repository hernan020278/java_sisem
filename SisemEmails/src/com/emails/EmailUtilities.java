package com.emails;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.comun.propiedad.DiccionarioGeneral;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.utilidad.Log;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class EmailUtilities
{
	public static Session getEmailSession(String organizationId)
	{
		String auth = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.authenticate", "false");

		return EmailUtilities.getEmailSession(organizationId, auth.equalsIgnoreCase("true"), EmailServer.SMTP);
	}

	public static StringBuffer getEmailContentAsString(Message message)
	{
		StringBuffer sb = new StringBuffer();
		RenderableMessage rm = null;
		try
		{
			rm = new RenderableMessage(message);
			sb.append(rm.getBodytext());
		}
		catch (MessagingException e) {
			e.printStackTrace();
			Log.error("EmailUtilities", e);
		}
		catch (IOException e) {
			e.printStackTrace();
			Log.error("EmailUtilities", e);
		}

		return sb;
	}

	public static Session getEmailSession(String organizationId, String jobType)
	{
		if (!jobType.equalsIgnoreCase("") && jobType != null)
		{
			String auth = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad(jobType + ".authenticate", "");
			if (Util.isEmpty(auth))
			{
				auth = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.authenticate", "false");
			}
			return EmailUtilities.getEmailSession(organizationId, auth.equalsIgnoreCase("true"), jobType);
		}
		else
		{
			String auth = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.authenticate", "false");
			return EmailUtilities.getEmailSession(organizationId, auth.equalsIgnoreCase("true"));
		}
	}

	/**
	 * @param organizationId
	 * @param auth
	 * @return
	 */

	/*
	 * public static Session getEmailSession(String organizationId, boolean auth)
	 * {
	 * return EmailUtilities.getEmailSession(organizationId, auth, "");
	 * }
	 */
	public static Session getEmailSession(String organizationId, boolean auth)
	{
		Log.debug("EmailUtilities", "getemailsession");
		Session session = null;
		try
		{
			Properties props = new Properties();
			props.put("mail.smtp.host", EmailServerFactory.getInstance(organizationId, EmailServer.SMTP).getServer());

			props.put("mail.transport.protocol", "smtp");
			if (auth)
			{
				props.put("mail.smtp.auth", "true");
				EmailAuthenticator emailAuthenticator = new EmailAuthenticator();
				emailAuthenticator.setOrganizationId(organizationId);
				session = Session.getInstance(props, emailAuthenticator);
			}
			else
			{
				Log.debug("EmailUtilities", "no authentication");
				session = Session.getInstance(props, null);
			}
			// session.setDebug(true);
			String debug = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.debug", "false");
			session.setDebug(debug.equalsIgnoreCase("true"));
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.error("EmailUtilities", e);
		}
		Log.debug("EmailUtilities", "we have a session");
		return session;
	}

	public static Session getEmailSession(String organizationId, boolean auth, String emailServerType)
	{

		Log.debug("EmailUtilities", "getemailsession");
		Session session = null;
		try
		{
			Properties props = new Properties();
			if (emailServerType.indexOf(EmailServer.SMTP) >= 0)
			{
				props.put("mail.smtp.host", DiccionarioGeneral.getInstance("emails", "").getPropiedad("mail.smtpserver", ""));
				props.put("mail.transport.protocol", "smtp");
				props.put("mail.smtp.starttls.enable", DiccionarioGeneral.getInstance("emails", "").getPropiedad("mail.smtp.starttls.enable", ""));
			}
			else if (emailServerType.indexOf(EmailServer.POP3) >= 0)
			{
				props.put("mail.pop3.host", DiccionarioGeneral.getInstance("emails", "").getPropiedad("mail.pop3server", ""));
			}
			props.put("mail.smtp.port", DiccionarioGeneral.getInstance("emails", "").getPropiedad("mail.smtp.port", ""));
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.userId", DiccionarioGeneral.getInstance("emails", "").getPropiedad("mail.userid", "test1"));
			props.put("mail.smtp.password", DiccionarioGeneral.getInstance("emails", "").getPropiedad("mail.password", "tester1"));

			if(auth)
			{
				props.put("mail.smtp.auth", "true");
				EmailAuthenticator emailAuthenticator = new EmailAuthenticator();
				emailAuthenticator.setOrganizationId(organizationId);
				emailAuthenticator.setJobType("");
				session = Session.getInstance(props, emailAuthenticator);
			}
			else
			{
				Log.debug("EmailUtilities", "no authentication");
				session = Session.getInstance(props, null);
			}
			// session.setDebug(true);
			String debug = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.debug", "false");
			session.setDebug(debug.equalsIgnoreCase("true"));
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.error("EmailUtilities", e);
		}
		Log.debug("EmailUtilities", "we have a session");
		return session;
	}

	public static Store getPop3Store(Session session)
	{
		Store store = null;
		try
		{
			store = session.getStore("pop3");
			store.connect();
		}
		catch (NoSuchProviderException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (MessagingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return store;
	}

	public static Folder getFolder(Store store, String type)
	{
		Folder folder = null;
		try
		{
			folder = store.getFolder("INBOX");
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		}
		try
		{
			folder.open(Folder.READ_WRITE);
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		}

		return folder;
	}

	public static String setSubject(String subject, String organizationId)
	{
		if (DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.testserver", "N").equalsIgnoreCase("Y"))
		{
			String testPrefix = PropiedadGeneral.getInstance(organizationId).getProperty("MISC", "TestMailPrefix", "[Puridiom Test]");
			return testPrefix + " " + subject;
		}
		else
		{
			return subject;
		}
	}

	public static boolean getAuthenticationType(String jobType, String organizationId)
	{
		boolean auth = true;
		if (!Util.isEmpty(jobType))
		{
			auth = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad(jobType + ".authenticate", "false").equals("true");
		}
		else
		{
			auth = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.authenticate", "false").equals("true");
		}
		return auth;
	}

	/**
	 * @param msg
	 * @param pFromEmailAddress
	 * @param pToEmailAddress
	 * @param pCCEmailAddress
	 * @param organizationId
	 * @return
	 */
	public static Message setHeaders(Message msg, String pFromEmailAddress, String[] pToEmailAddress, String pCCEmailAddress, String organizationId, String jobType)
	{
		return EmailUtilities.setHeaders(msg, pFromEmailAddress, pToEmailAddress, pCCEmailAddress, "", organizationId, jobType);
	}

	public static Message setHeaders(Message msg, String pFromEmailAddress, String[] pToEmailAddress, String pCCEmailAddress, String bccEmailaddress, String organizationId, String jobType)
	{
		// BCC
		String bcc = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.bcc", "");
		String preventEmailSpoofingProperty = (String) PropiedadGeneral.getInstance(organizationId).getProperty("MAIL", "PREVENTEMAILSPOOFING", "N");

		if (!Util.isEmpty(pCCEmailAddress))
		{
			bcc = bcc + pCCEmailAddress;
		}
		if (!Util.isEmpty(bcc))
		{
			try {
				msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
			}
			catch (AddressException e) {
				e.printStackTrace();
			}
			catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		// extra bcc
		if (!Util.isEmpty(bccEmailaddress))
		{
			try {
				msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(bccEmailaddress));
			}
			catch (AddressException e) {
				e.printStackTrace();
			}
			catch (MessagingException e) {
				e.printStackTrace();
			}
		}

		// FROM
		try
		{
			pFromEmailAddress = EmailUtilities.checkUserEmail(organizationId, pFromEmailAddress);
			pFromEmailAddress = EmailUtilities.checkDestinyEmail(pFromEmailAddress, organizationId);
			if (DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.testdomain", "N").equalsIgnoreCase("Y"))
			{
				pFromEmailAddress = pFromEmailAddress.substring(0, pFromEmailAddress.indexOf("@")) + "@farmaster.com";
			}

			if (preventEmailSpoofingProperty.equalsIgnoreCase("Y"))
			{
				InternetAddress replyToAddies[] = { new InternetAddress(EmailUtilities.checkSendFrom(pFromEmailAddress)) };
				msg.setReplyTo(replyToAddies);
			}
			else
			{
				msg.setFrom(new InternetAddress(EmailUtilities.checkSendFrom(pFromEmailAddress), "Soporte de Sistema " + organizationId.toUpperCase()));
			}
		}
		catch (AddressException e1) {
			e1.printStackTrace();
			Log.error("EmailUtilities", e1);
		}
		catch (MessagingException e1) {
			e1.printStackTrace();
			Log.error("EmailUtilities", e1);
		}
		catch (Exception e1) {
			e1.printStackTrace();
			Log.error("EmailUtilities", e1);
		}

		String replyTo = "";
		// REPLYTO
		// String replyTo = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.replyto", "");
		// reply-to is now the "from" - MSG requirement to avoid "email spoofing"
		if (preventEmailSpoofingProperty.equalsIgnoreCase("Y"))
		{
			replyTo = PropiedadGeneral.getInstance(organizationId).getProperty("mail", "from", "");
			if (Util.isEmpty(replyTo))
			{
				replyTo = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.from", "");
			}
		}
		else
		{
			replyTo = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.replyto", "");
		}

		if (!Util.isEmpty(replyTo))
		{
			if (!Util.isEmpty(replyTo))
			{
				try
				{
					InternetAddress replyToAddies[] = { new InternetAddress(replyTo) };
					if (preventEmailSpoofingProperty.equalsIgnoreCase("Y"))
					{
						msg.setFrom(new InternetAddress(replyTo));
					}
					else
					{
						msg.setReplyTo(replyToAddies);
					}
				}
				catch (MessagingException me)
				{
					Log.error("EmailUtilities", me);
				}
				catch (IllegalStateException iwe)
				{
					Log.error("EmailUtilities", iwe);
				}
			}
		}

		// TO
		try
		{

			String pToSingleEmailAddress;
			String redirectTo = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.redirectto", "");

			if (jobType.equalsIgnoreCase("efax") && DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("efax.testuniqueefax", "N").equalsIgnoreCase("Y"))
			{
				String propertyFax = PropiedadGeneral.getInstance(organizationId).getProperty("efax", "@efax", "@efaxsend.com");
				String propertyNumber = PropiedadGeneral.getInstance(organizationId).getProperty("efax", "faxnumber", "717-691-5690");

				String sendto = "1" + propertyNumber.replaceAll("\\W|[a-zA-Z]", "") + propertyFax;

				InternetAddress uniqueRecipient = new InternetAddress(sendto);
				msg.setRecipient(Message.RecipientType.TO, uniqueRecipient);

				Log.debug(EmailUtilities.class, " Set UNIQUE EMAIL for test to " + sendto);
			}
			else
			{
				if (DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.testuniqueemail", "N").equalsIgnoreCase("Y"))
				{
					String uniqueEmail = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.uniqueemail", "test1@puridiom.com");
					InternetAddress uniqueRecipient = new InternetAddress(uniqueEmail);
					msg.setRecipient(Message.RecipientType.TO, uniqueRecipient);

					Log.debug(EmailUtilities.class, " Set UNIQUE EMAIL for test to " + uniqueEmail);
				}
				else
				{
					InternetAddress recipients[] = new InternetAddress[pToEmailAddress.length];

					for (int i = 0; i < recipients.length; i++)
					{
						pToSingleEmailAddress = pToEmailAddress[i];
						pToSingleEmailAddress = EmailUtilities.checkUserEmail(organizationId, pToSingleEmailAddress);
						pToSingleEmailAddress = EmailUtilities.checkDestinyEmail(pToSingleEmailAddress, organizationId);

						if (DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.testdomain", "N").equalsIgnoreCase("Y"))
						{
							pToSingleEmailAddress = pToSingleEmailAddress.substring(0, pToSingleEmailAddress.indexOf("@")) + "@farmaster.com";
						}

						if (Util.isEmpty(redirectTo))
						{
							if (DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.testdomain", "N").equalsIgnoreCase("Y"))
							{
								pToSingleEmailAddress = pToSingleEmailAddress.substring(0, pToSingleEmailAddress.indexOf("@")) + "@farmaster.com";
							}
						}
						else
						{
							if (pFromEmailAddress.indexOf("thegarden.com") > 0 || pFromEmailAddress.indexOf("radiocity.com") > 0)
							{
								pToSingleEmailAddress = redirectTo;
							}
						}

						recipients[i] = new InternetAddress(pToSingleEmailAddress);
					}

					msg.setRecipients(Message.RecipientType.TO, recipients);
				}
			}
		}
		catch (AddressException e)
		{
			Log.error("EmailUtilities", e);
			e.printStackTrace();
		}
		catch (MessagingException e) {
			Log.error("EmailUtilities", e);
			e.printStackTrace();
		}
		catch (Exception e) {
			Log.error("EmailUtilities", e);
			e.printStackTrace();
		}

		Log.debug(EmailUtilities.class, "done setting headers");
		return msg;

	}

	public static String checkSendFrom(String sendFrom)
	{
		String ret = "";
		int index = sendFrom.indexOf(";");
		if (index > 0)
		{
			ret = sendFrom.substring(0, index);
		}
		else
		{
			ret = sendFrom;
		}

		return ret;
	}

	public static String checkDestinyEmail(String destinyEmail, String organizationId)
	{
		String destinyEmailTmp = destinyEmail.toLowerCase();
		String key;

		Map serverDomains = EmailManager.getInstance().getServerDomains();
		for (Iterator iter = serverDomains.keySet().iterator(); iter.hasNext();)
		{
			key = (String) iter.next();

			if (destinyEmailTmp.indexOf(key) > -1)
			{
				destinyEmail = destinyEmailTmp.replaceAll(key, (String) serverDomains.get(key));
				destinyEmail = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.redirectto", "test1@puridiom.com");
			}
		}

		return destinyEmail;
	}

	public static String checkUserEmail(String organizationId, String userEmail) throws Exception
	{
		if (userEmail.indexOf("@") <= 0)
		{
			userEmail = "hernan020278@gmail.com";
		}

		return userEmail;
	}

	/**
	 * Gets a file to save an email to.
	 * uses mail.filepath property from emails.properties
	 * <b>All emails are saved under the same directory but each "jobtype" has its own directory</b>
	 * 
	 * @param organizationId
	 * @param jobType
	 * @return
	 */
	public static File getEmailFile(String organizationId, String jobType)
	{
		String path = EmailUtilities.getEmailDirectory(organizationId, jobType);
		String fileExtension = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad(jobType + ".fileType", "txt");
		KeyGenerator ukg = KeyGenerator.getInst();
		String fileName = ukg.getUniqueKey().toString() + "." + fileExtension;
		return new File(path + fileName);
	}

	public static String getEmailDirectory(String organizationId, String jobType)
	{
		String path = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.filepath", "");
		return path + jobType + File.separator + "incoming" + File.separator;
	}

	public static String getContent(Message message, String jobType, String organizationId)
	{
		RenderableMessage rm = null;
		String body = "";
		String contentType = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad(jobType + ".contentType", "");
		try
		{
			Log.debug(EmailUtilities.class, "EmailUtilies getContent");
			rm = new RenderableMessage(message, contentType);
			body = rm.getBodytext().toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			body = "";
		}
		catch (MessagingException me)
		{
			me.printStackTrace();
			body = "";
		}
		return body;
	}

	public static String saveEmailContentsToDisc(Message message, String organizationId, String jobType)
	{
		String emailFileName = "";

		File emailFile = EmailUtilities.getEmailFile(organizationId, jobType);
		emailFileName = emailFile.getAbsolutePath();
		String content = EmailUtilities.getContent(message, jobType, organizationId);
		EmailUtilities.saveFile(content, emailFileName, organizationId);

		return emailFileName;
	}

	public static String saveEmailToDisc(Message message, String organizationId, String jobType)
	{
		FileOutputStream fos = null;
		String emailFileName = "";
		String subject = "";
		try
		{
			subject = message.getSubject();
			File emailFile = EmailUtilities.getEmailFile(organizationId, jobType);
			emailFileName = emailFile.getAbsolutePath();
			fos = new FileOutputStream(emailFile);
			message.writeTo(fos);
		}
		catch (FileNotFoundException e)
		{
			Log.error("HandleEmails", e.getMessage() + "Email was not saved");
			e.printStackTrace();
		}
		catch (IOException e)
		{
			Log.error("HandleEmails", e.getMessage() + "Email was not saved");
			e.printStackTrace();
		}
		catch (MessagingException e) {
			Log.error("HandleEmails", e.getMessage() + "Email was not saved");
			e.printStackTrace();
		}
		finally
		{
			if (fos != null)
			{
				try {
					fos.close();
				}
				catch (IOException e) {
					Log.error("HandleEmails", e.getMessage() + "Email was not saved to disk: " + subject);
					e.printStackTrace();
				}
			}
		}
		return emailFileName;
	}

	public static String printfrom(Address[] from)
	{
		StringBuffer sb = new StringBuffer();

		for (int j = 0; j < from.length; j++)
		{
			// System.out.println(this.nowDebug + "\tfrom: " + from[j]);
			InternetAddress fromAddy = (InternetAddress) from[j];
			sb.append(fromAddy.getAddress());
			if (from.length > 1)
			{
				sb.append(";");
			}
		}
		return sb.toString();
	}

	public static String messageDetails(Message message)
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			sb.append("from: " + EmailUtilities.printfrom(message.getFrom()));
			sb.append("Subject: " + message.getSubject() + "\r\n");
		}
		catch (MessagingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();

	}

	public static boolean checkTo(Address[] to, String organizationId)
	{
		String toDefault = DiccionarioGeneral.getInstance("emails", organizationId).getPropiedad("mail.app.to", "");
		Log.debug("EmailUtilities", "rules checking email was sent to: " + toDefault);
		for (int j = 0; j < to.length; j++)
		{
			InternetAddress addie = (InternetAddress) to[j];
			Log.debug("EmailUtilities", addie.toString());
			if (addie.toString().toLowerCase().indexOf(toDefault.toLowerCase()) > -1)
			{
				Log.debug("EmailUtilities", addie.toString() + "is a good Address");
				return true;
			}
		}
		Log.debug("EmailUtilities", "checkTo returns false");
		return false;
	}

	public static void saveFile(String content, String fileName, String organizationId)
	{
		File file = new File(fileName);
		Log.debug(EmailUtilities.class, "saving from for: " + file.getName());
		boolean created = false;
		try
		{
			created = file.createNewFile();
		}
		catch (IOException e1)
		{
			Log.error(WirelessEmailUtils.class, "error saving from address!" + e1.getMessage());
			e1.printStackTrace();
		}

		if (created)
		{
			try
			{
				Log.debug(EmailUtilities.class, "saving from content " + content);
				FileWriter fw = new FileWriter(file);
				fw.write(content);
				fw.close();
			}
			catch (FileNotFoundException e)
			{
				Log.error(EmailUtilities.class, "Response from file not found!" + e.getMessage());
				e.printStackTrace();
			}
			catch (IOException e)
			{
				Log.error(EmailUtilities.class, "Error writting response xml" + e.getMessage());
				e.printStackTrace();
			}
		}

	}

}
