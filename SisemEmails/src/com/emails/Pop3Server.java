package com.emails;

import com.comun.propiedad.DiccionarioGeneral;
import com.comun.utilidad.Util;

/**
 * @author renzo
 *
 */
public class Pop3Server extends EmailServer
{
	public Pop3Server(String _organizationId)
	{
		this.setOrganizationId(_organizationId);

		this.setServer(DiccionarioGeneral.getInstance("emails", this.getOrganizationId()).getPropiedad("mail.pop3server", "mail.tsagate.com"));
	}

	public Pop3Server(String _organizationId, String jobType)
	{

		this.setOrganizationId(_organizationId);
		if (!Util.isEmpty(jobType))
		{
			this.setServer(DiccionarioGeneral.getInstance("emails", this.getOrganizationId()).getPropiedad(jobType + ".pop3server", ""));
		}
		else
		{
			this.setServer(DiccionarioGeneral.getInstance("emails", this.getOrganizationId()).getPropiedad("mail.pop3server", ""));
		}
	}
}




