package com.emails;

import com.comun.propiedad.DiccionarioGeneral;
import com.comun.utilidad.Util;

public class SmtpServer extends EmailServer
{
	public SmtpServer(String _organizationId)
    {
    	this.setOrganizationId(_organizationId);

        this.setServer(DiccionarioGeneral.getInstance("emails", this.getOrganizationId()).getPropiedad("mail.smtpserver", "mail.tsagate.com"));
    }

	public SmtpServer(String _organizationId, String jobType)
    {
    	this.setOrganizationId(_organizationId);
    	String smtpServer = "";
    	if (!Util.isEmpty(jobType))
		{
    		smtpServer = DiccionarioGeneral.getInstance("emails", this.getOrganizationId()).getPropiedad(jobType + ".smtpserver", "");
    		if(Util.isEmpty(smtpServer))
    		{
    			smtpServer = DiccionarioGeneral.getInstance("emails", this.getOrganizationId()).getPropiedad("mail.smtpserver", "");
    		}
		}
    	else
    	{
    		smtpServer = DiccionarioGeneral.getInstance("emails", this.getOrganizationId()).getPropiedad("mail.smtpserver", "");
    	}

        this.setServer(smtpServer);
    }
}
