package com.emails;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import com.comun.propiedad.DiccionarioGeneral;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;


/**
 * @author renzo
 *
 */
public class EmailAuthenticator extends Authenticator
{
    private String userName = "";
    private String userPassword = "";
    public String props = "";
    private String organizationId = "";
    public String jobType;

    public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public void setProps()
    {
		if(!Util.isEmpty(jobType))
		{
			String user = DiccionarioGeneral.getInstance("emails", this.organizationId).getPropiedad(jobType + ".userid", "");
			if(Util.isEmpty(user))
			{
				user = DiccionarioGeneral.getInstance("emails", this.organizationId).getPropiedad("mail.userid", "test1");
			}
			this.setUserName(user);
			String password = DiccionarioGeneral.getInstance("emails", this.organizationId).getPropiedad(jobType + ".password", "");
			if(Util.isEmpty(password))
			{
				password = DiccionarioGeneral.getInstance("emails", this.organizationId).getPropiedad("mail.password", "tester1");
			}
			this.setUserPassword(password);
		}
		else
		{
	       this.setUserName(DiccionarioGeneral.getInstance("emails", this.organizationId).getPropiedad("mail.userid", "test1"));
	       this.setUserPassword(DiccionarioGeneral.getInstance("emails", this.organizationId).getPropiedad("mail.password", "tester1"));
		}
    }

    /* (non-Javadoc)
     * @see javax.mail.Authenticator#getPasswordAuthentication()
     */
    public PasswordAuthentication getPasswordAuthentication()
    {
        this.setProps();
        Log.debug(this, "userName: " + this.getUserName());
        Log.debug(this, "userPassword: " + this.getUserPassword());
        Log.debug(this, "organizationId: " + this.getOrganizationId());
        return new PasswordAuthentication(this.getUserName(), this.getUserPassword());
    }
    /**
     * @return Returns the userName.
     */
    public String getUserName() {
        return userName;
    }
    /**
     * @param userName The userName to set.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * @return Returns the userPassword.
     */
    public String getUserPassword() {
        return userPassword;
    }
    /**
     * @param userPassword The userPassword to set.
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
	public String getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}
	
	/**
	 * test main method
	 */
	public static void main (String [] args) {
		EmailAuthenticator emailAuthenticator = new EmailAuthenticator();
		
		emailAuthenticator.setOrganizationId("p4test");
		emailAuthenticator.setProps();
		
		System.out.println("username: " + emailAuthenticator.getUserName());
		System.out.println("password: " + emailAuthenticator.getUserPassword());
	}
}
