package com.control.accion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.ConexionControlBackup;
import com.comun.entidad.Cliente;
import com.comun.motor.Accion;
import com.comun.motor.AccionListener;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Estado;
import com.comun.utilidad.PropiedadesExterna;
import com.comun.utilidad.Util;

public class RestaurarDatabase extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception 
	{
		Object result = null;

		try 
		{
			PropiedadesExterna propiedadExterna = new PropiedadesExterna();
			Util util = new Util();

			String nombreSistema = propiedadExterna.getServidor("nombre-sistema");
			String rutaBackup = propiedadExterna.getServidor("ruta-backup");
			String nombreDatabase = propiedadExterna.getServidor("nombre-database");
			String organizacion = OrganizacionGeneral.getInstance(nombreSistema).getOrgcodigo();

			if (restaurarBackup(nombreSistema, rutaBackup + nombreDatabase)) {

				JOptionPane.showConfirmDialog(null, "El Backup Se restauro con exito" + "\n" + this.getClass().getName(), "Sistema Chentty", JOptionPane.PLAIN_MESSAGE,
						JOptionPane.INFORMATION_MESSAGE);

				util.abrirArchivoDesktop("C:\\sisem\\", nombreSistema + ".bat");

			}// Fin de if(man.realizarBackup("dbbackup_20121208"))			
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

	public static boolean restaurarBackup(String nombreSistema, String archivoRuta) 
	{
		String conSql = "";
		Connection conecction;
		CallableStatement cstm;
		boolean exito = false;

		try {

			conSql = "{call proc_restore_" + nombreSistema + "(?)}";

			conecction = ConexionControlBackup.crearConexion();

			cstm = conecction.prepareCall(conSql);
			cstm.setString(1, archivoRuta);

			cstm.execute();
			exito = true;

			cstm.close();
			conecction.close();
		}
		catch (SQLException e) {
			// util.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}

		return exito;
	}// Fin d e realizarBackup(String archivoRuta)

}// Fin de clase principal
