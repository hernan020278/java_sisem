package com.control.accion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.database.ConexionControlBackup;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Util;

public class EnviarBackupByFTP extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			System.out.println("Enviando backup via ftp...!!!");
			Util.getInst().enviarBackupByFTP();
			System.out.println("Termino Envio via ftp...!!!");
			
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

	public static boolean restaurarBackup(String nombreSistema, String archivoRuta)
	{

		String conSql = "";
		Connection conecction;
		CallableStatement cstm;
		boolean exito = false;
		try {
			conSql = "{call proc_restore_" + nombreSistema + "(?)}";
			conecction = ConexionControlBackup.crearConexion();
			cstm = conecction.prepareCall(conSql);
			cstm.setString(1, archivoRuta);
			cstm.execute();
			exito = true;
			cstm.close();
			conecction.close();
		} catch (SQLException e) {
			// util.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
		return exito;
	}// Fin d e realizarBackup(String archivoRuta)
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				try {
					EnviarBackupByFTP enviarBackupByFTP = new EnviarBackupByFTP();
					Map peticion = new HashMap();
					enviarBackupByFTP.ejecutarAccion(peticion);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
	}
}// Fin de clase principal
