package com.control.accion;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.MoverVentanaJComponent;
import com.comun.utilidad.Util;
import com.comun.utilidad.VentanaImagen;
import com.sun.awt.AWTUtilities;

public class FrmSisem extends JFrame {

	private JComponent contenedor;
	private JPanel panelPrincipal;

	// Variable del formulario Principal
	public FrmSisem() {

		addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {

				// contenedor.update(contenedor.getGraphics());
			}
		});
		contenedor = new VentanaImagen(AplicacionGeneral.getInstance().obtenerImagen("fondo_sisem.png"));
		this.setUndecorated(true);
		this.setContentPane(contenedor);
		AWTUtilities.setWindowOpaque(this, false);
		this.getRootPane().setOpaque(false);
		MoverVentanaJComponent moverVentana = new MoverVentanaJComponent(contenedor);
		this.addMouseListener(moverVentana);
		this.addMouseMotionListener(moverVentana);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		this.getContentPane().setLayout(new BorderLayout());
		this.setSize(new java.awt.Dimension(900, 600));
		this.setLocationRelativeTo(null);
		initComponents();
	}

	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		panelPrincipal = new JPanel();
		panelPrincipal.setLayout(null);
		panelPrincipal.setOpaque(false);
		super.setTitle("Sistema de Control Produccion");
		getContentPane().add(panelPrincipal);
		panMenuInicio = new JPanel();
		panMenuInicio.setOpaque(false);
		panMenuInicio.setLayout(null);
		panMenuInicio.setBounds(0, 0, 900, 600);
		panelPrincipal.add(panMenuInicio);
		cmdUrlWeb = new JLabel("");
		cmdUrlWeb.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {

				cmdUrlWebEvento();
			}
		});
		cmdUrlWeb.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdUrlWeb.setBounds(295, 72, 408, 289);
		panMenuInicio.add(cmdUrlWeb);
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setBounds(762, 489, 100, 100);
		panMenuInicio.add(cmdCerrar);
		cmdCerrar.setOpaque(false);
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_down.png")); // NOI18N
		cmdCerrar.setBorderPainted(false);
		cmdCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdCerrar.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_down.png")); // NOI18N
		cmdCerrar.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_up.png")); // NOI18N
		cmdCerrar.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				System.exit(0);
			}
		});
		setSize(new java.awt.Dimension(900, 600));
		setLocationRelativeTo(null);
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting cFrmPrincipal">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName()))
				{
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(FrmSisem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(FrmSisem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(FrmSisem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(FrmSisem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		/*
		 * Create and display the form
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				new FrmSisem().setVisible(true);
			}
		});
	}

	private javax.swing.JButton cmdCerrar;
	private JPanel panMenuInicio;
	private JLabel cmdUrlWeb;

	public void cmdUrlWebEvento()
	{

		if(java.awt.Desktop.isDesktopSupported()) {
			try {
				Desktop dk = Desktop.getDesktop();
				dk.browse(new URI("www.sysem.org"));
			} catch (Exception e) {
				System.out.println("Error al abrir URL: " + e.getMessage());
			}
		}
	}
}
