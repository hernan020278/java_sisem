package com.control.accion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.comun.database.ConexionControlBackup;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Util;

public class GenerarBackupByFTP extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			System.out.println("Generando backup para el envio via ftp...!!!");
			Util.getInst().crearBackupDatabase();
			System.out.println("Termino backup para el envio via ftp...!!!");
			
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

	public static boolean restaurarBackup(String nombreSistema, String archivoRuta)
	{

		String conSql = "";
		Connection conecction;
		CallableStatement cstm;
		boolean exito = false;
		try {
			conSql = "{call proc_restore_" + nombreSistema + "(?)}";
			conecction = ConexionControlBackup.crearConexion();
			cstm = conecction.prepareCall(conSql);
			cstm.setString(1, archivoRuta);
			cstm.execute();
			exito = true;
			cstm.close();
			conecction.close();
		} catch (SQLException e) {
			// util.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
		return exito;
	}// Fin d e realizarBackup(String archivoRuta)
}// Fin de clase principal
