package com.control;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.comun.database.ConexionControlBackup;
import com.comun.motor.AccionListener;
import com.comun.utilidad.Util;

public class ControlMain {

	private static ServerSocket SERVER_SOCKET;

	public static void main(final String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run()
			{
				try
				{
					SERVER_SOCKET = new ServerSocket(50000);
					Map peticion = new HashMap();
					String paginaAccion = (String) args[0];
					if(!Util.isEmpty(paginaAccion))
					{
						peticion.put("paginaAccion", paginaAccion);
						peticion = (Map) ((AccionListener) Class.forName("com.control.accion." + paginaAccion).newInstance()).ejecutarAccion(peticion);
					}
					else
					{
						JOptionPane.showConfirmDialog(null, "No existe una accion a realizar"
								+ "\n" + this.getClass().getName(), "Sistema",
								JOptionPane.PLAIN_MESSAGE,
								JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (IOException e) {
					JOptionPane.showConfirmDialog(null, "El backup esta en curso...!!!"
							+ "\n" + this.getClass().getName(), "Control de Acceso",
							JOptionPane.PLAIN_MESSAGE,
							JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}// Fin d e metodo main

	public static boolean restaurarBackup(String nombreSistema, String archivoRuta) {

		String conSql = "";
		Connection conecction;
		CallableStatement cstm;
		boolean exito = false;
		try {
			conSql = "{call proc_restore_" + nombreSistema + "(?)}";
			conecction = ConexionControlBackup.crearConexion();
			cstm = conecction.prepareCall(conSql);
			cstm.setString(1, archivoRuta);
			cstm.execute();
			exito = true;
			cstm.close();
			conecction.close();
		} catch (SQLException e) {
			// util.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
		return exito;
	}// Fin d e realizarBackup(String archivoRuta)
}
