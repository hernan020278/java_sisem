package com.usuario.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;

public class EliminarUsuario extends Accion
{
	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			peticion.put("Usuario_usucodigo", peticion.get("Pagina_valor"));
			Usuario usu = (Usuario)(new ObtenerUsuario()).ejecutarAccion(peticion);

			usu.setEstado(Estado.DELETED);

			peticion.put("tipoSql", TipoSql.UPDATE);
			peticion.put("entidadObjeto", usu);
			peticion.put("where", ("where usucodigo='" + usu.getUsucodigo() + "'"));
			afectados = dbc.ejecutarCommit(peticion);

			if (afectados > 0)
			{
				peticion.put("Mensaje_mostrar", "true");
				peticion.put("Mensaje_titulo", "Informacion de Almacenamiento");
				peticion.put("Mensaje_tipo", "info");
				peticion.put("Mensaje_valor", "Los datos se han borrado satisfactoriamente");
			}// Fin de if(bookMarkDevCod != 0)
			else
			{
				peticion.put("Mensaje_mostrar", "true");
				peticion.put("Mensaje_titulo", "Informacion de Almacenamiento");
				peticion.put("Mensaje_tipo", "error");
				peticion.put("Mensaje_valor", "Los datos se han borrado satisfactoriamente");
			}
			result = peticion;
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return result;
	}
}