package com.usuario.accion;

import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.entidad.Historia;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerCasoPaciente extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			String usucodigo = (String) peticion.get("Usuario_usucodigo");

			peticion.put("consulta", "select * from usuario where usucodigo='" + usucodigo + "'");
			peticion.put("entidadNombre", "Usuario");
			Usuario paciente = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			peticion.put("paciente", paciente);

			peticion.put("consulta", "select * from historia where paccodigo='" + usucodigo + "'");
			peticion.put("entidadNombre", "Historia");
			Historia historia = (Historia) (new ObtenerEntidad()).ejecutarAccion(peticion);
			peticion.put("historia", historia);

			peticion.put("consulta", "select * from registrocaso where paccodigo='" + usucodigo + "'");
			peticion.put("entidadNombre", "RegistroCaso");
			List listaRegistroCaso = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaRegistroCaso", listaRegistroCaso);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}