package com.usuario.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class AgregarUsuario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			Usuario empleado = new Usuario();
			BigDecimal codigo = new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString());
			String identidad = KeyGenerator.getInst().obtenerCodigoBarra();

			String sql = "select * from usuario where identidad='" + identidad + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Usuario");

			aux.ope = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if (Util.isEmpty(empleado.getIdentidad()))
			{
				empleado.setUsucodigo(codigo);
				empleado.setIdentidad(KeyGenerator.getInst().obtenerCodigoBarra().toString());
				empleado.setEstado(Estado.PERMANENTE);
			}

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}