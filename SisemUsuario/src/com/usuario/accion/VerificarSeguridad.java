package com.usuario.accion;

import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.utilidad.Auxiliar;

public class VerificarSeguridad extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = false;

		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			Auxiliar aux = Auxiliar.getInstance();

			String consulta = "select organizacion.orgcodigo, usuario.usucodigo, seguridad.segcodigo,perfilseguridad.nombre,perfilseguridad.acceso " +
					"from organizacion inner join usuario on " +
					"organizacion.orgcodigo=usuario.orgcodigo inner join agrupacion on " +
					"usuario.usucodigo=agrupacion.usucodigo inner join seguridad on " +
					"seguridad.segcodigo=agrupacion.segcodigo inner join perfilseguridad on " +
					"seguridad.segcodigo=perfilseguridad.segcodigo " +
					"where usuario.mail='" + aux.usuSes.getMail() + "' and usuario.clave='" + aux.usuSes.getClave() + "' " +
					"group by organizacion.orgcodigo, usuario.usucodigo, seguridad.segcodigo,perfilseguridad.nombre,perfilseguridad.acceso ";

			peticion.put("consulta", consulta);
			peticion.put("respuesta", Respuesta.LISTAMIXTA);

			List listResult = dbc.ejecutarConsulta(peticion);

			if (listResult != null && listResult.size() > 0)
			{
				result = true;
			}// fIN DE if (!controlador.usuSes.getEml_cod().equals(""))
			else
			{
				result = false;
			}

			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}