package com.usuario.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Util;

public class ObtenerListaColumnaUsuarios extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			List listaExpression = new ArrayList();
			List listaUsuario = (List) (new ObtenerListaUsuarios()).ejecutarAccion(peticion);

			if (peticion.containsKey("nombreColumna"))
			{
				String nombreColumna = (String) peticion.get("nombreColumna");
				for (int iteLis = 0; iteLis < listaUsuario.size(); iteLis++)
				{
					Usuario usuario = (Usuario) listaUsuario.get(iteLis);
					listaExpression.add(Util.getInst().obtenerDatoObjeto(usuario, nombreColumna));
				}
			}

			result = listaExpression;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}