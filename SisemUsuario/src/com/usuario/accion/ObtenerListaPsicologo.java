package com.usuario.accion;

import java.util.Map;

import com.browse.BrowseObject;
import com.browse.accion.GenerateBrowseObject;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerListaPsicologo extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			peticion.put("browseTabla", (String) peticion.get("browseTabla"));
			BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
			peticion.put("browseObject", browseObject);
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}