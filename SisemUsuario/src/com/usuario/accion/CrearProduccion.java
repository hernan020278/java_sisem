package com.usuario.accion;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Produccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class CrearProduccion extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			aux.prc = new Produccion();
			Util.getInst().limpiarEntidad(aux.prc);
			Util.getInst().setPeticionEntidad(peticion, aux.prc);

			String codigoFecha = String.valueOf(String.valueOf(Util.getInst().getFechaSql().getDate() + String.valueOf(Util.getInst().getFechaSql().getMonth() + 1) + String.valueOf(Util.getInst().getFechaSql().getYear()+1900)));
			if(peticion.containsKey("Produccion_fecelaboracion"))
			{
				Date prdFecelab = (Date) peticion.get("Produccion_fecelaboracion");
				codigoFecha = String.valueOf(String.valueOf(prdFecelab.getDate() + String.valueOf(prdFecelab.getMonth() + 1) + String.valueOf(prdFecelab.getYear()+1900)));
			}
			
			if(!Util.isEmpty(aux.ope.getUsucodigo().toString()))
			{
				aux.prc.setPrccodigo(new BigDecimal(KeyGenerator.getInst().getUniqueKey()));
				aux.prc.setUsucodigo(aux.ope.getUsucodigo());
				aux.prc.setOperario(aux.ope.getNombre());
				aux.prc.setIdentidad(codigoFecha);
				aux.prc.setEstado(Estado.REGISTRADO);
				aux.prc.setVersion(new BigDecimal(0));
			}
			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}