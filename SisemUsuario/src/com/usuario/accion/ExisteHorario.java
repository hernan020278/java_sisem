package com.usuario.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiplantillahorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class ExisteHorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {
			Auxiliar aux = Auxiliar.getInstance();
			result = false;

			String sql = "select * from asihorario where horarionombre = '" + (String) peticion.get("Asihorario_horarionombre") + "' and estado <> '0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asihorario");
			Asiplantillahorario asiplanhora = (Asiplantillahorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if(!Util.getInst().isEmpty(asiplanhora.getKyplantillahorario()))
			{
				result = true;
				this.setEstado(Estado.SUCCEEDED);
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}