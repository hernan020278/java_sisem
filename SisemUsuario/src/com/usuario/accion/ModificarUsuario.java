package com.usuario.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Area;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class ModificarUsuario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			String usucodigo = (String) peticion.get("Pagina_valor");
			peticion.put("consulta", "select * from usuario where usucodigo=" + usucodigo + "");
			peticion.put("entidadNombre", "Usuario");
			aux.ope = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if (Util.isEmpty(aux.ope.getIdentidad()))
			{
				aux.ope.setIdentidad(KeyGenerator.getInst().obtenerCodigoBarra().toString());
			}
			
			peticion.put("consulta", "select * from area where are_cod=" + aux.ope.getArecodigo() + "");
			peticion.put("entidadNombre", "Area");
			aux.are = (Area) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			peticion.put("consulta", "select * from subarea where subare_cod=" + aux.ope.getSubarecodigo() + "");
			peticion.put("entidadNombre", "SubArea");
			aux.subAre = (SubArea) (new ObtenerEntidad()).ejecutarAccion(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}