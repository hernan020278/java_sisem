package com.usuario.accion;

import java.util.List;
import java.util.Map;

import com.browse.BrowseObject;
import com.browse.accion.GenerateBrowseObject;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerListaUsuarios extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {
			if (!peticion.containsKey("consulta"))
			{
				peticion.put("consulta", "select * from usuario order by nombre");
			}
			peticion.put("entidadNombre", "Usuario");
			List listaUsuario = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

			result = listaUsuario;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}