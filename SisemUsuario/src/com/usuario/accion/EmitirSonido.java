package com.usuario.accion;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import com.comun.motor.Accion;
import com.comun.utilidad.AplicacionGeneral;

public class EmitirSonido extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object resul = null;
		String tipoSonido = (String) peticion.get("sonido");
		String ruta = AplicacionGeneral.getInstance().obtenerRuta("ruta-sonido", "");

        try 
        {
        	AudioClip ac;
			URL url = (new File(AplicacionGeneral.getInstance().obtenerRutaSonidos() + tipoSonido.toLowerCase() + ".wav")).toURI().toURL();
			if (url != null) {

				ac = Applet.newAudioClip(url);
				ac.play();

			} else {
				url = (new File(AplicacionGeneral.getInstance().obtenerRutaSonidos() + "defecto.wav")).toURI().toURL();
				ac = Applet.newAudioClip(url);
				ac.play();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resul;

	}

}