package com.usuario.accion;

import java.util.Map;

import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class AgregarPsicologo extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			System.out.println("Preparamos para agregar");
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}