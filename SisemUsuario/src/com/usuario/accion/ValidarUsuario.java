package com.usuario.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Util;

public class ValidarUsuario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = false;
		String mensajeValor = "";

		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			Usuario usuario = (Usuario) peticion.get("usuario");

			if (!Util.isEmpty(usuario.getIdentidad())) {
				if (!Util.isEmpty(usuario.getOrgcodigo())) {
					if (!Util.isEmpty(usuario.getNombre())) {
						if (!Util.isEmpty(usuario.getIdentidad())) {
							if (!Util.isEmpty(usuario.getClave())) {
								if (!Util.isEmpty(usuario.getMail())) {
									if (!existeUsuarioCodigo(peticion))
									{
										result = true;
									}
									else
									{
										mensajeValor = "Este usuario ya existe";

									}
								}// if (!Util.isEmpty(usuario.getMail())) {
								else
								{
									mensajeValor = "Mail invalido";
								}// if (!Util.isEmpty(usuario.getMail())) {
							}// if (!Util.isEmpty(usuario.getClave())) {
							else
							{
								mensajeValor = "Clave invalida";
							}// if (!Util.isEmpty(usuario.getClave())) {
						}// if (!Util.isEmpty(usuario.getUsuario())) {
						else
						{
							mensajeValor = "Usuario invalido";
						}// if (!Util.isEmpty(usuario.getUsuario())) {
					}// if (!Util.isEmpty(usuario.getNombre())) {
					else
					{
						mensajeValor = "Nombre invalido";
					}// if (!Util.isEmpty(usuario.getNombre())) {
				}
				else
				{
					mensajeValor = "Este usuario no tiene organizacion al q pertencer";
				}// if (!Util.isEmpty(usuario.getUsucodigo())) {
			}// if (!Util.isEmpty(usuario.getUsucodigo())) {
			else
			{
				mensajeValor = "Usuario Codigo invalido";
			}// if (!Util.isEmpty(usuario.getUsucodigo())) {

			if (!mensajeValor.equals(""))
			{
				peticion.put("usuarioValidado", false);
				this.setEstado(Estado.ABORTED);
			}
			else
			{
				peticion.put("usuarioValidado", true);
				peticion.remove("mensajeError");
				this.setEstado(Estado.SUCCEEDED);
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}

		return result;
	}

	public boolean existeUsuarioCodigo(Map peticion) {

		boolean existe = false;
		try {

			Usuario usuario = (Usuario) peticion.get("usuario");
			String sql = "select * from usuario where identidad='" + usuario.getIdentidad() + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Usuario");
			Usuario usu = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if (!Util.getInst().isEmpty(usu.getIdentidad()))
			{
				existe = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return existe;
	}
}