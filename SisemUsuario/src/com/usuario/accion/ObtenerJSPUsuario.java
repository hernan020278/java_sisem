package com.usuario.accion;

import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class ObtenerJSPUsuario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();

		try
		{
			Usuario usuario = (Usuario) peticion.get("usuario");
			Util.getInst().setPeticionEntidad(peticion, usuario);
			result = usuario;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{

			this.setEstado(Estado.FAILED);
			throw e;

		}
		return result;
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				System.out.println("paquete : " + ObtenerJSPUsuario.class.getName());

			}
		});

	}

}