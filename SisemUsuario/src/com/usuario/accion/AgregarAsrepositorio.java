package com.usuario.accion;

import java.util.Map;

import com.comun.entidad.Asirepositorio;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class AgregarAsrepositorio extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			Usuario empleado = new Usuario();
			String identidad = KeyGenerator.getInst().obtenerCodigoBarra();

//			String sql = "select * from asrepositorio where identidad = '" + identidad + "'";
//			peticion.put("consulta", sql);
//			peticion.put("entidadNombre", "Asrepositorio");
//			aux.asirepo = (Asrepositorio) (new ObtenerEntidad()).ejecutarAccion(peticion);

			aux.asirepo = new Asirepositorio();
			Util.getInst().limpiarEntidad(aux.asirepo);
			
			if (Util.isEmpty(empleado.getIdentidad()))
			{
//				aux.asirepo.setIdentidad(identidad);
				aux.asirepo.setEstado(Estado.GENERADO);
			}

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}