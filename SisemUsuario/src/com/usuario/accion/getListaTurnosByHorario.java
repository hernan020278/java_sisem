package com.usuario.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.utilidad.Auxiliar;

public class getListaTurnosByHorario extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;

    try {
      Auxiliar aux = Auxiliar.getInstance();
      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

      String sql = "select asiturno.turnonombre turno " +
          "from asiturno " +
          "where asiturno.kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia() + "' and asiturno.numerodni = '" + aux.usu.getIdentidad() + "' " +
          "order by asiturno.turnonombre asc";

      String[] listaComlumnaTipo = {"STRING"};
      String[] listaColumnaAlias = {"turno"};

      peticion.put("consulta", sql);
      peticion.put("respuesta", Respuesta.LISTAMIXTA);
      peticion.put("listaColumnaTipo", listaComlumnaTipo);
      peticion.put("listaColumnaAlias", listaColumnaAlias);

      List listaRegTurnos = dbc.ejecutarConsulta(peticion);
      List listaTurnos = new ArrayList();
      for (int iteEst = 0; iteEst < listaRegTurnos.size(); iteEst++) {
        Object[] registro = (Object[]) listaRegTurnos.get(iteEst);
        listaTurnos.add((String) registro[0]);
      }

      peticion.put("listaTurnos", listaTurnos);
      result = listaTurnos;
      this.setEstado(Estado.SUCCEEDED);
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      throw e;
    }
    return result;
  }

}