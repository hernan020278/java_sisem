package com.usuario.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Estado;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class CrearUsuario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String orgIde = (String) peticion.get("organizacionIde");
			
			Usuario usuario = new Usuario();
			Util.getInst().limpiarEntidad(usuario);
			Util.getInst().setPeticionEntidad(peticion, usuario);

			usuario.setUsucodigo(new BigDecimal(KeyGenerator.getInst().obtenerLongCodigo()));
			usuario.setOrgcodigo(OrganizacionGeneral.getOrgcodigo().toUpperCase());
			usuario.setEstado(Estado.REGISTRADO);
			usuario.setUdf3(KeyGenerator.getInst().getCadenaAlfanumAleatoria(15));
			usuario.setBloqueado("Y");

			result = usuario;

			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}