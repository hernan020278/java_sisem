package com.usuario.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ModificarPsicologo extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			String usucodigo = (String) peticion.get("Pagina_valor");
			peticion.put("consulta", "select * from usuario where usucodigo='" + usucodigo + "'");
			peticion.put("entidadNombre", "Usuario");
			Usuario psicologo = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			peticion.put("psicologo", psicologo);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}