package com.usuario.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ObtenerUsuarioPorIdentidad extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			Auxiliar aux = Auxiliar.getInstance();
			String identidad = (String) peticion.get("Usuario_identidad");

			peticion.put("consulta", "select * from usuario where identidad='" + identidad + "'");
			peticion.put("entidadNombre", "Usuario");
			aux.usu  = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			result = aux.usu;
			
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}