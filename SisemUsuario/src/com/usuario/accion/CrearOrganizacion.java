package com.usuario.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Organizacion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class CrearOrganizacion extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			aux.org = new Organizacion();
			Util.getInst().limpiarEntidad(aux.org);
			Util.getInst().setPeticionEntidad(peticion, aux.org);

			aux.org.setOrgcodigo(new BigDecimal(KeyGenerator.getInst().obtenerLongCodigo()).toString());
			aux.org.setEstado(Estado.REGISTRADO);

			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}