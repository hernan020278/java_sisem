package com.usuario.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Cliente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerCliente extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try 
		{
			if (!peticion.containsKey("consulta"))
			{
				String cliCod = (String) peticion.get("Pagina_valor");
				peticion.put("consulta", "select * from cliente where cli_cod='" + cliCod + "'");
			}
			peticion.put("entidadNombre", "Cliente");
			Cliente cliente = (Cliente) (new ObtenerEntidad()).ejecutarAccion(peticion);
			result = cliente;
			
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}