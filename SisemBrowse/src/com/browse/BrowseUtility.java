package com.browse;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.referencia.Estado;
import com.comun.referencia.FechaFormato;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class BrowseUtility {
   public static BrowseUtility browseUtility = new BrowseUtility();

   public static List populateValues(List list, BrowseObject b, String o, String userId) throws Exception {
      ArrayList rows = new ArrayList();

      try {
         if (list != null) {
            BrowseColumn[] browseColumns = b.getBrowseColumns();
            BrowseUtility bu = new BrowseUtility();
            String userDateFormat = "yyyy-MM-dd";

            for(int il = 0; il < list.size(); ++il) {
               Object object = list.get(il);
               List[] valueList = bu.getRowValues(object, browseColumns, o, userDateFormat);
               rows.add(valueList);
            }

            b.setRowCount(list.size());
            if (b.getPageSize() > 0) {
               b.setPageCount((b.getRowCount() - 1) / b.getPageSize() + 1);
            } else {
               b.setPageCount(1);
            }
         }
      } catch (Exception var11) {
         var11.printStackTrace();
      }

      return rows;
   }

   public static Object getInputColumnValue(BrowseColumn column) {
      Object obj = null;
      int size = 0;
      if (column.getInputSize() <= 0) {
         size = column.getSize();
      } else if (column.getSize() <= 0) {
         size = column.getSize();
      }

      obj = "<input type=text name=\"" + column.getColumnName() + "\" value=\"\" size=" + size + ">";
      return obj;
   }

   public static Object getInputColumnValue(BrowseColumn column, Object registro, String organizationId, BrowseColumn[] browseColumns, String browseId, String userDateFormat) throws Exception {
      Object obj = null;
      Browse browse = new Browse();
      BrowseObject browseObject = browse.getBrowseObject();
      Map keyMap = null;
      String inputValue = browse.getInputValue((Map)keyMap);
      int size = 0;
      if (column.getInputSize() <= 0) {
         size = column.getSize();
      } else if (column.getSize() <= 0) {
         size = column.getSize();
      }

      obj = "<input type=text name=\"" + column.getColumnName() + "\" value=\"" + inputValue + "\" size=" + size + ">";
      return obj;
   }

   public static Object getCheckboxColumnValue(BrowseColumn column, Object registro, String organizationId, BrowseColumn[] browseColumns, String browseId, String userDateFormat) throws Exception {
      Object obj = null;
      Browse browse = new Browse();
      BrowseObject browseObject = browse.getBrowseObject();
      Map keyMap = null;
      String inputValue = browse.getInputValue((Map)keyMap);
      String functionOnClick = "";
      if (!Util.isEmpty(column.getOnClick())) {
         functionOnClick = "onclick=\"" + column.getOnClick() + "\"";
      }

      if (inputValue.equals("Y")) {
         obj = "<input type=checkbox name=\"" + column.getColumnName() + "\" value=\"Y\" " + functionOnClick + " checked>";
      } else if (column.isChecked() && Util.isEmpty(inputValue)) {
         obj = "<input type=checkbox name=\"" + column.getColumnName() + "\" value=\"Y\" " + functionOnClick + " checked>";
      } else {
         obj = "<input type=checkbox name=\"" + column.getColumnName() + "\" value=\"Y\" " + functionOnClick + " >";
      }

      obj = obj + "<input type=hidden name=\"Input_" + column.getColumnName().substring(2) + "\" value=\"" + inputValue + "\">";
      return obj;
   }

   public static String getSelectColumnValue(BrowseColumn column, Object registro, BrowseColumn[] browseColumns, int rowIndex) throws Exception {
      StringBuffer selectInput = new StringBuffer();
      if (column.isSelectInput()) {
         selectInput.append("<select name=\"" + column.getColumnName() + "\" onchange=\"setRowStatus(" + rowIndex + ", 'U');\">");
         List selectOptions = column.getArgumentColumns();

         for(int colsIndex = 0; colsIndex < selectOptions.size(); ++colsIndex) {
            ComputedColumn computedColumn = (ComputedColumn)selectOptions.get(colsIndex);
            String optionValue = computedColumn.getColumnType();
            String optionText = (String)computedColumn.getValue();
            Object columnValueObj = column.getValue();
            String columnValue = "";
            if (columnValueObj == null && registro instanceof Object[]) {
               Object[] rowArray = (Object[])((Object[])registro);
               columnValueObj = rowArray[column.getIndex()];
            }

            if (columnValueObj instanceof String) {
               columnValue = (String)columnValueObj;
            }

            selectInput.append("<option value=\"" + optionValue + "\"");
            if (optionValue.equals(columnValue)) {
               selectInput.append(" selected");
            }

            selectInput.append(">");
            selectInput.append(optionText);
            selectInput.append("</option>");
         }

         selectInput.append("</select>");
      }

      return selectInput.toString();
   }

   public static Object getRadioColumnValue(BrowseColumn column, Object registro, String organizationId, BrowseColumn[] browseColumns, String browseId, String userDateFormat) throws Exception {
      Object obj = null;
      Browse browse = new Browse();
      BrowseObject browseObject = browse.getBrowseObject();
      Map keyMap = null;
      String radioValue = (String)((Map)keyMap).get("RadioValue");
      obj = "<input type=radio name='c_radio' value='" + radioValue + "'>";
      return obj;
   }

   public static Object getColumnValueObject(BrowseColumn column, Object registro, String organizationId, BrowseColumn[] browseColumns, String browseId, String userDateFormat) throws Exception {
      Object objetoValor = null;

      try {
         if (column.getType().equals("Input")) {
            objetoValor = getInputColumnValue(column, registro, organizationId, browseColumns, browseId, userDateFormat);
         } else if (column.getType().equals("Checkbox")) {
            objetoValor = getCheckboxColumnValue(column, registro, organizationId, browseColumns, browseId, userDateFormat);
         } else if (column.getType().equals("Radio")) {
            objetoValor = getRadioColumnValue(column, registro, organizationId, browseColumns, browseId, userDateFormat);
         } else if (column.getType().equalsIgnoreCase("Integer") && Util.isEmpty(column.getLink())) {
            if (column.isCheckbox()) {
               objetoValor = false;
            } else {
               objetoValor = getColumnValue(column.getColumnName(), registro, browseColumns);
            }
         } else if (!Util.isEmpty(column.getLink()) && !column.isTextInput()) {
            objetoValor = column.getLink();
         } else {
            int index = -1;

            for(int ite = 0; ite < browseColumns.length; ++ite) {
               BrowseColumn iteCol = browseColumns[ite];
               if (iteCol.getAlias().equalsIgnoreCase(column.getAlias())) {
                  index = ite;
                  break;
               }
            }

            if (registro instanceof Object[]) {
               Object[] objetoRegistro = (Object[])((Object[])registro);
               objetoValor = objetoRegistro[index];
            } else {
               objetoValor = registro;
            }

            String strFecha = objetoValor.toString();
            String[] aStrFecha = strFecha.split(" ");
            if (Util.getInst().isDateTimeObject(strFecha).equals("TIMESTAMP")) {
               objetoValor = Util.getInst().getToFecHor(objetoValor.toString());
            } else if (Util.getInst().isDateTimeObject(strFecha).equals("TIME")) {
               objetoValor = Util.getInst().fromStrdateToTime(aStrFecha[1]).toString();
            } else if (Util.getInst().isDateTimeObject(strFecha).equals("DATE")) {
               objetoValor = Util.getInst().fromStrdateToString(aStrFecha[0], "yyyy-MM-dd");
            }

            if (column.getColumnName().toUpperCase().contains("_ESTADO")) {
               objetoValor = Estado.toValor(objetoValor.toString());
            }
         }
      } catch (Exception var10) {
         var10.printStackTrace();
         throw var10;
      }

      return Util.ckNull(objetoValor);
   }

   public static Object getColumnObject(BrowseColumn column, Object registro, String organizationId, BrowseColumn[] browseColumns, String browseId, String userId) throws Exception {
      Object obj = null;

      try {
         String userDateFormat = "yyyy-MM-dd";
         obj = getColumnValueObject(column, registro, organizationId, browseColumns, browseId, userDateFormat);
         return obj;
      } catch (Exception var8) {
         throw var8;
      }
   }

   public static List getArgumentValuesFromColumn(BrowseColumn column, Object registro, BrowseColumn[] browseColumns, String organizationId, String userDateFormat) {
      List argumentValues = new ArrayList();
      List columnsToUse = column.getArgumentColumns();
      Object obj = null;

      for(int colsIndex = 0; colsIndex < columnsToUse.size(); ++colsIndex) {
         ComputedColumn computedColumn = (ComputedColumn)columnsToUse.get(colsIndex);
         String columnType = computedColumn.getColumnType();
         String columnValue = (String)computedColumn.getValue();
         String argument = "";
         if (!Util.isEmpty(columnValue) && !columnType.equalsIgnoreCase("constant") && !column.isSelectInput()) {
            Object columnValueObj = getColumnValue(columnValue, registro, browseColumns);
            if (column.getType().length() >= 0 && !column.getType().equalsIgnoreCase("String") && !column.getType().equalsIgnoreCase("constant")) {
               obj = formatBrowseColumnValue(columnValueObj, computedColumn.getColumnType(), organizationId, userDateFormat);
            }

            columnValue = obj != null ? String.valueOf(obj) : "";
         }

         if (!Util.isEmpty(columnValue)) {
            argumentValues.add(columnValue);
         }
      }

      return argumentValues;
   }

   public static List getCalculateValuesFromColumn(BrowseColumn column, Object registro, BrowseColumn[] browseColumns, String organizationId, String userDateFormat) {
      List calculateValues = new ArrayList();
      List columnsToUse = column.getCalculateColumns();
      String typeColumns = "";

      for(int colsIndex = 0; colsIndex < columnsToUse.size(); ++colsIndex) {
         ComputedColumn computedColumn = (ComputedColumn)columnsToUse.get(colsIndex);
         String columnType = computedColumn.getColumnType();
         String columnValue = (String)computedColumn.getValue();
         String argument = "";
         if (colsIndex == 0) {
            typeColumns = columnType;
         }

         if (!Util.isEmpty(columnValue) && !columnType.equalsIgnoreCase("constant") && !column.isSelectInput() && columnType.equalsIgnoreCase(typeColumns)) {
            Object columnValueObj = getColumnValue(columnValue, registro, browseColumns);
            columnValue = columnValueObj != null ? String.valueOf(columnValueObj) : "";
         }

         calculateValues.add(columnValue);
      }

      return calculateValues;
   }

   public static Object getColumnValue(BrowseColumn column, Object object, String organizationId, BrowseColumn[] browseColumns, String browseId, String userDateFormat) {
      Object result;
      if (column.getClassName().equals("Input")) {
         try {
            result = getInputColumnValue(column, object, organizationId, browseColumns, browseId, userDateFormat);
         } catch (Exception var15) {
            result = getInputColumnValue(column);
         }
      } else {
         String methodName = column.getMethodName();
         if (object == null) {
            result = null;
         } else {
            Class c = object.getClass();

            Method method;
            try {
               method = c.getMethod(methodName, (Class[])null);
               result = method.invoke(object);
            } catch (Exception var14) {
               try {
                  Method pkMethod = c.getMethod("getComp_id", (Class[])null);
                  Object pkObj = pkMethod.invoke(object);
                  method = pkObj.getClass().getMethod(methodName, (Class[])null);
                  result = method.invoke(pkObj);
               } catch (Exception var13) {
                  method = null;
                  result = null;
               }
            }
         }
      }

      return result;
   }

   public static Object getColumnValue(BrowseColumn column, Object object) {
      Object result;
      if (column.getClassName().equals("Input")) {
         result = getInputColumnValue(column);
      } else {
         String methodName = column.getMethodName();
         if (object == null) {
            result = null;
         } else {
            Class c = object.getClass();

            Method method;
            try {
               method = c.getMethod(methodName, (Class[])null);
               result = method.invoke(object);
            } catch (Exception var10) {
               try {
                  Method pkMethod = c.getMethod("getComp_id", (Class[])null);
                  Object pkObj = pkMethod.invoke(object);
                  method = pkObj.getClass().getMethod(methodName, (Class[])null);
                  result = method.invoke(pkObj);
               } catch (Exception var9) {
                  method = null;
                  result = null;
               }
            }
         }
      }

      return result;
   }

   public static Object getColumnValue(String columnName, Object object) {
      int ind = columnName.indexOf("_");
      columnName.substring(0, ind);
      String methodName = "get" + columnName.substring(ind + 1, ind + 2).toUpperCase() + columnName.substring(ind + 2);
      Class c = object.getClass();

      Method method;
      Object result;
      try {
         method = c.getMethod(methodName, (Class[])null);
         result = method.invoke(object);
      } catch (Exception var14) {
         try {
            Method pkMethod = c.getMethod("getComp_id", (Class[])null);
            Object pkObj = pkMethod.invoke(object);
            method = pkObj.getClass().getMethod(methodName, (Class[])null);
            result = method.invoke(pkObj);
         } catch (Exception var13) {
            try {
               Method pkMethod = c.getMethod("getComp_id", (Class[])null);
               Object pkObj = pkMethod.invoke(object);
               if (methodName.startsWith("getId_")) {
                  ind += 3;
                  methodName = "get" + columnName.substring(ind + 1, ind + 2).toUpperCase() + columnName.substring(ind + 2);
               }

               method = pkObj.getClass().getMethod(methodName, (Class[])null);
               result = method.invoke(pkObj);
            } catch (Exception var12) {
               method = null;
               result = null;
            }
         }
      }

      return result;
   }

   public static Object getAliasNameValue(String aliasName, Object registro, BrowseColumn[] browseColumns) {
      int index = -1;
      int currentIndex = 0;

      for(int i = 0; i < browseColumns.length; ++i) {
         BrowseColumn currentColumn = browseColumns[i];
         if (currentColumn.getAlias().equalsIgnoreCase(aliasName)) {
            index = currentIndex;
            i = browseColumns.length;
         }

         ++currentIndex;
      }

      Object returnObject;
      try {
         if (registro instanceof Object[]) {
            Object[] objetoRegistro = (Object[])((Object[])registro);
            returnObject = objetoRegistro[index];
         } else {
            returnObject = registro;
         }
      } catch (Exception var7) {
         returnObject = new String("");
      }

      return Util.ckNull(returnObject);
   }

   public static Object getColumnValue(String ColumnName, Object registro, BrowseColumn[] browseColumns) {
      int index = -1;
      int currentIndex = 0;

      for(int i = 0; i < browseColumns.length; ++i) {
         BrowseColumn currentColumn = browseColumns[i];
         if (currentColumn.getColumnName().equalsIgnoreCase(ColumnName)) {
            index = currentIndex;
            i = browseColumns.length;
         }

         ++currentIndex;
      }

      Object returnObject;
      try {
         if (registro instanceof Object[]) {
            Object[] objetoRegistro = (Object[])((Object[])registro);
            returnObject = objetoRegistro[index];
         } else {
            returnObject = registro;
         }
      } catch (Exception var7) {
         returnObject = new String("");
      }

      return Util.ckNull(returnObject);
   }

   public static String getColumnLinkObject(BrowseColumn column, Object objetoRegistro, BrowseColumn[] browseColumns) {
      String linkString = column.getLink();
      StringBuffer linkSB = new StringBuffer(linkString);
      ArrayList columnNames = column.getLinkedColumns();

      for(int index = 0; index < columnNames.size(); ++index) {
         Object value = getColumnValue((String)columnNames.get(index), objetoRegistro, browseColumns);
         if (value instanceof String && value.toString().indexOf("'") >= 0) {
            value = Util.encodeHtml(value.toString());
         }

         try {
            int poundIndex = linkSB.indexOf("'#");
            if (value instanceof String && (value.toString().contains("'") || value.toString().contains("\""))) {
               value = Util.encodeHtml(value.toString());
            }

            linkSB.replace(poundIndex + 1, linkSB.indexOf("'", poundIndex + 1), String.valueOf(value));
         } catch (Exception var9) {
            var9.printStackTrace();
         }
      }

      return linkSB.toString();
   }

   public static String populateScriptCodeParameters(BrowseColumn column, Object objetoRegistro, BrowseColumn[] browseColumns) {
      String scriptCode = column.getScriptCode();
      StringBuffer scriptCodeSB = new StringBuffer(scriptCode);
      List columnNames = column.getScriptCodeColumns();

      for(int index = 0; index < columnNames.size(); ++index) {
         Object value = getColumnValue((String)columnNames.get(index), objetoRegistro, browseColumns);
         if (value instanceof String && value.toString().indexOf("'") >= 0) {
            value = Util.encodeHtml(value.toString());
         }

         try {
            int poundIndex = scriptCodeSB.indexOf("'#");
            if (value instanceof String && (value.toString().contains("'") || value.toString().contains("\""))) {
               value = Util.encodeHtml(value.toString());
            }

            scriptCodeSB.replace(poundIndex + 1, scriptCodeSB.indexOf("'", poundIndex + 1), String.valueOf(value));
         } catch (Exception var9) {
            var9.printStackTrace();
         }
      }

      return scriptCodeSB.toString();
   }

   public static String populateLinkParameters(BrowseColumn column, Object object) {
      Log.debug(browseUtility, column.getClassName() + "  BrowseUtility.populateLinkParameters().");
      String clink = column.getLink();
      if (clink.length() > 0) {
         int p = clink.indexOf("?");
         String tmpName;
         if (p != -1) {
            StringBuffer sresult = new StringBuffer(clink.substring(0, p));
            String params = clink.substring(p + 1);
            StringTokenizer parser = new StringTokenizer(params, "&");
            int numTokens = parser.countTokens();
            tmpName = null;
            String sname = null;
            String sdata = null;
            String svalue = null;
            int bx = 0;

            while(true) {
               if (bx >= numTokens) {
                  clink = sresult.toString();
                  break;
               }

               tmpName = parser.nextToken();
               p = tmpName.indexOf("=");
               if (p != -1) {
                  sname = tmpName.substring(0, p);
                  svalue = tmpName.substring(p + 1).trim();
                  if (bx > 0) {
                     sresult.append("&");
                  } else {
                     sresult.append("?");
                  }

                  if (!svalue.substring(0, 1).equals("#")) {
                     sresult.append(sname + "=" + svalue);
                  } else {
                     svalue = svalue.substring(1);
                     sresult.append(sname + "=");
                     sdata = String.valueOf(getColumnValue(column, object)).trim();

                     for(int ip = sdata.indexOf(" "); ip >= 0; ip = sdata.indexOf(" ")) {
                        sresult.append(sdata.substring(0, ip) + "%20");
                        sdata = sdata.substring(ip + 1);
                     }

                     sresult.append(sdata);
                  }
               }

               ++bx;
            }
         }

         p = clink.indexOf("javascript");
         if (p != -1) {
            int pos = clink.indexOf("#");
            int subEnd = 0;
            String sdata = "";

            StringBuffer sb;
            for(sb = new StringBuffer(); pos > -1; pos = clink.indexOf("#", subEnd)) {
               sb.append(clink.substring(subEnd, pos));
               subEnd = clink.indexOf("^", pos);
               tmpName = clink.substring(pos + 1, subEnd);
               sdata = String.valueOf(getColumnValue(tmpName, object)).trim();
               sb.append(sdata);
               ++subEnd;
            }

            sb.append(clink.substring(subEnd));
            clink = sb.toString();
         }
      }

      Log.debug(browseUtility, column.getClassName() + "  BrowseUtility.populateLinkParameters() COMPLETE.");
      return clink;
   }

   private List[] getRowValues(Object object, BrowseColumn[] browseColumns, String organizationId, String dateFormat) {
      List[] registro = new ArrayList[3];
      List displayValues = new ArrayList();
      List hiddenValues = new ArrayList();
      List detailValues = new ArrayList();
      Log.debug(browseUtility, " BrowseUtility.getRowValues()");

      for(int i = 0; i < browseColumns.length; ++i) {
         BrowseColumn column = getBrowseColumnCopy(browseColumns[i]);
         Object entity = getEntityObject(object, column);
         Object result = null;
         if (entity != null) {
            result = getColumnValue(column, entity);
         }

         if (Util.isEmpty(column.getType()) && result instanceof BigDecimal) {
            column.setType("BigDecimal");
         }

         result = formatBrowseColumnValue(result, column, organizationId, dateFormat);
         column.setValue(result);
         column.setLink(populateLinkParameters(column, entity));
         if (column.isHidden()) {
            hiddenValues.add(column);
         } else if (column.isDetail()) {
            detailValues.add(column);
         } else {
            displayValues.add(column);
         }
      }

      registro[0] = displayValues;
      registro[1] = hiddenValues;
      registro[2] = detailValues;
      Log.debug(browseUtility, " BrowseUtility.getRowValues() COMPLETE.");
      return registro;
   }

   public static BrowseColumn getBrowseColumnCopy(BrowseColumn originalBC) {
      BrowseColumn column = new BrowseColumn(originalBC.getOid(), originalBC.getLanguage());
      column.setAlias(originalBC.getAlias());
      column.setAlignment(originalBC.getAlignment());
      column.setAllowFilter(originalBC.getAllowFilter());
      column.setArgumentColumns(originalBC.getArgumentColumns());
      column.setCheckbox(originalBC.isCheckbox());
      column.setClassName(originalBC.getClassName());
      column.setColumnName(originalBC.getColumnName());
      column.setComputeType(originalBC.getComputeType());
      column.setCalculateColumns(originalBC.getCalculateColumns());
      column.setDetail(originalBC.isDetail());
      column.setHidden(originalBC.isHidden());
      column.setHiddenInput(originalBC.isHiddenInput());
      column.setIndex(originalBC.getIndex());
      column.setInputSize(originalBC.getInputSize());
      column.setLabel(originalBC.getLabel());
      column.setLanguage(originalBC.getLanguage());
      column.setLink(originalBC.getLink());
      column.setLinkImage(originalBC.getLinkImage());
      column.setLinkedColumns(originalBC.getLinkedColumns());
      column.setMethodName(originalBC.getMethodName());
      column.setOid(originalBC.getOid());
      column.setSelectInput(originalBC.isSelectInput());
      column.setSize(originalBC.getSize());
      column.setSort(originalBC.getSort());
      column.setStoreRequestValue(originalBC.storeRequestValue());
      column.setTextInput(originalBC.isTextInput());
      column.setTrim(originalBC.getTrim());
      column.setType(originalBC.getType());
      column.setValue(originalBC.getValue());
      return column;
   }

   public static Object formatBrowseColumnValue(Object value, String type, String organizationId, String dateFormat) {
      return formatBrowseColumnValue(value, (String)type, new ArrayList(), organizationId, dateFormat);
   }

   public static Object formatBrowseColumnValue(Object value, String type, List arguments, String organizationId, String dateFormat) {
      Object result = value;
      PropiedadGeneral props = PropiedadGeneral.getInstance(organizationId);
      if (value != null && !Util.isEmpty(type)) {
         if (!type.equalsIgnoreCase("Date") && !(value instanceof Date) && !(value instanceof java.util.Date)) {
            if (type.equalsIgnoreCase("QtyDecimal")) {
               result = Util.getBigDecimalFormatted(value, Integer.valueOf(props.getProperty("MISC", "QTYDECIMALS", "2")));
            } else {
               String s_result;
               if (type.equalsIgnoreCase("CurrencyDecimal")) {
                  s_result = !arguments.isEmpty() ? (String)arguments.get(0) : props.getProperty("MISC", "BASECURRENCY", "USD");
                  if (value != null && !(value instanceof String)) {
                     result = Util.getFormattedCurrency(value, s_result, organizationId);
                  } else {
                     result = "";
                  }
               } else if (type.equalsIgnoreCase("CurrencyFormat")) {
                  s_result = !arguments.isEmpty() ? (String)arguments.get(0) : props.getProperty("MISC", "BASECURRENCY", "USD");
                  result = Util.getFormattedCurrency(value, s_result, organizationId);
               } else if (type.equalsIgnoreCase("DollarDecimal")) {
                  if (value != null && !(value instanceof String)) {
                     result = Util.getFormattedCurrency(value, props.getProperty("MISC", "BASECURRENCY", "USD"), organizationId);
                  } else {
                     result = "";
                  }
               } else if (type.equalsIgnoreCase("PriceDecimal")) {
                  result = Util.getFormattedCurrency(value, props.getProperty("MISC", "BASECURRENCY", "USD"), organizationId);
               } else if (type.equalsIgnoreCase("BigDecimal")) {
                  result = Util.getBigDecimalFormatted(value, 2);
               } else if (type.equalsIgnoreCase("NoDecimal")) {
                  result = Util.getBigDecimalFormatted(value, 0);
               } else if (type.equalsIgnoreCase("STATUS")) {
                  result = Estado.toString(String.valueOf(value), organizationId);
               } else if (type.equalsIgnoreCase("EMAIL")) {
                  s_result = (String)value;
                  if (s_result.indexOf(";") > 0) {
                     result = s_result.substring(0, s_result.indexOf(";")) + ";...";
                  }
               } else if (type.equalsIgnoreCase("UPPERCASE")) {
                  result = Util.ckNull(String.valueOf(value)).toUpperCase();
               } else if (type.equalsIgnoreCase("LOWERCASE")) {
                  result = Util.ckNull(String.valueOf(value)).toLowerCase();
               } else if (type.equalsIgnoreCase("STRING")) {
                  result = Util.ckNull(String.valueOf(value));
               }
            }
         } else {
            result = Util.getDateFormat(value, dateFormat);
         }
      }

      return result;
   }

   public static Object formatBrowseColumnValue(Object value, BrowseColumn column, List arguments, String organizationId, String dateFormat) {
      Object result = formatBrowseColumnValue(value, column.getType(), arguments, organizationId, dateFormat);
      return cleanObjectResult(result, column);
   }

   public static Object formatBrowseColumnValue(Object value, BrowseColumn column, String organizationId, String dateFormat) {
      Object result = formatBrowseColumnValue(value, column.getType(), organizationId, dateFormat);
      return cleanObjectResult(result, column);
   }

   public static Object cleanObjectResult(Object result, BrowseColumn column) {
      if (result == null) {
         if (result instanceof BigDecimal) {
            result = new BigDecimal("0");
         } else {
            result = "";
         }
      }

      if (result instanceof String) {
         String temp = String.valueOf(result);
         if (column.getTrim() > 0 && !column.isHidden() && temp.length() > column.getTrim()) {
            temp = temp.substring(0, column.getTrim());
         }

         if (!column.getClassName().equalsIgnoreCase("Input") && !column.getType().equals("Checkbox")) {
            temp = temp.replaceAll(String.valueOf('"'), "&quot;");
         }

         result = temp;
      }

      return result;
   }

   public static Object getEntityObject(Object object, BrowseColumn column) {
      Log.debug(browseUtility, column.getClassName() + " " + column.getColumnName() + " BrowseUtility.getEntityObject()");

      try {
         if (object instanceof Object[]) {
            Object[] objects = (Object[])((Object[])object);
            if (column.getClassName().equalsIgnoreCase("Input")) {
               return objects[0];
            } else {
               String className = "com.comun.entidad." + column.getClassName();
               Object entity = null;

               for(int i = 0; i < objects.length; ++i) {
                  Object entityObj = objects[i];
                  if (entityObj != null) {
                     String objName = entityObj.getClass().getName();
                     if (objName.equals(className)) {
                        entity = entityObj;
                     }
                  }
               }

               return entity;
            }
         } else {
            return object;
         }
      } catch (Exception var8) {
         var8.printStackTrace();
         return object;
      }
   }

   public static ArrayList getColumnNamesFromLink(BrowseColumn column) {
      String linkString = column.getLink();
      ArrayList links = new ArrayList();
      if (linkString.length() > 0) {
         int p = linkString.indexOf("?");
         String sname;
         if (p != -1) {
            StringBuffer sresult = new StringBuffer(linkString.substring(0, p));
            String params = linkString.substring(p + 1);
            StringTokenizer parser = new StringTokenizer(params, "&");
            int numTokens = parser.countTokens();
            String snamevalue = null;
            sname = null;
            String sdata = null;
            String svalue = null;

            for(int bx = 0; bx < numTokens; ++bx) {
               snamevalue = parser.nextToken();
               p = snamevalue.indexOf("=");
               if (p != -1) {
                  sname = snamevalue.substring(0, p);
                  svalue = snamevalue.substring(p + 1).trim();
                  if (bx > 0) {
                     sresult.append("&");
                  } else {
                     sresult.append("?");
                  }

                  if (svalue.substring(0, 1).equals("#")) {
                     svalue = svalue.substring(1);
                     sresult.append(sname + "=");
                     links.add(svalue);
                     sresult.append("#" + bx);
                  } else {
                     sresult.append(sname + "=" + svalue);
                  }
               }
            }

            linkString = sresult.toString();
         }

         p = linkString.indexOf("javascript");
         if (p != -1) {
            int pos = linkString.indexOf("#");
            int subEnd = 0;
            String sdata = "";
            StringBuffer sb = new StringBuffer();

            for(int bx = 0; pos > -1; pos = linkString.indexOf("#", subEnd)) {
               sb.append(linkString.substring(subEnd, pos));
               subEnd = linkString.indexOf("^", pos);
               sname = linkString.substring(pos + 1, subEnd);
               links.add(sname);
               sb.append("#" + bx);
               ++subEnd;
               ++bx;
            }

            sb.append(linkString.substring(subEnd));
            linkString = sb.toString();
         }
      }

      column.setLinkedColumns(links);
      column.setLink(linkString);
      return links;
   }

   public static ArrayList getColumnNamesFromScriptCode(BrowseColumn column) {
      String scriptCodeString = column.getScriptCode();
      ArrayList scriptCodes = new ArrayList();
      if (scriptCodeString.length() > 0) {
         int p = scriptCodeString.indexOf("?");
         String sname;
         if (p != -1) {
            StringBuffer sresult = new StringBuffer(scriptCodeString.substring(0, p));
            String params = scriptCodeString.substring(p + 1);
            StringTokenizer parser = new StringTokenizer(params, "&");
            int numTokens = parser.countTokens();
            String snamevalue = null;
            sname = null;
            String svalue = null;

            for(int bx = 0; bx < numTokens; ++bx) {
               snamevalue = parser.nextToken();
               p = snamevalue.indexOf("=");
               if (p != -1) {
                  sname = snamevalue.substring(0, p);
                  svalue = snamevalue.substring(p + 1).trim();
                  if (bx > 0) {
                     sresult.append("&");
                  } else {
                     sresult.append("?");
                  }

                  if (svalue.substring(0, 1).equals("#")) {
                     svalue = svalue.substring(1);
                     sresult.append(sname + "=");
                     scriptCodes.add(svalue);
                     sresult.append("#" + bx);
                  } else {
                     sresult.append(sname + "=" + svalue);
                  }
               }
            }

            scriptCodeString = sresult.toString();
         }

         p = scriptCodeString.indexOf("#");
         if (p != -1) {
            int pos = scriptCodeString.indexOf("#");
            int subEnd = 0;
            String sdata = "";
            StringBuffer sb = new StringBuffer();

            for(int bx = 0; pos > -1; pos = scriptCodeString.indexOf("#", subEnd)) {
               sb.append(scriptCodeString.substring(subEnd, pos));
               subEnd = scriptCodeString.indexOf("^", pos);
               sname = scriptCodeString.substring(pos + 1, subEnd);
               scriptCodes.add(sname);
               sb.append("#" + bx);
               ++subEnd;
               ++bx;
            }

            sb.append(scriptCodeString.substring(subEnd));
            scriptCodeString = sb.toString();
         }
      }

      column.setScriptCodeColumns(scriptCodes);
      column.setScriptCode(scriptCodeString);
      return scriptCodes;
   }

   public static ArrayList getColumnNamesFromFunction(BrowseColumn column) {
      String functionString = column.getOnClick();
      ArrayList functions = new ArrayList();
      if (functionString.length() > 0) {
         int p = functionString.indexOf("?");
         String sname;
         if (p != -1) {
            StringBuffer sresult = new StringBuffer(functionString.substring(0, p));
            String params = functionString.substring(p + 1);
            StringTokenizer parser = new StringTokenizer(params, "&");
            int numTokens = parser.countTokens();
            String snamevalue = null;
            sname = null;
            String sdata = null;
            String svalue = null;

            for(int bx = 0; bx < numTokens; ++bx) {
               snamevalue = parser.nextToken();
               p = snamevalue.indexOf("=");
               if (p != -1) {
                  sname = snamevalue.substring(0, p);
                  svalue = snamevalue.substring(p + 1).trim();
                  if (bx > 0) {
                     sresult.append("&");
                  } else {
                     sresult.append("?");
                  }

                  if (svalue.substring(0, 1).equals("#")) {
                     svalue = svalue.substring(1);
                     sresult.append(sname + "=");
                     functions.add(svalue);
                     sresult.append("#" + bx);
                  } else {
                     sresult.append(sname + "=" + svalue);
                  }
               }
            }

            functionString = sresult.toString();
         }

         p = functionString.indexOf("javascript");
         if (p != -1) {
            int pos = functionString.indexOf("#");
            int subEnd = 0;
            String sdata = "";
            StringBuffer sb = new StringBuffer();

            for(int bx = 0; pos > -1; pos = functionString.indexOf("#", subEnd)) {
               sb.append(functionString.substring(subEnd, pos));
               subEnd = functionString.indexOf("^", pos);
               sname = functionString.substring(pos + 1, subEnd);
               functions.add(sname);
               sb.append("#" + bx);
               ++subEnd;
               ++bx;
            }

            sb.append(functionString.substring(subEnd));
            functionString = sb.toString();
         }
      }

      column.setOnClick(functionString);
      return functions;
   }

   public static Map getColumnKeyValue(List browseKeys, Object resultObj, String organizationId, BrowseColumn[] browseColumns, String browseId, String userDateFormat) throws Exception {
      HashMap keyMap = new HashMap();

      try {
         for(int ik = 0; ik < browseKeys.size(); ++ik) {
            BrowseColumn keyColumn = (BrowseColumn)browseKeys.get(ik);
            Object keyObj = getColumnValueObject(keyColumn, resultObj, organizationId, browseColumns, browseId, userDateFormat);
            String thisKey = "";
            if (keyObj instanceof String) {
               thisKey = (String)keyObj;
            } else if (keyObj instanceof BigDecimal) {
               thisKey = ((BigDecimal)keyObj).toString();
            } else if (keyObj instanceof StringBuffer) {
               thisKey = keyObj.toString();
            }

            keyMap.put(keyColumn.getAlias(), thisKey);
         }

         return keyMap;
      } catch (Exception var11) {
         throw var11;
      }
   }

   public static Map getColumnKeyValue(List browseKeys, Object resultObj, String organizationId, BrowseColumn[] browseColumns, String browseId, String userDateFormat, String columnName) throws Exception {
      HashMap keyMap = new HashMap();

      try {
         for(int ik = 0; ik < browseKeys.size(); ++ik) {
            BrowseColumn keyColumn = (BrowseColumn)browseKeys.get(ik);
            Object keyObj = getColumnValueObject(keyColumn, resultObj, organizationId, browseColumns, browseId, userDateFormat);
            String thisKey = "";
            if (keyObj instanceof String) {
               thisKey = (String)keyObj;
            } else if (keyObj instanceof BigDecimal) {
               thisKey = ((BigDecimal)keyObj).toString();
            }

            keyMap.put(keyColumn.getAlias() + "-Input_" + columnName.substring(2), thisKey);
         }

         return keyMap;
      } catch (Exception var12) {
         throw var12;
      }
   }

   public static void obtenerDatosBrowseFilter(Map peticion, List filters) {
      if (filters != null) {
         int numFil = filters.size();
         String[] filtroColumna = new String[numFil];
         String[] filtroTipo = new String[numFil];
         String[] filtroOperador = new String[numFil];
         String[] filtroValor = new String[numFil];
         String[] filtroLogico = new String[numFil];
         String[] filtroOrden = new String[numFil];
         String[] filtroOriginal = new String[numFil];

         for(int iteFil = 0; iteFil < numFil; ++iteFil) {
            new StringBuffer("");
            BrowseFilter filter = (BrowseFilter)filters.get(iteFil);
            filtroColumna[iteFil] = filter.getColumnName();
            filtroTipo[iteFil] = filter.getType();
            filtroOperador[iteFil] = filter.getOperator();
            filtroValor[iteFil] = filter.getValue();
            filtroLogico[iteFil] = filter.getLogicalOperator();
            filtroOrden[iteFil] = filter.getSort();
         }

         peticion.put("filtroColumna", filtroColumna);
         peticion.put("filtroTipo", filtroTipo);
         peticion.put("filtroOperador", filtroOperador);
         peticion.put("filtroValor", filtroValor);
         peticion.put("filtroLogico", filtroLogico);
         peticion.put("filtroOrden", filtroOrden);
         peticion.put("filtroOriginal", filtroOriginal);
      }

   }

   public static Object obtenerValor(Browse browse, BrowseColumn[] browseColumns, Object objetoRegistro, int idxCol) {
      try {
         BrowseColumn columna = browseColumns[idxCol];
         return obtenerValor(browse, browseColumns, columna, objetoRegistro);
      } catch (Exception var5) {
         var5.printStackTrace();
         return "";
      }
   }

   public static Object obtenerValor(Browse browse, BrowseColumn[] browseColumns, BrowseColumn columna, Object objetoRegistro) {
      try {
         Object columnValue = getColumnObject(columna, objetoRegistro, "sisem", browseColumns, browse.getBrowseId(), "");
         String columnResult = "";
         if (!Util.isEmpty(columna.getLink()) && !columna.isTextInput()) {
            columnResult = getColumnLinkObject(columna, objetoRegistro, browseColumns);
            columnResult = columnResult.replace("javascript: abrirPagina(", "");
            columnResult = columnResult.replace("'", "");
            columnResult = columnResult.replace(");", "");
         } else if (columna.getType().equals("BIGDECIMAL") && !columna.getColumnName().contains("version")) {
            if (columnValue != null && columnValue.toString().contains(".")) {
               BigDecimal valor = (BigDecimal)columnValue;
               String qty_decimals = PropiedadGeneral.getInstance(OrganizacionGeneral.getOrgcodigo()).getProperty("MISC", "QTYDECIMALS", "2");
               columnValue = valor.setScale(Integer.valueOf(qty_decimals), 4);
            }
         } else if (columna.getType().equals("TIME") && !columna.getColumnName().contains("version") && columnValue != null) {
            String[] arrHora = columnValue.toString().split(" ");
            columnValue = Util.getInst().fromStrdateToString(arrHora.length>1 ?  arrHora[1] : arrHora[0], FechaFormato.HHmm);
         }

         columnValue = columnResult.equals("") ? columnValue : columnResult;
         return columnValue;
      } catch (Exception var8) {
         var8.printStackTrace();
         return "";
      }
   }
}
