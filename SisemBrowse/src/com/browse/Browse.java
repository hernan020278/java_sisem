package com.browse;

import com.comun.utilidad.Util;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Browse {
   private BrowseObject browseObject;
   private BrowseColumn inputColumn;
   private Map mapPaginas = new HashMap();
   private List browseList = new ArrayList();
   private List costRangeList = new ArrayList();
   private List currentFilters = new ArrayList();
   private List groupFilterList = new ArrayList();
   private Map inputValues = new HashMap();
   private Map browseListFromCheck = new HashMap();
   private String browseTabla;
   private String browseId;
   private String currentSortColumn;
   private String currentSortType;
   private int pageSize;
   private int rowCount;
   private int rowStart;
   private int rowEnd;
   private int pageCount;
   private int currentPage;
   private long lastAccessed = (new Date()).getTime();

   public int getRowEnd() {
      return this.rowEnd;
   }

   public void setRowEnd(int rowEnd) {
      this.rowEnd = rowEnd;
   }

   public int getRowStart() {
      if (this.rowCount == 0) {
         this.rowStart = 0;
      }

      return this.rowStart;
   }

   public void setRowStart(int rowStart) {
      this.rowStart = rowStart;
   }

   public String getCurrentSortColumn() {
      return this.currentSortColumn;
   }

   public void setCurrentSortColumn(String currentSortColumn) {
      this.currentSortColumn = currentSortColumn;
   }

   public String getCurrentSortType() {
      return this.currentSortType;
   }

   public void setCurrentSortType(String currentSortType) {
      this.currentSortType = currentSortType;
   }

   public String getBrowseTabla() {
      return this.browseTabla;
   }

   public void setBrowseTabla(String browseTabla) {
      this.browseTabla = browseTabla;
   }

   public void setMapPaginas(Map mapPaginas) {
      this.mapPaginas = mapPaginas;
   }

   public List getCurrentFilters() {
      return this.currentFilters;
   }

   public void setCurrentFilters(List value) {
      this.currentFilters = value;
   }

   public void addCurrentFilters(BrowseFilter value) {
      this.currentFilters.add(value);
   }

   public int getPageSize() {
      return this.pageSize;
   }

   public void setPageSize(int x) {
      this.pageSize = x;
   }

   public int getRowCount() {
      return this.rowCount;
   }

   public void setRowCount(int x) {
      this.rowCount = x;
   }

   public int getPageCount() {
      return this.pageCount;
   }

   public void setPageCount(int x) {
      this.pageCount = x;
   }

   public int getCurrentPage() {
      return this.currentPage;
   }

   public void setCurrentPage(int currentPage) {
      this.currentPage = currentPage;
   }

   public List getBrowseList() {
      return this.browseList;
   }

   public void setBrowseList(List browseList) {
      this.browseList = browseList;
   }

   public Map getBrowseListFromCheck() {
      return this.browseListFromCheck;
   }

   public void setBrowseListFromCheck(Map browseListFromCheck) {
      this.browseListFromCheck = browseListFromCheck;
   }

   public void setBrowseListFromCheck(Map keyMap, Object value) {
      if (value == null) {
         this.browseListFromCheck.remove(keyMap);
      } else {
         this.browseListFromCheck.put(keyMap, value);
      }

   }

   public Object getBrowseListFromCheck(Map keyMap) {
      Object value = this.browseListFromCheck.get(keyMap);
      return value;
   }

   public List getCostRangeList() {
      return this.costRangeList;
   }

   public void setCostRangeList(List costRangeList) {
      this.costRangeList = costRangeList;
   }

   public List getGroupFilterList() {
      return this.groupFilterList;
   }

   public void setGroupFilterList(List groupFilterList) {
      this.groupFilterList = groupFilterList;
   }

   public BrowseObject getBrowseObject() {
      return this.browseObject;
   }

   public void setBrowseObject(BrowseObject browseObject) {
      this.browseObject = browseObject;
   }

   public String getBrowseId() {
      return this.browseId;
   }

   public void setBrowseId(String browseId) {
      this.browseId = browseId;
   }

   public List getPageResults() {
      return this.browseList;
   }

   public List getPageResultsFromAllRegisters() {
      int startRow = this.currentPage * this.pageSize - this.pageSize;
      int endRow = this.currentPage * this.pageSize;
      if (endRow > this.getRowCount()) {
         endRow = this.getRowCount();
      }

      this.setRowStart(startRow + 1);
      this.setRowEnd(endRow);
      return this.browseList.subList(startRow, endRow);
   }

   public Map getInputValues() {
      return this.inputValues;
   }

   public void setInputValues(Map inputValues) {
      this.inputValues = inputValues;
   }

   public void setInputValue(Map keyMap, String value) {
      if (Util.isEmpty(value)) {
         this.inputValues.remove(keyMap);
      } else {
         this.inputValues.put(keyMap, value);
      }

   }

   public String getInputValue(Map keyMap) {
      String value = (String)this.inputValues.get(keyMap);
      return Util.ckNull(value);
   }

   public BrowseColumn getInputColumn() {
      return this.inputColumn;
   }

   public void setInputColumn(BrowseColumn inputColumn) {
      this.inputColumn = inputColumn;
   }

   public void setLastAccessed(long lastAccessed) {
      this.lastAccessed = lastAccessed;
   }

   public long getLastAccessed() {
      return this.lastAccessed;
   }

   public Map getInputValueYFromKey() {
      Map inputVal = this.inputValues;
      Map inputValY = new HashMap();
      Iterator iterator = this.inputValues.keySet().iterator();

      while(iterator.hasNext()) {
         Map keyMap = (Map)iterator.next();
         Iterator keyIterator = keyMap.keySet().iterator();
         String inputValue = (String)this.inputValues.get(keyMap);
         if (inputValue.equals("Y")) {
            inputValY.put(keyMap, "Y");
         }
      }

      return inputValY;
   }

   public Map getMapPaginas() {
      return this.mapPaginas;
   }

   public Map getMapPaginas(List listResult) {
      if (this.mapPaginas == null) {
         this.mapPaginas = new HashMap();
         int iteRow = 0;
         int rowsTable = 0;

         for(int itePag = 0; itePag < this.pageCount; ++itePag) {
            List listRows = new ArrayList();
            rowsTable = listResult.size() <= this.pageSize ? listResult.size() : this.pageSize;

            for(int ite = 0; ite < rowsTable; ++ite) {
               if (iteRow < listResult.size()) {
                  listRows.add(listResult.get(iteRow));
                  ++iteRow;
               }
            }

            this.mapPaginas.put(itePag, listRows);
         }
      }

      return this.mapPaginas;
   }
}
