package com.browse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BrowseObject {
   private String browseTabla;
   private BrowseColumn[] browseColumns;
   private List browseFilters = new ArrayList();
   private ArrayList mappingReturn = null;
   private Map columnTypes = new HashMap();
   private Map columnLabels = new HashMap();
   private String botonAgregar = "";
   private String treeRelation = "";
   private String sqlSelect = "";
   private String sqlFrom;
   private String sqlWhere;
   private String sqlOrderBy;
   private String sqlGroupBy;
   private String title;
   private String foreignDatabase;
   private String noAttributeErrorMsg;
   private int pageSize;
   private int maxRows;
   private int totalRecord;
   private int rowCount;
   private int pageCount;
   private int currentPage;
   private boolean detailIncluded = false;
   private boolean detailVisible = false;
   private boolean orderByDefault = false;
   private boolean attributeSet = true;
   private boolean requireAttributeValues = true;
   private String queryFilter = "";
   private boolean topFilter = true;
   private boolean pageSizeFilter = true;

   public String getBrowseTabla() {
      return this.browseTabla;
   }

   public void setBrowseTabla(String browseTabla) {
      this.browseTabla = browseTabla;
   }

   public boolean isRequireAttributeValues() {
      return this.requireAttributeValues;
   }

   public BrowseColumn[] getBrowseColumns() {
      return this.browseColumns;
   }

   public void setBrowseColumns(BrowseColumn[] x) {
      this.browseColumns = x;
   }

   public List getBrowseFilters() {
      return this.browseFilters;
   }

   public void setBrowseFilters(List value) {
      this.browseFilters = value;
   }

   public Map getColumnTypes() {
      return this.columnTypes;
   }

   public void setColumnTypes(Map value) {
      this.columnTypes = value;
   }

   public String[] getColumnNames() {
      String[] columnNames = new String[this.browseColumns.length];

      for(int iteColNam = 0; iteColNam < this.browseColumns.length; ++iteColNam) {
         columnNames[iteColNam] = this.browseColumns[iteColNam].getColumnName();
      }

      return columnNames;
   }

   public Map getColumnLabels() {
      return this.columnLabels;
   }

   public void setColumnLabels(Map value) {
      this.columnLabels = value;
   }

   public void addBrowseFilter(BrowseFilter value) {
      this.browseFilters.add(value);
   }

   public String getSqlFrom() {
      return this.sqlFrom;
   }

   public void setSqlFrom(String x) {
      this.sqlFrom = x;
   }

   public String getSqlWhere() {
      return this.sqlWhere;
   }

   public void setSqlWhere(String x) {
      this.sqlWhere = x;
   }

   public String getSqlOrderBy() {
      return this.sqlOrderBy;
   }

   public void setSqlOrderBy(String x) {
      this.sqlOrderBy = x;
   }

   public String getTitle() {
      return this.title;
   }

   public void setTitle(String x) {
      this.title = x;
   }

   public String getForeignDatabase() {
      return this.foreignDatabase;
   }

   public void setForeignDatabase(String foreignDatabase) {
      this.foreignDatabase = foreignDatabase;
   }

   public String getNoAttributeErrorMsg() {
      return this.noAttributeErrorMsg;
   }

   public void setNoAttributeErrorMsg(String noAttributeErrorMsg) {
      this.noAttributeErrorMsg = noAttributeErrorMsg;
   }

   public int getPageSize() {
      return this.pageSize;
   }

   public void setPageSize(int x) {
      this.pageSize = x;
   }

   public int getTotalRecord() {
      return this.totalRecord;
   }

   public void setTotalRecord(int totalRecord) {
      this.totalRecord = totalRecord;
   }

   public int getMaxRows() {
      return this.maxRows;
   }

   public void setMaxRows(int x) {
      this.maxRows = x;
   }

   public int getRowCount() {
      return this.rowCount;
   }

   public void setRowCount(int x) {
      this.rowCount = x;
   }

   public int getPageCount() {
      return this.pageCount;
   }

   public void setPageCount(int x) {
      this.pageCount = x;
   }

   public boolean isDetailIncluded() {
      return this.detailIncluded;
   }

   public void setDetailIncluded(boolean x) {
      this.detailIncluded = x;
   }

   public boolean isDetailVisible() {
      return this.detailVisible;
   }

   public void setDetailVisible(boolean x) {
      this.detailVisible = x;
   }

   public boolean isAttributeSet() {
      return this.attributeSet;
   }

   public void setAttributeSet(boolean attributeSet) {
      this.attributeSet = attributeSet;
   }

   public boolean requireAttributeValues() {
      return this.requireAttributeValues;
   }

   public void setRequireAttributeValues(boolean requireAttributeValues) {
      this.requireAttributeValues = requireAttributeValues;
   }

   public String getTreeRelation() {
      return this.treeRelation;
   }

   public void setTreeRelation(String treeRelation) {
      this.treeRelation = treeRelation;
   }

   public String getBotonAgregar() {
      return this.botonAgregar;
   }

   public void setBotonAgregar(String botonAgregar) {
      this.botonAgregar = botonAgregar;
   }

   public String getSqlSelect() {
      return this.sqlSelect;
   }

   public void setSqlSelect(String sqlSelect) {
      this.sqlSelect = sqlSelect;
   }

   public boolean isOrderByDefault() {
      return this.orderByDefault;
   }

   public void setOrderByDefault(boolean orderByDefault) {
      this.orderByDefault = orderByDefault;
   }

   public String getSqlGroupBy() {
      return this.sqlGroupBy;
   }

   public void setSqlGroupBy(String sqlGroupBy) {
      this.sqlGroupBy = sqlGroupBy;
   }

   public ArrayList getMappingReturn() {
      return this.mappingReturn;
   }

   public void setMappingReturn(ArrayList mappingReturn) {
      this.mappingReturn = mappingReturn;
   }

   public String getQueryFilter() {
      return this.queryFilter;
   }

   public void setQueryFilter(String queryFilter) {
      this.queryFilter = queryFilter;
   }

   public int getCurrentPage() {
      return this.currentPage;
   }

   public void setCurrentPage(int currentPage) {
      this.currentPage = currentPage;
   }

   public boolean isTopFilter() {
      return this.topFilter;
   }

   public void setTopFilter(boolean topFilter) {
      this.topFilter = topFilter;
   }

   public boolean isPageSizeFilter() {
      return this.pageSizeFilter;
   }

   public void setPageSizeFilter(boolean pageSizeFilter) {
      this.pageSizeFilter = pageSizeFilter;
   }
}
