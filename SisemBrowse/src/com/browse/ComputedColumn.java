package com.browse;

import com.comun.utilidad.Util;

public class ComputedColumn {
   private String columnType = "";
   private Object value;

   public String getColumnType() {
      return Util.ckNull(this.columnType);
   }

   public void setColumnType(String columnType) {
      this.columnType = columnType;
   }

   public Object getValue() {
      return this.value;
   }

   public void setValue(Object value) {
      this.value = value;
   }

   public String toString() {
      String col = "Computed Column Type" + this.getColumnType() + ", value: " + this.getValue();
      return col;
   }
}
