package com.browse;

import com.comun.utilidad.Util;
import java.util.ArrayList;
import java.util.List;

public class BrowseColumn {
   private String columnName = "";
   private String className = "";
   private String methodName = "";
   private String label = "";
   private String treeRelationColumn = "";
   private String treeDescription = "";
   private String treeRelation = "";
   private String link = "";
   private String linkImage = "";
   private String sort = "";
   private String type = "";
   private String allowFilter = "";
   private int size = 0;
   private int inputSize = 0;
   private int trim = 0;
   private Object value;
   private boolean hidden = false;
   private boolean hiddenInput = false;
   private boolean detail = false;
   private int index = -1;
   private String alignment = "left";
   private int iWidth = 10;
   private ArrayList linkedColumns = null;
   private String computeType = "";
   private List argumentColumns = new ArrayList();
   private List calculateColumns = new ArrayList();
   private String alias = "";
   private boolean storeRequestValue = false;
   private boolean checked = false;
   private boolean filterDefault = false;
   private boolean selectInput = false;
   private boolean textInput = false;
   private boolean buttonInput = false;
   private boolean checkbox = false;
   private String onClick = "";
   private String oid;
   private String language = "en";
   private boolean sortable = true;
   private String mouseover = "";
   private String mouseout = "";
   private String scriptCode = "";
   private boolean editable = false;
   private List scriptCodeColumns = new ArrayList();
   private String subselect;

   public BrowseColumn() {
   }

   public BrowseColumn(String inOid, String inLanguage) {
      this.oid = inOid;
      this.language = inLanguage;
   }

   public int getIndex() {
      return this.index;
   }

   public String getTreeRelationColumn() {
      return this.treeRelationColumn;
   }

   public void setTreeRelationColumn(String treeRelationColumn) {
      this.treeRelationColumn = treeRelationColumn;
   }

   public String getTreeDescription() {
      return this.treeDescription;
   }

   public void setTreeDescription(String treeDescription) {
      this.treeDescription = treeDescription;
   }

   public String getTreeRelation() {
      return this.treeRelation;
   }

   public void setTreeRelation(String treeRelation) {
      this.treeRelation = treeRelation;
   }

   public String getSubselect() {
      return this.subselect;
   }

   public void setSubselect(String subselect) {
      this.subselect = subselect;
   }

   public void setIndex(int index) {
      this.index = index;
   }

   public String getColumnName() {
      return this.columnName;
   }

   public void setColumnName(String value) {
      this.columnName = value;
   }

   public String getClassName() {
      return this.className;
   }

   public void setClassName(String value) {
      this.className = value;
   }

   public String getMethodName() {
      return this.methodName;
   }

   public void setMethodName(String value) {
      this.methodName = value;
   }

   public String getOnClick() {
      return this.onClick;
   }

   public void setOnClick(String value) {
      this.onClick = value;
   }

   public String getLabel() {
      return this.label;
   }

   public void setLabel(String value) {
      this.label = value;
   }

   public String getLink() {
      return this.link;
   }

   public void setLink(String value) {
      this.link = value;
   }

   public String getSort() {
      return this.sort;
   }

   public void setSort(String value) {
      this.sort = value;
   }

   public String getType() {
      return this.type;
   }

   public void setType(String value) {
      this.type = value;
   }

   public String getAllowFilter() {
      return this.allowFilter;
   }

   public void setAllowFilter(String value) {
      this.allowFilter = value;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int value) {
      this.size = value;
   }

   public int getInputSize() {
      return this.inputSize;
   }

   public void setInputSize(int value) {
      this.inputSize = value;
   }

   public int getTrim() {
      return this.trim;
   }

   public void setTrim(int value) {
      this.trim = value;
   }

   public Object getValue() {
      return this.value;
   }

   public void setValue(Object value) {
      this.value = value;
   }

   public boolean isHidden() {
      return this.hidden;
   }

   public void setHidden(boolean value) {
      this.hidden = value;
   }

   public boolean isHiddenInput() {
      return this.hiddenInput;
   }

   public void setHiddenInput(boolean value) {
      this.hiddenInput = value;
   }

   public boolean isSelectInput() {
      return this.selectInput;
   }

   public void setSelectInput(boolean value) {
      this.selectInput = value;
   }

   public boolean isButtonInput() {
      return this.buttonInput;
   }

   public void setButtonInput(boolean buttonInput) {
      this.buttonInput = buttonInput;
   }

   public boolean isTextInput() {
      return this.textInput;
   }

   public void setTextInput(boolean value) {
      this.textInput = value;
   }

   public boolean isCheckbox() {
      return this.checkbox;
   }

   public void setCheckbox(boolean value) {
      this.checkbox = value;
   }

   public boolean isChecked() {
      return this.checked;
   }

   public void setChecked(boolean value) {
      this.checked = value;
   }

   public boolean isDetail() {
      return this.detail;
   }

   public void setDetail(boolean value) {
      this.detail = value;
   }

   public String toString() {
      String col = "Column Name: " + this.getColumnName() + ",type: " + this.getType();
      return col;
   }

   public String getAlignment() {
      return this.alignment;
   }

   public void setAlignment(String alignment) {
      this.alignment = alignment;
   }

   public int getIWidth() {
      return this.getSize() > 0 ? this.getSize() : this.iWidth;
   }

   public void setIWidth(int iwidth) {
      this.iWidth = iwidth;
   }

   public ArrayList getLinkedColumns() {
      return this.linkedColumns;
   }

   public void setLinkedColumns(ArrayList linkedColumns) {
      this.linkedColumns = linkedColumns;
   }

   public List getArgumentColumns() {
      return this.argumentColumns;
   }

   public void setArgumentColumns(List argumentColumns) {
      this.argumentColumns = argumentColumns;
   }

   public List getCalculateColumns() {
      return this.calculateColumns;
   }

   public void setCalculateColumns(List calculateColumns) {
      this.calculateColumns = calculateColumns;
   }

   public String getComputeType() {
      return this.computeType;
   }

   public void setComputeType(String computeType) {
      this.computeType = computeType;
   }

   public String getAlias() {
      if (Util.isEmpty(this.alias)) {
         this.alias = this.getColumnName();
      }

      return this.alias;
   }

   public void setAlias(String alias) {
      this.alias = alias;
   }

   public boolean storeRequestValue() {
      return this.storeRequestValue;
   }

   public void setStoreRequestValue(boolean storeRequestValue) {
      this.storeRequestValue = storeRequestValue;
   }

   public boolean isFilterDefault() {
      return this.filterDefault;
   }

   public void setFilterDefault(boolean filterDefault) {
      this.filterDefault = filterDefault;
   }

   public String getOid() {
      return this.oid;
   }

   public void setOid(String oid) {
      this.oid = oid;
   }

   public String getLanguage() {
      return this.language;
   }

   public void setLanguage(String language) {
      this.language = language;
   }

   public String getLinkImage() {
      return this.linkImage;
   }

   public void setLinkImage(String linkImage) {
      this.linkImage = linkImage;
   }

   public boolean isEditable() {
      return this.editable;
   }

   public void setEditable(boolean editable) {
      this.editable = editable;
   }

   public boolean isSortable() {
      return this.sortable;
   }

   public void setSortable(boolean sortable) {
      this.sortable = sortable;
   }

   public String getMouseover() {
      return this.mouseover;
   }

   public void setMouseover(String mouseover) {
      this.mouseover = mouseover;
   }

   public String getMouseout() {
      return this.mouseout;
   }

   public void setMouseout(String mouseout) {
      this.mouseout = mouseout;
   }

   public String getScriptCode() {
      return this.scriptCode;
   }

   public void setScriptCode(String scriptCode) {
      this.scriptCode = scriptCode;
   }

   public List getScriptCodeColumns() {
      return this.scriptCodeColumns;
   }

   public void setScriptCodeColumns(List scriptCodeColumns) {
      this.scriptCodeColumns = scriptCodeColumns;
   }
}
