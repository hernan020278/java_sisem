package com.browse;

import com.comun.utilidad.Log;
import com.comun.utilidad.Util;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BrowseManager {
   private static BrowseManager INSTANCE = new BrowseManager();
   private HashMap browses = new HashMap();
   private HashMap organizations = new HashMap();
   private long lastCleanup = 0L;

   private BrowseManager() {
   }

   public static BrowseManager getInstance() {
      if (INSTANCE == null) {
         INSTANCE = new BrowseManager();
      }

      return INSTANCE;
   }

   public Browse getBrowse(String browseId) {
      Browse browse = (Browse)this.browses.get(browseId);
      browse.setLastAccessed((new Date()).getTime());
      return browse;
   }

   public void setBrowseInCache(Browse browse, String organizationId, String sessionId) throws Exception {
      try {
         if (browse == null) {
            throw new Exception("Browse es nulo");
         } else {
            String browseId = browse.getBrowseId();
            boolean newBrowse = true;
            if (!Util.isEmpty(browseId)) {
               browse.setLastAccessed((new Date()).getTime());
               if (this.browses.containsKey(browseId)) {
                  newBrowse = false;
               }

               this.browses.put(browseId, browse);
               if (newBrowse) {
                  Map sessionBrowses = (HashMap)this.organizations.get(organizationId);
                  if (sessionBrowses == null) {
                     sessionBrowses = new HashMap();
                  }

                  List sessionBrowseList = (List)sessionBrowses.get(sessionId);
                  if (sessionBrowseList == null) {
                     sessionBrowseList = new ArrayList();
                  }

                  ((List)sessionBrowseList).add(browseId);
                  sessionBrowses.put(sessionId, sessionBrowseList);
                  this.organizations.put(organizationId, sessionBrowses);
               }
            }

            this.checkBrowseCleanup();
         }
      } catch (Exception var8) {
         throw var8;
      }
   }

   public Set getAvailableBrowseIds() throws Exception {
      Set result = null;

      try {
         result = this.browses.keySet();
         return result;
      } catch (Exception var3) {
         throw var3;
      }
   }

   public Map getOrganizationBrowses() throws Exception {
      new HashMap();

      try {
         Map result = this.organizations;
         return result;
      } catch (Exception var3) {
         throw var3;
      }
   }

   public void destroyBrowse(String browseId) throws Exception {
      try {
         if (!Util.isEmpty(browseId)) {
            this.browses.remove(browseId);
         }

      } catch (Exception var3) {
         throw var3;
      }
   }

   public void destroyBrowseBySession(String organizationId, String sessionId) throws Exception {
      try {
         if (!Util.isEmpty(organizationId) && !Util.isEmpty(sessionId)) {
            Map sessionBrowses = (HashMap)this.organizations.get(organizationId);
            if (sessionBrowses != null) {
               List sessionBrowseList = (List)sessionBrowses.get(sessionId);
               if (sessionBrowseList != null) {
                  for(int i = 0; i < sessionBrowseList.size(); ++i) {
                     String browseId = (String)sessionBrowseList.get(i);
                     getInstance().destroyBrowse(browseId);
                  }
               }

               sessionBrowses.remove(sessionId);
               this.organizations.put(organizationId, sessionBrowses);
            }
         }

      } catch (Exception var7) {
         throw var7;
      }
   }

   public void cleanupBrowsesBySession(final String organizationId, final String sessionId) {
      class MyRunner implements Runnable {
         public void run() {
            try {
               BrowseManager.this.destroyBrowseBySession(organizationId, sessionId);
            } catch (Exception var2) {
               Log.error(this, var2.getMessage());
            }

         }
      }

      (new Thread(new MyRunner())).start();
   }

   private void checkBrowseCleanup() {
      long currentTime = (new Date()).getTime();
      if (this.lastCleanup == 0L) {
         this.lastCleanup = currentTime;
      }

      if (this.lastCleanup - currentTime > 3600000L) {
         class MyRunner implements Runnable {
            public void run() {
               try {
                  BrowseManager.this.browseCleanup();
                  BrowseManager.this.lastCleanup = (new Date()).getTime();
               } catch (Exception var2) {
                  Log.error(this, var2.getMessage());
               }

            }
         }

         (new Thread(new MyRunner())).start();
      }

   }

   private void browseCleanup() {
      try {
         Set browseIds = getInstance().getAvailableBrowseIds();
         Iterator iterator = browseIds.iterator();
         long currentTime = (new Date()).getTime();
         ArrayList listToDestroy = new ArrayList();

         while(iterator.hasNext()) {
            String browseId = (String)iterator.next();
            Browse browse = (Browse)this.browses.get(browseId);
            long timeDif = 0L;
            if (browse.getLastAccessed() > 0L) {
               timeDif = currentTime - browse.getLastAccessed();
            }

            if (timeDif > 3600000L) {
               listToDestroy.add(browseId);
            }
         }

         for(int i = 0; i < listToDestroy.size(); ++i) {
            String browseId = (String)listToDestroy.get(i);
            getInstance().destroyBrowse(browseId);
         }
      } catch (Exception var10) {
         Log.error(this, var10.getMessage());
      }

   }

   public String toString() {
      StringBuffer sb = new StringBuffer();
      sb.append("[ClassName=com.browse.BrowseManager]");
      return sb.toString();
   }
}
