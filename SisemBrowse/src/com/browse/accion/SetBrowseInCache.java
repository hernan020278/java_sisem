package com.browse.accion;

import com.browse.Browse;
import com.browse.BrowseManager;
import com.comun.motor.Accion;
import java.util.Map;

public class SetBrowseInCache extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {
      try {
         Browse browse = (Browse)peticion.get("browse");
         String organizationId = (String)peticion.get("organizationId");
         String sessionId = (String)peticion.get("sessionId");
         if (browse != null) {
            BrowseManager.getInstance().setBrowseInCache(browse, organizationId, sessionId);
         }

         this.estado = 80;
         return null;
      } catch (Exception var8) {
         this.estado = 0;
         throw var8;
      } finally {
         ;
      }
   }
}
