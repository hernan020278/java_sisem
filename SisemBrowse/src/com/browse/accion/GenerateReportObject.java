package com.browse.accion;

import com.comun.utilidad.AplicacionGeneral;

public class GenerateReportObject extends GenerateBrowseObject {
   protected String getBrowseXmlPath() {
      return AplicacionGeneral.getInstance().obtenerRutaReporteXml();
   }
}
