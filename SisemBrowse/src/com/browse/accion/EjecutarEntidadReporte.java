package com.browse.accion;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.utilidad.Log;
import com.reporte.jasperreport.JasperReportsHelper;
import java.util.HashMap;
import java.util.Map;

public class EjecutarEntidadReporte extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {
      Object ret = "e";

      try {
         String organizacionIde = OrganizacionGeneral.getOrgcodigo();
         String userId = (String)peticion.get("userId");
         String path = DiccionarioGeneral.getInstance("host", organizacionIde).getPropiedad("ruta-reporte-jasper", "");
         DBConeccion dbc = (DBConeccion)peticion.get("dbConeccion");
         Map parameters = new HashMap();
         PropiedadGeneral propiedadGeneral = PropiedadGeneral.getInstance(organizacionIde);
         String removePercent = propiedadGeneral.getProperty("REPORT OPTIONS", "REMOVEPERCENTFROMCRITERIA", "N");
         parameters.put("webreport", (String)peticion.get("webreport"));
         parameters.put("isReport", "N");
         parameters.put("reportPath", path);
         String reportName = (String)peticion.get("reportName");
         parameters.put("filePath", path + reportName);
         parameters.put("organizacionIde", organizacionIde);
         Map reportParameters = new HashMap();
         reportParameters.put("dbConeccion", dbc);
         reportParameters.put("organizacionIde", organizacionIde);
         reportParameters.put("oid", organizacionIde);
         reportParameters.put("userId", userId);
         reportParameters.put("reportCriteria", peticion.get("reportCriteria"));
         reportParameters.put("datasource", peticion.get("datasource"));
         reportParameters.put("subAreaDS", peticion.get("subAreaDS"));
         reportParameters.put("usuarioDS", peticion.get("usuarioDS"));
         reportParameters.put("listaPapeletaDS", peticion.get("listaPapeletaDS"));
         reportParameters.put("listaAsistenciaDS", peticion.get("listaAsistenciaDS"));
         parameters.put("reportParameters", reportParameters);
         parameters.put("dbConeccion", dbc);
         parameters.put("reportName", (String)peticion.get("reportName"));
         String noParams = (String)peticion.get("noParams");
         if (noParams == null) {
            noParams = "N";
         }

         if (noParams.equalsIgnoreCase("Y")) {
            parameters.put("isDatasource", new Boolean(false));
         } else {
            parameters.put("isDatasource", new Boolean(true));
         }

         String format = (String)peticion.get("format");
         if (format == null) {
            if (noParams.equalsIgnoreCase("Y")) {
               format = "html";
            } else {
               format = "pdf";
            }
         }

         parameters.put("format", format);
         long start = System.currentTimeMillis();
         ret = JasperReportsHelper.runReport(parameters);
         long end = System.currentTimeMillis();
         Log.debug(this, "It took " + (end - start) / 1000L + " seconds to execute the report" + (String)peticion.get("reportName"));
         this.setEstado(80);
      } catch (Exception var18) {
         var18.printStackTrace();
         this.setEstado(0);
      }

      return ret;
   }
}
