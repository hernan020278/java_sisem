package com.browse.accion;

import com.browse.Browse;
import com.browse.BrowseFilter;
import com.browse.BrowseObject;
import com.comun.motor.Accion;
import com.comun.utilidad.KeyGenerator;
import java.util.List;
import java.util.Map;

public class BrowseSetup extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {

      Object var2 = null;

      try {
         String organizationId = (String)peticion.get("organizationId");
         String userId = (String)peticion.get("userId");
         BrowseObject browseObject = (BrowseObject)peticion.get("browseObject");
         List browseList = (List)peticion.get("browseList");
         List costRangeList = (List)peticion.get("costRangeList");
         List groupFilterList = (List)peticion.get("groupFilterList");
         List filterList = browseObject.getBrowseFilters();
         String sortedColumn = "";
         String sortedType = "";
         KeyGenerator ukg = KeyGenerator.getInst();
         int rowCount = 0;
         int pageCount = 0;
         int pageSize = browseObject.getPageSize();
         if (pageSize == 0) {
            pageSize = 10;
         }

         if (browseList != null) {
            rowCount = browseObject.getTotalRecord();
         }

         if (pageSize > 0) {
            pageCount = (rowCount - 1) / pageSize + 1;
         }

         if (filterList != null) {
            for(int ix = 0; ix < filterList.size(); ++ix) {
               BrowseFilter filter = (BrowseFilter)filterList.get(ix);
               String sort = filter.getSort();
               if (sort != null && !sort.equalsIgnoreCase("N")) {
                  sortedColumn = filter.getColumnName().replace('.', '_');
                  sortedType = sort;
               }
            }
         }

         Browse browse = new Browse();
         browse.setBrowseId(ukg.getUniqueKey().toString());
         browse.setBrowseTabla(browseObject.getBrowseTabla());
         browse.setBrowseList(browseList);
         browse.setBrowseObject(browseObject);
         browse.setCostRangeList(costRangeList);
         browse.setCurrentFilters(browseObject.getBrowseFilters());
         browse.setCurrentPage(browseObject.getCurrentPage());
         browse.setCurrentSortColumn(sortedColumn);
         browse.setCurrentSortType(sortedType);
         browse.setGroupFilterList(groupFilterList);
         browse.setPageCount(pageCount);
         browse.setPageSize(pageSize);
         browse.setRowCount(rowCount);
         browseObject.setPageCount(pageCount);
         browseObject.setPageSize(pageSize);
         browseObject.setRowCount(rowCount);
         peticion.put("browseObject", browseObject);
         return browse;
      } catch (Exception var22) {
         this.estado = 0;
         throw var22;
      } finally {
         ;
      }
   }
}
