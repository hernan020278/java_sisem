package com.browse.accion;

import com.browse.BrowseObject;
import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.utilidad.Log;
import com.reporte.jasperreport.JasperReportsHelper;
import com.reporte.jasperreport.ReportUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EjecutarReporte extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {
      Object ret = "e";

      try {
         String organizacionIde = (String)peticion.get("organizacionIde");
         String userId = (String)peticion.get("userId");
         String path = DiccionarioGeneral.getInstance("host", organizacionIde).getPropiedad("ruta-reporte-jasper", "");
         DBConeccion dbc = (DBConeccion)peticion.get("dbConeccion");
         BrowseObject reportObject = (BrowseObject)peticion.get("browseObject");
         Map parameters = new HashMap();
         PropiedadGeneral propiedadGeneral = PropiedadGeneral.getInstance(organizacionIde);
         String removePercent = propiedadGeneral.getProperty("REPORT OPTIONS", "REMOVEPERCENTFROMCRITERIA", "N");
         parameters.put("webreport", (String)peticion.get("webreport"));
         parameters.put("isReport", "Y");
         parameters.put("reportPath", path);
         String reportName = (String)peticion.get("reportName");
         parameters.put("filePath", path + reportName);
         parameters.put("organizacionIde", organizacionIde);
         parameters.put("reportTitle", reportObject.getTitle());
         Map reportParameters = new HashMap();
         reportParameters.put("dbConeccion", dbc);
         reportParameters.put("organizacionIde", organizacionIde);
         reportParameters.put("oid", organizacionIde);
         reportParameters.put("userId", userId);
         reportParameters.put("reportTitle", reportObject.getTitle());
         reportParameters.put("reportCriteria", peticion.get("reportCriteria"));
         reportParameters.put("consultaListaComponentes", peticion.get("consultaListaComponentes"));
         reportParameters.put("entidadNombreListaComponentes", peticion.get("entidadNombreListaComponentes"));
         if (reportName.equals("rep-prdope-por-dia")) {
            peticion.put("datasource", ReportUtils.quitarRepetidos((List)peticion.get("datasource")));
            reportParameters.put("listaProduccionEncDS", ReportUtils.obtenerListaProduccionDS(dbc, reportObject.getQueryFilter()));
            reportParameters.put("listaProduccionTotDS", ReportUtils.obtenerListaProduccionDS(dbc, reportObject.getQueryFilter()));
            reportParameters.put("queryFilter", reportObject.getQueryFilter());
         }

         parameters.put("reportParameters", reportParameters);
         parameters.put("dbConeccion", dbc);
         List data = (List)peticion.get("datasource");
         int dataSize = data.size();
         int restricted = 0;
         new ArrayList();
         int topSize = Integer.parseInt(PropiedadGeneral.getInstance(organizacionIde).getProperty("REPORTS OPTIONS", "TOPSIZE", "25"));
         if (reportName.toLowerCase().indexOf("top") > -1 && topSize > 0) {
            List subData;
            if (dataSize > topSize) {
               subData = data.subList(0, topSize);
               restricted = 10;
            } else {
               subData = data;
               restricted = dataSize;
            }

            parameters.put("datasource", subData);
         } else {
            Log.error(this, reportName + " Returned: " + data.size());
            peticion.put("tooMuchData", "N");
            parameters.put("datasource", peticion.get("datasource"));
            restricted = dataSize;
         }

         parameters.put("dataSize", new BigDecimal(dataSize));
         parameters.put("dataSizeRestricted", new BigDecimal(restricted));
         List dataList = (List)peticion.get("datasource");
         if (dataList.size() < 1) {
            parameters.put("reportName", "nodata");
            parameters.put("errorType", "nodata");
         } else {
            parameters.put("reportName", (String)peticion.get("reportName"));
         }

         parameters.put("reportObject", reportObject);
         String noParams = (String)peticion.get("noParams");
         if (noParams == null) {
            noParams = "N";
         }

         if (noParams.equalsIgnoreCase("Y")) {
            parameters.put("isDatasource", new Boolean(false));
         } else {
            parameters.put("isDatasource", new Boolean(true));
         }

         String format = (String)peticion.get("format");
         if (format == null) {
            if (noParams.equalsIgnoreCase("Y")) {
               format = "html";
            } else {
               format = "pdf";
            }
         }

         parameters.put("format", format);
         long start = System.currentTimeMillis();
         ret = JasperReportsHelper.runReport(parameters);
         long end = System.currentTimeMillis();
         Log.debug(this, "It took " + (end - start) / 1000L + " seconds to execute the report" + (String)peticion.get("reportName"));
         this.setEstado(80);
      } catch (Exception var25) {
         var25.printStackTrace();
         this.setEstado(0);
      }

      return ret;
   }
}
