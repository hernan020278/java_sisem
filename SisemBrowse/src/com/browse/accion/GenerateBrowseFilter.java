package com.browse.accion;

import com.browse.BrowseFilter;
import com.browse.BrowseObject;
import com.comun.motor.Accion;
import com.comun.utilidad.Util;
import java.util.Map;

public class GenerateBrowseFilter extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {
      try {
         String organizationId = (String)peticion.get("organizationId");
         String currentPage = (String)peticion.get("currentPage");
         BrowseObject browseObject = (BrowseObject)peticion.get("browseObject");
         Object objFiltroColumna = peticion.get("filtroColumna");
         Map columnTypes = browseObject.getColumnTypes();
         String fromPage = Util.ckNull((String)peticion.get("fromPage"));
         String strBrowseFilter;
         if (objFiltroColumna instanceof String[]) {
            String[] arrFiltroColumna = (String[])((String[])objFiltroColumna);
            String[] arrFiltroTipo = (String[])((String[])peticion.get("filtroTipo"));
            String[] arrFiltroOperador = (String[])((String[])peticion.get("filtroOperador"));
            String[] arrFiltroValor = (String[])((String[])peticion.get("filtroValor"));
            String[] arrFiltroLogico = (String[])((String[])peticion.get("filtroLogico"));
            String[] arrFiltroOrden = (String[])((String[])peticion.get("filtroOrden"));
            String[] arrFiltroOriginal = (String[])((String[])peticion.get("filtroOriginal"));
            strBrowseFilter = "AND";
            if (fromPage.indexOf("main_menu") > 0) {
               strBrowseFilter = "OR";
            } else {
               strBrowseFilter = "AND";
            }

            for(int i = 0; i < arrFiltroColumna.length; ++i) {
               String filtroTipo = arrFiltroTipo[i];
               String filtroColumna = arrFiltroColumna[i];
               String filtroOperador = arrFiltroOperador[i];
               String filtroValor = arrFiltroValor[i];
               String filtroLogico = arrFiltroLogico[i];
               String filtroOrden = arrFiltroOrden[i];
               String filtroOriginal = Util.ckNull(arrFiltroOriginal[i]);
               if (Util.isEmpty(filtroOriginal)) {
                  filtroOriginal = "N";
               }

               if (!Util.isEmpty(filtroColumna) && !Util.isEmpty(filtroOperador) && !Util.isEmpty(filtroValor)) {
                  BrowseFilter browseFilter = null;
                  filtroColumna = !filtroColumna.contains(".") ? filtroColumna.replaceFirst("_", ".") : filtroColumna;
                  filtroValor = filtroValor.replaceAll("'", "''");
                  if (!Util.isEmpty(filtroValor) || !Util.isEmpty(filtroOrden) && !filtroOrden.equalsIgnoreCase("N")) {
                     browseFilter = this.setupBrowseFilter(filtroTipo, filtroColumna, filtroOperador, filtroValor, filtroLogico, filtroOrden, filtroOriginal);
                     String var10000 = arrFiltroLogico[i];
                     browseObject.addBrowseFilter(browseFilter);
                  }
               }
            }
         } else {
            String filtroTipo = (String)peticion.get("filtroTipo");
            String filtroColumna = (String)objFiltroColumna;
            String filtroOperador = (String)peticion.get("filtroOperador");
            String filtroValor = (String)peticion.get("filtroValor");
            String filtroOrden = (String)peticion.get("filtroOrden");
            String filtroLogico = (String)peticion.get("filtroLogico");
            String filtroOriginal = (String)peticion.get("filtroOriginal");
            if (!Util.isEmpty(filtroColumna) && !Util.isEmpty(filtroOperador) && !Util.isEmpty(filtroValor)) {
               filtroColumna = !filtroColumna.contains(".") ? filtroColumna.replaceFirst("_", ".") : filtroColumna;
               filtroValor = filtroValor.replaceAll("'", "''");
               if (!Util.isEmpty(filtroValor) || !Util.isEmpty(filtroOrden) && !filtroOrden.equalsIgnoreCase("N")) {
                  BrowseFilter browseFilter = this.setupBrowseFilter(filtroTipo, filtroColumna, filtroOperador, filtroValor, filtroLogico, filtroOrden, filtroOriginal);
                  browseObject.addBrowseFilter(browseFilter);
               }
            }
         }

         browseObject.setCurrentPage(currentPage != null && !currentPage.equals("0") ? Integer.parseInt(currentPage) : 1);
         this.setEstado(80);
         return browseObject;
      } catch (Exception var26) {
         throw var26;
      }
   }

   private BrowseFilter setupBrowseFilter(String filtroTipo, String filtroColumna, String filtroOperador, String filtroValor, String filtroLogico, String filtroOrden, String filtroOriginal) {
      BrowseFilter browseFilter = new BrowseFilter();
      if (Util.isEmpty(filtroOperador)) {
         filtroOperador = "=";
      }

      if (Util.isEmpty(filtroLogico)) {
         filtroLogico = "AND";
      }

      if (Util.isEmpty(filtroOrden)) {
         filtroOrden = "N";
      }

      if (Util.isEmpty(filtroOriginal)) {
         filtroOriginal = "N";
      }

      browseFilter.setType(filtroTipo);
      browseFilter.setColumnName(filtroColumna);
      browseFilter.setOperator(filtroOperador);
      browseFilter.setValue(filtroValor.trim());
      browseFilter.setLogicalOperator(filtroLogico);
      browseFilter.setSort(filtroOrden);
      browseFilter.setOriginalFilter(filtroOriginal.equalsIgnoreCase("Y"));
      return browseFilter;
   }
}
