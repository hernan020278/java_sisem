package com.browse.accion;

import com.browse.Browse;
import com.browse.BrowseObject;
import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.motor.IAccionListener;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

public class GenerarBrowse extends Accion implements IAccionListener {
  public Mono<Map> ejecutarAccion(Map peticion) {
    Object var2 = null;

    try {
      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
      BrowseObject browseObject = null;
      Browse browse = null;
      browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
      peticion.put("browseObject", browseObject);
      browseObject = (BrowseObject) (new GenerateBrowseFilter()).ejecutarAccion(peticion);
      peticion.put("browseObject", browseObject);
      List browseList = (List) (new BrowseRetrieve()).ejecutarAccion(peticion);
      peticion.put("browseList", browseList);
      browse = (Browse) (new BrowseSetup()).ejecutarAccion(peticion);
      peticion.put("browse", browse);
      (new SetBrowseInCache()).ejecutarAccion(peticion);
      this.setEstado(80);

    } catch (Exception var7) {
      this.setEstado(0);
    }
    return Mono.just(peticion);
  }
}
