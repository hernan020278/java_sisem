package com.browse.accion;

import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingUtilities;

public class GenerateBrowseObjectTest {
   public static void main(String[] args) {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            try {
               Map peticion = new HashMap();
               peticion.put("browseTabla", "lista_caso");
               peticion.put("organizacionIde", "sisem");
               peticion.put("pageSize", "25");
               peticion.put("userId", "HERNAN");
               GenerateBrowseObject generateBrowse = new GenerateBrowseObject();
               generateBrowse.ejecutarAccion(peticion);
            } catch (Exception var3) {
               var3.printStackTrace();
            }

         }
      });
   }
}
