package com.browse.accion;

import com.browse.Browse;
import com.browse.BrowseObject;
import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import java.util.Map;

public class GenerarBrowseOpciones extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {
      Object var2 = null;

      try {
         DBConeccion dbc = (DBConeccion)peticion.get("dbConeccion");
         BrowseObject browseObject = null;
         Browse browse = null;
         browseObject = (BrowseObject)(new GenerateBrowseObject()).ejecutarAccion(peticion);
         peticion.put("browseObject", browseObject);
         browseObject = (BrowseObject)(new GenerateBrowseFilter()).ejecutarAccion(peticion);
         peticion.put("browseObject", browseObject);
         this.setEstado(80);
         return peticion;
      } catch (Exception var6) {
         this.setEstado(0);
         throw var6;
      }
   }
}
