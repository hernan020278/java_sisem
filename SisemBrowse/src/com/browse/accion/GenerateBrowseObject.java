package com.browse.accion;

import com.browse.BrowseColumn;
import com.browse.BrowseObject;
import com.browse.BrowseUtility;
import com.comun.motor.Accion;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;
import com.comun.utilidad.XmlFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jdom.Element;

public class GenerateBrowseObject extends Accion {
   protected String getBrowseXmlPath() {
      return AplicacionGeneral.getInstance().obtenerRutaBrowseXml();
   }

   public Object ejecutarAccion(Map peticion) throws Exception {
      try {
         String organizacionIde = (String)peticion.get("organizacionIde");
         String browseTabla = (String)peticion.get("browseTabla");
         String pageSize = (String)peticion.get("pageSize");
         String windowTime = Util.ckNull((String)peticion.get("windowTime"));
         BrowseObject browseObject = new BrowseObject();
         browseObject.setBrowseTabla(browseTabla);
         String language = (String)peticion.get("language");
         if (Util.isEmpty(language)) {
            language = "en";
         }

         try {
            String filename = this.getBrowseXmlPath() + browseTabla;
            System.out.println("Archivo XML: " + filename);
            XmlFile xmlFile = new XmlFile(filename + ".xml", organizacionIde);
            String[] xArray = new String[]{"columns"};
            List lisRootNiv1 = xmlFile.getElementList(xArray);
            xArray[0] = "select";
            String selNiv1 = xmlFile.getText(xArray);
            List lisLab = new ArrayList();
            int numCol = 0;
            Iterator iteNumCol = lisRootNiv1.iterator();

            label284:
            while(true) {
               while(iteNumCol.hasNext()) {
                  Element iteColNiv2 = (Element)iteNumCol.next();
                  String columnName = iteColNiv2.getName();
                  String columna = !columnName.contains(".") ? columnName.replaceFirst("_", ".") : columnName;
                  if (!selNiv1.toUpperCase().contains(columna.toUpperCase()) && !selNiv1.toUpperCase().contains(columnName.toUpperCase())) {
                     if (this.existeColumnAlias(iteColNiv2, selNiv1)) {
                        ++numCol;
                     }
                  } else {
                     ++numCol;
                  }
               }

               BrowseColumn[] browseColumns = new BrowseColumn[numCol];
               Map columnTypes = new HashMap();
               Map columnLabels = new HashMap();
               StringBuffer sql = new StringBuffer("Select ");
               int indCol = 0;
               int selectColumnIndex = 0;
               Iterator iteLisRootNiv1 = lisRootNiv1.iterator();

               label271:
               while(true) {
                  Element iteColNiv2;
                  String maxRows;
                  String className;
                  String methodName;
                  String columna;
                  do {
                     if (!iteLisRootNiv1.hasNext()) {
                        sql.deleteCharAt(sql.length() - 1);
                        sql.deleteCharAt(sql.length() - 1);
                        browseObject.setSqlSelect("select " + selNiv1);
                        browseObject.setBrowseColumns(browseColumns);
                        browseObject.setColumnTypes(columnTypes);
                        browseObject.setColumnLabels(columnLabels);
                        xArray[0] = "botonAgregar";
                        browseObject.setBotonAgregar(xmlFile.getText(xArray));
                        xArray[0] = "treeRelation";
                        browseObject.setTreeRelation(xmlFile.getText(xArray));
                        xArray[0] = "sqlfrom";
                        browseObject.setSqlFrom(xmlFile.getText(xArray));
                        xArray[0] = "sqlwhere";
                        browseObject.setSqlWhere(xmlFile.getText(xArray));
                        xArray[0] = "sqlorderby";

                        try {
                           iteColNiv2 = xmlFile.getRootChild("sqlorderby");
                           maxRows = iteColNiv2.getAttributeValue("default");
                           if (maxRows != null) {
                              if (maxRows.equalsIgnoreCase("Y")) {
                                 browseObject.setOrderByDefault(true);
                              } else {
                                 browseObject.setOrderByDefault(false);
                              }
                           } else {
                              browseObject.setOrderByDefault(false);
                           }

                           browseObject.setSqlOrderBy(xmlFile.getText(xArray));
                        } catch (Exception var43) {
                           browseObject.setSqlOrderBy((String)null);
                        }

                        xArray[0] = "sqlgroupby";
                        String sqlGroupBy = null;

                        try {
                           sqlGroupBy = xmlFile.getText(xArray);
                        } catch (Exception var42) {
                           Log.debug(this, "no groupby for " + browseTabla);
                        }

                        browseObject.setSqlGroupBy(sqlGroupBy);
                        xArray[0] = "title";

                        try {
                           browseObject.setTitle(xmlFile.getText(xArray));
                        } catch (Exception var41) {
                           browseObject.setTitle("Options");
                        }

                        if (!Util.isEmpty(pageSize)) {
                           try {
                              browseObject.setPageSize(Integer.parseInt(pageSize));
                           } catch (Exception var40) {
                              browseObject.setPageSize(10);
                           }
                        }

                        if (browseObject.getPageSize() == 0) {
                           xArray[0] = "pagesize";

                           try {
                              browseObject.setPageSize(Integer.parseInt(xmlFile.getText(xArray)));
                           } catch (Exception var39) {
                              browseObject.setPageSize(20);
                           }
                        }

                        xArray[0] = "maxrows";

                        try {
                           if (Util.isEmpty(xmlFile.getText(xArray))) {
                              maxRows = "5000";
                              String execute = (String)peticion.get("execute");
                              if (Util.ckNull(execute).equals("N")) {
                                 if (windowTime.equals("N")) {
                                    maxRows = PropiedadGeneral.getInstance(organizacionIde).getProperty("REPORTS", "SCHEDULEMAXROWS", "5000");
                                 } else {
                                    maxRows = "0";
                                 }
                              } else {
                                 maxRows = PropiedadGeneral.getInstance(organizacionIde).getProperty("BROWSE", "MAXROWS", "5000");
                              }

                              try {
                                 browseObject.setMaxRows(Integer.parseInt(maxRows));
                              } catch (Exception var37) {
                                 browseObject.setMaxRows(5000);
                              }
                           } else {
                              browseObject.setMaxRows(Integer.parseInt(xmlFile.getText(xArray)));
                           }
                        } catch (Exception var38) {
                           var38.printStackTrace();
                        }

                        xArray[0] = "detail-visible";

                        try {
                           maxRows = xmlFile.getText(xArray);
                           if (Util.ckNull(maxRows).equals("Y")) {
                              browseObject.setDetailVisible(true);
                           } else {
                              browseObject.setDetailVisible(false);
                           }
                        } catch (Exception var36) {
                           var36.printStackTrace();
                           Log.error(this, "Error setting attribute detailVisible in Browse");
                        }
                        break label284;
                     }

                     iteColNiv2 = (Element)iteLisRootNiv1.next();
                     maxRows = iteColNiv2.getName();
                     int ind = maxRows.indexOf("_");
                     className = maxRows.substring(0, ind);
                     methodName = "get" + maxRows.substring(ind + 1, ind + 2).toUpperCase() + maxRows.substring(ind + 2);
                     columna = !maxRows.contains(".") ? maxRows.replaceFirst("_", ".") : maxRows;
                  } while(!selNiv1.toUpperCase().contains(columna.toUpperCase()) && !selNiv1.toUpperCase().contains(maxRows.toUpperCase()) && !this.existeColumnAlias(iteColNiv2, selNiv1));

                  browseColumns[indCol] = new BrowseColumn(organizacionIde, language);
                  browseColumns[indCol].setColumnName(maxRows);
                  browseColumns[indCol].setClassName(className);
                  browseColumns[indCol].setMethodName(methodName);
                  String attrName = null;
                  String attrValue = null;
                  String attribAlias = null;
                  List lisFieldNiv3 = iteColNiv2.getChildren();
                  Iterator iteLisAttrNiv4 = lisFieldNiv3.iterator();

                  while(true) {
                     label266:
                     do {
                        while(iteLisAttrNiv4.hasNext()) {
                           Element attrNiv4 = (Element)iteLisAttrNiv4.next();
                           attrName = attrNiv4.getName();
                           attrValue = attrNiv4.getText();
                           if (attrName.equalsIgnoreCase("type")) {
                              browseColumns[indCol].setType(attrValue);
                              continue label266;
                           }

                           if (attrName.equalsIgnoreCase("label")) {
                              browseColumns[indCol].setLabel(attrValue);
                           } else if (attrName.equalsIgnoreCase("visible")) {
                              if (Util.ckNull(attrValue).equals("0")) {
                                 browseColumns[indCol].setHidden(true);
                              }
                           } else if (attrName.equalsIgnoreCase("treeRelationColumn")) {
                              browseColumns[indCol].setTreeRelationColumn(attrValue);
                           } else if (attrName.equalsIgnoreCase("treeDescription")) {
                              browseColumns[indCol].setTreeDescription(attrValue);
                           } else if (attrName.equalsIgnoreCase("onClick")) {
                              browseColumns[indCol].setOnClick(attrValue);
                              BrowseUtility.getColumnNamesFromFunction(browseColumns[indCol]);
                           } else if (attrName.equalsIgnoreCase("checked")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                                 browseColumns[indCol].setChecked(true);
                              }
                           } else if (attrName.equalsIgnoreCase("link")) {
                              browseColumns[indCol].setLink(attrValue);
                              BrowseUtility.getColumnNamesFromLink(browseColumns[indCol]);
                           } else if (attrName.equalsIgnoreCase("scriptCode")) {
                              browseColumns[indCol].setScriptCode(attrValue);
                           } else if (attrName.equalsIgnoreCase("linkImage")) {
                              browseColumns[indCol].setLinkImage(attrValue);
                           } else if (attrName.equalsIgnoreCase("sort")) {
                              browseColumns[indCol].setSort(attrValue);
                           } else if (attrName.equalsIgnoreCase("allowFilter")) {
                              browseColumns[indCol].setAllowFilter(attrValue);
                           } else if (attrName.equalsIgnoreCase("trim")) {
                              browseColumns[indCol].setTrim(Integer.valueOf(attrValue));
                           } else if (attrName.equalsIgnoreCase("size")) {
                              browseColumns[indCol].setSize(Integer.valueOf(attrValue));
                           } else if (attrName.equalsIgnoreCase("input-size")) {
                              browseColumns[indCol].setInputSize(Integer.valueOf(attrValue));
                           } else if (attrName.equalsIgnoreCase("sortable")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("N")) {
                                 browseColumns[indCol].setSortable(false);
                              }
                           } else if (attrName.equalsIgnoreCase("hidden")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                                 browseColumns[indCol].setHidden(true);
                              }
                           } else if (attrName.equalsIgnoreCase("hidden-input")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                                 browseColumns[indCol].setHiddenInput(true);
                              }
                           } else if (attrName.equalsIgnoreCase("button-input")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                                 browseColumns[indCol].setButtonInput(true);
                              }
                           } else if (attrName.equalsIgnoreCase("text-input")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                                 browseColumns[indCol].setTextInput(true);
                              }
                           } else if (attrName.equalsIgnoreCase("select-input")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                                 browseColumns[indCol].setSelectInput(true);
                              }
                           } else if (attrName.equalsIgnoreCase("checkbox")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                                 browseColumns[indCol].setCheckbox(true);
                              }
                           } else if (attrName.equalsIgnoreCase("detail")) {
                              if (Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                                 browseColumns[indCol].setDetail(true);
                                 browseObject.setDetailIncluded(true);
                              }
                           } else if (attrName.equalsIgnoreCase("alias")) {
                              browseColumns[indCol].setAlias(attrValue);
                              attribAlias = attrValue;
                           } else if (attrName.equalsIgnoreCase("subselect")) {
                              browseColumns[indCol].setSubselect(attrValue);
                              attribAlias = attrValue;
                           } else if (attrName.equalsIgnoreCase("align")) {
                              browseColumns[indCol].setAlignment(attrValue);
                           } else if (attrName.equalsIgnoreCase("mouseover")) {
                              browseColumns[indCol].setMouseover(attrValue);
                           } else if (attrName.equalsIgnoreCase("mouseout")) {
                              browseColumns[indCol].setMouseout(attrValue);
                           } else if (attrName.equalsIgnoreCase("editable") && Util.ckNull(attrValue).equalsIgnoreCase("Y")) {
                              browseColumns[indCol].setEditable(true);
                           }
                        }

                        sql.append(maxRows.replace('_', '.') + " " + attribAlias);
                        sql.append(", ");
                        browseColumns[indCol].setIndex(selectColumnIndex);
                        ++selectColumnIndex;
                        columnTypes.put(maxRows, browseColumns[indCol].getType());
                        columnLabels.put(maxRows, browseColumns[indCol].getLabel());
                        if (!browseColumns[indCol].isHidden() && !browseColumns[indCol].isDetail()) {
                           lisLab.add(browseColumns[indCol]);
                        }

                        ++indCol;
                        continue label271;
                     } while(attrValue.indexOf("Decimal") < 0 && attrValue.indexOf("PriceQuantity") < 0);

                     browseColumns[indCol].setAlignment("right");
                  }
               }
            }
         } catch (Exception var44) {
            var44.printStackTrace();
         }

         this.setEstado(80);
         return browseObject;
      } catch (Exception var45) {
         throw var45;
      }
   }

   public boolean existeColumnAlias(Element ele, String texto) {
      List lisChild = ele.getChildren();
      Iterator iteLisChild = lisChild.iterator();
      boolean result = false;

      while(true) {
         String attrName;
         String attrValue;
         do {
            if (!iteLisChild.hasNext()) {
               return result;
            }

            Element attrEle = (Element)iteLisChild.next();
            attrName = attrEle.getName();
            attrValue = attrEle.getText();
         } while(!attrName.equalsIgnoreCase("columnAlias") && !attrName.equalsIgnoreCase("alias"));

         if (texto.toUpperCase().contains(attrValue.toUpperCase())) {
            result = true;
         }
      }
   }
}
