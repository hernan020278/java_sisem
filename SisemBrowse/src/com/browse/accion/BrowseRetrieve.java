package com.browse.accion;

import com.browse.BrowseColumn;
import com.browse.BrowseFilter;
import com.browse.BrowseObject;
import com.browse.BrowseValidationUtility;
import com.comun.database.DBConeccion;
import com.comun.database.DBConfiguracion;
import com.comun.motor.Accion;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.utilidad.Dates;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class BrowseRetrieve extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {

      StringBuffer query = new StringBuffer();
      String consulta = "";
      Object result = null;

      try {
         DBConeccion dbc = (DBConeccion)peticion.get("dbConeccion");
         String organizationIde = (String)peticion.get("organizationIde");
         String userId = (String)peticion.get("userId");
         BrowseObject browseObject = (BrowseObject)peticion.get("browseObject");
         String execute = (String)peticion.get("execute");
         PropiedadGeneral prop = PropiedadGeneral.getInstance(organizationIde);
         boolean hasDistinct = false;
         boolean hasDistinctOrGroup = false;
         String distinctFields = "";
         String userDateFormat = "dd/MM/yyy";
         if (execute == null) {
            execute = "Y";
         }

         StringBuffer queryCount = new StringBuffer();
         StringBuffer queryFilter = new StringBuffer();
         StringBuffer orderBy = new StringBuffer();
         String sqlSelect = browseObject.getSqlSelect();
         String sqlFrom = browseObject.getSqlFrom();
         String sqlWhere = browseObject.getSqlWhere();
         String sqlGroupBy = browseObject.getSqlGroupBy();
         String sqlOrderBy = browseObject.getSqlOrderBy();
         String sortColumn = "";
         query.append(sqlSelect);
         if (sqlSelect.indexOf("DISTINCT") != -1) {
            hasDistinct = true;
            hasDistinctOrGroup = true;
            distinctFields = sqlSelect.replace("Select ", "").replace("DISTINCT ", "");
         } else if (!Util.isEmpty(sqlGroupBy)) {
            hasDistinctOrGroup = true;
         }

         queryCount.append("Select count(*)");
         queryCount.append(" from " + sqlFrom + " where 1 = 1");
         query.append(" from " + sqlFrom + " where 1 = 1");
         List dateArguments = new ArrayList();
         List dateArgumentsCount = new ArrayList();
         List filters = browseObject.getBrowseFilters();
         int openParenthesis = 0;
         boolean parentesisAbierto = false;
         String buyerFromAsUserId = Util.ckNull((String)peticion.get("buyerFromAsUserId"));
         int orderByIndex;
         int i;
         String queryFinal;
         String key;
         if (Util.ckNull(sqlWhere).length() > 0) {
            sqlWhere = sqlWhere.replaceAll(":as_userid", "'" + userId + "'");
            if (execute.equalsIgnoreCase("Y")) {
               for(orderByIndex = sqlWhere.indexOf("@"); orderByIndex > 0; orderByIndex = sqlWhere.indexOf("@")) {
                  i = sqlWhere.indexOf("#");
                  String variable = sqlWhere.substring(orderByIndex + 1, i);
                  queryFinal = Util.ckNull((String)peticion.get(variable));
                  if (browseObject.requireAttributeValues() && Util.isEmpty(queryFinal)) {
                     browseObject.setAttributeSet(false);
                     return new ArrayList();
                  }

                  queryFinal = queryFinal.trim();
                  queryFinal = "'" + queryFinal + "'";
                  key = "@" + variable + "#";
                  sqlWhere = sqlWhere.replaceAll(key, queryFinal);
               }

               queryCount.append(" and ( " + sqlWhere + ")");
               query.append(" and ( " + sqlWhere + ")");
            }
         }

         String priorKey;
         if (filters != null) {
            boolean orOperatorSet = false;
            boolean andOperatorSet = false;
            priorKey = "";

            for(int iteFil = 0; iteFil < filters.size(); ++iteFil) {
               BrowseFilter filter = (BrowseFilter)filters.get(iteFil);
               key = filter.getColumnName();
               String value = filter.getValue();
               String operator = filter.getOperator();
               String logicalOperator = filter.getLogicalOperator();
               String sort = filter.getSort();
               String type = filter.getType();
               if (!BrowseValidationUtility.permissibleOperators.contains(operator)) {
                  operator = "=";
               }

               if (!BrowseValidationUtility.permissibleLogicalOperators.contains(logicalOperator)) {
                  logicalOperator = "and";
               }

               if (!BrowseValidationUtility.permissibleSortOperators.contains(sort)) {
                  sort = "N";
               }

               if (!Util.isEmpty(value)) {
                  if (Util.isEmpty(operator)) {
                     operator = "=";
                  }

                  if (Util.isEmpty(logicalOperator)) {
                     logicalOperator = "and";
                  }

                  if (Util.isEmpty(type)) {
                     type = "STRING";
                  }

                  if (!Util.isEmpty(value) && (type.equalsIgnoreCase("STRING") || type.equalsIgnoreCase("USER-ID")) && operator.equalsIgnoreCase("=")) {
                     if (value.startsWith("'") && value.endsWith("'")) {
                        while(value.startsWith("'") && value.endsWith("'")) {
                           value = value.substring(1);
                           value = value.substring(0, value.length() - 1);
                        }
                     } else {
                        operator = "like";
                     }
                  }

                  if (iteFil < filters.size() && !andOperatorSet) {
                     queryFilter.append(" (");
                     andOperatorSet = true;
                     ++openParenthesis;
                  }

                  if (type.equalsIgnoreCase("DATE")) {
                     if (value.startsWith(":")) {
                        if (execute.equalsIgnoreCase("Y")) {
                           dateArguments.add(Dates.getDateWithoutHour(value, userDateFormat));
                           dateArgumentsCount.add(Dates.getDateWithoutHour(value, userDateFormat));
                           if (operator.equals("=")) {
                              dateArguments.add(Dates.add(Dates.getDateWithoutHour(value, userDateFormat), 1));
                              dateArgumentsCount.add(Dates.add(Dates.getDateWithoutHour(value, userDateFormat), 1));
                              queryFilter.append(" " + key + " >= ? and " + key + " < ? ");
                           } else {
                              queryFilter.append(" " + key + " " + operator + " ?");
                           }
                        } else {
                           queryFilter.append(" " + key + " " + operator + " :as_date" + value);
                        }
                     } else {
                        queryFilter.append(" " + key + " " + operator + " '" + value + "'");
                     }
                  } else if (operator.equalsIgnoreCase("like")) {
                     if (value.indexOf(37) < 0) {
                        queryFilter.append(" upper(" + key + ") " + operator + " '%" + value.toUpperCase() + "%'");
                     } else {
                        queryFilter.append(" upper(" + key + ") " + operator + " '" + value.toUpperCase() + "'");
                     }
                  } else if (type.toUpperCase().indexOf("DECIMAL") >= 0) {
                     if (operator.equalsIgnoreCase("isnull")) {
                        queryFilter.append(" " + key + " is null");
                     } else {
                        queryFilter.append(" " + key + " " + operator + " " + value.toUpperCase() + "");
                     }
                  } else if (operator.equalsIgnoreCase("isnull")) {
                     queryFilter.append(" " + key + " is null");
                  } else {
                     queryFilter.append(" upper(" + key + ") " + operator + " '" + value.toUpperCase() + "'");
                  }
               }

               if (!Util.isEmpty(sort) && !sort.equalsIgnoreCase("N")) {
                  if (orderBy.length() > 0) {
                     if (!priorKey.equals(key)) {
                        orderBy.append(", " + key + " " + sort);
                        priorKey = key;
                     }
                  } else {
                     sortColumn = key;
                     orderBy.append(key + " " + sort);
                     priorKey = key;
                  }
               }

               if (iteFil + 1 >= filters.size()) {
                  if (iteFil + 1 == filters.size()) {
                     queryFilter.append(")");
                  }
               } else {
                  BrowseFilter nextFilter = (BrowseFilter)filters.get(iteFil + 1);
                  String nextLogicalOper = nextFilter.getLogicalOperator();
                  if (!Util.isEmpty(logicalOperator) && logicalOperator.equalsIgnoreCase("AND") && !Util.isEmpty(nextLogicalOper) && nextLogicalOper.equalsIgnoreCase("OR") || !Util.isEmpty(logicalOperator) && logicalOperator.equalsIgnoreCase("AND")) {
                     queryFilter.append(") " + logicalOperator + " (");
                     andOperatorSet = true;
                  } else {
                     queryFilter.append(" " + logicalOperator + " ");
                  }
               }
            }

            if (!Util.isEmpty(queryFilter.toString())) {
               StringBuffer queryFilterTemp = new StringBuffer();
               queryFilterTemp.append(" 2 = 2 and ( ");
               queryFilterTemp.append(queryFilter);
               queryFilterTemp.append(" )");
               queryFilter = queryFilterTemp;
            }
         }

         if (queryFilter.length() > 0) {
            query.append(" and (" + queryFilter + " )");
            queryCount.append(" and (" + queryFilter + " )");
            browseObject.setQueryFilter(queryFilter.toString());
         }

         if (!Util.isEmpty(sqlGroupBy)) {
            query.append(" group by ");
            query.append(sqlGroupBy);
            queryCount.append(" group by ");
            queryCount.append(sqlGroupBy);
            if (hasDistinct) {
               String[] distinctFieldsArray = distinctFields.split(",");
               orderByIndex = distinctFieldsArray.length;

               for(i = 0; i < orderByIndex; ++i) {
                  if (sqlGroupBy.indexOf(distinctFieldsArray[i].trim()) == -1) {
                     queryCount.append(",");
                     queryCount.append(distinctFieldsArray[i]);
                  }
               }
            }
         } else if (hasDistinct) {
            queryCount.append(" group by ");
            queryCount.append(distinctFields);
         }

         if (orderBy.length() > 0) {
            query.append(" order by " + orderBy);
         } else if (!Util.isEmpty(sqlOrderBy)) {
            query.append(" order by " + sqlOrderBy);
         }

         Log.debug(this, peticion.get("userId") + " - query: " + query.toString());
         if (execute.equalsIgnoreCase("Y")) {
            long start = System.currentTimeMillis();
            priorKey = null;
            Object[] arguments = new Object[dateArguments.size()];

            int idxInicio;
            for(idxInicio = 0; idxInicio < dateArguments.size(); ++idxInicio) {
               arguments[idxInicio] = dateArguments.get(idxInicio);
            }

            consulta = query.toString();
            if (DBConfiguracion.getInstance().getDriverJdbc().equals("MYSQL")) {
               idxInicio = 0;
               if (consulta.contains("row_number()")) {
                  idxInicio = consulta.indexOf("row_number()");
               }

               if (consulta.contains("dense_rank()")) {
                  idxInicio = consulta.indexOf("dense_rank()");
               }

               int idxFin = consulta.indexOf("rownum,") + 7;
               consulta.substring(0, idxInicio);
               consulta.substring(idxFin, consulta.length());
               consulta = consulta.substring(0, idxInicio).trim() + " " + consulta.substring(idxFin, consulta.length()).trim();
               if (consulta.contains("charindex")) {
                  consulta = consulta.replaceAll("charindex", "locate");
               }

               if (consulta.contains("dbo.")) {
                  consulta = consulta.replaceAll("dbo.", "");
               }
            }

            browseObject.setTotalRecord(dbc.obtenerCantidadRegistros(consulta));
            queryFinal = "";
            if (DBConfiguracion.getInstance().getDriverJdbc().equals("SQLSERVER")) {
               queryFinal = "select * from (" + consulta + ") as listaResultado where rownum between (" + browseObject.getCurrentPage() + "-1) * " + browseObject.getPageSize() + " + 1 and " + browseObject.getCurrentPage() + " * " + browseObject.getPageSize();
            } else {
               queryFinal = consulta + " LIMIT " + (browseObject.getCurrentPage() - 1) * browseObject.getPageSize() + "," + browseObject.getPageSize();
            }

            peticion.put("respuesta", "LISTAMIXTA");
            peticion.put("consulta", queryFinal);
            peticion.put("listaColumnaTipo", this.obtenerListaColumnaTipo(browseObject.getBrowseColumns()));
            peticion.put("listaColumnaAlias", this.obtenerListaColumnaAlias(browseObject.getBrowseColumns()));
            List listaResultado = dbc.ejecutarConsulta(peticion);
            long end = System.currentTimeMillis();
            Log.debug(this, "It took " + (end - start) / 1000L + " seconds to execute the query");
            Log.debug(this, "dbc.query COMPLETE.");
            result = listaResultado;
            Log.debug(this, "values for BrowseObjects have been populated.");
         } else {
            int whereIndex = query.indexOf("where");
            orderByIndex = query.indexOf(" order by");
            result = consulta.substring(whereIndex);
            if (orderByIndex > whereIndex) {
               peticion.put("sqlWhereForCount", consulta.substring(whereIndex, orderByIndex));
            }
         }

         peticion.put("sortedColumn", sortColumn);
         this.setEstado(dbc.getEstado());
         return result;
      } catch (Exception var43) {
         Log.error(this.getTareaNombre(), consulta);
         this.setEstado(0);
         throw var43;
      }
   }

   public static Date baseReportDate() {
      Calendar calendar = Calendar.getInstance();
      calendar.set(2006, 0, 1);
      return calendar.getTime();
   }

   public String[] obtenerListaColumnaTipo(BrowseColumn[] browseColumns) {
      String[] lisColTip = new String[browseColumns.length];

      for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; ++iteBrwCol) {
         lisColTip[iteBrwCol] = browseColumns[iteBrwCol].getType();
      }

      return lisColTip;
   }

   public String[] obtenerListaColumnaAlias(BrowseColumn[] browseColumns) {
      String[] lisColAli = new String[browseColumns.length];

      for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; ++iteBrwCol) {
         lisColAli[iteBrwCol] = browseColumns[iteBrwCol].getAlias();
      }

      return lisColAli;
   }
}
