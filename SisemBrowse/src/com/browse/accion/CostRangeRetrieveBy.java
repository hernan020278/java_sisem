package com.browse.accion;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import java.util.List;
import java.util.Map;

public class CostRangeRetrieveBy extends Accion {
   public Object executeTask(Object object) throws Exception {
      Map incomingRequest = (Map)object;
      DBConeccion dbs = (DBConeccion)incomingRequest.get("dbconeccion");
      StringBuffer queryString = new StringBuffer("from CostRange as costrange where 1=1 ");
      String maximumCost;
      if (incomingRequest.containsKey("CostRange_icCostRange")) {
         maximumCost = (String)incomingRequest.get("CostRange_icCostRange");
         queryString.append(" AND costrange.id.icCostRange = '" + maximumCost + "'");
      }

      if (incomingRequest.containsKey("CostRange_itemType")) {
         maximumCost = (String)incomingRequest.get("CostRange_itemType");
         queryString.append(" AND costrange.itemType = '" + maximumCost + "'");
      }

      if (incomingRequest.containsKey("CostRange_description")) {
         maximumCost = (String)incomingRequest.get("CostRange_description");
         queryString.append(" AND costrange.description = '" + maximumCost + "'");
      }

      if (incomingRequest.containsKey("CostRange_minimumCost")) {
         maximumCost = (String)incomingRequest.get("CostRange_minimumCost");
         queryString.append(" AND costrange.minimumCost = '" + maximumCost + "'");
      }

      if (incomingRequest.containsKey("CostRange_maximumCost")) {
         maximumCost = (String)incomingRequest.get("CostRange_maximumCost");
         queryString.append(" AND costrange.maximumCost = '" + maximumCost + "'");
      }

      List result = dbs.query(queryString.toString());
      this.setEstado(dbs.getEstado());
      return result;
   }
}
