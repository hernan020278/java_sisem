package com.browse;

import java.util.HashSet;
import java.util.Set;

public class BrowseValidationUtility {
   public static Set permissibleOperators = new HashSet();
   public static Set permissibleLogicalOperators = new HashSet();
   public static Set permissibleSortOperators = new HashSet();

   static {
      permissibleOperators.add("=");
      permissibleOperators.add(">");
      permissibleOperators.add(">=");
      permissibleOperators.add("LIKE");
      permissibleOperators.add("like");
      permissibleOperators.add("<");
      permissibleOperators.add("<=");
      permissibleOperators.add("<>");
      permissibleOperators.add("isnull");
      permissibleLogicalOperators.add("OR");
      permissibleLogicalOperators.add("or");
      permissibleLogicalOperators.add("AND");
      permissibleLogicalOperators.add("and");
      permissibleSortOperators.add("n");
      permissibleSortOperators.add("N");
      permissibleSortOperators.add("DESC");
      permissibleSortOperators.add("desc");
      permissibleSortOperators.add("ASC");
      permissibleSortOperators.add("asc");
   }
}
