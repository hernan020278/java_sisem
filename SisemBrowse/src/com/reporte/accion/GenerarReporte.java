package com.reporte.accion;

import com.browse.Browse;
import com.browse.BrowseObject;
import com.browse.accion.BrowseRetrieve;
import com.browse.accion.EjecutarListaReporte;
import com.browse.accion.GenerateBrowseFilter;
import com.browse.accion.GenerateReportObject;
import com.comun.motor.Accion;
import com.comun.utilidad.Util;
import java.util.List;
import java.util.Map;

public class GenerarReporte extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {
      Object result = null;

      try {
         BrowseObject browseObject = null;
         Browse browse = null;
         browseObject = (BrowseObject)(new GenerateReportObject()).ejecutarAccion(peticion);
         peticion.put("browseObject", browseObject);
         browseObject = (BrowseObject)(new GenerateBrowseFilter()).ejecutarAccion(peticion);
         peticion.put("browseObject", browseObject);
         List datasource = (List)(new BrowseRetrieve()).ejecutarAccion(peticion);
         peticion.put("datasource", datasource);
         if (datasource != null && datasource.size() > 0) {
            String url = (String)(new EjecutarListaReporte()).ejecutarAccion(peticion);
            Util.getInst().abrirArchivoDesktop(url);
         }

         this.setEstado(80);
      } catch (Exception var7) {
         this.setEstado(0);
         var7.printStackTrace();
      }

      return result;
   }
}
