package com.reporte.accion;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.utilidad.Util;
import java.util.List;
import java.util.Map;

public class ObtenerReportePorModulo extends Accion {
   public Object ejecutarAccion(Map peticion) throws Exception {
      List result = null;

      try {
         try {
            DBConeccion dbs = (DBConeccion)peticion.get("dbConeccion");
            String organizationId = (String)peticion.get("organizationId");
            String reporteModulo = Util.ckNull((String)peticion.get("reporteModulo"));
            peticion.put("consulta", "select * from reporte where modulo = '" + reporteModulo + "'");
            peticion.put("entidadNombre", "Reporte");
            List listaReporte = (List)(new ObtenerListaEntidad()).ejecutarAccion(peticion);
            peticion.put("listaReporte", listaReporte);
            result = listaReporte;
         } catch (Exception var7) {
            var7.printStackTrace();
            throw var7;
         }

         this.setEstado(80);
         return result;
      } catch (Exception var8) {
         this.setEstado(0);
         return result;
      }
   }
}
