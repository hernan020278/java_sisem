package com.reporte.datasource;

import com.comun.utilidad.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRRewindableDataSource;

public class HibernateQueryResultDataSource implements JRRewindableDataSource {
   private Iterator iterator;
   private Object currentValue;
   private List list;
   private String organizationId;

   public HibernateQueryResultDataSource(List _list) {
      this.iterator = _list.iterator();
      this.list = _list;
   }

   public boolean next() throws JRException {
      this.currentValue = this.iterator.hasNext() ? this.iterator.next() : null;
      return this.currentValue != null;
   }

   public int getSize() {
      return this.list != null ? this.list.size() : 0;
   }

   public Object getFieldValue(JRField field) throws JRException {
      Object value = null;

      try {
         String sfield = field.getName();
         String fieldName = sfield.substring(sfield.indexOf("_") + 1);
         String first = fieldName.substring(0, 1);
         first = first.toUpperCase();
         fieldName = first + fieldName.substring(1);
         Method fld = this.currentValue.getClass().getMethod("get" + fieldName, (Class[])null);
         Log.debug(this, "getFieldValue, field: " + fld.getName());
         value = fld.invoke(this.currentValue, (Object[])null);
         Log.debug(this, "getFieldValue, value:-" + value + "-");
      } catch (IllegalAccessException var7) {
         value = null;
         var7.printStackTrace();
      } catch (InvocationTargetException var8) {
         value = null;
         var8.printStackTrace();
      } catch (NoSuchMethodException var9) {
         value = null;
         var9.printStackTrace();
      }

      return value;
   }

   public String getOrganizationId() {
      return this.organizationId;
   }

   public void setOrganizationId(String organizationId) {
      this.organizationId = organizationId;
   }

   public List getList() {
      return this.list;
   }

   public void moveFirst() throws JRException {
   }
}
