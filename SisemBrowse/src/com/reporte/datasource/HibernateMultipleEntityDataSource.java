package com.reporte.datasource;

import com.comun.utilidad.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class HibernateMultipleEntityDataSource implements JRDataSource {
   private Iterator iterator;
   private Object[] currentValue;
   private List list;
   private String organizationId;

   public HibernateMultipleEntityDataSource(List _list) {
      if (_list != null) {
         this.iterator = _list.iterator();
         this.list = _list;
      } else {
         this.list = new ArrayList();
         this.iterator = this.list.iterator();
      }

   }

   public boolean next() throws JRException {
      if (this.list != null) {
         this.currentValue = (Object[])((Object[])(this.iterator.hasNext() ? this.iterator.next() : null));
         return this.currentValue != null;
      } else {
         return false;
      }
   }

   public int getSize() {
      return this.list != null ? this.list.size() : 0;
   }

   public Object getFieldValue(JRField field) throws JRException {
      Object value = null;

      try {
         String sfield = field.getName();
         String fieldName = sfield.substring(sfield.indexOf("_") + 1);
         String className = sfield.substring(0, sfield.indexOf("_"));
         String first = fieldName.substring(0, 1);
         first = first.toUpperCase();
         fieldName = first + fieldName.substring(1);

         for(int i = 0; i < this.currentValue.length; ++i) {
            Class c = this.currentValue[i].getClass();
            if (className.equals(c.getName().substring(c.getName().lastIndexOf(".") + 1))) {
               Method fld = c.getMethod("get" + fieldName, (Class[])null);
               Log.debug(this, "getFieldValue, field: " + fld.getName());
               value = fld.invoke(this.currentValue[i], (Object[])null);
               Log.debug(this, "getFieldValue, value:-" + value + "-");
               return value;
            }
         }
      } catch (IllegalAccessException var10) {
         value = null;
         var10.printStackTrace();
      } catch (InvocationTargetException var11) {
         value = null;
         var11.printStackTrace();
      } catch (NoSuchMethodException var12) {
         value = null;
         var12.printStackTrace();
      }

      return value;
   }

   public String getOrganizationId() {
      return this.organizationId;
   }

   public void setOrganizationId(String organizationId) {
      this.organizationId = organizationId;
   }

   public List getList() {
      return this.list;
   }
}
