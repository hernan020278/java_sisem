package com.reporte.datasource;

import com.comun.utilidad.Log;
import java.lang.reflect.Method;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class EntityDataSource implements JRDataSource {
   private Object entity;
   private boolean hasNext = true;
   private String organizationId = "sisem";

   public EntityDataSource(Object fromEntity) {
      this.entity = fromEntity;
   }

   public EntityDataSource(Object fromEntity, String _organizationId) {
      this.entity = fromEntity;
      this.setOrganizationId(_organizationId);
   }

   public boolean next() throws JRException {
      boolean nextItem = true;
      if (this.hasNext) {
         this.hasNext = false;
      } else {
         nextItem = false;
      }

      return nextItem;
   }

   public Object getFieldValue(JRField field) throws JRException {
      Object value = null;
      if (this.entity == null) {
         return null;
      } else {
         Method fld = null;

         try {
            String sfield = field.getName();
            String fieldName = sfield.substring(sfield.indexOf("_") + 1);
            String first = fieldName.substring(0, 1);
            first = first.toUpperCase();
            fieldName = first + fieldName.substring(1);
            Class c = this.entity.getClass();
            fld = c.getMethod("get" + fieldName, (Class[])null);
         } catch (NoSuchMethodException var9) {
            var9.printStackTrace();
            value = null;
            return null;
         }

         try {
            value = fld.invoke(this.entity, (Object[])null);
            Log.debug(this, "campo : " + field.getName() + " metodo : " + fld.getName() + " valor : " + value);
         } catch (Exception var8) {
            value = null;
            var8.printStackTrace();
         }

         return value;
      }
   }

   public String getOrganizationId() {
      return this.organizationId;
   }

   public void setOrganizationId(String organizationId) {
      this.organizationId = organizationId;
   }
}
