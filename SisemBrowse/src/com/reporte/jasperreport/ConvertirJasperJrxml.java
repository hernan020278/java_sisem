package com.reporte.jasperreport;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlWriter;

public class ConvertirJasperJrxml {
   public static String sourcePath;
   public static String destinationPath;
   public static String xml;
   public static JasperDesign jd = new JasperDesign();

   public static void main(String[] args) {
      sourcePath = "C:/SISTEMA_CHENTTY/REPORTES/RepCreditoCliente.jasper";
      destinationPath = "C:/SISTEMA_CHENTTY/REPORTES/RepCreditoCliente.jrxml";

      try {
         JasperReport report = (JasperReport)JRLoader.loadObject(sourcePath);
         JRXmlWriter.writeReport(report, destinationPath, "UTF-8");
      } catch (JRException var2) {
         var2.printStackTrace();
      }

   }
}
