package com.reporte.jasperreport;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Cmpproducto;
import com.comun.entidad.Produccion;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.utilidad.CodigoBarras;
import com.comun.utilidad.Util;
import com.reporte.datasource.HibernateQueryResultDataSource;
import java.awt.Image;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportUtils {
   public static String getText(String value) {
      StringBuffer sbValue = new StringBuffer(value.toString());
      StringBuffer temp = new StringBuffer(value.toString());

      for(int i = 40; i < sbValue.length(); i += 40) {
         int breakLineIndex = i;
         if (temp.charAt(i) != ' ') {
            breakLineIndex = temp.indexOf(" ", i);
            if (breakLineIndex > i + 40 || breakLineIndex >= temp.length() || breakLineIndex == -1) {
               breakLineIndex = i;
            }
         }

         temp.insert(breakLineIndex, "\r\n");
      }

      return temp.toString();
   }

   public static String getProperty(String oid, String section, String property, String sdefault) {
      String prop = "";
      PropiedadGeneral.getInstance(oid).getProperty("COMPANY", "Name", "TSA");
      return prop;
   }

   public static List quitarRepetidos(List listaDataSource) {
      List listaCodigo = new ArrayList();
      List listaOperario = new ArrayList();

      for(int iteLis = 0; iteLis < listaDataSource.size(); ++iteLis) {
         Object[] registro = (Object[])((Object[])listaDataSource.get(iteLis));
         if (!listaCodigo.contains(registro[0])) {
            listaCodigo.add(registro[0]);
            listaOperario.add(registro);
         }
      }

      return listaOperario;
   }

   public static HibernateQueryResultDataSource obtenerListaOperariosDS(DBConeccion dbc, String whereReporte) {
      new ArrayList();
      List listaProduccionEntidad = new ArrayList();
      Map peticion = new HashMap();
      HibernateQueryResultDataSource listaProduccionDS = null;

      try {
         whereReporte = whereReporte.equals("") ? "" : whereReporte + " ";
         String sql = "select usucodigo usucodigo, operario operario from produccion where (1=1) and " + whereReporte + "usucodigo<>0 and operario<>'' group by usucodigo, operario";
         String[] listaColumnaTipo = new String[]{"BIGDECIMAL", "STRING"};
         String[] listaColumnaAlias = new String[]{"usucodigo", "operario"};
         peticion.put("respuesta", "LISTAMIXTA");
         peticion.put("consulta", sql);
         peticion.put("listaColumnaTipo", listaColumnaTipo);
         peticion.put("listaColumnaAlias", listaColumnaAlias);
         List listaConsulta = dbc.ejecutarConsulta(peticion);

         for(int iteLisPro = 0; iteLisPro < listaConsulta.size(); ++iteLisPro) {
            Object[] registro = (Object[])((Object[])listaConsulta.get(iteLisPro));
            Produccion produccion = new Produccion();
            produccion.setUsucodigo((BigDecimal)registro[0]);
            produccion.setOperario((String)registro[0]);
            listaProduccionEntidad.add(produccion);
         }

         listaProduccionDS = new HibernateQueryResultDataSource(listaProduccionEntidad);
      } catch (Exception var13) {
         var13.printStackTrace();
      }

      return listaProduccionDS;
   }

   public static HibernateQueryResultDataSource obtenerListaProduccionDS(DBConeccion dbc, String whereReporte) {
      new ArrayList();
      List listaProduccionEntidad = new ArrayList();
      Map peticion = new HashMap();
      HibernateQueryResultDataSource listaProduccionDS = null;

      try {
         whereReporte = whereReporte.equals("") ? "" : "where " + whereReporte + " ";
         String sql = "select produccion.fecelaboracion fecelaboracion from produccion " + whereReporte + "group by produccion.fecelaboracion";
         String[] listaColumnaTipo = new String[]{"DATE"};
         String[] listaColumnaAlias = new String[]{"fecelaboracion"};
         peticion.put("respuesta", "LISTAMIXTA");
         peticion.put("consulta", sql);
         peticion.put("listaColumnaTipo", listaColumnaTipo);
         peticion.put("listaColumnaAlias", listaColumnaAlias);
         List listaConsulta = dbc.ejecutarConsulta(peticion);

         for(int iteLisPro = 0; iteLisPro < listaConsulta.size(); ++iteLisPro) {
            Object[] registro = (Object[])((Object[])listaConsulta.get(iteLisPro));
            Produccion produccion = new Produccion();
            produccion.setFecelaboracion((Date)registro[0]);
            listaProduccionEntidad.add(produccion);
         }

         listaProduccionDS = new HibernateQueryResultDataSource(listaProduccionEntidad);
      } catch (Exception var13) {
         var13.printStackTrace();
      }

      return listaProduccionDS;
   }

   public static HibernateQueryResultDataSource getListaComponenteTallaDS(DBConeccion dbc, Integer pedcodigo, Integer modcodigo) {
      Map peticion = new HashMap();
      HibernateQueryResultDataSource listaTallasDS = null;

      try {
         peticion.put("dbConeccion", dbc);
         peticion.put("prdcodigo", modcodigo);
         peticion.put("pedcodigo", pedcodigo);
         getListaTalla(peticion);
         List listaTallas = (List)peticion.get("listaTallas");
         List listaTallasEntidad = new ArrayList();

         for(int ite = 0; ite < listaTallas.size(); ++ite) {
            Object[] registro = (Object[])((Object[])listaTallas.get(ite));
            Cmpproducto cmpPrd = new Cmpproducto();
            cmpPrd.setPrd_des((String)registro[2]);
            listaTallasEntidad.add(cmpPrd);
         }

         listaTallasDS = new HibernateQueryResultDataSource(listaTallasEntidad);
      } catch (Exception var10) {
         var10.printStackTrace();
      }

      return listaTallasDS;
   }

   public static void getListaTalla(Map peticion) {
      try {
         DBConeccion dbc = (DBConeccion)peticion.get("dbConeccion");
         int prdcodigo = (Integer)peticion.get("prdcodigo");
         int pedcodigo = (Integer)peticion.get("pedcodigo");
         String sql = "select pedido.ped_cod pedcodigo, producto.prd_supuno modcodigo, producto.prd_des talla from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod inner join producto on producto.prd_cod=detpedido.prd_cod where producto.prd_supuno=" + prdcodigo + " and pedido.ped_cod=" + pedcodigo + " and producto.prd_nivel=3 group by pedido.ped_cod, prd_supuno, producto.prd_des order by dbo.funGetFillZero(producto.prd_des,5) asc";
         String[] listaColumnaTipo = new String[]{"INTEGER", "INTEGER", "STRING"};
         String[] listaColumnaAlias = new String[]{"pedcodigo", "modcodigo", "talla"};
         peticion.put("listaColumnaTipo", listaColumnaTipo);
         peticion.put("listaColumnaAlias", listaColumnaAlias);
         peticion.put("respuesta", "LISTAMIXTA");
         peticion.put("consulta", sql);
         List listaTallas = dbc.ejecutarConsulta(peticion);
         peticion.put("listaTallas", listaTallas);
      } catch (Exception var8) {
         var8.printStackTrace();
      }

   }

   public static HibernateQueryResultDataSource obtenerListaDS(DBConeccion dbc, String consulta, String entidadNombre) {
      HibernateQueryResultDataSource listaDS = null;
      HashMap peticion = new HashMap();

      try {
         peticion.put("dbConeccion", dbc);
         peticion.put("consulta", consulta);
         peticion.put("entidadNombre", entidadNombre);
         List listaEntidad = (List)(new ObtenerListaEntidad()).ejecutarAccion(peticion);
         listaDS = new HibernateQueryResultDataSource(listaEntidad);
      } catch (Exception var6) {
         var6.printStackTrace();
      }

      return listaDS;
   }

   public static BigDecimal obtenerPorcentajeProduccion(DBConeccion dbc, BigDecimal usucodigo, Date fecelaboracion) {
      HashMap peticion = new HashMap();

      try {
         String fecha = String.valueOf(fecelaboracion.getYear() + 1900 + "-" + (fecelaboracion.getMonth() + 1) + "-" + fecelaboracion.getDate());
         String sql = "select * from produccion where produccion.usucodigo=" + usucodigo + " and produccion.fecelaboracion='" + fecha + "'";
         peticion.put("consulta", sql);
         peticion.put("entidadNombre", "Produccion");
         peticion.put("dbConeccion", dbc);
         List listaProduccion = (List)(new ObtenerListaEntidad()).ejecutarAccion(peticion);
         new BigDecimal(0);
         if (listaProduccion != null && listaProduccion.size() > 0) {
            Produccion produccion = (Produccion)listaProduccion.get(0);
            Util.getInst();
            return Util.getBigDecimalFormatted(produccion.getProduccion(), 2);
         }
      } catch (Exception var9) {
         var9.printStackTrace();
      }

      return new BigDecimal(0);
   }

   public static BigDecimal getValorComponenteTalla(BigDecimal cmpcodigo, String prd_des, List listaValores) {
      BigDecimal valor = new BigDecimal(0.0D);

      try {
         for(int ite = 0; ite < listaValores.size(); ++ite) {
            Cmpproducto cmpPrd = (Cmpproducto)listaValores.get(ite);
            if (cmpPrd.getCmpcodigo().equals(cmpcodigo) && cmpPrd.getPrd_des().equals(prd_des)) {
               valor = Util.getBigDecimalFormatted(cmpPrd.getValor(), 2);
               System.out.println("Componente : " + cmpcodigo + " talla : " + prd_des);
               break;
            }
         }
      } catch (Exception var6) {
         var6.printStackTrace();
      }

      return valor;
   }

   public static BigDecimal obtenerTotalProduccion(DBConeccion dbc, Date fecelaboracion) {
      HashMap peticion = new HashMap();

      try {
         String fecha = String.valueOf(fecelaboracion.getYear() + 1900 + "-" + (fecelaboracion.getMonth() + 1) + "-" + fecelaboracion.getDate());
         String sql = "select sum(produccion) total from produccion where produccion.fecelaboracion='" + fecha + "' and usucodigo<>0";
         String[] listaColumnaTipo = new String[]{"BIGDECIMAL"};
         String[] listaColumnaAlias = new String[]{"total"};
         peticion.put("respuesta", "LISTAMIXTA");
         peticion.put("consulta", sql);
         peticion.put("listaColumnaTipo", listaColumnaTipo);
         peticion.put("listaColumnaAlias", listaColumnaAlias);
         List listaResultado = dbc.ejecutarConsulta(peticion);
         new BigDecimal(0);
         if (listaResultado != null && listaResultado.size() > 0) {
            Object[] campo = (Object[])((Object[])listaResultado.get(0));
            if (campo[0] instanceof BigDecimal) {
               Util.getInst();
               return Util.getBigDecimalFormatted(campo[0], 2);
            }
         }
      } catch (Exception var10) {
         var10.printStackTrace();
      }

      return new BigDecimal(0);
   }

   public static Image obtenerImagen(String codigoBarra) {
      if (codigoBarra.trim().length() == 8) {
         codigoBarra = codigoBarra + "0000";
      }

      CodigoBarras codigoBarras = new CodigoBarras();
      return codigoBarras.obtenerImageIcon(codigoBarra).getImage();
   }
}
