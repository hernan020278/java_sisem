package com.sis.consulta.accion;

import java.util.List;
import java.util.Map;

import smile.Network;
import smile.SMILEException;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Indicador;
import com.comun.entidad.RegistroConsulta;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class EstablecerEvidencias extends Accion 
{
	public Object ejecutarAccion(Map peticion) throws Exception 
	{
		Object result = null;
		try {
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String valEstInd = "";
			List listaRegistroConsulta = (List) peticion.get("listaRegistroConsulta");
			
			Network net = new Network();
			net.readFile("C:\\sisem\\bin\\RedPersonalidad.xdsl");

			// ESTABLECE EL ESTADO DE SINTOMAS DETECTADOS EN LA CONSULTA
			for (int iteTrasRco = 0; iteTrasRco < listaRegistroConsulta.size(); iteTrasRco++) 
			{
				RegistroConsulta rco = (RegistroConsulta) listaRegistroConsulta.get(iteTrasRco);
				net.setEvidence(rco.getIndcodigo(), Estado.toValor(rco.getEstado()).toUpperCase());
				net.updateBeliefs();
			}
			peticion.put("ESTADO_DIAGNOSTICO", net.getOutcomeIds("DG_RES"));
			peticion.put("VALOR_DIAGNOSTICO", net.getNodeValue("DG_RES"));
			
			/**
			 * OBTENER LA LISTA DE TRASTORNOS PARA EL DIAGNOSTICO
			 */
			peticion.put("consulta", "select * from indicador where tipo='TRASTORNO' order by tipo");
			peticion.put("entidadNombre", "Indicador");
			List listaTrastornos = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

			for (int iteTras = 0; iteTras < listaTrastornos.size(); iteTras++) 
			{
				Indicador indTras = (Indicador) listaTrastornos.get(iteTras);
				peticion.put("ESTADO_" + indTras.getIndcodigo(), net.getOutcomeIds(indTras.getIndcodigo()));
				peticion.put("VALOR_" + indTras.getIndcodigo(), net.getNodeValue(indTras.getIndcodigo()));
				
			}// Fin de recorrer los nodos de los trastornos
			
			peticion.put("listaTrastornos", listaTrastornos);
			this.setEstado(dbc.getEstado());
		}
		catch (SMILEException e) {
			System.out.println(e.getMessage());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}