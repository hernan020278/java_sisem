package com.sis.consulta.accion;

import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Historia;
import com.comun.entidad.Mensaje;
import com.comun.entidad.Muestra;
import com.comun.entidad.RegistroCaso;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.usuario.accion.ObtenerCasoPaciente;

public class EliminarCaso extends Accion
{
	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			peticion.put("consulta", "select * from muestra where muecodigo = 'MUE001'");
			peticion.put("entidadNombre", "Muestra");
			Muestra muestra = (Muestra) (new ObtenerEntidad()).ejecutarAccion(peticion);
			peticion.put("muestra", muestra);

			peticion.put("Usuario_usucodigo", peticion.get("Pagina_valor"));
			(new ObtenerCasoPaciente()).ejecutarAccion(peticion);

			Usuario pac = (Usuario) peticion.get("paciente");
			Historia his = (Historia) peticion.get("historia");
			List lisRgc = (List) peticion.get("listaRegistroCaso");
			Muestra mue = (Muestra) peticion.get("muestra");

			pac.setEstado(Estado.DELETED);

			peticion.put("tipoSql", TipoSql.UPDATE);
			peticion.put("entidadObjeto", pac);
			peticion.put("where", ("where usucodigo='" + pac.getUsucodigo() + "'"));
			afectados = dbc.ejecutarCommit(peticion);

			his.setEstado(Estado.DELETED);
			peticion.put("tipoSql", TipoSql.UPDATE);
			peticion.put("entidadObjeto", his);
			peticion.put("where", ("where hiscodigo='" + his.getHiscodigo() + "'"));
			afectados = dbc.ejecutarCommit(peticion);

			for (int iteRgc = 0; iteRgc < lisRgc.size(); iteRgc++)
			{
				RegistroCaso rgc = (RegistroCaso) lisRgc.get(iteRgc);
				rgc.setEstado(Estado.DELETED);
				peticion.put("tipoSql", TipoSql.UPDATE);
				peticion.put("entidadObjeto", rgc);
				peticion.put("where", ("where rcacodigo='" + rgc.getRcacodigo() + "'"));
				afectados = dbc.ejecutarCommit(peticion);
			}

			mue.setCantidad(mue.getCantidad() - 1);
			peticion.put("tipoSql", TipoSql.UPDATE);
			peticion.put("entidadObjeto", mue);
			peticion.put("where", "where muecodigo='" + mue.getMuecodigo() + "'");
			afectados = dbc.ejecutarCommit(peticion);

			if (afectados > 0)
			{
				dbc.commit();

				peticion.put("Mensaje_mostrar", "true");
				peticion.put("Mensaje_titulo", "Informacion de Almacenamiento");
				peticion.put("Mensaje_tipo", "info");
				peticion.put("Mensaje_valor", "Los datos se han borrado satisfactoriamente");
			}// Fin de if(bookMarkDevCod != 0)
			else
			{
				dbc.rollback();

				peticion.put("Mensaje_mostrar", "true");
				peticion.put("Mensaje_titulo", "Informacion de Almacenamiento");
				peticion.put("Mensaje_tipo", "error");
				peticion.put("Mensaje_valor", "Los datos se han borrado satisfactoriamente");
			}
			result = peticion;
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return result;
	}
}