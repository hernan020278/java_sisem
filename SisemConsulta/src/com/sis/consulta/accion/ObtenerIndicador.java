package com.sis.consulta.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.entidad.Indicador;
import com.comun.entidad.Red;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerIndicador extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			String indcodigo = (String) peticion.get("Indicador_indcodigo");

			peticion.put("consulta", "select * from indicador where indcodigo='" + indcodigo + "'");
			peticion.put("entidadNombre", "Indicador");
			Indicador indicador = (Indicador) (new ObtenerEntidad()).ejecutarAccion(peticion);
			peticion.put("indicador", indicador);

			peticion.put("consulta", "select * from indestado where indcodigo='" + indcodigo + "'");
			peticion.put("entidadNombre", "Indestado");
			List listaIndestado = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaIndestado", listaIndestado);
			/*
			 * 
			 * OBTENER LISTA DE REFERENCIAS SI ES TRASTORNO O SINTOMA
			 */
			String consulta = "";
			if (indicador.getTipo().equals("TRASTORNO"))
			{
				consulta = "select * from red where tracodigo = '" + indicador.getIndcodigo() + "'";
			}
			else if (indicador.getTipo().equals("SINTOMA"))
			{
				consulta = "select * from red where sincodigo = '" + indicador.getIndcodigo() + "'";
			}
			peticion.put("consulta", consulta);
			peticion.put("entidadNombre", "Red");
			List listaRed = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaRed", listaRed);

			List listaReferencia = new ArrayList();
			for (int iteRed = 0; iteRed < listaRed.size(); iteRed++)
			{
				Red red = (Red) listaRed.get(iteRed);
				String sql = "select * from indicador where indcodigo = '" + ((indicador.getTipo().equals("TRASTORNO")) ? red.getSincodigo() : red.getTracodigo()) + "'";
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Indicador");
				Indicador referencia = (Indicador) (new ObtenerEntidad()).ejecutarAccion(peticion);
				listaReferencia.add(referencia);
			}
			peticion.put("listaReferencia", listaReferencia);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}