package com.sis.consulta.accion;

import java.util.Map;

import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class AgregarCaso extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
//			System.out.println("preparamos para agregar psicologo");
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}