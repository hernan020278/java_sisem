package com.sis.consulta.accion;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.comun.motor.Accion;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.referencia.Estado;

public class GenerarArchivoCasos extends Accion
{
	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			String organizacionIde = (String) peticion.get("organizacionIde");
			String ruta = DiccionarioGeneral.getInstance("host", organizacionIde).getPropiedad("extract-ruta", "\\xml\\extracts\\");
			if (!ruta.endsWith("\\") && !ruta.endsWith("//"))
			{
				ruta = ruta + "\\";
			}

			String fileName = "RedPersonalidad.txt";

			File dir = new File(ruta);
			if (!dir.isDirectory()) {
				throw new IOException("The Voucher Extract Directory is not a valid directory.");
			}
			if (!dir.canWrite()) {
				throw new IOException("The batch file cannot be created because the application does not have write access for the Voucher Extract Directory.");
			}

			if (ruta.charAt(ruta.length() - 1) != File.separatorChar) {
				ruta = ruta + File.separator;
			}

			File extractFile = new File(dir + File.separator + fileName);
			extractFile.delete();
			if (!extractFile.exists())
			{
				if (!extractFile.createNewFile())
				{
					throw new IOException("The extract file could not be created.");
				}
			}
			result = extractFile;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e) {
			throw e;
		}

		return result;
	}
}