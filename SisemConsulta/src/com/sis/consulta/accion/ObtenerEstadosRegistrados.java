package com.sis.consulta.accion;

import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Indicador;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;

public class ObtenerEstadosRegistrados extends Accion
{
	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String indcodigo = (String) peticion.get("Indicador_indcodigo");
			
			String sql = "select registrocaso.indcodigo indicador, registrocaso.estado estado " +
					"from usuario inner join historia on historia.paccodigo=usuario.usucodigo " +
					"inner join registrocaso on registrocaso.paccodigo=historia.paccodigo and registrocaso.hiscodigo=historia.hiscodigo " +
					"inner join muestra on muestra.muecodigo=registrocaso.muecodigo " +
					"where usuario.estado <> '0000' and usuario.cargo='PACIENTE' and registrocaso.indcodigo='" + indcodigo + "' " +
					"group by registrocaso.indcodigo, registrocaso.estado ";			
			
			String[] listaComlumnaTipo = { "STRING", "STRING" };
			String[] listaColumnaAlias = { "indicador", "estado" };

			peticion.put("consulta", sql);
			peticion.put("respuesta", Respuesta.LISTAMIXTA);
			peticion.put("listaColumnaTipo", listaComlumnaTipo);
			peticion.put("listaColumnaAlias", listaColumnaAlias);
			
			List listaEstados = dbc.ejecutarConsulta(peticion);

			String[] estados = new String[listaEstados.size()];
			for(int iteEst = 0; iteEst < listaEstados.size(); iteEst++)
			{
				Object[] listaColumna = (Object[])listaEstados.get(iteEst);
				estados[iteEst] = (String) listaColumna[1];
			}
			result = estados;
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e) {
			throw e;
		}

		return result;
	}
}