package com.sis.consulta.accion;

import java.util.List;
import java.util.Map;

import smile.SMILEException;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Indicador;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.SistemaExperto;

public class ElaborarDatosEstadisticos extends Accion 
{
	public Object ejecutarAccion(Map peticion) throws Exception 
	{
		Object result = null;
		try 
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			/**
			 * OBTENER LA LISTA DE TRASTORNOS PARA EL DIAGNOSTICO
			 */
			int totalPacientes = (Integer) (new ObtenerTotalPacientes()).ejecutarAccion(peticion);

			String[] resEst;
			String sql = "";

			/**
			 * OBTENER LA LISTA DE TRASTORNOS PARA EL DIAGNOSTICO
			 */
			sql = "select * from indicador where tipo='TRASTORNO' order by tipo";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Indicador");
			List listaTrastornos = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

			for(int iteLisTra =0 ; iteLisTra< listaTrastornos.size(); iteLisTra++)
			{
				Indicador trastorno = (Indicador)listaTrastornos.get(iteLisTra);
			
				String[] estado = SistemaExperto.obtenerEstadosNodo(trastorno.getIndcodigo());
				double[] valor = SistemaExperto.obtenerValoresNodo(trastorno.getIndcodigo());

				peticion.put("ESTADO_" + trastorno.getIndcodigo(), estado);
				peticion.put("VALOR_" + trastorno.getIndcodigo(), valor);
			}
			
			String[] estado = SistemaExperto.obtenerEstadosNodo("DG_RES");
			double[] valor = SistemaExperto.obtenerValoresNodo("DG_RES");

			peticion.put("ESTADO_" + "DG_RES", estado);
			peticion.put("VALOR_" + "DG_RES", valor);
			peticion.put("listaTrastornos", listaTrastornos);

			this.setEstado(dbc.getEstado());
		}
		catch (SMILEException e) {
			System.out.println(e.getMessage());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}