package com.sis.consulta.accion;

import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.usuario.accion.ObtenerCasoPaciente;

public class ModificarCaso extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			peticion.put("Usuario_usucodigo", (String) peticion.get("Pagina_valor"));
			(new ObtenerCasoPaciente()).ejecutarAccion(peticion);
			/**
			 * Obtener listas de registros
			 */
			peticion.put("consulta", "select * from indicador order by tipo");
			peticion.put("entidadNombre", "Indicador");
			List listaIndicador = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaIndicador", listaIndicador);

			peticion.put("consulta", "select * from red order by tracodigo");
			peticion.put("entidadNombre", "Red");
			List listaRed = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaRed", listaRed);

			peticion.put("consulta", "select * from indestado order by indcodigo");
			peticion.put("entidadNombre", "Indestado");
			List listaIndestado = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listIndestado", listaIndestado);

			result = peticion;

			this.setEstado(Estado.SUCCEEDED);

		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}