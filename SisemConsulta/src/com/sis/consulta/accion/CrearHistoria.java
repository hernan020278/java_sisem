package com.sis.consulta.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Historia;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class CrearHistoria extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			Usuario paciente = (Usuario) peticion.get("paciente");

			Historia his = new Historia();
			his.setHiscodigo(new BigDecimal(KeyGenerator.getInst().obtenerLongCodigo()));
			his.setPaccodigo(paciente.getUsucodigo());
			his.setFecinscripcion(Util.getInst().getFechaSql());
			his.setDiagnostico("SIN DIAGNOSTICO");
			his.setEstado(Estado.REGISTRADO);
			his.setVersion(new BigDecimal(1));
			
			result = his;

			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}