package com.sis.consulta.accion;

import java.util.List;
import java.util.Map;

import com.comun.entidad.RegistroCaso;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Util;

public class ObtenerEstadoIndicador extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			String estado = "";
			String estadoImagen = "";
			List listaRegistroCaso = (List) peticion.get("listaRegistroCaso");
			String tamano = (String) peticion.get("tamano");
			String indicador = (String) peticion.get("indicador");
			
			RegistroCaso rgc = (RegistroCaso) Util.getInst().obtenerItemLista(listaRegistroCaso, "RegistroCaso", "indcodigo", indicador);

			if (!rgc.getIndcodigo().equals(""))
			{
				estado = rgc.getEstado();
				if (Estado.comparar(estado, Estado.ALTA) || Estado.comparar(estado, Estado.PRESENTE))
				{
					estadoImagen = "alta_" + tamano + ".png";
				}
				else if (Estado.comparar(estado, Estado.NORMAL))
				{
					estadoImagen = "normal_" + tamano + ".png";
				}
				else if (Estado.comparar(estado, Estado.BAJA) || Estado.comparar(estado, Estado.AUSENTE))
				{
					estadoImagen = "baja_" + tamano + ".png";
				}
				else
				{
					estadoImagen = "pregunta_" + tamano + ".png";
				}
			}
			else
			{
				estadoImagen = "pregunta_" + tamano + ".png";
			}
			
			return estadoImagen;
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return result;
	}
}