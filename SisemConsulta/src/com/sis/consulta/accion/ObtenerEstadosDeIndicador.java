package com.sis.consulta.accion;

import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerEstadosDeIndicador extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			if (!peticion.containsKey("consulta"))
			{
				peticion.put("consulta", "select * from indestado order by indcodigo");
			}
			peticion.put("entidadNombre", "Indestado");
			List listaIndEstados = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			result = listaIndEstados;
			return result;
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return result;
	}
}