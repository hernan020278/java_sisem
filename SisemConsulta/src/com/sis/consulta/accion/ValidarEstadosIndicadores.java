package com.sis.consulta.accion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.comun.entidad.Indestado;
import com.comun.entidad.Indicador;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ValidarEstadosIndicadores extends Accion
{
	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			List listaIndicador = (List) peticion.get("listaIndicador");
			boolean indicadorValidado = true;
			
			Map estadosIndicadorRegistrados = new HashMap();
			
			for (int iteInd = 0; iteInd < listaIndicador.size(); iteInd++)
			{
				Indicador ind = (Indicador) listaIndicador.get(iteInd);
//				Util.getInstance().imprimiValores(ind);
				peticion.put("Indicador_indcodigo", ind.getIndcodigo());
				String[] listaEstadosRegistrados = (String[]) (new ObtenerEstadosRegistrados()).ejecutarAccion(peticion);

				peticion.put("consulta", "select * from indestado where indcodigo = '" + ind.getIndcodigo() + "'");
				List listaEstadosIndicador = (List) (new ObtenerEstadosDeIndicador()).ejecutarAccion(peticion);

				String arrayEstadosIndicador = "";
				for(int iteIndEst = 0 ; iteIndEst < listaEstadosIndicador.size(); iteIndEst++)
				{
					Indestado indEst = (Indestado) listaEstadosIndicador.get(iteIndEst);
					arrayEstadosIndicador = arrayEstadosIndicador + ((arrayEstadosIndicador.equals(""))? "": "-") + indEst.getNombre();
				}
				
				String arrayEstadosRegistrados ="";
				for(int iteEstReg = 0; iteEstReg < listaEstadosRegistrados.length; iteEstReg++)
				{
					String estadoRegistrado = Estado.toValor(((String) listaEstadosRegistrados[iteEstReg])).toUpperCase();
					arrayEstadosRegistrados = arrayEstadosRegistrados + ((arrayEstadosRegistrados.equals(""))? "": "-") + estadoRegistrado;
				}
				
				if(arrayEstadosIndicador.length() > arrayEstadosRegistrados.length())
				{
					indicadorValidado = false;
				}
				estadosIndicadorRegistrados.put(ind.getNombre(), arrayEstadosIndicador + "#" + arrayEstadosRegistrados + " : " + ind.getIndcodigo());
			}
			peticion.put("indicadorValidado", indicadorValidado);
			peticion.put("estadosIndicadorRegistrados", estadosIndicadorRegistrados);
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e) 
		{
			throw e;
		}

		return result;
	}
}