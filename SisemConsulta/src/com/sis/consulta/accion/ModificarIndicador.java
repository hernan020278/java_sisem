package com.sis.consulta.accion;

import java.util.Map;

import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ModificarIndicador extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			peticion.put("Indicador_indcodigo", (String) peticion.get("Pagina_valor"));

			(new ObtenerIndicador()).ejecutarAccion(peticion);

			result = peticion;

			this.setEstado(Estado.SUCCEEDED);

		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}