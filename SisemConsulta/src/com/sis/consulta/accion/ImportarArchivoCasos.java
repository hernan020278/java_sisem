package com.sis.consulta.accion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.RegistroCaso;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Log;

public class ImportarArchivoCasos extends Accion {

	private File fileFuente = null;
	private BufferedReader br = null;

	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			String msg = "";
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String organizacionIde = (String) peticion.get("organizacionIde");
			List listaUsuario = (List) peticion.get("listaUsuario");
			List listaRegistroCaso = (List) peticion.get("listaRegistroCaso");

			fileFuente = (File) peticion.get("fileFuente");

			FileReader fw = new FileReader(fileFuente);
			br = new BufferedReader(fw);
			String sCadena = "";

			String[] arrayIndicadorCodigo = null;
			arrayIndicadorCodigo = br.readLine().split(" ");
			// System.out.println("Encabezados : " + arrayIndicadorCodigo.toString());
			String[] arrayIndicadorValor = null;
			List<String> listaRegistros = new ArrayList();
			while ((sCadena = br.readLine()) != null)
			{
				listaRegistros.add(sCadena);
			}

			for (int iteUsu = 0; iteUsu < listaRegistros.size(); iteUsu++)
			{
				Usuario usu = (Usuario) listaUsuario.get(iteUsu);
				arrayIndicadorValor = listaRegistros.get(iteUsu).split(" ");
				// System.out.print("Registro " + iteUsu + " :");
				for (int iteInd = 0; iteInd < arrayIndicadorCodigo.length; iteInd++)
				{
					String indicadorCodigo = arrayIndicadorCodigo[iteInd];
					String indicadorValor = arrayIndicadorValor[iteInd];

					for (int iteRca = 0; iteRca < listaRegistroCaso.size(); iteRca++)
					{
						RegistroCaso rca = (RegistroCaso) listaRegistroCaso.get(iteRca);
						if (usu.getUsucodigo().equals(rca.getPaccodigo()) && indicadorCodigo.equals(rca.getIndcodigo()))
						{
							rca.setEstado(Estado.toClave(indicadorValor));
							// System.out.print(" " + Estado.toEstado(indicadorValor));
							peticion.put("tipoSql", TipoSql.UPDATE);
							peticion.put("entidadObjeto", rca);
							peticion.put("where", ("where rcacodigo='" + rca.getRcacodigo() + "'"));
							afectados = dbc.ejecutarCommit(peticion);
							if (afectados == 0)
							{
								dbc.commit();
								msg = "Datos guardados satisfactoriamente!!!";
							}
							else
							{
								dbc.rollback();
								msg = "Error al importar datos!!!";
							}
							break;
						}
					}
				}
				// System.out.println("");
			}// for (int iteUsu = 0; iteUsu < listaUsuario.size(); iteUsu++)
			aux.msg.setMostrar(true);
			aux.msg.setTipo("info");
			aux.msg.setTitulo("Almacenamiento");
			aux.msg.setValor("Los datos se han guardado satisfactoriamente");
		}
		catch (Exception e)
		{
			// TODO: handle exception
		}
		finally {
			if (br != null) {
				try {
					br.close();
				}
				catch (IOException e) {
					Log.error(this, e.getMessage());
				}
			}
		}

		this.setEstado(Estado.SUCCEEDED);
		return null;
	}
}