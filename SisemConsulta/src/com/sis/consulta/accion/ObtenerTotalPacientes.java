package com.sis.consulta.accion;

import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.utilidad.Util;

public class ObtenerTotalPacientes extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String[] tipos = {"INTEGER"};
			String[] alias = {"total"};
			
			peticion.put("listaColumnaTipo", tipos);
			peticion.put("listaColumnaAlias", alias);
			
			if(!peticion.containsKey("consulta") || Util.isEmpty((String)peticion.get("consulta")))
			{
				String sql = "select count(*) as total from ( " +
						"select usuario.usucodigo " +
						"from usuario inner join historia on historia.paccodigo=usuario.usucodigo " +
						"inner join registrocaso on registrocaso.paccodigo=historia.paccodigo and registrocaso.hiscodigo=historia.hiscodigo " +
						"inner join muestra on muestra.muecodigo=registrocaso.muecodigo " +
						"where usuario.estado <> '0000' and usuario.cargo='PACIENTE' " +
						"group by usuario.usucodigo " +
						") as resultado";
				peticion.put("consulta", sql);
			}
			peticion.put("respuesta", Respuesta.LISTAMIXTA);

			List listaResultado = dbc.ejecutarConsulta(peticion);
			
			if(listaResultado != null && listaResultado.size() > 0)
			{
				Object[] listaCampos = (Object[]) listaResultado.get(0);
				result = (Integer) listaCampos[0];
			}
			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}