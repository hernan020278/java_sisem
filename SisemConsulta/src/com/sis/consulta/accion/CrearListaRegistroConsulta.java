package com.sis.consulta.accion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Indicador;
import com.comun.entidad.Muestra;
import com.comun.entidad.RegistroConsulta;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.KeyGenerator;

public class CrearListaRegistroConsulta extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			Muestra muestra = (Muestra) peticion.get("muestra");
			Usuario usuario =(Usuario) peticion.get("usuario");
			
			List lisSin = (List) peticion.get("listaSintomas");
			List lisRco = new ArrayList();
			for (int itelisSin = 0; itelisSin < lisSin.size(); itelisSin++)
			{
				Indicador iteSin = (Indicador) lisSin.get(itelisSin);
				RegistroConsulta rco = new RegistroConsulta();
				rco.setRcocodigo(new BigDecimal(KeyGenerator.getInst().obtenerLongCodigo()));
				rco.setIndcodigo(iteSin.getIndcodigo());
				rco.setMuecodigo(muestra.getMuecodigo());
				rco.setPaccodigo(usuario.getUsucodigo());
				rco.setEstado((iteSin.getTipo().equals("TRASTORNO")) ? Estado.NORMAL : ((iteSin.getTipo().equals("SINTOMA")) ? Estado.AUSENTE : Estado.NORMAL));
				rco.setDescripcion("SIN DESCRIPCION");
				rco.setPeso(0);
				rco.setVersion(1);
				lisRco.add(rco);
			}
			
			result = lisRco;

			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}