package com.sis.consulta.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Indestado;
import com.comun.entidad.Indicador;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerListaSintoma extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String indcodigo = (String) peticion.get("Indicador_indcodigo");
			List listaIndicador = (List) peticion.get("listaIndicador");
			List listaIndestado = (List) peticion.get("listaIndestado");
			List listaSintoma = new ArrayList();
			for (int iteInd = 0; iteInd < listaIndicador.size(); iteInd++)
			{
				Indicador indicador = (Indicador) listaIndicador.get(iteInd);
				for (int iteIde = 0; iteIde < listaIndestado.size(); iteIde++)
				{
					Indestado indestado = (Indestado) listaIndestado.get(iteIde);
					if (indicador.getIndcodigo().equals(indcodigo) && indestado.getIndcodigo().equals(indcodigo))
					{
						listaSintoma.add(indicador);
					}
				}
			}

			result = listaSintoma;

			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}