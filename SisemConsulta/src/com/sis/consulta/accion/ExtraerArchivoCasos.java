package com.sis.consulta.accion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.comun.entidad.Indicador;
import com.comun.entidad.RegistroCaso;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;

public class ExtraerArchivoCasos extends Accion {

	private File extractFile = null;
	private BufferedWriter bw = null;

	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;

		try
		{
			String organizacionIde = (String) peticion.get("organizacionIde");
			List listaUsuario = (List) peticion.get("listaUsuario");
			List listaIndicador = (List) peticion.get("listaIndicador");
			List listaRegistroCaso = (List) peticion.get("listaRegistroCaso");

			extractFile = (File) peticion.get("extractFile");
			String lineaEncabezados = "";
			String lineaDetalles = "";

			FileWriter fw = new FileWriter(extractFile, true);
			bw = new BufferedWriter(fw);

			for (int iteInd = 0; iteInd < listaIndicador.size(); iteInd++)
			{
				Indicador ind = (Indicador) listaIndicador.get(iteInd);
				lineaEncabezados = lineaEncabezados + (lineaEncabezados.equals("")?"":" ") + ind.getIndcodigo();
			}
			System.out.println(lineaEncabezados);
			bw.write(lineaEncabezados, 0, lineaEncabezados.length());
			bw.newLine();

			for (int iteUsu = 0; iteUsu < listaUsuario.size(); iteUsu++)
			{
				Usuario usu = (Usuario) listaUsuario.get(iteUsu);
				lineaDetalles = "";
				for (int iteInd = 0; iteInd < listaIndicador.size(); iteInd++)
				{
					Indicador ind = (Indicador) listaIndicador.get(iteInd);
					String estado = "";
					for (int iteRca = 0; iteRca < listaRegistroCaso.size(); iteRca++)
					{
						RegistroCaso rca = (RegistroCaso) listaRegistroCaso.get(iteRca);
						if (usu.getUsucodigo().equals(rca.getPaccodigo()) && ind.getIndcodigo().equals(rca.getIndcodigo()))
						{
							estado = Estado.toValor(rca.getEstado()).toUpperCase();
							break;
						}
//						System.out.print("Registro : " + iteRca + " - ");
//						Util.getInstance().imprimiValores(rca);
					}
					lineaDetalles = lineaDetalles + (lineaDetalles.equals("")?"":" ")  + estado;
//					System.out.print("Registro : " + iteInd + " - ");
//					Util.getInstance().imprimiValores(ind);
				}
//				System.out.print("Registro : " + iteUsu + " - ");
				Util.getInst().imprimiValores(usu);
				bw.write(lineaDetalles, 0, lineaDetalles.length());
				bw.newLine();
			}//for (int iteUsu = 0; iteUsu < listaUsuario.size(); iteUsu++)
		}
		catch (Exception e)
		{
			// TODO: handle exception
		}
		finally {
			if (bw != null) {
				try {
					bw.close();
				}
				catch (IOException e) {
					Log.error(this, e.getMessage());
				}
			}
		}

		this.setEstado(Estado.SUCCEEDED);
		return null;
	}
}