package com.sis.consulta.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Historia;
import com.comun.entidad.RegistroCaso;
import com.comun.entidad.RegistroConsulta;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerDiagnostico extends Accion 
{
	public Object ejecutarAccion(Map peticion) throws Exception 
	{
		Object result = null;
		try 
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			List listaRegistroConsulta = (List) peticion.get("listaRegistroConsulta");

			peticion.put("consulta", "select * from usuario where cargo = 'PACIENTE' and estado <> '0000' order by nombre");
			peticion.put("entidadNombre", "Usuario");
			List listaUsuarios = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

			int mayorCoincidencia = 0;
			List listaDiagnostico = new ArrayList();
			List listaCoincidencias = new ArrayList();

			for (int iteLisUsu = 0; iteLisUsu < listaUsuarios.size(); iteLisUsu++)
			{
				Usuario usuario = (Usuario) listaUsuarios.get(iteLisUsu);

				peticion.put("consulta", "select * from historia where paccodigo = '" + usuario.getUsucodigo() + "'");
				peticion.put("entidadNombre", "Historia");
				Historia historia = (Historia) (new ObtenerEntidad()).ejecutarAccion(peticion);

				String sql = "select registrocaso.*  from usuario inner join registrocaso "
						+ "on usuario.usucodigo = registrocaso.paccodigo "
						+ "where registrocaso.estado <> '0000' and usuario.estado<>'0000' and usuario.cargo='PACIENTE' and paccodigo='" + usuario.getUsucodigo() + "'";
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "RegistroCaso");
				List listaRegistroCaso = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

				int numeroCoincidencias = 0;

				for (int iteLisRca = 0; iteLisRca < listaRegistroCaso.size(); iteLisRca++)
				{
					RegistroCaso registroCaso = (RegistroCaso) listaRegistroCaso.get(iteLisRca);

					for (int itelisRco = 0; itelisRco < listaRegistroConsulta.size(); itelisRco++)
					{
						RegistroConsulta registroConsulta = (RegistroConsulta) listaRegistroConsulta.get(itelisRco);

						if (registroCaso.getIndcodigo().equals(registroConsulta.getIndcodigo()) && registroCaso.getEstado().equals(registroConsulta.getEstado()))
						{
							numeroCoincidencias++;
						}
					}
				}
				if (numeroCoincidencias > mayorCoincidencia)
				{
					mayorCoincidencia = numeroCoincidencias;
				}
				if(!listaCoincidencias.contains(numeroCoincidencias))
				{
					listaDiagnostico.add(usuario.getNombre() + "#" + historia.getDiagnostico() + "#" + numeroCoincidencias);
					listaCoincidencias.add(numeroCoincidencias);
				}
			}
			
			String[] listaOrdenada = new String[listaDiagnostico.size()];
			for(int ite =0 ; ite < listaOrdenada.length; ite++)
			{
				listaOrdenada[ite] = (String)listaDiagnostico.get(ite);
			}
			
			for (int indA = 0; indA < listaOrdenada.length; indA++) 
			{
				int indMay =0;
				int indMen =0;
				String[] arrLisA = listaOrdenada[indA].split("#");
				int valorA = Integer.parseInt(arrLisA[2]);
				for (int indB =indA; indB < listaOrdenada.length; indB++) 
				{
					String[] arrLisB = listaOrdenada[indB].split("#");
					int valorB = Integer.parseInt(arrLisB[2]);
					
					if (valorA < valorB) 
					{
						indMen = indA;
						indMay = indB;
						
						String auxMen = listaOrdenada[indMen];
						String auxMay = listaOrdenada[indMay];
						
						listaOrdenada[indMen]= auxMay;
						listaOrdenada[indMay]= auxMen;
						break;
					}
				}
			}			
			peticion.put("listaDiagnostico", listaDiagnostico);
			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}