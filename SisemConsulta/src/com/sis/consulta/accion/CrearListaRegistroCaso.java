package com.sis.consulta.accion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Historia;
import com.comun.entidad.Indicador;
import com.comun.entidad.Muestra;
import com.comun.entidad.RegistroCaso;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.KeyGenerator;

public class CrearListaRegistroCaso extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			Muestra muestra = (Muestra) peticion.get("muestra");
			Historia historia =(Historia) peticion.get("historia");
			
			List lisInd = (List) peticion.get("lisInd");
			List lisRgc = new ArrayList();
			for (int iteLisInd = 0; iteLisInd < lisInd.size(); iteLisInd++)
			{
				Indicador iteInd = (Indicador) lisInd.get(iteLisInd);
				RegistroCaso rgc = new RegistroCaso();
				rgc.setRcacodigo(new BigDecimal(KeyGenerator.getInst().obtenerLongCodigo()));
				rgc.setIndcodigo(iteInd.getIndcodigo());
				rgc.setMuecodigo(muestra.getMuecodigo());
				rgc.setHiscodigo(historia.getHiscodigo());
				rgc.setPaccodigo(historia.getPaccodigo());
				rgc.setEstado((iteInd.getTipo().equals("TRASTORNO")) ? Estado.NORMAL : ((iteInd.getTipo().equals("SINTOMA")) ? Estado.AUSENTE : Estado.NORMAL));
				rgc.setDescripcion("SIN DESCRIPCION");
				rgc.setPeso(0);
				rgc.setVersion(1);
				lisRgc.add(rgc);
			}
			
			result = lisRgc;

			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}