package com.sis.consulta.accion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Historia;
import com.comun.entidad.Indicador;
import com.comun.entidad.Muestra;
import com.comun.entidad.RegistroCaso;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class CrearListaRegistroCasoPorUsuario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			Muestra muestra = aux.mue;
			
			peticion.put("consulta", "select * from usuario where usucodigo >= 235434541675 and usucodigo <= 235434541874");
			peticion.put("entidadNombre", "Usuario");
			List listaUsuarios = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			
			peticion.put("consulta", "select * from indicador order by tipo");
			peticion.put("entidadNombre", "Indicador");
			List listaIndicador = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

			
			for(int iteLis = 0; iteLis < listaUsuarios.size(); iteLis++)
			{
				Usuario usuario = (Usuario) listaUsuarios.get(iteLis);

				String sql = "select * from historia where paccodigo>="  + usuario.getUsucodigo();
				peticion.put("consulta", "select * from historia where paccodigo="  + usuario.getUsucodigo());
				peticion.put("entidadNombre", "Historia");
				Historia historia = (Historia) (new ObtenerEntidad()).ejecutarAccion(peticion);
				
				for (int iteLisInd = 0; iteLisInd < listaIndicador.size(); iteLisInd++)
				{
//					Thread.sleep(500);
					Indicador iteInd = (Indicador) listaIndicador.get(iteLisInd);
					RegistroCaso rgc = new RegistroCaso();
					rgc.setRcacodigo(new BigDecimal(KeyGenerator.getInst().obtenerCodigoBarra()));
					rgc.setIndcodigo(iteInd.getIndcodigo());
					rgc.setMuecodigo(muestra.getMuecodigo());
					rgc.setHiscodigo(historia.getHiscodigo());
					rgc.setPaccodigo(historia.getPaccodigo());
					rgc.setEstado(obtenerEstadoAleatorio(iteInd.getTipo()));
					rgc.setDescripcion("SIN DESCRIPCION");
					rgc.setPeso(0);
					rgc.setVersion(1);
					
					Util.getInst().imprimiValores(rgc);
					
					peticion.put("tipoSql", TipoSql.INSERT);
					peticion.put("entidadObjeto", rgc);
					afectados = dbc.ejecutarCommit(peticion);
					if(afectados > 0 )
					{
						dbc.commit();
					}
					else
					{
						System.out.println("Error al guardar datos");
						dbc.rollback();
						break;
					}
				}
			}
			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
	
	public String obtenerEstadoAleatorio(String tipo)
	{
		String resultado = "";
		String[] estadoTrastorno = {"","ALTA","BAJA","NORMAL"};
		String[] estadoSintoma = {"","PRESENTE","AUSENTE"};
		String[] estadoDiagnostico = {"","HOSPITALIZACION","NORMAL","TRATAMIENTO"};
		
		if(tipo.equals("TRASTORNO"))
		{
			int Max = (3) + 1;
			int Min = 1;
			int aleatorio = ((int) (Math.random() * (Max - Min)) + Min);
			resultado = estadoTrastorno[aleatorio];
		}
		if(tipo.equals("SINTOMA"))
		{
			int Max = (2) + 1;
			int Min = 1;
			int aleatorio = ((int) (Math.random() * (Max - Min)) + Min);
			resultado = estadoSintoma[aleatorio];
		}
		
		if(tipo.equals("DIAGNOSTICO"))
		{
			int Max = (3) + 1;
			int Min = 1;
			int aleatorio = ((int) (Math.random() * (Max - Min)) + Min);
			resultado = estadoDiagnostico[aleatorio];
		}
		return Estado.toClave(resultado);
	}
}