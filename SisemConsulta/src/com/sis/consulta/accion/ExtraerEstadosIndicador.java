package com.sis.consulta.accion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import com.comun.entidad.Indicador;
import com.comun.entidad.RegistroCaso;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Log;

public class ExtraerEstadosIndicador extends Accion {

	private File extractFile = null;
	private BufferedWriter bw = null;

	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;

		try
		{
			String organizacionIde = (String) peticion.get("organizacionIde");
			Map estadosIndicadorRegistrados = (Map) peticion.get("estadosIndicadorRegistrados");

			extractFile = (File) peticion.get("extractFile");
			String lineaEncabezados = "";
			String lineaDetalles = "";

			FileWriter fw = new FileWriter(extractFile, true);
			bw = new BufferedWriter(fw);

			Iterator it = estadosIndicadorRegistrados.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry) it.next();
				String valor = (String) e.getValue();
				System.out.println(valor);
				bw.write(valor, 0, valor.length());
				bw.newLine();
			}
		}
		catch (Exception e)
		{
			// TODO: handle exception
		}
		finally {
			if (bw != null) {
				try {
					bw.close();
				}
				catch (IOException e) {
					Log.error(this, e.getMessage());
				}
			}
		}

		this.setEstado(Estado.SUCCEEDED);
		return null;
	}
}