package com.sis.consulta;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;

public class ObtenerListaCasos extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = false;

		try {

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String enccodigo = (String) peticion.get("Muestra_enccodigo");
			String espcodigo = (String) peticion.get("Muestra_espcodigo");

			String consulta = "select encuesta.enccodigo, usuario.identidad, usuario.nombre, usuario.fechanacimiento, muestra.fecha, muestra.diagnostico "
					+ "from encuesta, usuario, muestra "
					+ "where encuesta.enccodigo=muestra.enccodigo and usuario.usucodigo=muestra.usucodigo and "
					+ "usuario.cargo='PACIENTE' and muestra.enccodigo='" + enccodigo + "' and muestra.espcodigo='" + espcodigo + "'";

			peticion.put("consulta", consulta);
			peticion.put("respuesta", Respuesta.LISTAMIXTA);

			result = dbc.ejecutarConsulta(peticion);

			this.setEstado(dbc.getEstado());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}