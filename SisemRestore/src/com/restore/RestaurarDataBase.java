package com.vista;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.comun.database.ConexionControlBackup;
import com.comun.motor.AccionListener;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.PropiedadesExterna;
import com.comun.utilidad.Util;

public class RestaurarDataBase {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				String paginaAccion = (String) args[0];
				
				Map peticion = new HashMap();
				peticion.put("paginaAccion",paginaAccion);
				Map peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

				
				PropiedadesExterna propiedadExterna = new PropiedadesExterna();
				Util util = new Util();

				String nombreSistema = propiedadExterna.getServidor("nombre-sistema");
				String rutaBackup = propiedadExterna.getServidor("ruta-backup");
				String nombreDatabase = propiedadExterna.getServidor("nombre-database");
				String organizacion = OrganizacionGeneral.getInstance(nombreSistema).getOrgcodigo();

				if (restaurarBackup(nombreSistema, rutaBackup + nombreDatabase)) {

					JOptionPane.showConfirmDialog(null, "El Backup Se restauro con exito" + "\n" + this.getClass().getName(), "Sistema Chentty", JOptionPane.PLAIN_MESSAGE,
							JOptionPane.INFORMATION_MESSAGE);

					util.abrirArchivoDesktop("C:\\sisem\\", nombreSistema + ".bat");

				}// Fin de if(man.realizarBackup("dbbackup_20121208"))
			}
		});
	}// Fin d e metodo main

	public static boolean restaurarBackup(String nombreSistema, String archivoRuta) {

		String conSql = "";
		Connection conecction;
		CallableStatement cstm;
		boolean exito = false;

		try {

			conSql = "{call proc_restore_" + nombreSistema + "(?)}";

			conecction = ConexionControlBackup.crearConexion();

			cstm = conecction.prepareCall(conSql);
			cstm.setString(1, archivoRuta);

			cstm.execute();
			exito = true;

			cstm.close();
			conecction.close();
		}
		catch (SQLException e) {
			// util.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}

		return exito;
	}// Fin d e realizarBackup(String archivoRuta)

}// Fin de clase principal
