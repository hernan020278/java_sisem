package com.producto.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Producto;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerProducto extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try 
		{
			if (!peticion.containsKey("consulta"))
			{
				String prdcodigo = (String) peticion.get("Pagina_valor");
				peticion.put("consulta", "select * from producto where prd_cod='" + prdcodigo + "'");
			}
			peticion.put("entidadNombre", "Producto");
			Producto producto = (Producto) (new ObtenerEntidad()).ejecutarAccion(peticion);
			result = producto;
			
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}