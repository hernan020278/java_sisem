package com.producto.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Producto;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerTalla extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			String prdcodigo = (String) peticion.get("Pagina_valor");

			peticion.put("consulta", "select * from producto where prd_cod='" + prdcodigo + "'");
			peticion.put("entidadNombre", "Producto");
			Producto producto = (Producto) (new ObtenerEntidad()).ejecutarAccion(peticion);
			peticion.put("Talla", producto);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}