package com.producto.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Producto;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerDetPedido extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try 
		{
			if (!peticion.containsKey("consulta"))
			{
				String detpedCod = (String) peticion.get("Pagina_valor");
				peticion.put("consulta", "select * from detpedido where detped_cod='" + detpedCod + "'");
			}
			peticion.put("entidadNombre", "DetPedido");
			DetPedido detPedido = (DetPedido) (new ObtenerEntidad()).ejecutarAccion(peticion);
			result = detPedido;
			
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}