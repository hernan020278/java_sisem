package com.producto.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Componente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ModificarProducto extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			String cmpcodigo = (String) peticion.get("Pagina_valor");
			peticion.put("consulta", "select * from componente where cmpcodigo='" + cmpcodigo + "'");
			peticion.put("entidadNombre", "Componente");
			Componente componente = (Componente) (new ObtenerEntidad()).ejecutarAccion(peticion);
			peticion.put("componente", componente);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}