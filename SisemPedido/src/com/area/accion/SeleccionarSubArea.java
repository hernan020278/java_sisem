package com.area.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Area;
import com.comun.entidad.SubArea;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class SeleccionarSubArea extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			Auxiliar aux = Auxiliar.getInstance();
			int subAreCod = Integer.parseInt((String) peticion.get("Pagina_valor"));
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			boolean exito = true;
			
			String sql = "select * from subarea where subare_cod=" + subAreCod;
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "SubArea");
			aux.subAre = (SubArea) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select * from area where are_cod=" + aux.subAre.getAre_cod();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Area");
			aux.are = (Area) (new ObtenerEntidad()).ejecutarAccion(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}