package com.componente.accion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Componente;
import com.comun.entidad.DetComponente;
import com.comun.entidad.FicControl;
import com.comun.entidad.Ficha;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;
import com.pedido.accion.ObtenerDetComponente;

public class ActualizarComponentePorModelo extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String paginaValor = (String) peticion.get("Pagina_valor");
			String codigo = "";
			String[] codigos = null;
			String columnaNombre = "";
			String columnaValor = "";
			if(paginaValor.contains("#"))
			{
				String[] valor = paginaValor.split("#");
				codigo = valor[0];
				columnaNombre = valor[1];
				columnaValor = valor[2];
				codigos = codigo.split(";");
			} else if(codigo.contains(";"))
			{
				codigos = codigo.split(";");
			}
			
			String sql = "select * from detcomponente where detcmpcodigo=" + codigo; 
			peticion.put("consulta", sql);
			DetComponente detComponente = (DetComponente) (new ObtenerDetComponente()).ejecutarAccion(peticion);
			

			sql = "select * from componente where cmpcodigo=" + detComponente.getCmpcodigo(); 
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Componente");
			Componente componente = (Componente) (new ObtenerEntidad()).ejecutarAccion(peticion);

			/**
			 * ELIMINAMOS LAS FICHAS DE CONTROL EXISTENTES PARA REALIZAR LOS CAMBIOS PERTINENTES
			 */
			sql = "delete from ficcontrol where ficcodigo in (select ficcodigo from viewComponentePorFicha " +
					"where pedcodigo=" + detComponente.getPedcodigo() + " " +
					"and cmpcodigo=" + detComponente.getCmpcodigo() + " " +
					"and prd_des='" + columnaNombre + "')";

			peticion.put("delete", sql);
			peticion.put("tipoSql", TipoSql.DELETE);
			peticion.put("entidadObjeto",  new FicControl());
			afectados = dbc.ejecutarCommit(peticion);
			
			/**
			 * CREAMOS LAS FICHAS DE CONTROL CON LOS NUEVOS VALORES DE TIEMPO DE FABRICACION
			 */
			String fieldFicha = Util.getInst().obtenerListaFieldsParaSql(new Ficha());
//			sql = "select ficha.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
//					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
//					"inner join ficha on detpedido.prd_cod=ficha.prd_cod and detpedido.ped_cod=ficha.ped_cod " +
//					"and detpedido.detped_cod=ficha.detped_cod and pedido.ped_cod=" + detComponente.getPedcodigo() + " " +
//					"and producto.prd_des='" + columnaNombre + "'";

			sql = "select " + fieldFicha + " from viewFichaPedido " +
					"where ped_cod=" + detComponente.getPedcodigo() + " and prd_des='" + columnaNombre + "' and fic_estado like '%PENDIENTE%'";
			
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Ficha");
			List listaFichas = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			
			for(int iteLis = 0; iteLis < listaFichas.size(); iteLis++)
			{
				BigDecimal newBigDecimal =  new BigDecimal(0);
				Ficha ficha = (Ficha) listaFichas.get(iteLis);
				
				FicControl ficControl = new FicControl();
				ficControl.setFiccodigo(new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString()));
				ficControl.setPrdcodigo(new BigDecimal(ficha.getPrd_cod()));
				ficControl.setPedcodigo(new BigDecimal(ficha.getPed_cod()));
				ficControl.setDetpedcodigo(new BigDecimal(ficha.getDetped_cod()));
				ficControl.setIdentidad(KeyGenerator.getInst().obtenerCodigoBarra());
				ficControl.setArecodigo(newBigDecimal);
				ficControl.setSubarecodigo(newBigDecimal);
				ficControl.setUsucodigo(newBigDecimal);
				ficControl.setCmpcodigo(detComponente.getCmpcodigo());
				ficControl.setPrccodigo(newBigDecimal);
				ficControl.setFecingreso(Util.getInst().getFechaSql());
				ficControl.setFecsalida(Util.getInst().getFechaSql());
				ficControl.setUnidad(detComponente.getUnidad());
				ficControl.setCanpedido(componente.getFactor().multiply(new BigDecimal(ficha.getFic_canped())));
				ficControl.setCanfallado(newBigDecimal);
				ficControl.setCanrealizado(componente.getFactor().multiply(new BigDecimal(ficha.getFic_canped())));
				ficControl.setCancontabilizado(componente.getFactor().multiply(new BigDecimal(ficha.getFic_canped())));
				ficControl.setTcmpesperado(new BigDecimal(columnaValor));
				ficControl.setTcmprealizado(ficControl.getTcmpesperado().multiply(ficControl.getTcmpesperado()));
				ficControl.setTcmpcontabilizado(ficControl.getTcmpesperado().multiply(ficControl.getTcmpesperado()));
				ficControl.setObservacion("");
				ficControl.setBarra("");
				ficControl.setFicnivel(new BigDecimal(2));
				ficControl.setFicsupuno(new BigDecimal(ficha.getFic_bar()));
				ficControl.setFicsupdos(newBigDecimal);
				ficControl.setFicsuptres(newBigDecimal);
				ficControl.setFicsupcua(newBigDecimal);
				ficControl.setEstado(Estado.PENDIENTE);
				ficControl.setVersion(new BigDecimal(1));
				
				peticion.put("tipoSql", TipoSql.INSERT);
				peticion.put("entidadObjeto", ficControl);
				afectados = dbc.ejecutarCommit(peticion);
			}
			
			if(afectados > 0){dbc.commit();}else{dbc.rollback();}
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}