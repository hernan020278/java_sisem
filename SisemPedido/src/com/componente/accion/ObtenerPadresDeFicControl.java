package com.componente.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Componente;
import com.comun.entidad.DetComponente;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Ficha;
import com.comun.entidad.Pedido;
import com.comun.entidad.Producto;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class ObtenerPadresDeFicControl extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try
		{
			String sql = "select * from ficha where ficha.fic_bar='" + aux.ficCtrl.getFicsupuno() + "' and ficha.fic_estado='PENDIENTE'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Ficha");
			aux.fic = (Ficha) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select * from componente where cmpcodigo=" + aux.ficCtrl.getCmpcodigo() + " and estado<>'0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Componente");
			aux.cmp = (Componente) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select * from pedido where ped_cod=" + aux.ficCtrl.getPedcodigo();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Pedido");
			aux.ped = (Pedido) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select * from detpedido where detped_cod=" + aux.ficCtrl.getDetpedcodigo();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "DetPedido");
			aux.detPed = (DetPedido) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select * from detcomponente where pedcodigo=" + aux.ped.getPed_cod() + " and cmpcodigo=" + aux.cmp.getCmpcodigo() + " and estado<>'0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "DetComponente");
			aux.detCmp = (DetComponente) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select producto.* from producto inner join detpedido on producto.prd_cod=detpedido.prd_cod where producto.prd_cod=" + aux.detPed.getPrd_cod();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Producto");
			aux.tal = (Producto) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select producto.* from producto inner join detpedido on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + aux.detPed.getPrd_cod() + " and producto.prd_supdos=" + aux.tal.getPrd_supdos();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Producto");
			aux.prd = (Producto) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select producto.* from producto inner join detpedido on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + aux.detPed.getPrd_cod() + " and producto.prd_supdos=" + aux.tal.getPrd_supdos() + " and producto.prd_supuno=" + aux.tal.getPrd_supuno();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Producto");
			aux.mod = (Producto) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select * from usuario where usucodigo='" + aux.ficCtrl.getUsucodigo() + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Usuario");
			aux.ope = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);			
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}