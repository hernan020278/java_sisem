package com.componente.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.FicControl;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class DuplicarNuevaFicControl extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
		try
		{

			String sql = "select * from ficcontrol where ficcontrol.identidad='"+aux.ficCtrl.getIdentidad() + "' and " +
					"ficcontrol.cmpcodigo=" + aux.ficCtrl.getCmpcodigo() + " and ficcontrol.prccodigo=0 and ficcontrol.usucodigo=0";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			FicControl ficControl = (FicControl)  (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			if(Util.isEmpty(ficControl.getIdentidad()))
			{
				BigDecimal newBigDecimal =  new BigDecimal(0);
				ficControl.setObjeto(aux.ficCtrl);
				ficControl.setFiccodigo(new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString()));
				ficControl.setPrccodigo(newBigDecimal);
				ficControl.setUsucodigo(newBigDecimal);
				ficControl.setCanrealizado(newBigDecimal);
				ficControl.setTcmprealizado(newBigDecimal);
				ficControl.setEstado(Estado.PENDIENTE);
				ficControl.setVersion(new BigDecimal(1));
				
				peticion.put("tipoSql", TipoSql.INSERT);
				peticion.put("entidadObjeto", ficControl);
				afectados = dbc.ejecutarCommit(peticion);			
			}//if(Util.isEmpty(ficControl.getIdentidad()))

			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}