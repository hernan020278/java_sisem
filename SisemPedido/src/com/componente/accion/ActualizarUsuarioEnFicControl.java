package com.componente.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Componente;
import com.comun.entidad.FicControl;
import com.comun.entidad.Ficha;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class ActualizarUsuarioEnFicControl extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String sql = "select * from ficcontrol where ficcontrol.prccodigo=" + aux.prc.getPrccodigo() + " and ficcontrol.estado='" + Estado.PRODUCCION + "'";
			
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			List listaFicControl = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			for(int iteLisFicCtrl = 0; iteLisFicCtrl < listaFicControl.size(); iteLisFicCtrl++)
			{
				FicControl ficControl =(FicControl) listaFicControl.get(iteLisFicCtrl);
				ficControl.setUsucodigo(aux.ope.getUsucodigo());
				
				peticion.put("tipoSql", TipoSql.UPDATE);
				peticion.put("entidadObjeto", ficControl);
				peticion.put("where", "where ficcodigo=" + ficControl.getFiccodigo());
				afectados = dbc.ejecutarCommit(peticion);
			}// for(int iteLisFicCtrl =0; iteLisFicCtrl < listaFicControl.size(); iteLisFicCtrl++)
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}