package com.componente.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.FicControl;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class AgregarComponentePorProduccion extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			/**
			 * CREAMOS LAS FICHAS DE CONTROL CON LOS NUEVOS VALORES DE TIEMPO DE FABRICACION
			 */
			String sql = "select * from ficcontrol where ficcontrol.cmpcodigo=" + aux.cmp.getCmpcodigo() + " and ficcontrol.usucodigo=0 and ficcontrol.prccodigo=0 and estado='" + Estado.PENDIENTE +"'" ;

			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			
			aux.ficCtrl = (FicControl)(new ObtenerEntidad()).ejecutarAccion(peticion);
			
			if(!Util.isEmpty(aux.ficCtrl.getFiccodigo().toString()))
			{
				peticion.put("tipoSql", TipoSql.DELETE);
				peticion.put("entidadObjeto", aux.ficCtrl);
				peticion.put("where", ("where ficcodigo=" + aux.ficCtrl.getFiccodigo()));
				afectados = dbc.ejecutarCommit(peticion);
				Util.getInst().limpiarEntidad(aux.ficCtrl,true);
			}
			if(Util.isEmpty(aux.ficCtrl.getIdentidad())  && !Util.isEmpty(aux.cmp.getIdentidad()) && !Util.isEmpty(aux.prc.getIdentidad()))
			{
				BigDecimal newBigDecimal = new BigDecimal(0);
				
				aux.ficCtrl.setFiccodigo(new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString()));
				aux.ficCtrl.setPrdcodigo(newBigDecimal);
				aux.ficCtrl.setPedcodigo(newBigDecimal);
				aux.ficCtrl.setDetpedcodigo(newBigDecimal);
				aux.ficCtrl.setIdentidad(KeyGenerator.getInst().obtenerCodigoBarra());
				aux.ficCtrl.setArecodigo(newBigDecimal);
				aux.ficCtrl.setSubarecodigo(newBigDecimal);
				aux.ficCtrl.setUsucodigo(aux.ope.getUsucodigo());
				aux.ficCtrl.setCmpcodigo(aux.cmp.getCmpcodigo());
				aux.ficCtrl.setPrccodigo(aux.prc.getPrccodigo());
				aux.ficCtrl.setFecingreso(aux.prc.getFecregistro());
				aux.ficCtrl.setFecsalida(aux.prc.getFecelaboracion());
				aux.ficCtrl.setUnidad("MIN");
				aux.ficCtrl.setCanpedido(aux.cmp.getFactor());
				aux.ficCtrl.setCanfallado(newBigDecimal);
				aux.ficCtrl.setCanrealizado(aux.cmp.getFactor());
				aux.ficCtrl.setCancontabilizado(aux.cmp.getFactor());
				aux.ficCtrl.setTcmpesperado(new BigDecimal(1));
				aux.ficCtrl.setTcmprealizado(new BigDecimal(1));
				aux.ficCtrl.setTcmpcontabilizado(aux.ficCtrl.getTcmpesperado().multiply(aux.ficCtrl.getTcmprealizado()));
				aux.ficCtrl.setObservacion("");
				aux.ficCtrl.setBarra("");
				aux.ficCtrl.setFicnivel(new BigDecimal(2));
				aux.ficCtrl.setFicsupuno(newBigDecimal);
				aux.ficCtrl.setFicsupdos(newBigDecimal);
				aux.ficCtrl.setFicsuptres(newBigDecimal);
				aux.ficCtrl.setFicsupcua(newBigDecimal);
				aux.ficCtrl.setEstado(Estado.TERMINADO);
				aux.ficCtrl.setVersion(new BigDecimal(1));
				
				peticion.put("tipoSql", TipoSql.INSERT);
				peticion.put("entidadObjeto", aux.ficCtrl);
				afectados = dbc.ejecutarCommit(peticion);
				
			}//if(listaResultado == null || listaResultado.size() == 0)
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}