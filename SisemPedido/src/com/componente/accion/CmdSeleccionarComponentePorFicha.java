package com.componente.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Componente;
import com.comun.entidad.DetComponente;
import com.comun.entidad.FicControl;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

public class CmdSeleccionarComponentePorFicha extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			BigDecimal cmpcodigo = new BigDecimal((String) peticion.get("Pagina_valor"));
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String sql = "select * from componente where cmpcodigo=" + cmpcodigo + " and estado<>'0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Componente");
			aux.cmp = (Componente) (new ObtenerEntidad()).ejecutarAccion(peticion);

			sql = "select * from detcomponente where pedcodigo="+ aux.ped.getPed_cod() +" and cmpcodigo=" + cmpcodigo + " and estado<>'0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "DetComponente");
			aux.detCmp = (DetComponente) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			if(aux.detCmp.getPedcodigo().compareTo(new BigDecimal(aux.ped.getPed_cod())) == 0)
			{
				/**
				 * ELIMINAMOS LAS FICHAS DE CONTROL EXISTENTES PARA REALIZAR LOS CAMBIOS PERTINENTES
				 */
				sql = "delete from ficcontrol where ficcodigo in (select ficcodigo from viewComponentePorFicha vcf " +
						"where vcf.ped_cod=" + aux.ped.getPed_cod() + " and vcf.fic_bar='" + aux.fic.getFic_bar() + "' and vcf.cmpcodigo=" + cmpcodigo + ")";
				
				peticion.put("delete", sql);
				peticion.put("tipoSql", TipoSql.DELETE);
				peticion.put("entidadObjeto",  new FicControl());
				afectados = dbc.ejecutarCommit(peticion);
				
				(new AgregarComponentePorFicha()).ejecutarAccion(peticion);
			}
				
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}