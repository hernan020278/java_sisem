package com.componente.accion;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

public class ObtenerListaUsarioPrueba extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
		try
		{
			Date fecact = (Date) peticion.get("fechaActual");
			
            String sql = "select usuario.nombre from usuario inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo";

			
			String[] listaColumnaTipo ={"STRING"};
			String[] listaColumnaAlias ={"nombre"};
			
			peticion.put("listaColumnaTipo", listaColumnaTipo);
			peticion.put("listaColumnaAlias", listaColumnaAlias);
			
			peticion.put("consulta", sql);
			peticion.put("respuesta", Respuesta.LISTAMIXTA);
			
			List listaResultado = dbc.ejecutarConsulta(peticion);
			peticion.put("listaNombre", listaResultado);
			
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}