package com.componente.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Componente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class AgregarComponente extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Componente componente = new Componente();
			BigDecimal codigo = new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString());
			String identidad = KeyGenerator.getInst().obtenerCodigoBarra();

			String sql = "select * from componente where identidad='" + identidad + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Componente");

			componente = (Componente) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if (Util.isEmpty(componente.getIdentidad()))
			{
				componente.setCmpcodigo(codigo);
				componente.setIdentidad(KeyGenerator.getInst().obtenerCodigoBarra().toString());
				componente.setEstado(Estado.PERMANENTE);
			}

			peticion.put("componente", componente);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}