package com.componente.accion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.DetComponente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class SeleccionarComponentePorModeloPartida extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			BigDecimal cmpcodigo = new BigDecimal((String) peticion.get("Pagina_valor"));
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			boolean exito = true;
			
			String sql = "select detcomponente.* from componente inner join detcomponente on componente.cmpcodigo=detcomponente.cmpcodigo " +
					"where detcomponente.prdcodigo=" + aux.mod.getPrd_cod() + " and componente.cmpcodigo=" + cmpcodigo + " and detcomponente.estado<>'0000' and componente.tipo='PARTIDA'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "DetComponente");
			List listaDetComponente = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			
			sql = "select detcomponente.* from componente inner join detcomponente on componente.cmpcodigo=detcomponente.cmpcodigo " +
					"where detcomponente.prdcodigo=" + aux.mod.getPrd_cod() + " and detcomponente.estado<>'0000' and componente.tipo='PARTIDA'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "DetComponente");
			List totallistaDetComponente = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

			if((listaDetComponente == null || listaDetComponente.size() == 0) && totallistaDetComponente.size() < 5)
			{
				BigDecimal newBidDecimal = new BigDecimal(0);
				DetComponente detComponente = new DetComponente();
				Util.getInst().limpiarEntidad(detComponente,true);

				detComponente.setDetcmpcodigo(new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString()));
				detComponente.setPrdcodigo(new BigDecimal(aux.mod.getPrd_cod()));
				detComponente.setCmpcodigo(cmpcodigo);
				detComponente.setPedcodigo(new BigDecimal(aux.ped.getPed_cod()));
				detComponente.setDetpedcodigo(newBidDecimal);
				detComponente.setUnidad("TMP");
				detComponente.setCantidad(newBidDecimal);
				detComponente.setEstado(Estado.PERMANENTE);
				detComponente.setVersion(new BigDecimal(0));
				
				peticion.put("tipoSql", TipoSql.INSERT);
				peticion.put("entidadObjeto", detComponente);
				afectados = dbc.ejecutarCommit(peticion);
			}
			else
			{
				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Error de Almacenamiento");
				aux.msg.setTipo("error");
				aux.msg.setValor("El componente ya existe o superan los 5 items");
			}
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}