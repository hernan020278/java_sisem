package com.componente.accion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.FicControl;
import com.comun.entidad.Produccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class EliminarComponentePorFicha extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		BigDecimal newBigDecimal = new BigDecimal(0);
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String paginaValor = (String) peticion.get("Pagina_valor");
			String[] codigos = null;
			if(!paginaValor.contains("#") && paginaValor.contains(";"))
			{
				codigos = paginaValor.split(";");
			}
		
			/**
			 * ELIMINAMOS LAS FICHAS DE CONTROL EXISTENTES PARA REALIZAR LOS CAMBIOS PERTINENTES
			 */
			String sql = "select * from ficcontrol where ficcontrol.ficcodigo=" + paginaValor;
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			aux.ficCtrl = (FicControl) (new ObtenerEntidad()).ejecutarAccion(peticion);

			BigDecimal totalCanContabilizado= (BigDecimal )(new TotalFicControlCanContabilizado()).ejecutarAccion(peticion);
			BigDecimal totalTcmpContabilizado= (BigDecimal )(new TotalFicControlTcmpContabilizado()).ejecutarAccion(peticion);
			if(totalCanContabilizado.compareTo(newBigDecimal) > 0)
			{
				aux.ficCtrl.setCanrealizado(newBigDecimal);
				aux.ficCtrl.setCancontabilizado(totalCanContabilizado);
				aux.ficCtrl.setTcmprealizado(newBigDecimal);
				aux.ficCtrl.setTcmpcontabilizado(totalTcmpContabilizado);
			}
			else
			{
				aux.ficCtrl.setCanrealizado(aux.ficCtrl.getCanpedido());
				aux.ficCtrl.setCancontabilizado(aux.ficCtrl.getCanpedido());
				aux.ficCtrl.setTcmprealizado(aux.ficCtrl.getTcmpesperado());
				aux.ficCtrl.setTcmpcontabilizado(aux.ficCtrl.getCanpedido().multiply(aux.ficCtrl.getTcmpesperado()));
			}
			
			(new ActualizarTotalContabilizadoFicControl()).ejecutarAccion(peticion);
			
			peticion.put("tipoSql", TipoSql.DELETE);
			peticion.put("entidadObjeto", aux.ficCtrl);
			peticion.put("where", ("where ficcodigo=" + aux.ficCtrl.getFiccodigo()));
			afectados = dbc.ejecutarCommit(peticion);
			
			peticion.put("browseTabla", "lis-cmp-por-fic");
			
			List listaFiltro = new ArrayList();
			listaFiltro.add(Util.getInst().crearRegistroFiltro("ficcontrol.prccodigo", "BIGDECIMAL", "=", aux.prc.getPrccodigo().toString(),"",""));
			Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

			(new GenerarBrowse()).ejecutarAccion(peticion);
			
			/**
			 * ELIMINAR FICHAS DE CONTROL DE COMPONENTE QUE NO TIENEN REGISTRO DE PRODUCCION
			 */
			peticion.put("FicControl_ficCodigo", aux.ficCtrl.getFiccodigo());
			peticion.put("FicControl_identidad", aux.ficCtrl.getIdentidad());
			(new EliminarComponentePorFichaSinProduccion()).ejecutarAccion(peticion);			
			/**********************************************************
			 * RECUPERAMOS LA LISTA DE FICHAS DE CONTROL ACTUALIZADAS *
			 **********************************************************/
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}