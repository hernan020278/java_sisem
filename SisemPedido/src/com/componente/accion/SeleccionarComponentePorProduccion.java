package com.componente.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Componente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class SeleccionarComponentePorProduccion extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			BigDecimal identidad = new BigDecimal((String) peticion.get("Pagina_valor"));
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String sql = "select * from componente where identidad=" + identidad + " and estado<>'0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Componente");
			aux.cmp = (Componente) (new ObtenerEntidad()).ejecutarAccion(peticion);

			if(!Util.isEmpty(aux.prc.getIdentidad()) && !Util.isEmpty(aux.cmp.getIdentidad()))
			{
				/**
				 * ELIMINAMOS LAS FICHAS DE CONTROL EXISTENTES PARA REALIZAR LOS CAMBIOS PERTINENTES
				 */
//				sql = "delete from ficcontrol where ficcodigo in (select ficcodigo from viewComponentePorProduccion vcp " +
//						"where vcp.prccodigo=" + aux.prc.getPrccodigo() +  " and vcp.identidad=" + aux.cmp.getidentidad() + ")";
//				
//				peticion.put("delete", sql);
//				peticion.put("tipoSql", TipoSql.DELETE);
//				peticion.put("entidadObjeto",  new FicControl());
//				afectados = dbc.ejecutarCommit(peticion);
//				
//				if(afectados > 0){dbc.commit();}else{dbc.rollback();}			
				
				(new AgregarComponentePorProduccion()).ejecutarAccion(peticion);
				
			}
				
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}