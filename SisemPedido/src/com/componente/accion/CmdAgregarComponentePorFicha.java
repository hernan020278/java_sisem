package com.componente.accion;

import java.util.Map;

import com.browse.BrowseObject;
import com.browse.accion.GenerarBrowse;
import com.browse.accion.GenerateBrowseObject;
import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Componente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class CmdAgregarComponentePorFicha extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			peticion.put("browseTabla", "sel-cmp-por-fic");
			(new GenerarBrowse()).ejecutarAccion(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}