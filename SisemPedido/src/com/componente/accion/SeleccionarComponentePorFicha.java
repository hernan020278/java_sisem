package com.componente.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Componente;
import com.comun.entidad.FicControl;
import com.comun.entidad.Ficha;
import com.comun.entidad.Produccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class SeleccionarComponentePorFicha extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try
		{
			String paginaValor = (String) peticion.get("Pagina_valor");
			String[] codigos = null;
			if(!paginaValor.contains("#") && paginaValor.contains(";"))
			{
				codigos = paginaValor.split(";");
			}
			String sql = "select * from ficcontrol where ficcodigo='" + paginaValor + "' and estado<>'0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			aux.ficCtrl= (FicControl) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			(new ObtenerPadresDeFicControl()).ejecutarAccion(peticion);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}