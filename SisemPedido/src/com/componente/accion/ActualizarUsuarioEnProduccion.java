package com.componente.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

public class ActualizarUsuarioEnProduccion extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			if(aux.ope.getUsucodigo().compareTo(aux.prc.getUsucodigo()) != 0)
			{
				aux.prc.setPrccodigo(aux.prc.getPrccodigo());
				aux.prc.setUsucodigo(aux.ope.getUsucodigo());
				
				peticion.put("tipoSql", TipoSql.UPDATE);
				peticion.put("entidadObjeto", aux.prc);
				peticion.put("where", "where prccodigo=" + aux.prc.getPrccodigo());
				afectados = dbc.ejecutarCommit(peticion);
			}
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}