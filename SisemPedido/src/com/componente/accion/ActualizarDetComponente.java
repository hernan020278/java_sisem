package com.componente.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.DetComponente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Util;
import com.pedido.accion.ObtenerDetComponente;

public class ActualizarDetComponente extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String paginaValor = (String) peticion.get("Pagina_valor");
			String codigo = "";
			String columnaNombre = "";
			String columnaValor = "";
			if(paginaValor.contains("#"))
			{
				String[] valor = paginaValor.split("#");
				codigo = valor[0];
				columnaNombre = valor[1];
				columnaValor = valor[2];
				
			}
			
			String sql = "select * from detcomponente where detcmpcodigo=" + codigo; 
			peticion.put("consulta", sql);
			
			DetComponente detComponente = (DetComponente) (new ObtenerDetComponente()).ejecutarAccion(peticion);
			peticion.put(columnaNombre, columnaValor);
			Util.getInst().setPeticionEntidad(peticion, detComponente);
			
			
			peticion.put("tipoSql", TipoSql.UPDATE);
			peticion.put("entidadObjeto", detComponente);
			peticion.put("where", "where detcmpcodigo='" + codigo + "'");
			afectados = dbc.ejecutarCommit(peticion);
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}