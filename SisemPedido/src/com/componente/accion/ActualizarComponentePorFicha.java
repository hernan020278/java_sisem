
package com.componente.accion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Componente;
import com.comun.entidad.FicControl;
import com.comun.entidad.Ficha;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class ActualizarComponentePorFicha extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String paginaValor = (String) peticion.get("Pagina_valor");
			String codigo = "";
			String[] codigos = null;
			String columnaNombre = "";
			String columnaValor = "";
			if(paginaValor.contains("#"))
			{
				String[] valor = paginaValor.split("#");
				codigo = valor[0];
				columnaNombre = valor[1];
				columnaValor = valor[2];
				codigos = codigo.split(";");
			} else if(codigo.contains(";"))
			{
				codigos = codigo.split(";");
			}
			String sql = "select * from ficcontrol where ficcontrol.ficcodigo="+codigo;
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			aux.ficCtrl = (FicControl)  (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select * from componente where cmpcodigo=" + aux.ficCtrl.getCmpcodigo();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Componente");
			aux.cmp = (Componente) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if(aux.cmp.getTipo().equals("FABRICACION") && columnaNombre.equalsIgnoreCase("FicControl_canrealizado"))
			{
				peticion.put(columnaNombre, columnaValor);
				Util.getInst().setPeticionEntidad(peticion, aux.ficCtrl);
				
				BigDecimal totalCanContabilizado= (BigDecimal )(new TotalFicControlCanContabilizado()).ejecutarAccion(peticion);
				BigDecimal totalTcmpContabilizado= (BigDecimal )(new TotalFicControlTcmpContabilizado()).ejecutarAccion(peticion);
				totalCanContabilizado = totalCanContabilizado.add(aux.ficCtrl.getCanrealizado());
				
				if((totalCanContabilizado.compareTo(aux.ficCtrl.getCanpedido())==-1
					||totalCanContabilizado.compareTo(aux.ficCtrl.getCanpedido())==0))
				{
					aux.ficCtrl.setCancontabilizado(totalCanContabilizado);
					aux.ficCtrl.setTcmprealizado(aux.ficCtrl.getCanrealizado().multiply(aux.ficCtrl.getTcmpesperado()));
					aux.ficCtrl.setTcmpcontabilizado(totalTcmpContabilizado.add(aux.ficCtrl.getTcmprealizado()));
					
					peticion.put("tipoSql", TipoSql.UPDATE);
					peticion.put("entidadObjeto", aux.ficCtrl);
					peticion.put("where", "where ficcodigo=" + aux.ficCtrl.getFiccodigo());
					afectados = dbc.ejecutarCommit(peticion);
					
					(new DuplicarNuevaFicControl()).ejecutarAccion(peticion);
					(new ActualizarTotalContabilizadoFicControl()).ejecutarAccion(peticion);
				}
			}
			if(!aux.cmp.getTipo().equals("FABRICACION") && columnaNombre.equalsIgnoreCase("FicControl_tcmprealizado"))
			{
				peticion.put(columnaNombre, columnaValor);
				Util.getInst().setPeticionEntidad(peticion, aux.ficCtrl);
				
				BigDecimal totalTcmpContabilizado= (BigDecimal )(new TotalFicControlTcmpContabilizado()).ejecutarAccion(peticion);
				
				aux.ficCtrl.setTcmpcontabilizado(totalTcmpContabilizado.add(aux.ficCtrl.getTcmprealizado()));
				peticion.put("tipoSql", TipoSql.UPDATE);
				peticion.put("entidadObjeto", aux.ficCtrl);
				peticion.put("where", "where ficcodigo=" + aux.ficCtrl.getFiccodigo());
				afectados = dbc.ejecutarCommit(peticion);
				
				(new DuplicarNuevaFicControl()).ejecutarAccion(peticion);
				(new ActualizarTotalContabilizadoFicControl()).ejecutarAccion(peticion);
			}
			
			
			/**********************************************************
			 * RECUPERAMOS LA LISTA DE FICHAS DE CONTROL ACTUALIZADAS *
			 **********************************************************/
			peticion.put("browseTabla", "lis-cmp-por-fic");
			
			List listaFiltro = new ArrayList();
			listaFiltro.add(Util.getInst().crearRegistroFiltro("ficcontrol.prccodigo", "BIGDECIMAL", "=", aux.prc.getPrccodigo().toString(),"",""));
			Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

			(new GenerarBrowse()).ejecutarAccion(peticion);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}