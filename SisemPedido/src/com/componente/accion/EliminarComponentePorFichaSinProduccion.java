package com.componente.accion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.FicControl;
import com.comun.entidad.Produccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class EliminarComponentePorFichaSinProduccion extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		BigDecimal newBigDecimal = new BigDecimal(0);
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			/**
			 * ELIMINAR FICHAS DE CONTROL DE COMPONENTE QUE NO TIENEN REGISTRO DE PRODUCCION
			 */
			BigDecimal ficCodigo = (BigDecimal) peticion.get("FicControl_ficCodigo");
			String identidad = (String) peticion.get("FicControl_identidad");
			
			String sql = "select * from ficcontrol where ficcodigo <> " + ficCodigo + " and identidad=" + identidad + " and estado <> '" + Estado.PENDIENTE + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			List listaFicControl = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			if(listaFicControl != null && listaFicControl.size() > 0)
			{
				for(int ite = 0; ite < listaFicControl.size(); ite++)
				{
					FicControl ficControl = (FicControl) listaFicControl.get(ite);
					sql = "select * from produccion where prccodigo=" + ficControl.getPrccodigo();
					peticion.put("consulta", sql);
					peticion.put("entidadNombre", "Produccion");
					Produccion produccion = (Produccion) (new ObtenerEntidad()).ejecutarAccion(peticion);
					if(Util.isEmpty(produccion.getPrccodigo().toString()))
					{
						sql = "delete from ficcontrol where estado <> '" + Estado.PENDIENTE + "' and ficcontrol.ficcodigo = " + ficControl.getFiccodigo();
						peticion.put("tipoSql", TipoSql.BATCH);
						peticion.put("consulta", sql);
						afectados = dbc.ejecutarCommit(peticion);
					}
				}
			}//if(listaFicControl != null && listaFicControl.size() > 0)
			
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}