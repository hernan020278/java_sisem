package com.componente.accion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.DetComponente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class SeleccionarModeloComponente extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			BigDecimal cmpcodigo = new BigDecimal((String) peticion.get("Pagina_valor"));
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			boolean exito = true;
			
			String sql = "select detcomponente.* from componente inner join detcomponente on componente.cmpcodigo=detcomponente.cmpcodigo " +
					"where detcomponente.prdcodigo=" + aux.mod.getPrd_cod() + " and componente.cmpcodigo=" + cmpcodigo + " and detcomponente.estado<>'0000' and componente.tipo<>'PARTIDA'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "DetComponente");
			List listaDetComponente = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			
			if(listaDetComponente == null || listaDetComponente.size() == 0)
			{

				DetComponente detComponente = new DetComponente();
				Util.getInst().limpiarEntidad(detComponente);

				detComponente.setDetcmpcodigo(new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString()));
				detComponente.setPrdcodigo(new BigDecimal(aux.mod.getPrd_cod()));
				detComponente.setCmpcodigo(cmpcodigo);
				detComponente.setPedcodigo(new BigDecimal(aux.detPed.getPed_cod()));
				detComponente.setDetpedcodigo(new BigDecimal(aux.detPed.getDetped_cod()));
				detComponente.setUnidad("TMP");
				detComponente.setCantidad(new BigDecimal(0));
				detComponente.setEstado(Estado.PERMANENTE);
				detComponente.setVersion(new BigDecimal(0));
				
				peticion.put("tipoSql", TipoSql.INSERT);
				peticion.put("entidadObjeto", detComponente);
				afectados = dbc.ejecutarCommit(peticion);
			}
			else
			{
				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Error de Almacenamiento");
				aux.msg.setTipo("error");
				aux.msg.setValor("El componente ya existe");
			}
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

	
//	String sql = "select producto.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
//					"inner join producto on producto.prd_cod=detpedido.prd_cod "+
//					"where producto.prd_nivel=2 and producto.prd_supuno=" + aux.mod.getPrd_cod();
//
//	peticion.put("entidadNombre", "Producto");
//	peticion.put("consulta", sql);
//	List listaProductos = (List) (new ObtenerListaEntidad().ejecutarAccion(peticion));
//
//	for(int iteLisPrd = 0; iteLisPrd < listaProductos.size(); iteLisPrd++)
//	{
//		
//		Producto producto = (Producto) listaProductos.get(iteLisPrd);
//		
//		sql = "select producto.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
//				"inner join producto on producto.prd_cod=detpedido.prd_cod "+
//				"where producto.prd_nivel=3 and producto.prd_supuno=" + aux.mod.getPrd_cod() + " and producto.prd_supdos=" + producto.getPrd_cod();
//
//		peticion.put("entidadNombre", "Producto");
//		peticion.put("consulta", sql);
//		List listaTallas = (List) (new ObtenerListaEtiqueta().ejecutarAccion(peticion));
//
//		for(int iteLisTal = 0; iteLisTal < listaTallas.size(); iteLisTal++)
//		{
//			
//			Producto talla = (Producto) listaTallas.get(iteLisTal);
//			
//			String[] tipo = {"BIGDECIMAL", "BIGDECIMAL", "BIGDECIMAL","BIGDECIMAL", "STRING"};
//			String[] alias = {"pedcodigo", "detpedcodigo","prdcodigo", "cantidad", "barra"};
//			
//			sql = "select ficcontrol.pedcodigo pedcodigo, ficcontrol.detpedcodigo detpedcodigo, ficcontrol.prdcodigo prdcodigo, ficcontrol.canpedido cantidad, ficcontrol.barra barra " +
//				"from cliente inner join pedido on cliente.cli_cod=pedido.cli_cod inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
//				"inner join producto on producto.prd_cod=detpedido.prd_cod " +
//				"inner join ficcontrol on ficcontrol.pedcodigo=detpedido.ped_cod and ficcontrol.detpedcodigo=detpedido.detped_cod " +
//				"and ficcontrol.prdcodigo=detpedido.prd_cod " +
//				"inner join area on area.are_cod=ficcontrol.arecodigo inner join subarea on subarea.subare_cod=ficcontrol.subarecodigo " +
//				"where ficcontrol.ficnivel=1 and " +
//				"producto.prd_supuno=" + aux.mod.getPrd_cod() + " and " +
//				"producto.prd_supdos=" + producto.getPrd_cod() + " and " +
//				"ficcontrol.prdcodigo=" + talla.getPrd_cod() + " " +
//				"group by ficcontrol.pedcodigo, ficcontrol.detpedcodigo, ficcontrol.prdcodigo, ficcontrol.canpedido, ficcontrol.barra ";
//			
//			peticion.put("respuesta", Respuesta.LISTAMIXTA);
//			peticion.put("consulta", sql);
//
//			peticion.put("listaColumnaTipo", tipo);
//			peticion.put("listaColumnaAlias", alias);
//
//			List listaFichas = dbc.ejecutarConsulta(peticion);
//
//			for(int iteLisFic = 0; iteLisFic < listaFichas.size(); iteLisFic++)
//			{
//				Object[] registro = (Object[]) listaFichas.get(iteLisFic);
//				BigDecimal ficcodigo = new BigDecimal(UniqueKeyGenerator.getInst().getUniqueKey().toString());
//				BigDecimal prdcodigo = (BigDecimal) registro[1];
//				BigDecimal pedcodigo = (BigDecimal) registro[2];
//				BigDecimal detpedcodigo = (BigDecimal) registro[3];
//				BigDecimal barra = new BigDecimal((String) registro[4]);
//				
//				Ficcontrol ficcontrol = new Ficcontrol();
//				Util.getInstance().limpiarEntidad(ficcontrol);
//				ficcontrol.setFiccodigo(ficcodigo);
//				ficcontrol.setPedcodigo(pedcodigo);
//				ficcontrol.setDetpedcodigo(detpedcodigo);
//				ficcontrol.setPrdcodigo(prdcodigo);
//				ficcontrol.setCmpcodigo(cmpcodigo);
//				ficcontrol.setFecingreso(Util.fechaSql);
//				ficcontrol.setFecsalida(Util.fechaSql);
//				ficcontrol.setUnidad("UND");
//				ficcontrol.setFicnivel(new BigDecimal(2));
//				ficcontrol.setFicsupuno(barra);
//				ficcontrol.setEstado(Estado.PERMANENTE);
//				
//				peticion.put("tipoSql", TipoSql.INSERT);
//				peticion.put("entidadObjeto", ficcontrol);
//				afectados = dbc.ejecutarCommit(peticion);
//
//				if(afectados == 0)
//				{
//					exito = false;
//				}
//
//			}//for(int iteLisFic = 0; iteLisFic < listaFichas.size(); iteLisFic++)
//		}//for(int iteLisTal = 0; iteLisTal < listaTallas.size(); iteLisTal++)
//	}//for(int iteLisPrd = 0; iteLisPrd < listaProductos.size(); iteLisPrd++)
	
}