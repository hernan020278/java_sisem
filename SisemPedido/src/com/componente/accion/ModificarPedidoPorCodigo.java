package com.componente.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Componente;
import com.comun.entidad.FicControl;
import com.comun.entidad.Ficha;
import com.comun.entidad.Pedido;
import com.comun.entidad.Produccion;
import com.comun.entidad.Producto;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class ModificarPedidoPorCodigo extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try
		{
			String paginaValor = (String) peticion.get("Pagina_valor");
			String codigo = "";
			String[] codigos = null;
			String columnaNombre = "";
			String columnaValor = "";
			if(paginaValor.contains("#"))
			{
				String[] valor = paginaValor.split("#");
				codigo = valor[0];
				columnaNombre = valor[1];
				columnaValor = valor[2];
				codigos = codigo.split(";");
			} else if(codigo.contains(";"))
			{
				codigos = codigo.split(";");
			}
			String sql = "select * from pedido where ped_cod=" + paginaValor + " and pedido.ped_est='PRODUCCION'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Pedido");
			aux.ped= (Pedido) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select * from producto where prd_cod in(" +
					"select producto.prd_supuno from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " + 
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where pedido.ped_cod=" + paginaValor + " and ped_est='PRODUCCION' and producto.prd_nivel=3 group by producto.prd_supuno)";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Producto");
			aux.mod= (Producto) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}