package com.componente.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.FicControl;
import com.comun.entidad.Ficha;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

public class AgregarComponentePorFicha extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			/**
			 * CREAMOS LAS FICHAS DE CONTROL CON LOS NUEVOS VALORES DE TIEMPO DE FABRICACION
			 */
			String fieldFicCtrl = Util.getInst().obtenerListaFieldsParaSql(new FicControl());
			String sql = "select " + fieldFicCtrl + " from viewComponentePorFicha vcf " +
					"where vcf.ped_cod=" + aux.ped.getPed_cod() + " and vcf.fic_bar='" + aux.fic.getFic_bar() + "' and vcf.cmpcodigo=" + aux.cmp.getCmpcodigo();			

			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			
			FicControl ficCtrl = (FicControl)(new ObtenerEntidad()).ejecutarAccion(peticion);
			
			if(Util.isEmpty(ficCtrl.getIdentidad()) && !Util.isEmpty(aux.ope.getIdentidad()) && !Util.isEmpty(aux.cmp.getIdentidad()) && aux.detCmp.getDetcmpcodigo().compareTo(new BigDecimal(0)) == 1)
			{
				String fieldFic = Util.getInst().obtenerListaFieldsParaSql(aux.fic);
				sql = "select " + fieldFic + " from viewComponentePorFicha vcf " +
						"where vcf.ped_cod=" + aux.ped.getPed_cod() + " and vcf.fic_bar='" + aux.fic.getFic_bar() + "'";
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Ficha");
				
				Ficha ficha = (Ficha) (new ObtenerEntidad()).ejecutarAccion(peticion);
				BigDecimal newBigDecimal = new BigDecimal(0);
				FicControl ficControl = new FicControl();
				
				ficControl.setFiccodigo(new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString()));
				ficControl.setPrdcodigo(new BigDecimal(ficha.getPrd_cod()));
				ficControl.setPedcodigo(new BigDecimal(ficha.getPed_cod()));
				ficControl.setDetpedcodigo(new BigDecimal(ficha.getDetped_cod()));
				ficControl.setIdentidad(KeyGenerator.getInst().obtenerCodigoBarra());
				ficControl.setArecodigo(newBigDecimal);
				ficControl.setSubarecodigo(newBigDecimal);
				ficControl.setUsucodigo(aux.ope.getUsucodigo());
				ficControl.setCmpcodigo(aux.cmp.getCmpcodigo());
				ficControl.setFecingreso(Util.getInst().getFechaSql());
				ficControl.setFecsalida(Util.getInst().getFechaSql());
				ficControl.setUnidad(aux.detCmp.getUnidad());
				
				ficControl.setCanpedido(aux.cmp.getFactor().multiply(new BigDecimal(ficha.getFic_canped())));
				ficControl.setCanrealizado(aux.cmp.getFactor().multiply(new BigDecimal(ficha.getFic_canped())));
				ficControl.setCancontabilizado(aux.cmp.getFactor().multiply(new BigDecimal(ficha.getFic_canped())));
				
				ficControl.setTcmpesperado(newBigDecimal);
				ficControl.setTcmprealizado(newBigDecimal);
				ficControl.setTcmpcontabilizado(newBigDecimal);
				
				ficControl.setObservacion("");
				ficControl.setBarra("");
				ficControl.setFicnivel(new BigDecimal(2));
				ficControl.setFicsupuno(new BigDecimal(ficha.getFic_bar()));
				ficControl.setFicsupdos(newBigDecimal);
				ficControl.setFicsuptres(newBigDecimal);
				ficControl.setFicsupcua(newBigDecimal);
				ficControl.setEstado(Estado.PRODUCCION);
				ficControl.setVersion(new BigDecimal(1));
				
				peticion.put("tipoSql", TipoSql.INSERT);
				peticion.put("entidadObjeto", ficControl);
				afectados = dbc.ejecutarCommit(peticion);
				
			}//if(listaResultado == null || listaResultado.size() == 0)
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}