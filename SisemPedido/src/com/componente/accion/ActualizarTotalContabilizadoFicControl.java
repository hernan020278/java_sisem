package com.componente.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

public class ActualizarTotalContabilizadoFicControl extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
		try
		{
			String estado = aux.ficCtrl.getEstado();
			if(aux.ficCtrl.getCanpedido().compareTo(aux.ficCtrl.getCancontabilizado()) == 0)
			{
				estado = Estado.TERMINADO;
			}
			else if(aux.ficCtrl.getCanpedido().compareTo(aux.ficCtrl.getCancontabilizado()) == 1)
			{
				estado = Estado.PRODUCCION;
			}
			String sql = "update ficcontrol set " +
					"cancontabilizado=" + aux.ficCtrl.getCancontabilizado() + ", tcmpcontabilizado=" + aux.ficCtrl.getTcmpcontabilizado() + ", " +
					"estado='" + estado + "' where ficcontrol.identidad='" + aux.ficCtrl.getIdentidad() + "' and ficcontrol.estado <> '" + Estado.PENDIENTE + "'";
			peticion.put("tipoSql", TipoSql.BATCH);
			peticion.put("consulta", sql);
			
			aux.ficCtrl.setEstado(estado);
			afectados = dbc.ejecutarCommit(peticion);			

			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}