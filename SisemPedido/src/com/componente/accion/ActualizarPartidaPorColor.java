package com.componente.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;

public class ActualizarPartidaPorColor extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String paginaValor = (String) peticion.get("Pagina_valor");
			String codigo = "";
			String[] codigos = null;
			String columnaNombre = "";
			String columnaValor = "";
			if(paginaValor.contains("#"))
			{
				String[] valor = paginaValor.split("#");
				codigo = valor[0];
				columnaNombre = valor[1];
				columnaValor = valor[2];
				codigos = codigo.split(";");
			} else if(codigo.contains(";"))
			{
				codigos = codigo.split(";");
			}
			
			String sql = "update producto set " + (columnaNombre.replaceFirst("_",".")) + "='" + columnaValor + "' where prd_cod in ( " +
					"select producto.prd_cod " +
					"from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where pedido.ped_cod=" + codigos[0] + " and producto.prd_col='" + codigos[1] + "' " +
					"and (prd_nivel=2 or prd_nivel = 3)" +
					")";	
			peticion.put("consulta", sql);
			peticion.put("tipoSql", TipoSql.BATCH);
			afectados = dbc.ejecutarCommit(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}