package com.pedido.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class GenerarBrowseAsrepositorio extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			peticion.put("browseTabla", "brw-asrepositorio");
			
			String asicodigo = (String) peticion.get("Asasistencia_asicodigo");
			
			List listaFiltro = new ArrayList();
			listaFiltro.add(Util.getInst().crearRegistroFiltro("asrepositorio.asicodigo", "BIGDECIMAL", "=", asicodigo ,"",""));
			Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

			(new GenerarBrowse()).ejecutarAccion(peticion);
			
			peticion.put("browseAsrepositorio", peticion.get("browse"));
			peticion.put("browseObjectAsrepositorio", peticion.get("browseObject"));
			peticion.put("browseListAsrepositorio", peticion.get("browseList"));
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}