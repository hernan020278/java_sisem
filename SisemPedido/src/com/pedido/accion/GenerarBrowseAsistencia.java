package com.pedido.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.motor.Accion;
import com.comun.motor.IAccionListener;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import reactor.core.publisher.Mono;

public class GenerarBrowseAsistencia extends Accion implements IAccionListener{

	public Mono<Map> ejecutarAccion(Map peticion) {

		Auxiliar aux = Auxiliar.getInstance();
		peticion.put("browseTabla", "brw-asasistencia");

		List listaFiltro = new ArrayList();
		listaFiltro.add(Util.getInst().crearRegistroFiltro("asiasistencia.kyctrlasistencia", "BIGDECIMAL", "=", aux.asictrlasi.getKyctrlasistencia().toString(),"AND",""));
		Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

		(new GenerarBrowse()).ejecutarAccion(peticion);

		peticion.put("browseAsasistencia", peticion.get("browse"));
		peticion.put("browseObjectAsasistencia", peticion.get("browseObject"));
		peticion.put("browseListAsasistencia", peticion.get("browseList"));

		return Mono.just(peticion);
	}

}