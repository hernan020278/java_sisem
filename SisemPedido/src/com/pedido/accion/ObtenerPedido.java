package com.pedido.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Pedido;
import com.comun.entidad.Producto;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Util;

public class ObtenerPedido extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			if (!peticion.containsKey("consulta"))
			{
				String pedidoPedcod = (String) peticion.get("Pedido_ped_cod");
				peticion.put("consulta", "select * from pedido where ped_cod=" + pedidoPedcod + "");
			}
			peticion.put("entidadNombre", "Pedido");
			Pedido pedido = (Pedido) (new ObtenerEntidad()).ejecutarAccion(peticion);
			result = pedido;
			this.setEstado(Estado.SUCCEEDED);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}