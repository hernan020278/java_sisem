package com.pedido.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.motor.Accion;
import com.comun.motor.IAccionListener;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import reactor.core.publisher.Mono;

public class GenerarBrowseTotalHoraDeHorario extends Accion implements IAccionListener{

  public Mono<Map> ejecutarAccion(Map peticion) {

    Object result = null;

    Auxiliar aux = Auxiliar.getInstance();
    peticion.put("browseTabla", "brw_totalhora-de-horario");

    List listaFiltro = new ArrayList();
    listaFiltro.add(Util.getInst().crearRegistroFiltro("asihorario.kyctrlasistencia", "BIGDECIMAL", "=", String.valueOf(aux.asictrlasi.getKyctrlasistencia()), "", ""));
    Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

    (new GenerarBrowse()).ejecutarAccion(peticion);

    peticion.put("browseResumen", peticion.get("browse"));
    peticion.put("browseObjectResumen", peticion.get("browseObject"));
    peticion.put("browseListResumen", peticion.get("browseList"));

    this.setEstado(Estado.SUCCEEDED);

    return Mono.just(peticion);
  }

}