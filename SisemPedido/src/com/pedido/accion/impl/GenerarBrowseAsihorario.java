package com.pedido.accion.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.motor.Accion;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.motor.IAccionListener;
import reactor.core.publisher.Mono;

public class GenerarBrowseAsihorario extends Accion implements IAccionListener {

	@Override
	public Mono<Map> ejecutarAccion(Map peticion) {

		Auxiliar aux = Auxiliar.getInstance();
		peticion.put("browseTabla", "brw-asihorario");

		List listaFiltro = new ArrayList();
		listaFiltro.add(Util.getInst().crearRegistroFiltro("asihorario.kyctrlasistencia", "BIGDECIMAL", "=", String.valueOf(aux.asictrlasi.getKyctrlasistencia()),"",""));
		Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

		return Mono.just(peticion);
	}

}