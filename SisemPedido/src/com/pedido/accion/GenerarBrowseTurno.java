package com.pedido.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class GenerarBrowseTurno extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();

			String tipoBrowse = (String) peticion.get("tipoBrowse");
			if(Util.getInst().isEmpty(tipoBrowse)){peticion.put("browseTabla", "lis-asiturno");}
			else if (tipoBrowse.equals("BROWSE")){peticion.put("browseTabla", "brw-asiturno");}
			
			List listaFiltro = new ArrayList();
			listaFiltro.add(Util.getInst().crearRegistroFiltro("asiturno.kyhorario", "BIGDECIMAL", "=", String.valueOf(aux.asihora.getKyhorario()),"",""));
			Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

			(new GenerarBrowse()).ejecutarAccion(peticion);
			
			peticion.put("browseTurno", peticion.get("browse"));
			peticion.put("browseObjectTurno", peticion.get("browseObject"));
			peticion.put("browseListTurno", peticion.get("browseList"));
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}