package com.pedido.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Componente;
import com.comun.entidad.DetComponente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;

public class EliminarDetComponente extends Accion
{
	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String paginaValor = (String) peticion.get("Pagina_valor");
			String codigo = "";
			String[] codigos = null;
			String columnaNombre = "";
			String columnaValor = "";
			if(paginaValor.contains("#"))
			{
				String[] valor = paginaValor.split("#");
				codigo = valor[0];
				columnaNombre = valor[1];
				columnaValor = valor[2];
				codigos = codigo.split(";");
			} else if(codigo.contains(";"))
			{
				codigos = codigo.split(";");
			}
			peticion.put("DetComponente_detcmpcodigo", paginaValor);
			DetComponente detComponente = (DetComponente) (new ObtenerDetComponente()).ejecutarAccion(peticion);
			String sql = "delete from detcomponente where detcomponente.detcmpcodigo=" + detComponente.getDetcmpcodigo(); 
			peticion.put("tipoSql", TipoSql.BATCH);
			peticion.put("consulta", sql);
			afectados = dbc.ejecutarCommit(peticion);
			
			sql = "delete from ficcontrol where " +
					"ficcontrol.pedcodigo=" + detComponente.getPedcodigo() + " and " +
					"ficcontrol.cmpcodigo=" + detComponente.getCmpcodigo(); 
			peticion.put("tipoSql", TipoSql.BATCH);
			peticion.put("consulta", sql);
			afectados = dbc.ejecutarCommit(peticion);

			if (afectados > 0)
			{
				peticion.put("Mensaje_mostrar", "true");
				peticion.put("Mensaje_titulo", "Informacion de Almacenamiento");
				peticion.put("Mensaje_tipo", "info");
				peticion.put("Mensaje_valor", "Los datos se han borrado satisfactoriamente");
			}// Fin de if(bookMarkDevCod != 0)
			else
			{
				peticion.put("Mensaje_mostrar", "true");
				peticion.put("Mensaje_titulo", "Informacion de Almacenamiento");
				peticion.put("Mensaje_tipo", "error");
				peticion.put("Mensaje_valor", "Los datos se han borrado satisfactoriamente");
			}
			result = peticion;
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return result;
	}
}