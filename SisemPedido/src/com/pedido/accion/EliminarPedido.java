package com.pedido.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Pedido;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;

public class EliminarPedido extends Accion
{
	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			peticion.put("Pedido_ped_cod", peticion.get("Pagina_valor"));
			(new ObtenerPedido()).ejecutarAccion(peticion);

			Pedido ped = (Pedido) peticion.get("pedido");

			ped.setPed_est(Estado.DELETED);

			peticion.put("tipoSql", TipoSql.UPDATE);
			peticion.put("entidadObjeto", ped);
			peticion.put("where", ("where ped_cod='" + ped.getPed_cod() + "'"));
			afectados = dbc.ejecutarCommit(peticion);

			if (afectados > 0)
			{
				peticion.put("Mensaje_mostrar", "true");
				peticion.put("Mensaje_titulo", "Informacion de Almacenamiento");
				peticion.put("Mensaje_tipo", "info");
				peticion.put("Mensaje_valor", "Los datos se han borrado satisfactoriamente");
			}// Fin de if(bookMarkDevCod != 0)
			else
			{
				peticion.put("Mensaje_mostrar", "true");
				peticion.put("Mensaje_titulo", "Informacion de Almacenamiento");
				peticion.put("Mensaje_tipo", "error");
				peticion.put("Mensaje_valor", "Los datos se han borrado satisfactoriamente");
			}
			result = peticion;
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return result;
	}
}