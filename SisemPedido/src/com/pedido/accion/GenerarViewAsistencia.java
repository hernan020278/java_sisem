package com.pedido.accion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class GenerarViewAsistencia extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			Date fecha = (Date) peticion.get("Asasistencia_fecha");
			peticion.put("browseTabla", "view-asasistencia");
			
			String usucodigo = "12345678901234";
			
			if(!Util.getInst().isEmpty(aux.usu.getUsucodigo().toString())){usucodigo = aux.usu.getUsucodigo().toString();}
			
			List listaFiltro = new ArrayList();
			listaFiltro.add(Util.getInst().crearRegistroFiltro("usuario.usucodigo", "BIGDECIMAL", "=", usucodigo,"AND",""));
			listaFiltro.add(Util.getInst().crearRegistroFiltro("asictrlasistencia.casperiodo", "STRING", "=", aux.asictrlasi.getPeriodo(),"AND",""));
			listaFiltro.add(Util.getInst().crearRegistroFiltro("asi.fecha", "DATE", "=", fecha.toString() ,"",""));
			Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

			(new GenerarBrowse()).ejecutarAccion(peticion);
			
			peticion.put("browseAsasistencia", peticion.get("browse"));
			peticion.put("browseObjectAsasistencia", peticion.get("browseObject"));
			peticion.put("browseListAsasistencia", peticion.get("browseList"));
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}