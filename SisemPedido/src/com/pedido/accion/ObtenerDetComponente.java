package com.pedido.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.DetComponente;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerDetComponente extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try 
		{
			if (!peticion.containsKey("consulta"))
			{
				String detcmpcodigo = (String) peticion.get("DetComponente_detcmpcodigo");
				peticion.put("consulta", "select * from detcomponente where detcmpcodigo='" + detcmpcodigo + "'");
			}
			peticion.put("entidadNombre", "DetComponente");
			DetComponente detComponente = (DetComponente) (new ObtenerEntidad()).ejecutarAccion(peticion);

			result = detComponente;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}