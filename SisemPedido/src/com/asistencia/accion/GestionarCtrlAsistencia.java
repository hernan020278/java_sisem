package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Area;
import com.comun.entidad.Asictrlasistencia;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

import java.util.Map;

public class GestionarCtrlAsistencia extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
					
			String usucodigo = (String) peticion.get("Pagina_valor");
			peticion.put("consulta", "select * from usuario where usucodigo = " + usucodigo);
			peticion.put("entidadNombre", "Usuario");
			aux.usu = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);

			String periodo = Util.getInst().obtenerPeriodo(Util.getInst().getFechaSql());
			peticion.put("fechaActual", Util.getInst().getFechaSql());
			
			peticion.put("Usuario_identidad", aux.usu.getIdentidad());
			peticion.put("AgregarAsictrlasistencia_periodo", periodo);

			(new AgregarAsictrlasistencia()).ejecutarAccion(peticion);

			(new AgregarAsihorario()).ejecutarAccion(peticion);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}