package com.asistencia.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class GenerarSeleccionAsihorarios extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			peticion.put("browseTabla", "sel-asihorario");
			
			List listaFiltro = new ArrayList();
			listaFiltro.add(Util.getInst().crearRegistroFiltro("asihorario.kyctrlasistencia", "BIGDECIMAL", "=", String.valueOf(aux.asictrlasi.getKyctrlasistencia()),"",""));
			Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

			(new GenerarBrowse()).ejecutarAccion(peticion);
			
			peticion.put("browseAsihorario", peticion.get("browse"));
			peticion.put("browseObjectAsihorario", peticion.get("browseObject"));
			peticion.put("browseListAsihorario", peticion.get("browseList"));
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}