package com.asistencia.accion;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

import java.util.Map;

public class EliminarRepositorio extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {
    int afectados = 0;
    try {
      Auxiliar aux = Auxiliar.getInstance();

      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

      String sql = "delete from asirepositorio where kyasistencia = " + aux.asiAsisLast.getKyasistencia() + " and repositorionombre = '" + Modo.SALIDA_ANTES_HORA + "' and respositoriotipo='INASISTENCIA'";
      peticion.put("consulta", sql);
      peticion.put("tipoSql", TipoSql.BATCH);

			afectados = dbc.ejecutarCommit(peticion);

    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
    return afectados;
  }
}