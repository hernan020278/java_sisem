package com.asistencia.accion;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Asirepositorio;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

/**
 * Guardarmos un registro de asistencia estableciendo el modo en que se esta registrando INGRESO, SALIDA o FALTA
 * 
 * @param aux.asictrlasi
 * @param aux.usu
 * @param aux.asiturn
 * @param fecha
 * @param turno
 * @return void
 * @author hernan
 */
public class AgregarExtra extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		int afectados = 0;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			BigDecimal newBigDecimal = new BigDecimal(0);
			BigDecimal totalHoraExt = (BigDecimal) peticion.get("totalHoraExt");
			BigDecimal totalHoraExtUno = newBigDecimal;
			BigDecimal totalHoraExtDos = newBigDecimal;
			if(totalHoraExt.compareTo(newBigDecimal) == 1)
			{
				Timestamp hora = (Timestamp) peticion.get("hora");
				String estadoRegistro = (String) peticion.get("estadoRegistro");
				Asirepositorio asirepo = new Asirepositorio();
				Util.getInst().limpiarEntidad(asirepo);
				if(totalHoraExt.compareTo(new BigDecimal(2)) == 1)
				{
					totalHoraExtUno = new BigDecimal(2);
					totalHoraExtDos = totalHoraExt.subtract(totalHoraExtUno);
				}
				else
				{
					totalHoraExtUno = totalHoraExt;
					totalHoraExtDos = newBigDecimal;
				}
				if(totalHoraExtUno.compareTo(newBigDecimal) == 1)
				{
					asirepo.setKyusuario(aux.usu.getUsucodigo());
					asirepo.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
					asirepo.setKyhorario(aux.asihora.getKyhorario());
					asirepo.setKyasistencia(aux.asiasis.getKyasistencia());
					asirepo.setNumerodni(aux.usu.getIdentidad());
					asirepo.setPeriodo(aux.asictrlasi.getPeriodo());
					asirepo.setHorarionombre(aux.asihora.getHorarionombre());
					asirepo.setRepositorionombre("EXTRAUNO");
					asirepo.setRepositorionombre(estadoRegistro);
					asirepo.setFechahora(hora);
					asirepo.setHoraregistrada(totalHoraExtUno);
					asirepo.setHoraconsumida(newBigDecimal);
					asirepo.setHorareales(newBigDecimal);
					asirepo.setEstado(Estado.GENERADO);

					peticion.put("tipoSql", TipoSql.INSERT);
					peticion.put("entidadObjeto", asirepo);
					afectados = dbc.ejecutarCommit(peticion);
				}
				if(totalHoraExtDos.compareTo(newBigDecimal) == 1)
				{
					asirepo.setKyusuario(aux.usu.getUsucodigo());
					asirepo.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
					asirepo.setKyhorario(aux.asihora.getKyhorario());
					asirepo.setKyasistencia(aux.asiasis.getKyasistencia());
					asirepo.setNumerodni(aux.usu.getIdentidad());
					asirepo.setPeriodo(aux.asictrlasi.getPeriodo());
					asirepo.setHorarionombre(aux.asihora.getHorarionombre());
					asirepo.setRepositorionombre("EXTRADOS");
					asirepo.setRepositorionombre(estadoRegistro);
					asirepo.setFechahora(hora);
					asirepo.setHoraregistrada(totalHoraExtDos);
					asirepo.setHoraconsumida(newBigDecimal);
					asirepo.setHorareales(newBigDecimal);
					asirepo.setEstado(Estado.GENERADO);

					peticion.put("tipoSql", TipoSql.INSERT);
					peticion.put("entidadObjeto", asirepo);
					afectados = dbc.ejecutarCommit(peticion);
				}
			}// if(totalHoraIna.compareTo(newBigDecimal) == 1)
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return afectados;
	}
}