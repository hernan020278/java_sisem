package com.asistencia.accion;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Asirepositorio;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

/**
 * Guardarmos un registro en el repositorio ya sea de inasistencia o horas extras
 *
 * @param aux.asiasis
 * @param totalHoraIna
 * @param totalHoraExt
 * @param estadoRegistro
 * @param dbConeccion
 * @author hernan
 * @return void
 */

public class GuardarAsirepositorio extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();
      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
      /*
       * AGREGANDO HORAS DE INASISTENCIA
       */
      BigDecimal newBigDecimal = new BigDecimal(0);
      BigDecimal totalHoraIna = (BigDecimal) peticion.get("totalHoraIna");
      BigDecimal totalHoraExt = (BigDecimal) peticion.get("totalHoraExt");
      BigDecimal totalHoraExtUno = newBigDecimal;
      BigDecimal totalHoraExtDos = newBigDecimal;

      if (totalHoraIna.compareTo(newBigDecimal) == 1)
      {
        Timestamp hora = (Timestamp) peticion.get("hora");
        String estadoRegistro = (String) peticion.get("estadoRegistro");

        Asirepositorio asirepo = new Asirepositorio();

        asirepo.setKyusuario(aux.usu.getUsucodigo());
        asirepo.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
        asirepo.setKyhorario(aux.asihora.getKyhorario());
        asirepo.setKyasistencia(aux.asiasis.getKyasistencia());
        asirepo.setNumerodni(aux.usu.getIdentidad());
        asirepo.setPeriodo(aux.asictrlasi.getPeriodo());
        asirepo.setHorarionombre(aux.asihora.getHorarionombre());
        asirepo.setRepositoriotipo("INASISTENCIA");
        asirepo.setRepositorionombre(estadoRegistro);
        asirepo.setFechahora(hora);
        asirepo.setHoraregistrada(totalHoraIna);
        asirepo.setHoraconsumida(newBigDecimal);
        asirepo.setHorareales(newBigDecimal);
        asirepo.setEstado(Estado.APROBADO);

        String tipoSql = TipoSql.INSERT;
        peticion.put("tipoSql", tipoSql);
        peticion.put("entidadObjeto", asirepo);

        afectados = dbc.ejecutarCommit(peticion);
      }
      /*
       * AGREGANDO HORAS EXTRAS
       */
      if (totalHoraExt.compareTo(newBigDecimal) == 1) {
        Timestamp hora = (Timestamp) peticion.get("hora");
        String estadoRegistro = (String) peticion.get("estadoRegistro");

        Asirepositorio asirepo = new Asirepositorio();
        Util.getInst().limpiarEntidad(asirepo);

        if (totalHoraExt.compareTo(new BigDecimal(2)) == 1) {
          totalHoraExtUno = new BigDecimal(2);
          totalHoraExtDos = totalHoraExt.subtract(totalHoraExtUno);
        } else {
          totalHoraExtUno = totalHoraExt;
          totalHoraExtDos = newBigDecimal;
        }

        if (totalHoraExtUno.compareTo(newBigDecimal) == 1)
        {
          BigDecimal codigo = new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString());

          asirepo.setKyusuario(aux.usu.getUsucodigo());
          asirepo.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
          asirepo.setKyhorario(aux.asihora.getKyhorario());
          asirepo.setKyasistencia(aux.asiasis.getKyasistencia());
          asirepo.setNumerodni(aux.usu.getIdentidad());
          asirepo.setPeriodo(aux.asictrlasi.getPeriodo());
          asirepo.setHorarionombre(aux.asihora.getHorarionombre());
          asirepo.setRepositoriotipo("EXTRAUNO");
          asirepo.setRepositorionombre(estadoRegistro);
          asirepo.setFechahora(hora);
          asirepo.setHoraregistrada(totalHoraIna);
          asirepo.setHoraconsumida(newBigDecimal);
          asirepo.setHorareales(newBigDecimal);
          asirepo.setEstado(Estado.APROBADO);

          String tipoSql = TipoSql.INSERT;
          peticion.put("tipoSql", tipoSql);
          peticion.put("entidadObjeto", asirepo);

          afectados = dbc.ejecutarCommit(peticion);
        }
        if (totalHoraExtDos.compareTo(newBigDecimal) == 1)
        {
          BigDecimal codigo = new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString());

          asirepo.setKyusuario(aux.usu.getUsucodigo());
          asirepo.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
          asirepo.setKyhorario(aux.asihora.getKyhorario());
          asirepo.setKyasistencia(aux.asiasis.getKyasistencia());
          asirepo.setNumerodni(aux.usu.getIdentidad());
          asirepo.setPeriodo(aux.asictrlasi.getPeriodo());
          asirepo.setHorarionombre(aux.asihora.getHorarionombre());
          asirepo.setRepositoriotipo("EXTRADOS");
          asirepo.setRepositorionombre(estadoRegistro);
          asirepo.setFechahora(hora);
          asirepo.setHoraregistrada(totalHoraIna);
          asirepo.setHoraconsumida(newBigDecimal);
          asirepo.setHorareales(newBigDecimal);
          asirepo.setEstado(Estado.APROBADO);

          String tipoSql = TipoSql.INSERT;
          peticion.put("tipoSql", tipoSql);
          peticion.put("entidadObjeto", asirepo);

          afectados = dbc.ejecutarCommit(peticion);
        }
      }
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
    return afectados;
  }
}