package com.asistencia.accion;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.utilidad.Auxiliar;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class TotalHoraNoTrabajadaTurno extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    Auxiliar aux = Auxiliar.getInstance();
    DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
    try {
      String sql = "select sum(horanotrabajada) as total " +
          "from asirepositorio inner join asiasistencia on asiasistencia.kyasistencia=asirepositorio.kyasistencia " +
          "where asiasistencia.kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia() + "' " +
          "and asiasistencia.kyusuario = '" + aux.usu.getUsucodigo() + "' " +
          "and asiasistencia.fecha = '" + aux.asiAsisLast.getFecha() + "' " +
          "and asiasistencia.turnonombre = '" + aux.asiturn.getTurnonombre() + "'";


      String[] listaColumnaTipo = {"BIGDECIMAL"};
      String[] listaColumnaAlias = {"total"};

      peticion.put("listaColumnaTipo", listaColumnaTipo);
      peticion.put("listaColumnaAlias", listaColumnaAlias);

      peticion.put("consulta", sql);
      peticion.put("respuesta", Respuesta.LISTAMIXTA);

      List listaResultado = dbc.ejecutarConsulta(peticion);

      if (listaResultado != null && listaResultado.size() > 0) {
        Object[] registro = (Object[]) listaResultado.get(0);
        if (registro[0] instanceof BigDecimal) {
          result = (BigDecimal) registro[0];
        } else {
          result = new BigDecimal(0);
        }
      }
      this.setEstado(Estado.SUCCEEDED);

    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      throw e;
    }
    return result;
  }

}