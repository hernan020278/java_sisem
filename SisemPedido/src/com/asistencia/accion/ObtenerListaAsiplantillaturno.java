package com.asistencia.accion;

import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ObtenerListaAsiplantillaturno extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();
      List lisAsiPlanTurn = null;

      String sql = "SELECT asiplantillaturno.* FROM asiplantillaturno WHERE kyplantillahorario = "+ aux.asiplanhora.getKyplantillahorario();

      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiplantillaturno");
      lisAsiPlanTurn = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

      peticion.put("lisAsiPlanTurn", lisAsiPlanTurn);
      // peticion.put("listaAsiturnoAnt", listaAsiturnoAnt);
      result = peticion;
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }

    return result;
  }
}