package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asihorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

import java.util.Map;

public class EliminarAsihorario extends Accion
{
	public Object ejecutarAccion(Map peticion) throws Exception
	{
		Object result = null;
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			Auxiliar aux  = Auxiliar.getInstance();
			
			String papcodigo = (String) peticion.get("Pagina_valor");
			peticion.put("consulta", "select * from asihorario where kyhorario = " + papcodigo + " and horariotipo='HORARIO'");
			peticion.put("entidadNombre", "Asihorario");
			aux.asihora = (Asihorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if(!aux.asihora.getEstado().equals(Estado.APROBADO))
			{
				aux.asihora.setEstado(Estado.DESAPROBADO);

				peticion.put("tipoSql", TipoSql.DELETE);
				peticion.put("entidadObjeto", aux.asihora);
				peticion.put("where", ("where kyhorario='" + papcodigo + "'"));
				
				afectados = dbc.ejecutarCommit(peticion);
			}else{
				msgBox("Asistencia", "No se puede eliminar una papeleta APROBADA y de tipo HORARIO", "error");
			}
			peticion.put("afectados", afectados);
			
			result = peticion;
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return result;
	}
}