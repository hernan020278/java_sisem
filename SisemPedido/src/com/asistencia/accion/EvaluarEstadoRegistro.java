package com.asistencia.accion;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Map;

import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class EvaluarEstadoRegistro extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {
		boolean fueraTurno = true;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();

			Date fecact = (Date) peticion.get("fechaActual");
			Timestamp horaIngreso = (Timestamp) peticion.get("horaIngreso");
			Timestamp horaSalida = (Timestamp) peticion.get("horaSalida");
			Timestamp hora = (Timestamp) peticion.get("hora");

			String modoRegistro = (String) peticion.get("modoRegistro");
			String estadoRegistro = (String) peticion.get("estadoRegistro");

			if (modoRegistro.equals(Modo.INGRESO))
			{
				horaIngreso = new Timestamp(hora.getTime());
				horaSalida = new Timestamp(18000000);
			}
			else if (modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP))
			{
				horaIngreso = new Timestamp(aux.asiAsisLast.getHoraingreso().getTime());
				horaSalida = new Timestamp(hora.getTime());
			}

			if(aux.asiAsisLast.getEstadosalida().equals(Modo.SALIDA_ANTES_HORA))
			{
				estadoRegistro = Modo.INGRESO_MULTIPLE;
			} 
			else if((Boolean) (new ExisteFeriado()).ejecutarAccion(peticion)) 
			{
				if(modoRegistro.equals(Modo.INGRESO)){estadoRegistro = Modo.INGRESO_FERIADO;}
				else if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)){estadoRegistro = Modo.SALIDA_FERIADO;}
			} 
			else if((Boolean) (new ExisteAsihorario()).ejecutarAccion(peticion))
			{
				String tipo = (String) peticion.get("Asihorario_tipo");
				tipo = tipo.replace(" ", "_");
				if(modoRegistro.equals(Modo.INGRESO)){estadoRegistro = "ING" + tipo;}
				else if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)){estadoRegistro = "SAL" + tipo;}
			}
			else
			{
//				for(int iteLis = 0; iteLis < listaAsiturno.size(); iteLis++)
//				{
//					Asiturno asiturnouno = (Asiturno) listaAsiturno.get(iteLis);
				if( ( modoRegistro.equals(Modo.INGRESO) || modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP) ) && !Util.getInst().isEmpty(aux.asiturn.getKyturno()))
				{
					if(!Util.getInst().existeHoraEnRango(hora, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
					{
						if(modoRegistro.equals(Modo.INGRESO)){estadoRegistro = Modo.INGRESO_FUERA_TURNO;}
						else if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)){estadoRegistro = Modo.SALIDA_FUERA_TURNO;}
					}
					else if(Util.getInst().existeHoraEnRango(hora, aux.asiturn.getEntrada(), aux.asiturn.getLimentradauno()))
					{
						if(modoRegistro.equals(Modo.INGRESO)){estadoRegistro = Modo.INGRESO_LIM_1;}
						else if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)){estadoRegistro = Modo.SALIDA_LIM_1;}
					}
					else if(Util.getInst().existeHoraEnRango(hora, aux.asiturn.getLimentradauno(), aux.asiturn.getLimentradados()))
					{
						if(modoRegistro.equals(Modo.INGRESO)){estadoRegistro = Modo.INGRESO_LIM_2;}
						else if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)){estadoRegistro = Modo.SALIDA_LIM_2;}
					}
					else if(Util.getInst().existeHoraEnRango(hora, aux.asiturn.getLimentradados(), aux.asiturn.getLimentrada()))
					{
						if(modoRegistro.equals(Modo.INGRESO)){estadoRegistro = Modo.INGRESO_LIM_3;}
						else if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)){estadoRegistro = Modo.SALIDA_LIM_3;}
					}
					else if(Util.getInst().existeHoraEnRango(hora, aux.asiturn.getLimentrada(), aux.asiturn.getSalida()))
					{
						if(modoRegistro.equals(Modo.INGRESO)){estadoRegistro = Modo.INGRESO_LIM_4;}
						else if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)){estadoRegistro = Modo.SALIDA_LIM_4;}
					}
				}//if(modoRegistro.equals(Modo.INGRESO))
				else
				{
					if(modoRegistro.equals(Modo.INGRESO)){estadoRegistro = Modo.INGRESO_FUERA_TURNO;}
					else if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)){estadoRegistro = Modo.SALIDA_FUERA_TURNO;}
				}
			}//
//		}
			peticion.put("horaIngreso", horaIngreso);
			peticion.put("horaSalida", horaSalida);
			peticion.put("estadoRegistro", estadoRegistro);
    	}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return fueraTurno;		// Timestamp parHorIni, Timestamp parHorFin, Timestamp parHorLimTar, Timestamp parHorLimSal
	}
}