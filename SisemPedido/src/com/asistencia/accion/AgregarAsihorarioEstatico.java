package com.asistencia.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiplantillahorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class AgregarAsihorarioEstatico extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			Asiplantillahorario asiplahora = (Asiplantillahorario) peticion.get("Asiplantillahorario");

			Asihorario asihora = new Asihorario();
			Util.getInst().limpiarEntidad(asihora, true);

			asihora.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
			asihora.setKyusuario(aux.usu.getUsucodigo());
			asihora.setNumerodni(aux.usu.getIdentidad());
			asihora.setPeriodo(aux.asictrlasi.getPeriodo());
			asihora.setHorarionombre(asiplahora.getDescripcion());
			asihora.setEstatico("Y");
			asihora.setEstado(Estado.ACTIVO);

			Util.getInst().imprimiValores(asihora);
			String tipoSql = TipoSql.INSERT;
			peticion.put("tipoSql", tipoSql);
			peticion.put("entidadObjeto", asihora);
			afectados = dbc.ejecutarCommit(peticion);
			
			aux.asihora = asihora;
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}