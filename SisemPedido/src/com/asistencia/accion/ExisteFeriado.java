package com.asistencia.accion;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiturno;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class ExisteFeriado extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {
    boolean existeFeriado = false;
    try {
      Auxiliar aux = Auxiliar.getInstance();
      Date fecact = (Date) peticion.get("fechaActual");
      Timestamp hora = (Timestamp) peticion.get("hora");
      hora = Util.getInst().obtenerHoraAComparar(fecact, hora);
      String dia = (String) peticion.get("dia");


      String sql = "select asihorario.horariotipo, asiturno.* from asiturno INNER JOIN asihorario ON asiturno.kyhorario=asihorario.kyhorario " +
          "where asihorario.estado = 'APROBADO'  and asihorario.horariotipo = 'HORARIO' and asiturno.turnotipo = 'FERIADO' " +
          "and asiturno.periodo = '" + aux.asictrlasi.getPeriodo() + "' and asiturno.numerodni = '" + aux.usu.getIdentidad() + "' " +
          "and ( '" + Util.getInst().fromTimestampToString(hora) + "' >= asiturno.entrada ) " +
          "and ( '" + Util.getInst().fromTimestampToString(hora) + "' <= asiturno.salida ) " +
          "and dia = '" + dia.toUpperCase() + "'";

      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiturno");
      Asiturno asiturno = (Asiturno) (new ObtenerEntidad()).ejecutarAccion(peticion);
      if(!Util.getInst().isEmpty(asiturno.getKyturno()))
      {
        existeFeriado = true;
      }
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
    return existeFeriado;    // Timestamp parHorIni, Timestamp parHorFin, Timestamp parHorLimTar, Timestamp parHorLimSal

  }
}