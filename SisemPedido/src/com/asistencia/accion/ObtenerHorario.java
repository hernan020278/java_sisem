package com.asistencia.accion;

import java.sql.Date;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiplantillahorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ObtenerHorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

        Object result = null;
        try 
        {
        	Auxiliar aux = Auxiliar.getInstance();
        	
			String identidad = (String) peticion.get("Usuario_identidad");
			Date fecact = (Date) peticion.get("fechaActual");
//			String periodo = Util.getInst().obtenerPeriodo(fecact);
			
			String sql = "select ashorario.* " +
				"from usuario inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " +
				"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
				"inner join ashorario on ashorario.horcodigo=asctrlhorario.horcodigo " +
				"left join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
				"where ( aspapeleta.estado = '0215' or aspapeleta.estado = '0217' ) and aspapeleta.tipo = 'HORARIO' " + 
				"and asctrlhorario.fecinicio <= '" + fecact + "' and '" + fecact + "' <= asctrlhorario.fecfinal " +
				"and usuario.identidad = '" + identidad + "'";
			
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asiplantillahorario");
			aux.asiplanhora = (Asiplantillahorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			result = peticion;
    	} 
    	catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
    	
    	return result;
	}
}