package com.asistencia.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asictrlasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerAsctrlasistenciaByPeriodo extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			String periodo = (String) peticion.get("Asictrlasistencia_periodo");
			String identidad = (String) peticion.get("Usuario_identidad");

			peticion.put("consulta", "select * from asictrlasistencia where periodo='" + periodo + "'");
			peticion.put("entidadNombre", "Asictrlasistencia");
			result = (Asictrlasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}