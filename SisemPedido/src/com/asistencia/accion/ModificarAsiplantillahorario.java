package com.asistencia.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiplantillahorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ModificarAsiplantillahorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
					
			String kyplantillahorario = (String) peticion.get("Pagina_valor");
			peticion.put("consulta", "select * from asiplantillahorario where kyplantillahorario = " + kyplantillahorario);
			peticion.put("entidadNombre", "Asiplantillahorario");
			aux.asiplanhora = (Asiplantillahorario) (new ObtenerEntidad()).ejecutarAccion(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}