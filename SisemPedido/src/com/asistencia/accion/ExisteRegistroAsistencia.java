package com.asistencia.accion;

import java.sql.Date;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

/**
 * Evalua si existe un registro de asistencia en una fecact determinada y en turno determinado
 *
 * @param aux.asictrlasi
 * @param aux.usu
 * @param fecact
 * @param turno
 * @author hernan
 * @return void
 */

public class ExisteRegistroAsistencia extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {
    Object result = null;

    try {

      Auxiliar aux = Auxiliar.getInstance();
      Date fecact = (Date) peticion.get("fechaActual");
      String turno = (String) peticion.get("turno");

      String sql = "select * from asasistencia " +
          "where kyctrlasistencia=" + aux.asictrlasi.getKyctrlasistencia() + " and kuusuario=" + aux.usu.getUsucodigo() + " " +
          "and fecha='" + fecact.toString() + "' and turnonombre='" + turno + "'";

      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiasistencia");
      Asiasistencia asiasis = (Asiasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);

      result = false;
      if(!Util.getInst().isEmpty(asiasis.getKyasistencia()))
      {
        result = true;
        this.setEstado(Estado.SUCCEEDED);
      }

    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      throw e;
    }
    return result;
  }

}