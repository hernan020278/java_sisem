package com.asistencia.accion;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class GetListaTurnosByDiaAndDiaAnt extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;

    try {
      Auxiliar aux = Auxiliar.getInstance();
      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
      Date fecact = (Date) peticion.get("fechaActual");

      String dia = Util.getInst().strDia[fecact.getDay()];

      List listaAsiturno = null;
      String sql = "";

      sql = "select asiturno.* " +
          "from asiturno inner join asihorario on asihorario.kyhorario=asiturno.kyhorario " +
          "where ( asihorario.estado = 'ACTIVO' or asihorario.estado = 'MODIFICADO' ) and asihorario.horariotipo = 'HORARIO' " +
          "and '" + fecact.toString() + "' >= asihorario.fechainicio and '" + fecact.toString() + "' <= asihorario.fechafinal " +
          "and asihorario.periodo = '" + aux.asictrlasi.getPeriodo() + "' and asihorario.numerodni = '" + aux.usu.getIdentidad() + "' " +
          "and asiturno.turnodia = '" + dia + "' order by asiturno.kyturno asc";

      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiturno");
      listaAsiturno = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

      peticion.put("listaAsiturno", listaAsiturno);

      this.setEstado(Estado.SUCCEEDED);
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      throw e;
    }
    return result;
  }

}