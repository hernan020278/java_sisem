package com.asistencia.accion;

import com.comun.database.DBConeccion;
import com.comun.entidad.Asiplantillaturno;
import com.comun.entidad.Asiturno;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;

import java.util.List;
import java.util.Map;

public class ActualizarListaAsiturno extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

      String tipoSql = TipoSql.DELETE;
      peticion.put("entidadObjeto", new Asiturno());
      peticion.put("tipoSql", tipoSql);
      peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where kyhorario = '" + aux.asihora.getKyhorario() + "'"));
      afectados = dbc.ejecutarCommit(peticion);
			
			(new ObtenerListaAsiplantillaturno()).ejecutarAccion(peticion);
			
			List lisAsiPlanTurn = (List) peticion.get("lisAsiPlanTurn");
			
			for (int idx=0; idx<lisAsiPlanTurn.size(); idx++) 
			{
			  aux.asiplanturn = (Asiplantillaturno) lisAsiPlanTurn.get(idx);
			  
			  Asiturno asiturn = new Asiturno();
			  
			  asiturn.setKyturno(KeyGenerator.getInst().getKyTabla());
			  asiturn.setKyusuario(aux.usu.getUsucodigo());
			  asiturn.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
			  asiturn.setKyhorario(aux.asihora.getKyhorario());
			  asiturn.setNumerodni(aux.usu.getIdentidad());
			  asiturn.setPeriodo(aux.asictrlasi.getPeriodo());
			  asiturn.setHorarionombre(aux.asihora.getHorarionombre());
			  asiturn.setTurnonombre(aux.asiplanturn.getTurnonombre());
			  asiturn.setTurnodia(aux.asiplanturn.getTurnodia());
			  asiturn.setTurnotipo(aux.asiplanturn.getTurnotipo());
			  asiturn.setEntrada(aux.asiplanturn.getEntrada());
        asiturn.setLimentradauno(aux.asiplanturn.getLimentradauno());
        asiturn.setLimentradados(aux.asiplanturn.getLimentradados());
        asiturn.setLimentrada(aux.asiplanturn.getLimentrada());
        asiturn.setLimsalidauno(aux.asiplanturn.getLimentradauno());
        asiturn.setLimsalidados(aux.asiplanturn.getLimentradauno());
        asiturn.setLimsalida(aux.asiplanturn.getLimentradauno());
        asiturn.setSalida(aux.asiplanturn.getLimentradauno());
        asiturn.setEstado(Estado.ACTIVO);

        tipoSql = TipoSql.INSERT;
        peticion.put("entidadObjeto", asiturn);
        peticion.put("tipoSql", tipoSql);
        peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where kyhorario = '" + aux.asihora.getKyhorario() + "'"));
        afectados = dbc.ejecutarCommit(peticion);
			}
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}