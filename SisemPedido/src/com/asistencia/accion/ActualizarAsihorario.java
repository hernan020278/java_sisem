package com.asistencia.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

public class ActualizarAsihorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			aux.asihora.setKyusuario(aux.usu.getUsucodigo());
			aux.asihora.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
			aux.asihora.setNumerodni(aux.usu.getIdentidad());
			aux.asihora.setPeriodo(aux.asictrlasi.getPeriodo());
			aux.asihora.setHorariotipo(aux.asiplanhora.getDescripcion());
			aux.asihora.setHorarionombre(aux.asiplanhora.getDescripcion());

			String tipoSql = TipoSql.UPDATE;
			peticion.put("entidadObjeto", aux.asihora);
			peticion.put("tipoSql", tipoSql);
			peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where kyhorario = '" + aux.asihora.getKyhorario() + "'"));
			afectados = dbc.ejecutarCommit(peticion);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}