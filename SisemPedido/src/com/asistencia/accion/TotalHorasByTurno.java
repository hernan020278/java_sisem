package com.asistencia.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class TotalHorasByTurno extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
		try
		{
			(new GetTurnoByDiaNombre()).ejecutarAccion(peticion);
			
			BigDecimal totalHora = Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), aux.asiturn.getSalida());
			
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}