package com.asistencia.accion;

import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ObtenerListaTurnos extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

        Object result = null;
        try 
        {
        	Auxiliar aux = Auxiliar.getInstance();
        	String dia = (String) peticion.get("dia");
//        	String diaAnt = (String) peticion.get("diaAnt");
        	
        	List listaAsiturno = null;
//			List listaAsiturnoAnt = null;
			
			String sql = "select asiturno.* " +
					"from usuario inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " +
						"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
						"inner join ashorario on ashorario.horcodigo=asctrlhorario.horcodigo " +
						"inner join asiturno on asiturno.horcodigo=ashorario.horcodigo " +
						"left join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
					"where aspapeleta.estado = '0215' and aspapeleta.tipo = 'HORARIO' " +
					"and asictrlasistencia.periodo = '" + aux.asictrlasi.getPeriodo()  + "' and usuario.identidad = '" + aux.usu.getIdentidad() + "' ";
			
			String sqlDia = sql + "and dia = '" + dia + "' order by asiturno.nombre asc";
//			String sqlDiaAnt = sql + "and dia = '" + diaAnt + "' order by asiturno.nombre asc";
			
			peticion.put("consulta", sqlDia);
			peticion.put("entidadNombre", "Asiturno");
			listaAsiturno = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

//			peticion.put("consulta", sqlDiaAnt);
//			peticion.put("entidadNombre", "Asiturno");
//			listaAsiturnoAnt = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			
			peticion.put("listaAsiturno", listaAsiturno);
//			peticion.put("listaAsiturnoAnt", listaAsiturnoAnt);
			result = peticion;
    	} 
    	catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
    	
    	return result;
	}
}