package com.asistencia.accion;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.comun.entidad.Asiturno;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class RegistrarFaltaNoLaborada extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			String tipo = (String) peticion.get("Asihorario_tipo");
			String modo = (String) peticion.get("Asihorario_modo");
			String turno = (String) peticion.get("Asihorario_refturno");
			Date fecinicio = (Date) peticion.get("Asihorario_fechainicio");
			Date fecfinal = (Date) peticion.get("Asihorario_fecejefinal");
			Timestamp horinicio = (Timestamp) peticion.get("Asihorario_horinicio");
			Timestamp horfinal = (Timestamp) peticion.get("Asihorario_horfinal");
			BigDecimal totalHora = new BigDecimal(0);
			if(fecinicio.getTime() <= fecfinal.getTime())
			{
				Date fecactTmp = new Date(fecinicio.getYear(), fecinicio.getMonth(), fecinicio.getDate());
				int numDias = Util.getInst().diferenciaFechas(fecinicio, fecfinal, "DIAS");
				double sumHoras = 0.00;
				for(int iteDia = 0; iteDia <= numDias; iteDia++)
				{
					fecactTmp.setDate(fecinicio.getDate() + iteDia);
					
					peticion.put("fechaActual", fecactTmp);
					(new GetListaTurnosByDiaAndDiaAnt()).ejecutarAccion(peticion);
					
					List listaTurno = (List) peticion.get("listaAsiturno");
					boolean agregarHora = true;
					for(int iteLis = 0; iteLis < listaTurno.size(); iteLis++)
					{
						Asiturno asiturno = (Asiturno) listaTurno.get(iteLis);
						if(asiturno.getTurnotipo().equals("LABORABLE"))
						{
							boolean existeAsistencia = (Boolean) (new ExisteRegistroAsistencia()).ejecutarAccion(peticion);
							if(!existeAsistencia)
							{
						        (new GuardarAsiasistencia()).ejecutarAccion(peticion);
						        (new GuardarAsirepositorio()).ejecutarAccion(peticion);
							}
						}// if(asiturno.getTipo().equals("LABORABLE"))
					}// for(int iteLis = 0; iteLis < listaTurno.size(); iteLis++)
				}// for(int iteDia = 0; iteDia <= numDias; iteDia++)
			}// if (fecinicio.getTime() < fecfinal.getTime())
			peticion.put("totalHora", totalHora);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
		return result;
	}
}