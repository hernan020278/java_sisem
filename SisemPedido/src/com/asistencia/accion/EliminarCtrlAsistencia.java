package com.asistencia.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

public class EliminarCtrlAsistencia extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			String usucodigo = (String) peticion.get("Pagina_valor");
			peticion.put("consulta", "update usuario set estado='0000' where usucodigo = " + usucodigo);
			peticion.put("tipoSql", TipoSql.BATCH);
			afectados = dbc.ejecutarCommit(peticion);
			
			if(afectados==0){msgBox("Eliminacion de registro", "Error de eliminacion del registro!!!", "error");}
			else{msgBox("Eliminacion de registro", "El control de asistencia ha sido eliminado!!!", "successful");}
			
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}