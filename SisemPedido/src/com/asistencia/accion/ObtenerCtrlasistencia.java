package com.asistencia.accion;

import java.sql.Date;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asictrlasistencia;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ObtenerCtrlasistencia extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

        Object result = null;
        try 
        {
        	Auxiliar aux = Auxiliar.getInstance();
        	
			String identidad = (String) peticion.get("Usuario_identidad");
			Date fecact = (Date) peticion.get("fechaActual");
//			String periodo = Util.getInst().obtenerPeriodo(fecact);
			
			String sql = "select * from usuario where identidad = '" + identidad + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Usuario");
			aux.usu = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			sql = "select asictrlasistencia.* " +
					"from usuario inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " +
					"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
					"left join ashorario on ashorario.horcodigo=asctrlhorario.horcodigo " +
					"left join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
					"where aspapeleta.estado = '0215' and asictrlasistencia.fecinicio <= '" + fecact + "' and '" + fecact + "' <= asictrlasistencia.fecfinal " +
					"and usuario.identidad = '" + identidad + "'";
			
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asictrlasistencia");
			aux.asictrlasi = (Asictrlasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);
			peticion.put("periodo", aux.asictrlasi.getPeriodo());
			result = peticion;
    	} 
    	catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
    	
    	return result;
	}
}