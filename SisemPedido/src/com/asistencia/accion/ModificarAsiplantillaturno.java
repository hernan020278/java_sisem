package com.asistencia.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiplantillahorario;
import com.comun.entidad.Asiplantillaturno;
import com.comun.entidad.Asiturno;
import com.comun.motor.Accion;
import com.comun.motor.AccionListener;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;

public class ModificarAsiplantillaturno extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;

    try {
      Auxiliar aux = Auxiliar.getInstance();

      String kyplantillaturno = (String) peticion.get("Pagina_valor");
      peticion.put("consulta", "select * from asiplantillaturno where kyplantillaturno = " + kyplantillaturno);
      peticion.put("entidadNombre", Asiplantillaturno.class.getSimpleName());
      aux.asiplanturn = (Asiplantillaturno) (new ObtenerEntidad()).ejecutarAccion(peticion);

      peticion.put("consulta",
          "select * from asiplantillahorario where kyplantillahorario = " + aux.asiplanturn.getKyplantillahorario());
      peticion.put("entidadNombre", Asiplantillahorario.class.getSimpleName());
      aux.asiplanhora = (Asiplantillahorario) (new ObtenerEntidad()).ejecutarAccion(peticion);

      result = peticion;
      this.setEstado(Estado.SUCCEEDED);

    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      throw e;
    }
    return result;
  }
}