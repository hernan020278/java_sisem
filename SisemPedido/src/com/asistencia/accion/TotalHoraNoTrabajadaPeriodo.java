package com.asistencia.accion;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

public class TotalHoraNoTrabajadaPeriodo extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    Auxiliar aux = Auxiliar.getInstance();
    DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
    try {
      Date fecact = (Date) peticion.get("fechaActual");

      String sql = "select sum(horanotrabajada) as total " +
          "from asirespositorio inner join asiasistencia on asiasistencia.kyasistencia=asirespositorio.kyasistencia " +
          "where asiasistencia.kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia() + "' " +
          "and asiasistencia.usucodigo = '" + aux.usu.getUsucodigo() + "'";

      String[] listaColumnaTipo = {"BIGDECIMAL"};
      String[] listaColumnaAlias = {"total"};

      peticion.put("listaColumnaTipo", listaColumnaTipo);
      peticion.put("listaColumnaAlias", listaColumnaAlias);

      peticion.put("consulta", sql);
      peticion.put("respuesta", Respuesta.LISTAMIXTA);

      List listaResultado = dbc.ejecutarConsulta(peticion);

      if (listaResultado != null && listaResultado.size() > 0) {
        Object[] registro = (Object[]) listaResultado.get(0);
        if (registro[0] instanceof BigDecimal) {
          result = (BigDecimal) registro[0];
        } else {
          result = new BigDecimal(0);
        }
      }
      this.setEstado(Estado.SUCCEEDED);

    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      throw e;
    }
    return result;
  }

}