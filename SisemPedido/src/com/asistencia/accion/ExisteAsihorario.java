package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asihorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Map;

public class ExisteAsihorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception 
	{
		Object result = null;
		String tipo = "";
		boolean existePapeleta = false;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			Date fecact = (Date) peticion.get("fechaActual");
			Timestamp hora = (Timestamp) peticion.get("hora");
			String dia = (String) peticion.get("dia");

			String sql = "select asihorario.* from asihorario " +
			"where asihorario.estado = 'APROBADO' " + 
			"and ( asihorario.horariotipo = 'SUSPENSION' or asihorario.horariotipo = 'AMONESTACION' or asihorario.horariotipo = 'VACACIONES' " + 
			"or asihorario.horariotipo = 'PERMISO' or asihorario.horariotipo = 'SALIDA' or asihorario.horariotipo = 'DESCANSO MEDICO' or asihorario.horariotipo = 'CITA MEDICA') " +
			"and asihorario.kyusuario = " + aux.usu.getUsucodigo() +
			"and ( '"+fecact+"' >= asihorario.fechainicio and '"+fecact+"' <= asihorario.fechafinal )";

			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asihorario");
			
			Asihorario asihora = (Asihorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if(!Util.getInst().isEmpty(asihora.getKyhorario()))
			{
				tipo = asihora.getHorariotipo();
				existePapeleta = true;
			}
			peticion.put("Asihorario_horariotipo", asihora.getHorariotipo());
			result = existePapeleta;
    	} 
    	catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
    	return result;
	}
}