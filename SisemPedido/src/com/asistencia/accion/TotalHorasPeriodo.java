package com.asistencia.accion;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Asiturno;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class TotalHorasPeriodo extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
		try
		{
			String tipo = (String) peticion.get("Asihorario_tipo");
			String modo = (String) peticion.get("Asihorario_modo");
			String turno = (String) peticion.get("Asihorario_refturno");
			Date fecinicio = (Date) peticion.get("Asihorario_fechainicio");
			Date fecfinal = (Date) peticion.get("Asihorario_fecejefinal");
			
			BigDecimal totalHora = new BigDecimal(0);
			
			if(fecinicio.getTime() <= fecfinal.getTime())
			{
				Date fecactTmp = new Date(fecinicio.getYear(), fecinicio.getMonth(), fecinicio.getDate());
				int numDias = Util.getInst().diferenciaFechas(fecinicio, fecfinal, "DIAS");
				double sumHoras = 0.00;
				if(modo.equals("PERIODO") || modo.equals("TURNO"))
				{
					for(int iteDia = 0; iteDia <= numDias; iteDia++)
					{
						fecactTmp.setDate(fecinicio.getDate() + iteDia);
						peticion.put("fechaActual", fecactTmp);
						
						(new GetListaTurnosByDiaAndDiaAnt()).ejecutarAccion(peticion);
						
						List listaTurno = (List) peticion.get("listaAsiturno");
						for(int iteLis = 0; iteLis < listaTurno.size(); iteLis++)
						{
							Asiturno asiturno = (Asiturno) listaTurno.get(iteLis);
							if(asiturno.getTurnotipo().equals("LABORABLE"))
							{
								if( ( modo.equals("PERIODO") && turno.equals("TODOS") ) || ( modo.equals("TURNO") && turno.equals("TODOS") ) )
								{
									totalHora = totalHora.add(Util.getInst().getTotalHoraDecimal(asiturno.getEntrada(), asiturno.getSalida()));
								}
								else if( ( modo.equals("PERIODO") && turno.equals(asiturno.getTurnonombre()) ) || ( modo.equals("TURNO") && turno.equals(asiturno.getTurnonombre()) ) )
								{
									totalHora = totalHora.add(Util.getInst().getTotalHoraDecimal(asiturno.getEntrada(), asiturno.getSalida()));
								}
							}//if(asiturno.getTipo().equals("LABORABLE"))
						}// for(int iteLis = 0; iteLis < listaTurno.size(); iteLis++)
					}//for(int iteDia = 0; iteDia <= numDias; iteDia++)
				}// if(modo.equals("PERIODO") || modo.equals("TURNO"))
			}// if (fecinicio.getTime() < fecfinal.getTime())
			peticion.put("totalHora", totalHora);
			result = totalHora;
			
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}