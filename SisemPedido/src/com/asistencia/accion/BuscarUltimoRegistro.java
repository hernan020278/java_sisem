package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

import java.sql.Date;
import java.util.Map;

public class BuscarUltimoRegistro extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();
      Date fecact = (Date) peticion.get("fechaActual");
      Date fecant = (Date) peticion.get("fecant");
      String periodo = (String) peticion.get("periodo");
      String identidad = (String) peticion.get("Usuario_identidad");

      if (Util.getInst().isEmpty(aux.asiAsisLast.getKyasistencia())) {
        String sql = "select * from asasistencia where kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia() + "' " +
            "and kuusuario = '" + aux.usu.getUsucodigo() + "' " +
            "and ( ( fecha = '" + fecact + "' and modoregistro = '" + Modo.SALIDA + "' ) " +
            "or ( ( fecha = '" + fecact + "' or fecha = '" + fecant + "' ) and modoregistro = '" + Modo.SALESP + "' ) ) " +
//          "and ( turno = '" + aux.asiturn.getNombre()+ "' or turno='" + Modo.SIN_TURNO + "') " +
            "order by kyasistencia desc";

        peticion.put("consulta", sql);
        peticion.put("entidadNombre", "Asiasistencia");
        aux.asiAsisLast = (Asiasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);
      }

      result = peticion;
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }

    return result;
  }
}