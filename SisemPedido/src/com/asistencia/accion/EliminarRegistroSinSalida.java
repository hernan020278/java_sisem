package com.asistencia.accion;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asiasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class EliminarRegistroSinSalida extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    try {
      
      Auxiliar aux = Auxiliar.getInstance();
      Date fecact = (Date) peticion.get("fechaActual");
      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

      String sql = "select * from asasistencia where kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia() + "' " +
          "and kyusuario = '" + aux.usu.getUsucodigo() + "' " +
          "and fecha <= '" + fecact + "' " +
          "and estadosalida = 'SIN_REGISTRO' and modoregistro = 'INGRESO' ";

      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiasistencia");
      List listaAsiasistencia = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

      for (int iteLis = 0; iteLis < listaAsiasistencia.size(); iteLis++) {
        Asiasistencia asiasis = (Asiasistencia) listaAsiasistencia.get(iteLis);
        peticion.put("Pagina_valor", asiasis.getKyasistencia());
        (new EliminarAsasistencia()).ejecutarAccion(peticion);
      }
      result = peticion;
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }

    return result;
  }
}