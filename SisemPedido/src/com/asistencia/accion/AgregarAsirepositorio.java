package com.asistencia.accion;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Asirepositorio;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class AgregarAsirepositorio extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {
    int afectados = 0;
    try {
      Auxiliar aux = Auxiliar.getInstance();

      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

      BigDecimal newBigDecimal = new BigDecimal(0);
      BigDecimal totalHoraIna = (BigDecimal) peticion.get("totalHoraIna");

      if (totalHoraIna.compareTo(newBigDecimal) == 1) {
        Timestamp hora = (Timestamp) peticion.get("hora");
        String estadoRegistro = (String) peticion.get("estadoRegistro");

        Asirepositorio asirepo = new Asirepositorio();
        Util.getInst().limpiarEntidad(asirepo);

        asirepo.setKyusuario(aux.usu.getUsucodigo());
        asirepo.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
        asirepo.setKyhorario(aux.asihora.getKyhorario());
        asirepo.setKyasistencia(aux.asiasis.getKyasistencia());

        asirepo.setNumerodni(aux.usu.getIdentidad());
        asirepo.setPeriodo(aux.asictrlasi.getPeriodo());
        asirepo.setHorarionombre(aux.asihora.getHorarionombre());
        asirepo.setRepositoriotipo("INASISTENCIA");
        asirepo.setRepositorionombre(estadoRegistro);
        asirepo.setFechahora(hora);
        asirepo.setHoraregistrada(totalHoraIna);
        asirepo.setHoraconsumida(newBigDecimal);
        asirepo.setHorareales(newBigDecimal);
        asirepo.setEstado(Estado.APROBADO);

        String tipoSql = TipoSql.INSERT;
        peticion.put("tipoSql", tipoSql);
        peticion.put("entidadObjeto", asirepo);

        afectados = dbc.ejecutarCommit(peticion);
      }
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
    return afectados;
  }
}