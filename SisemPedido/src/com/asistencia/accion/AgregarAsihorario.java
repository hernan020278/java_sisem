package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asihorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

import java.math.BigDecimal;
import java.util.Map;

public class AgregarAsihorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			BigDecimal codigo = new BigDecimal(KeyGenerator.getInst().getUniqueKey().toString());
			String sql = "SELECT * FROM asihorario WHERE numerodni='" + aux.usu.getIdentidad() +
					"' AND periodo='" + aux.asictrlasi.getPeriodo() + "'";
			
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asihorario");
			aux.asihora = (Asihorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			if(Util.isEmpty(aux.asihora.getKyhorario()))
			{
				aux.asihora.setKyhorario(KeyGenerator.getInst().getKyTabla());
				aux.asihora.setKyusuario(aux.usu.getUsucodigo());
				aux.asihora.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
				aux.asihora.setNumerodni(aux.usu.getIdentidad());
				aux.asihora.setPeriodo(aux.asictrlasi.getPeriodo());
				aux.asihora.setHorariotipo(Estado.HORARIO);
				aux.asihora.setFechainicio(Util.getInst().fromDateToTimestamp(aux.asictrlasi.getFechainicio()));
				aux.asihora.setFechafinal(Util.getInst().fromDateToTimestamp(aux.asictrlasi.getFechafinal()));
				aux.asihora.setEstado(Estado.ACTIVO);
				
				String tipoSql = TipoSql.INSERT;
				peticion.put("tipoSql", tipoSql);
				peticion.put("entidadObjeto", aux.asihora);
				afectados = dbc.ejecutarCommit(peticion);
			}
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}