package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiturno;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public class ObtenerHorarioTurno extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();
      Date fechaActual = (Date) peticion.get("fechaActual");
      Time horaIngreso = (Time) peticion.get("horaIngreso");
      Time horaSalida = (Time) peticion.get("horaSalida");

      String dia = (String) peticion.get("dia");

      String sql = "";
      sql = "select * from asiturno where kyturno = '" + aux.asiAsisLast.getKyturno() + "' ";
      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiturno");
      Asiturno turnTmpUno = (Asiturno) (new ObtenerEntidad()).ejecutarAccion(peticion);

      List listaAsiturno = (List) peticion.get("listaAsiturno");

      Asiturno asiturn = new Asiturno();
      Asiturno asturnFirsPos = new Asiturno();

      Asiturno turnTmpDos = new Asiturno();
      Util.getInst().limpiarEntidad(turnTmpDos, true);
      Util.getInst().limpiarEntidad(asiturn, true);
      Util.getInst().limpiarEntidad(asturnFirsPos, true);
      
      for (int iteUno = 0; iteUno < listaAsiturno.size(); iteUno++) {
        Util.getInst().imprimiValores(asiturn);
        Util.getInst().imprimiValores(asturnFirsPos);

        if (listaAsiturno.size() == 1)
        {
          asiturn = (Asiturno) listaAsiturno.get(iteUno);
          Util.getInst().clonarEntidad(asiturn, turnTmpDos);
          break;
        }
        else
        {
          asiturn = (Asiturno) listaAsiturno.get(iteUno); // ESTABLECEMOS LA HORA CERO
          
          if ((iteUno + 1) == listaAsiturno.size()) {
            Util.getInst().clonarEntidad(asturnFirsPos, turnTmpDos);
            break;
          } else {
            asturnFirsPos = (Asiturno) listaAsiturno.get(iteUno + 1);
            if (Util.getInst().existeHoraEnRangoIngSal(horaIngreso, horaSalida, asturnFirsPos.getEntrada(), asturnFirsPos.getSalida()) && asturnFirsPos.getTurnodia().equals(dia)) {
              Util.getInst().clonarEntidad(asturnFirsPos, turnTmpDos);
              break;
            } else if (Util.getInst().existeHoraEnRangoIngSal(horaIngreso, horaSalida, asiturn.getSalida(), asturnFirsPos.getEntrada()) && asiturn.getTurnodia().equals(dia)) {
              Util.getInst().clonarEntidad(asiturn, turnTmpDos);
              break;
            } else if (Util.getInst().existeHoraEnRangoIngSal(horaIngreso, horaSalida, asiturn.getEntrada(), asiturn.getSalida()) && asiturn.getTurnodia().equals(dia)) {
              Util.getInst().clonarEntidad(asiturn, turnTmpDos);
              break;
            } else if (Util.getInst().menorHoraInicio(horaIngreso, horaSalida, asiturn.getEntrada()) && asiturn.getTurnodia().equals(dia)) {
              Util.getInst().clonarEntidad(asiturn, turnTmpDos);
              break;
            }
          }
        }
      }// for(int iteUno = 0; iteUno < listaAsiturno.size(); iteUno++)

      if (!aux.asiAsisLast.getEstadosalida().equals(Modo.SIN_REGISTRO) && !(turnTmpDos.getKyturno() == turnTmpUno.getKyturno())) {
        aux.asiturn = turnTmpDos;
        Util.getInst().limpiarEntidad(aux.asiAsisLast, true);
      }//if(aux.asiturn.getTurcodigo().equals(turnTmpUno.getTurcodigo())){
      else {
        aux.asiturn = turnTmpUno;
      }

      java.sql.Date fechaInicio = new Date(aux.asiturn.getEntrada().getYear(), aux.asiturn.getEntrada().getMonth(), aux.asiturn.getEntrada().getDate());
      java.sql.Date fechaFinal = new Date(aux.asiturn.getSalida().getYear(), aux.asiturn.getSalida().getMonth(), aux.asiturn.getSalida().getDate());

      if(!Util.getInst().isEmpty(aux.asiAsisLast.getKyasistencia()))
      {
        fechaActual = new Date(aux.asiAsisLast.getFecha().getTime());
      }

      sql = "select * from asihorario where kyhorario = " + aux.asiturn.getKyhorario();
      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asihorario");

      aux.asihora = (Asihorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
      result = peticion;
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
    return result;
  }
}