package com.asistencia.accion;

import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

import javax.swing.*;
import java.util.Map;

public class ObtenerModoRegistro extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {
    Object result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();
      String modoRegistro = "";

      if (Util.getInst().isEmpty(aux.asiAsisLast.getKyasistencia()))
      {
        modoRegistro = Modo.INGRESO;
      }
      else
      {
        if (aux.asiAsisLast.getModoregistro().equals(Modo.INGRESO))
        {
          (new ObtenerHorario()).ejecutarAccion(peticion);

          modoRegistro = Modo.SALIDA;
          if (aux.asihora.getHorariotipo().equals("FLEXIBLE")) {
            int rpta = JOptionPane.showConfirmDialog(null, "Fin de jornada", "Horario Especial", JOptionPane.YES_NO_OPTION);
            if (rpta == JOptionPane.NO_OPTION) {
              modoRegistro = Modo.SALESP;
            }
          }
        }
        else if (aux.asiAsisLast.getModoregistro().equals(Modo.SALIDA) || aux.asiAsisLast.getModoregistro().equals(Modo.SALESP))
        {
          modoRegistro = Modo.INGRESO;
        }
      }//Fin de evaluar que existe algun registro de asistencia

      peticion.put("modoRegistro", modoRegistro);
      result = peticion;
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }

    return result;
  }
}