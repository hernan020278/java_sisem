package com.asistencia.accion;

import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;

import java.sql.Timestamp;
import java.util.Map;

public class ObtenerHoraIngresoSalida extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {
    Map result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();

      Timestamp horaIngreso = (Timestamp) peticion.get("horaIngreso");
      Timestamp horaSalida = (Timestamp) peticion.get("horaSalida");
      Timestamp hora = (Timestamp) peticion.get("hora");

      String modoRegistro = (String) peticion.get("modoRegistro");

      if (modoRegistro.equals(Modo.INGRESO)) {
        horaIngreso = new Timestamp(hora.getTime());
        horaSalida = new Timestamp(18000000);
      } else if (modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)) {
        horaIngreso = new Timestamp(aux.asiAsisLast.getHoraingreso().getTime());
        horaSalida = new Timestamp(hora.getTime());
      }
      peticion.put("horaIngreso", horaIngreso);
      peticion.put("horaSalida", horaSalida);

      result = peticion;
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
    return result;    // Timestamp parHorIni, Timestamp parHorFin, Timestamp parHorLimTar, Timestamp parHorLimSal
  }
}