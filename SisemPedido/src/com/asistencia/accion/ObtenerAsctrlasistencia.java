package com.asistencia.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asictrlasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class ObtenerAsctrlasistencia extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			String ctrlasicodigo = (String) peticion.get("Asictrlasistencia_kyctrlasistencia");

			peticion.put("consulta", "select * from asictrlasistencia where ctrlasicodigo='" + ctrlasicodigo + "'");
			peticion.put("entidadNombre", "Asictrlasistencia");
			result = (Asictrlasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}