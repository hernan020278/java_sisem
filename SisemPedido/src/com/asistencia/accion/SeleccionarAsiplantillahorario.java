package com.asistencia.accion;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asiplantillahorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class SeleccionarAsiplantillahorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			Auxiliar aux = Auxiliar.getInstance();
			BigDecimal papcodigo = new BigDecimal((String) peticion.get("Pagina_valor"));
			
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			boolean exito = true;
			
			String sql = "select * from asiplantillahorario where kyplantillahorario = " + papcodigo;
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", Asiplantillahorario.class.getSimpleName());
			aux.asiplanhora = (Asiplantillahorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}