package com.asistencia.accion;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class GetHorasTrabajadasSalida extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {
		int afectados = 0;
		Object result = null;
        try 
        {
    		Auxiliar aux = Auxiliar.getInstance();
    		
    		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion"); 

    		String estadoRegistro = (String) peticion.get("estadoRegistro");
    		String modoRegistro = (String) peticion.get("modoRegistro");
    		
    		Date fecact = (Date) peticion.get("fechaActual");
    		
    		Timestamp horaIngreso = (Timestamp) peticion.get("horaIngreso");
    		Timestamp horaSalida = (Timestamp) peticion.get("horaSalida");
    		
//    		Timestamp horaIngreso = horaIngreso;
//    		Timestamp horaSalida = horaSalida;
    		
//    		Timestamp hora = (Timestamp) peticion.get("hora");
    		Timestamp horaConsIng = new Timestamp(0);
    		Timestamp horaConsSal = new Timestamp(0);
    		Timestamp horaContIng = new Timestamp(0);
    		Timestamp horaContSal = new Timestamp(0);
    		
    		BigDecimal newBigDecimal = new BigDecimal(0);
    		BigDecimal totalHoraCons = newBigDecimal;
    		BigDecimal totalHoraCont = newBigDecimal;
    		BigDecimal totalHoraIna = newBigDecimal;
    		BigDecimal totalHoraExt = newBigDecimal;
    		BigDecimal totalHoraTrab = newBigDecimal;
    		
    		BigDecimal horaCons = newBigDecimal;
    		BigDecimal horaCont = newBigDecimal;
    		
    		BigDecimal horaOblTurn = Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), aux.asiturn.getSalida());
    		BigDecimal horaTrabTurn = (BigDecimal) (new TotalHoraTrabajadaTurno()).ejecutarAccion(peticion); 
    		BigDecimal horaNoTrabTurn = (BigDecimal) (new TotalHoraNoTrabajadaTurno()).ejecutarAccion(peticion);

    		/*
    		 * ESTABLECEMOS CONDICIONES PARA EL INGRESO
    		 */
			if(modoRegistro.equals(Modo.INGRESO))
			{
				if(estadoRegistro.equals(Modo.INGRESO_LIM_1)
				|| estadoRegistro.equals(Modo.INGRESO_LIM_2) || estadoRegistro.equals(Modo.INGRESO_LIM_3)
				|| estadoRegistro.equals(Modo.INGRESO_LIM_4))
				{
					/*
					 * HORAS CONSIDERADAS Y CONTABILIZADAS
					 */
					if(Util.getInst().existeHoraEnRango(horaIngreso, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
					{
						totalHoraIna = Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), horaIngreso);
						totalHoraCons = newBigDecimal;
						totalHoraCont = newBigDecimal;
						totalHoraTrab = newBigDecimal;
					}
				}
				else if(estadoRegistro.equals(Modo.INGRESO_FUERA_TURNO))
				{
					totalHoraIna = newBigDecimal;
					totalHoraCons = newBigDecimal;
					totalHoraCont = newBigDecimal;
					totalHoraTrab = newBigDecimal;
				}
				else if(estadoRegistro.equals(Modo.INGRESO_MULTIPLE))
				{
					if(Util.getInst().existeHoraEnRango(horaIngreso, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
					{
						(new EliminarRepositorio()).ejecutarAccion(peticion);
						totalHoraIna = Util.getInst().getTotalHoraDecimal(aux.asiAsisLast.getHorasalida(), horaIngreso);
						totalHoraCons = newBigDecimal;
						totalHoraCont = newBigDecimal;
						totalHoraTrab = newBigDecimal;
					}
				}
				else // CONDICION PARA REGISTRAR LOS INGRESOS EN FERIADO, CON CUALQUIER TIPO DE PAPELETA
				{
					totalHoraIna = newBigDecimal;
					totalHoraCons = newBigDecimal;
					totalHoraCont = newBigDecimal;
					totalHoraTrab = newBigDecimal;
				}
			}// if(modoRegistro.equals(Modo.INGRESO))
    		/*
    		 * ESTABLECEMOS CONDICIONES PARA LA SALIDA
    		 */
			if(modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP))
			{
				if(estadoRegistro.equals(Modo.SALIDA_LIM_1) || estadoRegistro.equals(Modo.SALIDA_LIM_2) || estadoRegistro.equals(Modo.SALIDA_LIM_3)
				|| estadoRegistro.equals(Modo.SALIDA_LIM_4) || estadoRegistro.equals(Modo.SALIDA_ANTES_HORA))
				{
					totalHoraIna = Util.getInst().getTotalHoraDecimal(horaSalida, aux.asiturn.getSalida());
					totalHoraCons = Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), horaSalida);
					if(horaIngreso.getTime() <= aux.asiturn.getEntrada().getTime() && horaSalida.getTime() >= aux.asiturn.getSalida().getTime())
					{
						totalHoraCont = Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), aux.asiturn.getSalida());
						totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
						totalHoraExt = Util.getInst().getTotalHoraDecimal(horaIngreso, aux.asiturn.getEntrada());
						totalHoraExt = totalHoraExt.add(Util.getInst().getTotalHoraDecimal(aux.asiturn.getSalida(), horaSalida));
					} else if((horaIngreso.getTime() <= aux.asiturn.getEntrada().getTime() && horaSalida.getTime() <= aux.asiturn.getEntrada().getTime())
							|| (horaIngreso.getTime() >= aux.asiturn.getSalida().getTime() && horaSalida.getTime() >= aux.asiturn.getSalida().getTime()))
					{
						totalHoraCont = newBigDecimal;
						totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
						totalHoraExt = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
					}
					else if(horaIngreso.getTime() <= aux.asiturn.getEntrada().getTime() && Util.getInst().existeHoraEnRango(horaSalida, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
					{
						totalHoraCont = Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), horaSalida);
						totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
						totalHoraExt = totalHoraExt.add(Util.getInst().getTotalHoraDecimal(horaIngreso, aux.asiturn.getEntrada()));
					}
					else if(horaSalida.getTime() >= aux.asiturn.getSalida().getTime() && Util.getInst().existeHoraEnRango(horaIngreso, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
					{
						totalHoraCont = Util.getInst().getTotalHoraDecimal(horaIngreso, aux.asiturn.getSalida());
						totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
						totalHoraExt = totalHoraExt.add(Util.getInst().getTotalHoraDecimal(aux.asiturn.getSalida(), horaSalida));
					}
					else if(Util.getInst().existeHoraEnRango(horaIngreso, aux.asiturn.getEntrada(), aux.asiturn.getSalida())
							&& Util.getInst().existeHoraEnRango(horaSalida, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
					{
						totalHoraCont = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
						totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
						totalHoraExt = newBigDecimal;
					}
				}
				else if(estadoRegistro.equals(Modo.SALIDA_FUERA_TURNO))
				{
					totalHoraIna = newBigDecimal;
					totalHoraCons = newBigDecimal;
					totalHoraCont = newBigDecimal;
					totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
					if(!Util.getInst().isEmpty(aux.asiturn.getKyhorario()))
					{
						if(horaIngreso.getTime() <= aux.asiturn.getEntrada().getTime() && horaSalida.getTime() >= aux.asiturn.getSalida().getTime())
						{
							totalHoraCont = Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), aux.asiturn.getSalida());
							totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
							totalHoraExt = Util.getInst().getTotalHoraDecimal(horaIngreso, aux.asiturn.getEntrada());
							totalHoraExt = totalHoraExt.add(Util.getInst().getTotalHoraDecimal(aux.asiturn.getSalida(), horaSalida));
						}
						else if((horaIngreso.getTime() <= aux.asiturn.getEntrada().getTime() && horaSalida.getTime() <= aux.asiturn.getEntrada().getTime())
								|| (horaIngreso.getTime() >= aux.asiturn.getSalida().getTime() && horaSalida.getTime() >= aux.asiturn.getSalida().getTime()))
						{
							totalHoraCont = newBigDecimal;
							totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
							totalHoraExt = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
						}
						else if(horaIngreso.getTime() <= aux.asiturn.getEntrada().getTime() && Util.getInst().existeHoraEnRango(horaSalida, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
						{
							totalHoraCont = Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), horaSalida);
							totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
							totalHoraExt = totalHoraExt.add(Util.getInst().getTotalHoraDecimal(horaIngreso, aux.asiturn.getEntrada()));
						}
						else if(horaSalida.getTime() >= aux.asiturn.getSalida().getTime() && Util.getInst().existeHoraEnRango(horaIngreso, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
						{
							totalHoraCont = Util.getInst().getTotalHoraDecimal(horaIngreso, aux.asiturn.getSalida());
							totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
							totalHoraExt = totalHoraExt.add(Util.getInst().getTotalHoraDecimal(aux.asiturn.getSalida(), horaSalida));
						}
						else if(Util.getInst().existeHoraEnRango(horaIngreso, aux.asiturn.getEntrada(), aux.asiturn.getSalida())
								&& Util.getInst().existeHoraEnRango(horaSalida, aux.asiturn.getEntrada(), aux.asiturn.getSalida()))
						{
							totalHoraCont = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
							totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
							totalHoraExt = newBigDecimal;
						}
					}// if(Util.getInst().isEmpty(aux.asiturn.getTurcodigo().toString()))
				}
				else // CONDICION PARA REGISTRAR LA SALIDA EN FERIADO O CON CUALQUIER TIPO DE PAPELETA
				{
					totalHoraIna = newBigDecimal;
					totalHoraCons = newBigDecimal;
					totalHoraCont = newBigDecimal;
					totalHoraTrab = Util.getInst().getTotalHoraDecimal(horaIngreso, horaSalida);
				}
			}// if(modoRegistro.equals(Modo.SALIDA))
            
    		peticion.put("totalHoraExt", totalHoraExt);
    		peticion.put("totalHoraIna", totalHoraIna);
            peticion.put("totalHoraCons", totalHoraCons);
            peticion.put("totalHoraCont", totalHoraCont);
            peticion.put("totalHoraTrab", totalHoraTrab);
            
        }
        catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
        return result;
	}
}