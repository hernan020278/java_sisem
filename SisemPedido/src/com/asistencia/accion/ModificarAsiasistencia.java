package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

import java.util.Map;

public class ModificarAsiasistencia extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
					
			String asicodigo = (String) peticion.get("Pagina_valor");
			peticion.put("consulta", "select * from asiasistencia where kyasistencia = " + asicodigo);
			peticion.put("entidadNombre", "Asiasistencia");
			aux.asiasis = (Asiasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}