package com.asistencia.accion;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.database.DBConeccion;
import com.comun.entidad.Asiasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class MarcarAsistencia extends Accion 
{
	public Object ejecutarAccion(Map peticion) throws Exception 
	{
		Object result = null;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();

			Date fecact = (Date) peticion.get("Asasistencia_fecha");
			Date fecant = new Date(fecact.getYear(), fecact.getMonth() , fecact.getDate() - 1);
			Date fecpos = new Date(fecact.getYear(), fecact.getMonth() , fecact.getDate() + 1);
			Timestamp hora = (Timestamp) peticion.get("Asasistencia_hora");

			String dia = Util.getInst().strDia[fecact.getDay()];
			String diaant = Util.getInst().strDia[fecant.getDay()];
			String diapos = Util.getInst().strDia[fecpos.getDay()];
        	
			peticion.put("fechaActual", fecact);
			peticion.put("fecant", fecant);
			peticion.put("fecpos", fecpos);
			peticion.put("hora", hora);
			peticion.put("dia", dia);
			peticion.put("diaant", diaant);
			peticion.put("diaant", diapos);
			
			/*
			 * CAMBIAR CUANDO SE PONE EN FORMULAIRO
			 */
			
			aux.asiasis = new Asiasistencia();
			aux.asiAsisLast = new Asiasistencia();
			Util.getInst().limpiarEntidad(aux.asiasis);
			Util.getInst().limpiarEntidad(aux.asiAsisLast);
			
			iniciarVariables(peticion);

			(new ObtenerCtrlasistencia()).ejecutarAccion(peticion);
			
			(new ObtenerAsiPlantillaHorario()).ejecutarAccion(peticion);
			
			(new BuscarRegistroSinSalida()).ejecutarAccion(peticion);
			
			(new BuscarUltimoRegistro()).ejecutarAccion(peticion);
			
			(new ObtenerModoRegistro()).ejecutarAccion(peticion);
			
			(new ObtenerHoraIngresoSalida()).ejecutarAccion(peticion);
			
			(new GetListaTurnosByDiaAndDiaAnt()).ejecutarAccion(peticion);
			
			(new ObtenerHorarioTurno()).ejecutarAccion(peticion);
			
			(new EvaluarEstadoRegistro()).ejecutarAccion(peticion);
			
			(new GetHorasTrabajadasSalida()).ejecutarAccion(peticion);
			
			(new GuardarAsiasistencia()).ejecutarAccion(peticion);
			
			(new AgregarAsirepositorio()).ejecutarAccion(peticion);
			
			(new AgregarExtra()).ejecutarAccion(peticion);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
	
    public void iniciarVariables(Map peticion) {

        peticion.put("turno", Modo.SIN_TURNO);
        peticion.put("estadoRegistro", Modo.SIN_REGISTRO); 
        peticion.put("modoRegistro", Modo.SIN_REGISTRO); 
        
        peticion.put("horaIngreso", new Timestamp(0));
        peticion.put("horaSalida", new Timestamp(0));
        
        peticion.put("almacenarRegistro", false);
        peticion.put("tienePermiso", false);
        peticion.put("tieneControlFeriado", false);
        peticion.put("existeFeriado", false);
        peticion.put("varUsuVal", false);
        
        BigDecimal newBigDecimal = new BigDecimal(0.00);
        peticion.put("totalHoraTrab", newBigDecimal);
        peticion.put("totalHoraCons", newBigDecimal);
        peticion.put("totalHoraCont", newBigDecimal);
        peticion.put("totalHoraObl", newBigDecimal);
        
    }//Metodo para iniciar Variables
    
    public static void main(String[] args)
    {
    	SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() 
			{
				Map peticion = new HashMap();
				try {
					peticion.put("dbConeccion", new DBConeccion("SISEM"));
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Util.getInst().fechaTemp.set(2014, 8-1, 1, 8, 45, 0);
				Timestamp hora = new Timestamp(Util.getInst().fechaTemp.getTimeInMillis());
				Date fecha = new Date(hora.getTime());
				peticion.put("Usuario_identidad", "10793205");
				peticion.put("Asasistencia_fecha", fecha);
				peticion.put("Asasistencia_hora", hora);

				try {
					(new MarcarAsistencia()).ejecutarAccion(peticion);
//					Log.debug(this, "ffff");
//					Timestamp horaIngreso = new Timestamp(24,0,0);
//					Timestamp horaSalida = new Timestamp(0,0,0);
//					horaSalida.setHours(0);
//					horaSalida.setMinutes(0);
//					horaSalida.setSeconds(0);
//					
//					System.out.println( "fechoract : " + fechoract.toString() + " hora : " + Util.getInst().fromTimeToString(hora));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
    }
}