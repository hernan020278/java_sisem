package com.asistencia.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;

public class AgregarAsiturno extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			aux.asiturn.setKyturno(KeyGenerator.getInst().getKyTabla());
			aux.asiturn.setKyusuario(aux.usu.getUsucodigo());
			aux.asiturn.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
			aux.asiturn.setKyhorario(aux.asihora.getKyhorario());
			aux.asiturn.setNumerodni(aux.usu.getIdentidad());
			aux.asiturn.setPeriodo(aux.asictrlasi.getPeriodo());
			aux.asiturn.setHorarionombre(aux.asiplanhora.getDescripcion());
			aux.asiturn.setTurnonombre(aux.asiplanturn.getTurnonombre());
			aux.asiturn.setTurnodia(aux.asiplanturn.getTurnodia());
      aux.asiturn.setTurnotipo(aux.asiplanturn.getTurnotipo());
      aux.asiturn.setEntrada(aux.asiplanturn.getEntrada());
      aux.asiturn.setLimentradauno(aux.asiplanturn.getLimentradauno());
      aux.asiturn.setLimentradados(aux.asiplanturn.getLimentradados());
      aux.asiturn.setLimentrada(aux.asiplanturn.getLimentrada());
      aux.asiturn.setLimsalidauno(aux.asiplanturn.getLimsalidauno());
      aux.asiturn.setLimsalidados(aux.asiplanturn.getLimentradados());
      aux.asiturn.setLimsalida(aux.asiplanturn.getLimsalida());
      aux.asiturn.setSalida(aux.asiplanturn.getSalida());
      aux.asiturn.setEstado(Estado.ACTIVO);
      
			String tipoSql = TipoSql.INSERT;
			peticion.put("entidadObjeto", aux.asiturn);
			peticion.put("tipoSql", tipoSql);
			peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where kyturno = '" + aux.asiturn.getKyturno() + "'"));
			afectados = dbc.ejecutarCommit(peticion);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}