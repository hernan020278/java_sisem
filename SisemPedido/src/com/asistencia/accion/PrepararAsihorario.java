package com.asistencia.accion;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

import java.util.Map;

public class PrepararAsihorario extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();

      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

      aux.asihora.setKyusuario(aux.usu.getUsucodigo());
      aux.asihora.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
      aux.asihora.setNumerodni(aux.usu.getIdentidad());
      aux.asihora.setPeriodo(aux.asictrlasi.getPeriodo());
      aux.asihora.setHorariotipo("HORARIO");
      aux.asihora.setHorarionombre("HORARIO_GENERAL");
      aux.asihora.setEstado(Estado.APROBADO);

      peticion.put("Asictrlasistencia_periodo", aux.asictrlasi.getPeriodo());
      (new AgregarAsihorario()).ejecutarAccion(peticion);

      result = peticion;
      this.setEstado(Estado.SUCCEEDED);
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      throw e;
    }
    return result;
  }
}