package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asictrlasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;

import java.sql.Date;
import java.util.Map;

public class AgregarAsictrlasistencia extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {
    Object result = null;

    try {
      Auxiliar aux = Auxiliar.getInstance();

      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
      String Usuario_identidad = (String) peticion.get("Usuario_identidad");
      String periodo = (String) peticion.get("Asictrlasistencia_periodo");
      Date fechaActual = (Date) peticion.get("fechaActual");
      Date fechaAnterior = Util.getInst().fromDateToDateSql(Util.getInst().agregarMes(fechaActual, -1));

      Asictrlasistencia asiCtrlasiActual = new Asictrlasistencia();
      Date fechainicio = new Date(Util.getInst().getFechaSql().getTime());
      Date fechafinal = new Date(Util.getInst().getFechaSql().getTime());
      int diasMes = Util.getInst().obtenerDiasDelMes(Util.getInst().getFechaSql());

      String sql = "select * from asictrlasistencia " +
          "where asictrlasistencia.fechainicio <= '" + fechaActual + "' and '" + fechaActual + "' <= asictrlasistencia.fechafinal " +
          "and asictrlasistencia.numerodni = '" + Usuario_identidad + "'";

      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asictrlasistencia");
      asiCtrlasiActual = (Asictrlasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);

      asiCtrlasiActual.setNumerodni(aux.usu.getIdentidad());

      if (Util.isEmpty(asiCtrlasiActual.getKyctrlasistencia())) {

        sql = "select * from asictrlasistencia " +
            "where asictrlasistencia.fechainicio <= '" + fechaAnterior + "' and '" + fechaAnterior + "' <= asictrlasistencia.fechafinal " +
            "and asictrlasistencia.numerodni = '" + Usuario_identidad + "'";

        peticion.put("consulta", sql);
        peticion.put("entidadNombre", "Asictrlasistencia");
        Asictrlasistencia asctrlAnterior = (Asictrlasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);

        if (!Util.isEmpty(asiCtrlasiActual.getKyctrlasistencia()))
        {
          fechainicio.setTime(Util.getInst().fromDateToDateSql(Util.getInst().agregarDias(asctrlAnterior.getFechainicio(), 1)).getTime());
          fechafinal.setTime(Util.getInst().fromDateToDateSql(Util.getInst().agregarMes(asctrlAnterior.getFechafinal(), 1)).getTime());
        }
        else
        {
          fechainicio.setDate(1);
          fechafinal.setDate(diasMes);
        }

        periodo = Util.getInst().obtenerPeriodo(fechainicio);

        asiCtrlasiActual.setKyctrlasistencia(KeyGenerator.getInst().getKyTabla());
        asiCtrlasiActual.setPeriodo(periodo);
        asiCtrlasiActual.setFechainicio(fechainicio);
        asiCtrlasiActual.setFechafinal(fechafinal);
        asiCtrlasiActual.setEstado(Estado.ACTIVO);

        String tipoSql = TipoSql.INSERT;
        peticion.put("tipoSql", tipoSql);
        peticion.put("entidadObjeto", asiCtrlasiActual);

        afectados = dbc.ejecutarCommit(peticion);
      }
      aux.asictrlasi = asiCtrlasiActual;
      result = peticion;
      this.setEstado(Estado.SUCCEEDED);

    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      throw e;
    }
    return result;
  }

}