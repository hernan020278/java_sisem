package com.asistencia.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;

public class EliminarAsasistencia extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String asicodigo = (String) peticion.get("Pagina_valor");
			
    		String sql = "delete from asrepositorio where asicodigo = " + asicodigo;
			peticion.put("consulta", sql);
			peticion.put("tipoSql", TipoSql.BATCH);
			afectados = dbc.ejecutarCommit(peticion);
			
    		sql = "delete from asasistencia where asicodigo = " + asicodigo;
			peticion.put("consulta", sql);
			peticion.put("tipoSql", TipoSql.BATCH);
			afectados = dbc.ejecutarCommit(peticion);
			peticion.put("afectados", afectados);
			
			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}