package com.asistencia.accion;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

import javax.swing.*;
import java.math.BigDecimal;
import java.sql.*;
import java.util.Map;

/**
 * Guardarmos un registro de asistencia estableciendo el modo en que se esta registrando INGRESO, SALIDA o FALTA
 *
 * @param aux.asictrlasi
 * @param aux.usu
 * @param aux.asiturn
 * @param fecha
 * @param turno
 * @author hernan
 * @return void
 */

public class GuardarAsiasistencia extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    int afectados = 0;
    try {
      Auxiliar aux = Auxiliar.getInstance();

      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

      BigDecimal totalHoraIna = (BigDecimal) peticion.get("totalHoraIna");

      Date fecact = (Date) peticion.get("fechaActual");
      Timestamp horaIngreso = (Timestamp) peticion.get("horaIngreso");
      Timestamp horaSalida = (Timestamp) peticion.get("horaSalida");
      String estadoRegistro = (String) peticion.get("estadoRegistro");
      String modoRegistro = (String) peticion.get("modoRegistro");

      BigDecimal totalHoraTrab = (BigDecimal) peticion.get("totalHoraTrab");
      BigDecimal totalHoraCons = (BigDecimal) peticion.get("totalHoraCons");
      BigDecimal totalHoraCont = (BigDecimal) peticion.get("totalHoraCont");
      BigDecimal asisHorExt = (BigDecimal) peticion.get("asisHorExt");

      if (modoRegistro.equals(Modo.INGRESO)) {
        aux.asiasis.setKyusuario(aux.usu.getUsucodigo());
        aux.asiasis.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
        aux.asiasis.setKyhorario(aux.asihora.getKyhorario());
        aux.asiasis.setNumerodni(aux.usu.getIdentidad());
        aux.asiasis.setPeriodo(aux.asictrlasi.getPeriodo());
        aux.asiasis.setHorarionombre(aux.asihora.getHorarionombre());
        aux.asiasis.setFecha(fecact);
        aux.asiasis.setTurnonombre(Util.getInst().isEmpty(aux.asiturn.getTurnonombre()) ? Modo.SIN_TURNO : aux.asiturn.getTurnonombre());
        aux.asiasis.setTurnodia(aux.asiturn.getTurnodia());
        aux.asiasis.setTurnotipo(aux.asiturn.getTurnotipo());
        aux.asiasis.setHoraingreso(horaIngreso);
        aux.asiasis.setEstadoingreso(estadoRegistro);
        aux.asiasis.setHorasalida(horaSalida);
        aux.asiasis.setEstadosalida(Modo.SIN_REGISTRO);
        aux.asiasis.setHoratrabajada(totalHoraTrab);
        aux.asiasis.setHoraconsumida(totalHoraCons);
        aux.asiasis.setHoracomputada(totalHoraCont);
        aux.asiasis.setModoregistro(modoRegistro);

        String tipoSql = TipoSql.INSERT;
        peticion.put("tipoSql", tipoSql);
        peticion.put("entidadObjeto", aux.asiasis);
        afectados = dbc.ejecutarCommit(peticion);

        actualizarUsuarioComark(aux.usu.getIdentidad(), Modo.INGRESO);
      } else if (modoRegistro.equals(Modo.SALIDA) || modoRegistro.equals(Modo.SALESP)) {
        Util.getInst().clonarEntidad(aux.asiAsisLast, aux.asiasis);
        aux.asiasis.setHorasalida(horaSalida);
        aux.asiasis.setEstadosalida(estadoRegistro);
        aux.asiasis.setHoratrabajada(totalHoraTrab);
        aux.asiasis.setHoraconsumida(totalHoraCons);
        aux.asiasis.setHoracomputada(totalHoraCont);
        aux.asiasis.setModoregistro(modoRegistro);

        String tipoSql = TipoSql.UPDATE;
        peticion.put("tipoSql", tipoSql);
        peticion.put("entidadObjeto", aux.asiasis);
        afectados = dbc.ejecutarCommit(peticion);

        actualizarUsuarioComark(aux.usu.getIdentidad(), Modo.SALIDA);
      }
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
    return afectados;
  }

  private void actualizarUsuarioComark(String parUsuCodi, String parUsuEsta) {
    try {
      String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
      String urldbc = "jdbc:sqlserver://192.168.1.10:1433;databaseName=dbsistemabase1";
      Class.forName(driver);
      Connection coneccion = DriverManager.getConnection(urldbc, "usuario", "020278");
      coneccion.setAutoCommit(true);

      String sql = "UPDATE usuario SET usu_estado='" + Modo.SALIDA + "'";
      PreparedStatement pstm = coneccion.prepareStatement(sql);
      afectados = pstm.executeUpdate();

      sql = "UPDATE usuario SET usu_estado='" + parUsuEsta + "' WHERE usu_ide='" + parUsuCodi + "'";
      pstm = coneccion.prepareStatement(sql);
      afectados = pstm.executeUpdate();
    } catch (ClassNotFoundException e) {
      JOptionPane.showConfirmDialog(null,
          "No se encontro el controlador de Base de Datos!!!",
          "Error de archivo", JOptionPane.PLAIN_MESSAGE,
          JOptionPane.INFORMATION_MESSAGE);

      throw new RuntimeException(e);
    } catch (SQLException e) {
      JOptionPane.showConfirmDialog(null, "Error de Sentencia SQL - 5!!!",
          "Error de conexion", JOptionPane.PLAIN_MESSAGE,
          JOptionPane.INFORMATION_MESSAGE);
      throw new RuntimeException(e);
    }
  }
}