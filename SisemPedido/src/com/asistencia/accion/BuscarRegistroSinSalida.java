package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiasistencia;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

import java.sql.Date;
import java.util.Map;

public class BuscarRegistroSinSalida extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();
      Date fecact = (Date) peticion.get("fechaActual");
      Date fecant = (Date) peticion.get("fecant");

      String sql = "select * from asiasistencia where kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia() + "' " +
          "and kyusuario = '" + aux.usu.getUsucodigo() + "' " +
          //"and fecha = '" + fecact + "' " +
          "and estadosalida = 'SIN_REGISTRO' and modoregistro = 'INGRESO' " +
          "order by kyasistencia desc";

      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiasistencia");
      aux.asiAsisLast = (Asiasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);

      if (Util.getInst().isEmpty(aux.asiAsisLast.getKyasistencia())) {
        sql = "select * from asiasistencia where kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia() + "' " +
            "and kyusuario = '" + aux.usu.getUsucodigo() + "' " +
            "and fecha = '" + fecant + "' " +
            "and estadosalida = 'SIN_REGISTRO' and modoregistro = 'INGRESO' " +
            "order by kyasistencia desc";

        peticion.put("consulta", sql);
        peticion.put("entidadNombre", "Asiasistencia");
        aux.asiAsisLast = (Asiasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);
      }

      result = peticion;
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }

    return result;
  }
}