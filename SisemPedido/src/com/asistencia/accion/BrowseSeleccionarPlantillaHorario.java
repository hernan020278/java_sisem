package com.asistencia.accion;

import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class BrowseSeleccionarPlantillaHorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {

			peticion.put("browseTabla", "sel-plantillahorario");
			(new GenerarBrowse()).ejecutarAccion(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}