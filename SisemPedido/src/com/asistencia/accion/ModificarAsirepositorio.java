package com.asistencia.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asiasistencia;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asirepositorio;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ModificarAsirepositorio extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
					
			String repcodigo = (String) peticion.get("Pagina_valor");

			peticion.put("consulta", "select * from asirepositorio where kyrepositorio = " + repcodigo);
			peticion.put("entidadNombre", "Asirepositorio");
			aux.asirepo = (Asirepositorio) (new ObtenerEntidad()).ejecutarAccion(peticion);

			peticion.put("consulta", "select * from asiasistencia where kyasistencia = " + aux.asirepo.getKyasistencia());
			peticion.put("entidadNombre", "Asiasistencia");
			aux.asiasis = (Asiasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);

			peticion.put("consulta", "select * from asihorario where kyhorario = " + aux.asirepo.getKyhorario());
			peticion.put("entidadNombre", "Asihorario");
			aux.asihora = (Asihorario) (new ObtenerEntidad()).ejecutarAccion(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}