package com.asistencia.accion;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asiturno;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class GetTurnoByDiaNombre extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try {
        	Auxiliar aux = Auxiliar.getInstance();
        	DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
        	String dia = (String) peticion.get("Asiturno_dia");
        	String nombre = (String) peticion.get("Asiturno_nombre");
        	
			String sql = "select asiturno.* " +
					"from usuario inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " +
						"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
						"inner join ashorario on ashorario.horcodigo=asctrlhorario.horcodigo " +
						"inner join asiturno on asiturno.horcodigo=ashorario.horcodigo " +
						"left join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
					"where aspapeleta.estado = '0215' and aspapeleta.tipo = 'HORARIO' " +
					"and asictrlasistencia.periodo = '" + aux.asictrlasi.getPeriodo()  + "' and usuario.identidad = '" + aux.usu.getIdentidad() + "' " +
					"and asiturno.dia = '" + dia + "' and asiturno.nombre = '" + nombre + "' order by asiturno.nombre asc";

			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asiturno");
			
			aux.asiturn = (Asiturno) (new ObtenerEntidad()).ejecutarAccion(peticion);
			
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}