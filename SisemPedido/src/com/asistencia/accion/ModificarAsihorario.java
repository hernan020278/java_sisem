package com.asistencia.accion;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiplantillahorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

import java.util.Map;

public class ModificarAsihorario extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			
			peticion.put("consulta", "select * from asihorario where kyhorario = " + aux.asihora.getKyhorario());
			peticion.put("entidadNombre", "Asihorario");
			aux.asihora = (Asihorario) (new ObtenerEntidad()).ejecutarAccion(peticion);

			result = peticion;
			this.setEstado(Estado.SUCCEEDED);

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}