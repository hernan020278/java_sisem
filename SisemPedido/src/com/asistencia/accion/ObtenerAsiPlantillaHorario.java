package com.asistencia.accion;

import java.sql.Date;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiplantillahorario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;

public class ObtenerAsiPlantillaHorario extends Accion {

  public Object ejecutarAccion(Map peticion) throws Exception {

    Object result = null;
    try {
      Auxiliar aux = Auxiliar.getInstance();

      String identidad = (String) peticion.get("Usuario_identidad");
      Date fecact = (Date) peticion.get("fechaActual");
      String periodo = (String) peticion.get("periodo");

      String sql = "select asiplantillahorario.* from asiplantillahorario";

      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiplantillahorario");
      aux.asiplanhora = (Asiplantillahorario) (new ObtenerEntidad()).ejecutarAccion(peticion);

      result = peticion;
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }

    return result;
  }
}