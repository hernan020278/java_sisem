package com.sis.contenedor;

import java.sql.Date;
import java.sql.Time;
import java.util.HashMap;

import com.sis.asesor.DlgCaso;
import com.sis.asesor.DlgConsulta;
import com.sis.asesor.DlgDiagnostico;
import com.sis.asesor.DlgEstadistica;
import com.sis.asesor.DlgIndicador;
import com.sis.asesor.DlgPresentacion;
import com.sis.asesor.DlgPsicologo;
import com.sis.asesor.FrmInicio;
import com.sis.browse.DlgBrowseOpciones;
import com.sis.browse.DlgBrowseTabla;
import com.sis.browse.DlgControlProceso;
import com.sis.browse.DlgReporteOpciones;
import com.sis.browse.DlgReporteTabla;
import com.sis.usuario.DlgAcceso;

public class Contenedor {

	static HashMap<String, Object> componentes = initMap();

	private static HashMap<String, Object> initMap() {

		componentes = new HashMap<String, Object>();
		// ---------------------------------------------
		// -
		// -
		// -
		// VISTA - DE SISTEMA ANTERIOR
		// -
		// -
		// -
		// ---------------------------------------------
		FrmInicio frmInicio = new FrmInicio();
		componentes.put("frmInicio", frmInicio);

		DlgControlProceso dlgControlProceso = new DlgControlProceso(frmInicio);
		componentes.put("dlgControlProceso", dlgControlProceso);

		DlgAcceso dlgAcceso = new DlgAcceso(frmInicio);
		componentes.put("dlgAcceso", dlgAcceso);

		DlgPsicologo dlgPsicologo = new DlgPsicologo(frmInicio);
		componentes.put("dlgPsicologo", dlgPsicologo);

		DlgCaso dlgCaso = new DlgCaso(frmInicio);
		componentes.put("dlgCaso", dlgCaso);

		DlgIndicador dlgIndicador = new DlgIndicador(frmInicio);
		componentes.put("dlgIndicador", dlgIndicador);

		DlgBrowseOpciones dlgBrowseOpciones = new DlgBrowseOpciones(frmInicio);
		componentes.put("dlgBrowseOpciones", dlgBrowseOpciones);

		DlgBrowseTabla dlgBrowseTabla = new DlgBrowseTabla(frmInicio);
		componentes.put("dlgBrowseTabla", dlgBrowseTabla);

		DlgPresentacion dlgPresentacion = new DlgPresentacion(frmInicio);
		componentes.put("dlgPresentacion", dlgPresentacion);

		DlgConsulta dlgConsulta = new DlgConsulta(frmInicio);
		componentes.put("dlgConsulta", dlgConsulta);

		DlgDiagnostico dlgDiagnostico = new DlgDiagnostico(frmInicio);
		componentes.put("dlgDiagnostico", dlgDiagnostico);

		DlgEstadistica dlgEstadistica = new DlgEstadistica(frmInicio);
		componentes.put("dlgEstadistica", dlgEstadistica);

		DlgReporteTabla dlgReporteTabla = new DlgReporteTabla(frmInicio);
		componentes.put("dlgReporteTabla", dlgReporteTabla);

		DlgReporteOpciones dlgReporteOpciones = new DlgReporteOpciones(frmInicio);
		componentes.put("dlgReporteOpciones", dlgReporteOpciones);

		// ---------------------------------------------
		// VARIABLES PARA LA MANIPULACION DEL SISTEMA-
		// ---------------------------------------------
		Date varFecActReg = new Date(2011, 1, 1);
		componentes.put("varFecAct", varFecActReg);
		Time varHorActReg = new Time(0, 0, 0);
		componentes.put("varHorAct", varHorActReg);
		return componentes;
	}

	public static Object getComponent(String parCompName) {

		return componentes.get(parCompName);
	}
}
