/*
 * Created on Jul 15, 2003 
 */
package com.sis.main;

import java.util.Map;

import com.comun.referencia.Peticion;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Log;
import com.sis.manejador.ManejadorFactory;
import com.sis.manejador.ManejadorListener;

public class SisemControlador
{
	private ManejadorListener manejador;

	public Map manejarEvento(Map peticion) throws Exception
	{
		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			PaginaGeneral pagGen = PaginaGeneral.getInstance();
			manejador = ManejadorFactory.getInstance().obtenerManejador(peticion);
			if (manejador != null)
			{
				pagGen.pagAct.setNavegar(false);
				if (pagGen.pagAct.getPeticion().equals(Peticion.AJAX))
				{
					manejador.setPeticionAsincrono(peticion);
					peticion.put("nombreHilo", pagGen.obtenerPaginaActual().getEvento());
					pagGen.obtenerPaginaActual().crearHilo(manejador);
				}
				else if (pagGen.pagAct.getPeticion().equals(Peticion.SUBMIT))
				{
					manejador.manejarEvento(peticion);
				}
			}
			else
			{
				Log.debug(this, "El manejador de la pagina " + pagGen.pagAct.getPaginaHijo() + " no fue encontrado.");
			}
		}
		catch (Exception e)
		{
			peticion.put("e", e);
			e.printStackTrace();
		}
		return peticion;
	}

	public String getAccionActual()
	{
		return (manejador.getAccion() != null) ? manejador.getAccion() : "";
	}

	public void setAccionActual(String accion)
	{
		manejador.setAccion(accion);
	}

	public Map getPeticionAsincrono()
	{
		return manejador.getPeticionASincrono();
	}

}
