package com.sis.main;

import java.util.Map;

import com.sis.manejador.Manejador;
import com.sis.manejador.ManejadorListener;

public class Pagina {

	private int orden;
	private String organizacionIde;
	private String tipoConeccion;
	private String paginaHijo;
	private String paginaPadre;
	private String acceso;
	private String modo;
	private String evento;
	private String accion;
	private String browseTabla;
	private String peticion;
	private boolean navegar;
	public static HiloEvento hilo;

	public Pagina() {

	}

	public void setObjeto(Pagina obj)
	{

		this.orden = obj.getOrden();
		this.organizacionIde = obj.getOrganizacionIde();
		this.tipoConeccion = obj.getTipoConeccion();
		this.paginaHijo = obj.getPaginaHijo();
		this.paginaPadre = obj.getPaginaPadre();
		this.acceso = obj.getAcceso();
		this.modo = obj.getModo();
		this.evento = obj.getEvento();
		this.accion = obj.getAccion();
		this.navegar = obj.isNavegar();
		this.browseTabla = obj.getBrowseTabla();
	}

	public String getBrowseTabla() {

		return browseTabla;
	}

	public void setBrowseTabla(String browseTabla) {

		this.browseTabla = browseTabla;
	}

	public int getOrden() {

		return orden;
	}

	public void setOrden(int orden) {

		this.orden = orden;
	}

	public boolean isNavegar() {

		return navegar;
	}

	public void setNavegar(boolean navegar) {

		this.navegar = navegar;
	}

	public String getOrganizacionIde() {

		return organizacionIde;
	}

	public void setOrganizacionIde(String organizacionIde) {

		this.organizacionIde = organizacionIde;
	}

	public String getTipoConeccion() {

		return tipoConeccion;
	}

	public void setTipoConeccion(String tipoConeccion) {

		this.tipoConeccion = tipoConeccion;
	}

	public String getPaginaHijo() {

		return paginaHijo;
	}

	public void setPaginaHijo(String paginaHijo) {

		this.paginaHijo = paginaHijo;
	}

	public String getPaginaPadre() {

		return paginaPadre;
	}

	public void setPaginaPadre(String paginaPadre) {

		this.paginaPadre = paginaPadre;
	}

	public String getAcceso() {

		return acceso;
	}

	public void setAcceso(String acceso) {

		this.acceso = acceso;
	}

	public String getModo() {

		return modo;
	}

	public void setModo(String modo) {

		this.modo = modo;
	}

	public String getEvento() {

		return evento;
	}

	public void setEvento(String evento) {

		this.evento = evento;
	}

	public String getAccion() {

		return accion;
	}

	public void setAccion(String accion) {

		this.accion = accion;
	}

	public String getPeticion() {

		return peticion;
	}

	public void setPeticion(String peticion) {

		this.peticion = peticion;
	}


	public synchronized void crearHilo(ManejadorListener manejador)
	{
		String nombreHilo = this.getEvento();
		hilo = new HiloEvento(manejador, manejador.getPeticionASincrono());
		hilo.setHiloActivo(true);
		Thread eventoHilo = new Thread(hilo, nombreHilo);
		eventoHilo.start();
//		
	}

	public boolean isStopedAllThread()
	{

//		System.out.println("Verificamos si todos los hilos estan parados de la pagina " + paginaHijo);
		if(hilo != null)
		{
//			
			if(hilo.isHiloActivo())
			{
				return false;
			}
		}
		return true;
	}

	public boolean cancelAllThread()
	{

		System.out.println("Cancelamos todos los hilos de la pagina " + paginaHijo);
		if(hilo != null)
		{
			
			hilo = null;
		}
		return true;
	}
	
	public class HiloEvento implements Runnable
	{
		private String nombre = "";
		private Map peticionAsincrono;
		private ManejadorListener manejador;
		private boolean hiloActivo;
		
		public HiloEvento(ManejadorListener manejador, Map peticion)
		{
			this.nombre = (String) peticion.get("nombreHilo");
			this.peticionAsincrono = peticion;
			this.manejador = manejador;
		}
		
		public void run() {
			try
			{	
				hiloActivo = true;
//				
				manejador.manejarEvento(peticionAsincrono);
				hiloActivo = false;
//				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		public String getNombre() {
		
			return nombre;
		}
		public void setNombre(String nombre) {
		
			this.nombre = nombre;
		}

		public boolean isHiloActivo() 
		{
			return hiloActivo;
		}

		public void setHiloActivo(boolean hiloActivo) {
		
			this.hiloActivo = hiloActivo;
		}

//		public Map getPeticionAsincrono() {
//			return peticionAsincrono;
//		}
//
//		public ManejadorListener getManejador() {
//			return manejador;
//		}
		
	}
}
