package com.sis.usuario;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.JPasswordFieldChanged;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.sis.asesor.FrmInicio;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;

public class DlgAcceso extends VistaAdatper implements VistaListener {

	public DlgAcceso(FrmInicio parent) {

		super(parent);
		setTitle("Administracion de las Areas");
		setResizable(false);

		initComponents();

		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		// Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));

		// Se pasa el conjunto de teclas al panel principal
		// super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);

		super.addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent we) {
				frmAbierto = true;
			}
		});

	}

	private void initComponents() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);

		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);

		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 332, 182);

		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);

		panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(10, 11, 306, 158);
		panelPrincipal.add(panel);
		panel.setLayout(null);
		panFondo1 = new jcMousePanel.jcMousePanel();
		panFondo1.setBounds(212, 11, 80, 70);
		panel.add(panFondo1);

		panFondo1.setIcon(AplicacionGeneral.getInstance().obtenerImagen("llave.png")); // NOI18N
		panFondo1.setLayout(null);
		jLabel2 = new javax.swing.JLabel();
		jLabel2.setBounds(128, 15, 80, 27);
		panel.add(jLabel2);
		jLabel2.setFont(new Font("Tahoma", Font.BOLD, 14));
		jLabel2.setHorizontalAlignment(SwingConstants.CENTER);

		jLabel2.setForeground(new java.awt.Color(0, 153, 51));
		jLabel2.setText("Contrase\u00F1a");
		jLabel1 = new javax.swing.JLabel();
		jLabel1.setBounds(22, 15, 96, 27);
		panel.add(jLabel1);
		jLabel1.setFont(new Font("Tahoma", Font.BOLD, 14));
		jLabel1.setHorizontalAlignment(SwingConstants.CENTER);

		jLabel1.setForeground(new java.awt.Color(0, 153, 0));
		jLabel1.setText("Usuario");
		txtUsuIde = new JTextFieldChanged(10);
		txtUsuIde.setBounds(22, 53, 96, 27);
		panel.add(txtUsuIde);
		txtUsuIde.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txtUsuIde.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		txtUsuClave = new JPasswordFieldChanged(4);
		txtUsuClave.setBounds(128, 53, 80, 27);
		panel.add(txtUsuClave);
		txtUsuClave.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txtUsuClave.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		cmdIniciar = new javax.swing.JButton();
		cmdIniciar.setBounds(10, 91, 142, 55);
		panel.add(cmdIniciar);
		cmdIniciar.setFont(new Font("Tahoma", Font.PLAIN, 14));

		cmdIniciar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("login.png")); // NOI18N
		cmdIniciar.setText("Iniciar Sesion");
		cmdIniciar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdIniciar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setBounds(162, 91, 130, 55);
		panel.add(cmdCerrar);
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));

		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png")); // NOI18N
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdCerrar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdCerrar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdCerrarEvento();
			}

		});
		cmdIniciar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdIniciarEvento();
			}
		});
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgAcceso window = new DlgAcceso(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private javax.swing.JButton cmdCerrar;
	private javax.swing.JButton cmdIniciar;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JPanel panelPrincipal;
	private jcMousePanel.jcMousePanel panFondo1;
	private javax.swing.JPasswordField txtUsuClave;
	private javax.swing.JTextField txtUsuIde;
	private JPanel panel;

	@Override
	public void iniciarFormulario() {
		try
		{
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR)) {

				frmAbierto = false;
				limpiarFormulario();
				activarFormulario(true);

			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			else if (pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR)) {

				llenarFormulario();

			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setVisible(true);
	}

	@Override
	public void limpiarFormulario()
	{
		txtUsuIde.setText("");
		txtUsuClave.setText("");
	}

	@Override
	public void llenarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void activarFormulario(boolean activo) {

		txtUsuClave.setEnabled(true);
		txtUsuIde.setEnabled(true);
		cmdIniciar.setEnabled(true);
		cmdCerrar.setEnabled(true);
	}

	@Override
	public void refrescarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPeticion(Map peticion)
	{
		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	/*
	 * 
	 * 
	 * METODOS DESARROLLADOS POR EL USUARIO
	 */
	public void cmdIniciarEvento() {
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			String panelActivo = (String) peticion.get("panelActivo");
			peticion = new HashMap();
			peticion.put("panelActivo", panelActivo);
			peticion.put("Usuario_usuario", txtUsuIde.getText());
			peticion.put("Usuario_clave", txtUsuClave.getText());
			ejecutar("cmdIniciarEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			if (!Util.isEmpty(aux.usuSes.getNombre()))
			{
				this.dispose();
			}
			else {
				iniciarFormulario();
			}
		}
	}

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{
		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{
		return null;
	}
}