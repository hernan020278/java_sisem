package com.sis.asesor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RectangleInsets;
import org.jfree.util.Rotation;

import com.comun.entidad.Indicador;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;

public class DlgEstadistica extends VistaAdatper implements VistaListener {

	private boolean frmAbierto;
	private JPanel panelPrincipal;

	public DlgEstadistica(FrmInicio parent) {

		super(parent);
		setTitle("Consulta de Pacientes");
		setResizable(false);

		initComponents();

		super.addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent we) {
				frmAbierto = true;
			}
		});
	}

	private void initComponents() {

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);

		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);

		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);

		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 1034, 625);

		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);

		panGraficoBarra3D = new javax.swing.JPanel();

		setTitle("Grafico Estadistico de la Poblacion");
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		panPie3D = new javax.swing.JPanel();
		panPie3D.setBounds(0, 0, 337, 230);
		panelPrincipal.add(panPie3D);

		panPie3D.setBorder(null);
		panPie3D.setLayout(new java.awt.BorderLayout());

		panGraficoBarra3D.setBorder(javax.swing.BorderFactory
				.createEtchedBorder());
		panGraficoBarra3D.setLayout(new java.awt.BorderLayout());
		panelPrincipal.add(panGraficoBarra3D);
		panGraficoBarra3D.setBounds(337, 0, 683, 616);
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.setBounds(10, 572, 120, 44);
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdCerrarEvento();
			}
		});
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));

		cmdCerrar.setText("Cerrar");
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(10, 241, 314, 290);
		panelPrincipal.add(panel);
		panel.setLayout(null);
		
		JLabel lblLugar = new JLabel();
		lblLugar.setBounds(10, 53, 126, 23);
		panel.add(lblLugar);
		lblLugar.setText("Lugar");
		lblLugar.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLugar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel lblPoblacion_2 = new JLabel();
		lblPoblacion_2.setBounds(10, 87, 126, 23);
		panel.add(lblPoblacion_2);
		lblPoblacion_2.setText("Poblacion");
		lblPoblacion_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPoblacion_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		lblCasosRegistrados = new JLabel();
		lblCasosRegistrados.setBounds(10, 121, 126, 23);
		panel.add(lblCasosRegistrados);
		lblCasosRegistrados.setText("Casos Registrados");
		lblCasosRegistrados.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCasosRegistrados.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		textField_2 = new JTextField();
		textField_2.setBounds(159, 121, 74, 23);
		panel.add(textField_2);
		textField_2.setText("237");
		textField_2.setHorizontalAlignment(SwingConstants.CENTER);
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textField_2.setEditable(false);
		textField_2.setBackground(new Color(255, 255, 204));
		
		textField_1 = new JTextField();
		textField_1.setBounds(159, 87, 145, 23);
		panel.add(textField_1);
		textField_1.setText("9854684");
		textField_1.setHorizontalAlignment(SwingConstants.LEFT);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textField_1.setEditable(false);
		textField_1.setBackground(new Color(255, 255, 204));
		
		txtArequipa = new JTextField();
		txtArequipa.setBounds(159, 53, 94, 23);
		panel.add(txtArequipa);
		txtArequipa.setText("Arequipa");
		txtArequipa.setHorizontalAlignment(SwingConstants.CENTER);
		txtArequipa.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtArequipa.setEditable(false);
		txtArequipa.setBackground(new Color(255, 255, 204));
		
		textField_3 = new JTextField();
		textField_3.setText("10");
		textField_3.setHorizontalAlignment(SwingConstants.CENTER);
		textField_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textField_3.setEditable(false);
		textField_3.setBackground(new Color(255, 255, 204));
		textField_3.setBounds(159, 159, 74, 23);
		panel.add(textField_3);
		
		JLabel lblUsuariosEvaluados = new JLabel();
		lblUsuariosEvaluados.setText("Usuarios Evaluados");
		lblUsuariosEvaluados.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsuariosEvaluados.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsuariosEvaluados.setBounds(10, 159, 126, 23);
		panel.add(lblUsuariosEvaluados);
		
		lblInformacionDeLa = new JLabel();
		lblInformacionDeLa.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		lblInformacionDeLa.setText("Informacion de la Muestra");
		lblInformacionDeLa.setHorizontalAlignment(SwingConstants.CENTER);
		lblInformacionDeLa.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblInformacionDeLa.setBounds(10, 11, 294, 31);
		panel.add(lblInformacionDeLa);
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgEstadistica window = new DlgEstadistica(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	public static javax.swing.JButton cmdCerrar;
	private javax.swing.JPanel panGraficoBarra3D;
	private javax.swing.JPanel panPie3D;
	private JTextField txtArequipa;
	private JTextField textField_1;
	private JTextField textField_2;
	private JLabel lblCasosRegistrados;
	private JTextField textField_3;
	private JLabel lblInformacionDeLa;

	// End of variables declaration//GEN-END:variables

	@Override
	public void iniciarFormulario() {
		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				llenarFormulario();
				cmdCerrar.setEnabled(true);
				this.setVisible(true);
			}
			frmAbierto = true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario()
	{
	}

	@Override
	public void llenarFormulario()
	{
		// panChartAlta.removeAll();
		// panChartNormal.removeAll();
		panPie3D.removeAll();
		panGraficoBarra3D.removeAll();

//		JFreeChart chartAlta = createChartPie3D(createDataSetPie3D(Estado.ALTA), Estado.toValor(Estado.ALTA));
//		ChartPanel chartPanelAlta = new ChartPanel(chartAlta);
//		panChartAlta.add(chartPanelAlta);
//
//		JFreeChart chartNormal = createChartPie3D(createDataSetPie3D(Estado.NORMAL), Estado.toValor(Estado.NORMAL));
//		ChartPanel chartPanelNormal = new ChartPanel(chartNormal);
//		panChartNormal.add(chartPanelNormal);
//
		JFreeChart chartPied3D = createChartPie3D(createDataSetPie3D(),"Estado de la Poblacion General");
		ChartPanel chartPanelPie3D = new ChartPanel(chartPied3D);
		panPie3D.add(chartPanelPie3D);

		JFreeChart chartBarra3D = createChartBarra3D(createDataSetBarra3D());
		ChartPanel chartPanelBarra3D = new ChartPanel(chartBarra3D);
		panGraficoBarra3D.add(chartPanelBarra3D);

		panGraficoBarra3D.updateUI();
		panPie3D.updateUI();
	}

	@Override
	public void activarFormulario(boolean activo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refrescarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPeticion(Map peticion)
	{
		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{
		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{
		return null;
	}

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public JFreeChart createChartLinea(CategoryDataset parDataSet) {
		// create the chart...
		JFreeChart chart = ChartFactory.createLineChart(
				"Incidencia en la Poblacion", // chart title
				"Indicadores segun el test MMPI", // domain axis label
				"Porcentaje", // range axis label
				parDataSet, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips?
				false // URLs?
				);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		// get a reference to the plot for further customisation...
		chart.setBackgroundPaint(Color.white);
		chart.setBorderVisible(true);
		chart.setBorderPaint(Color.BLACK);
		TextTitle subtitle = new TextTitle("Cuadro de prevalencia de cada trastorno");

		chart.addSubtitle(subtitle);

		CategoryPlot plot = (CategoryPlot) chart.getPlot();

		plot.setDomainGridlinePaint(Color.BLUE);
		plot.setDomainGridlinesVisible(true);

		plot.setRangeGridlinePaint(Color.BLUE);
		plot.setRangeGridlinesVisible(true);

		plot.setInsets(new RectangleInsets(5.0, 5.0, 5.0, 5.0), true);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0));

		return chart;

	}// Fin de metodo para crear el panel de chart

	public CategoryDataset createDataSetLinea() {

		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		List listaTrastornos = (List) peticion.get("listaTrastornos");

		for (int iteTras = 0; iteTras < listaTrastornos.size(); iteTras++)
		{
			Indicador trastorno = (Indicador) listaTrastornos.get(iteTras);
			String[] estadoTrastorno = (String[]) peticion.get("ESTADO_" + trastorno.getIndcodigo());
			double[] valorTrastorno = (double[]) peticion.get("VALOR_" + trastorno.getIndcodigo());
			for (int iteEst = 0; iteEst < estadoTrastorno.length; iteEst++)
			{
				dataset.setValue(valorTrastorno[iteEst], estadoTrastorno[iteEst], trastorno.getNombre());
			}
		}// Fin de recorrer todas las categorias
		dataset.setValue(1.00, "Control", "Control");
		return dataset;

	}// Fin de metodo apra crear el conjunto de datos para el chart

	public DefaultPieDataset createDataSetPie3D() {

		DefaultPieDataset dataset = new DefaultPieDataset();
		String[] estado = (String[]) peticion.get("ESTADO_" + "DG_RES");
		double[] valor = (double[]) peticion.get("VALOR_" + "DG_RES");
		for (int iteDgRes = 0; iteDgRes < estado.length; iteDgRes++)
		{
			String estadoDg = estado[iteDgRes];
			double valorDg = valor[iteDgRes];
			dataset.setValue(estadoDg, (valorDg * 100));
		}
		return dataset;
	}// Fin de metodo apra crear el conjunto de datos para el chart

	public JFreeChart createChartPie3D(DefaultPieDataset parDataSet, String titulo) {
		// create the chart...
		JFreeChart chart = ChartFactory.createPieChart3D(
				titulo, // chart title
				parDataSet, // data
				false, // include legend
				true, // include tooltips
				false); // include urls

		PiePlot3D plot3D = (PiePlot3D) chart.getPlot();
		plot3D.setSectionOutlinesVisible(true);
		plot3D.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		plot3D.setNoDataMessage("No existe dato disponible");
		plot3D.setOutlineVisible(true);
		plot3D.setLabelGap(0.02);
		plot3D.setForegroundAlpha(0.8f);
		plot3D.setCircular(true);
		plot3D.setDarkerSides(true);
		plot3D.setStartAngle(290);
		plot3D.setDirection(Rotation.CLOCKWISE);

		return chart;

	}// Fin de metodo para crear el panel de chart

	public JFreeChart createChartBarra3D(CategoryDataset parDataSet) {
		// create the chart...
		JFreeChart chart = ChartFactory.createBarChart3D(
				"Porcentaje del Diagnostico", // chart title
				"Resultado de la UMLConsulta", // domain axis label
				"Porcentaje", // range axis label
				parDataSet, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips?
				false // URLs?
				);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		// get a reference to the plot for further customisation...
		CategoryPlot plot = (CategoryPlot) chart.getPlot();

		// set the range axis to display integers only...
		 NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		 rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		// disable bar outlines...
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(true);

		// the SWTGraphics2D class doesn't handle GradientPaint well, so
		// replace the gradient painter from the default theme with a
		// standard painter...
		renderer.setBarPainter(new StandardBarPainter());

		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(45.00));
		// OPTIONAL CUSTOMISATION COMPLETED.

		return chart;

	}// Fin de metodo para crear el panel de chart	
	
	
	public CategoryDataset createDataSetBarra3D() {

		// CREAMOS EL CONJUNTO DE DATOS PARA EL DIAGNOSTICO
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		// GENERAMOS EL GRAFICO PARA DIAGNOSTICO

		List listaTrastornos = (List) peticion.get("listaTrastornos");

		for (int iteTras = 0; iteTras < listaTrastornos.size(); iteTras++)
		{
			Indicador trastorno = (Indicador) listaTrastornos.get(iteTras);
			String[] estadoTrastorno = (String[]) peticion.get("ESTADO_" + trastorno.getIndcodigo());
			double[] valorTrastorno = (double[]) peticion.get("VALOR_" + trastorno.getIndcodigo());
			for (int iteEst = 0; iteEst < estadoTrastorno.length; iteEst++)
			{
				dataset.setValue((valorTrastorno[iteEst]*100.00), estadoTrastorno[iteEst], trastorno.getNombre());
			}
		}// Fin de recorrer todas las categorias
		dataset.setValue(100.00, "Control", "Control");

		return dataset;

	}// Fin de metodo apra crear el conjunto de datos para el chart	
}
