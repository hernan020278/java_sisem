package com.sis.asesor;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaPsicologo extends DefaultTableModel {
	private static final long serialVersionUID = 1L;

	public ModeloTablaPsicologo() {

		super.addColumn("Codigo"); 		// 0 String
		super.addColumn("DNI"); 		// 1 String
		super.addColumn("Nombre"); 		// 2 String
		super.addColumn("Edad");   		// 3 Integer
		super.addColumn("Colegiado");  	// 4 String
		super.addColumn("Usuario");	  	// 5 String
		super.addColumn("Clave");	  	// 6 String
	}

	@Override
	public Class getColumnClass(int columna) {
		if ((columna >= 0 && columna <= 2) || (columna >= 4 && columna <= 6)) {
			return String.class;
		}
		if (columna == 3) {
			return Integer.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		// if (col == 5) {
		// return true;
		// }
		return false;
	}
}
