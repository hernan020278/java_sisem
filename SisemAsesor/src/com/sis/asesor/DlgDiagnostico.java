package com.sis.asesor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleInsets;

import com.comun.entidad.Indicador;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.ItemListaRender;
import com.comun.utilidad.swing.PanelImgMsg;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;

public class DlgDiagnostico extends VistaAdatper implements VistaListener {

	private boolean frmAbierto;
	private JPanel panelPrincipal;
	private DefaultListModel modeloListaNombre;
	private DefaultListModel modeloListaDiagnostico;

	public DlgDiagnostico(FrmInicio parent) {

		super(parent);
		setTitle("Consulta de Pacientes");
		setResizable(false);

		initComponents();

		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		// Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));
		// Se pasa el conjunto de teclas al panel principal
		// super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);

		super.addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent we) {
				frmAbierto = true;
			}
		});
	}

	private void initComponents() {

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);

		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);

		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);

		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 893, 470);

		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);

		panGraficoDiagnostico = new javax.swing.JPanel();
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdCerrarEvento();
			}
		});
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		panGraficoTrastorno = new javax.swing.JPanel();
		cmdProbDiag = new javax.swing.JButton();
		cmdProbDiag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdProbDiagAction(e);
			}
		});
		cmdProbDiag.setIcon(AplicacionGeneral.getInstance().obtenerImagen("ficha.png"));

		setTitle("Resultado del Diagnostico");
		
		panelDiagnostico = new PanelImgMsg();
		panelDiagnostico.setBounds(139, 68, 500, 300);
		panelPrincipal.add(panelDiagnostico);
		panelDiagnostico.setLayout(null);
		
		etiDiagnostico = new JLabel("<html><p>Titulo</p><p></p></html>");
		etiDiagnostico.setHorizontalAlignment(SwingConstants.CENTER);
		etiDiagnostico.setFont(new Font("Bookman Old Style", Font.ITALIC, 12));
		etiDiagnostico.setBounds(212, 23, 266, 255);
		panelDiagnostico.add(etiDiagnostico);
		
		scrListaUsuarios = new JScrollPane();
		scrListaUsuarios.setOpaque(false);
		scrListaUsuarios.setBounds(22, 23, 178, 255);
		panelDiagnostico.add(scrListaUsuarios);
		
		modeloListaNombre = new DefaultListModel();
		modeloListaDiagnostico = new DefaultListModel();
		
		listaUsuarios = new JList(modeloListaNombre);
		listaUsuarios.setOpaque(false);
		listaUsuarios.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				keyReleasedEvento(e);
			}
		});
		listaUsuarios.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				seleccionarItemLista(e);
			}
		});
		listaUsuarios.setLocation(0, 11);
		listaUsuarios.setSize(186, 278);
		listaUsuarios.setName("TRASTORNO");
		listaUsuarios.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		listaUsuarios.setCellRenderer(new ItemListaRender());
		listaUsuarios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrListaUsuarios.setViewportView(listaUsuarios);

		panGraficoDiagnostico.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		panGraficoDiagnostico.setLayout(new java.awt.BorderLayout());
		panelPrincipal.add(panGraficoDiagnostico);
		panGraficoDiagnostico.setBounds(10, 10, 310, 390);

		cmdCerrar.setText("Cerrar");
		cmdCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.setBounds(755, 410, 125, 41);

		panGraficoTrastorno.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		panGraficoTrastorno.setLayout(new java.awt.BorderLayout());
		panelPrincipal.add(panGraficoTrastorno);
		panGraficoTrastorno.setBounds(330, 10, 550, 390);

		cmdProbDiag.setText("Mostrar");
		cmdProbDiag.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		panelPrincipal.add(cmdProbDiag);
		cmdProbDiag.setBounds(10, 410, 125, 41);

	}// </editor-fold>//GEN-END:initComponents

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgDiagnostico window = new DlgDiagnostico(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	public static javax.swing.JButton cmdCerrar;
	public static javax.swing.JButton cmdProbDiag;
	private javax.swing.JPanel panGraficoDiagnostico;
	private javax.swing.JPanel panGraficoTrastorno;
	private PanelImgMsg panelDiagnostico;
	private JLabel etiDiagnostico;
	private JScrollPane scrListaUsuarios;
	private JList listaUsuarios;

	// End of variables declaration//GEN-END:variables

	@Override
	public void iniciarFormulario() {
		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				llenarFormulario();
				cmdCerrar.setVisible(true);
				cmdProbDiag.setVisible(true);
				this.setVisible(true);
			}
			frmAbierto = true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario()
	{
	}

	@Override
	public void llenarFormulario()
	{
		crearGraficos();
		llenarListaDiagnostico();
		panelDiagnostico.setVisible(false);
	}

	private void llenarListaDiagnostico()
	{
		modeloListaNombre.removeAllElements();
		modeloListaDiagnostico.removeAllElements();
		List listaDiagnostico = (List) peticion.get("listaDiagnostico");
		for(int iteLis =0;iteLis < listaDiagnostico.size(); iteLis++)
		{
			String[] diagnostico = ((String)listaDiagnostico.get(iteLis)).split("#");
			modeloListaNombre.addElement(diagnostico[0] + " : " + diagnostico[2]);
			modeloListaDiagnostico.addElement(diagnostico[1]);
		}
	}
	@Override
	public void activarFormulario(boolean activo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refrescarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPeticion(Map peticion)
	{
		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{
		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{
		return null;
	}

	public void crearGraficos() {

		// Creación del panel con el gráfico para el diagnostico
		JFreeChart chartDiag = createChartDiagnostico(createDataSetDiagnostico());
		ChartPanel chartPanelDiag = new ChartPanel(chartDiag);
		panGraficoDiagnostico.add(chartPanelDiag);

		// Creacion del panel grafico para los trastornos
		JFreeChart chartTras = createChartTrastorno(createDataSetTrastorno());
		ChartPanel chartPanelTras = new ChartPanel(chartTras);
		panGraficoTrastorno.add(chartPanelTras);
	}

	public JFreeChart createChartDiagnostico(CategoryDataset parDataSet) {
		// create the chart...
		JFreeChart chart = ChartFactory.createBarChart3D(
				"Porcentaje del Diagnostico", // chart title
				"Resultado de la UMLConsulta", // domain axis label
				"Porcentaje", // range axis label
				parDataSet, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips?
				false // URLs?
				);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		// get a reference to the plot for further customisation...
		CategoryPlot plot = (CategoryPlot) chart.getPlot();

		// set the range axis to display integers only...
		// NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		// rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		// disable bar outlines...
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(true);

		// the SWTGraphics2D class doesn't handle GradientPaint well, so
		// replace the gradient painter from the default theme with a
		// standard painter...
		renderer.setBarPainter(new StandardBarPainter());

		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(0.00));
		// OPTIONAL CUSTOMISATION COMPLETED.

		return chart;

	}// Fin de metodo para crear el panel de chart

	public CategoryDataset createDataSetDiagnostico() {

		// CREAMOS EL CONJUNTO DE DATOS PARA EL DIAGNOSTICO
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		// GENERAMOS EL GRAFICO PARA DIAGNOSTICO

		String[] estadoDiagnostico = (String[]) peticion.get("ESTADO_DIAGNOSTICO");
		double[] valorDiagnostico = (double[]) peticion.get("VALOR_DIAGNOSTICO");
		for (int iteDiag = 0; iteDiag < estadoDiagnostico.length; iteDiag++)
		{
			dataset.setValue(valorDiagnostico[iteDiag], estadoDiagnostico[iteDiag], "Diagnostico");
		}

		return dataset;

	}// Fin de metodo apra crear el conjunto de datos para el chart

	public JFreeChart createChartTrastorno(CategoryDataset parDataSet) {
		// create the chart...
		JFreeChart chart = ChartFactory.createLineChart(
				"Trastornos de la Personalidad", // chart title
				"Indicadores segun el test MMPI", // domain axis label
				"Porcentaje", // range axis label
				parDataSet, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips?
				false // URLs?
				);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		// get a reference to the plot for further customisation...
		chart.setBackgroundPaint(Color.white);
		chart.setBorderVisible(true);
		chart.setBorderPaint(Color.BLACK);
		TextTitle subtitle = new TextTitle("Cuadro de prevalencia de cada trastorno");

		chart.addSubtitle(subtitle);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		// get a reference to the plot for further customisation...
		CategoryPlot plot = (CategoryPlot) chart.getPlot();

		// plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.BLUE);
		plot.setDomainGridlinesVisible(true);

		plot.setRangeGridlinePaint(Color.BLUE);
		plot.setRangeGridlinesVisible(true);

		plot.setInsets(new RectangleInsets(5.0, 5.0, 5.0, 5.0), true);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0));
		// OPTIONAL CUSTOMISATION COMPLETED.

		return chart;

	}// Fin de metodo para crear el panel de chart

	public CategoryDataset createDataSetTrastorno()
	{
		// CREAMOS EL CONJUNTO DE DATOS PARA TRASTORNO
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		aux.lisInd = (List) peticion.get("listaTrastornos");

		for (int iteTras = 0; iteTras < aux.lisInd.size(); iteTras++)
		{
			Indicador trastorno = (Indicador) aux.lisInd.get(iteTras);
			String[] estadoTrastorno = (String[]) peticion.get("ESTADO_" + trastorno.getIndcodigo());
			double[] valorTrastorno = (double[]) peticion.get("VALOR_" + trastorno.getIndcodigo());
			for (int iteEst = 0; iteEst < estadoTrastorno.length; iteEst++)
			{
				dataset.setValue(valorTrastorno[iteEst], estadoTrastorno[iteEst], trastorno.getNombre());
			}
		}// Fin de recorrer todas las categorias
		dataset.setValue(1.00, "Control", "Control");
		return dataset;
	}// Fin de metodo apra crear el conjunto de datos para el chart

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}
	
	private void cmdProbDiagAction(ActionEvent e) {
		
		if(cmdProbDiag.getText().equals("Mostrar"))
		{
	        cmdProbDiag.setText("Ocultar");
	        panelDiagnostico.setVisible(true);
//	        txtMueDiag.setText();
//	        txtMueDiag.setVisible(true);
		}
		else if(cmdProbDiag.getText().equals("Ocultar"))
		{
	        cmdProbDiag.setText("Mostrar");
	        panelDiagnostico.setVisible(false);
//	        txtMueDiag.setText();
//	        txtMueDiag.setVisible(true);
		}
	}
	
	public void keyReleasedEvento(KeyEvent e)
	{
		if (e.getKeyCode() == KeyEvent.VK_UP
				|| e.getKeyCode() == KeyEvent.VK_DOWN
				|| e.getKeyCode() == KeyEvent.VK_PAGE_UP
				|| e.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| e.getKeyCode() == KeyEvent.VK_ENTER
				|| e.getKeyCode() == KeyEvent.VK_F2
				|| e.getKeyCode() == KeyEvent.VK_TAB) {

			seleccionarItemLista(null);

		}// Fin de if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN)
	}
	public void seleccionarItemLista(ListSelectionEvent e)
	{
		if (e.getValueIsAdjusting())
		{
			String diagnostico = (String) modeloListaDiagnostico.get(listaUsuarios.getSelectedIndex());
			etiDiagnostico.setText("<html><p align='justify'>" + diagnostico + "<p></html>");
		}
		
	}
	
}
