package com.sis.asesor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.MoverVentanaJComponent;
import com.comun.utilidad.SistemaExperto;
import com.comun.utilidad.Util;
import com.comun.utilidad.VentanaImagen;
import com.comun.utilidad.swing.PanelImgFoto;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatperInicio;
import com.sis.main.VistaListener;
import com.sun.awt.AWTUtilities;

public class FrmInicio extends VistaAdatperInicio implements VistaListener {

	private JComponent contenedor;
	private JPanel panelPrincipal;

	// Variable del formulario Principal
	public FrmInicio() {

		addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {

				// contenedor.update(contenedor.getGraphics());
			}
		});
		contenedor = new VentanaImagen(null);
		this.setUndecorated(true);
		this.setContentPane(contenedor);
		AWTUtilities.setWindowOpaque(this, false);
		this.getRootPane().setOpaque(false);
		MoverVentanaJComponent moverVentana = new MoverVentanaJComponent(contenedor);
		this.addMouseListener(moverVentana);
		this.addMouseMotionListener(moverVentana);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		this.getContentPane().setLayout(new BorderLayout());
		this.setSize(new java.awt.Dimension(810, 597));
		this.setLocationRelativeTo(null);
		initComponents();
	}

	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setOpaque(false);
		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		panelPrincipal = new JPanel();
		panelPrincipal.setLayout(null);
		panelPrincipal.setOpaque(false);
		getContentPane().add(panelPrincipal);
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_down.png")); // NOI18N
		cmdCerrar.setBorderPainted(false);
		cmdCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdCerrar.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_down.png")); // NOI18N
		cmdCerrar.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_up.png")); // NOI18N
		cmdCerrar.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				cmdCerrarEvento();
			}
		});
		cmdInicio = new javax.swing.JButton();
		cmdInicio.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdInicioEvento();
			}
		});
		cmdInicio.setOpaque(false);
		cmdInicio.setBounds(10, 490, 100, 100);
		panelPrincipal.add(cmdInicio);
		cmdInicio.setIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_down.png")); // NOI18N
		cmdInicio.setBorderPainted(false);
		cmdInicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdInicio.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_down.png")); // NOI18N
		cmdInicio.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_up.png")); // NOI18N
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.setBounds(700, 490, 100, 100);
		JSeparator separator = new JSeparator();
		separator.setBounds(200, 444, 400, 2);
		panelPrincipal.add(separator);
		etiMuestraLugar = new JLabel();
		etiMuestraLugar.setBounds(200, 449, 176, 28);
		panelPrincipal.add(etiMuestraLugar);
		etiMuestraLugar.setText("Ciudad Arequipa");
		etiMuestraLugar.setForeground(new Color(153, 102, 0));
		etiMuestraLugar.setFont(new Font("Bookman Old Style", Font.PLAIN, 18));
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(200, 479, 400, 10);
		panelPrincipal.add(separator_1);
		etiMuestraCantidad = new JLabel();
		etiMuestraCantidad.setBounds(373, 449, 227, 28);
		panelPrincipal.add(etiMuestraCantidad);
		etiMuestraCantidad.setText("Total Encuestados : 50");
		etiMuestraCantidad.setHorizontalAlignment(SwingConstants.RIGHT);
		etiMuestraCantidad.setForeground(new Color(153, 102, 0));
		etiMuestraCantidad.setFont(new Font("Bookman Old Style", Font.PLAIN, 18));
		etiUsuarioNombre = new JLabel();
		etiUsuarioNombre.setBounds(200, 488, 400, 20);
		panelPrincipal.add(etiUsuarioNombre);
		etiUsuarioNombre.setText("Psicologo : Hernan Mendoza");
		etiUsuarioNombre.setForeground(new Color(153, 51, 0));
		etiUsuarioNombre.setFont(new Font("Bookman Old Style", Font.PLAIN, 18));
		etiUsuarioUdf1 = new JLabel();
		etiUsuarioUdf1.setBounds(200, 519, 400, 20);
		panelPrincipal.add(etiUsuarioUdf1);
		etiUsuarioUdf1.setText("Colegiado : 221312321");
		etiUsuarioUdf1.setForeground(new Color(0, 51, 51));
		etiUsuarioUdf1.setFont(new Font("Bookman Old Style", Font.PLAIN, 18));
		separator_2 = new JSeparator();
		separator_2.setBounds(200, 550, 400, 10);
		panelPrincipal.add(separator_2);
		panelLogo = new PanelImgFoto();
		panelLogo.setImagen(null, null, null, "CONSTRAINT");
		panelLogo.setBounds(261, 163, 278, 270);
		panelPrincipal.add(panelLogo);
		panMenuPsicologo = new JPanel();
		panMenuPsicologo.setOpaque(false);
		panMenuPsicologo.setLayout(null);
		panMenuPsicologo.setBounds(0, 0, 810, 597);
		panelPrincipal.add(panMenuPsicologo);
		cmdListaPsicologo = new JButton();
		cmdListaPsicologo.setMargin(new Insets(0, 0, 0, 0));
		cmdListaPsicologo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdListaPsicologoEvento();
			}
		});
		cmdListaPsicologo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdListaPsicologo.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("consulta_down.gif"));
		cmdListaPsicologo.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("consulta_up.gif"));
		cmdListaPsicologo.setIcon(AplicacionGeneral.getInstance().obtenerImagen("consulta_down.gif"));
		cmdListaPsicologo.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdListaPsicologo.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdListaPsicologo.setText("Lista Psicologos");
		cmdListaPsicologo.setOpaque(false);
		cmdListaPsicologo.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdListaPsicologo.setFont(new Font("Tahoma", Font.BOLD, 14));
		cmdListaPsicologo.setBorderPainted(false);
		cmdListaPsicologo.setBounds(49, 277, 148, 130);
		panMenuPsicologo.add(cmdListaPsicologo);
		cmdListaCasos = new JButton();
		cmdListaCasos.setMargin(new Insets(0, 0, 0, 0));
		cmdListaCasos.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdListaCasoEvento();
			}
		});
		cmdListaCasos.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdListaCasos.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar_down.png"));
		cmdListaCasos.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar_up.png"));
		cmdListaCasos.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar_down.png"));
		cmdListaCasos.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdListaCasos.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdListaCasos.setToolTipText("");
		cmdListaCasos.setText("Lista Casos");
		cmdListaCasos.setOpaque(false);
		cmdListaCasos.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdListaCasos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdListaCasos.setBorderPainted(false);
		cmdListaCasos.setBounds(606, 277, 148, 130);
		panMenuPsicologo.add(cmdListaCasos);
		cmdListaIndicador = new JButton();
		cmdListaIndicador.setBounds(606, 63, 148, 130);
		panMenuPsicologo.add(cmdListaIndicador);
		cmdListaIndicador.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("indicador_down.png"));
		cmdListaIndicador.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("indicador_up.png"));
		cmdListaIndicador.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdListaIndicadorEvento();
			}
		});
		cmdListaIndicador.setIcon(AplicacionGeneral.getInstance().obtenerImagen("indicador_down.png"));
		cmdListaIndicador.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdListaIndicador.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdListaIndicador.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdListaIndicador.setText("Indicadores");
		cmdListaIndicador.setOpaque(false);
		cmdListaIndicador.setMargin(new Insets(0, 0, 0, 0));
		cmdListaIndicador.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdListaIndicador.setFont(new Font("Tahoma", Font.BOLD, 14));
		cmdListaIndicador.setBorderPainted(false);
		cmdAprender = new JButton();
		cmdAprender.setBounds(49, 63, 148, 130);
		panMenuPsicologo.add(cmdAprender);
		cmdAprender.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdAprenderEvento();
			}
		});
		cmdAprender.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("IndicadorDown.gif"));
		cmdAprender.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("IndicadorUp.gif"));
		cmdAprender.setIcon(AplicacionGeneral.getInstance().obtenerImagen("IndicadorDown.gif"));
		cmdAprender.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAprender.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdAprender.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdAprender.setToolTipText("");
		cmdAprender.setText("Aprendizaje");
		cmdAprender.setOpaque(false);
		cmdAprender.setMargin(new Insets(0, 0, 0, 0));
		cmdAprender.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdAprender.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdAprender.setBorderPainted(false);
		cmdImportarDataMining = new JButton();
		cmdImportarDataMining.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdImportarDataMining.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdImportarDataMiningEvento();
			}
		});
		cmdImportarDataMining.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("lista_indicador_down.png"));
		cmdImportarDataMining.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("lista_indicador_up.png"));
		cmdImportarDataMining.setIcon(AplicacionGeneral.getInstance().obtenerImagen("lista_indicador_down.png"));
		cmdImportarDataMining.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdImportarDataMining.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdImportarDataMining.setToolTipText("");
		cmdImportarDataMining.setText("Importar DataMining");
		cmdImportarDataMining.setOpaque(false);
		cmdImportarDataMining.setMargin(new Insets(0, 0, 0, 0));
		cmdImportarDataMining.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdImportarDataMining.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdImportarDataMining.setBorderPainted(false);
		cmdImportarDataMining.setBounds(330, 22, 161, 130);
		panMenuPsicologo.add(cmdImportarDataMining);
		panMenuInicio = new JPanel();
		panMenuInicio.setOpaque(false);
		panMenuInicio.setLayout(null);
		panMenuInicio.setBounds(0, 0, 810, 597);
		panelPrincipal.add(panMenuInicio);
		cmdMenuPsicologo = new JButton();
		cmdMenuPsicologo.setMargin(new Insets(0, 0, 0, 0));
		cmdMenuPsicologo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdMenuPsicologo();
			}
		});
		cmdMenuPsicologo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdMenuPsicologo.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("admin_especialista_down.png"));
		cmdMenuPsicologo.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("admin_especialista_up.png"));
		cmdMenuPsicologo.setIcon(AplicacionGeneral.getInstance().obtenerImagen("admin_especialista_down.png"));
		cmdMenuPsicologo.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdMenuPsicologo.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdMenuPsicologo.setText("Acceso de Psicologos");
		cmdMenuPsicologo.setOpaque(false);
		cmdMenuPsicologo.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdMenuPsicologo.setFont(new Font("Tahoma", Font.BOLD, 14));
		cmdMenuPsicologo.setBorderPainted(false);
		cmdMenuPsicologo.setBounds(315, 11, 162, 130);
		panMenuInicio.add(cmdMenuPsicologo);
		cmdEstadistica = new JButton();
		cmdEstadistica.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdEstadisticaEvento();
			}
		});
		cmdEstadistica.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("reporte_down.png"));
		cmdEstadistica.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("reporte_up.png"));
		cmdEstadistica.setIcon(AplicacionGeneral.getInstance().obtenerImagen("reporte_down.png"));
		cmdEstadistica.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdEstadistica.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdEstadistica.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdEstadistica.setText("Estadistica");
		cmdEstadistica.setOpaque(false);
		cmdEstadistica.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdEstadistica.setFont(new Font("Tahoma", Font.BOLD, 14));
		cmdEstadistica.setBorderPainted(false);
		cmdEstadistica.setBounds(33, 22, 162, 130);
		panMenuInicio.add(cmdEstadistica);
		cmdAgregarPsicologo = new JButton();
		cmdAgregarPsicologo.setMargin(new Insets(0, 0, 0, 0));
		cmdAgregarPsicologo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAgregarPsicologo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdAgregarPsicologoEvento();
			}
		});
		cmdAgregarPsicologo.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar_psicologo_down.png"));
		cmdAgregarPsicologo.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar_psicologo_up.png"));
		cmdAgregarPsicologo.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar_psicologo_down.png"));
		cmdAgregarPsicologo.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdAgregarPsicologo.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdAgregarPsicologo.setText("Registrar Psicologo");
		cmdAgregarPsicologo.setOpaque(false);
		cmdAgregarPsicologo.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdAgregarPsicologo.setFont(new Font("Tahoma", Font.BOLD, 14));
		cmdAgregarPsicologo.setBorderPainted(false);
		cmdAgregarPsicologo.setBounds(33, 173, 162, 130);
		panMenuInicio.add(cmdAgregarPsicologo);
		cmdCerrarSession = new JButton();
		cmdCerrarSession.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdCerrarSessionEvento();
			}
		});
		cmdCerrarSession.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrarSession.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonCerrarSesionDown.png"));
		cmdCerrarSession.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonCerraSesionUp.png"));
		cmdCerrarSession.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonCerrarSesionDown.png"));
		cmdCerrarSession.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdCerrarSession.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdCerrarSession.setText("Cerrar Session");
		cmdCerrarSession.setOpaque(false);
		cmdCerrarSession.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdCerrarSession.setFont(new Font("Tahoma", Font.BOLD, 14));
		cmdCerrarSession.setBorderPainted(false);
		cmdCerrarSession.setBounds(595, 314, 140, 130);
		panMenuInicio.add(cmdCerrarSession);
		cmdConsultar = new javax.swing.JButton();
		cmdConsultar.setBounds(585, 11, 150, 130);
		panMenuInicio.add(cmdConsultar);
		cmdConsultar.setOpaque(false);
		cmdConsultar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
		cmdConsultar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("consulta_down.png")); // NOI18N
		cmdConsultar.setText("Consultar");
		cmdConsultar.setBorderPainted(false);
		cmdConsultar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdConsultar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		cmdConsultar.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("consulta_down.png")); // NOI18N
		cmdConsultar.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("consulta_up.png")); // NOI18N
		cmdConsultar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
		cmdConsultar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		cmdListaConsulta = new JButton();
		cmdListaConsulta.setBounds(605, 159, 130, 130);
		panMenuInicio.add(cmdListaConsulta);
		cmdListaConsulta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdListaConsulta.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdListaConsultaEvento();
			}
		});
		cmdListaConsulta.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("lista_consultas_down.png"));
		cmdListaConsulta.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("lista_consultas_up.png"));
		cmdListaConsulta.setIcon(AplicacionGeneral.getInstance().obtenerImagen("lista_consultas_down.png"));
		cmdListaConsulta.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdListaConsulta.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdListaConsulta.setToolTipText("");
		cmdListaConsulta.setText("Consultas");
		cmdListaConsulta.setOpaque(false);
		cmdListaConsulta.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdListaConsulta.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdListaConsulta.setBorderPainted(false);
		cmdReporteCasos = new JButton();
		cmdReporteCasos.setBounds(33, 314, 163, 130);
		panMenuInicio.add(cmdReporteCasos);
		cmdReporteCasos.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdReporteCasos.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("casos_down.png"));
		cmdReporteCasos.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("casos_up.png"));
		cmdReporteCasos.setIcon(AplicacionGeneral.getInstance().obtenerImagen("casos_down.png"));
		cmdReporteCasos.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdReporteCasosEvento();
			}
		});
		cmdReporteCasos.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdReporteCasos.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdReporteCasos.setToolTipText("");
		cmdReporteCasos.setText("Reportes Casos");
		cmdReporteCasos.setOpaque(false);
		cmdReporteCasos.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdReporteCasos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdReporteCasos.setBorderPainted(false);
		cmdConsultar.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				cmdConsultarEvento();
			}
		});
		setSize(new java.awt.Dimension(810, 597));
		setLocationRelativeTo(null);
	}// </editor-fold>//GEN-END:initComponents

	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			int resMsg = JOptionPane.showConfirmDialog(this, "���DESEA SALIR DEL DEL SISTEMA !!!", "Sistema de ", JOptionPane.YES_NO_OPTION);
			if(resMsg == JOptionPane.YES_OPTION)
			{
				if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
				{
					ejecutar("cmdCerrarEvento", Peticion.AJAX);
				}
				else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
				{
					estadoEvento = Modo.EVENTO_ENESPERA;
					System.exit(0);
				}
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			System.exit(0);
		}
	}// GEN-LAST:event_cmdCerrarActionPerformed

	public void cmdInicioEvento()
	{

		panMenuInicio.setVisible(true);
		panMenuPsicologo.setVisible(false);
	}

	public void cmdConsultarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdConsultarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdReporteCasosEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdReporteCasosEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdEstadisticaEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdEstadisticaEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdAprenderEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdAprenderEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			boolean indicadorValidado = (Boolean) peticion.get("indicadorValidado");
			if(!indicadorValidado)
			{
				Util.getInst().abrirArchivoDesktop("C:\\sisem\\bin\\", "RedPersonalidad.txt");
				SistemaExperto sistemaExperto = new SistemaExperto();
				sistemaExperto.aprendiendoRedBayes("C:\\sisem\\bin\\RedPersonalidad.txt", "C:\\sisem\\bin\\RedPersonalidad.xdsl");
			}
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdImportarDataMiningEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			String[] extension = {"txt"};
			File fileFuente = Util.getInst().obtenerFileFuente(extension, AplicacionGeneral.getInstance().obtenerRutaSistema() + "\\bin\\");
			peticion.put("fileFuente", fileFuente);
			if(fileFuente != null)
			{
				ejecutar("cmdImportarDataMiningEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdListaPsicologoEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdListaPsicologoEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdListaConsultaEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdListaConsultaEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdListaIndicadorEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdListaIndicadorEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdListaCasoEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdListaCasoEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento

	public void cmdMenuPsicologo()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			peticion.put("panelActivo", "PANEL_PSICOLOGO");
			ejecutar("cmdMenuPsicologo", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			if(!Util.isEmpty(aux.usuSes.getUsucodigo().toString()))
			{
				iniciarFormulario();
			}
		}
	}

	public void cmdAgregarPsicologoEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdAgregarPsicologoEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}

	public void cmdCerrarSessionEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarSessionEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting cFrmPrincipal">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName()))
				{
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		/*
		 * Create and display the form
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				new FrmInicio().setVisible(true);
			}
		});
	}

	private javax.swing.JButton cmdConsultar;
	private javax.swing.JButton cmdCerrar;
	private javax.swing.JButton cmdInicio;
	private JPanel panMenuPsicologo;
	private JButton cmdListaPsicologo;
	private JButton cmdListaCasos;
	private JPanel panMenuInicio;
	private JButton cmdMenuPsicologo;
	private JButton cmdEstadistica;
	private JButton cmdListaConsulta;
	private JSeparator separator_2;
	private JLabel etiMuestraLugar;
	private JLabel etiMuestraCantidad;
	private JLabel etiUsuarioNombre;
	private PanelImgFoto panelLogo;
	private JLabel etiUsuarioUdf1;
	private JButton cmdListaIndicador;
	private JButton cmdAprender;
	private JButton cmdReporteCasos;
	private JButton cmdAgregarPsicologo;
	private JButton cmdCerrarSession;
	private JButton cmdImportarDataMining;

	/*************************************
	 * METODOS DE LA INTERFAZ DE VISTA
	 *************************************/
	@Override
	public void iniciarFormulario()
	{

		frmAbierto = false;
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		paginaHijo = this.getClass().getSimpleName();
		pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
		if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
		{
			String panelInicio = "PANEL_INICIO";
			if(peticion.containsKey("panelActivo"))
			{
				panelInicio = (String) peticion.get("panelActivo");
			}
			if(panelInicio.equals("PANEL_USUARIO"))
			{
				panMenuInicio.setVisible(false);
				panMenuPsicologo.setVisible(false);
			}
			else if(panelInicio.equals("PANEL_PSICOLOGO"))
			{
				panMenuInicio.setVisible(false);
				panMenuPsicologo.setVisible(true);
			}
			else if(panelInicio.equals("PANEL_INDICADOR"))
			{
				panMenuInicio.setVisible(false);
				panMenuPsicologo.setVisible(false);
			}
			else
			{
				panMenuInicio.setVisible(true);
				panMenuPsicologo.setVisible(false);
			}
			limpiarFormulario();
			llenarFormulario();
			if(!this.isVisible()){super.setVisible(true);};
		}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
		else if(pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR)) {
			llenarFormulario();
		}
	}

	@Override
	public void limpiarFormulario()
	{

		panelLogo.setImagen(null, null, null, null);
		etiMuestraLugar.setText("");
		etiMuestraCantidad.setText("");
		etiUsuarioNombre.setText("");
		etiUsuarioUdf1.setText("");
	}

	@Override
	public void llenarFormulario()
	{

		String rutaImagen = AplicacionGeneral.getInstance().obtenerRutaImagenes();
		panelLogo.setImagen(new ImageIcon(rutaImagen + "logo.png"), null, null, "CONSTRAINT");
		etiMuestraLugar.setText("Ciudad : " + aux.mue.getLugar());
		etiMuestraCantidad.setText("Encuestados : " + String.valueOf(aux.mue.getCantidad()));
		etiUsuarioNombre.setText("Psicologo : " + aux.usuSes.getNombre());
		etiUsuarioUdf1.setText("Colegiatura : " + aux.usuSes.getUdf1());
	}

	@Override
	public void activarFormulario(boolean activo) {

		// TODO Auto-generated method stub
	}

	@Override
	public void refrescarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void obtenerDatoFormulario() {

	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		boolean validado = true;
		return validado;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return null;
	}
}
