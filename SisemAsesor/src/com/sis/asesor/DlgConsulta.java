package com.sis.asesor;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import com.comun.entidad.Indicador;
import com.comun.entidad.RegistroConsulta;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.PanelImgFoto;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;

public class DlgConsulta extends VistaAdatper implements VistaListener {

	private boolean frmAbierto;
	private int pregNum;

	public DlgConsulta(FrmInicio parent) {

		super(parent);
		setTitle("Consulta de Pacientes");
		setResizable(false);
		initComponents();
		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		// Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));
		// Se pasa el conjunto de teclas al panel principal
		// super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);
		super.addWindowListener(new WindowAdapter() {

			@Override
			public void windowActivated(WindowEvent we) {

				frmAbierto = true;
			}
		});
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {

			public void run() {

				try {
					DlgConsulta window = new DlgConsulta(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	public static javax.swing.JButton cmdCerrar;
	public static javax.swing.JButton cmdOpcNO;
	public static javax.swing.JButton cmdOpcSI;
	private javax.swing.JLabel etiIndicadorNombre;
	private java.awt.Label etiTitulo;
	private javax.swing.ButtonGroup grpOpcRes;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JPanel panContenedor;
	private PanelImgFoto panIndicadorImagen;
	private javax.swing.JPanel panOpc;
	private javax.swing.JPanel panSinto;
	private javax.swing.JPanel panSintoDef;
	private javax.swing.JTextArea txtIndicadorDefinicion;
	private javax.swing.JTextArea txtIndicadorPregunta;
	private List listaSintoma;
	private String respuestaEstado;
	private JPanel panelPrincipal;
	private JLabel etiAccion;
	private JProgressBar barraProgreso;

	// End of variables declaration//GEN-END:variables
	private void initComponents() {

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);
		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);
		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 744, 454);
		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);
		grpOpcRes = new javax.swing.ButtonGroup();
		etiTitulo = new java.awt.Label();
		panContenedor = new javax.swing.JPanel();
		panSinto = new javax.swing.JPanel();
		etiIndicadorNombre = new javax.swing.JLabel();
		panSintoDef = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		txtIndicadorDefinicion = new javax.swing.JTextArea();
		txtIndicadorDefinicion.setFont(new Font("Monospaced", Font.PLAIN, 14));
		panOpc = new javax.swing.JPanel();
		cmdOpcNO = new javax.swing.JButton();
		cmdOpcNO.setName("AUSENTE");
		cmdOpcNO.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdOpcionEvento(e);
			}
		});
		cmdOpcSI = new javax.swing.JButton();
		cmdOpcSI.setName("PRESENTE");
		cmdOpcSI.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdOpcionEvento(e);
			}
		});
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdCerrarEvento();
			}
		});
		jScrollPane2 = new javax.swing.JScrollPane();
		txtIndicadorPregunta = new javax.swing.JTextArea();
		panIndicadorImagen = new PanelImgFoto();
		setTitle("Consulta Psicologica Virtual");
		etiTitulo.setAlignment(java.awt.Label.CENTER);
		etiTitulo
				.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		etiTitulo.setFont(new java.awt.Font("Bookman Old Style", 0, 36)); // NOI18N
		etiTitulo.setText("Consulta Psicologica Virtual");
		panelPrincipal.add(etiTitulo);
		etiTitulo.setBounds(10, 10, 720, 50);
		panContenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		panContenedor.setLayout(null);
		panSinto.setBackground(new java.awt.Color(255, 255, 204));
		panSinto.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		panSinto.setLayout(null);
		etiIndicadorNombre.setFont(new java.awt.Font("Bookman Old Style", 0, 18)); // NOI18N
		etiIndicadorNombre.setText("Sintoma : Delirio");
		panSinto.add(etiIndicadorNombre);
		etiIndicadorNombre.setBounds(10, 10, 380, 20);
		panSintoDef.setBackground(new java.awt.Color(255, 255, 204));
		panSintoDef.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Definicion",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Bookman Old Style", 1, 12))); // NOI18N
		panSintoDef.setLayout(null);
		txtIndicadorDefinicion.setBackground(new java.awt.Color(204, 255, 204));
		txtIndicadorDefinicion.setColumns(20);
		txtIndicadorDefinicion.setLineWrap(true);
		txtIndicadorDefinicion.setRows(5);
		txtIndicadorDefinicion.setText("Definicion especifica acerca de una ");
		jScrollPane1.setViewportView(txtIndicadorDefinicion);
		panSintoDef.add(jScrollPane1);
		jScrollPane1.setBounds(10, 20, 360, 120);
		panSinto.add(panSintoDef);
		panSintoDef.setBounds(10, 40, 380, 150);
		panOpc.setBackground(new java.awt.Color(255, 255, 204));
		panOpc.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		panOpc.setLayout(null);
		cmdOpcNO.setFont(new java.awt.Font("Bookman Old Style", 0, 18)); // NOI18N
		cmdOpcNO.setText("NO");
		cmdOpcNO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdOpcNO.setMargin(new java.awt.Insets(0, 0, 0, 0));
		panOpc.add(cmdOpcNO);
		cmdOpcNO.setBounds(80, 10, 60, 30);
		cmdOpcSI.setFont(new java.awt.Font("Bookman Old Style", 0, 18)); // NOI18N
		cmdOpcSI.setText("SI");
		cmdOpcSI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdOpcSI.setMargin(new java.awt.Insets(0, 0, 0, 0));
		panOpc.add(cmdOpcSI);
		cmdOpcSI.setBounds(10, 10, 60, 30);
		panSinto.add(panOpc);
		panOpc.setBounds(10, 200, 153, 50);
		cmdCerrar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panSinto.add(cmdCerrar);
		cmdCerrar.setBounds(271, 200, 119, 50);
		panContenedor.add(panSinto);
		panSinto.setBounds(310, 100, 400, 260);
		etiAccion = new JLabel();
		etiAccion.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		etiAccion.setText("Eventos en espera");
		etiAccion.setHorizontalAlignment(SwingConstants.CENTER);
		etiAccion.setFont(new Font("Tahoma", Font.PLAIN, 11));
		etiAccion.setBounds(166, 201, 105, 27);
		panSinto.add(etiAccion);
		barraProgreso = new JProgressBar();
		barraProgreso.setFont(new Font("Tahoma", Font.PLAIN, 16));
		barraProgreso.setBounds(166, 227, 105, 20);
		panSinto.add(barraProgreso);
		jScrollPane2
				.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		jScrollPane2.setHorizontalScrollBar(null);
		txtIndicadorPregunta.setColumns(20);
		txtIndicadorPregunta.setEditable(false);
		txtIndicadorPregunta.setFont(new Font("Monospaced", Font.PLAIN, 14)); // NOI18N
		txtIndicadorPregunta.setLineWrap(true);
		txtIndicadorPregunta.setRows(3);
		txtIndicadorPregunta.setText("¿Ha experimentado ud ideas delirantes?");
		jScrollPane2.setViewportView(txtIndicadorPregunta);
		panContenedor.add(jScrollPane2);
		jScrollPane2.setBounds(10, 10, 700, 80);
		panContenedor.add(panIndicadorImagen);
		panIndicadorImagen.setBounds(10, 100, 290, 260);
		panelPrincipal.add(panContenedor);
		panContenedor.setBounds(10, 70, 720, 370);
	}// </editor-fold>//GEN-END:initComponents

	@Override
	public void iniciarFormulario()
	{

		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				limpiarFormulario();
				activarFormulario(false);
				refrescarFormulario();
				llenarFormulario();
				this.setVisible(true);
			}
			frmAbierto = true;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario()
	{

		etiIndicadorNombre.setText("");
		txtIndicadorPregunta.setText("");
		txtIndicadorDefinicion.setText("");
		panIndicadorImagen.setImagen(null, null, null, "CONSTRAINT");
	}

	@Override
	public void llenarFormulario()
	{

		listaSintoma = (List) peticion.get("listaSintomas");
		pregNum = 0;
		llenarIndicador(pregNum);
	}

	public void llenarIndicador(int idx)
	{

		try {
			if(listaSintoma != null && listaSintoma.size() > 0)
			{
				aux.indSin = (Indicador) listaSintoma.get(idx);
				etiIndicadorNombre.setText(aux.indSin.getNombre());
				txtIndicadorPregunta.setText(aux.indSin.getPregunta());
				txtIndicadorDefinicion.setText(aux.indSin.getDefinicion());
				String rutaImagen = AplicacionGeneral.getInstance().obtenerRutaImagenes();
				ImageIcon imgIco = null;
				ImageIcon imgEst = null;
				imgIco = new ImageIcon(rutaImagen + aux.indSin.getImagen());
				panIndicadorImagen.setImagen(imgIco, null, null, "CONSTRAINT");
				aux.rcoAct = (RegistroConsulta) Util.getInst().obtenerItemLista(aux.lisRco, "RegistroConsulta", "indcodigo", aux.indSin.getIndcodigo());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void activarFormulario(boolean activo)
	{

		etiIndicadorNombre.setEnabled(activo);
		txtIndicadorPregunta.setEnabled(activo);
		txtIndicadorDefinicion.setEnabled(activo);
		panIndicadorImagen.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario() {

		if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			cmdOpcSI.setEnabled(true);
			cmdOpcNO.setEnabled(true);
			cmdCerrar.setEnabled(true);
			txtIndicadorDefinicion.setEnabled(true);
			txtIndicadorPregunta.setEnabled(true);
		}
	}

	@Override
	public void obtenerDatoFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return barraProgreso;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return etiAccion;
	}

	public void cmdOpcionEvento(ActionEvent e)
	{

		JButton boton = (JButton) e.getSource();
		respuestaEstado = Estado.toClave(boton.getName());
		if(pregNum + 1 < listaSintoma.size())
		{
			pregNum++;
			cmdOpcionEvento();
		}
		else
		{
			terminarConsultaEvento();
		}
		// 
	}

	public void cmdOpcionEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			peticion.put("estado", respuestaEstado);
			ejecutar("cmdOpcionEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			llenarIndicador(pregNum);
			refrescarFormulario();
		}
	}

	public void terminarConsultaEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("terminarConsultaEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}
}