package com.sis.asesor;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaCasos extends DefaultTableModel {
	private static final long serialVersionUID = 1L;

	public ModeloTablaCasos() {

		super.addColumn("Nombre"); 		// 0 String
		super.addColumn("Edad");   		// 1 Integer
		super.addColumn("Fecha");  		// 2 String
		super.addColumn("Diagnostico"); // 3 String
		super.addColumn("DNI");	  		// 4 String
		super.addColumn("Historia"); 	// 5 String
	}

	@Override
	public Class getColumnClass(int columna) {
		if ((columna >= 2 && columna <= 5) || columna == 0) {
			return String.class;
		}
		if (columna == 1) {
			return Integer.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		// if (col == 5) {
		// return true;
		// }
		return false;
	}
}
