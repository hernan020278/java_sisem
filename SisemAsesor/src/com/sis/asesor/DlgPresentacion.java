package com.sis.asesor;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoEntero;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;

public class DlgPresentacion extends VistaAdatper implements VistaListener {

	private boolean frmAbierto;

	public DlgPresentacion(FrmInicio parent) {

		super(parent);
		setTitle("Presentacion para la consulta de pacientes");
		setResizable(false);
		initComponents();
		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		// Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));
		// Se pasa el conjunto de teclas al panel principal
		// super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);
		super.addWindowListener(new WindowAdapter() {

			@Override
			public void windowActivated(WindowEvent we) {

				frmAbierto = true;
			}
		});
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {

			public void run() {

				try {
					DlgPresentacion window = new DlgPresentacion(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	public static javax.swing.JButton cmdEmpezar;
	public static javax.swing.JButton cmdCerrar;
	private javax.swing.JLabel etiTitulo;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextArea txtPresentacion;
	private JTextFieldChanged txtUsuarioNombre;
	private JTextFieldFormatoEntero txtUsuarioEdad;
	private JPanel panelPrincipal;

	// End of variables declaration//GEN-END:variables
	/**
	 * Initialize the contents of the frame.
	 */
	private void initComponents() {

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);
		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);
		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 516, 402);
		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);
		jScrollPane1 = new javax.swing.JScrollPane();
		txtPresentacion = new javax.swing.JTextArea();
		etiTitulo = new javax.swing.JLabel();
		cmdEmpezar = new javax.swing.JButton();
		cmdEmpezar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("Doctor.png"));
		cmdEmpezar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdEmpezar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdEmpezarEvento();
			}
		});
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdCerrarEvento();
			}
		});
		setTitle("Presentacion para realizar la consulta");
		txtPresentacion.setColumns(20);
		txtPresentacion.setEditable(false);
		txtPresentacion.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
		txtPresentacion.setLineWrap(true);
		txtPresentacion.setRows(5);
		txtPresentacion
				.setText("Usted esta a punto de realizar una consulta acerca del estado de su personalidad, por favor sea tan amable de contestar las preguntas que a continuacion se le van ha formular, respecto a los sintomas que presenta.\nResponda con la mayor veracidad posible.\nGracias.");
		jScrollPane1.setViewportView(txtPresentacion);
		panelPrincipal.add(jScrollPane1);
		jScrollPane1.setBounds(20, 117, 480, 210);
		etiTitulo.setFont(new java.awt.Font("Bookman Old Style", 0, 24)); // NOI18N
		etiTitulo.setForeground(new java.awt.Color(153, 0, 0));
		etiTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		etiTitulo.setText("Consultorio Psicologico");
		panelPrincipal.add(etiTitulo);
		etiTitulo.setBounds(20, 11, 480, 39);
		cmdEmpezar.setFont(new java.awt.Font("Bookman Old Style", 0, 18)); // NOI18N
		cmdEmpezar.setForeground(new java.awt.Color(51, 51, 51));
		cmdEmpezar.setText("Empezar");
		panelPrincipal.add(cmdEmpezar);
		cmdEmpezar.setBounds(20, 338, 164, 50);
		cmdCerrar.setFont(new java.awt.Font("Bookman Old Style", 0, 18)); // NOI18N
		cmdCerrar.setForeground(new java.awt.Color(51, 51, 51));
		cmdCerrar.setText("Cerrar");
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.setBounds(336, 338, 164, 50);
		txtUsuarioNombre = new JTextFieldChanged(20);
		txtUsuarioNombre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtUsuarioNombre.setBounds(85, 60, 326, 23);
		panelPrincipal.add(txtUsuarioNombre);
		JLabel label_1 = new JLabel();
		label_1.setText("Nombre");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_1.setBounds(20, 61, 64, 23);
		panelPrincipal.add(label_1);
		JLabel label_2 = new JLabel();
		label_2.setText("Edad");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_2.setBounds(417, 61, 40, 23);
		panelPrincipal.add(label_2);
		txtUsuarioEdad = new JTextFieldFormatoEntero(2);
		txtUsuarioEdad.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtUsuarioEdad.setBounds(460, 60, 40, 23);
		panelPrincipal.add(txtUsuarioEdad);
	}// </editor-fold>//GEN-END:initComponents

	@Override
	public void iniciarFormulario() {

		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				refrescarFormulario();
				this.setVisible(true);
			}
			frmAbierto = true;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario() {

		txtUsuarioEdad.setText("");
		txtUsuarioNombre.setText("");
	}

	@Override
	public void llenarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void activarFormulario(boolean activo) {

		cmdEmpezar.setEnabled(activo);
		cmdCerrar.setEnabled(activo);
		txtUsuarioEdad.setEnabled(activo);
		txtUsuarioNombre.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario()
	{

		activarFormulario(true);
		limpiarFormulario();
	}

	@Override
	public void obtenerDatoFormulario() {

		peticion.put("Usuario_nombre", txtUsuarioNombre.getText());
		peticion.put("Usuario_udf2", txtUsuarioEdad.getText());
		peticion.put("Usuario_cargo", "USUARIO");
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		if(!txtUsuarioNombre.getText().equals(""))
		{
			if(!txtUsuarioEdad.getText().equals(""))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return null;
	}

	/*
	 * 
	 * 
	 * METODOS DE CONTROLES DE FORMULARIO
	 */
	public void cmdEmpezarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdEmpezarEvento", Peticion.AJAX);
			}
			else
			{
				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Error de Validacion");
				aux.msg.setTipo("error");
				aux.msg.setValor("Ingrese datos en los campos");
				mensaje();
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}
}
