package com.sis.asesor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.comun.entidad.Indicador;
import com.comun.entidad.Red;
import com.comun.entidad.RegistroCaso;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.ItemListaRender;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoEntero;
import com.comun.utilidad.swing.PanelImgFoto;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;

public class DlgCaso extends VistaAdatper implements VistaListener {

	private boolean frmAbierto;
	private JPanel panelPrincipal;

	public DlgCaso(FrmInicio parent) {

		super(parent);
		setTitle("Administracion de sintomas y trastornos de pacientes");
		setResizable(false);

		initComponents();

		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		// Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));

		// Se pasa el conjunto de teclas al panel principal
		// super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);

		super.addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent we) {
				frmAbierto = true;
			}
		});

	}

	private void initComponents()
	{
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);

		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);

		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);

		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 814, 589);

		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);

		cmdGuardar = new javax.swing.JButton();
		cmdGuardar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdGuardarEvento();
			}
		});
		cmdGuardar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panUsuario = new javax.swing.JPanel();
		etiUsuDni = new javax.swing.JLabel();
		etiUsuDni.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtUsuarioIdentidad = new JTextFieldChanged(8);
		txtUsuarioIdentidad.setText("10793205");
		txtUsuarioIdentidad.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiUsuNom = new javax.swing.JLabel();
		etiUsuNom.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtUsuarioNombre = new JTextFieldChanged(20);
		txtUsuarioNombre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiUsuEdad = new javax.swing.JLabel();
		etiUsuEdad.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtUsuarioEdad = new JTextFieldFormatoEntero(2);
		txtUsuarioEdad.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdValidar = new javax.swing.JButton();
		cmdValidar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdValidar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdValidarClickEvento();
			}
		});
		cmdValidar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("ficha.png"));
		cmdValidar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdValidar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdCerrarEvento();
			}
		});
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiAccion = new javax.swing.JLabel();
		etiAccion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		barraProgreso = new javax.swing.JProgressBar();
		barraProgreso.setFont(new Font("Tahoma", Font.PLAIN, 16));
		setTitle("Agregar Nuevo Caso");
		setResizable(false);
		getContentPane().setLayout(null);
		cmdGuardar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("grabar.png")); // NOI18N
		cmdGuardar.setText("Guardar");
		cmdGuardar.setIconTextGap(0);
		cmdGuardar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		panelPrincipal.add(cmdGuardar);
		cmdGuardar.setBounds(10, 535, 122, 46);
		panUsuario.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del UMLPaciente"));
		panUsuario.setOpaque(false);
		panUsuario.setLayout(null);
		etiUsuDni.setText("DNI");
		panUsuario.add(etiUsuDni);
		etiUsuDni.setBounds(10, 24, 30, 23);
		panUsuario.add(txtUsuarioIdentidad);
		txtUsuarioIdentidad.setBounds(47, 24, 86, 23);
		etiUsuNom.setText("Nombre");
		panUsuario.add(etiUsuNom);
		etiUsuNom.setBounds(141, 24, 64, 23);
		panUsuario.add(txtUsuarioNombre);
		txtUsuarioNombre.setBounds(203, 24, 127, 23);
		etiUsuEdad.setText("Edad");
		panUsuario.add(etiUsuEdad);
		etiUsuEdad.setBounds(566, 24, 40, 23);
		panUsuario.add(txtUsuarioEdad);
		txtUsuarioEdad.setBounds(616, 24, 40, 23);
		cmdValidar.setText("Validar");
		panUsuario.add(cmdValidar);
		cmdValidar.setBounds(658, 13, 121, 40);
		panelPrincipal.add(panUsuario);
		panUsuario.setBounds(10, 0, 789, 60);

		lblApellido = new JLabel();
		lblApellido.setText("Apellido");
		lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblApellido.setBounds(340, 24, 64, 23);
		panUsuario.add(lblApellido);

		txtUsuarioApellido = new JTextFieldChanged(20);
		txtUsuarioApellido.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtUsuarioApellido.setBounds(402, 24, 160, 23);
		panUsuario.add(txtUsuarioApellido);
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png")); // NOI18N
		cmdCerrar.setText("Cerrar");
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.setBounds(677, 535, 122, 42);
		etiAccion.setHorizontalAlignment(SwingConstants.CENTER);
		etiAccion.setText("Eventos en espera");
		panelPrincipal.add(etiAccion);
		etiAccion.setBounds(508, 535, 166, 27);
		panelPrincipal.add(barraProgreso);
		barraProgreso.setBounds(508, 561, 166, 20);

		modLisTrastorno = new DefaultListModel();

		modLisSintoma = new DefaultListModel();

		tabPanConsulta = new JTabbedPane();
		tabPanConsulta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabPanConsulta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tabPanConsulta.setBounds(10, 61, 789, 471);
		panelPrincipal.add(tabPanConsulta);

		panConsulta = new JPanel();
		panConsulta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabPanConsulta.addTab("Consulta", AplicacionGeneral.getInstance().obtenerImagen("ficha.png"), panConsulta, null);
		panConsulta.setLayout(null);

		panDefinicion = new JPanel();
		panDefinicion.setBounds(5, 5, 770, 102);
		panConsulta.add(panDefinicion);
		panDefinicion.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panDefinicion.setLayout(null);
		panFotoDefinicion.setBounds(5, 5, 111, 93);
		panDefinicion.add(panFotoDefinicion);

		scrDefinicion = new JScrollPane();
		scrDefinicion.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		scrDefinicion.setBounds(124, 5, 636, 86);
		panDefinicion.add(scrDefinicion);

		txtDefinicion = new JTextArea();
		txtDefinicion.setBackground(SystemColor.info);
		txtDefinicion.setLineWrap(true);
		txtDefinicion.setEditable(false);
		txtDefinicion.setEnabled(false);
		txtDefinicion.setFont(new Font("Monospaced", Font.PLAIN, 16));
		txtDefinicion.setRows(3);
		scrDefinicion.setViewportView(txtDefinicion);

		etiTrastorno = new JLabel("Hipocondriasis");
		etiTrastorno.setBounds(5, 110, 355, 42);
		panConsulta.add(etiTrastorno);
		etiTrastorno.setHorizontalAlignment(SwingConstants.CENTER);
		etiTrastorno.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		etiTrastorno.setFont(new Font("Tahoma", Font.PLAIN, 16));

		etiSintoma = new JLabel("Hipocondriasis");
		etiSintoma.setBounds(370, 110, 405, 42);
		panConsulta.add(etiSintoma);
		etiSintoma.setHorizontalAlignment(SwingConstants.CENTER);
		etiSintoma.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiSintoma.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		scrTrastorno = new JScrollPane();
		scrTrastorno.setOpaque(false);
		scrTrastorno.setBounds(5, 155, 355, 224);
		panConsulta.add(scrTrastorno);
		listaTrastorno = new JList(modLisTrastorno);
		listaTrastorno.setOpaque(false);
		listaTrastorno.setName("TRASTORNO");
		listaTrastorno.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				keyReleasedEvento(e);
			}
		});
		listaTrastorno.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				seleccionarListaEvento(e);
			}
		});
		listaTrastorno.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		listaTrastorno.setCellRenderer(new ItemListaRender());
		listaTrastorno.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrTrastorno.setViewportView(listaTrastorno);

		JScrollPane scrSintoma = new JScrollPane();
		scrSintoma.setBounds(370, 155, 405, 224);
		panConsulta.add(scrSintoma);
		listaSintoma = new JList(modLisSintoma);
		listaSintoma.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				keyReleasedEvento(e);
			}
		});
		listaSintoma.setName("SINTOMA");
		listaSintoma.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				seleccionarListaEvento(e);
			}
		});
		listaSintoma.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		listaSintoma.setCellRenderer(new ItemListaRender());
		listaSintoma.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrSintoma.setViewportView(listaSintoma);

		cmdAlta = new JButton();
		cmdAlta.setBounds(5, 382, 100, 40);
		panConsulta.add(cmdAlta);
		cmdAlta.setName("ALTA");
		cmdAlta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdEstablecerEstadoEvento(e);
			}
		});
		cmdAlta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAlta.setIcon(AplicacionGeneral.getInstance().obtenerImagen("aspa_ico.png"));
		cmdAlta.setText("Alta");
		cmdAlta.setMargin(new Insets(0, 0, 0, 0));
		cmdAlta.setIconTextGap(0);
		cmdAlta.setFont(new Font("Tahoma", Font.PLAIN, 16));

		cmdNormal = new JButton();
		cmdNormal.setBounds(137, 382, 100, 40);
		panConsulta.add(cmdNormal);
		cmdNormal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdEstablecerEstadoEvento(e);
			}
		});
		cmdNormal.setName("NORMAL");
		cmdNormal.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdNormal.setIcon(AplicacionGeneral.getInstance().obtenerImagen("eliminar.png"));
		cmdNormal.setText("Normal");
		cmdNormal.setMargin(new Insets(0, 0, 0, 0));
		cmdNormal.setIconTextGap(0);
		cmdNormal.setFont(new Font("Tahoma", Font.PLAIN, 16));

		cmdBaja = new JButton();
		cmdBaja.setBounds(260, 382, 100, 40);
		panConsulta.add(cmdBaja);
		cmdBaja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdEstablecerEstadoEvento(e);
			}
		});
		cmdBaja.setName("BAJA");
		cmdBaja.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdBaja.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdBaja.setText("Baja");
		cmdBaja.setMargin(new Insets(0, 0, 0, 0));
		cmdBaja.setIconTextGap(0);
		cmdBaja.setFont(new Font("Tahoma", Font.PLAIN, 16));

		cmdPresenta = new JButton("Presente");
		cmdPresenta.setBounds(370, 382, 133, 40);
		panConsulta.add(cmdPresenta);
		cmdPresenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdEstablecerEstadoEvento(e);
			}
		});
		cmdPresenta.setName("PRESENTE");
		cmdPresenta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPresenta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdPresenta.setMargin(new Insets(0, 0, 0, 0));
		cmdPresenta.setIcon(AplicacionGeneral.getInstance().obtenerImagen("aspa_ico.png"));

		cmdNoPresenta = new JButton("Ausente");
		cmdNoPresenta.setBounds(642, 382, 133, 40);
		panConsulta.add(cmdNoPresenta);
		cmdNoPresenta.setName("AUSENTE");
		cmdNoPresenta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdNoPresenta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdNoPresenta.setMargin(new Insets(0, 0, 0, 0));
		cmdNoPresenta.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));

		panDiagnostico = new JPanel();
		panDiagnostico.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabPanConsulta.addTab("Diagnostico", AplicacionGeneral.getInstance().obtenerImagen("Doctor.png"), panDiagnostico, null);
		panDiagnostico.setLayout(null);

		panel_1 = new JPanel();
		panel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del psicologo", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(5, 5, 389, 111);
		panDiagnostico.add(panel_1);
		panel_1.setLayout(null);

		JLabel label = new JLabel();
		label.setText("DNI");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(10, 20, 90, 23);
		panel_1.add(label);

		txtPsicologoIdentidad = new JTextField();
		txtPsicologoIdentidad.setText("10793205");
		txtPsicologoIdentidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPsicologoIdentidad.setEditable(false);
		txtPsicologoIdentidad.setBackground(new Color(255, 255, 204));
		txtPsicologoIdentidad.setBounds(120, 20, 90, 23);
		panel_1.add(txtPsicologoIdentidad);

		txtPsicologoNombre = new JTextField();
		txtPsicologoNombre.setText("Hernan Mendoza Ticllahuanaco");
		txtPsicologoNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPsicologoNombre.setEditable(false);
		txtPsicologoNombre.setBackground(new Color(255, 255, 204));
		txtPsicologoNombre.setBounds(120, 48, 259, 23);
		panel_1.add(txtPsicologoNombre);

		JLabel label_1 = new JLabel();
		label_1.setText("Psicologo");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(10, 48, 90, 23);
		panel_1.add(label_1);

		JLabel label_2 = new JLabel();
		label_2.setText("N\u00C2\u00BA Colegiado");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setBounds(10, 75, 90, 23);
		panel_1.add(label_2);

		txtPsicologoColegiado = new JTextField();
		txtPsicologoColegiado.setText("13412341");
		txtPsicologoColegiado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPsicologoColegiado.setEditable(false);
		txtPsicologoColegiado.setBackground(new Color(255, 255, 204));
		txtPsicologoColegiado.setBounds(120, 75, 90, 23);
		panel_1.add(txtPsicologoColegiado);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Paciente", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(397, 5, 377, 111);
		panDiagnostico.add(panel_2);
		panel_2.setLayout(null);

		JLabel label_3 = new JLabel();
		label_3.setText("DNI");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setBounds(10, 20, 90, 23);
		panel_2.add(label_3);

		JLabel label_4 = new JLabel();
		label_4.setText("UMLPaciente");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(10, 48, 90, 23);
		panel_2.add(label_4);

		JLabel label_5 = new JLabel();
		label_5.setText("Edad");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(10, 75, 90, 23);
		panel_2.add(label_5);

		txtPacienteEdad = new JTextField();
		txtPacienteEdad.setText("32");
		txtPacienteEdad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPacienteEdad.setEditable(false);
		txtPacienteEdad.setBackground(new Color(255, 255, 204));
		txtPacienteEdad.setBounds(100, 75, 40, 23);
		panel_2.add(txtPacienteEdad);

		txtPacienteNombre = new JTextField();
		txtPacienteNombre.setText("Hernan Mendoza Ticllahuanaco");
		txtPacienteNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPacienteNombre.setEditable(false);
		txtPacienteNombre.setBackground(new Color(255, 255, 204));
		txtPacienteNombre.setBounds(100, 48, 267, 23);
		panel_2.add(txtPacienteNombre);

		txtPacienteIdentidad = new JTextField();
		txtPacienteIdentidad.setText("10793205");
		txtPacienteIdentidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPacienteIdentidad.setEditable(false);
		txtPacienteIdentidad.setBackground(new Color(255, 255, 204));
		txtPacienteIdentidad.setBounds(100, 20, 90, 23);
		panel_2.add(txtPacienteIdentidad);

		JPanel panel_3 = new JPanel();
		panel_3.setForeground(Color.ORANGE);
		panel_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_3.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos de la Muestra", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_3.setBounds(10, 127, 764, 246);
		panDiagnostico.add(panel_3);
		panel_3.setLayout(null);

		JLabel lblLugar = new JLabel();
		lblLugar.setText("Lugar");
		lblLugar.setHorizontalAlignment(SwingConstants.LEFT);
		lblLugar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLugar.setBounds(10, 24, 74, 23);
		panel_3.add(lblLugar);

		txtMuestraLugar = new JTextField();
		txtMuestraLugar.setText("Arequipa");
		txtMuestraLugar.setHorizontalAlignment(SwingConstants.CENTER);
		txtMuestraLugar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtMuestraLugar.setEditable(false);
		txtMuestraLugar.setBackground(new Color(255, 255, 204));
		txtMuestraLugar.setBounds(94, 24, 115, 23);
		panel_3.add(txtMuestraLugar);

		JLabel lblEvaluados = new JLabel();
		lblEvaluados.setText("Evaluados");
		lblEvaluados.setHorizontalAlignment(SwingConstants.LEFT);
		lblEvaluados.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEvaluados.setBounds(10, 54, 74, 23);
		panel_3.add(lblEvaluados);

		txtMuestraCantidad = new JTextField();
		txtMuestraCantidad.setText("35");
		txtMuestraCantidad.setHorizontalAlignment(SwingConstants.CENTER);
		txtMuestraCantidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtMuestraCantidad.setEditable(false);
		txtMuestraCantidad.setBackground(new Color(255, 255, 204));
		txtMuestraCantidad.setBounds(94, 54, 57, 23);
		panel_3.add(txtMuestraCantidad);

		JLabel lblInicio = new JLabel();
		lblInicio.setText("Inicio");
		lblInicio.setHorizontalAlignment(SwingConstants.LEFT);
		lblInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblInicio.setBounds(10, 85, 74, 23);
		panel_3.add(lblInicio);

		txtMuestraFecinicio = new JTextField();
		txtMuestraFecinicio.setText("12/12/2013");
		txtMuestraFecinicio.setHorizontalAlignment(SwingConstants.CENTER);
		txtMuestraFecinicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtMuestraFecinicio.setEditable(false);
		txtMuestraFecinicio.setBackground(new Color(255, 255, 204));
		txtMuestraFecinicio.setBounds(94, 85, 115, 23);
		panel_3.add(txtMuestraFecinicio);

		JPanel panel_4 = new JPanel();
		panel_4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_4.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Diagnostico del paciente", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_4.setBounds(219, 29, 533, 204);
		panel_3.add(panel_4);
		panel_4.setLayout(null);

		scrHistoriaDiagnostico = new JScrollPane();
		scrHistoriaDiagnostico.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrHistoriaDiagnostico.setBounds(10, 22, 513, 171);
		panel_4.add(scrHistoriaDiagnostico);

		txtHistoriaDiagnostico = new JTextArea();
		txtHistoriaDiagnostico.setLineWrap(true);
		txtHistoriaDiagnostico.setBackground(SystemColor.info);
		txtHistoriaDiagnostico.setFont(new Font("Monospaced", Font.PLAIN, 16));
		scrHistoriaDiagnostico.setViewportView(txtHistoriaDiagnostico);

		etiDiagnostico = new JLabel("");
		etiDiagnostico.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		etiDiagnostico.setIcon(AplicacionGeneral.getInstance().obtenerImagen("fondo_diagnostico.png"));
		etiDiagnostico.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiDiagnostico.setBounds(10, 119, 199, 114);
		panel_3.add(etiDiagnostico);

		cmdDiagNormal = new JButton();
		cmdDiagNormal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdEstablecerDiagnosticoEvento(e);
			}
		});
		cmdDiagNormal.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDiagNormal.setIcon(AplicacionGeneral.getInstance().obtenerImagen("aspa_ico.png"));
		cmdDiagNormal.setText("Normal");
		cmdDiagNormal.setName("NORMAL");
		cmdDiagNormal.setMargin(new Insets(0, 0, 0, 0));
		cmdDiagNormal.setIconTextGap(0);
		cmdDiagNormal.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdDiagNormal.setBounds(184, 377, 134, 45);
		panDiagnostico.add(cmdDiagNormal);

		cmdDiagTratamiento = new JButton();
		cmdDiagTratamiento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdEstablecerDiagnosticoEvento(e);
			}
		});
		cmdDiagTratamiento.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDiagTratamiento.setIcon(AplicacionGeneral.getInstance().obtenerImagen("eliminar.png"));
		cmdDiagTratamiento.setText("Tratamiento");
		cmdDiagTratamiento.setName("TRATAMIENTO");
		cmdDiagTratamiento.setMargin(new Insets(0, 0, 0, 0));
		cmdDiagTratamiento.setIconTextGap(0);
		cmdDiagTratamiento.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdDiagTratamiento.setBounds(328, 377, 134, 45);
		panDiagnostico.add(cmdDiagTratamiento);

		cmdDiagHospitalizacion = new JButton();
		cmdDiagHospitalizacion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDiagHospitalizacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdEstablecerDiagnosticoEvento(e);
			}
		});
		cmdDiagHospitalizacion.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdDiagHospitalizacion.setText("Hospitalizacion");
		cmdDiagHospitalizacion.setName("HOSPITALIZACION");
		cmdDiagHospitalizacion.setMargin(new Insets(0, 0, 0, 0));
		cmdDiagHospitalizacion.setIconTextGap(0);
		cmdDiagHospitalizacion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdDiagHospitalizacion.setBounds(10, 377, 164, 45);
		panDiagnostico.add(cmdDiagHospitalizacion);
		cmdNoPresenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdEstablecerEstadoEvento(e);
			}
		});
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgCaso window = new DlgCaso(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public javax.swing.JButton cmdCerrar;
	public javax.swing.JButton cmdGuardar;
	public javax.swing.JButton cmdValidar;
	public javax.swing.JLabel etiAccion;
	private javax.swing.JLabel etiUsuDni;
	private javax.swing.JLabel etiUsuEdad;
	private javax.swing.JLabel etiUsuNom;
	private PanelImgFoto panFotoDefinicion = new PanelImgFoto();
	private javax.swing.JPanel panUsuario;
	public javax.swing.JProgressBar barraProgreso;
	public JTextFieldChanged txtUsuarioIdentidad;
	public JTextFieldFormatoEntero txtUsuarioEdad;
	public JTextFieldChanged txtUsuarioNombre;
	private JLabel etiTrastorno;
	private DefaultListModel modLisTrastorno;
	private DefaultListModel modLisSintoma;
	private List listaTrastornoCodigo;
	private List listaSintomaCodigo;
	private JScrollPane scrTrastorno;
	private JList listaTrastorno;
	private JList listaSintoma;
	private JButton cmdBaja;
	private JButton cmdNormal;
	private JButton cmdAlta;
	private JButton cmdPresenta;
	private JButton cmdNoPresenta;
	private JLabel etiSintoma;
	private JPanel panDefinicion;
	private JScrollPane scrDefinicion;
	private JTextArea txtDefinicion;
	private JPanel panDiagnostico;
	private JPanel panel_1;
	private JTextField txtPsicologoIdentidad;
	private JTextField txtPsicologoNombre;
	private JTextField txtPsicologoColegiado;
	private JTextField txtPacienteEdad;
	private JTextField txtPacienteNombre;
	private JTextField txtPacienteIdentidad;
	private JTextField txtMuestraLugar;
	private JTextField txtMuestraCantidad;
	private JTextField txtMuestraFecinicio;
	private JScrollPane scrHistoriaDiagnostico;
	private JTextArea txtHistoriaDiagnostico;
	private JTabbedPane tabPanConsulta;
	private JPanel panConsulta;
	private JButton cmdDiagNormal;
	private JButton cmdDiagTratamiento;
	private JLabel etiDiagnostico;
	private JButton cmdDiagHospitalizacion;
	private String estado;
	private String tipo;
	private JLabel lblApellido;
	private JTextFieldChanged txtUsuarioApellido;

	public void cmdAgregarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				limpiarFormulario();
				activarUsuario(true);
				txtUsuarioIdentidad.requestFocus();
				pagGen.pagAct.setModo(Modo.AGREGANDO);
				refrescarFormulario();
			}
			else if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				if (validarFormulario())
				{
					obtenerDatoFormulario();
					ejecutar("cmdGuardarEvento", Peticion.AJAX);
				}
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			pagGen.pagAct.setModo(Modo.VISUALIZANDO);
			iniciarFormulario();
		}
	}

	public void cmdGuardarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			if (validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdGuardarEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdValidarClickEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdValidarClickEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			boolean resultado = (Boolean) peticion.get("resultado");
			if (resultado)
			{
				limpiarIndicador();
				tabPanConsulta.setEnabled(true);
				activarUsuario(false);
				activarIndicador(true);
				activarControlDiagnostico(true);
				llenarFormulario();
				llenarDiagnostico();
			}
		}
	}

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void seleccionarListaEvento(ListSelectionEvent e)
	{
		if (e.getValueIsAdjusting())
		{
			seleccionarItemLista(((JList) e.getSource()).getName());
		}
	}

	public void establecerItemSeleccion(String tipo)
	{
		if (tipo.equals("TRASTORNO"))
		{
			llenarTrastorno();
			for (int iteLis = 0; iteLis < modLisTrastorno.getSize(); iteLis++)
			{
				String valorItem = (String) modLisTrastorno.get(iteLis);
				String[] item = valorItem.split("#");
				if (item[0].equals(aux.indTra.getNombre()))
				{
					listaTrastorno.setSelectedIndex(iteLis);
					break;
				}
			}
		}
		else if (tipo.equals("SINTOMA"))
		{
			llenarSintoma();
			for (int iteLis = 0; iteLis < modLisSintoma.getSize(); iteLis++)
			{
				String valorItem = (String) modLisSintoma.get(iteLis);
				String[] item = valorItem.split("#");
				if (item[0].equals(aux.indSin.getNombre()))
				{
					listaSintoma.setSelectedIndex(iteLis);
					break;
				}
			}
		}
	}

	public void keyReleasedEvento(KeyEvent e)
	{
		if (e.getKeyCode() == KeyEvent.VK_UP
				|| e.getKeyCode() == KeyEvent.VK_DOWN
				|| e.getKeyCode() == KeyEvent.VK_PAGE_UP
				|| e.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| e.getKeyCode() == KeyEvent.VK_ENTER
				|| e.getKeyCode() == KeyEvent.VK_F2
				|| e.getKeyCode() == KeyEvent.VK_TAB) {

			seleccionarItemLista(((JList) e.getSource()).getName());

		}// Fin de if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN)
	}
	public void seleccionarItemLista(String tipo)
	{
		seleccionarIndicador(tipo);
		llenarDefinicion(tipo);
		if (tipo.equals("TRASTORNO"))
		{
			llenarSintoma();
		}
		activarControlIndicador(true, tipo);
	}

	public void cmdEstablecerDiagnosticoEvento(ActionEvent e)
	{
		JButton cmdObj = (JButton) e.getSource();
		estado = cmdObj.getName();
		seleccionarIndicador("DIAGNOSTICO");
		cmdEstablecerDiagnosticoEvento();
	}

	public void cmdEstablecerDiagnosticoEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			peticion.put("estado", Estado.toClave(estado));
			ejecutar("cmdEstablecerDiagnosticoEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			String rutaImagen = AplicacionGeneral.getInstance().obtenerRutaImagenes();
			estado = (String) peticion.get("estado");
			etiDiagnostico.setIcon(new ImageIcon(rutaImagen + "diagnostico_" + Estado.toValor(estado).toLowerCase() + ".png"));
			tabPanConsulta.setEnabled(true);
			activarUsuario(false);
			activarIndicador(true);
			activarControlIndicador(false, "AMBOS");
			activarControlDiagnostico(true);
		}
	}

	public void cmdEstablecerEstadoEvento(ActionEvent e)
	{
		if (listaTrastorno.getSelectedIndex() > -1)
		{
			JButton cmdObj = (JButton) e.getSource();
			estado = cmdObj.getName();
			tipo = (Estado.comparar(estado, Estado.PRESENTE) || Estado.comparar(estado, Estado.AUSENTE)) ? "SINTOMA" : "TRASTORNO";
			seleccionarIndicador(tipo);
			cmdEstablecerEstadoEvento();
		}
		else
		{
			aux.msg.setMostrar(true);
			aux.msg.setTipo("inf");
			aux.msg.setTitulo("Error de Seleccion");
			aux.msg.setValor("Debe seleccionar un item de la lista");
			mensaje();
		}
	}

	public void cmdEstablecerEstadoEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			activarControlIndicador(false, "AMBOS");
			peticion = new HashMap();
			peticion.put("estado", Estado.toClave(estado));
			peticion.put("tipo", tipo);
			ejecutar("cmdEstablecerEstadoEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			String tipo = (String) peticion.get("tipo");
//			establecerItemSeleccion(tipo);
			actualizarItemListaIndicador(tipo);
			llenarDefinicion(tipo);
			tabPanConsulta.setEnabled(true);
			activarUsuario(false);
			activarIndicador(true);
			activarControlIndicador(false, "AMBOS");
			activarControlDiagnostico(true);
		}
	}

	public void seleccionarIndicador(String tipo)
	{
		String itemLista = "";
		String[] valorLista = { "RESULTADO", "diagnostico_normal.png" };
		if (tipo.equals("TRASTORNO"))
		{
			itemLista = (String) listaTrastorno.getSelectedValue();
			valorLista = itemLista.split("#");
			aux.indTra = (Indicador) Util.getInst().obtenerItemLista(aux.lisInd, "Indicador", "nombre", valorLista[0]);
			aux.rgcTra = (RegistroCaso) Util.getInst().obtenerItemLista(aux.lisRca, "RegistroCaso", "indcodigo", aux.indTra.getIndcodigo());
		}
		else if (tipo.equals("SINTOMA"))
		{
			itemLista = (String) listaSintoma.getSelectedValue();
			valorLista = itemLista.split("#");
			aux.indSin = (Indicador) Util.getInst().obtenerItemLista(aux.lisInd, "Indicador", "nombre", valorLista[0]);
			aux.rgcSin = (RegistroCaso) Util.getInst().obtenerItemLista(aux.lisRca, "RegistroCaso", "indcodigo", aux.indSin.getIndcodigo());
		}
		else if (tipo.equals("DIAGNOSTICO"))
		{
			aux.indDia = (Indicador) Util.getInst().obtenerItemLista(aux.lisInd, "Indicador", "nombre", valorLista[0]);
			aux.rgcDia = (RegistroCaso) Util.getInst().obtenerItemLista(aux.lisRca, "RegistroCaso", "indcodigo", aux.indDia.getIndcodigo());
		}
		aux.indAct = (Indicador) Util.getInst().obtenerItemLista(aux.lisInd, "Indicador", "nombre", valorLista[0]);
		aux.rgcAct = (RegistroCaso) Util.getInst().obtenerItemLista(aux.lisRca, "RegistroCaso", "indcodigo", aux.indAct.getIndcodigo());
	}

	/*
	 * 
	 * 
	 * (non-Javadoc)
	 * 
	 * @see com.sis.control.VistaListener#iniciarFormulario()
	 */

	@Override
	public void iniciarFormulario() {
		try
		{
			
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				refrescarFormulario();
				this.setVisible(true);
			}			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario()
	{
		limpiarUsuario();
		limpiarIndicador();
		limpiarDiagnostico();
	}

	public void limpiarUsuario()
	{
		txtUsuarioIdentidad.setText("");
		txtUsuarioNombre.setText("");
		txtUsuarioApellido.setText("");
		txtUsuarioEdad.setValue(0);
	}

	public void limpiarDiagnostico()
	{
		txtPsicologoIdentidad.setText("");
		txtPsicologoNombre.setText("");
		txtPsicologoColegiado.setText("");

		txtPacienteEdad.setText("");
		txtPacienteIdentidad.setText("");
		txtPacienteNombre.setText("");

		txtMuestraLugar.setText("");
		txtMuestraFecinicio.setText("");
		txtMuestraCantidad.setText("");

		txtHistoriaDiagnostico.setText("");

		String rutaImagen = AplicacionGeneral.getInstance().obtenerRutaImagenes();
		etiDiagnostico.setIcon(new ImageIcon(rutaImagen + "diagnostico_normal.png"));
	}

	public void limpiarIndicador()
	{
		panFotoDefinicion.setImagen(null, null, null, "CONSTRAINT");
		txtDefinicion.setText("");
		modLisTrastorno.removeAllElements();
		modLisSintoma.removeAllElements();
	}

	@Override
	public void llenarFormulario()
	{
		llenarUsuario();
		llenarTrastorno();
		llenarDiagnostico();
	}

	public void llenarUsuario()
	{
		txtUsuarioIdentidad.setText(aux.pac.getIdentidad());
		txtUsuarioNombre.setText(aux.pac.getNombre());
		txtUsuarioApellido.setText(aux.pac.getApellido());
		txtUsuarioEdad.setValue(Util.getInteger(aux.pac.getUdf2()));
	}

	public void llenarDiagnostico()
	{
		txtPsicologoIdentidad.setText(aux.usuSes.getIdentidad());
		txtPsicologoNombre.setText(aux.usuSes.getNombre());
		txtPsicologoColegiado.setText(aux.usuSes.getUdf1());

		txtPacienteIdentidad.setText(aux.pac.getIdentidad());
		txtPacienteNombre.setText(aux.pac.getNombre());
		txtPacienteEdad.setText(aux.pac.getUdf2());

		txtMuestraLugar.setText(aux.mue.getLugar());
		txtMuestraFecinicio.setText(Util.getInst().fromDateToString(aux.mue.getFecinicio(), "dd/MM/yyyy"));
		txtMuestraCantidad.setText(String.valueOf(aux.mue.getCantidad()));

		txtHistoriaDiagnostico.setText(aux.his.getDiagnostico());
	}

	public void llenarMuestra()
	{
		txtMuestraLugar.setText(aux.mue.getLugar());
		txtMuestraFecinicio.setText(Util.getInst().fromDateToString(aux.mue.getFecinicio(), "dd/MM/yyyy"));
		txtMuestraCantidad.setText(String.valueOf(aux.mue.getCantidad()));
	}

	public void llenarDefinicion(String tipo)
	{
		String rutaImagen = AplicacionGeneral.getInstance().obtenerRutaImagenes();
		ImageIcon imgIco = null;
		ImageIcon imgEst = null;
		String definicion = "";
		if (tipo.equals("TRASTORNO"))
		{
			definicion = aux.indTra.getDefinicion();
			imgIco = new ImageIcon(rutaImagen + aux.indTra.getImagen());
			imgEst = new ImageIcon(rutaImagen + obtenerEstadoImagen(aux.indTra.getIndcodigo(), "big"));
			etiTrastorno.setIcon(imgEst);
			etiTrastorno.setText(aux.indTra.getNombre());
		}
		else if (tipo.equals("SINTOMA"))
		{
			definicion = aux.indSin.getDefinicion();
			imgIco = new ImageIcon(rutaImagen + aux.indSin.getImagen());
			imgEst = new ImageIcon(rutaImagen + obtenerEstadoImagen(aux.indSin.getIndcodigo(), "big"));
			etiSintoma.setIcon(imgEst);
			etiSintoma.setText(aux.indSin.getNombre());
		}
		if (tipo.equals("TRASTORNO") || tipo.equals("SINTOMA"))
		{
			panFotoDefinicion.setImagen(imgIco, null, null, "CONSTRAINT");
			txtDefinicion.setText(definicion);
		}
		if (tipo.equals("DIAGNOSTICO"))
		{
			etiDiagnostico.setIcon(new ImageIcon(rutaImagen + "diagnostico_" + Estado.toValor(aux.rgcAct.getEstado()).toLowerCase() + ".png"));
		}
	}

	public String obtenerEstadoImagen(String indicador, String tamano)
	{
		String estado = "";
		String estadoImagen = "";

		RegistroCaso rgc = (RegistroCaso) Util.getInst().obtenerItemLista(aux.lisRca, "RegistroCaso", "indcodigo", indicador);

		if (!rgc.getIndcodigo().equals(""))
		{
			estado = rgc.getEstado();
			if (Estado.comparar(estado, Estado.ALTA) || Estado.comparar(estado, Estado.PRESENTE))
			{
				estadoImagen = "alta_" + tamano + ".png";
			}
			else if (Estado.comparar(estado, Estado.NORMAL))
			{
				estadoImagen = "normal_" + tamano + ".png";
			}
			else if (Estado.comparar(estado, Estado.BAJA) || Estado.comparar(estado, Estado.AUSENTE))
			{
				estadoImagen = "baja_" + tamano + ".png";
			}
			else
			{
				estadoImagen = "pregunta_" + tamano + ".png";
			}
		}
		else
		{
			estadoImagen = "pregunta_" + tamano + ".png";
		}
		return estadoImagen;
	}

	public void llenarTrastorno()
	{
		modLisTrastorno.removeAllElements();
		listaTrastornoCodigo = new ArrayList();
		for (int iteLisInd = 0; iteLisInd < aux.lisInd.size(); iteLisInd++)
		{
			Indicador trastorno = (Indicador) aux.lisInd.get(iteLisInd);
			if (trastorno.getTipo().equals("TRASTORNO"))
			{
				modLisTrastorno.addElement(trastorno.getNombre() + "#" + obtenerEstadoImagen(trastorno.getIndcodigo(), "sma"));
				listaTrastornoCodigo.add(trastorno.getIndcodigo());
			}
		}
	}

	public void llenarSintoma()
	{
		modLisSintoma.removeAllElements();
		listaSintomaCodigo = new ArrayList();
		for (int iteRed = 0; iteRed < aux.lisRed.size(); iteRed++)
		{
			Red red = (Red) aux.lisRed.get(iteRed);
			if (aux.indTra.getIndcodigo().equals(red.getTracodigo()))
			{
				Indicador sintoma = (Indicador) Util.getInst().obtenerItemLista(aux.lisInd, "Indicador", "indcodigo", red.getSincodigo());
				modLisSintoma.addElement(sintoma.getNombre() + "#" + obtenerEstadoImagen(sintoma.getIndcodigo(), "sma"));
				listaSintomaCodigo.add(sintoma.getIndcodigo());
			}
		}
	}

	public void actualizarItemListaIndicador(String tipo)
	{
		for (int iteLisInd = 0; iteLisInd < aux.lisInd.size(); iteLisInd++)
		{
			Indicador ind = (Indicador) aux.lisInd.get(iteLisInd);
			if (tipo.equals("TRASTORNO"))
			{
				String codigoIndicador = (String)listaTrastornoCodigo.get(listaTrastorno.getSelectedIndex());
				for(int ite = 0; ite < listaTrastornoCodigo.size(); ite++)
				{
					if(ind.getIndcodigo().equals(codigoIndicador))
					{
						Object item = ind.getNombre() + "#" + obtenerEstadoImagen(ind.getIndcodigo(), "sma");
						modLisTrastorno.setElementAt(item, listaTrastorno.getSelectedIndex());
					}
				}
			}
			else if (tipo.equals("SINTOMA"))
			{
				String codigoIndicador = (String)listaSintomaCodigo.get(listaSintoma.getSelectedIndex());
				for(int ite = 0; ite < listaSintomaCodigo.size(); ite++)
				{
					if(ind.getIndcodigo().equals(codigoIndicador))
					{
						Object item = ind.getNombre() + "#" + obtenerEstadoImagen(ind.getIndcodigo(), "sma");
						modLisSintoma.setElementAt(item, listaSintoma.getSelectedIndex());
					}
				}
			}
		}
	}

	@Override
	public void activarFormulario(boolean activo)
	{
		tabPanConsulta.setEnabled(activo);
		activarUsuario(activo);
		activarIndicador(activo);
		activarControlIndicador(activo, "AMBOS");
		activarControlDiagnostico(activo);

	}

	public void activarUsuario(boolean activo)
	{
		txtUsuarioIdentidad.setEnabled(activo);
		txtUsuarioNombre.setEnabled(true);
		txtUsuarioApellido.setEnabled(true);
		txtUsuarioEdad.setEnabled(true);
		cmdValidar.setEnabled(activo);
	}

	public void activarIndicador(boolean activo)
	{
		panFotoDefinicion.setEnabled(activo);
		txtDefinicion.setEnabled(activo);
		listaTrastorno.setEnabled(true);
		listaSintoma.setEnabled(true);
		txtHistoriaDiagnostico.setEnabled(activo);
	}

	public void activarControlDiagnostico(boolean activo)
	{
		cmdDiagHospitalizacion.setEnabled(activo);
		cmdDiagNormal.setEnabled(activo);
		cmdDiagTratamiento.setEnabled(activo);
		cmdGuardar.setEnabled(activo);
		cmdCerrar.setEnabled(activo);
	}

	public void activarControlIndicador(boolean activo, String tipo)
	{
		if (tipo.equals("TRASTORNO") && (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)))
		{
			cmdAlta.setEnabled(activo);
			cmdNormal.setEnabled(activo);
			cmdBaja.setEnabled(activo);
			cmdPresenta.setEnabled(!activo);
			cmdNoPresenta.setEnabled(!activo);
		}
		else if (tipo.equals("SINTOMA") && (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)))
		{
			cmdAlta.setEnabled(!activo);
			cmdNormal.setEnabled(!activo);
			cmdBaja.setEnabled(!activo);
			cmdPresenta.setEnabled(activo);
			cmdNoPresenta.setEnabled(activo);
		}
		else if (tipo.equals("AMBOS"))
		{
			cmdAlta.setEnabled(activo);
			cmdNormal.setEnabled(activo);
			cmdBaja.setEnabled(activo);
			cmdPresenta.setEnabled(activo);
			cmdNoPresenta.setEnabled(activo);
		}
	}

	@Override
	public void refrescarFormulario()
	{
		limpiarFormulario();
		tabPanConsulta.setSelectedIndex(0);
		if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			activarFormulario(false);
			llenarFormulario();

			cmdGuardar.setEnabled(false);
			cmdCerrar.setEnabled(true);
		}
		else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			seleccionarIndicador("DIAGNOSTICO");
			llenarDefinicion("DIAGNOSTICO");

			tabPanConsulta.setEnabled(true);
			activarUsuario(false);
			activarIndicador(true);
			activarControlIndicador(false, "AMBOS");
			activarControlDiagnostico(true);
			llenarFormulario();
			txtUsuarioIdentidad.requestFocus();

			cmdGuardar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
			tabPanConsulta.setEnabled(false);
			activarUsuario(true);
			activarIndicador(false);
			activarControlIndicador(false, "AMBOS");
			activarControlDiagnostico(false);
			txtUsuarioIdentidad.requestFocus();

			cmdGuardar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
	}

	@Override
	public void obtenerDatoFormulario() {
		peticion.put("Usuario_identidad", txtUsuarioIdentidad.getText());
		peticion.put("Usuario_nombre", txtUsuarioNombre.getText());
		peticion.put("Usuario_apellido", txtUsuarioApellido.getText());
		peticion.put("Usuario_udf2", txtUsuarioEdad.getValue().toString());
		peticion.put("Usuario_cargo", "PACIENTE");
		peticion.put("Historia_diagnostico", txtHistoriaDiagnostico.getText());
	}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario()
	{
		if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			if (!txtUsuarioIdentidad.getText().equals(""))
			{
				if (!txtUsuarioNombre.getText().equals(""))
				{
					return true;
				}
			}
		}
		else if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPeticion(Map peticion)
	{
		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{
		return barraProgreso;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{
		return etiAccion;
	}
}
