package com.sis.manejador;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.browse.BrowseObject;
import com.browse.accion.GenerateBrowseObject;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.entidad.Muestra;
import com.comun.entidad.Organizacion;
import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.reporte.accion.ObtenerReportePorModulo;
import com.sis.consulta.accion.ElaborarDatosEstadisticos;
import com.sis.consulta.accion.ExtraerArchivoCasos;
import com.sis.consulta.accion.ExtraerEstadosIndicador;
import com.sis.consulta.accion.GenerarArchivoCasos;
import com.sis.consulta.accion.ImportarArchivoCasos;
import com.sis.consulta.accion.ValidarEstadosIndicadores;
import com.sis.main.PaginaGeneral;

public class ManejadorFrmInicio extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.org = (aux.org == null) ? new Organizacion() : aux.org;
		aux.mue = (aux.mue == null) ? new Muestra() : aux.mue;
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			if(Util.isEmpty(aux.mue.getMuecodigo()))
			{
				 Util.getInst().limpiarEntidad(aux.mue);
			}
		}
	}

	public void cmdAgregarPsicologoEvento(Map peticion) 
	{
		try
		{
			abrirPagina("DlgPsicologo", pagGen.pagAct.getPaginaHijo(), Modo.AGREGANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarSessionEvento(Map peticion) 
	{
		try
		{
			Util.getInst().limpiarEntidad(aux.mue);
			Util.getInst().limpiarEntidad(aux.usuSes);
			actualizaPagina(Modo.VISUALIZANDO);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	public void cmdAgregarDlgUnoEvento(Map peticion) {

		try
		{
			abrirPagina("DlgUno", pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarCasoEvento(Map peticion) {

		try
		{
			abrirPagina("DlgCaso", pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdImportarDataMiningEvento(Map peticion)
	{
		try 
		{
			peticion.put("consulta", "select * from usuario where cargo = 'PACIENTE' and estado <> '0000' order by nombre");
			peticion.put("entidadNombre", "Usuario");
			aux.lisUsu = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaUsuario", aux.lisUsu);

			peticion.put("consulta", "select registrocaso.*  from usuario inner join registrocaso "
					+ "on usuario.usucodigo = registrocaso.paccodigo "
					+ "where registrocaso.estado <> '0000' and usuario.estado<>'0000' and usuario.cargo='PACIENTE'");
			peticion.put("entidadNombre", "RegistroCaso");
			aux.lisRca = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaRegistroCaso", aux.lisRca);
			
			(new ImportarArchivoCasos()).ejecutarAccion(peticion);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAprenderEvento(Map peticion) {

//		try {
//			(new CrearListaRegistroCasoPorUsuario()).ejecutarAccion(peticion);
//		}
//		catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		try
		{
			peticion.put("consulta", "select * from indicador order by tipo");
			peticion.put("entidadNombre", "Indicador");
			aux.lisInd = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaIndicador", aux.lisInd);

			(new ValidarEstadosIndicadores()).ejecutarAccion(peticion);
			boolean indicadoresValidado = (Boolean) peticion.get("indicadorValidado");

			File extractFile = (File) (new GenerarArchivoCasos()).ejecutarAccion(peticion);
			peticion.put("extractFile", extractFile);

			if (true)
			{
				peticion.put("consulta", "select * from usuario where cargo = 'PACIENTE' and estado <> '0000' order by nombre");
				peticion.put("entidadNombre", "Usuario");
				aux.lisUsu = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
				peticion.put("listaUsuario", aux.lisUsu);

				peticion.put("consulta", "select registrocaso.*  from usuario inner join registrocaso "
						+ "on usuario.usucodigo = registrocaso.paccodigo "
						+ "where registrocaso.estado <> '0000' and usuario.estado<>'0000' and usuario.cargo='PACIENTE'");
				peticion.put("entidadNombre", "RegistroCaso");
				aux.lisRca = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
				peticion.put("listaRegistroCaso", aux.lisRca);

				(new ExtraerArchivoCasos()).ejecutarAccion(peticion);

				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Informacion de Extraccion de Conocimiento");
				aux.msg.setTipo("info");
				aux.msg.setValor("El archivo fue extraido satisfactoriamente!!!");
			}
			else
			{
				(new ExtraerEstadosIndicador()).ejecutarAccion(peticion);
				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Informacion de Extraccion de Conocimiento");
				aux.msg.setTipo("info");
				aux.msg.setValor("Los casos registrados no son suficientes para crear una base de conocimientos!!!");
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdListaIndicadorEvento(Map peticion)
	{
		try
		{
			peticion.put("browseTabla", "lista_indicador");
			BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
			peticion.put("browseObject", browseObject);

			abrirPagina("DlgBrowseOpciones-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, "lista_indicador");
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdListaPsicologoEvento(Map peticion)
	{
		try
		{
			if(aux.usuSes.getCargo().equals("ADMINISTRADOR"))
			{
				peticion.put("browseTabla", "lista_psicologo_admin");
			}
			else
			{
				peticion.put("browseTabla", "lista_psicologo");
			}
			BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
			peticion.put("browseObject", browseObject);

			abrirPagina("DlgBrowseOpciones-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdEstadisticaEvento(Map peticion)
	{
		try
		{
			(new ElaborarDatosEstadisticos()).ejecutarAccion(peticion);

			abrirPagina("DlgEstadistica", pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdMenuIndicadorEvento(Map peticion)
	{
		try
		{
			if (Util.isEmpty(aux.usuSes.getUsucodigo().toString()))
			{
				abrirPagina("DlgAcceso", pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdMenuPsicologo(Map peticion)
	{
		try
		{
			if (Util.isEmpty(aux.usuSes.getUsucodigo().toString()))
			{
				abrirPagina("DlgAcceso", pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdListaCasoEvento(Map peticion)
	{
		try
		{
			peticion.put("browseTabla", "lista_caso");
			BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
			peticion.put("browseObject", browseObject);

			abrirPagina("DlgBrowseOpciones-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdListaConsultaEvento(Map peticion)
	{
		try
		{
			peticion.put("browseTabla", "lista_consulta");
			BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
			peticion.put("browseObject", browseObject);

			abrirPagina("DlgBrowseOpciones-" + pagGen.pagAct.getPaginaHijo(), "FrmInicio", Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdConsultarEvento(Map peticion)
	{
		try
		{
			abrirPagina("DlgPresentacion", pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdReporteCasosEvento(Map peticion)
	{
		try
		{

			peticion.put("reporteModulo", "REPCAS");

			(new ObtenerReportePorModulo()).ejecutarAccion(peticion);

			abrirPagina("DlgReporteTabla", pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
