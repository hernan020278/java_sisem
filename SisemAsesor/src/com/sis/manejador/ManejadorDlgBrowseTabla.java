package com.sis.manejador;

import java.util.ArrayList;
import java.util.Map;

import com.browse.BrowseObject;
import com.browse.accion.GenerarBrowse;
import com.browse.accion.GenerateBrowseObject;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgBrowseTabla extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)){}
	}

	public void cmdBuscarEvento(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
				BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
				peticion.put("browseObject", browseObject);

				abrirPagina("DlgBrowseOpciones-" + pagGen.pagAct.getPaginaPadre(), pagGen.pagAct.getPaginaPadre(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void refrescarRegistrosEvento(Map peticion)
	{
		try
		{
			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			(new GenerarBrowse()).ejecutarAccion(peticion);

			actualizaPagina(Modo.VISUALIZANDO);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina(paginaHijo, paginaPadre, Modo.AGREGANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdModificarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			
			abrirPagina(paginaHijo, paginaPadre, Modo.MODIFICANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdEliminarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			Util.getInst().setPeticionEntidad(peticion, aux.msg);

			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			(new GenerarBrowse()).ejecutarAccion(peticion);

			actualizaPagina(Modo.VISUALIZANDO);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdSeleccionarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			cerrarFormulario(peticion);
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
