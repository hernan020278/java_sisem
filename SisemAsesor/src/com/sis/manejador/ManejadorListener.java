/*
 * Created on Jul 15, 2003 
 */
package com.sis.manejador;

import java.util.Map;

import com.comun.database.DBConeccion;

public interface ManejadorListener {

	public Map manejarEvento(Map peticion) throws Exception;

	public void iniciarFormulario(Map peticion);

	public void iniciarInstancias();

	public void limpiarInstancias();

	public void abrirFormulario(Map peticion);

	public void cerrarFormulario(Map peticion);

	public void procesarEvento(Map peticion) throws Exception;
	
	public void procesarMetodo(Map peticion) throws Exception;

	public boolean empezarDBConnection(Map peticion);

	public DBConeccion terminarDBConnection(Map peticion);

	public void etiBuscarEvento(Map peticion);

	public void setPeticionAsincrono(Map peticion);

	public Map getPeticionASincrono();

	public void setAccion(String accion);

	public String getAccion();
}
