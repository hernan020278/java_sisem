package com.sis.manejador;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.BrowseObject;
import com.browse.accion.GenerateBrowseObject;
import com.comun.database.DBConeccion;
import com.comun.entidad.Indicador;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.consulta.accion.EliminarCaso;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgIndicador extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();
				aux.indAct = (Indicador) peticion.get("indicador");
				aux.lisIes = (List) peticion.get("listaIndestado");
				aux.lisRef = (List) peticion.get("listaReferencia");
			}
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.indAct = (aux.indAct == null) ? new Indicador() : aux.indAct;
		aux.lisInd = (aux.lisInd == null) ? new ArrayList() : aux.lisInd;
		aux.lisIes = (aux.lisIes == null) ? new ArrayList() : aux.lisIes;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			Util.getInst().limpiarEntidad(aux.indAct);
		}
		aux.lisInd.clear();
		aux.lisIes.clear();
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

				if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
				{
					// peticion.put("organizacionIde", aux.org.getOrgcodigo());
					// aux.psi = (Usuario) (new CrearUsuario()).ejecutarAccion(peticion);
				}
				else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
				{
					Util.getInst().setPeticionEntidad(peticion, aux.indAct);
				}

				peticion.put("tipoSql", tipoSql);
				peticion.put("entidadObjeto", aux.indAct);
				peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where indcodigo='" + aux.indAct.getIndcodigo() + "'"));
				afectados = dbc.ejecutarCommit(peticion);

				if (afectados > 0)
				{
					dbc.commit();
				}// Fin de if(bookMarkDevCod != 0)
				else
				{
					dbc.rollback();
				}

				cerrarFormulario(peticion);
			}
			else
			{
				this.setEstado(Estado.DUPLICATED_REQUEST);
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdModificarEvento(Map peticion) {

		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				// pagGen.pagAct.setModo(Modo.MODIFICANDO);
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO) || pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				// pagGen.pagAct.setAcceso(Acceso.INICIAR);
				// pagGen.pagAct.setModo(Modo.VISUALIZANDO);
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdBuscarEvento(Map peticion) {

		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
				peticion.put("browseObject", browseObject);

				pagGen.pagAct.setTipoConeccion("transaccion");
				pagGen.pagAct.setPaginaHijo("/browse/browse_form.jsp");
				pagGen.pagAct.setPaginaPadre("/especialista/especialista.jsp");
				pagGen.pagAct.setAcceso(Acceso.INICIAR);
				pagGen.pagAct.setModo(Modo.VISUALIZANDO);
				pagGen.pagAct.setEvento("iniciarFormulario");
				pagGen.pagAct.setAccion("");
				pagGen.pagAct.setNavegar(true);
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdEliminarEvento(Map peticion) {
		try
		{
			peticion.put("paciente", aux.pac);
			peticion.put("historia", aux.his);
			peticion.put("listaRegistroCaso", aux.lisRca);
			peticion.put("muestra", aux.mue);
			peticion.put("mensaje", aux.msg);
			peticion.put("Pagina_modo", pagGen.pagAct.getModo());

			(new EliminarCaso()).ejecutarAccion(peticion);
			iniciarFormulario(peticion);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
