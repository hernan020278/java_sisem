package com.sis.manejador;

import java.util.ArrayList;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.entidad.Indicador;
import com.comun.entidad.Muestra;
import com.comun.entidad.RegistroConsulta;
import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.consulta.accion.EstablecerEvidencias;
import com.sis.consulta.accion.ObtenerDiagnostico;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgConsulta extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
		aux.usu = (aux.usu == null) ? new Usuario() : aux.usu;
		aux.mue = (aux.mue == null) ? new Muestra() : aux.mue;

		aux.indSin = (aux.indSin == null) ? new Indicador() : aux.indSin;
		aux.rcoAct = (aux.rcoAct == null) ? new RegistroConsulta() : aux.rcoAct;

		aux.lisInd = (aux.lisInd == null) ? new ArrayList() : aux.lisInd;
		aux.lisRco = (aux.lisRco == null) ? new ArrayList() : aux.lisRco;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)) {
			Util.getInst().limpiarEntidad(aux.indSin);
			Util.getInst().limpiarEntidad(aux.rcoAct);
		}
	}

	public void cmdOpcionEvento(Map peticion) {
		try
		{
			String estado = (String) peticion.get("estado");
			aux.lisRco.remove(aux.rcoAct);
			aux.rcoAct.setEstado(estado);
			aux.lisRco.add(aux.rcoAct);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void terminarConsultaEvento(Map peticion) {
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			peticion.put("listaRegistroConsulta", aux.lisRco);

			peticion.put("tipoSql", TipoSql.INSERT);
			peticion.put("entidadObjeto", aux.usu);
			peticion.put("where", "");
			afectados = dbc.ejecutarCommit(peticion);

			for (int iteRco = 0; iteRco < aux.lisRco.size(); iteRco++)
			{
				RegistroConsulta rco = (RegistroConsulta) aux.lisRco.get(iteRco);
				peticion.put("tipoSql", TipoSql.INSERT);
				peticion.put("entidadObjeto", rco);
				peticion.put("where", "");
				dbc.ejecutarCommit(peticion);
			}

			if (afectados > 0)
			{
				dbc.commit();

				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Consulta Virtual");
				aux.msg.setTipo("info");
				aux.msg.setValor("Los datos se han Guardado Satisfactoriamente");
			}// Fin de if(bookMarkDevCod != 0)
			else
			{
				dbc.rollback();

				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Informacion de Almacenamiento");
				aux.msg.setTipo("error");
				aux.msg.setValor("Error al almacenar los datos");
			}

			(new EstablecerEvidencias()).ejecutarAccion(peticion);
			(new ObtenerDiagnostico()).ejecutarAccion(peticion);

			abrirPagina("DlgDiagnostico", "FrmInicio", Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
