package com.sis.manejador;

import java.util.Map;

import javax.swing.JOptionPane;

import com.browse.BrowseObject;
import com.browse.accion.GenerateBrowseObject;
import com.comun.database.DBConeccion;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.emails.EmailManager;
import com.sis.consulta.accion.EliminarCaso;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.CrearUsuario;

public class ManejadorDlgPsicologo extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();

				if(peticion.containsKey("psicologo")){aux.psi = (Usuario) peticion.get("psicologo");}
			}
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.psi = (aux.psi == null) ? new Usuario() : aux.psi;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			Util.getInst().limpiarEntidad(aux.psi);
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

				if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
				{
					peticion.put("organizacionIde", aux.org.getOrgcodigo());
					aux.psi = (Usuario) (new CrearUsuario()).ejecutarAccion(peticion);
				}
				else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
				{
					Util.getInst().setPeticionEntidad(peticion, aux.psi);
				}

				// boolean usuarioValidado = (Boolean) (new ValidarUsuario()).ejecutarAccion(peticion);
				boolean usuarioValidado = true;

				if (usuarioValidado)
				{
					peticion.put("tipoSql", tipoSql);
					peticion.put("entidadObjeto", aux.psi);
					peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where usucodigo='" + aux.psi.getUsucodigo() + "'"));
					afectados = dbc.ejecutarCommit(peticion);

					if (!aux.psi.getUsucodigo().equals(""))
					{
						dbc.commit();
						String mensaje = "Se ha creado un nuevo usuario para Ud"
								+ "\nUsuario : " + aux.psi.getUsuario()
								+ "\nClave : " + aux.psi.getClave()
								+ "\nhttp://sysem.sytes.net/sisem/controlador?" +
								"Pagina_organizacion=asesor&" +
								"Pagina_tipoConeccion=coneccion&" +
								"Pagina_Hijo=confirmacion.jsp&" +
								"Pagina_paginaHijo=confirmacion.jsp&" +
								"Pagina_paginaPadre=confirmacion.jsp&" +
								"Pagina_modoAcceso=INICIAR&" +
								"Pagina_estadoPagina=VISUALIZACION&" +
								"Pagina_evento=confirmarClaveUsuarioEvento&" +
								"Pagina_peticion=SUBMIT&Usuario_udf3=" + aux.psi.getUdf3();
						
						String valido = JOptionPane.showInputDialog("�Tiene acceso a internet?",mensaje);
						if(valido != null)
						{
							EmailManager.getInstance().sendHtmlEmail("hernan020278@hotmail.com", aux.psi.getMail(), null, "Confirmacion de password", mensaje, null, OrganizacionGeneral.getOrgcodigo());
						}
						aux.msg.setMostrar(true);
						aux.msg.setTipo("info");
						aux.msg.setTitulo("Email de confirmacion");
						aux.msg.setValor("Se ha enviado un correo de confirmacion a su correo");
					}// Fin de if(bookMarkDevCod != 0)
					else
					{
						dbc.rollback();
					}
					
					cerrarFormulario(peticion);
				}
				else
				{
					this.setEstado(Estado.DUPLICATED_REQUEST);
				}
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdModificarEvento(Map peticion) {

		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				// pagGen.pagAct.setModo(Modo.MODIFICANDO);
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO) || pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				// pagGen.pagAct.setAcceso(Acceso.INICIAR);
				// pagGen.pagAct.setModo(Modo.VISUALIZANDO);
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdBuscarEvento(Map peticion) {

		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
				peticion.put("browseObject", browseObject);

				pagGen.pagAct.setTipoConeccion("transaccion");
				pagGen.pagAct.setPaginaHijo("/browse/browse_form.jsp");
				pagGen.pagAct.setPaginaPadre("/especialista/especialista.jsp");
				pagGen.pagAct.setAcceso(Acceso.INICIAR);
				pagGen.pagAct.setModo(Modo.VISUALIZANDO);
				pagGen.pagAct.setEvento("iniciarFormulario");
				pagGen.pagAct.setAccion("");
				pagGen.pagAct.setNavegar(true);
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdEliminarEvento(Map peticion) {
		try
		{
			peticion.put("paciente", aux.pac);
			peticion.put("historia", aux.his);
			peticion.put("listaRegistroCaso", aux.lisRca);
			peticion.put("muestra", aux.mue);
			peticion.put("mensaje", aux.msg);
			peticion.put("Pagina_modo", pagGen.pagAct.getModo());

			(new EliminarCaso()).ejecutarAccion(peticion);
			iniciarFormulario(peticion);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
