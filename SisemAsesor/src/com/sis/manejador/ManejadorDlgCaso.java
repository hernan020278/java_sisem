package com.sis.manejador;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Historia;
import com.comun.entidad.Indicador;
import com.comun.entidad.Muestra;
import com.comun.entidad.Red;
import com.comun.entidad.RegistroCaso;
import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.consulta.accion.CrearHistoria;
import com.sis.consulta.accion.CrearListaRegistroCaso;
import com.sis.consulta.accion.ObtenerTotalPacientes;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.CrearUsuario;

public class ManejadorDlgCaso extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
				{
					iniciarInstancias();
					limpiarInstancias();

					peticion.put("consulta", "select * from usuario where usucodigo='" + aux.pac.getUsucodigo() + "'");
					peticion.put("entidadNombre", "Usuario");
					aux.pac = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
				}
				else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
				{
					aux.pac = (Usuario) peticion.get("paciente");
					aux.his = (Historia) peticion.get("historia");
					aux.lisRca = (List) peticion.get("listaRegistroCaso");
					aux.lisInd = (List) peticion.get("listaIndicador");
					aux.lisRed = (List) peticion.get("listaRed");
					aux.lisIes = (List) peticion.get("listaIndestado");
				}
			}
			else if (pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
			}

			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
		aux.pac = (aux.pac == null) ? new Usuario() : aux.pac;
		aux.his = (aux.his == null) ? new Historia() : aux.his;
		aux.mue = (aux.mue == null) ? new Muestra() : aux.mue;
		aux.est = (aux.est == null) ? new com.comun.entidad.Indestado() : aux.est;
		aux.red = (aux.red == null) ? new Red() : aux.red;
		aux.indAct = (aux.indAct == null) ? new Indicador() : aux.indAct;
		aux.rgcAct = (aux.rgcAct == null) ? new RegistroCaso() : aux.rgcAct;
		aux.indDia = (aux.indDia == null) ? new Indicador() : aux.indTra;
		aux.rgcDia = (aux.rgcDia == null) ? new RegistroCaso() : aux.rgcDia;
		aux.indTra = (aux.indTra == null) ? new Indicador() : aux.indTra;
		aux.rgcTra = (aux.rgcTra == null) ? new RegistroCaso() : aux.rgcTra;
		aux.indSin = (aux.indSin == null) ? new Indicador() : aux.indSin;
		aux.rgcSin = (aux.rgcSin == null) ? new RegistroCaso() : aux.rgcSin;

		aux.lisInd = (aux.lisInd == null) ? new ArrayList() : aux.lisInd;
		aux.lisRed = (aux.lisRed == null) ? new ArrayList() : aux.lisRed;
		aux.lisRca = (aux.lisRca == null) ? new ArrayList() : aux.lisRca;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			Util.getInst().limpiarEntidad(aux.pac);
			Util.getInst().limpiarEntidad(aux.his);
			Util.getInst().limpiarEntidad(aux.indAct);
			Util.getInst().limpiarEntidad(aux.rgcAct);
			Util.getInst().limpiarEntidad(aux.indDia);
			Util.getInst().limpiarEntidad(aux.rgcDia);
			Util.getInst().limpiarEntidad(aux.indTra);
			Util.getInst().limpiarEntidad(aux.rgcTra);
			Util.getInst().limpiarEntidad(aux.indSin);
			Util.getInst().limpiarEntidad(aux.rgcSin);
		}
		aux.lisInd.clear();
		aux.lisRed.clear();
		aux.lisRca.clear();
	}

	public void cmdValidarClickEvento(Map peticion) {

		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

			String identidad = (String) peticion.get("Usuario_identidad");
			String sql = "select * from usuario where identidad='" + identidad + "' and estado <> '" + Estado.DELETED + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Usuario");
			aux.pac = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);

			if (Util.isEmpty(aux.pac.getUsucodigo().toString()))
			{
				peticion.put("organizacionIde", aux.org.getOrgcodigo());
				aux.pac = (Usuario) (new CrearUsuario()).ejecutarAccion(peticion);
				/**
				 * Obtener listas de registros
				 */
				peticion.put("consulta", "select * from indicador order by tipo");
				peticion.put("entidadNombre", "Indicador");
				aux.lisInd = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
				peticion.put("lisInd", aux.lisInd);

				peticion.put("consulta", "select * from red order by tracodigo");
				peticion.put("entidadNombre", "Red");
				aux.lisRed = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

				peticion.put("consulta", "select * from indestado order by indcodigo");
				peticion.put("entidadNombre", "Indestado");
				aux.lisIes = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);

				/**
				 * Crear registros para historia clinica
				 */
				peticion.put("muestra", aux.mue);
				peticion.put("paciente", aux.pac);
				aux.his = (Historia) (new CrearHistoria()).ejecutarAccion(peticion);
				peticion.put("historia", aux.his);

				aux.lisRca = (List) (new CrearListaRegistroCaso()).ejecutarAccion(peticion);

				peticion.put("resultado", true);
			}
			else
			{
				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Informacion de Almacenamiento");
				aux.msg.setTipo("info");
				aux.msg.setValor("Este usuario ya existe");
				peticion.put("resultado", false);
			}
			
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void lisTrastornoEvento(Map peticion) {

		try
		{
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

				boolean usuarioValidado = true;

				if (usuarioValidado)
				{
					Util.getInst().setPeticionEntidad(peticion, aux.pac);
					peticion.put("tipoSql", tipoSql);
					peticion.put("entidadObjeto", aux.pac);
					peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where usucodigo='" + aux.pac.getUsucodigo() + "'"));
					afectados = dbc.ejecutarCommit(peticion);

					Util.getInst().setPeticionEntidad(peticion, aux.his);
					peticion.put("tipoSql", tipoSql);
					peticion.put("entidadObjeto", aux.his);
					peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where hiscodigo='" + aux.his.getHiscodigo() + "'"));
					dbc.ejecutarCommit(peticion);

					for (int iteRgc = 0; iteRgc < aux.lisRca.size(); iteRgc++)
					{
						RegistroCaso rgc = (RegistroCaso) aux.lisRca.get(iteRgc);
						peticion.put("tipoSql", tipoSql);
						peticion.put("entidadObjeto", rgc);
						peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where rcacodigo='" + rgc.getRcacodigo() + "'"));
						dbc.ejecutarCommit(peticion);
					}

					int totalCasos = (Integer) (new ObtenerTotalPacientes()).ejecutarAccion(peticion);
					
					aux.mue.setCantidad(totalCasos);
					peticion.put("tipoSql", TipoSql.UPDATE);
					peticion.put("entidadObjeto", aux.mue);
					peticion.put("where", "where muecodigo='" + aux.mue.getMuecodigo() + "'");
					afectados = dbc.ejecutarCommit(peticion);

					if (afectados > 0)
					{
						dbc.commit();
					}// Fin de if(bookMarkDevCod != 0)
					else
					{
						dbc.rollback();
					}

					cerrarFormulario(peticion);
				}
				else
				{
					this.setEstado(Estado.DUPLICATED_REQUEST);
				}
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdEstablecerDiagnosticoEvento(Map peticion) {
		try
		{
			String estado = (String) peticion.get("estado");
			aux.lisRca.remove(aux.rgcAct);
			aux.rgcAct.setEstado(estado);
			aux.lisRca.add(aux.rgcAct);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdEstablecerEstadoEvento(Map peticion) {
		try
		{
			String estado = (String) peticion.get("estado");
			aux.lisRca.remove(aux.rgcAct);
			aux.rgcAct.setEstado(estado);
			aux.lisRca.add(aux.rgcAct);
			actualizaPagina(pagGen.pagAct.getModo());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
