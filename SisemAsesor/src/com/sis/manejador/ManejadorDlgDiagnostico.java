package com.sis.manejador;

import java.util.Map;

import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgDiagnostico extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			else if (pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
			}

			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)) {
			// Util.getInstance().limpiarEntidad(aux.indSin);
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
