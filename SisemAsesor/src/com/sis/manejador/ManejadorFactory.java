/*
 * Created on Jul 15, 2003 
 */
package com.sis.manejador;

import java.util.Map;

import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;
import com.sis.main.Pagina;
import com.sis.main.PaginaGeneral;

public class ManejadorFactory
{

	private static ManejadorFactory INSTANCE;
	private ManejadorListener manejador = null;

	public static ManejadorFactory getInstance()
	{
		if (INSTANCE == null)
		{

			return new ManejadorFactory();
		}
		return INSTANCE;
	}

	public ManejadorListener obtenerManejador(Map peticion) throws Exception {

		Log.debug(this,"Obteniendo manejador... exito");

		try
		{
			PaginaGeneral pagGen = PaginaGeneral.getInstance();
			pagGen.pagAct.setObjeto(PaginaGeneral.getInstance().establecerPaginaCache(pagGen.pagAct));

			String paginaHijo = Util.getInst().obtenerNombreFromUrlJsp(pagGen.pagAct.getPaginaHijo());
			String objetoManejador = getNombrePaquete() + paginaHijo;
			manejador = (ManejadorListener) Class.forName(objetoManejador).newInstance();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return manejador;

	}

	private String getNombrePaquete() throws java.lang.Exception {
		String packageName = "";
		try {
			packageName = "com.sis.manejador.";
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (packageName.indexOf("com", 0) < 0) {
				packageName = "com.manejador.";
			}
			return packageName;
		}
	}
}
