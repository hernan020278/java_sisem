package com.sis.manejador;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.entidad.Historia;
import com.comun.entidad.Muestra;
import com.comun.entidad.Organizacion;
import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.consulta.accion.CrearListaRegistroConsulta;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.CrearUsuario;

public class ManejadorDlgConducir extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			else if (pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
			}

			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.org = (aux.org == null) ? new Organizacion() : aux.org;
		aux.mue = (aux.mue == null) ? new Muestra() : aux.mue;
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
		aux.usu = (aux.usu == null) ? new Usuario() : aux.usu;
		aux.his = (aux.his == null) ? new Historia() : aux.his;
		aux.lisRco = (aux.lisRco == null) ? new ArrayList() : aux.lisRco;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			Util.getInst().limpiarEntidad(aux.usu);
		}
		aux.lisRco.clear();
	}

	public void cmdEmpezarEvento(Map peticion)
	{
		try
		{
			peticion.put("organizacionIde", aux.org.getOrgcodigo());
			aux.usu = (Usuario) (new CrearUsuario()).ejecutarAccion(peticion);
			/**
			 * OBTENER LA LISTA DE SINTOMAS PARA LA CONSULTA
			 */
			peticion.put("consulta", "select * from indicador where tipo='SINTOMA' order by tipo");
			peticion.put("entidadNombre", "Indicador");
			aux.lisInd = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			peticion.put("listaSintomas", aux.lisInd);

			/**
			 * CREAR REGISTROS PARA CONSULTA CLINICA
			 */
			peticion.put("muestra", aux.mue);
			peticion.put("usuario", aux.usu);

			aux.lisRco = (List) (new CrearListaRegistroConsulta()).ejecutarAccion(peticion);
			abrirPagina("DlgConsulta", "FrmInicio", Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
