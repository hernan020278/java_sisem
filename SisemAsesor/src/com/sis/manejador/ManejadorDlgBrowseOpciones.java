package com.sis.manejador;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.ObtenerListaColumnaUsuarios;

public class ManejadorDlgBrowseOpciones extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
		}
	}

	public void cmdBuscarEvento(Map peticion)
	{
		try
		{
			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			(new GenerarBrowse()).ejecutarAccion(peticion);

			if (pagGen.pagAct.getPaginaPadre().equals("DlgTablaGeneral"))
			{
				cerrarFormulario(peticion);
			}
			else
			{
				abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaPadre(), pagGen.pagAct.getPaginaPadre(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void txtExpresionObtenerListaEvento(Map peticion)
	{
		try
		{
			String nombreColumna = (String) peticion.get("nombreColumna");
			String consulta = (String) peticion.get("consulta");
			String filtro = (String) peticion.get("filtro");
			String campo = Util.getInst().parsearCampoObjeto(nombreColumna);

			consulta = consulta.replace("filtro", campo + " " + filtro);
			peticion.put("consulta", consulta);

			List listaExpresion = (List) (new ObtenerListaColumnaUsuarios()).ejecutarAccion(peticion);

			peticion.put("listaExpresion", listaExpresion);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
