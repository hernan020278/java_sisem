package com.sis.browse;

import java.awt.Component;
import java.awt.Font;
import java.math.BigDecimal;

import javax.swing.AbstractCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.browse.BrowseColumn;
import com.browse.BrowseUtility;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.referencia.Modo;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoDecimal;
import com.sis.main.VistaAdatper;

public class CellEditTextInput extends AbstractCellEditor implements TableCellEditor {

	private VistaAdatper dlgVistaAdapter;
	private JTextFieldChanged txtCelda;
	private int fila;
	private int columna;
	private JTable tabla;
	private BrowseColumn[] browseColumns; 

	public CellEditTextInput(VistaAdatper dlgVistaAdapter, JTable tabla, BrowseColumn[] browseColumns) {

		this.dlgVistaAdapter = dlgVistaAdapter;
		this.tabla = tabla;
		this.browseColumns = browseColumns;
		txtCelda = new JTextFieldChanged(50);
		txtCelda.setFont(new Font("Tahoma", Font.PLAIN, 14));
	}// TabDetKarIngEditorJTextEntero(DlgAdminKardex parDlgKardex)

	@Override
	public boolean stopCellEditing() {

		String valorCelda = (String) getCellEditorValue();// Valor de Descuento
		valorCelda = (valorCelda.equals("")? " ": valorCelda);
//		if(dlgVistaAdapter.frmAbierto) {
			if(this.tabla.isEditing()) {
				if(dlgVistaAdapter.pagGen.pagAct.getModo().equals(Modo.AGREGANDO)	|| dlgVistaAdapter.pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)) {
					String valorMetodo = "";
					String columnaNombre = "";
					String scriptCode = "";
					String aliasTablaHeader = (String) tabla.getColumnModel().getColumn(columna).getHeaderValue();
					for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
					{
						BrowseColumn browseColumn = browseColumns[iteBrwCol];
						if(iteBrwCol == 0)
						{
							columnaNombre = aliasTablaHeader;
							scriptCode = browseColumn.getScriptCode();
						}
						if(browseColumn.getAlias().equalsIgnoreCase(aliasTablaHeader) || browseColumn.getLabel().equalsIgnoreCase(aliasTablaHeader))
						{
							columnaNombre = browseColumn.getColumnName();
							scriptCode = browseColumn.getScriptCode();
							break;
						}
					}
					for(int iteCol = 0; iteCol <this.tabla.getColumnCount(); iteCol++)
					{
						String header =  (String) this.tabla.getColumnModel().getColumn(iteCol).getHeaderValue();
					
						if(header.equalsIgnoreCase(scriptCode))
						{
								valorMetodo = (String)tabla.getValueAt(fila, iteCol);
								break;
						}
					}
					if (valorMetodo.contains(","))
					{
						String[] valor = valorMetodo.split(",");

						dlgVistaAdapter.paginaHijo = (valor[0] != null) ? valor[0] : "";
						dlgVistaAdapter.paginaPadre = dlgVistaAdapter.pagGen.pagAct.getPaginaHijo();
						dlgVistaAdapter.paginaEvento = (valor[1] != null) ? valor[1] : "";
						dlgVistaAdapter.paginaAccion = (valor[2] != null) ? valor[2] : "";
						String valorTemp = (valor[3] != null) ? valor[3] : "";
						dlgVistaAdapter.paginaValor = valorTemp + "#"+ columnaNombre + "#" + valorCelda;

						dlgVistaAdapter.abrirPagina(dlgVistaAdapter.paginaHijo, dlgVistaAdapter.paginaEvento, dlgVistaAdapter.paginaAccion, dlgVistaAdapter.paginaValor);
					}
				} else {
					JOptionPane.showConfirmDialog(null,
							"���SE ACTUALIZA CUANDO SE AGREGA O SE MODIFICA!!!"
									+ "\n" + this.getClass().getName(),
							"Sistema",
							JOptionPane.PLAIN_MESSAGE,
							JOptionPane.INFORMATION_MESSAGE);
					cancelCellEditing();
				}
			}// fin de if (tabDetFac.isEditing())
//		}// Fin de if(dlgVistaAdapter.frmAbierto)
		return super.stopCellEditing();
	}

	@Override
	public void cancelCellEditing() {

		super.cancelCellEditing();
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, final Object value, boolean isSelected, int row, int column) {
		fila = row;
		columna = column;
		tabla = table;
		
		txtCelda.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
		txtCelda.setText((String)value);
		return txtCelda;
	}

	@Override
	public Object getCellEditorValue() {

		return txtCelda.getText();
	}
}