package com.sis.browse;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.comun.referencia.Modo;

public class CellRenderJCheckBox implements TableCellRenderer {

	private JCheckBox checkBox;

	public CellRenderJCheckBox(JCheckBox checkBox)
	{
		this.checkBox = checkBox;
		checkBox.setHorizontalAlignment(javax.swing.JTextField.CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col)
	{
		checkBox.setSelected((Boolean) value);
		return checkBox;
	}
}