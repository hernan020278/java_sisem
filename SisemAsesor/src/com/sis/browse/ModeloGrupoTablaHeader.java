package com.sis.browse;

import javax.swing.table.DefaultTableModel;

import com.browse.BrowseColumn;

public class ModeloGrupoTablaHeader extends DefaultTableModel
{
	private static final long serialVersionUID = 1L;
	private BrowseColumn browseColumns[];

	public ModeloGrupoTablaHeader() {

		super.addColumn("Uno");
		super.addColumn("Dos");
		super.addColumn("Tres");
	}

	public Class getColumnClass(int col) {
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}
}
