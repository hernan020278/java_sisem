package com.sis.browse;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.browse.BrowseColumn;

public class ModeloTablaGeneral extends DefaultTableModel
{
	private static final long serialVersionUID = 1L;
	private BrowseColumn browseColumns[];

	public ModeloTablaGeneral(BrowseColumn browseColumns[])
	{
		this.browseColumns = browseColumns;
		for (int iteBrwCol = 0; iteBrwCol < this.browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = (BrowseColumn) this.browseColumns[iteBrwCol];
			super.addColumn(browseColumn.getColumnName());
		}
	}

	@Override
	public Class getColumnClass(int col)
	{
		if (col < browseColumns.length)
		{
			BrowseColumn browseColumn = (BrowseColumn) this.browseColumns[col];
			if (browseColumn.getType().equals("STRING"))
			{
				if (browseColumn.isButtonInput())
				{
					return JButton.class;
				}
				return String.class;
			}
			else if (browseColumn.getType().equals("INTEGER"))
			{
				if (browseColumn.isCheckbox())
				{
					return Boolean.class;
				}
				return Integer.class;
			}
			else if (browseColumn.getType().equals("DOUBLE"))
			{
				return Double.class;
			}
			else if (browseColumn.getType().equals("BOOLEAN"))
			{
				return Boolean.class;
			}
		}
		// else if(col >= browseColumns.length)
		// {
		// return JButton.class;
		// }
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		BrowseColumn browseColumn = browseColumns[col];

		if (browseColumn.isEditable())
		{
			return true;
		}
		return false;
	}
}
