package com.sis.browse;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.comun.referencia.Modo;

public class CellRenderJButton implements TableCellRenderer {

	private JButton boton;

	public CellRenderJButton(JButton boton)
	{
		this.boton = boton;
		boton.setFocusable(false);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col)
	{
		if (((String) value).contains(","))
		{
			String[] valor = ((String) value).split(",");
			if (valor[1].toUpperCase().indexOf(Modo.VISUALIZANDO) > 0)
			{
				boton.setText(valor[3]);
			}
		}
		else
		{
			boton.setText((String) value);
		}
		return boton;
	}
}