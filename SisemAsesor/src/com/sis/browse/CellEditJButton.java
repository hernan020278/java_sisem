package com.sis.browse;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.comun.referencia.Modo;
import com.sis.main.VistaAdatper;

public class CellEditJButton extends AbstractCellEditor implements TableCellEditor {

	private JButton boton;
	private VistaAdatper dlgTablaGeneral;
	private JTable table;
	private int col, fil;

	public CellEditJButton(JButton boton, VistaAdatper parDlgTablaGeneral)
	{
		this.dlgTablaGeneral = parDlgTablaGeneral;
		this.boton = boton;
		this.boton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String valorCelda = (String) getCellEditorValue();
				if (valorCelda.contains(","))
				{
					String[] valor = valorCelda.split(",");

					dlgTablaGeneral.paginaHijo = (valor[0] != null) ? valor[0] : "";
					dlgTablaGeneral.paginaPadre = dlgTablaGeneral.pagGen.pagAct.getPaginaHijo();
					dlgTablaGeneral.paginaEvento = (valor[1] != null) ? valor[1] : "";
					dlgTablaGeneral.paginaAccion = (valor[2] != null) ? valor[2] : "";
					dlgTablaGeneral.paginaValor = (valor[3] != null) ? valor[3] : "";

					dlgTablaGeneral.abrirPagina(dlgTablaGeneral.paginaHijo, dlgTablaGeneral.paginaEvento, dlgTablaGeneral.paginaAccion, dlgTablaGeneral.paginaValor);
				}
			}
		});
	}// Fin de CellEditJComboArtCodIntTabDetKar

	@Override
	public java.awt.Component getTableCellEditorComponent(javax.swing.JTable table, Object value, boolean isSelected, int row, int column)
	{
		this.table = table;
		if (((String) value).contains(","))
		{
			String[] valor = ((String) value).split(",");
			if (valor[1].toUpperCase().indexOf(Modo.VISUALIZANDO) > 0)
			{
				boton.setText(valor[3]);
			}
		}
		else
		{
			boton.setText((String) value);
		}
		boton.setName((String) value);
		return boton;
	}

	@Override
	public boolean stopCellEditing()
	{
		return super.stopCellEditing();

	}// Fin de stopCellEditing

	// Hitting enter results in an actioncommand "comboBoxEdited"
	// Implementing CellEditor
	@Override
	public Object getCellEditorValue() {
		return boton.getName();
	}// Fin de getCellEditorValue
}