package com.sis.browse;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaUnida extends DefaultTableModel
{
	private static final long serialVersionUID = 1L;

	public ModeloTablaUnida()
	{
		super.addColumn("Uno");
		super.addColumn("Dos");
		super.addColumn("Tres");
		super.addColumn("Cuatro");
		super.addColumn("Cinco");
		super.addColumn("Seis");
	}

	public Class getColumnClass(int col) {
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}
}
