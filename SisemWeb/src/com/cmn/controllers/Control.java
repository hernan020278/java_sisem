package com.cmn.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.comun.utilidad.Log;
import com.comun.utilidad.Util;

public class Control extends Controlador {
	public Map index(Map data)  throws IOException{
		Map result = new HashMap<>();
		try {
			HttpServletResponse response = (HttpServletResponse) data.get("response");
			HttpServletRequest request = (HttpServletRequest) data.get("request");

			String varURI = request.getRequestURI();
			varURI = (varURI.startsWith("/") ? varURI.substring(1) : varURI);
			String[] segments = varURI.split("/");

			if (segments.length == 2 || (segments.length == 3 && segments[2] == "index.jsp")) {
				data.put("dbapp", "cmn");
				data.put("modulo", "controllers");
				data.put("archivo", "cmn/home");
				data.put("method", "index");
			} else if (segments.length == 3 && segments[2].contains("Auto")) {
				Map app = (Map) data.get("app");
				data.put("dbapp", app.get("pol_temp"));
				data.put("modulo", "controllers");
				data.put("archivo", app.get("pol_temp").toString() + "/" + segments[2]);
				data.put("method", "ejecutar");
			} else if (segments.length == 4) {
				data.put("dbapp", segments[2]);
				data.put("modulo", "controllers");
				data.put("archivo", segments[2] + "/" + segments[3]);
				data.put("method", "index");
			} else if (segments.length == 5) {
				data.put("dbapp", segments[2]);
				data.put("modulo", "controllers");
				data.put("archivo", segments[2] + "/" + segments[3]);
				data.put("method", segments[4]);
			} else if (segments.length == 6) {
				data.put("dbapp", segments[2]);
				data.put("modulo", "controllers");
				data.put("archivo", segments[2] + "/" + segments[3]);
				data.put("method", segments[4]);
				data.put("kycom", segments[5]);
			}

			this.ejecutar(data);

		} catch (Exception e) {

			throw new IOException(e.getMessage());
		}
		return data;
	}

	public Map ejecutar(Map data) throws IOException {
		Map result = new HashMap<>();
		try {
			boolean is_json = false;
			
			String tipoPeticion = data.get("tipoPeticion").toString();
			if ((data.get("archivo").toString() == "cmn/control" && data.get("method").toString() == "ejecutar")
					|| (data.get("archivo").toString().contains("Auto"))
					|| (data.containsKey("is_json") && data.get("is_json").toString() == "true")) {
				is_json = true;
			}

			data = this.run(data.get("archivo").toString(), data);

			/*
			if (is_json) {
				response.setContentType("application/json");
				response.setHeader("Cache-Control", "nocache");
				response.setCharacterEncoding("utf-8");

				String dataJson = Util.getInst().getDataJson(data);
				response.getWriter().print(dataJson);

				// echo json_encode($data);
			} else {
				/*
				 * resp.setContentType("text/plain"); resp.setHeader("Cache-Control",
				 * "no-cache"); resp.setHeader("Content", "text/html;charset=iso-8859-1");
				 * resp.getWriter().write(output);
				 */
			/*
				request.getServletContext().getRequestDispatcher("pagina.jsp").forward(request, response);
			}

			Log.debug(this, request.getParameter("userId") + " - HiltonServletController.doPost END ");
			long startRequest = System.currentTimeMillis();
			long endRequest = System.currentTimeMillis();
			*/
		} catch (Exception e) {

			throw new IOException(e.getMessage());
		}
		return data;
	}
}
