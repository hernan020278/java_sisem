package com.cmn.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.comun.utilidad.Log;

public class home extends Controlador
{
	public Map index(Map data)
	{
		try
		{
			data = this.ejecutar(data);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return data;
	}

	public Map ejecutar(Map data) throws IOException
	{
		Map result = new HashMap<>();
		try
		{
			HttpServletResponse response = (HttpServletResponse) data.get("response");
			HttpServletRequest request = (HttpServletRequest) data.get("request");

			
			if( request.getAttribute("crearComunidad") != null && request.getAttribute("crearComunidad") != "") 
			{
				request.setAttribute("crearComunidad", false);
				//request.getServletContext().getRequestDispatcher("/SisemWeb/code/app/com/cmn/views/main.jsp").forward(request, response);
				response.sendRedirect("/SisemWeb/code/app/com/cmn/views/main.jsp");
			}
			else 
			{
				request.removeAttribute("q");
				//request.getServletContext().getRequestDispatcher("/SisemWeb/code/app/com/cmn/views/buscar.jsp").forward(request, response);
				response.sendRedirect("/SisemWeb/code/app/com/cmn/views/template.jsp");
			}
			/*
			if(isset($_SESSION['crearComunidad']) && $_SESSION['crearComunidad'])
			{
				$template = Array();
				$_SESSION['crearComunidad']=false;
				$template["header"] = $this->vista('cmn/header', Array("userdata"=>(isset($_SESSION) ? $_SESSION : null)), true);
				$template["body"] = $this->vista('cmn/body', Array("userdata"=>(isset($_SESSION) ? $_SESSION : null)), true);
				$this->vista("cmn/template",Array(
						"userdata"=>$_SESSION,
						"template"=>$template
				));
			}
			else
			{
				unset($_SESSION['q']);
				$template["header"] = $this->vista('cmn/header', Array("userdata"=>(isset($_SESSION) ? $_SESSION : null)), true);
				$template["body"] = $this->vista('cmn/search', Array("userdata"=>(isset($_SESSION) ? $_SESSION : null)), true);
				$this->vista("cmn/template",Array(
						"template"=>$template,
						'title'=>'Sisprom - La nube es solo el principio',
						'metadata'=>Array(
								'description'=>'Sisprom es la Red de Negocios mas completa y poderosa donde podras interacturar personal y comercialmente con empresas, instituciones o personas',
								'keywords'=>'sisprom, sisprom.com, sisprom srl, sisprom labs, empresas, pymes, mypes, compras online, ventas online, sistemas de informacion, online, comunidades, foros, aplicaciones'
						)
				));
			}
*/
			//Log.debug(this, "");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new IOException(e.getMessage());
		}
		return data; 
	}
}
