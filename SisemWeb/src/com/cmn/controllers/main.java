package com.cmn.controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.comun.utilidad.Util;

public class main {

	public main() {}
	
	public void index(Map data)
	{
		try 
		{
			HttpServletResponse response = (HttpServletResponse) data.get("response");
			
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "nocache");
			response.setCharacterEncoding("utf-8");
			
			String dataJson =  Util.getInst().getDataJson(data);
			response.getWriter().print(dataJson);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
