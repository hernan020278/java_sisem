package com.cmn.controllers;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.comun.utilidad.Util;

public class Controlador {

	private Controlador controller;

	public Map<String, Object> run(String archivo, Map<String, Object> data) throws IOException {
		try {

			if (!archivo.isEmpty()) 
			{
				String[] arrArchivo = archivo.split("/");
				if (arrArchivo.length == 1) 
				{
					throw new IOException("Controlador no tiene la nomenclatura [carpeta/" + arrArchivo[0] + "]");
				}
				data.put("dbapp", arrArchivo[0]);
				data.put("archivo", arrArchivo[1]);
			}
			String objController = "com.cmn.controllers." + data.get("archivo").toString();
			controller = (Controlador) Class.forName(objController).newInstance();

			if (data.containsKey("method") && data.containsKey("archivo")
					&& data.get("archivo").toString().contains("Auto")
					&& data.get("method").toString().contains("index")) {
				data.remove("method");
			} // if(isset($data['method']) && !empty($data['method']))

			if (data.get("archivo").toString().contains("GetLista")) {
				data.remove("sql");
			} // if(isset($data['method']) && !empty($data['method']))

			/**
			 * 
			 */
			Object resp = null;
			Class[] argTypes = new Class[] { Map.class };
			String metodo = data.get("method").toString();
			try 
			{
				Method objMetodo = controller.getClass().getMethod(metodo, argTypes);
				resp = objMetodo.invoke(controller, data);
				data = (Map) resp;
			} 
			catch (Exception exception) 
			{
//				Log.error(this, "Error en el metodo : " + metodo);
				exception.printStackTrace();
			}
			// error_log($data['archivo']."\n", 3, "error.log");
		} catch (Exception e) {
			throw new IOException("Error al obtener valor de data");
		}
		return data;
	}
}
