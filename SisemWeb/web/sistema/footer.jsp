			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<ul id="cm_GlobalGrid" class="dropdown-menu" role="menu" style="display:none" >
			<li><a tabindex="-1" id="cm_GlobalGrid_deta" href="javascript:void(0);">Ver</a></li>
		    <li><a tabindex="-1" id="cm_GlobalGrid_edit" href="javascript:void(0);">Editar</a></li>
		    <li><a tabindex="-1" id="cm_GlobalGrid_dele" href="javascript:void(0);">Eliminar</a></li>
		</ul>
		<ul id="cm_PopupGrid" class="dropdown-menu" role="menu" style="display:none" >
			<li><a tabindex="-1" id="cm_PopupGrid_edit" href="javascript:void(0);">Editar</a></li>
		    <li><a tabindex="-1" id="cm_PopupGrid_dele" href="javascript:void(0);">Eliminar</a></li>
		    <li><a tabindex="-1" id="cm_PopupGrid_stck" href="javascript:void(0);">Stock</a></li>
		</ul>
		<ul id="cm_KardexGrid" class="dropdown-menu" role="menu" style="display:none" >
			<li><a tabindex="-1" id="cm_KardexGrid_edit" href="javascript:void(0);">Edit</a></li>
		</ul>
		<ul id="cm_CcteGrid" class="dropdown-menu" role="menu" style="display:none" >
			<li><a tabindex="-1" id="cm_CcteGrid_deta" href="javascript:void(0);">Ver</a></li>
		    <li><a tabindex="-1" id="cm_CcteGrid_edit" href="javascript:void(0);">Editar</a></li>
		    <li><a tabindex="-1" id="cm_CcteGrid_apro" href="javascript:void(0);">Aprobar</a></li>
		    <li><a tabindex="-1" id="cm_CcteGrid_anul" href="javascript:void(0);">Anular</a></li>		    
		    <li><a tabindex="-1" id="cm_CcteGrid_dele" href="javascript:void(0);">Eliminar</a></li>
		    <li><a tabindex="-1" id="cm_CcteGrid_prin" href="javascript:void(0);">Imprimir</a></li>
		</ul>
		<ul id="cm_DocuGrid" class="dropdown-menu" role="menu" style="display:none" >
			<li><a tabindex="-1" id="cm_DocuGrid_deta" href="javascript:void(0);">Ver</a></li>
		    <li><a tabindex="-1" id="cm_DocuGrid_edit" href="javascript:void(0);">Editar</a></li>
		    <li><a tabindex="-1" id="cm_DocuGrid_apro" href="javascript:void(0);">Aprobar</a></li>
		    <li><a tabindex="-1" id="cm_DocuGrid_anul" href="javascript:void(0);">Anular</a></li>		    
		    <li><a tabindex="-1" id="cm_DocuGrid_dele" href="javascript:void(0);">Eliminar</a></li>
		    <li><a tabindex="-1" id="cm_DocuGrid_prin" href="javascript:void(0);">Imprimir</a></li>
		</ul>
		<ul id="cm_LogiGuiaGrid" class="dropdown-menu" role="menu" style="display:none" >
			<li><a tabindex="-1" id="cm_LogiGuiaGrid_deta" href="javascript:void(0);">Ver</a></li>
		    <li><a tabindex="-1" id="cm_LogiGuiaGrid_edit" href="javascript:void(0);">Editar</a></li>
		    <li><a tabindex="-1" id="cm_LogiGuiaGrid_apro" href="javascript:void(0);">Aprobar</a></li>
		    <li><a tabindex="-1" id="cm_LogiGuiaGrid_anul" href="javascript:void(0);">Anular</a></li>		    
		    <li><a tabindex="-1" id="cm_LogiGuiaGrid_dele" href="javascript:void(0);">Eliminar</a></li>
		    <li><a tabindex="-1" id="cm_LogiGuiaGrid_prin" href="javascript:void(0);">Imprimir</a></li>
		</ul>

		<script src="<%=apps_url%>js/bootstrap/bootstrap.min.js"></script>
		<script src="<%=apps_url%>js/notification/SmartNotification.min.js"></script>
		<script src="<%=apps_url%>js/smartwidgets/jarvis.widget.min.js"></script>
		<script src="<%=apps_url%>js/plugin/jquery-timepicker/jquery.timepicker.js"></script>
		<script src="<%=apps_url%>js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="<%=apps_url%>js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<script src="<%=apps_url%>js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<script src="<%=apps_url%>js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<script src="<%=apps_url%>js/plugin/fastclick/fastclick.js"></script>
		<script src="<%=apps_url%>js/plugin/blockUI/jquery.blockUI.js"></script>
		<script src="<%=apps_url%>js/plugin/contextjs/context.js"></script>
		<script src="<%=apps_url%>js/moment/moment-with-locales.js"></script>
		<script src="<%=apps_url%>js/plugin/x-editable/x-editable.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="<%=apps_url%>js/theme.js"></script>

		<!-- Demo purpose only -->
		<script src="<%=apps_url%>js/demo.js"></script>

		<!-- SISEM FRAMEWORK -->
		<script src="<%=apps_url%>js/sisem.js"></script>
		<script src="<%=apps_url%>js/requirejs/require.js"></script>

<%-- 		<script src="<%=apps_url%>scripts/init.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/corp/loca.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/corp/sucu.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/corp/alma.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/corp/enti.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/vent/prec.js"></script> --%>

<%-- 		<script src="<%=apps_url%>scripts/logi/arti.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/unid.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/marc.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/cate.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/docu.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/kard.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/ccor.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/pagc.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/rdoc.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/logi/rbal.js"></script> --%>

<%-- 		<script src="<%=apps_url%>scripts/vent/lpre.js"></script> --%>
<%-- 		<script src="<%=apps_url%>scripts/segu/usua.js"></script> --%>
		
<%-- 		<script src="<%=apps_url%>scripts/test.js"></script> --%>
		<script type="text/JavaScript">
			<%
				String valorGet = ((request.getParameter("Pagina_valor")!=null) ? request.getParameter("Pagina_valor") : "");
			%>
			Pagina_valor = "<%=valorGet%>";
			//abrirPaginaAjax("sistema/acceso.jsp");
		</script>
	</body>
</html>