<%@ include file="/sistema/context.jsp"%>
<html>
<head>
	<!-------------------------------------------- CSS DEL SISTEMA PRINCIPAL------------------------------------------------>

	<link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/reset.css"/>
<%-- 	<link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/sisem.css"/> --%>

	<!-------------------------------------------- FRAMEWORK JQUERY------------------------------------------------>

	<script type="text/javascript" src="<%=contextPath%>/js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/js/jquery-ui.js"></script>

	<link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/jquery-ui.css">
	<link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/jquery.panel.css">
	
    <!-------------------------------------------------------SCRIPT RADIOCHECKED--------------------------------------------->

    <script type="text/javascript" src="<%=contextPath%>/js/prettyCheckable.js"></script>
    <link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/prettyCheckable.css"/>

	<!-------------------------------------------JS Y CSS PARA MSGBOX ------------------------------------------------------>	    

    <script type="text/javascript" src="<%=contextPath%>/js/jquery.msgBox.js"></script>
    <link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/msgBoxLight.css"/>
    
	<!---------------------------------SCRIPT INICIAR COMPONENTES JQUERY---------------------------------------------------->	    

	<script type="text/javascript" src="<%=contextPath%>/js/sisem.js"></script>
	<script type="text/JavaScript" src="<%=contextPath%>/js/config.js"></script>

	<!------------------------------------------------ CONFIGURACION SCRIP DE SISTEMA--------------------------------------->

</head>
<body>
	<form name="frm" target="_parent" action="<%=contextPath%>/controlador" method="post">
