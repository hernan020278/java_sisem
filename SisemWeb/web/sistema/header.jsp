<%
String apps_url = "/sisem/";
%>
<!DOCTYPE html>
<html lang="en-us" ng-app="sisprom">
<head>
		<script type="text/javascript">
			var gIdLoc=0;
			var gIdSuc=0;
			var gIdAlm=0;
			var base_url= "<%=apps_url%>";
			var estado	= null;
			var strDia 	= new Array();
			var fechor = '';
			strDia[0]="DOMINGO";
			strDia[1]="LUNES";
			strDia[2]="MARTES";
			strDia[3]="MIERCOLES";
			strDia[4]="JUEVES";
			strDia[5]="VIERNES";
			strDia[6]="SABADO";
			var strMes = Array();
			strMes[0]="0";
			strMes[1]="ENERO";
			strMes[2]="FEBRERO";
			strMes[3]="MARZO";
			strMes[4]="ABRIL";
			strMes[5]="MAYO";
			strMes[6]="JUNIO";
			strMes[7]="JULIO";
			strMes[8]="AGOSTO";
			strMes[9]="SEPTIEMBRE";
			strMes[10]="OCTUBRE";
			strMes[11]="NOVIEMBRE";
			strMes[12]="DICIEMBRE";

			estado = {
					"":{label:'<span class="label label-danger">VACIO</span>',text:'VACIO'},
					"0000":{label:'<span class="label label-danger">SINESTADO</span>',text:'SINESTADO'},
					"0001":{label:'<span class="label label-success">APROBADO</span>',text:'APROBADO'},
					"0002":{label:'<span class="label label-primary">PENDIENTE</span>',text:'PENDIENTE'},
					"0003":{label:'<span class="label label-danger">ANULADO</span>',text:'ANULADO'},
					"0215":{label:'<span class="label label-success">APROBADO</span>',text:'APROBADO'},
					"0216":{label:'<span class="label label-primary">DESAPROBADO</span>',text:'DESAPROBADO'},
					"0217":{label:'<span class="label label-danger">MODIFICADO</span>',text:'MODIFICADO'},
					"0218":{label:'<span class="label label-danger">REFERENCIADO</span>',text:'REFERENCIADO'},
					"0219":{label:'<span class="label label-danger">ENCREDITO</span>',text:'ENCREDITO'},
					"0220":{label:'<span class="label label-danger">CANCELADO</span>',text:'CANCELADO'}
			};

			tipDoc = {
				'0001':'FACTURA',
				'0002':'BOLETA',
				'0003':'RECIBO',
				'0004':'PROFORMA',
				'0005':'NOTA DE INGRESO',
				'0006':'NOTA DE SALIDA',
				'0007':'GUIA DE REMISION',
				'0008':'NOTA DE PEDIDO',
				'0009':'COTIZACION PROVEEDOR',
				'0010':'PEDIDO PROVEEDOR',
				'0011':'PROFORMA',
				'0012':'PEDIDO CLIENTE'
			};
			tipOpe = {
				'0001':'Compra',
				'0002':'Venta',
				'0003':'Traslado_entre_almacenes',
				'0004':'Saldo Inicial',
				'0005':'Cotizacion proveedor',
				'0006':'Pedido proveedor',
				'0007':'Ingreso Almacen',
				'0008':'Egreso Almacen',
				'0009':'Cotizacion cliente',
				'0010':'Pedido cliente'
			};
			lisDocByOpe = {
				'0001' : Array('0001','0005'),
				'0002' : Array('0001','0002','0003'),
				'0003' : Array('0005','0006','0007'),
				'0004' : Array('0005'),
				'0005' : Array('0009'),
				'0006' : Array('0010'),
				'0007' : Array('0005','0007'),
				'0008' : Array('0006','0007'),
				'0009' : Array('0011'),
				'0010' : Array('0012')
			};
			
			msgColor = {
				'success'  : '#356635',
				'error'	   : '#931313',
				'info'	   : '#d6dde7',
				'warning'  : '#efe1b3' 
			}

			function callkeydownhandler(evnt) {
				   var ev = (evnt) ? evnt : event;
				   var code=(ev.which) ? ev.which : event.keyCode;
			}
			if (window.document.addEventListener) {
			   window.document.addEventListener("keydown", callkeydownhandler, false);
			} else {
			   window.document.attachEvent("onkeydown", callkeydownhandler);
			}
							
		</script>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title>Sisprom</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<base href="<%=apps_url%>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<%=apps_url%>css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<%=apps_url%>css/font-awesome.min.css">

		<link rel="stylesheet" type="text/css" media="screen" href="<%=apps_url%>css/plugins/jquery.datetimepicker.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<%=apps_url%>css/smartadmin-production.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<%=apps_url%>css/smartadmin-skins.css">

		<!-- JQUERY DATATABLE CSS -->
		<link rel="stylesheet" type="text/css" media="screen" href="<%=apps_url%>css/datatable/jquery.dataTables.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<%=apps_url%>css/demo.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<%=apps_url%>img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<%=apps_url%>img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="<%=apps_url%>img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<%=apps_url%>img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<%=apps_url%>img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<%=apps_url%>img/splash/touch-icon-ipad-retina.png">
		
		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		
		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="<%=apps_url%>img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="<%=apps_url%>img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="<%=apps_url%>img/splash/iphone.png" media="screen and (max-device-width: 320px)">
		

		<script src="<%=apps_url%>js/libs/jquery-2.0.2.min.js"></script>
		<script src="<%=apps_url%>js/libs/jquery-ui-1.10.3.min.js"></script>
		<style>
		.highlights,
		.highlights:hover,
		.highlights:hover td{
			background-color:#F1DA91;
		}
		</style>
</head>
	<body class="">
		<!-- POSSIBLE CLASSES: minified, fixed-ribbon, fixed-header, fixed-width
			 You can also add different skin classes such as "smart-skin-1", "smart-skin-2" etc...-->
		
		<!-- HEADER -->
		<div id="shortcut">
			<ul>		
			</ul>
		</div>
		<header id="header">
			<div id="logo-group">

				<!-- PLACE YOUR LOGO HERE -->
				<span id="logo" style="margin-top:5px;"> <a href=""><img src="img/logo.png" alt="SmartAdmin"></a> </span>
				<!-- END LOGO PLACEHOLDER -->

				<!-- Note: The activity badge color changes when clicked and resets the number to 0
				Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
				<span style="display:none;" id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span>

				<!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
				<div class="ajax-dropdown">

					<!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
					<div class="btn-group btn-group-justified" data-toggle="buttons">
						<label class="btn btn-default">
							<input type="radio" name="activity" id="ajax/notify/mail.html">
							Msgs (14) </label>
						<label class="btn btn-default">
							<input type="radio" name="activity" id="ajax/notify/notifications.html">
							notify (3) </label>
						<label class="btn btn-default">
							<input type="radio" name="activity" id="ajax/notify/tasks.html">
							Tasks (4) </label>
					</div>

					<!-- notification content -->
					<div class="ajax-notifications custom-scroll">

						<div class="alert alert-transparent">
							<h4>Click a button to show messages here</h4>
							This blank page message helps protect your privacy, or you can show the first message here automatically.
						</div>

						<i class="fa fa-lock fa-4x fa-border"></i>

					</div>
					<!-- end notification content -->

					<!-- footer: refresh area -->
					<span> Last updated on: 12/12/2013 9:43AM
						<button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
							<i class="fa fa-refresh"></i>
						</button> </span>
					<!-- end footer -->

				</div>
				<!-- END AJAX-DROPDOWN -->
			</div>

			<!-- projects dropdown -->
			<div id="project-context">

				<span class="label">Cliente Test S.A.C</span>
				SISVEN v1.0
				<!--<span id="project-selector" class="popover-trigger-element dropdown-toggle" data-toggle="dropdown">Mi Cuenta <i class="fa fa-angle-down"></i></span>-->
			</div>
			<!-- end projects dropdown -->

			<!-- pulled right: nav area -->
			<div class="pull-right">

				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="comun/home/logout" title="Cerrar Sesion" data-logout-msg="Esta seguro de salir del sistema, desea continuar?"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->

				<!-- input: search field -->
				<form action="#ajax/search.html" class="header-search pull-right">
					<input type="text" name="param" placeholder="Find reports and more" id="search-fld">
					<button type="submit">
						<i class="fa fa-search"></i>
					</button>
					<a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
				</form>
				<!-- end input: search field -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" onclick="launchFullscreen(document.documentElement);" title="Full Screen"><i class="fa fa-fullscreen"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- BODY -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
					
					<a href="javascript:void(0);" id="show-shortcut">
						<img src="img/avatars/sunny.png" alt="me" class="online" /> 
						<span>
							<?=$user_data['nomb']?> <?=$user_data['ape1']?> <?=$user_data['ape2']?>
						</span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
						<a name="vent_sispromPedi" href="javascript:void(0);" title="Pedidos"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Inicio</span></a>
					</li>
					<li>
						<a href="javascript:void(0);"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent"> Maestros</span></a>
						<ul>
							<li><a name="loca" href="javascript:void(0);"><i class="fa fa-plane"></i> Local</a></li>
							<li><a name="sucu" href="javascript:void(0);"><i class="fa fa-plane"></i> Sucursal</a></li>
<!-- 							<li><a name="alma" href="javascript:void(0);"><i class="fa fa-plane"></i> Almacen</a></li> -->
							<li>
								<a href="#"><i class="fa fa-plane"></i>  Entidades</a>
								<ul>
									<li>
										<a name="entiClie" href="javascript:void(0);"><i class="fa fa-plane"></i> Clientes</a>
									</li>
									<li>
										<a name="entiProv" href="javascript:void(0);"><i class="glyphicon glyphicon-plane"></i> Proovedores</a>
									</li>
									<li>
										<a name="entiTrab" href="javascript:void(0);"><i class="glyphicon glyphicon-plane"></i> Trabajadores</a>
									</li>
									<li>
										<a name="entiTran" href="javascript:void(0);"><i class="glyphicon glyphicon-plane"></i> Transportistas</a>
									</li>
								</ul>
							</li>
							<li>
								<a name="admiArti" href="javascript:void(0);"><i class="fa fa-plane"></i> Articulos</a>
							</li>
							<li>
								<a name="admiMarc" href="javascript:void(0);"><i class="fa fa-plane"></i> Marca</a>
							</li>
							<li>
								<a name="logiUmed" href="javascript:void(0);"><i class="fa fa-plane"></i> Unidades de Medida</a>
							</li>
							<li>
								<a name="admiLpre" href="javascript:void(0);"><i class="fa fa-plane"></i> Lista de Precios</a>
							</li>
						</ul>
					</li>
					<li class="">
						<a href="javascript:void(0);" title="Logistica"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent"> Operaciones</span></a>
						<ul>
							<li>
								<a name="ccorProv" href="javascript:void(0);"><i class="fa fa-plane"></i> Cuenta Proveedor</a>
							</li>
							<li>
								<a name="ccorClie" href="javascript:void(0);"><i class="fa fa-plane"></i> Cuenta Cliente</a>
							</li>
							<li>
								<a name="docuCotp" href="javascript:void(0);"><i class="fa fa-plane"></i> Cotizacion Proveedor</a>
							</li>
							<li>
								<a name="docuPedp" href="javascript:void(0);"><i class="fa fa-plane"></i> Pedido Proveedor</a>
							</li>
							<li>
								<a name="docuComp" href="javascript:void(0);"><i class="fa fa-plane"></i> Compras</a>
							</li>
							<li>
								<a name="docuCotc" href="javascript:void(0);"><i class="fa fa-plane"></i> Cotizacion Cliente</a>
							</li>
							<li>
								<a name="docuPedc" href="javascript:void(0);"><i class="fa fa-plane"></i> Pedido Cliente</a>
							</li>
							<li>
								<a name="docuVent" href="javascript:void(0);"><i class="fa fa-plane"></i> Venta</a>
							</li>
							<li>
								<a name="docuTalm" href="javascript:void(0);"><i class="fa fa-plane"></i> Traslado Almacen</a>
							</li>
							<li>
								<a name="docuNing" href="javascript:void(0);"><i class="fa fa-plane"></i> Ingreso Almacen</a>
							</li>
							<li>
								<a name="docuNegr" href="javascript:void(0);"><i class="fa fa-plane"></i> Egreso Almacen</a>
							</li>
							<li>
								<a name="docuKard" href="javascript:void(0);"><i class="fa fa-plane"></i> Kardex</a>
							</li>
						</ul>
					</li>
<!-- 					<li> -->
<!-- 						<a href="javascript:void(0);"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent"> Seguridad</span></a> -->
<!-- 						<ul> -->
<!-- 							<li> -->
<!-- 								<a name="seguGrup" href="javascript:void(0);"><i class="fa fa-plane"></i> Grupos</a> -->
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								<a name="seguUsua" href="javascript:void(0);"><i class="fa fa-plane"></i> Usuarios</a> -->
<!-- 							</li> -->
<!-- 						</ul> -->
<!-- 					</li> -->
					<li>
						<a href="javascript:void(0);"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Reportes</span></a>
						<ul>
<!-- 							<li><a name="repVenCot" href="javascript:void(0);"><i class="fa fa-plane"></i>Venta-Cotizacion</a></li> -->
<!-- 							<li><a name="repEntCot" href="javascript:void(0);"><i class="fa fa-plane"></i>Entrega-Cotizacion</a></li> -->
<!-- 							<li><a name="repEntVen" href="javascript:void(0);"><i class="fa fa-plane"></i>Entrega-Venta</a></li> -->
<!-- 							<li><a name="repEntCom" href="javascript:void(0);"><i class="fa fa-plane"></i>Entrega-Compra</a></li> -->
							<li><a name="repDocVen" href="javascript:void(0);"><i class="fa fa-plane"></i>Ventas</a></li>
							<li><a name="repDocCom" href="javascript:void(0);"><i class="fa fa-plane"></i>Compras</a></li>
							<li><a name="repBalGen" href="javascript:void(0);"><i class="fa fa-plane"></i>Balance General</a></li>
<!-- 							<li><a name="repStock" href="javascript:void(0);"><i class="fa fa-plane"></i>Stock</a></li> -->
<!-- 							<li><a name="repCompra" href="javascript:void(0);"><i class="fa fa-plane"></i>Compras</a></li> -->
<!-- 							<li><a name="repLisPrd" href="javascript:void(0);"><i class="fa fa-plane"></i>Lista Productos</a></li> -->
<!-- 							<li><a name="repLisStk" href="javascript:void(0);"><i class="fa fa-plane"></i>Lista Stock</a></li> -->
<!-- 							<li><a name="repLisVen" href="javascript:void(0);"><i class="fa fa-plane"></i>Lista Ventas</a></li> -->
						</ul>
					</li>
				</ul>
			</nav>
			<span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>
		<!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main" >

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true" data-reset-msg="Would you like to RESET all your saved widgets and clear LocalStorage?"><i class="fa fa-refresh"></i></span> </span>
				<ol class="breadcrumb">
					<!-- This is auto generated -->
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
