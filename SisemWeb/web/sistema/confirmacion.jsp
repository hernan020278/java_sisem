<%@ include file="/sistema/header.jsp"%>

<%@ page import="com.sis.manejador.ManejadorFactory"%>
<%@ page import="com.sis.manejador.ManejadorListener"%>
<%@ page import="com.sis.contenedor.Contenedor"%>
<%@ page import="com.comun.utilidad.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.sis.control.*"%>

<!--------------------------------------------------- CONFIGURACION CHOCOSLIDER----------------------------------------->

<script  type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.chocoslider.js"></script>
<link type="text/css" rel="stylesheet"  href="<%=request.getContextPath()%>/css/jquery.chocoslider.css" media="screen">

<%
Map peticion = new HashMap();
Auxiliar auxiliar = Auxiliar.getInstance();
PaginaGeneral pagGen = PaginaGeneral.getInstance();
Util util = Util.getInst();
%>

<script type='text/javascript'>

var mostrarMensaje = <%=auxiliar.msg.isMostrar()%>; 
if ( mostrarMensaje == true) 
{
	msgBox("<%=auxiliar.msg.getTitulo()%>", "<%=auxiliar.msg.getValor()%>","<%=auxiliar.msg.getTipo()%>");
}
</script>

<%
pagGen.pagAct.setOrganizacionIde("sisem");
pagGen.pagAct.setTipoConeccion("transaccion");
pagGen.pagAct.setPaginaHijo("/sistema/confirmacion.jsp");
pagGen.pagAct.setPaginaPadre("/sistema/confirmacion.jsp");
pagGen.pagAct.setAcceso(Acceso.INICIAR);
pagGen.pagAct.setModo(Modo.VISUALIZANDO);
pagGen.pagAct.setEvento("iniciarFormulario");
pagGen.pagAct.setAccion("");
pagGen.pagAct.setNavegar(false);

ManejadorListener manejador = ManejadorFactory.getInstance().obtenerManejador(peticion);
manejador.manejarEvento(peticion);
%>

<input type="hidden" name="paginaActual" value="/sistema/main.jsp"/>

<input type="hidden" name="Pagina_organizacionIde" value="<%=pagGen.pagAct.getOrganizacionIde()%>"/>
<input type="hidden" name="Pagina_tipoConeccion" value="<%=pagGen.pagAct.getTipoConeccion()%>"/>
<input type="hidden" name="Pagina_paginaHijo" value="<%=pagGen.pagAct.getPaginaHijo()%>"/>
<input type="hidden" name="Pagina_paginaPadre" value="<%=pagGen.pagAct.getPaginaPadre()%>"/>
<input type="hidden" name="Pagina_modoAcceso" value="<%=pagGen.pagAct.getAcceso()%>"/>
<input type="hidden" name="Pagina_estadoPagina" value="<%=pagGen.pagAct.getModo()%>"/>
<input type="hidden" name="Pagina_evento" value="<%=pagGen.pagAct.getEvento()%>"/>
<input type="hidden" name="Pagina_accion" value="<%=pagGen.pagAct.getAccion()%>"/>
<input type="hidden" name="Pagina_peticion" value="<%=pagGen.pagAct.getPeticion()%>"/>
<input type="hidden" name="Pagina_navegar" value="<%=pagGen.pagAct.isNavegar()%>"/>

            <div title="Menu Principal" class="main-contenido">
            	<div class="main-titulo">
                	<p>Bienvenido a tu psicologo personal</p>
                </div><!--<div class="main-titulo">-->
            	<div class="main-subtitulo">
                	<p>Atraves de este sistema podras evaluar el estado de tu personalidad</p>
                </div><!--<div class="main-titulo">-->
            	<div class="main-login">
                    <div class="main-banner">
                        <div id="slider">
                            <a href="#"><img src="img/img1.png" alt="Foto 1" title=""></a>
                            <a href="#"><img src="img/img2.png" alt="Foto 1" title=""></a>
                        </div>
                    </div><!--<div class="main-banner">-->

                </div><!--<div class="main-login">-->
            </div><!--<div title="Menu Principal" class="main-contenido">-->

<script language="JavaScript">

var dataJson = "<%=util.getDataJson(auxiliar).toString()%>";
aux = eval("(" + dataJson + ")");

function iniciarFormulario()
{
	if (aux.pag.modoAcceso == "INICIAR") 
	{
		limpiarFormulario();
		llenarFormulario();
// 		activarFormulario(true);
		refrescarFormulario();
	}
}	

function llenarFormulario()
{
	frm.Pagina_organizacionIde.value = aux.pag.organizacionIde; 
	frm.Pagina_tipoConeccion.value = aux.pag.tipoConeccion;
	frm.Pagina_paginaHijo.value = aux.pag.paginaHijo;
	frm.Pagina_paginaPadre.value = aux.pag.paginaPadre;
	frm.Pagina_modoAcceso.value = aux.pag.modoAcceso;
	frm.Pagina_estadoPagina.value = aux.pag.estadoPagina;
	frm.Pagina_evento.value = aux.pag.evento;
	frm.Pagina_accion.value = aux.pag.accion;
	frm.Pagina_navegar.value = aux.pag.navegar;
}

function activarFormulario(parAct)
{
	activarElemento(frm.Usuario_usuario, parAct);
	activarElemento(frm.Usuario_clave, parAct);
}

function refrescarFormulario() {

	switch (aux.pag.estadoPagina) {
	case "<%=Modo.VISUALIZANDO%>":

 		activarFormulario(true);
		frm.cmdAcceder.setAttribute("value","Acceder");
		frm.cmdConsultar.setAttribute("value","Consultar");

		activarElemento(frm.cmdAcceder, true);
		activarElemento(frm.cmdConsultar, false);

		break;

	case "<%=Modo.MODIFICANDO%>":

// 		activarPersona(true);

// 		frm.cmdAgregar.setAttribute("Guardar");
// 		frm.cmdModificar.setAttribute("Cancelar");

// 		activarElemento(frm.cmdAgregar, true);
// 		activarElemento(frm.cmdModificar, true);
// 		activarElemento(frm.cmdBuscar, false);
// 		activarElemento(frm.cmdEliminar, false);
// 		activarElemento(frm.cmdCerrar, false);
		break;

	case "<%=Modo.AGREGANDO%>":
		
// 		limpiarFormulario();
// 		activarFormulario(true);

// 		frm.cmdAgregar.setAttribute("Guardar");
// 		frm.cmdModificar.setAttribute("Cancelar");

// 		activarElemento(frm.cmdAgregar, true);
// 		activarElemento(frm.cmdModificar, true);
// 		activarElemento(frm.cmdBuscar, false);
// 		activarElemento(frm.cmdEliminar, false);
// 		activarElemento(frm.cmdCerrar, false);

		break;

	case "<%=Modo.BUSCANDO%>":

// 		limpiarFormulario();
// 		activarFormulario(true);
// 		frm.cmdModificar.setAttribute("Cancelar");

// 		activarElemento(frm.cmdAgregar, false);
// 		activarElemento(frm.cmdModificar, true);
// 		activarElemento(frm.cmdBuscar, false);
// 		activarElemento(frm.cmdEliminar, false);
// 		activarElemento(frm.cmdCerrar, false);

		break;

	case "<%=Modo.SUSPENDIDO%>":

// 		activarFormulario(false);

// 		activarElemento(frm.cmdAgregar, false);
// 		activarElemento(frm.cmdModificar, false);
// 		activarElemento(frm.cmdBuscar, false);
// 		activarElemento(frm.cmdEliminar, false);
// 		activarElemento(frm.cmdCerrar, false);

		break;
	}// Fin de switch para estado del formaulario de cliente
}//function refrescarFormulario() {
	
function cmdAccederEvento()
{
	if (aux.msg.mostrar == true) 
	{
		msgBox(aux.msg.titulo, aux.msg.valor,aux.msg.tipo);
	}
	iniciarFormulario();
}

</script>

<%@ include file="/sistema/footer.jsp"%>
   