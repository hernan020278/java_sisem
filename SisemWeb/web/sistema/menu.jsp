<%@ include file="/sistema/header.jsp"%>

<%@ page import="com.sis.manejador.ManejadorFactory"%>
<%@ page import="com.sis.manejador.ManejadorListener"%>
<%@ page import="com.sis.contenedor.Contenedor"%>
<%@ page import="com.comun.utilidad.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.sis.control.*"%>

<!--------------------------------------------------- CONFIGURACION CLOUD SLIDER----------------------------------------->

<script  type="text/javascript" src="<%=request.getContextPath()%>/js/cloud-carousel.1.0.5.js"></script>
<link type="text/css" rel="stylesheet"  href="<%=request.getContextPath()%>/css/cloud-carousel.1.0.5.css" media="screen">

<%
	Map peticion = new HashMap();
Auxiliar auxiliar = Auxiliar.getInstance();
PaginaGeneral pagGen = PaginaGeneral.getInstance();
Util util = Util.getInst();

ManejadorListener manejador = ManejadorFactory.getInstance().obtenerManejador(peticion);
manejador.manejarEvento(peticion);
%>

<input type="hidden" name="paginaActual" value="/sistema/menu.jsp"/>

<input type="hidden" name="Pagina_organizacionIde" value="<%=pagGen.pagAct.getOrganizacionIde()%>"/>
<input type="hidden" name="Pagina_tipoConeccion" value="<%=pagGen.pagAct.getTipoConeccion()%>"/>
<input type="hidden" name="Pagina_paginaHijo" value="<%=pagGen.pagAct.getPaginaHijo()%>"/>
<input type="hidden" name="Pagina_paginaPadre" value="<%=pagGen.pagAct.getPaginaPadre()%>"/>
<input type="hidden" name="Pagina_modoAcceso" value="<%=pagGen.pagAct.getAcceso()%>"/>
<input type="hidden" name="Pagina_estadoPagina" value="<%=pagGen.pagAct.getModo()%>"/>
<input type="hidden" name="Pagina_evento" value="<%=pagGen.pagAct.getEvento()%>"/>
<input type="hidden" name="Pagina_accion" value="<%=pagGen.pagAct.getAccion()%>"/>
<input type="hidden" name="Pagina_peticion" value="<%=pagGen.pagAct.getPeticion()%>"/>
<input type="hidden" name="Pagina_navegar" value="<%=pagGen.pagAct.isNavegar()%>"/>

<div class="bodyCenterLeft">

    <div id = "carousel">            
        <!-- All images with class of "cloudcarousel" will be turned into carousel items -->
        <!-- You can place links around these images -->
        <a class="class_cambiarDiv" href="#bodyCenterRightDina1"><img class = "cloudcarousel" src="img/carousel/flag1.png" alt="Adminsitracion de Especialista" title="Especialistas" /></a>
        <a class="class_cambiarDiv" href="#bodyCenterRightDina2"><img class = "cloudcarousel" src="img/carousel/flag2.png" alt="Consulta de Pacientes" title="Consultas" /></a>
        <a class="class_cambiarDiv" href="#bodyCenterRightDina3"><img class = "cloudcarousel" src="img/carousel/flag3.png" alt="Grafico Estadistico" title="Estadistica" /></a>
        <a href="#" onclick="javascript: ejecutar('cmdCerrarEvento','SUBMIT');"><img class = "cloudcarousel" src="img/carousel/flag4.png" alt="Lista de Casos" title="Casos" /></a>
    </div><!--<div id = "carousel">-->
	<!--<input id="left-but"  type="button" value="Left" />-->
	<!--<input id="right-but" type="button" value="Right" />-->
    
    <!-- Define elements to accept the alt and title text from the images. -->
    <!--<p id="title-text"></p>-->
    <!--<p id="alt-text"></p>-->
</div><!--<div id="bodyCenterLeft">-->

<div class="bodyCenterRight">

	<div class="class_oculto" id="bodyCenterRightDina1">
		<div class="tituloParrafo">Administracion de Especialista</div>
		<div class="linea"></div>
		<div class="bodyCenterRightImg">
			<img src="img/form/admin_especialista.png" width="270" height="200" />
		</div>
		<div class="bodyCenterRightText">
			<p>Todos los dias realizamos un desayuno familiar con todos los huespedes ud puede participar o si gusta e lo llevamos el desayuno a su habitacion</p>
			<input tipoObjeto="ui.boton" type="button" value="Nuevo" id="cmdNuevoEspecialista" onclick="javascript: ejecutar(this.id + 'Evento', 'SUBMIT');"/>
			<input tipoObjeto="ui.boton" type="button" value="Navegar" id="cmdNavegarEspecialista" onclick="javascript: ejecutar(this.id + 'Evento', 'SUBMIT');"/>
		</div>
	</div><!--<div class="class_oculto" id="bodyCenterRightDina1">-->
	
	<div class="class_oculto" id="bodyCenterRightDina2">
        <div class="tituloParrafo">Consulta de Pacientes</div>
        <div class="linea"></div>
        <div class="bodyCenterRightImg">
            <img src="img/form/consulta_paciente.png" width="270" height="200"/>
        </div>
        
        <div class="bodyCenterRightText">
            <p>Todos los dias realizamos un desayuno familiar con todos los huespedes ud puede participar o si gusta e lo llevamos el desayuno a su habitacion</p>
			<input tipoObjeto="ui.boton" type="button" value="Nuevo" id="cmdNuevoPaciente" onclick="javascript: ejecutarAjax(this.id + 'Evento');"/>
			<input tipoObjeto="ui.boton" type="button" value="Navegar" id="cmdNavegarPaciente" onclick="javascript: ejecutarAjax(this.id + 'Evento');"/>
        </div>
    </div><!--<div class="class_oculto" id="bodyCenterRightDina2">-->
    
	<div class="class_oculto" id="bodyCenterRightDina3">
		<div class="tituloParrafo">Grafico Estadistico</div>
		<div class="linea"></div>
		<div class="bodyCenterRightImg">
			<img src="img/form/grafico_estadistico.png" width="270" height="200" />
		</div>
		<div class="bodyCenterRightText">
			<p>Todos los dias realizamos un desayuno familiar con todos los huespedes ud puede participar o si gusta e lo llevamos el desayuno a su habitacion</p>
			<input tipoObjeto="ui.boton" type="button" value="Ver Grafico" id="cmdVerGrafico" onclick="javascript: ejecutarAjax(this.id + 'Evento');"/>
		</div>
	</div><!--<div class="class_oculto" id="bodyCenterRightDina3">-->
	
	<div class="class_oculto" id="bodyCenterRightDina4">
		<div class="tituloParrafo">Administracion de Casos</div>
		<div class="linea"></div>
		<div class="bodyCenterRightImg">
			<img src="img/form/admin_casos.png" width="270" height="200" />
		</div>
		<div class="bodyCenterRightText">
			<p>Todos los dias realizamos un desayuno familiar con todos los huespedes ud puede participar o si gusta e lo llevamos el desayuno a su habitacion</p>
			<input tipoObjeto="ui.boton" type="button" value="Nuevo" id="cmdNuevoCaso" onclick="javascript: ejecutarAjax(this.id + 'Evento');"/>
			<input tipoObjeto="ui.boton" type="button" value="Navegar" id="cmdNavegarCaso" onclick="javascript: ejecutarAjax(this.id + 'Evento');"/>
		</div>
	</div><!--<div class="class_oculto" id="bodyCenterRightDina4">-->
             
</div><!--<div class="bodyCenterRight">-->
<script language="JavaScript">

var dataJson = "<%=util.getDataJson(auxiliar).toString()%>";
aux = eval("(" + dataJson + ")");

function iniciarFormulario()
{
	if (aux.pag.modoAcceso == "INICIAR")  
	{
		llenarFormulario();
		activarFormulario(true);
		refrescarFormulario();
	}
}	

function llenarFormulario()
{
	frm.Pagina_organizacionIde.value = aux.pag.organizacionIde; 
	frm.Pagina_tipoConeccion.value = aux.pag.tipoConeccion;
	frm.Pagina_paginaHijo.value = aux.pag.paginaHijo;
	frm.Pagina_paginaPadre.value = aux.pag.paginaPadre;
	frm.Pagina_modoAcceso.value = aux.pag.modoAcceso;
	frm.Pagina_estadoPagina.value = aux.pag.estadoPagina;
	frm.Pagina_evento.value = aux.pag.evento;
	frm.Pagina_accion.value = aux.pag.accion;
	frm.Pagina_navegar = aux.pag.navegar;
}

function activarFormulario(parAct){}

function refrescarFormulario() {}
	
function cmdNuevoCasoEvento()
{
}
function cmdNuevoEspecialistaEvento()
{
	if(aux.pag.navegar == true)
	{
		ejecutarSubmit("cmdNuevoEspecialista");		
	} 
	else 
	{
		if (aux.msg.mostrar == true) {
			msgBox(aux.msg.titulo, aux.msg.valor,aux.msg.tipo);
		}
		iniciarFormulario();
	}
}
</script>

<%@ include file="/sistema/footer.jsp"%>
