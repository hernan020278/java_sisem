<?xml version='1.0' encoding='UTF-8' ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/sistema/context.jsp"%>
<head>
	<!-------------------------------------------- CSS DEL SISTEMA PRINCIPAL------------------------------------------------>

	<link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/reset.css"/>
	<link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/sisem.css"/>

	<!-------------------------------------------- FRAMEWORK JQUERY------------------------------------------------>

	<script type="text/javascript" src="<%=contextPath%>/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/js/jquery-ui.js"></script>

	<link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/jquery-ui.css">
	<link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/jquery.panel.css">
	
    <!-------------------------------------------------------SCRIPT TEXT---------------------------------------------------->

    <script type="text/javascript" src="<%=contextPath%>/js/jquery.meio.mask.js"></script>

    <!-------------------------------------------------------SCRIPT SELECTBOX----------------------------------------------->

<!--	<script type="text/javascript" src="<=contextPath>/js/jquery.selectBoxIt.js"></script> -->
<!--	<link type="text/css" rel="stylesheet" href="<=contextPath>/css/jquery.selectBoxIt.css"/> -->

    <!-------------------------------------------------------SCRIPT RADIOCHECKED--------------------------------------------->

    <script type="text/javascript" src="<%=contextPath%>/js/prettyCheckable.js"></script>
    <link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/prettyCheckable.css"/>

	<!--------------------------------------------------- CONFIGURACION CHOCOSLIDER----------------------------------------->
	
<%-- 	<script  type="text/javascript" src="<%=contextPath%>/js/jquery.chocoslider.js"></script> --%>
<%-- 	<link type="text/css" rel="stylesheet"  href="<%=contextPath%>/css/jquery.chocoslider.css" media="screen"> --%>

    <!-----------------------------------------------SCRIPT PARA MANEJAR EL SLIDER------------------------------------------>
    
<!--    <script type="text/javascript" src="<%=contextPath%>/js/tms-0.3.js"></script> -->
<!--    <script type="text/javascript" src="<%=contextPath%>/js/tms_presets.js"></script> -->
    
	<!-------------------------------------------JS Y CSS PARA MSGBOX ------------------------------------------------------>	    

    <script type="text/javascript" src="<%=contextPath%>/js/jquery.msgBox.js"></script>
    <link type="text/css" rel="stylesheet" href="<%=contextPath%>/css/msgBoxLight.css"/>
    
	<!-------------------------------------------JS Y CSS PARA EFECTO DE BOTON---------------------------------------------->	    

<!--     <script src="<%=contextPath%>/js/jquery.animate-shadow.js"></script> -->
<!-- 	<link href="<%=contextPath%>/css/shadow-animation.css" rel="stylesheet"> -->

	<!------------------------------------------------ CONFIGURACION SCRIP DE SISTEMA--------------------------------------->

<%-- 	<script type="text/javascript" src="<%=contextPath%>/js/date_check.js"></script> --%>
	<script type="text/javascript" src="<%=contextPath%>/js/sisem.js"></script>
<%-- 	<script type="text/javascript" src="<%=contextPath%>/js/ajax.js"></script> --%>
	<script type="text/JavaScript" src="<%=contextPath%>/js/config.js"></script>

</head>
<body>
	<form name="frm" target="_parent" action="<%=contextPath%>/controlador" method="post">
	<div class="header"></div>
	
    <!----------------------------- <div class="divBody"> -------------------------------------------------->
	<div class="body">

	
		<!-- --------------------------------------<div class="divBodyTop"> -------------------------------->
		<div class="bodyTop">
		</div>
		<!-- --------------------------------------<div class="divBodyTop"> -------------------------------->
		
           <!-----------------------------------------<div id="bodyCenter">------------------------------------>
           <div id="bodyCenter" class="bodyCenter">
			