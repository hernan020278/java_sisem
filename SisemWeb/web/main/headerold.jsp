<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<?php
// 	$config = parse_ini_file('util/config.ini');

// 	$cntIndCnt = file_get_contents("util/inicio_" . $config['idioma'] . ".txt");
// 	$linIndCntTmp = explode("\n",$cntIndCnt);
// 	$arrayNulo = Array(1);
// 	$linIndCnt = array_merge($arrayNulo, $linIndCntTmp);
session_start();
?>
<head>
    <title>Aldamotors Admin</title>
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="author" content="" />		
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<link rel="stylesheet" href="stylesheets/all.css" type="text/css" />
	
	<!--[if gte IE 9]>
	<link rel="stylesheet" href="stylesheets/ie9.css" type="text/css" />
	<![endif]-->
	
	<!--[if gte IE 8]>
	<link rel="stylesheet" href="stylesheets/ie8.css" type="text/css" />
	<![endif]-->
    
    <script type="text/javascript" src="resources/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/funciones.js"></script>
    
    <link rel="stylesheet" type="text/css" href="js/lightbox/themes/default/jquery.lightbox.css" />
    <!--[if IE 6]>
    <link rel="stylesheet" type="text/css" href="js/lightbox/themes/default/jquery.lightbox.ie6.css" />
    <![endif]-->
    <script type="text/javascript" src="js/lightbox/jquery.lightbox.js"></script>
        
    <link rel="stylesheet" href="javascripts/themes/alertify.core.css" />
	<link rel="stylesheet" href="javascripts/themes/alertify.default.css" id="toggleCSS" />
	<link rel="stylesheet" href="stylesheets/pnotify.custom.min.css" type="text/css" />
	<meta name="viewport" content="width=device-width">
	<style>
		.alertify-log-custom {
			background: blue;
		}
	</style>
    
    <!--<script src="javascripts/jquery-1.9.1.js"></script>-->
	<script src="javascripts/alertify.js"></script>
	<script src="javascripts/jquery.numeric.js"></script>
    <script type="text/javascript" src="js/alertas.js"></script>
<!--     <script type="text/javascript" src="javascripts/init.js"></script> -->
    <script type="text/javascript" src="javascripts/jquery.blockUI.js"></script>
    <script src="javascripts/pnotify.custom.js" type="text/javascript"></script>
    
	<!-------------------------------------------JS Y CSS PARA MSGBOX ----------------------------------------------->	    

<!--     <script type="text/javascript" src="js/jquery.msgBox.js"></script> -->
<!--     <link type="text/css" rel="stylesheet" href="css/msgBoxLight.css"/> -->
    
	<!------------------------------------------------ CONFIGURACION SCRIP DE SISTEMA--------------------------------------->

	<script type="text/javascript" src="js/config.js"></script>
	<script type="text/javascript" src="js/sisem.js"></script>
	<script type="text/javascript" src="js/date_check.js"></script>

</head>
<body>
	
	<div id="resultado"></div>

	<div id="bloqueoAjax" class="bloqueo-ajax"></div>

	<form name="frm" target="_parent" action="controlador/SisemListener.php" onsubmit="return false;" method="post">
    
    <input type="hidden" name="idioma" value=""/>
    
<div id="wrapper">
	
	<div id="header">
		<div id='logo' style="position: absolute; top: 24px; left: 0px;"></div>			
		<a href="javascript:;" id="reveal-nav">
			<span class="reveal-bar"></span>
			<span class="reveal-bar"></span>
			<span class="reveal-bar"></span>
		</a>
	</div> <!-- #header -->
	
	<div id="search">
		<!--<form>
			<input type="text" name="search" placeholder="Search..." id="searchField" />
		</form>	-->	
	</div> <!-- #search -->
	
	<div id="sidebar">		
		
		<ul id="mainNav">			
			<!--INICIO DEL MENU-->	
			<?php include("includes/menu.php");?>		
			<!--FIN DEL MENU-->
		</ul>
	
				
	</div> <!-- #sidebar -->

	<div id="content">
			