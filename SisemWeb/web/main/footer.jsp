<%
// 	$config = parse_ini_file('util/config.ini');

// 	$contenido = file_get_contents("util/inicio_" . $config['idioma'] . ".txt");
// 	$linTmp = explode("\n",$contenido);
// 	$arrayNulo = Array(1);
// 	$lineas = array_merge($arrayNulo, $linTmp);
%>

				</div>
				<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
		<div id="shortcut">
			<ul>
				<li>
					<a href="#ajax/inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
				</li>
				<li>
					<a href="#ajax/calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
				</li>
				<li>
					<a href="#ajax/gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
				</li>
				<li>
					<a href="#ajax/invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
				</li>
				<li>
					<a href="#ajax/gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
				</li>
				<li>
					<a href="#ajax/profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
				</li>
			</ul>
		</div>
		<!-- END SHORTCUT AREA -->

		</form>
		
	</body>

	<!--================================================== -->

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
	<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script>
// 		if (!window.jQuery) {
// 			document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
// 		}
	</script>

	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script>
// 		if (!window.jQuery.ui) {
// 			document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');
// 		}
	</script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
	<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

	<!-- ANGULAR JS -->
<!-- 		<script src="js/angular/angular.js"></script> -->
<!-- 		<script src="js/angular/angular-route.js"></script> -->

	<!-- BOOTSTRAP JS -->
	<script src="js/bootstrap/bootstrap.min.js"></script>

	<!-- CUSTOM NOTIFICATION -->
	<script src="js/notification/SmartNotification.min.js"></script>

	<!-- JARVIS WIDGETS -->
	<script src="js/smartwidgets/jarvis.widget.min.js"></script>

	<!-- EASY PIE CHARTS -->
	<script src="js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

	<!-- SPARKLINES -->
	<script src="js/plugin/sparkline/jquery.sparkline.min.js"></script>

	<!-- JQUERY VALIDATE -->
	<script src="js/plugin/jquery-validate/jquery.validate.min.js"></script>

	<!-- JQUERY MASKED INPUT -->
	<script src="js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	<!-- JQUERY SELECT2 INPUT -->
	<script src="js/plugin/select2/select2.min.js"></script>

	<!-- JQUERY UI + Bootstrap Slider -->
	<script src="js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

	<!-- browser msie issue fix -->
	<script src="js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

	<!-- FastClick: For mobile devices: you can disable this in app.js -->
	<script src="js/plugin/fastclick/fastclick.js"></script>

	<!-- JQUERY JQGRID -->
	<script src="js/plugin/jqGrid/jquery.jqGrid.min.js"></script>

	<!-- JQUERY DATATABLE -->
	<script src="js/datatable/jquery.dataTables.min.js"></script>
	<script src="js/plugin/blockUI/jquery.blockUI.js"></script>

	<!-- JQUERY NUMERIC -->
	<script src="js/jquery.numeric.js"></script>

	<!--[if IE 7]>

	<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

	<![endif]-->

	<!-- Demo purpose only -->
	<script src="js/demo.js"></script>

	<!-- MAIN APP JS FILE -->
	<script src="js/theme.js"></script>

	<!-- SISEM FRAMEWORK -->
	<script src="js/grid.js"></script>
	<script src="js/config.js"></script>
	<script src="js/sisem.js"></script>
<!-- 	<script src="js/init.js"></script> -->
	
	<script type="text/JavaScript">
		<% if(request.getParameter("idSucEmpresa") != null){session.setAttribute("idSucEmpresa",request.getParameter("idSucEmpresa"));}%>
		Pagina_valor = '<%=((request.getParameter("Pagina_valor")!=null)?request.getParameter("Pagina_valor"):"")%>';
		abrirPaginaAjax("/main/asistencia.jsp");
	</script>
</html>