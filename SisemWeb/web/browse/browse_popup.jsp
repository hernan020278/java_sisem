<%@ include file="/sistema/context.jsp"%>
<html>
	<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="sat, 11 sep 1971 12:00:00 gmt">

 	<!---------------------------------SCRIPT INICIAR COMPONENTES JQUERY---------------------------------------------------->
	
	<script type="text/javascript" src="<%=contextPath%>/js/sisem.js"></script>
	
	</head>
	<body marginwidth=0 marginheight=0 topmargin=0 leftmargin=0>
		<form name="frm" target="_parent" action="<%=contextPath%>/controlador" method="post">
			<table border=0 cellspacing=0 cellpadding=0 width=100%>
				<tr>
					<td align=center valign=top><br>
					<b>Processing... Please wait.</b><br>
					<br>
					<br>
					<img src="<%=contextPath%>/img/processing.gif" border=1 height=15px></td>
				</tr>
				<tr>
					<td id=requestParameters></td>
				</tr>
			</table>
			<!---- JavaScripts for Entry Validation ----->
			<script language="JavaScript1.2">
			<!--  hide script from old browsers
			
				setParameters();
			 	submitPopup();
			
				function setParameters() {
					var params = window.parent.popupParameters;
					params = params.replace(/&#x2f;/g,"/");
					var myHtml = "";
					var parentWindow = window.parent;
			
					if (params == undefined || params == null || isEmpty(params)) {
						if (window.opener != undefined) {
							parentWindow = window.opener;
							params = parentWindow.popupParameters;
						}
					}
			
					if (params != null || params.length > 0) {
						var sepInd = params.indexOf("&");
						var keyInd = params.indexOf("=");
			
						while (sepInd > 0) {
							key = params.substring(0, keyInd);
							val = params.substring(keyInd + 1, sepInd);
			
							myHtml = myHtml + "<input type=hidden name=" + '"' + key + '"' + " value=" + '"' + val + '"' + ">";
			
							params = params.substring(sepInd + 1, params.length);
			
							sepInd = params.indexOf("&");
							keyInd = params.indexOf("=");
						}
			
						if (keyInd > 0) {
							key = params.substring(0, keyInd);
							val = params.substring(keyInd + 1, params.length);
			
							myHtml = myHtml + "<input type=hidden name=" + '"' + key + '"' + " value=" + '"' + val + '"' + ">";
						}
					}
			
					parentWindow.popupParameters = "";
					document.getElementById("requestParameters").innerHTML = myHtml;
				}
			
				function submitPopup() 
				{
					var evento = window.parent.frm.Pagina_evento.value;
					var peticion = window.parent.frm.Pagina_peticion.value;
					
					document.body.style.cursor = "wait";
					ejecutar(evento, peticion);
				}
			
			// end hiding contents -->
			</script>
		</form>		
	</body>
</html>