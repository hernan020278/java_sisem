<%@ include file="/sistema/header.jsp"%>

<%@page import="com.sis.manejador.ManejadorFactory"%>
<%@page import="com.sis.manejador.ManejadorListener"%>
<%@page import="com.browse.*"%>
<%@page import="com.comun.propiedad.*"%>
<%@page import="com.comun.utilidad.*"%>
<%@ page import="com.comun.referencia.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.sis.control.*"%>

<%
	Map peticion = new HashMap();
Auxiliar aux = Auxiliar.getInstance();
PaginaGeneral pagGen = PaginaGeneral.getInstance();
Util util = Util.getInst();

ManejadorListener manejador = ManejadorFactory.getInstance().obtenerManejador(peticion);
manejador.manejarEvento(peticion);

String oid = pagGen.pagAct.getOrganizacionIde();
String language = "EN";

BrowseObject browseObject = (BrowseObject) request.getAttribute("browseObject");
BrowseColumn browseColumns[] = browseObject.getBrowseColumns();
PropiedadGeneral propertiesManager = PropiedadGeneral.getInstance(oid);
%>

<script type="text/javascript">
function setTypeNames(txt_tipo) {
	console.log("tipo : " + txt_tipo);
	document.getElementById("typeNames").innerHTML += "<input type='hidden' name='txt_tipo' value='" + txt_tipo + "'>";
}
</script>

<input type="hidden" name="Pagina_organizacionIde" value="<%=pagGen.pagAct.getOrganizacionIde()%>"/>
<input type="hidden" name="Pagina_tipoConeccion" value="<%=pagGen.pagAct.getTipoConeccion()%>"/>
<input type="hidden" name="Pagina_paginaHijo" value="<%=pagGen.pagAct.getPaginaHijo()%>"/>
<input type="hidden" name="Pagina_paginaPadre" value="<%=pagGen.pagAct.getPaginaPadre()%>"/>
<input type="hidden" name="Pagina_modoAcceso" value="<%=pagGen.pagAct.getAcceso()%>"/>
<input type="hidden" name="Pagina_estadoPagina" value="<%=pagGen.pagAct.getModo()%>"/>
<input type="hidden" name="Pagina_evento" value="<%=pagGen.pagAct.getEvento()%>"/>
<input type="hidden" name="Pagina_accion" value="<%=pagGen.pagAct.getAccion()%>"/>
<input type="hidden" name="Pagina_peticion" value="<%=pagGen.pagAct.getPeticion()%>"/>
<input type="hidden" name="Pagina_navegar" value="<%=pagGen.pagAct.isNavegar()%>"/>

<input type="hidden" name="browseTabla" value=""/>

<div id="typeNames"></div>
<div id="filtro_opciones"></div>

<div id="DlgBrowseOpciones" title="Filtro de Opciones" class="ui-panel" style="width: 650px">
	<div class="ui-panel-titlebar">Opciones para <%=browseObject.getTitle()%></div>
	<div class="ui-panel-content">
		<div class="ui-table">
			<div class="ui-tr">
				<div class="ui-td" style="width: 160px" align="center">Nombre de Columna</div>
				<div class="ui-td" style="width: 64px" align="center">Operador</div>
				<div class="ui-td" style="width: 160px" align="center">Expresion</div>
				<div class="ui-td" style="width: 80px" align="center">Logico</div>
				<div class="ui-td" style="width: 120px" align="center">Orden</div>
			</div><!--<div class="ui-tr">-->
			
			<div class="linea"></div>
			
			<%		
			int idxFil = 0 ;
			int filterRows = 5;
	
			for (int iteFil = 0; iteFil < filterRows; iteFil++) 
			{
				int columnCount = 0; %>
			<div class="ui-tr">
				<div class="ui-td" style="width: 160px">
					<div class="ui-selectbox">
						<select id="cmb_columna" name="cmb_columna" onchange="javascript: updateFilterStatus(<%=iteFil%>);">
						<%// load values from BrowseObject.browseColumns
						for (int iteCol = 0; iteCol < browseColumns.length; iteCol++) 
						{
							BrowseColumn column = browseColumns[iteCol];
							if ((column.isHidden() && !column.getAllowFilter().equals("Y")) || column.getType().equalsIgnoreCase("checkbox")) 
							{
					           if (idxFil == iteCol) idxFil++ ;
					           continue ;
							}
							if (idxFil == iteCol) 
							{%>				
								<option value="<%=column.getColumnName()%>" selected><%=column.getLabel()%></option>
							<%} 
							else 
							{%>
								<option value="<%=column.getColumnName()%>"><%=column.getLabel()%></option>
							<%}
							if (iteFil == 0) 
							{%>
							<script type="text/javascript">
								setTypeNames("<%= column.getType() %>");
							</script>
							<%}
							columnCount++;
						}//for (int iteCol=0; iteCol < browseColumns.length; iteCol++) 
						if (iteFil == 0) 
						{
							if (columnCount < filterRows) 
							{
								filterRows = columnCount;
							}
						}
						idxFil++;%>
						</select>
					</div>
				</div><!-- <div class="ui-td"> -->
				
				<div class="ui-td" style="width: 64px">
					<div class="ui-selectbox">
						<select id="cmb_operador" name="cmb_operador">
							<option value="=">=</option>
							<option value=">">></option>
							<option value="<"><</option>
							<option value=">=">>=</option>
							<option value="<="><=</option>
							<option value="<>"><></option>
					    </select>
					</div>
					<input type=hidden name="txt_original" value="Y">
				</div><!-- <div class="ui-td"> -->
				<div class="ui-td" style="width: 160px">
			        <input tipoObjeto="meio.mask" type="text" id="txt_valor" name="txt_valor" alt="texto" maxlength="20" />
			    </div><!-- <div class="ui-td"> -->
				
				<div class="ui-td" style="width: 80px">
					<div class="ui-selectbox">
						<select id="cmb_logico" name="cmb_logico">
							<option value=""> </option>
							<option value="AND">AND</option>
							<option value="OR">OR</option>
						</select>
					</div>
				</div><!-- <div class="ui-td"> -->					
				<div class="ui-td" style="width: 120px">
					<div class="ui-selectbox">
						<select id="cmb_orden" name="cmb_orden">
							<option value=""> </option>
							<option value="ASC">Ascending</option>
							<option value="DESC">Descending</option>
					    </select>
					</div>
				</div><!-- <div class="ui-td"> -->
			</div><!-- <div class="ui-tr"> -->
			<div class="division"></div>
			<%}//for (int iteFil = 0; iteFil < filterRows; iteFil++)%>
			
			<div class="linea"></div>
			
			<div class="ui-tr">
				<div class="ui-td" style="height: 30px;line-height: 30px">Display</div>
				<% if ((browseObject.getTitle()).indexOf("Report") < 0 &&
					(browseObject.getBrowseTabla()).indexOf("print-check-invoices") < 0 && (browseObject.getBrowseTabla()).indexOf("invoice-exported-reset") < 0 )
				{ %>
						
				<div class="ui-td">
					<div class="ui-selectbox">
						<select id="pageSize" name="pageSize">
							<option value="1" <% if (browseObject.getPageSize() == 1) {%>selected<%}%>>1</option>
							<option value="5" <% if (browseObject.getPageSize() == 5) {%>selected<%}%>>5</option>
							<option value="10" <% if (browseObject.getPageSize() == 10) {%>selected<%}%>>10</option>
							<option value="15" <% if (browseObject.getPageSize() == 15) {%>selected<%}%>>15</option>
							<option value="20" <% if (browseObject.getPageSize() == 20) {%>selected<%}%>>20</option>
							<option value="25" <% if (browseObject.getPageSize() == 25) {%>selected<%}%>>25</option>
							<option value="50" <% if (browseObject.getPageSize() == 50) {%>selected<%}%>>50</option>
						</select>
					</div>
				</div><!-- <div class="ui-td"> -->
				<div class="ui-td" style="height: 30px;line-height: 30px">Resultados por pagina</div>
				<%}%>
			</div><!-- <div class="ui-tr"> -->
			
			<div class="linea"></div>
			
			<div class="ui-tr">
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Buscar" name="cmdBuscar" id="cmdBuscar" onclick="javascript: browseTable('especialista');" />
				</div><!-- <div class="ui-td"> -->
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Cerrar" name="cmdCerrar" id="cmdCerrar" onclick="javascript: ejecutar(this.id + 'Evento','SUBMIT');" />
				</div><!-- <div class="ui-td"> -->
			</div><!-- <div class="ui-tr"> -->
		</div><!--<div class="ui-table">-->
	</div><!-- <div class="ui-panel-content"> -->
	</br> </br>
</div><!--<div id="DlgEspecialista" title="Administracion de Especialistas" class="ui-panel">-->


<script value=JavaScript>
<!-- Hide script
var dataJson = "<%=util.getDataJson(aux).toString()%>";
aux = eval("(" + <%=util.getDataJson(aux).toString()%> + ")");
pagGen = eval("(" + <%=util.getDataJson(pagGen).toString()%> + ")");

function iniciarFormulario()
{
if (pagGen.pagAct.modoAcceso == "INICIAR") 
{
	refrescarFormulario();
	llenarFormulario();
}
}	

function llenarFormulario()
{
	frm.browseTabla.value = "<%=browseObject.getBrowseTabla()%>";
	
	frm.Pagina_organizacionIde.value = aux.pag.organizacionIde; 
	frm.Pagina_tipoConeccion.value = aux.pag.tipoConeccion;
	frm.Pagina_paginaHijo.value = aux.pag.paginaHijo;
	frm.Pagina_paginaPadre.value = aux.pag.paginaPadre;
	frm.Pagina_acceso.value = aux.pag.modoAcceso;
	frm.Pagina_modo.value = aux.pag.estadoPagina;
	frm.Pagina_evento.value = aux.pag.evento;
	frm.Pagina_accion.value = aux.pag.accion;
	frm.Pagina_peticion = aux.pag.peticion;
	frm.Pagina_navegar.value = aux.pag.navegar;
}

function activarEspecialista(parAct)
{
}

function refrescarFormulario(valor) {

	switch (valor) 
	{
	case "1":
		break;
	}// Fin de switch para estado del formaulario de cliente
}//function refrescarFormulario() {

function cmdAgregarEvento() {
	if (frm.cmdAgregar.getAttribute("value") == "Agregar") 
	{
		frm.Usuario_usucodigo.focus();
	} 
	else if (frm.cmdAgregar.getAttribute("value") == "Guardar") 
	{
		if (aux.msg.mostrar == true) {
			msgBox(aux.msg.titulo, aux.msg.valor,aux.msg.tipo);
		}
	}// FIN DE EVALUAR QUE VA HA HACER EL BOTON
	iniciarFormulario();
}

function cmdBuscarEvento() {

	setFilterOptions(); 
	setOwnerFilter(); 
	
	ejecutar('cmdBuscarEvento','SUBMIT')
	
}

var currentTypeOptions = '';
var currentIndex = '';
var currentColumnValue = '';
var filterStatus = new Array();

var bReset = true;
var filterFields = "";

function setFilterOptions() {
//  	var myTable = document.getElementById("filterOptions");
	var logicoDefault = "AND";
	var filterFields = "";

	for(var iteFilCol = 0; iteFilCol < frm.cmb_columna.length ; iteFilCol++)
	{
		var columna = selectValue(frm.cmb_columna[iteFilCol]);
		var tipo = frm.txt_tipo[iteFilCol].value;
		var operador = selectValue(frm.cmb_operador[iteFilCol]);
		var valor = frm.txt_valor[iteFilCol].value;
		var logico = selectValue(frm.cmb_logico[iteFilCol]);
		var orden = selectValue(frm.cmb_orden[iteFilCol]);
		var original = frm.txt_original[iteFilCol].value;
		
		if(!isEmpty(columna) && !isEmpty(operador) && !isEmpty(valor))
		{
			if((iteFilCol + 1) == frm.cmb_columna.length)
			{
				logico = "";				
			}
			filterFields = filterFields 
				+ "<input type='hidden' name='filtro_columna' value='" + columna + "'/>"
				+ "<input type='hidden' name='filtro_tipo' value='" + tipo + "'/>"
				+ "<input type='hidden' name='filtro_operador' value='" + operador + "'/>"
				+ "<input type='hidden' name='filtro_valor' value='" + valor + "'/>"
				+ "<input type='hidden' name='filtro_logico' value='" + logico + "'/>"
				+ "<input type='hidden' name='filtro_orden' value='" + orden + "'/>"
				+ "<input type='hidden' name='filtro_original' value='" + original + "'/>";
		}
		document.getElementById('filtro_opciones').innerHTML = filterFields;		
	}
}

function generateReport()
{
	var filterFields = setFilterOptions();
	frm.browseTabla.value = '<%=browseObject.getBrowseTabla()%>';
	if(frm.browseTabla.value == 'requisition-line')
	{
		setOriginalFilter("RequisitionHeader_owner", "=", "HERNAN");
	}
	doSubmit('reports/report_confirmation.jsp', 'ReportSave');
}
/* please do not delete */
function generateReport()
{
	var filterFields = setFilterOptions();
	frm.browseTabla.value = '<%=browseObject.getBrowseTabla()%>';
	popupParameters = "browseTabla=<%=browseObject.getBrowseTabla()%>";
	popupParameters = popupParameters + ";reportName=<%=browseObject.getBrowseTabla()%>";

	if(frm.browseTabla.value == 'requisition-line')
	{
		popupParameters = popupParameters + ";colname=RequisitionHeader_owner;cmb_operador==;txt_valor=${userId};cmb_logico=AND;txt_original=Y;cmb_orden=N;"
	}
	//doSubmit('', 'ReportExecute');
	doSubmitToPopup('', 'ReportExecute');
}

function isCriteriaEntered()
{
	var myTable = document.getElementById("filterOptions");

	for (var i=0; i < myTable.rows.length - 2; i++)
	{
		var filterTxt = trim(frm.txt_valor[i]);

		if (!isEmpty(filterTxt))
		{
			return true;
		}
	}
	return false;
}

function selectValue ( objSeleccion ) {
	var frmcolname = objSeleccion.options[objSeleccion.selectedIndex].value;
	return frmcolname;
}

// End Hide script -->
</script>

<%@ include file="/sistema/footer.jsp"%>