<%@ include file="/sistema/header.jsp"%>

<%@ page import="com.sis.manejador.ManejadorFactory"%>
<%@ page import="com.sis.manejador.ManejadorListener"%>
<%@ page import="com.browse.*"%>
<%@ page import="com.comun.utilidad.*"%>
<%@ page import="org.json.JSONObject"%>
<%@ page import="java.util.*"%>
<%@ page import="java.math.*"%>
<%@ page import="com.comun.referencia.*"%>
<%@ page import="com.sis.control.*"%>

<%
	Map peticion = new HashMap();
Auxiliar auxiliar = Auxiliar.getInstance();
PaginaGeneral pagGen = PaginaGeneral.getInstance();
Util util = Util.getInst();

ManejadorListener manejador = ManejadorFactory.getInstance().obtenerManejador(peticion);
manejador.manejarEvento(peticion);

	String oid = auxiliar.org.getOrgcodigo();

	JSONObject objJson = (JSONObject) request.getAttribute("dataJson");
	String dataJson = objJson.toString();
	BrowseObject browseObject = (BrowseObject) request.getAttribute("browseObject");
	List filters = browseObject.getBrowseFilters();
	BrowseColumn browseColumns[] = browseObject.getBrowseColumns();

	String	allowBrowse = "true";
	String  filterType = Util.isEmpty((String) request.getAttribute("filterType")) ? "ADVANCED": (String) request.getAttribute("filterType");
	String	createdFrom = Util.ckNull((String) request.getAttribute("createdfrom"));
	String  appMenuAction = Util.ckNull((String) request.getAttribute("appMenuAction"));
	String  originalRetrieve = (String) request.getAttribute("originalRetrieve");
	String  currencyCode = (String) request.getAttribute("currencyCode");
	String  pp =(String) request.getAttribute("module");
	String	s_refresh = Util.isEmpty((String) request.getAttribute("refreshOpener"))? "N": (String) request.getAttribute("refreshOpener");
	String	sortedColumn = "";
	String	sortedOrder = "";
	if (!Util.isEmpty((String)request.getAttribute("spSortColumn"))) {
		sortedColumn = (String)request.getAttribute("spSortColumn");
		sortedOrder = (String)request.getAttribute("spSortOrder");
	}
	boolean allowEdit = true;
	int totalRecordCount = 0;

	String title = "";

	StringBuffer sbFilterOptions = new StringBuffer();
	StringBuffer sbAoColumns = new StringBuffer();
	StringBuffer sbType = new StringBuffer();
	sbAoColumns.append("[");
	for (int i = 0; i < browseColumns.length; i++) {
		BrowseColumn column = browseColumns[i];
		if(!column.isHidden() && !Util.isEmpty(column.getLabel()) && !column.getAllowFilter().equals("N") && !column.getType().equalsIgnoreCase("checkbox")){
	    	if (column.isFilterDefault()) {
		sbFilterOptions.append("<option value='" + i +  "' selected>" + column.getLabel() + "</option>");
	} else {
		sbFilterOptions.append("<option value='" + i + "'>" + column.getLabel() + "</option>");
	}
		}
		String colName = "\"sName\": \"" + column.getColumnName() + "\"";
    	String label = ", \"sTitle\": \"" + column.getLabel() + "\"";
    	String width = ", \"sWidth\": \"" + column.getIWidth() + "%\"";
//     	String link = (column.getLink().equals("")) ? (", \"sType\" : \"html\"  ")  : "";
    	String sortable = (!column.getAllowFilter().equals("Y"))? ", \"bSortable\": false, \"bSearchable\": false" : "";
    	sbAoColumns.append("{"  + colName + label + sortable + "}");
    	if(i != browseColumns.length -1){
        	sbAoColumns.append(",");
   		}
    	String type = column.getType();
		sbType.append("<input type=\"hidden\" name=\"typeName\" value=\"" + type + "\">");
		sbType.append("<input type=\"hidden\" name=\"columnName\" value=\"" + column.getColumnName() + "\">");
	}
	sbAoColumns.append("]");

	StringBuffer sbOriginalFilterFields = new StringBuffer();
	StringBuffer sbFilterFields = new StringBuffer();
	if (filters != null) {
		for (int ix=0; ix < filters.size(); ix++) {
	StringBuffer sb = new StringBuffer("");
	BrowseFilter filter = (BrowseFilter) filters.get(ix);
	String	colname = filter.getColumnName().replace('.', '_');
	String	sort = filter.getSort();
	String	logicalOperator = "AND";
	if ((ix + 1) < filters.size()) {
		BrowseFilter nextFilter = (BrowseFilter) filters.get(ix + 1);
		logicalOperator = nextFilter.getLogicalOperator();
	}

	sb.append("{ \"name\": \"colname\", \"value\": \"" + colname + "\"},");
	sb.append("{ \"name\": \"filter_txt\", \"value\": \"" + filter.getValue().replace("'","\\\"") + "\"},");
	sb.append("{ \"name\": \"operator\", \"value\": \"" + filter.getOperator() + "\"},");
	sb.append("{ \"name\": \"logicalOperator\", \"value\": \"" + logicalOperator + "\"},");
	sb.append("{ \"name\": \"sort\", \"value\": \"" + filter.getSort() + "\"},");

	if (sort != null && !sort.equalsIgnoreCase("N") && sortedColumn.length() == 0) {
		sortedColumn = colname;
		sortedOrder = sort;
	}
	if (filter.isOriginalFilter()) {
		sb.append("{ \"name\": \"originalFilter\", \"value\": \"Y\"},");
		sbOriginalFilterFields.append(sb);
	} else {
		sb.append("{ \"name\": \"originalFilter\", \"value\": \"N\"},");
		sbFilterFields.append(sb);
	}
		}
		if(sbOriginalFilterFields.toString().endsWith(",")){
	sbOriginalFilterFields = new StringBuffer(sbOriginalFilterFields.substring(0,sbOriginalFilterFields.length() - 1));
		}
		if(sbFilterFields.toString().endsWith(",")){
	sbFilterFields = new StringBuffer(sbFilterFields.substring(0,sbFilterFields.length() - 1));
		}
	}
%>
<div id="typeNames"><%=sbType%></div>
<input type="hidden" name="refreshOpener" value="<%=s_refresh%>">
<input type="hidden" name="allowBrowse" value="<%=allowBrowse%>">
<input type="hidden" name="browseId" value="">
<input type="hidden" name="currencyCode" value="<%=currencyCode%>">
<input type="hidden" name="originalRetrieve" value="N">
<input type="hidden" name="filterType" value="<%=filterType%>">
<input type="hidden" name="createdfrom" value="<%=createdFrom%>">
<input type="hidden" name="appMenuAction" value="<%=appMenuAction%>">
<input type="hidden" name="rowschanged" value="N">

<input type="hidden" name="Pagina_organizacionIde" value="<%=pagGen.pagAct.getOrganizacionIde()%>"/>
<input type="hidden" name="Pagina_tipoConeccion" value="<%=pagGen.pagAct.getTipoConeccion()%>"/>
<input type="hidden" name="Pagina_paginaHijo" value="<%=pagGen.pagAct.getPaginaHijo()%>"/>
<input type="hidden" name="Pagina_paginaPadre" value="<%=pagGen.pagAct.getPaginaPadre()%>"/>
<input type="hidden" name="Pagina_modoAcceso" value="<%=pagGen.pagAct.getAcceso()%>"/>
<input type="hidden" name="Pagina_estadoPagina" value="<%=pagGen.pagAct.getModo()%>"/>
<input type="hidden" name="Pagina_evento" value="<%=pagGen.pagAct.getEvento()%>"/>
<input type="hidden" name="Pagina_accion" value="<%=pagGen.pagAct.getAccion()%>"/>
<input type="hidden" name="Pagina_peticion" value="<%=pagGen.pagAct.getPeticion()%>"/>
<input type="hidden" name="Pagina_navegar" value="<%=pagGen.pagAct.isNavegar()%>"/>





<div id="DlgBrowseFilter" title="Filtro de Opciones" class="ui-panel" style="width: 800px">
	<div class="ui-panel-titlebar">Opciones para <%=browseObject.getTitle()%></div>
	<div class="ui-panel-content">
		<div class="ui-table">
			<div class="ui-tr">
			<!-- start rounded corners-->
			<div id="container1" class="dataTableContainer">
				<table id="tablabrowser">
					<thead>
					</thead>
					<tbody id="browseRows">
					</tbody>
				</table>
			</div>			
			
			</div><!-- <div class="ui-tr"> -->
			
			<div class="linea"></div>
			
			<div class="ui-tr">
			<%
				if ((browseObject.getTitle()).indexOf("Report") >= 0) {
			%>
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Imprimir" name="cmdImprimir" id="cmdImprimir" onclick="javascript: ejecutar(this.id + 'Evento','AJAX');" />
				</div><!-- <div class="ui-td"> -->
			<%
				}
			%>
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Buscar" name="cmdBuscar" id="cmdBuscar" onclick="javascript: browseTable('especialista');" />
				</div><!-- <div class="ui-td"> -->
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Cerrar" name="cmdCerrar" id="cmdCerrar" onclick="javascript: ejecutar(this.id + 'Evento','SUBMIT');" />
				</div><!-- <div class="ui-td"> -->
			</div><!-- <div class="ui-tr"> -->
			
		</div><!--<div class="ui-table">-->
	</div><!-- <div class="ui-panel-content"> -->
	</br> </br>
</div><!--<div id="DlgEspecialista" title="Administracion de Especialistas" class="ui-panel">-->


<SCRIPT value=JavaScript>
<!-- Hide script
var dataJson = "<%=util.getDataJson(auxiliar).toString()%>";
aux = eval("(" + dataJson + ")");

function iniciarFormulario()
{
	if (aux.pag.modoAcceso == "INICIAR") 
	{
		refrescarFormulario();
		llenarFormulario();
		crearModeloTablas();
	}
}	

function llenarFormulario()
{
	frm.Pagina_organizacionIde.value = aux.pag.organizacionIde; 
	frm.Pagina_tipoConeccion.value = aux.pag.tipoConeccion;
	frm.Pagina_paginaHijo.value = aux.pag.paginaHijo;
	frm.Pagina_paginaPadre.value = aux.pag.paginaPadre;
	frm.Pagina_modoAcceso.value = aux.pag.modoAcceso;
	frm.Pagina_estadoPagina.value = aux.pag.estadoPagina;
	frm.Pagina_evento.value = aux.pag.evento;
	frm.Pagina_accion.value = aux.pag.accion;
	frm.Pagina_navegar.value = aux.pag.navegar;
	
}

function refrescarFormulario(valor) 
{
	switch (valor) 
	{
	case "1":
		break;
	}// Fin de switch para estado del formaulario de cliente
}//function refrescarFormulario() {

function crearModeloTablas()
{
	
	console.log("cargando datatable..");
	var browser = browserCheck();
	var browseId = frm.browseId.value;
	var browseTabla = "<%=browseObject.getBrowseTabla()%>";
	var filterStatus = new Array();
	var filterFields = "";
	var currentTypeOptions = '';
	var currentIndex = '';
	var filterStatus = new Array();
	var filterSet = false;
	var currentPage;
	/* Table initialization */
	var s_oid ='<%=oid%>';
	var s_contextPath = '<%=request.getContextPath()%>';
	var s_browseTabla = browseTabla;
	var allowBrowse = '<%=allowBrowse%>';
	var firstTime = "Y";
	var aoColumns = <%=sbAoColumns.toString()%>;
	var irows;
	var totalRows;
	var rowCount;
	var pageSize;
	var evento = "cmdBuscarEvento";

	var datos = [
   		[ "10793205", "Nombre Uno", "Apellido Uno", "Correo" ],
   		[ "10793205", "Nombre Uno", "Apellido Dos", "Correo" ],
   		[ "10793205", "Nombre Uno", "Apellido Tres", "Correo" ],
   		[ "10793205", "Nombre Uno", "Apellido Cuatro", "Correo" ],
   	];	
	
	<%if(dataJson != null)
	{%>
		datos = <%=dataJson%>;			
	<%}%>
	
	console.log("context : " + s_contextPath);
	$(document).ready(function() {
		var aSelected = [];
		var oTable = $('#tablabrowser').dataTable({
			"sDom": '<"H"l<"dataTables_filter_custom">p>rt<"F"ip>',
			"bProcessing": false,
		    "bServerSide": false,
		    "bJQueryUI": true,
		    "sPaginationType": "full_numbers",
		    
		    "iDisplayLength": <%=browseObject.getPageSize()%>,
		   	"aLengthMenu": [
		                    [15, 30, 50, 100, -1],
		                    [15, 30, 50, 100, "All"]],
		   	"sAjaxSource": s_contextPath + "/para/controlador",
		   	"aaData": datos.aaData,
		   	"aoColumns": aoColumns,
		    "fnServerParams": function ( aoData ) {
	        	aoData.push( 
	        		{ "name": "organizationId", "value": s_oid },
					{ "name": "userId", "value": s_uid },
					{ "name": "browseTabla", "value": s_browseTabla },
					{ "name": "browseId", "value": frm.browseId.value },
					{ "name": "allowBrowse", "value": allowBrowse },
					{ "name": "Pagina_evento", "value": evento },
					{ "name": "Pagina_estado", "value": "BUSCANDO" },
					{ "name": "Pagina_peticion", "value": "SUBMIT" });
				<%if(!Util.isEmpty(sbFilterFields.toString()))
				{%>
					aoData.push(<%=sbFilterFields%>);
				<%}%>
				<%if(!Util.isEmpty(sbOriginalFilterFields.toString())){%>
			 		aoData.push(<%=sbOriginalFilterFields%>);
			 	<%}%>
	        },
		    "fnServerData":	function(sSource, aoData, fnCallback, oSettings) {
// 			    validateForm();
			 	var hiddenEls = $("#tablabrowser").find(":hidden").not("script");
			 	var sData = hiddenEls.serialize();
				oSettings.jqXHR = $.ajax( {
					"dataType": 'json',
					"type": "POST",
					"url": sSource + "?" + sData,
					"data": aoData,
					"success": function (json) {
					 	$(".dataTables_filter_custom").html("Filter "
					 		+ "<select name=\"dbcolumnTop\" size=\"1\">" + "<%=sbFilterOptions.toString()%></select>"
								+ "<div id=\"defaultOption\" style=\"visibility:visible; display:inline;\">"
									+ "<input name=\"filterTop\" class=\"text\" size=15 type=text value=\"\">"
								+ "</div>"
								+ "<div id=\"selectOption\" style=\"visibility:hidden; display:none;\"></div>"
								+ "<div class=\"fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers\" style=\"visibility:visible; display:inline;\">"
									+ "<a id=\"filterButton\" class=\"ui-button ui-state-default\">Filter</a>"
									+ "<a id=\"resetButton\" class=\"ui-button ui-state-default\">Reset</a>"
								+ "</div>"
						);
						if (frm.dbcolumnTop) {
							retrieveTypes(frm.dbcolumnTop[frm.dbcolumnTop.selectedIndex] ? frm.dbcolumnTop[frm.dbcolumnTop.selectedIndex].value : frm.dbcolumnTop.value);
						}
						process = "browse-filter-sort-next-page.xml";
						frm.browseId.value = json.browseId;
						irows = json.iTotalDisplayRecords;
						$("#irows").text(json.iTotalRecords);
						totalRows = json.iTotalDisplayRecords;
						rowCount = json.iTotalDisplayRecords;
						pageSize = json.b_pageSize;
// 						addHiddenFields(json.sbHiddenFields);
						currentPage = json.b_currentPage;
						if (myPage != "") {
							if (json.hasDetails) {
								$('#container1').css('margin','5px 3%');
								$('#container1').css('width','85%');
							}
							$('.text').css('height','1.3em');
						} else if (navigator.appName == 'Microsoft Internet Explorer') {
							$('#container1').css('margin','5px 5%');
							$('#container1').css('width','100%');
							$('.text').css('height','1.6em');
						}
						fnCallback(json);
					}//"success": function (json) {
				});//oSettings.jqXHR = $.ajax( {
			}//"fnServerData":	function(sSource, aoData, fnCallback, oSettings) {
		});//var oTable = $('#tablabrowser').dataTable({
		$("#filterButton").live("click", function() {
			oTable.fnFilter($('input[name="filterTop"]').val() == "" ? $('select[name="filterTop"]').val(): $('input[name="filterTop"]').val(),
				$('select[name="dbcolumnTop"]').val()
			);
		});
		$("#resetButton").live("click", function() {
			<%sbFilterFields = new StringBuffer();%>
			fnResetAllFilters(oTable);
		});
		$("#tablabrowser tbody tr").live("mouseover", function() {
			$(this).children().addClass("highlighted");
		});
		$("#tablabrowser tbody tr").live("mouseout", function() {
			$(this).children().removeClass("highlighted");
		});
		$('select[name="dbcolumnTop"]').live("change", function() {
			var idxColumn = frm.dbcolumnTop.value;
			retrieveTypes(idxColumn);
		});
		if (myPage != "") {
			$(document).tooltip({
				items : "tr",
				content : function() {
					var element = $(this);
					if (element.is("tr")) {
						return fnFormatDetails(oTable, this);
					}
				},
				show : null,
				position : {
					my : 'left top',
					at : 'right top-15',
					collision : 'none'
				},
				tooltipClass : "right"
			});
		}
	});//$(document).ready(function() {

	function fnFormatDetails(oTable, nTr) {
		var out = "";
		var oData = oTable.fnGetData(nTr);
		if (oData != undefined)
			out = oData.detailsRow;
		return out;
	}
	function fnResetAllFilters(oTable, bDraw) {
		var oSettings = oTable.fnSettings();
		for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
			oSettings.aoPreSearchCols[iCol].sSearch = '';
		}
		oSettings.oPreviousSearch.sSearch = '';
		if (typeof bDraw === 'undefined')
			bDraw = true;
		if (bDraw)
			oTable.fnDraw();
	}
	function retrieveTypes(idxColumn) {
		var typeValue, typeClass, columnValue;
		if (frm.typeName[idxColumn] == null
				|| frm.typeName[idxColumn] == undefined) {
			typeValue = frm.typeName.value;
			columnValue = frm.columnName.value;
		} else {
			typeValue = frm.typeName[idxColumn].value;
			columnValue = frm.columnName[idxColumn].value;
		}
		if (isType(typeValue)) {
			$("#selectOption").empty();
			typeClass = typeValue;
			if (typeClass.indexOf('-TYPE') > 0) {
				typeClass = 'TYPE';
			}
			$.ajax({
				type : "POST",
				url : s_contextPath + '/TableLookupAll',
				data : "type=" + typeValue + "&typeClass=" + typeClass
					+ "&columnValue=" + columnValue
					+ "&uid=" + s_uid
					+ "&oid=" + s_oid
					+ "&process=types-list-retrieve.xml"
					+ "&resultObj=typesList",
				dataType : 'xml',
				success : function(xml) {
					if ($(xml).find('row').text() != "") {
						var selectType = "<select name=\"filterTop\" size=\"1\">";
						selectType += "<option value=\"\"></option>";
						$(xml).find('row').each(function() {
							selectType += '<option value="' + $(this).find('key').text() + '">'	+ $(this).find('value').text() + '<\/option>';
						});
						selectType += "</select>";
						$("#selectOption").append(selectType);
						hideArea("defaultOption");
						displayAreaInline("selectOption");
					}//if ($(xml).find('row').text() != "") {
				}//success : function(xml) {
			});//$.ajax({
		} else {
// 			hideArea("selectOption");
// 			displayAreaInline("defaultOption");
		}
	}

	function isType(typeValue) {
		return (typeValue.indexOf('-TYPE') >= 0) || (typeValue == 'STATUS')
				|| (typeValue == 'DATE') || (typeValue == 'GENERALSTATUS')
				|| (typeValue == 'TIMEZONE') || (typeValue == 'PYINVSTATUS')
				|| (typeValue == 'ADDRFLD10') || (typeValue == 'ADDRFLD11');
	}
	function setCheckedValue(ckbo) {
		if (ckbo.checked) {
			ckbo.value = 'Y';
		} else {
			ckbo.value = 'N';
		}
	}	
}//function crearModeloTablas()

// End Hide script -->
</SCRIPT>

<%@ include file="/sistema/footer.jsp"%>