<%@ page import="com.comun.referencia.*"%>
<%@ page import="com.comun.utilidad.*"%>
<%@ page import="com.sis.control.*"%>

<%
Auxiliar auxTemp = Auxiliar.getInstance();
PaginaGeneral pagGenTemp = PaginaGeneral.getInstance();
if(pagGenTemp.pagAct.getPeticion().equals("SUBMITPOPUP"))
{%>
	<%@ include file="/sistema/header_popup.jsp"%>
<%
	}
else
{
%>
	<%@ include file="/sistema/header.jsp"%>
<%
	}
%>

<%
	String browseCampo = (String) request.getAttribute("browseCampo");
	String browseTabla = (String) request.getAttribute("browseTabla");
	if(pagGenTemp.pagAct.getModo().equals(Modo.VISUALIZANDO))
	{
%>
		<%@ include file="/browse/browse_opciones.jsp"%>
	<%
		}
		else if(pagGenTemp.pagAct.getModo().equals(Modo.BUSCANDO))
		{
	%>
		<!--------------------------------------------------- CONFIGURACION DATA TABLE----------------------------------------->
		
		<script  type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"></script>
		<link type="text/css" rel="stylesheet"  href="<%=request.getContextPath()%>/css/jquery.dataTables.css" media="screen">

		<%@ include file="/browse/browse_table.jsp"%>
	<%}
%>
<input type="hidden" name="paginaActual" value="/browse/browse_form.jsp"/>
<input type="hidden" name="browseCampo" value="<%=browseCampo%>"/>
<input type="hidden" name="browseTabla" value="<%=browseTabla%>"/>

<%if(pagGenTemp.pagAct.getPeticion().equals("SUBMITPOPUP"))
{%>
	<%@ include file="/sistema/footer_popup.jsp"%>
<%}
else
{%>
	<%@ include file="/sistema/footer.jsp"%>
<%}%>

