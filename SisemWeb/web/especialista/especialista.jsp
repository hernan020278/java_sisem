<%@ page import="com.sis.manejador.ManejadorFactory"%>
<%@ page import="com.sis.manejador.ManejadorListener"%>
<%@ page import="com.comun.utilidad.*"%>
<%@ page import="com.sis.contenedor.Contenedor"%>
<%@ page import="com.comun.utilidad.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.sis.control.*"%>

<%@ include file="/sistema/header.jsp"%>
<%
	Map peticion = new HashMap();
Auxiliar auxiliar = Auxiliar.getInstance();
PaginaGeneral pagGen = PaginaGeneral.getInstance();
Util util = Util.getInst();
	
ManejadorListener manejador = ManejadorFactory.getInstance().obtenerManejador(peticion);
manejador.manejarEvento(peticion);
%>
<input type="hidden" name="paginaActual" value="/sistema/especialista.jsp"/>

<input type="hidden" name="Pagina_organizacionIde" value="<%=pagGen.pagAct.getOrganizacionIde()%>"/>
<input type="hidden" name="Pagina_tipoConeccion" value="<%=pagGen.pagAct.getTipoConeccion()%>"/>
<input type="hidden" name="Pagina_paginaHijo" value="<%=pagGen.pagAct.getPaginaHijo()%>"/>
<input type="hidden" name="Pagina_paginaPadre" value="<%=pagGen.pagAct.getPaginaPadre()%>"/>
<input type="hidden" name="Pagina_modoAcceso" value="<%=pagGen.pagAct.getAcceso()%>"/>
<input type="hidden" name="Pagina_estadoPagina" value="<%=pagGen.pagAct.getModo()%>"/>
<input type="hidden" name="Pagina_evento" value="<%=pagGen.pagAct.getEvento()%>"/>
<input type="hidden" name="Pagina_accion" value="<%=pagGen.pagAct.getAccion()%>"/>
<input type="hidden" name="Pagina_peticion" value="<%=pagGen.pagAct.getPeticion()%>"/>
<input type="hidden" name="Pagina_navegar" value="<%=pagGen.pagAct.isNavegar()%>"/>

<input type="hidden" name="Usuario_orgcodigo"/>
<input type="hidden" name="browseTabla"/>
<div id="DlgEspecialista" title="Administracion de Especialistas" class="ui-panel">
	<div class="ui-panel-titlebar">Administracion de la Informacion de Especialistas</div>
	<div class="ui-panel-content">
		<div class="ui-table">
			<div class="ui-tr">
				<div class="ui-td">
					<fieldset>
						<legend><a href="javascript: browseTablePopup('/especialista/especialista.jsp', 'especialista', 'usucodigo');">D.N.I.</a></legend>
						<input tipoObjeto="meio.mask" type="text" id="Usuario_usucodigo" name="Usuario_usucodigo" value="" alt="texto" maxlength="8" />
					</fieldset>
				</div>
				<div class="ui-td">
					<fieldset>
						<legend>Tipo</legend>
						<select tipoObjeto="ui.combobox" id="Usuario_cargo" name="Usuario_cargo" maxlength="12">
							<option value="ESPECIALISTA">Especialista</option>
							<option value="USUARIO">Usuario</option>
						</select>
					</fieldset>
				</div>
				<div class="ui-td">
					<fieldset>
						<legend>Nombre</legend>
						<input tipoObjeto="meio.mask" type="text" id="Usuario_nombre" name="Usuario_nombre" alt="texto" maxlength="20" />
					</fieldset>
				</div>
				<div class="ui-td">
					<fieldset>
						<legend>Fecha Nacimiento</legend>
						<input tipoObjeto="datepicker" type="text" id="Usuario_fechanacimiento" name="Usuario_fechanacimiento" maxlength="8" />
					</fieldset>
				</div>
			</div>
			<!--<div class="ui-tr">-->
			<div class="ui-tr">
				<div class="ui-td">
					<fieldset>
						<legend>Usuario</legend>
						<input tipoObjeto="meio.mask" type="text" id="Usuario_usuario" name="Usuario_usuario" maxlength="8" />
					</fieldset>
				</div>
				<div class="ui-td">
					<fieldset>
						<legend>Clave</legend>
						<input tipoObjeto="meio.mask" type="text" id="Usuario_clave" name="Usuario_clave" alt="texto" maxlength="4" />
					</fieldset>
				</div>
				<div class="ui-td">
					<fieldset>
						<legend>Email</legend>
						<input tipoObjeto="meio.mask" type="text" id="Usuario_mail" name="Usuario_mail" alt="texto" maxlength="65" />
					</fieldset>
				</div>
			</div>
			<!--<div class="ui-tr">-->
			<div class="ui-tr">
				</br>
			</div>
			<!--<div class="ui-tr">-->
			<div class="ui-tr">
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Agregar" name="cmdAgregar" id="cmdAgregar" onclick="javascript: ejecutar(this.id + 'Evento', 'AJAX');" />
				</div>
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Modificar" name="cmdModificar" id="cmdModificar" onclick="javascript: ejecutar(this.id + 'Evento','AJAX');" />
				</div>
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Buscar" name="cmdBuscar" id="cmdBuscar" onclick="javascript: browseOptions('especialista');" />
				</div>
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Eliminar" name="cmdEliminar" id="cmdEliminar" onclick="javascript: ejecutar(this.id + 'Evento','AJAX');" />
				</div>
				<div class="ui-td">
					<input tipoObjeto="ui.boton" type="button" value="Cerrar" name="cmdCerrar" id="cmdCerrar" onclick="javascript: ejecutar(this.id + 'Evento', 'SUBMIT');" />
				</div>
			</div>
			<!--<div class="ui-tr">-->
		</div>
		<!--<div class="ui-table">-->
	</div>
	<!-- <div class="ui-panel-content"> -->
	</br> </br>
</div>
<!--<div id="DlgEspecialista" title="Administracion de Especialistas" class="ui-panel">-->
<script language="JavaScript">

var dataJson = "<%=util.getDataJson(auxiliar).toString()%>";
aux = eval("(" + <%=util.getDataJson(auxiliar).toString()%> + ")");
pagGen = eval("(" + <%=util.getDataJson(pagGen).toString()%> + ")");

function iniciarFormulario()
{
	if (aux.pag.modoAcceso == "INICIAR") 
	{
		refrescarFormulario();
		llenarFormulario();
	}
}	

function llenarFormulario()
{
	frm.Pagina_organizacionIde.value = aux.pag.organizacionIde; 
	frm.Pagina_tipoConeccion.value = aux.pag.tipoConeccion;
	frm.Pagina_paginaHijo.value = aux.pag.paginaHijo;
	frm.Pagina_paginaPadre.value = aux.pag.paginaPadre;
	frm.Pagina_modoAcceso.value = aux.pag.modoAcceso;
	frm.Pagina_estadoPagina.value = aux.pag.estadoPagina;
	frm.Pagina_evento.value = aux.pag.evento;
	frm.Pagina_accion.value = aux.pag.accion;
	frm.Pagina_navegar = aux.pag.navegar;
	
	setValue(frm.Usuario_usucodigo, aux.usu.usucodigo);
	setValue(frm.Usuario_orgcodigo, aux.pag.organizacionIde);
	setValue(frm.Usuario_cargo, aux.usu.cargo);
	setValue(frm.Usuario_nombre, aux.usu.nombre);
	setValue(frm.Usuario_fechanacimiento, aux.usu.fechanacimiento);
	setValue(frm.Usuario_usuario, aux.usu.usuario);
	setValue(frm.Usuario_clave, aux.usu.clave);
	setValue(frm.Usuario_mail, aux.usu.mail);
}

function activarEspecialista(parAct)
{
	activarElemento(frm.Usuario_usucodigo, parAct);
	activarElemento(frm.Usuario_cargo, parAct);
	activarElemento(frm.Usuario_nombre, parAct);
	activarElemento(frm.Usuario_fechanacimiento, parAct);
	activarElemento(frm.Usuario_usuario, parAct);
	activarElemento(frm.Usuario_clave, parAct);
	activarElemento(frm.Usuario_mail, parAct);
}

function refrescarFormulario() {

	switch (aux.pag.estadoPagina) {
	case "<%=Modo.VISUALIZANDO%>":
		limpiarFormulario();
 		activarEspecialista(false);
		frm.cmdAgregar.setAttribute("value","Agregar");
		frm.cmdModificar.setAttribute("value","Modificar");

		activarElemento(frm.cmdAgregar, true);
		activarElemento(frm.cmdModificar, false);
		activarElemento(frm.cmdBuscar, true);
		activarElemento(frm.cmdEliminar, false);
		activarElemento(frm.cmdCerrar, true);

		break;

	case "<%=Modo.MODIFICANDO%>":

		activarPersona(true);

		frm.cmdAgregar.setAttribute("value","Guardar");
		frm.cmdModificar.setAttribute("value","Cancelar");

		activarElemento(frm.cmdAgregar, true);
		activarElemento(frm.cmdModificar, true);
		activarElemento(frm.cmdBuscar, false);
		activarElemento(frm.cmdEliminar, false);
		activarElemento(frm.cmdCerrar, false);
		break;

	case "<%=Modo.AGREGANDO%>":
		
		limpiarFormulario();
		activarEspecialista(true);

		frm.cmdAgregar.setAttribute("value","Guardar");
		frm.cmdModificar.setAttribute("value","Cancelar");

		activarElemento(frm.cmdAgregar, true);
		activarElemento(frm.cmdModificar, true);
		activarElemento(frm.cmdBuscar, false);
		activarElemento(frm.cmdEliminar, false);
		activarElemento(frm.cmdCerrar, false);

		break;

	case "<%=Modo.BUSCANDO%>":

		limpiarFormulario();
		activarEspecialista(true);
		frm.cmdModificar.setAttribute("value","Cancelar");

		activarElemento(frm.cmdAgregar, false);
		activarElemento(frm.cmdModificar, true);
		activarElemento(frm.cmdBuscar, false);
		activarElemento(frm.cmdEliminar, false);
		activarElemento(frm.cmdCerrar, false);

		break;

	case "<%=Modo.SUSPENDIDO%>":

			activarEspecialista(false);

			activarElemento(frm.cmdAgregar, false);
			activarElemento(frm.cmdModificar, false);
			activarElemento(frm.cmdBuscar, false);
			activarElemento(frm.cmdEliminar, false);
			activarElemento(frm.cmdCerrar, false);

			break;
		}// Fin de switch para estado del formaulario de cliente
	}//function refrescarFormulario() {

	function cmdAgregarEvento() 
	{
		if (frm.cmdAgregar.getAttribute("value") == "Agregar") 
		{
			frm.Usuario_usucodigo.focus();
		} 
		else if (frm.cmdAgregar.getAttribute("value") == "Guardar") 
		{
			if (aux.msg.mostrar == true) {
				msgBox(aux.msg.titulo, aux.msg.valor,aux.msg.tipo);
			}
		}// FIN DE EVALUAR QUE VA HA HACER EL BOTON
		iniciarFormulario();
	}
	
	function cmdModificarEvento() 
	{
		if (frm.cmdModificar.getAttribute("value") == "Modificar") 
		{
			frm.Usuario_usucodigo.focus();
		} 
		else if (frm.cmdModificar.getAttribute("value") == "Cancelar") 
		{
			if (aux.msg.mostrar == true) {
				msgBox(aux.msg.titulo, aux.msg.valor,aux.msg.tipo);
			}
		}// FIN DE EVALUAR QUE VA HA HACER EL BOTON
		iniciarFormulario();
	}
	function cmdBuscarEvento() 
	{
		ejecutarSubmit("cmdBuscarEvento");			
	}
</script>

<%@ include file="/sistema/footer.jsp"%>