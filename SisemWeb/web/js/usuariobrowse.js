function iniciarFormulario()
{
	if(frm.aux.value)
	{
		ajaxRespText = frm.aux.value;
		aux = eval("(" + ajaxRespText + ")");
	}
//	$('#tablaOrdencompra').dataTable({
//		"bJQueryUI": true,
//		"iDisplayLength": [25],
//		"bSort": false,
//		"sPaginationType": "full_numbers"
//	});	
	
	setUpperAllTexto();
	llenarFormulario();
}

function llenarFormulario()
{
	llenarGridTabla();
}

function llenarGridTabla()
{
	var $grid = new grid({
		id:'idTablaUsuario',
		$el: $('#tablaUsuario'),
//		cols: [			 		    
//		    {descr:'Codigo',width:100},		    
//		    {descr:'Nombre',width:300},
//		    {descr:'Apellido',width:300},
//		    {descr:'Acciones',width:100}
//		],
		onLoading: function(){
			console.log('cargando');
			$('#tablaUsuario').block({
				css:{
					border: 'none', 
			        padding: '15px', 
			        backgroundColor: '#000', 
			        '-webkit-border-radius': '10px', 
			        '-moz-border-radius': '10px', 
			        opacity: .5, 
			        color: '#fff'
				},
				message:'Espere..'
			});
		},
		onComplete: function(){
			console.log('termino');
			$('#tablaUsuario').unblock();
		},
		data: '/sisem/controlador',
		params: obtenerParametrosJson(),
		itemdescr: 'registro(s)',
		//toolbarURL: '',
		toolbarHTML: '',
		onContentLoaded: function($el){
		},
		inObject : 'grid',
		load: function (data, $table){
			aux = data;
			if(!$grid.headBuilded){
				
				var cols='[';
				for(var iteCol=0; iteCol<aux.grid.colName.length; iteCol++){
				  cols+='{\"descr\":\"'+aux.grid.colName[iteCol]+'\",\"width\":100}';
				  if((iteCol+1)<aux.grid.colName.length){cols+=',';}
				  //console.log(aux.grid.colName[iteCol]);
				}
				cols+=']';
				
				$.extend($grid,{cols: $.parseJSON(cols)});
				
				var colHTML = '';
				$.each($grid.cols, function (index, column) {
					if($.type(column)=='string')
						colHTML += '<th>' + column + '</th>';
					else
						colHTML += '<th width="'+column.width+'px">' + column.descr + '</th>';
				});
				$grid.$colheader.append(colHTML);	
				$grid.$footer.attr('colspan', $grid.cols.length);
				$grid.$topheader.attr('colspan', $grid.cols.length);
				$.extend($grid, {headBuilded:true});
			}
		
			if (aux.grid.items != null && aux.grid.items.length > 0)
			{
				setValue(frm.txtNroDetalle, aux.grid.items.length);
				for (var iteFil=0; iteFil<aux.grid.items.length; iteFil++)
				{
					var $row='<tr>';
					for(var iteCol=0; iteCol<aux.grid.items[iteFil].length; iteCol++){
						$row+='<td>'+aux.grid.items[iteFil][iteCol]+'</td>';
					}
					$row+='</tr>';
					$table.append($row);
				}
			}
			//refrescarFormulario();			
		}
	});	
}

function cmdAprobarEvento()
{
	$('#tablaOrdencompra').trigger('reloadGrid');
}

function cmdAnularEvento()
{
	$('#tablaOrdencompra').trigger('reloadGrid');
}

function cmdDesaprobarEvento()
{
	$('#tablaOrdencompra').trigger('reloadGrid');
}

function cmdBuscarEvento()
{
	$('#tablaOrdencompra').dataTable().fnClearTable();
	llenarTabla();
}

function validarFormulario()
{
//	if(frm.Pagina_evento.value == 'cmdRegresarEvento')
//	{
//		return true;
//	}
	return true;
}

function generarTextoSapEvento()
{
	window.open("http://localhost/amb/controlador/archivoSap.txt", "_blank","toolbar=yes");
}