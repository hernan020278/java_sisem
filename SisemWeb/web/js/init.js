var $n = $('#left-panel nav');
$n.find('[name=menUsuario]').click(function(){usuario.init();});
$n.find('[name=menSubarea]').click(function(){subarea.init();});
$n.find('[name=menArea]').click(function(){area.init();});
$n.find('[name=menCtrlAsistencia]').click(function(){ctrlasistencia.init();});
$n.find('[name=menAsistencia]').click(function(){asistencia.init();});
jQuery(function ($) {
	$.datepicker.regional['es'] = {
		changeMonth: true,
		changeYear: true,
		closeText: 'Cerrar',
		//prevText: ' nextText: 'Sig>',
		currentText: 'Hoy',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
		'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
		'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	$.datepicker.setDefaults($.datepicker.regional['es']);
	//moment.lang("es-ES");
	
	var hash = window.location.hash;
	if(hash!=""){
		hash = hash.replace("#","");
		eval(hash+".init()");
	}	
});
jQuery(window).bind('resize', function() {
	if (grid = $('.ui-jqgrid-btable:visible')) {
		grid.each(function(index) {
	    	gridId = $(this).attr('id');
	     	gridParentWidth = $('#gbox_' + gridId).parent().width();
	    	$('#' + gridId).setGridWidth(gridParentWidth);
  		});
    }
}).trigger('resize');