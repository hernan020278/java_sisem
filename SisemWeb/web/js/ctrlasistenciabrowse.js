function iniciarFormulario()
{
	if(frm.aux.value)
	{
		ajaxRespText = frm.aux.value;
		aux = eval("(" + ajaxRespText + ")");
	}
//	$('#tablaOrdencompra').dataTable({
//		"bJQueryUI": true,
//		"iDisplayLength": [25],
//		"bSort": false,
//		"sPaginationType": "full_numbers"
//	});	
	
	setUpperAllTexto();
	llenarFormulario();
}

function llenarFormulario()
{
	llenarGridTabla();
}

function llenarGridTabla()
{
	var $grid = new grid({
		id:'idTablaCtrlasistencia',
		$el: $('#tablaCtrlasistencia'),
		cols: [			 		    
		    {descr:'Codigo',width:100},		    
		    {descr:'Nombre',width:300},
		    {descr:'Apellido',width:300},
		    {descr:'Acciones',width:100}
		],
		onLoading: function(){
			console.log('cargando');
			$('#tablaUsuario').block({
				css:{
					border: 'none', 
			        padding: '15px', 
			        backgroundColor: '#000', 
			        '-webkit-border-radius': '10px', 
			        '-moz-border-radius': '10px', 
			        opacity: .5, 
			        color: '#fff'
				},
				message:'Espere..'
			});
		},
		onComplete: function(){
			console.log('termino');
			$('#tablaUsuario').unblock();
		},
		data: '/sisem/controlador',
		params: obtenerParametrosJson(),
		itemdescr: 'registro(s)',
		//toolbarURL: '',
		toolbarHTML: '',
		onContentLoaded: function($el){
		},
		inObject : 'grid',
		load: function (data, $table){
			aux = data;
			if (aux.grid.items != null && aux.grid.items.length > 0)
			{
				setValue(frm.txtNroDetalle, aux.grid.items.length);
				for (var iteFil = 0; iteFil < aux.grid.items.length; iteFil++)
				{
					var $row = '<tr>' +
						'<td>'+aux.grid.items[iteFil][0]+'</td>'+
						'<td>'+aux.grid.items[iteFil][2]+'</td>'+
						'<td>'+aux.grid.items[iteFil][3]+'</td>'+
						'<td>ACCIONES</td>'+
					'</tr>';
					$table.append($row);
				}
			}
			//refrescarFormulario();			
		}
	});	
}

function cmdAprobarEvento()
{
	$('#tablaOrdencompra').trigger('reloadGrid');
}

function cmdAnularEvento()
{
	$('#tablaOrdencompra').trigger('reloadGrid');
}

function cmdDesaprobarEvento()
{
	$('#tablaOrdencompra').trigger('reloadGrid');
}

function cmdBuscarEvento()
{
	$('#tablaOrdencompra').dataTable().fnClearTable();
	llenarTabla();
}

function validarFormulario()
{
//	if(frm.Pagina_evento.value == 'cmdRegresarEvento')
//	{
//		return true;
//	}
	return true;
}

function generarTextoSapEvento()
{
	window.open("http://localhost/amb/controlador/archivoSap.txt", "_blank","toolbar=yes");
}