var theFocus = null;
var allowEdit = true;
var popupAction = null;
var popupUserId = null;
var popupMailId = null;
var popupOrganizationId = null;
var popupParameters = "";
var openedFromPopup = false;
var validFileTypes = "";
var unloadComplete = false;
var strEnableAuditTrail = "Y";

//var Pagina_paginaHijo;
//var Pagina_paginaPadre;
//var Pagina_evento;
//var Pagina_modo.value = modo;
//var Pagina_acceso = acceso;
//var Pagina_browseTabla;
var Pagina_valor;

var aux = "";

/***********************************
 * Checkear Browse                 *
 ***********************************/
function browserCheck() {
	var bw = "";
	if (navigator.appName == "Netscape") {
		bw = "NS";
		if (navigator.appVersion.charAt(0) == "5") {
			bw = "NS6";
		}
	} else if (navigator.appVersion.indexOf("MSIE") != -1) {
		bw = "IE";
	}
	return bw;
}

/*******************************************************************************
 * * * CONFIGURACION JAVASCRIPT GENERAL* * *
 ******************************************************************************/
var marTabAct = "";
var style;
var default_left;
var default_width;

function modificarActionForm(formIde, target, action, method)
{
	var tagForm = document.getElementById(formIde);
	tagForm.setAttribute('target', target);
	tagForm.setAttribute('action', action);
	tagForm.setAttribute('method', method);
}

function crearJavaScript(source) 
{
	var ide = source.replace(/\./g, '_');
	if (!document.getElementById(ide)) 
	{
		if(browserCheck() == "IE")
		{
			var scriptJS = document.createElement('script');
			scriptJS.setAttribute('id', ide);
			scriptJS.setAttribute('name', ide);
			scriptJS.setAttribute('type', 'text/javascript');
			scriptJS.setAttribute('src', 'js/' + source);
			window.document.getElementsByTagName('head')[0].appendChild(scriptJS);
		}
		else
		{
			$('body'). append('<script ide="' + ide + '" name="' + ide +'" src="js/' + source + '" type="text/javascript"></script>');		
		}
	}
}

function eliminarJavaScript(source) 
{
	var ide = source.replace(/\./g, '_');
	if (document.getElementById(ide)) 
	{
		if(browserCheck() == "IE")
		{
			var scriptJS = document.getElementById(ide);
			window.document.getElementsByTagName('head')[0].removeChild(scriptJS);
		}
		else
		{
			$("#" + ide).remove();		
		}
	}
}

function crearStyleCss(source) 
{
	var ide = source.replace(/\./g, '_');
	if (!document.getElementById(ide)) 
	{
		if(browserCheck() == "IE")
		{
			var scriptCSS = document.createElement('link');
			scriptCSS.setAttribute('id', ide);
			scriptCSS.setAttribute('name', ide);
			scriptCSS.setAttribute('type', 'text/css');
			scriptCSS.setAttribute('rel', 'stylesheet');
			scriptCSS.setAttribute('href', 'css/' + source);
			window.document.getElementsByTagName('head')[0].appendChild(scriptCSS);
		}
		else
		{
			$('body'). append('<link type="text/css" rel="stylesheet" ide="' + ide + '" name="' + ide +'" href="css/' + source + '" media="screen"/>');		
		}
	}
}

function eliminarStyleCss(source) 
{
	var ide = source.replace(/\./g, '_');
	if (document.getElementById(ide)) 
	{
		if(browserCheck() == "IE")
		{
			var scriptCSS = document.getElementById(ide);
			window.document.getElementsByTagName('head')[0].removeChild(scriptCSS);
		}
		else
		{
			$("#" + ide).remove();		
		}
	}
}

function login() {
	$
			.msgBox({
				type : "prompt",
				title : "Acceso de Usuarios",
				inputs : [ {
					header : "Usuario",
					type : "text",
					name : "Usuario_usucodigo"
				}, {
					header : "Clave",
					type : "password",
					name : "Usuario_clave"
				}, {
					header : "Recordarme",
					type : "checkbox",
					name : "rememberMe",
					value : "theValue"
				} ],
				buttons : [ {
					value : "Acceder"
				}, {
					value : "Cancelar"
				} ],
				success : function(result, values) {
					var v = result + " has been clicked\n";
					$(values)
							.each(
									function(index, input) {
										v += input.name
												+ " : "
												+ input.value
												+ (input.checked != null ? (" - checked: " + input.checked)
														: "") + "\n";
									});
					alert(v);
					doSubmit("iniciarSession", "index");
				},
				opacity : 0.9
			});
}

/*******************************************************************************
 * * * FUNCIONES UTILITARIOS GENERALES * * *
 ******************************************************************************/
/* Inicio funciones para bloquear pantalla en un paeticion */
function activarBloquer() 
{
	document.getElementById("bloqueoAjax").style.width = $("body").width() + "px";
	document.getElementById("bloqueoAjax").style.height = $("body").height() + "px";
	document.getElementById("bloqueoAjax").style.visibility = "visible";
	document.body.style.cursor = "wait";
}

function desactivarBloquer() 
{
	document.getElementById("bloqueoAjax").style.width = "0px";
	document.getElementById("bloqueoAjax").style.height = "0px";
	document.getElementById("bloqueoAjax").style.visibility = "hidden";
	document.body.style.cursor = "default";
}
/* Fin de funciones para bloquear pantalla en una peticion */
function limpiarFormulario() 
{
	var inputs = document.getElementsByTagName("input");
	var selects = document.getElementsByTagName("select");
	for ( var iteInp = 0; iteInp < inputs.length; iteInp++) 
	{
		if ((inputs[iteInp].type == "text")
				|| ((inputs[iteInp].type == "hidden") && (inputs[iteInp].name
						.match("Pagina_") == null))
				|| (inputs[iteInp].type == "password")
				|| (inputs[iteInp].type == "radio")
				|| (inputs[iteInp].type == "checked")) 
		{
			inputs[iteInp].value = "";
		}
	}
	for ( var iteSel = 0; iteSel < selects.length; iteSel++) 
	{
		if (selects[iteSel].type == "select-one") 
		{
			selects[iteSel].options.selectedIndex = -1;
		}
	}
}

function setValue(ele, value) 
{
	if (ele && value != undefined) {
		if ((ele.type == "text") || (ele.type == "hidden") || (ele.type == "password")) 
		{
			ele.value = value;
		}
		else if (ele.type == "select-one") 
		{
			ele.options.value = "";
			for ( var iteSel = 0; iteSel < ele.length; iteSel++) 
			{
				if (ele.options[iteSel].value.trim() == value.trim() || ele.options[iteSel].text.trim() == value.trim()) 
				{
					ele.options[iteSel].selected = true;
				}
			}
		}
		else if(ele.length > 1)
		{
			for(iteEle = 0; iteEle < ele.length; iteEle++)
			{
				if((ele[iteEle].type == 'radio' || ele[iteEle].type == 'checkbox') && ele[iteEle].value == value)
				{
					ele[iteEle].checked = true
				}
			}
		}
	}
}

function reportFilter(x) 
{
	frm.browseTabla.value = x;
	popupParameters = "browseTabla=" + x;
	doSubmit('/browse/browse_filter_report_options.jsp', 'ReportGetOptions');
}
function trimString(x) 
{
	while ('' + x.charAt(x.length - 1) == ' ') 
	{
		x = x.substring(0, x.length - 1);
	}
	while ('' + x.charAt(0) == ' ') 
	{
		x = x.substring(1, x.length);
	}

	return x;
}
//function trim(x) 
//{
//	x.value = trimString(x.value);
//	return x.value;
//}
function isEmpty(s) 
{
	return ((s == undefined) || (s == null) || (s.length == 0));
}
function checkOneEmail(email) {
	var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i

	if (filter.test(email)) {
		return true;
	} else {
		alert("Please input a valid email address!");
		return false;
	}
}
function browserCheck() {
	var bw = "";
	if (navigator.appName == "Netscape") {
		bw = "NS";
		if (navigator.appVersion.charAt(0) == "5") {
			bw = "NS6";
		}
	} else if (navigator.appVersion.indexOf("MSIE") != -1) {
		bw = "IE";
	}
	return bw;
}
function abrirBusquedad(tabla, campo) {
	popupParameters = popupParameters + "browseCampo=" + campo;
	if (frm.browseTabla) {
		frm.browseTabla.value = tabla;
	} else {
		popupParameters = popupParameters + "&browseTabla=" + tabla;
	}

	popupParameters = popupParameters + "&" + obtenerParametros();
	generarPopup('width=1010px', 'height=580px');
}
function generarPopup(w, h) {
	if (w == undefined) {
		w = '500px';
	}
	if (h == undefined) {
		h = '300px';
	}
	w = w.toLowerCase().replace("width=", "");
	w = w.toLowerCase().replace("px", "");
	h = h.toLowerCase().replace("height=", "");
	h = h.toLowerCase().replace("px", "");

	w = parseInt(w);
	h = parseInt(h);

	if (typeof (window.innerWidth) == 'number') {
		// Non-IE
		wWidth = window.innerWidth;
		wHeight = window.innerHeight;
	} else if (document.documentElement
			&& (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
		// IE 6+ in 'standards compliant mode'
		wWidth = document.documentElement.clientWidth;
		wHeight = document.documentElement.clientHeight;
	} else if (document.body
			&& (document.body.clientWidth || document.body.clientHeight)) {
		// IE 4 compatible
		wWidth = document.body.clientWidth;
		wHeight = document.body.clientHeight;
	}

	// Make it fit
	if (w > wWidth) {
		w = wWidth - 60
	}
	if (h > wHeight) {
		h = wHeight - 60
	}
	mostrarPopup('browse/browse_popup.jsp', w, h, null, true);
}

/*******************************************************************************
 * CREACION DE WINDOW POPUP CON JQUERY
 ******************************************************************************/
var jPopupContainer = null;
var jPopFrame = null;
var jReturnFunc;
var jPopupIsShown = false;
var jDefaultPage = "";
var jReturnVal = null;
var jAHeight = 0;
var jAWidth = 0;

function initPopUp() {
	if (jPopupContainer == null) {
		theBody = document.getElementsByTagName('BODY')[0];
		popcont = document.createElement('div');
		popcont.id = 'jPopupContainer';
		popcont.innerHTML = ''
				+ '<div id="jPopupInner">'
				+ '<iframe src="'
				+ jDefaultPage
				+ '" style="background-color:transparent;" scrolling="auto" frameborder="0" allowtransparency="true" id="jPopupFrame" name="jPopupFrame"></iframe>'
				+ '</div>';

		theBody.appendChild(popcont);
		jPopupContainer = document.getElementById("jPopupContainer");
		jPopFrame = document.getElementById("jPopupFrame");
	}
}

function mostrarPopup(url, jWidth, jHeight, returnFunc, showCloseBox) {
	initPopUp();
	if (jWidth == null || isNaN(jWidth)) {
		jWidth = 100;
	}
	if (jHeight == null || isNaN(jHeight)) {
		jHeight = 100;
	}
	if ($.browser.msie) {
		jAHeight = 0;
		jAWidth = 0;
	}

	jPopupIsShown = true;
	$("#jPopupContainer").dialog({
		autoOpen : false,
		resizable : true,
		height : jHeight + jAHeight,
		width : jWidth + jAWidth,
		modal : true,
		draggable : true,
		title : frm.Pagina_organizacionIde.value,

		open : function() {
			// $(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar").addClass("jTitleBar");
			// $(this).load(url);
			/*
			 * $("#jPopupContainer").load('/puridiom/url', function(response,
			 * status, xhr) { if (status == "error") { var msg = "Sorry but
			 * there was an error: "; $("#error").html(msg + xhr.status + " " +
			 * xhr.statusText); } });
			 */
		}
	});

	$("#jPopupContainer").dialog("open");
	centerPopWin();
	jPopFrame.src = url;
	jPopFrame.width = jWidth - 25;
	jPopFrame.height = jHeight - 50;
	// $(".ui-widget-header").removeClass("ui-widget-header");
}

function centerPopWin() {
	$("#jPopupContainer").dialog({
		position : "center"
	});
}

function hidePopWin(callReturnFunc) {
	jPopupIsShown = false;
	var theBody = document.getElementsByTagName("BODY")[0];
	theBody.style.overflow = "";
	// restoreTabIndexes();
	jPopupContainer.style.display = "none";
	if (callReturnFunc == true && jReturnFunc != null) {
		// Set the return code to run in a timeout.
		// Was having issues using with an Ajax.Request();
		jReturnVal = window.frames["jPopupFrame"].returnVal;
		window.setTimeout('gReturnFunc(gReturnVal);', 1);
	}
	jPopFrame.src = jDefaultPage;
	$("#jPopupContainer").dialog("close");
}
/*******************************************************************************
 * SCRIPT PARA FUNCIONES AJAX
 ******************************************************************************/
var aux;

/*******************************************************************************
 * INICIAR OBJETO AJAX *
 ******************************************************************************/
Ajax = function(url, funcion, funcionError, metodo, parametros, contentType) {

	this.READY_STATE_UNINITIALIZED = 0;
	this.READY_STATE_LOADING = 1;
	this.READY_STATE_LOADED = 2;
	this.READY_STATE_INTERACTIVE = 3;
	this.READY_STATE_COMPLETE = 4;

	this.url = url;
	this.req = null;
	this.onload = funcion;
	this.onerror = (funcionError) ? funcionError : this.defaultError;
	this.cargaContenidoXML(url, metodo, parametros, contentType);
}

Ajax.prototype = {
	cargaContenidoXML : function(url, metodo, parametros, contentType) {
		if (window.XMLHttpRequest) {
			this.req = new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			this.req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		if (this.req) {
			try {
				var loader = this;
				this.req.onreadystatechange = function() {
					loader.onReadyState.call(loader);
				}
				this.req.open(metodo, url, true);
				if (contentType) {
					this.req.setRequestHeader("Content-Type", contentType);
				}
				this.req.send(parametros);
			} catch (err) {
				this.onerror.call(this);
			}
		}
	},
	onReadyState : function() {
		var req = this.req;
		var ready = req.readyState;
		if (ready == this.READY_STATE_COMPLETE) {
			var httpStatus = req.status;
			if (httpStatus == 200 || httpStatus == 0) {
				this.onload.call(this);
			} else {
				this.onerror.call(this);
			}
		}
	},
	defaultError : function() {
		alert("Se ha producido un error al obtener los datos"
				+ "\n\nreadyState:" + this.req.readyState + "\nstatus: "
				+ this.req.status + "\nheaders: "
				+ this.req.getAllResponseHeaders());
	}
}

function validarValorUnico(numFilId, checkId, keyId)
{
	validado = true;
	valor = '';
	if(validarSeleccion(numFilId, checkId, keyId))
	{
		filTab = parseInt(id(numFilId).value);
		numFil = parseInt(id(numFilId).value);
		for (var iteFil = 0; iteFil < numFil; iteFil++)
		{
			if($("#" + checkId + iteFil).prop('checked') && valor != '' && valor != document.getElementById(keyId + iteFil).value)
			{
				validado = false;
				break;
			}
			if($("#" + checkId + iteFil).prop('checked')){valor = document.getElementById(keyId + iteFil).value;}
		}
	}//if(validarSeleccion(numFilId, checkId, keyId))
	else{validado=false;}
	return validado;
}

function validarSeleccion(numFilId, checkId, keyId)
{
	validado = false;
	filTab = parseInt(id(numFilId).value);
	for (var iteFil = 0; iteFil < filTab; iteFil++)
	{
		if($("#" + checkId + iteFil).prop('checked'))
		{
			$("#" + checkId).removeClass("error");
			validado = true;
		}
		else
		{
			$("#" + checkId).addClass("error");
		}
	}
	return validado;
}

function getSeleccionValidado(numFilId, checkId, keyId)
{
	validado = false;
	if(validarSeleccion(numFilId, checkId, keyId))
	{
		var filTab = parseInt(id(numFilId).value);
		var numFil = parseInt(id(numFilId).value);
		var _table = null;
		for (var iteFil = 0; iteFil < numFil; iteFil++)
		{
			_table = $("#" + checkId + iteFil).closest('table');
			if(!$("#" + checkId + iteFil).prop('checked'))
			{
//				$("#" + keyId + iteFil).remove();
				var _obj = $("#" + keyId + iteFil);
				var _item = _obj.closest('tr');
				_item.remove();
//				JQeliminarFilTabById(numFilId, $("#" + keyId + iteFil));
				filTab = filTab - 1;
				$("#" + numFilId).val(filTab);
			}
			else if($("#" + checkId + iteFil).prop('checked'))
			{
				validado = true;
			}
		}
	}//if(validarSeleccion(numFilId, checkId, keyId))
	if(_table != null)
	{
		JQreindexarTabla(numFilId, _table);
	}
	return validado;
}

function validarSeleccion(numFilId, checkId, keyId)
{
	var validado = false;
	var filTab = parseInt(id(numFilId).value);
	for (var iteFil = 0; iteFil < filTab; iteFil++)
	{
		if($("#" + checkId + iteFil).prop('checked'))
		{
			$("#" + checkId).removeClass("error");
			validado = true;
		}
		else
		{
			$("#" + checkId).addClass("error");
		}
	}
	return validado;
}

function validarControles(grupo) 
{
	var textAreas = document.getElementsByTagName("textarea");
	var inputs = document.getElementsByTagName("input");
	var selects = document.getElementsByTagName("select");
	
	var validado = true;
	
	for ( var iteTxtA = 0; iteTxtA < textAreas.length; iteTxtA++) 
	{
		if(textAreas[iteTxtA].title == grupo && textAreas[iteTxtA].value.trim() == "")
		{
			$("#" + textAreas[iteTxtA].id).addClass("error");
			validado = false;
		}
		else
		{
			$("#" + textAreas[iteTxtA].id).removeClass("error");
		}
	}
	for ( var iteInp = 0; iteInp < inputs.length; iteInp++) {
		if ((inputs[iteInp].type == "text")
				|| (inputs[iteInp].type == "password")
				|| (inputs[iteInp].type == "radio")
				|| (inputs[iteInp].type == "checked")) 
		{
			if(inputs[iteInp].title == grupo && (inputs[iteInp].value.trim() == "" || inputs[iteInp].value == "0" || inputs[iteInp].value == "0.00"))
			{
				$("#" + inputs[iteInp].id).addClass("error");
				validado = false;
			}
			else
			{
				$("#" + inputs[iteInp].id).removeClass("error");
			}
		}
	}
	for ( var iteSel = 0; iteSel < selects.length; iteSel++) 
	{
		if (selects[iteSel].type == "select-one") 
		{
			var index = selects[iteSel].options.selectedIndex;
			if(index <= 0 && selects[iteSel].title == grupo)
			{
				$("#" + selects[iteSel].id).addClass("error");
				validado = false;
			}
			else
			{
				$("#" + selects[iteSel].id).removeClass("error");
			}
		}
	}
	
	return validado;
	
}

function activarControles(activo) 
{
	var textAreas = document.getElementsByTagName("textarea");
	var inputs = document.getElementsByTagName("input");
	var buttons = document.getElementsByTagName("button");
	var selects = document.getElementsByTagName("select");
	var parametros = "";

	for ( var iteTxtA = 0; iteTxtA < textAreas.length; iteTxtA++) 
	{
		activarElemento(textAreas[iteTxtA], activo);
	}
	for ( var iteBut = 0; iteBut < buttons.length; iteBut++) 
	{
		activarElemento(buttons[iteBut], activo);
	}
	for ( var iteInp = 0; iteInp < inputs.length; iteInp++) {
		if ((inputs[iteInp].type == "text")
				|| (inputs[iteInp].type == "hidden")
				|| (inputs[iteInp].type == "password")
				|| (inputs[iteInp].type == "radio")
				|| (inputs[iteInp].type == "checked")
				|| (inputs[iteInp].type == "button")) 
		{
			activarElemento(inputs[iteInp], activo);
		}
	}
	for ( var iteSel = 0; iteSel < selects.length; iteSel++) {
		if (selects[iteSel].type == "select-one") 
		{
			activarElemento(selects[iteSel], activo);
		}
	}
}

function activarElemento(ele, activo) {
	if (ele) 
	{
		if(ele.length && ele.type == undefined && ele.length > 1)
		{
			for(iteEle = 0; iteEle < ele.length; iteEle++)
			{
				ele[iteEle].disabled = !activo;
			}
		}
		else
		{
			ele.disabled = !activo;	
			if(ele.type.toUpperCase() == 'BUTTON' || ele.tagName.toUpperCase() == 'BUTTON')
			{
				if(!$('#' + ele.id).data('clase')){$('#' + ele.id).data('clase', $('#' + ele.id).attr('class'));}
				_clase = $('#' + ele.id).data('clase');
				if(_clase){
					if(activo)
					{
						console.log(_clase);
						$('#' + ele.id).removeClass('btn btn-small');
						$('#' + ele.id).addClass(_clase);
					}
					else
					{
						console.log(_clase);
						$('#' + ele.id).removeClass(_clase);
						$('#' + ele.id).addClass('btn btn-small');
					}
				}
			}
			else
			{
				ele.style.backgroundColor=((activo)? 'white' : '#F0F0F0');
			}
		}
	}
}

function obtenerParametros() {
	var textAreas = document.getElementsByTagName("textarea");
	var inputs = document.getElementsByTagName("input");
	var selects = document.getElementsByTagName("select");
	var parametros = "";

	for ( var iteTxtA = 0; iteTxtA < textAreas.length; iteTxtA++) {
		parametros = parametros + textAreas[iteTxtA].name + "="
				+ textAreas[iteTxtA].value + "&";
	}
	for ( var iteInp = 0; iteInp < inputs.length; iteInp++) {
		if ((inputs[iteInp].type == "text")
				|| (inputs[iteInp].type == "hidden")
				|| (inputs[iteInp].type == "password")) {
			parametros = parametros + inputs[iteInp].name + "="
					+ inputs[iteInp].value + "&";
		}
		if(inputs[iteInp].type == "radio" && inputs[iteInp].checked)
		{
			parametros = parametros + inputs[iteInp].name + "=" + inputs[iteInp].value + "&";
		}
		if(inputs[iteInp].type == "checkbox")
		{
			parametros = parametros + inputs[iteInp].name + "=" + inputs[iteInp].checked + "&";
		}
	}
	for ( var iteSel = 0; iteSel < selects.length; iteSel++) {
		if (selects[iteSel].type == "select-one") {
			var index = selects[iteSel].options.selectedIndex;
			if (index > -1) {
				parametros = parametros + selects[iteSel].name + "="
						+ selects[iteSel].options[index].value + "&";
			}
		}
	}
	return parametros;
}

function obtenerParametrosJson() {
	frm.Pagina_evento.value = 'cmdBuscarGridEvento';
	var strJson = "{"; 
	
	var textAreas = document.getElementsByTagName("textarea");
	var inputs = document.getElementsByTagName("input");
	var selects = document.getElementsByTagName("select");
	var aParam = new Array();

	for ( var iteTxtA = 0; iteTxtA < textAreas.length; iteTxtA++) 
	{
		strJson = strJson + "'" + textAreas[iteTxtA].name + "' : '" + textAreas[iteTxtA].value + "',";
	}
	for ( var iteInp = 0; iteInp < inputs.length; iteInp++) 
	{
		if ((inputs[iteInp].type == "text")
				|| (inputs[iteInp].type == "hidden")
				|| (inputs[iteInp].type == "password")) 
		{
			if(inputs[iteInp].name != 'aux')
			{
				strJson = strJson +  "'" + inputs[iteInp].name + "' : '" + inputs[iteInp].value + "',";	
			}
		}
		if(inputs[iteInp].type == "radio" && inputs[iteInp].checked)
		{
			strJson = strJson +  "'" + inputs[iteInp].name + "' : '" + inputs[iteInp].value + "',";
		}
		if(inputs[iteInp].type == "checkbox")
		{
			strJson = strJson +  "'" + inputs[iteInp].name + "' : '" + inputs[iteInp].checked + "',";
		}
	}
	for ( var iteSel = 0; iteSel < selects.length; iteSel++) 
	{
		if (selects[iteSel].type == "select-one") 
		{
			var index = selects[iteSel].options.selectedIndex;
			if (index > -1) 
			{
				strJson = strJson +  "'" + selects[iteSel].name + "' : '" + selects[iteSel].options[index].value + "',";

			}
		}
	}
	strJson = strJson.substr(strJson, strJson.length - 1, strJson.length);
	strJson += '}';
	strJson = strJson.replace(/,\'\' : \'\'/g, '');
	
	return eval("(" + strJson + ")");
}

function procesarErrorAjax() {
	msgBox('Error Ajax', 'Error al procesar un proceso ajax', 'error');
}

function procesarEventoSolicitado() 
{
	try
	{
		eval(frm.Pagina_evento.value + '()');
		if(aux.msg.mostrar)
		{
			msgBox(aux.msg.titulo, aux.msg.valor, aux.msg.tipo);
		}
	}
	catch (err)
	{
		if(aux.msg != undefined && isEmpty(aux.msg.valor))
		{
			msgBox('procesarAjaxJQuery()', err + '<br/>' + aux.msg.valor , 'error');
		}
		else
		{
			msgBox('procesarAjaxJQuery()', err, 'error');
		}
	}
}

function procesarAjax() {
	ajaxRespText = ajax.req.responseText;
	aux = eval("(" + ajaxRespText + ")");
	// aux = JSON.parse(ajaxRespText);
	procesarEventoSolicitado();
}
function procesarAjaxJQuery(ajaxResp) {
	try {
		aux = $.parseJSON(ajaxResp);
		if (aux.pagNew.navegar && (aux.pagNew.paginaHijo != aux.pagAct.paginaHijo)) 
		{
			abrirPaginaAjax(aux.pagNew.paginaHijo);
		} 
		else 
		{
			procesarEventoSolicitado();
			desactivarBloquer();
		}	
	} 
	catch (err) 
	{
		if(aux.msg != undefined && isEmpty(aux.msg.valor))
		{
			msgBox('procesarAjaxJQuery()', err + '<br/>' + aux.msg.valor + '<br/>' + ajaxResp, 'error');
		}
		else
		{
			msgBox('procesarAjaxJQuery()', err + '<br/>' + ajaxResp, 'error');	
		}
	}
}

function ejecutarValor(evento, valor, peticion) 
{
	frm.Pagina_evento.value = evento;
	frm.Pagina_valor.value = valor;
	ejecutar(evento, peticion);
}

function ejecutar(eventoObjeto, peticion) {
	validado = false;
	frm.Pagina_evento.value = eventoObjeto;
	// if(typeof window.validarFormulario == "function")
	if (validarFormulario()) {
		if (peticion == "AJAX") {
			ejecutarAjax(eventoObjeto);
		} else if ((peticion == "SUBMIT")) {
			if (document.getElementById("jPopupContainer")) {
				$("#jPopupContainer").dialog("close");
			}
			ejecutarSubmit(eventoObjeto);
		} else if ((peticion == "SUBMITPOPUP")) {
			ejecutarSubmitPopup(eventoObjeto);
		} else if ((peticion == "CLIENT")) {
			procesarEventoSolicitado();
		}
	} else {
		msgBox("Error de Validacion", "Se necesita validar datos", "error");
	}
}

function ejecutarAjax(eventoObjeto) {
//	frm.Pagina_evento.value = eventoObjeto;
//	frm.Pagina_peticion.value = "AJAX";
//	activarBloquer();
	$.ajax({
		url : "controlador/SisemListener.php",
		type : "POST",
		async : true,
		data : obtenerParametros(),
		datatype : 'json',
		beforeSend: function() 
		{
			frm.Pagina_evento.value = eventoObjeto;
			frm.Pagina_peticion.value = "AJAX";
			activarBloquer();
	    },
		success : function(ajaxResp) {
			procesarAjaxJQuery(ajaxResp);
		},
		error : procesarErrorAjax
	});

	/*
	 * ajax = new
	 * Ajax("http://www.intipunkulodge.com/tmp/com/sisem/control/SisemListener.php",
	 * procesarAjax,procesarErrorAjax, "POST",
	 * obtenerParametros(),"application/x-www-form-urlencoded");
	 */
}

function ejecutarSubmit(eventoObjeto) {
	frm.Pagina_evento.value = eventoObjeto;
	frm.Pagina_peticion.value = "SUBMIT";
	frm.target = "_self";
	frm.submit();
}
function ejecutarSubmitPopup(eventoObjeto) {
	var parentWindow = window.parent;
	if (parentWindow.popupAction != null) {
		frm.action = parentWindow.popupAction;
		parentWindow.popupAction = null;
	}
	frm.target = "_self";
	frm.submit();
}
function ejecutarServletAjax(eventoObjeto) {
	frm.Pagina_evento.value = eventoObjeto;
	ajax = new Ajax("http://localhost:8080/sisem/ServletAjax", procesarAjax,
			procesarErrorAjax, "POST", obtenerParametros(),
			"application/x-www-form-urlencoded");
}

function abrirPaginaAjax(url) {

	$.ajax({
		type : "POST",
		url : url,
		data : {},
		beforeSend: function() 
		{
//			alert('uno');
			activarBloquer();

			eliminarJavaScript(obtenerNombreFromUrl(frm.Pagina_paginaHijo.value) + ".js");
			frm.Pagina_paginaHijo.value = url;
			
			crearJavaScript(obtenerNombreFromUrl(url) + ".js");
			cargarPlugin(frm.Pagina_paginaHijo.value);
			
	    },
		success : function(datos) 
		{
//			alert('dos');
			procesarSuccessAjaxJQuery(datos, url)
			iniciarPlugin(frm.Pagina_paginaHijo.value);
			actualizarItemMenu();
		},
	    complete: function() 
	    {
//	    	alert('tres');
			iniciarFormulario();
			desactivarBloquer();
	    },
		error : function(xhr, ajaxOptions, thrownError) {
			msgBox("Error Ajax", thrownError, "error");
		}
	});
}

function ubicarTop() {
	$("html, body").animate({
		scrollTop : 0
	}, 1000);
}

function abrirPaginaJsp() {
	if (aux.nav) {
		window.open("http://localhost:8080/sisem/controlador", "_self", true);
	}
}

function browseOptions(brwNm) {
	frm.browseTabla.value = brwNm;
	frm.Pagina_estadoPagina.value = "VISUALIZANDO";
	ejecutar('cmdBuscarEvento', 'SUBMIT');
}

function browseTable(brwNm) {
	setFilterOptions();

	frm.browseTabla.value = brwNm;
	frm.Pagina_estadoPagina.value = "BUSCANDO";
	ejecutar('cmdBuscarEvento', 'SUBMIT');
}

function browseTablePopup(paginaPadre, tabla, campo) {
	frm.Pagina_estadoPagina.value = "BUSCANDO";
	frm.Pagina_evento.value = "etiBuscarEvento";
	frm.Pagina_paginaHijo.value = "/browse/browse_form.jsp";
	frm.Pagina_paginaPadre.value = paginaPadre;
	frm.Pagina_peticion.value = "SUBMITPOPUP";
	abrirBusquedad(tabla, campo);
}

function cambiarIdioma(idioma) {
	frm.idioma.value = idioma;
	ejecutar("cambiarIdiomaEvento", "SUBMIT");
}

function obtenerNombreFromUrl(url) {
	splitPaginaHijo = url.split('/');
	pagina = "";

	if (splitPaginaHijo.length > 0) {
		pagina = splitPaginaHijo[splitPaginaHijo.length - 1];
		pagina = pagina.replace(/\.php/g, '');

		if (pagina.indexOf('/') > -1) {
			arrayPagina = pagina.split('_');
			pagina = "";

			for ( var ite = 0; ite < arrayPagina.length; ite++) {
				// pagina = pagina + strtoupper(substr($iteArrPag, 0, 1)) .
				// substr($iteArrPag, 1, count($iteArrPag));
			}
		} else {
			// pagina = strtoupper((substr($pagina, 0, 1))) . substr($pagina, 1,
			// strlen($pagina));
		}
	}
	return pagina;
}

//Funci�n para verificar si existe una clase
function existeClase(obj,cls)
{
	return obj.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}
//Funci�n para agregar una clase, si no existe la clase enviada - agrega la clase.

function eliminarFilaTabla(obj){   
	var oTr = obj;
	while(oTr.nodeName.toLowerCase()!='tr'){
		oTr=oTr.parentNode;                
	}
	var root = oTr.parentNode;
	root.removeChild(oTr);
}

function eliminarFilTabById(numFilId, obj){   
	var oTr = obj;
	while(oTr.nodeName.toLowerCase()!='tr'){
		oTr=oTr.parentNode;                
	}
	var root = oTr.parentNode;
	root.removeChild(oTr);
	numFil = parseInt($("#" + numFilId).val());
	numFil = (numFil > 0) ? (numFil - 1) : 0;
	$('#' + numFilId).val(numFil);
	
	/* Re-index */
	//var _items = $('#tablaDetalleCompra_wrapper tr')
	/** Re-index */
}
function JQeliminarFilTabById(numFilId,obj){
	var _item = obj.closest('tr');
	var _table = obj.closest('table');
	var _index = 0;
	_item.remove();
	for(i=1;i<_table.find('tr').length;i++){
		var _row = _table.find('tr').eq(i);
		if(_row.find('input').length>0){
			for(j=0;j<_row.find('input').length;j++){
				var _input = _row.find('input').eq(j);
				var _id = _input.attr('id');
				var _name = _input.attr('name');
				var split_new_id = _id.split("_");
				var split_new_name = _name.split("_");
				var num_digits_id_i = split_new_id[split_new_id.length-1].length;
				var num_digits_name_i = split_new_name[split_new_name.length-1].length; 
				var new_id = _id.substring(0,_id.length-num_digits_id_i);
				var new_name = _name.substring(0,_name.length-num_digits_name_i);
				_input.attr('id',new_id+_index);
				_input.attr('name',new_name+_index);
			}
			_index++;
		}
	}
	numFil = parseInt($("#" + numFilId).val());
	numFil = (numFil > 0) ? (numFil - 1) : 0;
	$('#' + numFilId).val(numFil);
}

function JQreindexarTabla(numFilId, objTabla){
//	var _item = obj.closest('tr');
//	var _table = obj.closest('table');
	var _table = objTabla;
	var _index = 0;
//	_item.remove();
	for(i=1;i<_table.find('tr').length;i++){
		var _row = _table.find('tr').eq(i);
		if(_row.find('input').length>0){
			for(j=0;j<_row.find('input').length;j++){
				var _input = _row.find('input').eq(j);
				var _id = _input.attr('id');
				var _name = _input.attr('name');
				var split_new_id = _id.split("_");
				var split_new_name = _name.split("_");
				var num_digits_id_i = split_new_id[split_new_id.length-1].length;
				var num_digits_name_i = split_new_name[split_new_name.length-1].length; 
				var new_id = _id.substring(0,_id.length-num_digits_id_i);
				var new_name = _name.substring(0,_name.length-num_digits_name_i);
				_input.attr('id',new_id+_index);
				_input.attr('name',new_name+_index);
			}
			_index++;
		}
	}
	$('#' + numFilId).val(_index);
}

function eliminarFilaListaTabla(numFilId, ele, lista)
{
	var oTr = ele;
	while(oTr.nodeName.toLowerCase()!='tr'){
		oTr=oTr.parentNode;                
	}
	index = oTr.rowIndex - 1;
	if (index > -1) 
	{
	    lista.splice(index, 1);
	    
		numFil = parseInt($("#" + numFilId).val());
		numFil = (numFil > 0) ? (numFil - 1) : 0;
		$('#' + numFilId).val(numFil);
	}
}

function limpiarTabla(idTabla){
	var nFilas = $('#'+idTabla).find('tr').length;
	n = 0;
	$('#'+idTabla+' tr').each(function() {
	   if (n > 0 && n < nFilas)
		  $(this).remove();
	   n++;
	});	
	id('txtNroDetalle').value = 0;
}

function llenarComboGeneral(elementById, lista)
{
	$('#' + elementById + ' option').remove();
	$('#' + elementById).append('<option value="0">Seleccione</option>');
	if (lista instanceof Array)
	{
		for (var ite = 0; ite < lista.length; ite++)
		{
			$('#' + elementById).append('<option value="' + lista[ite].codigo + '">' + lista[ite].descripcion + '</option>');
		}
	}
}

function validarNumericos()
{
	$(".entero").each(function(idx, item){
		if( !$.isNumeric($(item).val() ) ){
			$(item).val('0');
		}
	});	
	$(".decimal").each(function(idx, item){
		if( !$.isNumeric($(item).val() ) ){
			$(item).val('0.00');
		}
	});	
}

function formatoDecimal(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var source = evt.target;
    var valor = source.value;
    console.log(charCode);
    var respuesta = false;
    //8  : BACKSPACE
    //46 : DELETE 
    //37 : FLECHA LEFT
    //39 : FLECHA RIGHT
    if ((charCode >= 48 && charCode <= 57)  || charCode == 46 || charCode == 8 || charCode == 37 || charCode == 39 || charCode == 9 || charCode == 18)
    {
		var posPto = valor.indexOf('.');
		var decimales = ( posPto == -1 )? '' : valor.substr(posPto + 1, valor.length);
		if( charCode == 8 || charCode == 37 || charCode == 39 )
		{
			respuesta = true;
		}
		else if(( posPto > 0 && charCode == 46 ) || decimales.length == 2)
		{
			respuesta = false;
		}
		else
		{
			respuesta = true;
		}
   }
   return respuesta;
}

function formatoEntero(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode == 8 || charCode == 9 || charCode == 18)
            return true;
        
    return false;
}

function seleccionarItemCombo(combo)
 {
	 if (combo.type == "select-one") {
		var index = combo.options.selectedIndex;
		if (index > -1) {
			selects[iteSel].options[index].value + "&";
		}
	}
 }
 
 function addClass(obj,cls)
 {
  if(!existeClase(obj,cls)) {
   obj.className+=" "+cls;
  }
 }
 //Funci�n para Eliminar una clase
 function removeClass(obj,cls)
 {
  if(existeClase(obj,cls)) {
   var exp =new RegExp('(\\s|^)'+cls+'(\\s|$)');
   obj.className=obj.className.replace(exp,"");
  }
 }

 function JQseleccionarFilaTabla(tablaId, clave, valor){
//		var _item = obj.closest('tr');
//		var _table = _this.closest('table');
	 	var _table = $('#' + tablaId);
	 	var _tbody = _table.find('tbody');
		var _index = 0;
//		_item.remove();
		for(var i=0;i<_tbody.find('tr').length;i++){
			var _row = _tbody.find('tr:eq('+i+')');
//			$('#tablaTramite').find('tbody').find('tr:eq(0)').find('td:eq(0)').find('[name="tramite_id_Tramite"]').val()
			if(_row.find('td:eq(0)').find('input').val()==valor){
				_row.find('td').css('background-color','rgb(176, 190, 217)');
			}
			else{
				_row.find('td').css('background-color','rgb(255,255,255)');
			}
		}
	}
 
 function seleccionarFilaTabla(tablaId, clave, valor)
 {
// 	clave.substr(clave.lastIndexOf('_') + ite, 1);
 	for(ite = 0; ite < document.getElementById(tablaId).rows.length; ite++)
 	{
		aEleTr = document.getElementById(tablaId).rows[ite].children;
		for(iteTd= 0; iteTd < aEleTr.length; iteTd++)
		{
			eleTd = aEleTr[iteTd];
			if(eleTd.nodeName.toLowerCase() =='td')
			{
	 			if(document.getElementById(clave + (ite - 1)) && document.getElementById(clave + (ite - 1)).value == valor)
	 			{
	 				eleTd.style.backgroundColor = 'rgb(176, 190, 217)';
	 			}
	 			else
	 			{
	 				eleTd.style.backgroundColor = 'rgb(255, 255, 255)';
	 			}
			}
		}		
 	}
 }
 
 function existeDatoTabla(tablaId, clave, valor)
 {
// 	clave.substr(clave.lastIndexOf('_') + ite, 1);
 	for(ite = 0; ite < document.getElementById(tablaId).rows.length; ite++)
 	{
		if(document.getElementById(clave + ite) && document.getElementById(clave + ite).value == valor)
		{
			return true;
		}
 	}
 	return false;
 }

 
 var flag = 0;
 function getDataFilaTabla(ide)
 {
 	if(parseFloat(id("txtNroDetalle").value)>0){
 		var pos= id("txtNroDetalle").value;
 		var x=document.getElementById(ide);	
 		var fila=parseInt(x.rowIndex);	
 		var data=document.getElementById('tablaMoto').rows[fila].cells;
 		id('detCompraTxtModelo'+pos).value		=data[2].innerHTML;
 		id('detCompraTxtIdMoto'+pos).value		=data[0].innerHTML;
 		id('detCompraTxtStock'+pos).value		=data[6].innerHTML;
 		id('detCompraTxtNroChasis'+pos).value	=data[3].innerHTML;
 		id('detCompraTxtCantidad'+pos).value		=1;
 		id('detCompraTxtPrecio'+pos).value		=data[7].innerHTML;
 		id('detCompraTxtDescuento'+pos).value	=0;
 		
 		if(flag == 0){
 			var cantidad = parseFloat(id('detCompraTxtCantidad'+pos).value);
 			var precio = parseFloat(id('detCompraTxtPrecio'+pos).value) - parseFloat(id('detCompraTxtDescuento'+pos).value);
 			var total = parseFloat(id("txtTotal").value) + (cantidad * precio);
 			id("txtTotal").value = total.toFixed(2);
 			var igv = total*0.18;
 			var subtotal = total - igv;
 			id("txtIgv").value = igv.toFixed(2);
 			id("txtSubTotal").value = subtotal.toFixed(2);
 			flag = 1;
 		}
 	}
 }

function getNumber(value)
{
	if (isNaN(value))
	{
		return 0;
	}
	else
	{
		if(isFloat(value)){return parseFloat(value);}
		else if(isInteger(value)){return parseInt(value);}
	}
}
 
function isFloat(value) 
{
	if (isNaN(value) || value.indexOf(".") < 0)
	{
		return false;
	} 
	else
	{
		if (isNaN(parseFloat(value)))
		{
			return false;
		} 
		else
		{
			return true;
		}
	}
}

function isInteger(value)
{
	if (isNaN(value))
	{
		return false;
	} 
	else
	{
		if (isNaN(parseInt(value)))
		{
			return false;
		} 
		else 
		{
			return true;
		}
	}
}

 function calcularCantidadxPrecio(totalFilas, cantidadId, precioId)
 {
     cant = id(totalFilas).value;
 	var total = 0;
 	if(cant > 0){ 
 		for(d = 0; d < cant; d++){
 			var tmp = $('#' + cantidadId + d).val();		
 			if(tmp === undefined || tmp===null)
 			{
 				total = total.toFixed(2);
 			}
 			else
 			{
 				cantidad	= $('#' + cantidadId + d).val();
 				precio		= $('#' + precioId + d).val();
 				total = total + (cantidad * precio);
 			}
 		}
 	}
 	return total.toFixed(2); 	
 }

 function calcularCantidadxPrecioxTotal(totalFilas, cantidadId, precioId, totalId)
 {
     cant = id(totalFilas).value;
 	var total = 0.000;
 	if(cant > 0){ 
 		for(d = 0; d < cant; d++){
 			var tmp = $('#' + cantidadId + d).val();		
 			if(tmp === undefined || tmp===null)
 			{
 				total = total;
 			}
 			else
 			{
 				cantidad	= $('#' + cantidadId + d).val();
 				precio		= $('#' + precioId + d).val();
 				$('#' + totalId + d).val(cantidad * precio);
 				total = total + (cantidad * precio);
 			}
 		}
 	}
 	return (total > 0) ? total.toFixed(2) : total; 	
 }

 function sumarColumnaTipo(numFilId, columnaId, tipo)
 {
     cant = id(numFilId).value;
     cant = parseFloat(cant);
 	var total = 0;
 	if(cant > 0){ 
 		for(d = 0; d < cant; d++){
 			var tmp = $('#' + columnaId + d).val();		
 			if(tmp === undefined || tmp===null)
 			{
 				return total.toFixed(2);
 			}
 			else
 			{
 				if(tipo == 'INTEGER')
 				{
 					cantidad	= parseInt($('#' + columnaId + d).val());
 				}
 				else
 				{
 					cantidad	= parseFloat($('#' + columnaId + d).val());
 				}
 				total = total + cantidad;
 			}
 		}
 	}
 	return total.toFixed(2);
 }

 function sumarColumna(numFilId, columnaId, tipo)
 {
     cant = id(numFilId).value;
 	var total = 0;
 	if(cant > 0){ 
 		for(d = 0; d < cant; d++){
 			var tmp = $('#' + columnaId + d).val();		
 			if(tmp === undefined || tmp===null)
 			{
 				return total.toFixed(2);
 			}
 			else
 			{
 				if(tipo == 'INTEGER')
 				{
 					cantidad	= parseInt($('#' + columnaId + d).val());
 				}
 				else
 				{
 					cantidad	= parseFloat($('#' + columnaId + d).val());
 				}
 				total = total + cantidad;
 			}
 		}
 	}
 	return total.toFixed(2);
 }

 function sumarColumnaExcepto(numFilId, columnaId, colExcId, colExcVal)
 {
     cant = id(numFilId).value;
 	var total = 0.00;
 	if(cant > 0){ 
 		for(d = 0; d < cant; d++){
 			var tmp = $('#' + columnaId + d).val();		
 			if(tmp === undefined || tmp===null)
 			{
 				return total.toFixed(2);
 			}
 			else
 			{
 				if($('#' + colExcId + d).val() != colExcVal)
 				{
 					cantidad	= getNumber($('#' + columnaId + d).val());
 	 				total = total + cantidad;
 				}
 			}
 		}
 	}
 	return total.toFixed(2);
 } 
 function sumaColumna(numFilId, columnaId)
 {
     cant = id(numFilId).value;
 	var total = 0;
 	if(cant > 0){ 
 		for(d = 0; d < cant; d++){
 			var tmp = $('#' + columnaId + d).val();		
 			if(tmp === undefined || tmp===null)
 			{
 				total = total;
 			}
 			else
 			{
				cantidad	= $('#' + columnaId + d).val();
 				total = total + cantidad;
 			}
 		}
 	}
 	return total.toFixed(2);
 }
 
function setValueSelect(eleSelectById, element)
{
	element.value = $('#' + eleSelectById + ' option:selected').html();
}

function leftStr(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}
function lightStr(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

function indiceTabla(obj){   
	var oTr = obj;
	while(oTr.nodeName.toLowerCase()!='tr'){
		oTr=oTr.parentNode;                
	}
	return oTr.rowIndex;
}

function setSel(idEle, inicio,fin){
	input=document.getElementById(idEle);
	if(typeof document.selection != 'undefined' && document.selection){
		tex=input.value;
		input.value='';
		input.focus();
		var str = document.selection.createRange();
		input.value=tex;
		str.move('character', inicio);
		str.moveEnd("character", fin);
		str.select();
	}
	else if(typeof input.selectionStart != 'undefined'){
		input.setSelectionRange(inicio,fin);
		input.focus();
	}
}
$.fn.selectRange = function(start, end) {
    if(!end) end = start;
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

function getPos(idEle){
	input=document.getElementById(idEle);
	if(typeof document.selection != 'undefined' && document.selection && typeof input.selectionStart == 'undefined'){
		var str =document.selection.createRange();
		stored_range = str.duplicate();
		stored_range.moveToElementText(input);
		stored_range.setEndPoint( 'EndToEnd', str );
		input.selectionStart = stored_range.text.length - str.text.length;
		input.selectionEnd = input.selectionStart + str.text.length;
		return input.selectionStart;
		//alert(input.selectionStart)
	}else if(typeof input.selectionStart != 'undefined'){
		//alert(input.selectionStart);
		return input.selectionStart;
	}
}