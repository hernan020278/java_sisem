var grid = function(p){
	if(p==null) return false;
	if(p.pagination==null) p.pagination = true;
	if(p.search==null) p.search = true;
	if(p.headfixed==null) p.headfixed = true;
	if(p.id==null) p.id = "datatable_id";
	if(p.headBuilded==null) p.headBuilded = false;
//	p.cols = [			 		    
//	    {descr:'Codigo',width:100},		    
//	    {descr:'Nombre',width:300},
//	    {descr:'Apellido',width:300},
//	    {descr:'Acciones',width:100}
//    ];
	
	//p.cols=null;
	
	var $table = $('<table id="'+p.id+'" class="table table-bordered table-hover datagrid" style="width:100%">');
	var $thead = $('<thead class="header"><tr></tr></thead>');
	var $tfoot = $('<tfoot><tr></tr></tfoot>');
	var _head_th='<th>';
	_head_th+='<span class="datagrid-header-title"></span>';
	_head_th+='<div class="datagrid-header-left" style="float:left;">';
	_head_th+='<button type="button" name="btnRefreshGrid" class="btn btn-small btn-success"><span class="fa fa-refresh"></span></button>';
	_head_th+='</div>';
	_head_th+='<div class="datagrid-header-right" style="float:right;">';
	_head_th+='<div class="input-append search">';
	_head_th+='<div class="input-append">';
	_head_th+='<input type="text" id="appendedInputButton" name="appendedInputButton" placeholder="Buscar">';
	_head_th+='<button type="button" class="btn btn-primary btn-small"><span class="icon-magnifying-glass-alt"></span> Buscar</button>';
	_head_th+='</div>';
	_head_th+='</div>';
	_head_th+='</div>';
	_head_th+='</th>';
	var _foot ='<th>';
	_foot+='<div class="dataTables_info datagrid-footer-left" style="width:50%;display:none;">';
	_foot+='<div class="grid-controls">';
	_foot+='<span><i class="datagrid-start"></i> - <i class="datagrid-end"></i> de <i class="datagrid-count"></i></span>';
	_foot+='<select class="datagrid-pagesize input-small" style="float:none;"><option value="10" selected>10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option></select>';
	_foot+='<span>Por P&aacute;gina</span>';
	_foot+='</div>';
	_foot+='</div>';
	//_foot+='</th>';
	//_foot+='<th>';
	_foot+='<div class="dataTables_paginate datagrid-footer-right" style="width:50%;display:none;">';
	_foot+='<div class="datagrid-pager">';
	_foot+='<button type="button" class="btn btn-primary btn-small datagrid-prevpage"><span class="fa fa-arrow-circle-left"></span></button>';
	_foot+='<span>P&aacute;gina</span>';
	_foot+='<select class="datagrid-pages-all input-small" style="float:none;"></select>';
	_foot+='<span>de <i class="grid-pages"></i></span>';
	_foot+='<button type="button" class="btn btn-primary btn-small datagrid-nextpage"><span class="fa fa-arrow-circle-right"></span></button>';
	_foot+='</div>';
	_foot+='</div>';
	_foot+='</th>';
	$thead.find('tr').append(_head_th);
	$tfoot.find('tr').append(_foot);
	$table.append($thead);
	$table.append($tfoot);
	p.$el.append($table);
	p.$thead = p.$el.find('thead');
	p.$theadset = p.$el.find('.datagrid-header-left');
	p.$tfoot = p.$el.find('tfoot');
	p.$footer = p.$el.find('tfoot th');
	p.$footerchildren = p.$footer.children().show().css('visibility', 'hidden');
	p.$topheader = p.$el.find('thead th');
	p.$searchcontrol = p.$el.find('.search');
	/*this.$filtercontrol = this.$element.find('.filter');*/
	p.$pagesize = p.$el.find('.datagrid-pagesize');
	p.$pagedropdown = p.$el.find('.datagrid-pages-all');
	p.$prevpagebtn = p.$el.find('.datagrid-prevpage');
	p.$nextpagebtn = p.$el.find('.datagrid-nextpage');
	p.$pageslabel = p.$el.find('.datagrid-pages');
	p.$countlabel = p.$el.find('.datagrid-count');
	p.$startlabel = p.$el.find('.datagrid-start');
	p.$endlabel = p.$el.find('.datagrid-end');
	p.$tbody = $('<tbody style="height: '+p.$height+'px; overflow: auto">').insertAfter(p.$thead);
	p.$colheader = $('<tr>').appendTo(p.$thead);

	if(p.toolbarURL!=null||p.toolbarHTML!=null){
		if(p.toolbarURL!=null){
			$.post(p.toolbarURL,function(html){
				p.$theadset.append(html);
				if($.isFunction(p.onContentLoaded))
					p.onContentLoaded(p.$theadset);
			});
		}else{
			p.$theadset.append(p.toolbarHTML);
			if($.isFunction(p.onContentLoaded))
				p.onContentLoaded(p.$theadset);
		}
	}
	/*if(p.headfixed){
		var headHTML = '<table id="table-fixed" style="position:fixed;top:0;width:'+($('#mainPanel .fuelux').width()-15)+'px;z-index:2000;display:none;" class="table table-bordered table-hover datagrid table-fixed-header"><thead>'+colHTML+'</thead></table>';		
		$('#mainPanel .fuelux').prepend(headHTML);
		$('section').scroll(function(){
			var sc = window.pageYOffset; 
			if(sc>p.$thead.find('tr').eq(1).position().top){
				$('#table-fixed').show();
			}else{
				$('#table-fixed').hide();
			}
		});
	}*/
	if(p.paramSearch!=null){
		p.$thead.find('#appendedInputButton').before('<select name="paramSearch"></select>');
		if(p.paramSearch.length>0){
			for(var i=0;i<p.paramSearch.length;i++){
				p.$thead.find('[name=paramSearch]').append('<option value="'+p.paramSearch[i].val+'">'+p.paramSearch[i].txt+'</option>');
			}
		}
	}
	$.extend(p,{
		updatePageDropdown: function(data){
			if(data==null){
				/*K.notification({
					title: 'Items no encontrados',
					text: 'No hay items para la b&uacute;squeda seleccionada!',
					type: 'error'
				});*/
				new PNotify({title:'Alerta',text:'No se han encontrado coincidencias para la busqueda',type:'error'});
				return false;
			}else{
				if(parseInt(data.total_items)==0){
					/*K.notification({
						title: 'Items no encontrados',
						text: 'No hay items para la b&uacute;squeda seleccionada!',
						type: 'error'
					});*/
					new PNotify({title:'Alerta',text:'No se han encontrado coincidencias para la busqueda',type:'error'});
					return false;
				}
			}	
			p.$pagedropdown.empty();
			var pageHTML = '';
			for (var i = 1; i <= data.total_pages; i++) {
				pageHTML += '<option value="'+i+'">' + i + '</option>';
			}
			p.$pagedropdown.html(pageHTML);
			p.$pagedropdown.find('option[value='+data.page+']').attr('selected','selected');
			p.$pageslabel.text(data.total_pages);
			p.$countlabel.html(data.total_items + ' ' + p.itemdescr);
			var ini = 1 + (data.page-1)*data.items_page;
			p.$startlabel.text(ini);
			p.$endlabel.text(ini+parseInt(data.total_page_items)-1);
		
			p.$prevpagebtn.removeAttr('disabled');
			p.$nextpagebtn.removeAttr('disabled');
			if(parseInt(data.page)==1){
				p.$prevpagebtn.attr('disabled',"disabled");
			}
			if(parseInt(data.page)==parseInt(data.total_pages)){
				p.$nextpagebtn.attr('disabled',"disabled");
			}
		},
		renderData: function(page){
			if($.isFunction(p.params)){
				p.params = p.params();
			}
			if($.isFunction(p.checkRender)){
				if(p.checkRender(p.$theadset)==false) return p;
			}		
			if(p.pagination==true){
				var paramSearch = '';
				if(p.paramSearch!=null){				
					paramSearch = p.$thead.find('[name=paramSearch] :selected').val();
				}
				$.extend(p.params,{
					text: p.$searchcontrol.find('input').val(),
					paramSearch: paramSearch,
					page_rows: p.$pagesize.find('option:selected').val(),
					page: (page) ? page : (p.$pagedropdown.find('option:selected').val()?p.$pagedropdown.find('option:selected').val():1)
				});
			}
			p.$tbody.empty();
			if($.isFunction(p.onLoading)){
				p.onLoading();
			}
			$.post(p.data,p.params,function(data){
				if(p.pagination==true){
					p.$footerchildren.css('visibility', function () {
						if(p.inObject!=null){
							return (data[p.inObject].paging.total_items > 0) ? 'visible' : 'hidden';
						}else{
							return (data.paging.total_items > 0) ? 'visible' : 'hidden';
						}
					});
					if(p.inObject!=null){
						p.updatePageDropdown(data[p.inObject].paging);
					}else{
						p.updatePageDropdown(data.paging);
					}
				}
				if(p.inObject!=null){
					if($.isFunction(p.fill)){
						if(data[p.inObject].paging!=null){
							if(data[p.inObject].items!=null)
								for(var i=0,j=data[p.inObject].items.length; i<j; i++){
									p.$tbody.append(p.fill(data[p.inObject].items[i],$('<tr class="item">')));
								}
						}else{
							if(data!=null)
								p.fill(data[p.inObject],p.$tbody);
						}
					}else p.load(data,p.$tbody);
				}else{
					if($.isFunction(p.fill)){
						if(data.paging!=null){
							if(data.items!=null)
								for(var i=0,j=data.items.length; i<j; i++){
									p.$tbody.append(p.fill(data.items[i],$('<tr class="item">')));
								}
						}else{
							if(data!=null)
								p.fill(data,p.$tbody);
						}
					}else p.load(data,p.$tbody);
				}	
				//K.resetModals();
				if($.isFunction(p.onComplete)){
					p.onComplete();
				}
				/**  Saving params in element p.$el */
				p.$el.data('grid_params',p.params);
			},'json');
		},
		reinit: function(newP){
			$.extend(p,newP);
			p.renderData();
		}
	});
	if(p.pagination==true){
		p.$prevpagebtn.click(function(){
			var val = parseInt(p.$pagedropdown.find('option:selected').val());
			p.$pagedropdown.find('option[value='+(val--)+']').attr('selected','selected');
			p.renderData(val);
		});
		p.$nextpagebtn.click(function(){
			var val = parseInt(p.$pagedropdown.find('option:selected').val());
			p.$pagedropdown.find('option:selected').removeAttr('selected');
			p.$pagedropdown.find('option[value='+(val++)+']').attr('selected','selected');
			p.renderData(val);
		});
		p.$pagesize.change(function(){ p.renderData(); });
		p.$pagedropdown.change(function(){ p.renderData(); });
	}else{
		p.$tfoot.remove();
	}
	if(p.search==true){
		p.$searchcontrol.find('input').keyup(function(e){
			if(e.keyCode == 13) p.$searchcontrol.find('button').click();
		});
		p.$searchcontrol.find('button').click(function(){
			p.renderData();
		});
	}else{
		p.$searchcontrol.remove();
	}
	p.renderData();
	p.$el.on("reloadGrid",function(){
		//var newP = p.$el.data('grid_params');
		//$.extend(p,newP);
		if(p.onReloadGrid!=null){
			if($.isFunction(p.onReloadGrid)){
				p.onReloadGrid();
			}
		}else{
			p.renderData();
		}
	});
	p.$el.find('[name=btnRefreshGrid]').click(function(){
		p.$el.trigger('reloadGrid');
	});
	//p.$tbody.css({height:$(window).height()-p.$thead.height()-p.$tfoot.height()-$('#titleBar').height()-$('#dock').height()-10});
	return p;
};
$.fn.tooltip = function(){
	$.noop();
};
$.fn.popover = function(){
	$.noop();
};
$.fn.alert = function(){
	$.noop();
};
$.fn.cookie = $.cookie = function(){
	$.noop();
};
$.fn.timeago = function(){
	$.noop();
};
