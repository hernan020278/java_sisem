var marTabAct = "";

function cargarPlugin(pagina) 
{
	/***************************************************************************
	 * CARGAR PLUGIN NECESARIOS PARA CADA PAGINA *
	 **************************************************************************/
	if (pagina === "index.php") {
		crearJavaScript("index.js");
	} else if (pagina === "proyecto.php") {
		eliminarJavaScript("index.js");
		crearStyleCss("tab_jquery.css");

		crearStyleCss("gallery.css");
		crearJavaScript("jquery.poshytip.min.js");

		crearStyleCss("tip-darkgray.css");

		crearStyleCss("jquery.fancybox.css");
		crearJavaScript("jquery.fancybox.js");

	} else if (pagina === "contacto.php") {
		eliminarJavaScript("index.js");
		crearStyleCss("msgBoxLight.css");
		crearJavaScript("jquery.msgBox.js");
	}
}
/***************************************************************************
 * CARGAR PLUGIN NECESARIOS PARA CADA PAGINA *
 **************************************************************************/

function setFormatoNumero()
{
	$(document).ready(function(){
	    $('.entero').numeric();
	    $('.decimal').numeric("."); 
	});
}

function iniciarPlugin(pagina) {
	
	setFormatoNumero();
	if (pagina == "index.php") {
		$('#slider')._TMS({
			banners : true,
			waitBannerAnimation : false,
			preset : 'diagonalFade',
			easing : 'easeOutQuad',
			pagination : true,
			duration : 400,
			slideshow : 8000,
			bannerShow : function(banner) {
				banner.css({
					marginRight : -500
				}).stop().animate({
					marginRight : 0
				}, 600)
			},
			bannerHide : function(banner) {
				banner.stop().animate({
					marginRight : -500
				}, 600)
			}
		});
	} else if (pagina == "proyecto.php") {
		eliminarJavaScript("index.js");

		// ///////////////////////////////////
		// ACTIVAMOS TABS SIMPLES CON JQUERY//
		// ///////////////////////////////////
		// hiding tab content except first one
		$(".tabContent").not(":first").hide();
		// adding Active class to first selected tab and show
		$("ul.tabs li:first").addClass("active").show();
		// Click event on tab
		$("ul.tabs li").click(function() {
			// Removing class of Active tab
			$("ul.tabs li.active").removeClass("active");
			// Adding Active class to Clicked tab
			$(this).addClass("active");
			// hiding all the tab contents
			$(".tabContent").hide();
			// showing the clicked tab's content using fading effect
			$($('a', this).attr("href")).fadeIn('slow');
			return false;
		});

		// Thumbs functions
		// ------------------------------------------------------ //

		function thumbsFunctions() {

			// Gallery over

			$('.gallery li a img').hover(function() {
				// on rollover
				$(this).stop().animate({
					opacity : "0.5"
				}, "fast");
			}, function() {
				// on out
				$(this).stop().animate({
					opacity : "1"
				}, "fast");
			});

			// tips

			$('.gallery a').poshytip({
				className : 'tip-darkgray',
				showTimeout : 1,
				alignTo : 'target',
				alignX : 'center',
				offsetY : -15,
				allowTipHover : false
			});

			// Change title type, overlay closing speed
			$(".fancybox").fancybox({
				helpers : {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

		}
		// init
		thumbsFunctions();
	} else if (pagina == "portafolio.php") {
		// // Change title type, overlay closing speed
		// $(".fancybox").fancybox({
		// helpers : {
		// title : {
		// type : 'outside'
		// },
		// overlay : {
		// speedOut : 0
		// }
		// }
		// });
	} else if (pagina == "contacto.php") {
		$('#mapaGoogle').html("<iframe width='468' height='292' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com.pe/maps/ms?msid=207351506732075378291.0004a40646b78b8ebd98d&amp;msa=0&amp;ie=UTF8&amp;t=h&amp;ll=-16.359901,-71.544836&amp;spn=0.006012,0.010021&amp;z=16&amp;output=embed'></iframe>");
	}
}

function actualizarItemMenu() 
{
//	$("#menu").children("li").removeAttr("id");
	$('.nav').each(function(index, input) 
	{
		$(this).removeClass("active");
	});
	
	$("#" + frm.menuIde.value).addClass("active");
}


function abrirPluginFacebbok(d, s, id) {
	// $("#pluginFacebook").children("div").remove();
	var elem = document.getElementById("facebook-jssdk");
	elem.parentNode.removeChild(elem);

	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id))
		return;
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
	fjs.parentNode.insertBefore(js, fjs);
}

function animarMarquee(modo) 
{
	if (!marTabAct || marTabAct == "") 
	{
		marTabAct = "martab1";
	}
	if (modo == "empezar") 
	{
		document.getElementById(marTabAct).start();
	} 
	else if (modo == "parar") 
	{
		document.getElementById(marTabAct).stop();
	} 
	else if (modo == "arriba") 
	{
		document.getElementById(marTabAct).direction = 'up';
		document.getElementById(marTabAct).start();
	} 
	else if (modo == "abajo") 
	{
		document.getElementById(marTabAct).direction = 'down';
		document.getElementById(marTabAct).start();
	} 
	else if (modo == "rapido") 
	{
		document.getElementById(marTabAct).scrollAmount = '20';
		document.getElementById(marTabAct).start();
	} 
	else if (modo == "lento") 
	{
		document.getElementById(marTabAct).scrollAmount = '1';
		document.getElementById(marTabAct).start();
	}
}

function procesarSuccessAjaxJQuery(datos, url)
{
	if (url == "portafolio_1.php" || url == "portafolio_2.php"
			|| url == "portafolio_3.php" || url == "portafolio_4.php") {
		$("#portafolio").hide().html(datos).fadeIn("slow"); // Le damos
		// efecto
		// $('#portafolio').html(datos);
		// $('h2').append('<a href="#top" class="gototop">Subir</a>');
		// linkInterno = $('a[href^="#"]');
		// linkInterno.on('click',function(e)
		// {
		// e.preventDefault();
		// var href = $(this).attr('href');
		// $(window).load(function() {
		// $("html, body").animate({ scrollTop: $(document).height() },
		// 1000);
		// });
		// $("html, body").animate({ scrollTop:
		// $(document).height()-$(window).height() });
		// $("html, body").animate({ scrollTop:
		// $("#piePagina").scrollTop() + 500}, 1000);
		// $("html, body").animate({ scrollTop: 500}, 1000);
		$("html, body").animate({
			scrollTop : $('#portafolio').offset().top
		}, 1000);
	} else if (url == "acerca_1.php" || url == "acerca_2.php"
			|| url == "acerca_3.php" || url == "acerca_4.php"
			|| url == "acerca_5.php") {
		$("#contenidoAcerca").hide().html(datos).fadeIn("slow"); // Le
		// damos
		// efecto
	} else if (url == "tours_1.php" || url == "tours_2.php"
			|| url == "tours_3.php" || url == "tours_4.php"
			|| url == "tours_5.php") {
		$("#contenidoTours").hide().html(datos).fadeIn("slow"); // Le
		// damos
		// efecto
	} else if (url == "proyecto.php") {
		$('#content').html(datos);
	} else {
		$('#content').html(datos);
	}
}

function msgBox(titulo, mensaje, tipo) 
{
	$('#resultado').html("<h1>"+mensaje+"</h1>").css({'position':'fixed','z-index':'300', 'top':'0px;'}).fadeIn();
	setTimeout(function() {  
		// Ocultamos la foto 1  
		$('#resultado').fadeOut();  
	}, 3000);  

//	$.msgBox({
//		title : titulo,
//		content : mensaje,
//		type : tipo,
//		showButtons : false,
//		opacity : 0.6,
//		autoClose : false
//	});
}

/*
 * Menu de navegacion
 */
function cmdUsuarioEvento(){}

function cmdOrdenCompraRepuestoEvento(){}

function cmdCompraMotoEvento(){}

function cmdCompraRepuestoEvento(){}

function cmdCuentaPagarEvento(){}

function cmdCuentaCobrarEvento(){}

function cmdGastosadmEvento(){}

function cmdTramiteEvento(){}

function cmdKardexMotoEvento(){}  

function cmdPerfilseguridadEvento(){}
