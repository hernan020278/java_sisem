package com.timer.accion;

import java.util.Map;

import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.timer.UpdateJob;

public class StatusPuridiomServicesShow extends Accion
{

	public Object executeTask(Object object) throws Exception
	{
		Map incomingRequest = (Map) object;
		Object result = null;
		String oid = (String) incomingRequest.get("organizationId");

		String status = "Stopped";
		String lastRun = "Not Date";

		try
		{
			UpdateJob updateJob = new UpdateJob(oid);
			updateJob.getXmlDocument();

			if (updateJob.getRunning() != null && updateJob.getRunning().equals("Y"))
			{
				status = "Running";
			}

			if (!updateJob.getScheduleLastRun().equals(""))
			{
				lastRun = updateJob.getScheduleLastRun();
			}

			incomingRequest.put("status", status);
			incomingRequest.put("lastRun", lastRun);

			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}

}
