package com.timer.accion;

import java.util.Map;

import com.comun.motor.Accion;
import com.comun.referencia.Estado;

public class TimerSetup extends Accion
{
	public Object executeTask(Object object) throws Exception
	{
		Map peticion = (Map)object;
		try
		{
			peticion.put("SendQueue_status", "00");
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
		}
		return peticion;
	}

}
