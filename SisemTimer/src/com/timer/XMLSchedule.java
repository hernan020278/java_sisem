package com.timer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.naming.spi.DirectoryManager;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;

public class XMLSchedule {
	private Document xmlDocument = null;
	private File scheduleFile = null;
	private String organizationId = "";
	private List<Element> jobs = null;
	
	public XMLSchedule( String _organizationId)
	{
		String fileName = "schedule.xml";
		this.organizationId = _organizationId;
		
		
		try
		{
			System.out.println("loading schedule: " + fileName + " oid = " + this.getOrganizationId());
			Log.debug(this, fileName);

			this.scheduleFile = Util.getOidFile(this.filepath(this.getOrganizationId()) + fileName, this.getOrganizationId());

			System.out.println("Schedule found: " + this.getScheduleFile().getPath());

			if (this.getScheduleFile().exists())
			{
				DOMBuilder docBuilder = new DOMBuilder();
				this.xmlDocument = docBuilder.build(this.getScheduleFile());
				
				jobs = this.xmlDocument.getRootElement().getChildren("job");
				
				
			}
			else
			{
				Log.error(this, "Schedule not found: " + this.getScheduleFile().getPath());
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	
	public synchronized void output() {
		try {
			XMLOutputter outputter = new XMLOutputter("", true);
			outputter.setExpandEmptyElements(true);
			outputter.setTextNormalize(false);
			outputter.setNewlines(false);

			outputter.output(this.getXmlDocument(), new FileOutputStream(this.getScheduleFile()));
//			outputter.output(this.getXmlDocument(), System.out);

		} catch (IOException ie) {
			Log.error(this, ie.toString());
		}
	}

	public void change(int i){
		if(jobs.get(i).getChildText("enabled")!=null)
			if(jobs.get(i).getChildText("enabled").equals("true"))
			{
				jobs.get(i).removeChild("enabled");
				Element etiquetaNueva = new Element("enabled");  
				etiquetaNueva.setText("false");  
				jobs.get(i).addContent(etiquetaNueva);
				this.output();
			}
			else
			{
				jobs.get(i).removeChild("enabled");
				Element etiquetaNueva = new Element("enabled");  
				etiquetaNueva.setText("true");  
				jobs.get(i).addContent(etiquetaNueva);
				this.output();
			}
		else
		{
			Element etiquetaNueva = new Element("enabled");  
			etiquetaNueva.setText("false");  
			jobs.get(i).addContent(etiquetaNueva);
			this.output();
		}
	}
	
	public void change(String a)
	{
		int i = Integer.parseInt(a);
		if(jobs.get(i).getChildText("enabled")!=null)
			if(jobs.get(i).getChildText("enabled").equals("true"))
			{
				jobs.get(i).removeChild("enabled");
				Element etiquetaNueva = new Element("enabled");  
				etiquetaNueva.setText("false");  
				jobs.get(i).addContent(etiquetaNueva);
				this.output();
			}
			else
			{
				jobs.get(i).removeChild("enabled");
				Element etiquetaNueva = new Element("enabled");  
				etiquetaNueva.setText("true");  
				jobs.get(i).addContent(etiquetaNueva);
				this.output();
			}
		else
		{
			Element etiquetaNueva = new Element("enabled");  
			etiquetaNueva.setText("false");  
			jobs.get(i).addContent(etiquetaNueva);
			this.output();
		}
	}
	
	public void Disable(int i){
		if(jobs.get(i).getChildText("enabled")!=null)
			jobs.get(i).removeChild("enabled");
	    Element etiquetaNueva = new Element("enabled");  
	    etiquetaNueva.setText("false");  
	    jobs.get(i).addContent(etiquetaNueva);
	    this.output();
	}
	
	public void Disable(String a){
		int i = Integer.parseInt(a);
		if(jobs.get(i).getChildText("enabled")!=null)
			jobs.get(i).removeChild("enabled");
	    Element etiquetaNueva = new Element("enabled");  
	    etiquetaNueva.setText("false");  
	    jobs.get(i).addContent(etiquetaNueva);
	    this.output();
	}
	
	public void Enable(int i){
		if(jobs.get(i).getChildText("enabled")!=null)
			jobs.get(i).removeChild("enabled");
	    Element etiquetaNueva = new Element("enabled");  
	    etiquetaNueva.setText("true");  
	    jobs.get(i).addContent(etiquetaNueva);
	    this.output();
	}
	
	public String getclass(int i){
		return jobs.get(i).getChildText("class");
	}
	public String getname(int i){
		return jobs.get(i).getChildText("name");
	}
	public String getdesc(int i){
		return jobs.get(i).getChildText("description");
	}
	public String getTime(int i){
		return jobs.get(i).getChildText("time");
	}
	public String getEnabled(int i)	{
		return jobs.get(i).getChildText("enabled");
	}
	/*public String getLast(int i){
		return jobs.get(i).getChildText("last-run");
	}*/
	public String getNext(int i){
		return jobs.get(i).getChildText("next-run");
	}
	public List getJobs(){
		return jobs;
	}
	public int jobsLenght()	{
		return jobs.size();
	}
	public File getScheduleFile() {
		return scheduleFile;
	}
	
	public String getOrganizationId() {
		return organizationId;
	}
	
	public String filepath(String organizationId)
	{
		return AplicacionGeneral.getInstance().obtenerRutaScheduleXml();
		//return DictionaryManager.getInstance("host", organizationId).getProperty("schedules");
	}
	
	public Document getXmlDocument() {
		return xmlDocument;
	}
}
