package com.timer;

import java.util.Calendar;
import java.util.TimeZone;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.Dates;

public class ScheduleStart
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if(args == null || args.length < 1)
		{
			args = new String[1];
			//args[0] = "her11p";
			//args[0] = "bri10p";
			//args[0] = "ttr09p";
			//args[0] = "ama10p";
			//args[0] = "tdc10p";
			args[0] = "SISEM";
		}
		if(args != null && args.length > 0)
		{
			Runtime runtime = Runtime.getRuntime();
			long heapMaxSize = runtime.maxMemory() / 1024;
			System.err.println("Allocated memory: " + heapMaxSize) ;
			System.err.println("free memory: " + runtime.freeMemory()/ 1024) ;

			for (int i = 0; i < args.length; i++)
			{
				System.out.println("Schedule starts at " + Calendar.getInstance().getTime() + "for oid: " + args[i]);
				OrganizacionGeneral.getInstance(args[i]);
				System.err.println("Initial used memory: " + (runtime.totalMemory()  - runtime.freeMemory())/ 1024) ;

				ScheduleManager.getInstance().start(OrganizacionGeneral.getOrgcodigo());
				System.out.println("Schedule done: " + Dates.getNow("", "") + " " + TimeZone.getDefault().getID());
				System.err.println("Used memory: " + (runtime.totalMemory()  - runtime.freeMemory())/ 1024) ;
				System.err.println("free memory: " + runtime.freeMemory()/ 1024) ;
				runtime.gc();
				runtime.gc();
				System.err.println("after gc: " + runtime.freeMemory()/ 1024) ;
				System.err.println("after gc used memory: " + (runtime.totalMemory()  - runtime.freeMemory())/ 1024) ;
			}
		}
		System.exit(0);
	}
}
