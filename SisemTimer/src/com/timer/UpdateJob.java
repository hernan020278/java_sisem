/**
 *
 */
package com.timer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jdom.CDATA;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Dates;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;

/**
 * @author JOHNNY
 *
 */
public class UpdateJob {

	private Document xmlDocument = null;

	private File scheduleFile = null;

	private String organizationId = "";

	public UpdateJob(String _organizationId)
	{
		String fileName = "schedule.xml";
		this.organizationId = _organizationId;

		try
		{
			System.out.println("loading schedule: " + fileName + " oid = " + this.getOrganizationId());
			Log.debug(this, fileName);

			this.scheduleFile = Util.getOidFile(this.filepath(this.getOrganizationId()) + fileName, this.getOrganizationId());

			System.out.println("Schedule found: " + this.getScheduleFile().getPath());

			if (this.getScheduleFile().exists())
			{
				DOMBuilder docBuilder = new DOMBuilder();
				this.xmlDocument = docBuilder.build(this.getScheduleFile());
			}
			else
			{
				Log.error(this, "Schedule not found: " + this.getScheduleFile().getPath());
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}

	public String filepath(String organizationId)
	{
		return AplicacionGeneral.getInstance().obtenerRutaScheduleXml();
		//return DictionaryManager.getInstance("host", organizationId).getProperty("schedules");
	}

	public void changeJobsLastRuntoNextRun(){
		List jobList = (List)this.getXmlDocument().getRootElement().getChildren("job");
		int size = jobList.size();

		for(int i=0; i<size; i++){
			Element jobElement = (Element)jobList.get(i);
			Element nextRunElement = jobElement.getChild("next-run");

			if(nextRunElement != null)
			{
				break;
			}else{
				Element lastRunElement = jobElement.getChild("last-run");
				if(lastRunElement != null){
					lastRunElement.setName("next-run");
				}
			}
		}
	}


	public synchronized void setNextRun(int jobOrder) throws Exception
	{
		Element jobElement = (Element) this.getXmlDocument().getRootElement().getChildren("job").get(jobOrder - 1);
		Element nextRunElement = jobElement.getChild("next-run");
		Element time = jobElement.getChild("time");

		CDATA cdata = new CDATA("");
		List cdataList;
		Calendar calendar = Calendar.getInstance();
		String nextRun = "";
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss z");

		//2. Use TIME attribute in the same manner as the reports scheduling::
		Date previousDate = new Date();
		String previousHour = " 00:00:00";
		String mask = "yyyy/MM/dd";
		String strTime = "";
		if(time != null){
			strTime = time.getTextNormalize();
		}

		boolean setNextTime = false;
		boolean addNextRun = false;

		if (nextRunElement != null)
		{
			nextRun = nextRunElement.getTextNormalize();
			if(!Util.isEmpty(nextRun) && (strTime.equalsIgnoreCase("D") || strTime.equalsIgnoreCase("W") || strTime.equalsIgnoreCase("M")))
			{
				String dateYMD = nextRunElement.getTextNormalize().substring(0, mask.length());
				previousDate = Dates.getDate(dateYMD, mask);
				if (nextRun.length() > mask.length()) {
					previousHour = nextRunElement.getTextNormalize().substring(mask.length(), nextRun.length());
				}
				else
				{
					previousHour = sdf.format(Calendar.getInstance().getTime());
				}
			}else{
				previousDate = calendar.getTime();
				previousHour = sdf.format(Calendar.getInstance().getTime());
			}
		}
		else
		{
			nextRunElement = new Element("next-run");
			previousDate = calendar.getTime();
			previousHour = sdf.format(Calendar.getInstance().getTime());
			jobElement.addContent(nextRunElement);
		}

		boolean executeEveryRun = false;

		if(!Util.isEmpty(strTime)){
			//a. Default value is zero [0] = execute every run.
			if(strTime.equalsIgnoreCase("0")){
				executeEveryRun = true;
			}//b. [nnn] = run every nnn minutes
			else if(isNumber(strTime)){
				int numMinutes = Integer.parseInt(strTime);
				previousDate = calendar.getTime();
				cdata = new CDATA(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(Dates.addMinutes(previousDate, numMinutes)));
			}//c. [D] = run daily (add 24 hours to next-run attribute)
			else if(strTime.equalsIgnoreCase("D")){
				cdata = new CDATA(new SimpleDateFormat("yyyy/MM/dd").format(Dates.add(previousDate, 1)) + " "+ sdf.format(sdf.parse(previousHour.trim())));
			}//d. [W] = run weekly (add 7 days to next-run attribute]
			else if(strTime.equalsIgnoreCase("W")){
				cdata = new CDATA(new SimpleDateFormat("yyyy/MM/dd").format(Dates.addWeeks(previousDate, 1)) + " "+ sdf.format(sdf.parse(previousHour.trim())));
			}//e. [M] = runs monthly (adds a month to next-run attribute)
			else if(strTime.equalsIgnoreCase("M")){
				cdata = new CDATA(new SimpleDateFormat("yyyy/MM/dd").format(Dates.addMonths(previousDate, 1)) + " "+ sdf.format(sdf.parse(previousHour.trim())));
			}//If any of other options = execute every run.
			else{
				executeEveryRun = true;
				List dataTime = new ArrayList();
				dataTime.add("0");
				time.setContent(dataTime);
			}
		}else{
			executeEveryRun = true;
			if(time == null)
			{
				time =  new Element("time");
				jobElement.addContent(time);
			}
			List dataTime = new ArrayList();
			dataTime.add("0");
			time.setContent(dataTime);
		}

		cdataList = new ArrayList();
		if(executeEveryRun){
			cdataList.add("");
		}else{
			cdataList.add(cdata);
		}
		nextRunElement.setContent(cdataList);
	}

	public synchronized void setLastRun(int jobOrder) throws Exception
	{
		Element jobElement = (Element) this.getXmlDocument().getRootElement().getChildren("job").get(jobOrder - 1);
		Element lastRunElement = jobElement.getChild("last-run");

		CDATA cdata;
		List cdataList;
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss z");
		String hour = " 00:00:00";
		String lastRun = "";
		String mask = "yyyy/MM/dd";

		if (lastRunElement != null)
		{
			lastRun = lastRunElement.getTextNormalize();
			if(Util.isEmpty(lastRun))
			{
				return;
			}

			if (lastRun.length() > mask.length()) {
				hour = lastRunElement.getTextNormalize().substring(mask.length(), lastRun.length());
			}
			else
			{
				hour = sdf.format(Calendar.getInstance().getTime());
			}

			cdata = new CDATA(new SimpleDateFormat("yyyy/MM/dd").format(Dates.add(calendar.getTime(), 0)) + " "+ sdf.format(sdf.parse(hour.trim())));
			cdataList = new ArrayList();
			cdataList.add(cdata);
			lastRunElement.setContent(cdataList);
		}
		else
		{
			hour = sdf.format(Calendar.getInstance().getTime());
			cdata = new CDATA(new SimpleDateFormat("yyyy/MM/dd").format(Dates.add(calendar.getTime(), 0)) + " "+ sdf.format(sdf.parse(hour.trim())));
			cdataList = new ArrayList();
			cdataList.add(cdata);
			lastRunElement = new Element("last-run");
			lastRunElement.setContent(cdataList);

			jobElement.addContent(lastRunElement);
		}
	}

	private boolean isNumber (String str){
		if(Util.isEmpty(str))
		{
			return false;
		}else{
			for (int i = 0; i < str.length(); i++) {
			//If we find a non-digit character we return false.
				if (!Character.isDigit(str.charAt(i)))
				{
				      	return false;
				}
		    }
			return true;
		}
	}

	public synchronized void setScheduleLastRun()
	{
		List elements = this.getXmlDocument().getRootElement().getChildren("last-run");
		Element lastRunElement = null;
		if(elements.size() > 0)
		{
			lastRunElement = (Element) elements.get(0);
		}
		else {
			lastRunElement = new Element("last-run");
			elements.add(lastRunElement);
		}

		CDATA cdata = null;
		List cdataList;
		Calendar calendar = Calendar.getInstance();

		if (lastRunElement != null) {
			try
			{
				cdata = new CDATA(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(Dates.add(calendar.getTime(), 0)));
			} catch(Exception exception)
			{
				Log.error(this, "It is not possible parse the specified hour in: sdf.parse(hour.trim())");
			}
			cdataList = new ArrayList();
			cdataList.add(cdata);
			lastRunElement.setContent(cdataList);
		}
	}

	public String getScheduleLastRun()
	{
		String lastRunElementText = "";

		if(this.getXmlDocument().getRootElement().getChild("last-run") != null){
			Element lastRunElement = (Element) this.getXmlDocument().getRootElement().getChild("last-run");
			lastRunElementText = lastRunElement.getTextNormalize();
		}

		return lastRunElementText;
	}

	public synchronized void setRunning(String status)
	{
		List elements = this.getXmlDocument().getRootElement().getChildren("running");
		Element runningElement = null;

		if(elements.size() > 0)
		{
			runningElement = (Element) elements.get(0);
		} else
		{
			runningElement = new Element("running");
			elements.add(runningElement);
		}
		runningElement.setText(status);
	}

	public String getRunning()
	{
		List elements = this.getXmlDocument().getRootElement().getChildren("running");
		Element runningElement = null;
		String value = "";

		if(elements.size() > 0)
		{
			runningElement = (Element) elements.get(0);
			value = runningElement.getText();
		}

		return value;
	}

	public synchronized void output() {
		try {
			XMLOutputter outputter = new XMLOutputter("", true);
			outputter.setExpandEmptyElements(true);
			outputter.setTextNormalize(false);
			outputter.setNewlines(false);

			outputter.output(this.getXmlDocument(), new FileOutputStream(this.getScheduleFile()));
//			outputter.output(this.getXmlDocument(), System.out);

		} catch (IOException ie) {
			Log.error(this, ie.toString());
		}
	}

	/**
	 * @return the organizationId
	 */
	public String getOrganizationId() {
		return organizationId;
	}

	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * @return the scheduleFile
	 */
	public File getScheduleFile() {
		return scheduleFile;
	}

	/**
	 * @param scheduleFile the scheduleFile to set
	 */
	public void setScheduleFile(File scheduleFile) {
		this.scheduleFile = scheduleFile;
	}

	/**
	 * @return the xmlDocument
	 */
	public Document getXmlDocument() {
		return xmlDocument;
	}

	/**
	 * @param xmlDocument the xmlDocument to set
	 */
	public void setXmlDocument(Document xmlDocument) {
		this.xmlDocument = xmlDocument;
	}

}
