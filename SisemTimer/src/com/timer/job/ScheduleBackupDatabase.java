package com.timer.job;

import java.awt.TrayIcon;
import java.sql.Time;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ToolTipManager;

import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.PropiedadesExterna;
import com.comun.utilidad.Util;

public class ScheduleBackupDatabase extends TimerTask {

	private static long FREQ_ONCE_PER_DAY = 1000 * 60 * 60;
	private static PropiedadesExterna testProp;
//	private static int FREQ_ONE_DAY = 1;
//	private int freqHour = 21;
//	private int freqMinute = 18;
	private Timer timer;
	public ScheduleBackupDatabase() {
		if(testProp==null){testProp = new PropiedadesExterna("config");}
		String intervaloDia = testProp.getServidor("backup-intervaloDia");
		FREQ_ONCE_PER_DAY = FREQ_ONCE_PER_DAY * Integer.parseInt(intervaloDia);

		timer = new Timer();
		//timer.scheduleAtFixedRate(this, getScheduleTime(23, 01), FREQ_ONCE_PER_DAY);
		timer.scheduleAtFixedRate(this, getScheduleTime(), FREQ_ONCE_PER_DAY);
	}
	
	public ScheduleBackupDatabase(int hora, int minuto, int intervaloDias) {
		if(testProp==null){testProp = new PropiedadesExterna("config");}
		testProp.setPropiedades("backup-hora", hora+":"+minuto+":00");
		testProp.setPropiedades("backup-intervaloDia", String.valueOf(intervaloDias));
		
		String intervaloDia = testProp.getServidor("backup-intervaloDia");
		FREQ_ONCE_PER_DAY = FREQ_ONCE_PER_DAY * Integer.parseInt(intervaloDia);
		
		timer = new Timer();
		//timer.scheduleAtFixedRate(this, getScheduleTime(23, 01), FREQ_ONCE_PER_DAY);		
		timer.scheduleAtFixedRate(this, getScheduleTime(), FREQ_ONCE_PER_DAY);
	}

	public void cancelSchedule(){
		timer.cancel();
		timer.purge();
	}
	
	public void changeSchedule(int hora, int minuto){
		cancelSchedule();
//		ScheduleBackupDatabase scheduleBackup = new ScheduleBackupDatabase(6,23);
	}
	/**
	 * Implements TimerTask's abstract run method.
	 */
	@Override
	public void run() {
		ToolTipManager.sharedInstance().setDismissDelay(60000);
		TimerTrayIcon.trayIcon.displayMessage("Backup programado","Se ha iniciado el backup de la base de de datos", TrayIcon.MessageType.INFO);
		Util.getInst().ejecutarArchivoDesktop("cmd.exe /K start "+AplicacionGeneral.getInstance().obtenerRutaSistema()+"bin\\generar_backup_by_ftp.bat");
	}

	private Date getScheduleTime() {

		String parStrTime = testProp.getServidor("backup-hora");
		Time hora = Util.getInst().fromStrdateToTime(parStrTime);
		Date dateTime = new java.util.Date();
		dateTime.setHours(hora.getHours());
		dateTime.setMinutes(hora.getMinutes());
		// Calendar tomorrow = new GregorianCalendar();
		// //tomorrow.add(Calendar.DATE, FREQ_ONE_DAY);
		// Calendar result = new GregorianCalendar(
		// tomorrow.get(Calendar.YEAR),
		// tomorrow.get(Calendar.MONTH),
		// tomorrow.get(Calendar.DATE),
		// freqHour,
		// freqMinute
		// );
		return dateTime;
	}

	/** Construct and use a TimerTask and Timer. */
	public static void main(String[] arguments) {
		ScheduleBackupDatabase scheduleBackup = new ScheduleBackupDatabase(6,43,1);
//		TimerTask fetchMail = new ScheduleBackupDatabase();
//		// perform the task once a day at 4 a.m., starting tomorrow morning
//		// (other styles are possible as well)
//		Timer timer = new Timer();
//		timer.scheduleAtFixedRate(fetchMail, getScheduleTime(18,0), FREQ_ONCE_PER_DAY);
	}
}
