package com.timer.job;

import java.awt.AWTException;
import java.awt.CheckboxMenuItem;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.sql.Time;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EtchedBorder;

import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.PropiedadesExterna;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.JTextFieldFormatoEntero;
import com.comun.utilidad.swing.JTextFieldFormatoHora;

public class TimerTrayIcon {
	
	private static ScheduleBackupDatabase scheduleBackup;
	private static JTextFieldFormatoEntero txtIntervaloDia;
	private static JTextFieldFormatoHora txtHora;
	private static JLabel etiFecha;
	private static JLabel etiTitulo;
	private static JLabel etiHora;
	private static JFrame formSchedule;
	private static JButton cmdGuardar;
	private static JButton cmdCerrar;
	private static PopupMenu popup;
	public static TrayIcon trayIcon;
	public static SystemTray tray;
	private static ServerSocket SERVER_SOCKET_TRAY;
	
	private static void createAndShowGUI() {

		// Check the SystemTray support
		if(!SystemTray.isSupported()) {
			System.out.println("SystemTray no es soportado");
			return;
		}
	
		formSchedule = new JFrame();
		formSchedule.setTitle("Programar Backup");
		formSchedule.setLayout(new GridLayout(3, 2));
		formSchedule.setDefaultCloseOperation(formSchedule.EXIT_ON_CLOSE);
		
		etiTitulo = new JLabel("Programar Backup");
		etiTitulo.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		etiTitulo.setForeground(new Color(160, 82, 45));
		etiTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		etiTitulo.setFont(new Font("Verdana", Font.PLAIN, 20));
		etiTitulo.setBounds(10, 9, 240, 46);
		formSchedule.getContentPane().add(etiTitulo);
		
		etiFecha = new JLabel("Intervalo Horas");
		etiFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiFecha.setBounds(10, 71, 94, 25);
		formSchedule.getContentPane().add(etiFecha);
		
		etiHora = new JLabel("Ingrese Hora");
		etiHora.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiHora.setBounds(10, 100, 94, 25);
		formSchedule.getContentPane().add(etiHora);
		
//		txtEditFecha = new JTextEditorFecha();
//		txtEditFecha.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
//		txtEditFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
//
//		cmbFecha = new JDateChooser(txtEditFecha);
//		cmbFecha.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
//		cmbFecha.setDateFormatString("dd/MM/yyyy");
//		cmbFecha.setDate(new Date());
		
		txtIntervaloDia = new JTextFieldFormatoEntero(2);
		txtIntervaloDia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtIntervaloDia.setValue(1);
		txtIntervaloDia.setBounds(114, 71, 136, 25);
		formSchedule.getContentPane().add(txtIntervaloDia);
		
		txtHora = new JTextFieldFormatoHora();
		txtHora.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtHora.setBounds(114, 100, 136, 25);
		formSchedule.getContentPane().add(txtHora);
		
		cmdGuardar = new JButton("Programar");
		cmdGuardar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdGuardar.setBounds(10, 136, 115, 40);
		formSchedule.getContentPane().add(cmdGuardar);
		
		cmdCerrar = new JButton("Cerrar");
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.setBounds(130, 136, 115, 40);
		formSchedule.getContentPane().add(cmdCerrar);

		formSchedule.getContentPane().setLayout(null);
		formSchedule.setSize(new Dimension(268, 215));
		formSchedule.setLocationRelativeTo(null);
		formSchedule.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		popup = new PopupMenu();
		trayIcon = new TrayIcon(createImage("Icono.gif", "tray icon"));
		tray = SystemTray.getSystemTray();
		// Create a popup menu components
		MenuItem aboutItem = new MenuItem("Acerca de..");
		MenuItem setSchedule = new MenuItem("Programar Backup");
		CheckboxMenuItem chkAutoSize = new CheckboxMenuItem("Set auto size");
		CheckboxMenuItem chkToolTip = new CheckboxMenuItem("Set tooltip");
		Menu displayMenu = new Menu("Display");
		MenuItem errorItem = new MenuItem("Error");
		MenuItem warningItem = new MenuItem("Warning");
		MenuItem infoItem = new MenuItem("Info");
		MenuItem noneItem = new MenuItem("None");
		displayMenu.add(errorItem);
		displayMenu.add(warningItem);
		displayMenu.add(infoItem);
		displayMenu.add(noneItem);
		MenuItem exitItem = new MenuItem("Exit");
		// Add components to popup menu
		popup.add(aboutItem);
//		popup.addSeparator();
//		popup.add(chkAutoSize);
//		popup.add(chkToolTip);
		popup.addSeparator();
		popup.add(setSchedule);
		popup.addSeparator();
//		popup.add(displayMenu);
		popup.add(exitItem);
		trayIcon.setPopupMenu(popup);
		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			System.out.println("TrayIcon could not be added.");
			return;
		}
		
		cmdGuardar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Time hora = Util.getInst().fromStrdateToTime(txtHora.getText());
				int intervaloDia = (Integer)txtIntervaloDia.getValue();
				if(scheduleBackup!=null){scheduleBackup.cancelSchedule();}
				scheduleBackup = new ScheduleBackupDatabase(hora.getHours(), hora.getMinutes(), intervaloDia);
				formSchedule.setVisible(false);
			}
		});
		
		cmdCerrar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				formSchedule.setVisible(false);
			}
		});

		trayIcon.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				JOptionPane.showMessageDialog(null,
						"This dialog box is run from System Tray");
			}
		});
		aboutItem.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				JOptionPane.showMessageDialog(null,
						"This dialog box is run from the About menu item");
			}
		});
		setSchedule.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				PropiedadesExterna testProp = new PropiedadesExterna("config");
				String intervaloDia = testProp.getServidor("backup-intervaloDia");
				String parStrTime = testProp.getServidor("backup-hora");
				Time hora = Util.getInst().fromStrdateToTime(parStrTime);
				
				txtIntervaloDia.setValue(Integer.parseInt(intervaloDia));
				txtHora.setText(hora.toString());
				
				formSchedule.setVisible(true);
			}
		});
		
		chkAutoSize.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent e) {

				int chkAutoSizeId = e.getStateChange();
				if(chkAutoSizeId == ItemEvent.SELECTED) {
					trayIcon.setImageAutoSize(true);
				} else {
					trayIcon.setImageAutoSize(false);
				}
			}
		});
		chkToolTip.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent e) {

				int chkToolTipId = e.getStateChange();
				if(chkToolTipId == ItemEvent.SELECTED) {
					trayIcon.setToolTip("Sun TrayIcon");
				} else {
					trayIcon.setToolTip(null);
				}
			}
		});
		ActionListener listener = new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				MenuItem item = (MenuItem) e.getSource();
				ToolTipManager.sharedInstance().setDismissDelay(12000);
				// TrayIcon.MessageType type = null;
				System.out.println(item.getLabel());
				if("Error".equals(item.getLabel())) {
					trayIcon.displayMessage("Sun TrayIcon Demo","This is an error message", TrayIcon.MessageType.ERROR);
				} else if("Warning".equals(item.getLabel())) {
					trayIcon.displayMessage("Sun TrayIcon Demo","This is a warning message", TrayIcon.MessageType.WARNING);
				} else if("Info".equals(item.getLabel())) {
					trayIcon.displayMessage("Sun TrayIcon Demo","This is an info message", TrayIcon.MessageType.INFO);
				} else if("None".equals(item.getLabel())) {
					trayIcon.displayMessage("Sun TrayIcon Demo","This is an ordinary message", TrayIcon.MessageType.NONE);
				}
			}
		};
		errorItem.addActionListener(listener);
		warningItem.addActionListener(listener);
		infoItem.addActionListener(listener);
		noneItem.addActionListener(listener);
		exitItem.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				tray.remove(trayIcon);
				System.exit(0);
			}
		});
		
		scheduleBackup = new ScheduleBackupDatabase();
	}

	// Obtain the image URL
	protected static Image createImage(String img, String description) {

//		URL imageURL = TimerTrayIcon.class.getResource(img);
//		System.err.println(imageURL);
//		if(imageURL == null) {
//			System.err.println("Resource not found: " + img);
//			return null;
//		} else {
//			 //return (new ImageIcon(imageURL, description)).getImage();
			return AplicacionGeneral.getInstance().obtenerImagen("backup.png").getImage();
//		}
	}

	public void ejecutar(){
		/* Use an appropriate Look and Feel */
		try {
			UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
		} catch (UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		UIManager.put("swing.boldMetal", Boolean.FALSE);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					SERVER_SOCKET_TRAY = new ServerSocket(60000);
					createAndShowGUI();
					ToolTipManager.sharedInstance().setDismissDelay(12000);
					trayIcon.displayMessage("Cronograma de Backup","Se ha iniciado el cronograma de backup", TrayIcon.MessageType.INFO);
				} catch (IOException e) {
					System.out.println("No se puede Ejecutar el sistema backup otra ves!!!");
				}					
			}
		});		
	}
	
	public static void main(String[] args) {
		TimerTrayIcon timerTrayIcon = new TimerTrayIcon();
		timerTrayIcon.ejecutar();

	}
}
