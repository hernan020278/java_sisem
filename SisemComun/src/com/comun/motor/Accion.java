/*
 * Created on Aug 1, 2003
 */
package com.comun.motor;

import java.util.Map;

import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;

public class Accion implements AccionListener {

	protected boolean sincrono = true;

	protected String tareaNombre = "";
	protected String tareaArchivo = "";
	protected int afectados = 0;

	protected int estado = Estado.READY;

	public Accion() {
		setTareaArchivo(this.getClass().getName());

		String nombreArchivo = "ArchivoAccion: " + this.getClass().getSimpleName();
		String repeat = "";
		for (int i=0; i< nombreArchivo.length(); i++) {
			repeat += "_";
		}
		if (!(this.getClass().getSimpleName().equalsIgnoreCase("ObtenerEntidad")
				|| this.getClass().getSimpleName().equalsIgnoreCase("ObtenerListaEntidad")
				|| this.getClass().getSimpleName().equalsIgnoreCase("GenerarBrowse")
				|| this.getClass().getSimpleName().equalsIgnoreCase("GenerateBrowseFilter")
				|| this.getClass().getSimpleName().equalsIgnoreCase("BrowseRetrieve")
				|| this.getClass().getSimpleName().equalsIgnoreCase("BrowseSetup")
				|| this.getClass().getSimpleName().equalsIgnoreCase("SetBrowseInCache"))
		){
			System.out.println(repeat);
			System.out.println(nombreArchivo);
		}
	}

	@Override
	public Object ejecutarAccion(Map peticion) throws Exception {
		Object returnObject = null;
		try {
		}
		catch (Exception e) {
			throw e;
		}
		return returnObject;
	}

	public String getTareaNombre() {
		return tareaNombre;
	}

	public void setTareaNombre(String tareaNombre) {
		this.tareaNombre = tareaNombre;
	}

	public boolean isSincrono() {
		return sincrono;
	}

	public void setSincrono(boolean sincrono) {
		this.sincrono = sincrono;
	}

	public void setReglaArchivo(String reglaArchivo) {
		reglaArchivo = reglaArchivo;
	}

	public String getTareaObjeto() {
		return tareaArchivo;
	}

	public void setTareaObjeto(String tareaArchivo) {
		this.tareaArchivo = tareaArchivo;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getTareaArchivo() {
		return tareaArchivo;
	}

	public void setTareaArchivo(String tareaArchivo) {
		this.tareaArchivo = tareaArchivo;
	}
	
	public void msgBox(String titulo, String valor, String tipo)
	{
		Auxiliar aux = Auxiliar.getInstance();
		aux.msg.setMostrar(true);
		aux.msg.setTipo(tipo);
		aux.msg.setTitulo(titulo);
		aux.msg.setValor(valor);
		
	}
}
