package com.comun.motor;

import reactor.core.publisher.Mono;

import java.util.Map;

public interface IAccionListener {

  Mono<Map> ejecutarAccion(Map peticion);

}
