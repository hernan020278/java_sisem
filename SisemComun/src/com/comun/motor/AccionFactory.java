/*
 * Created on Jul 15, 2003 
 */
package com.comun.motor;


public class AccionFactory
{

	private static AccionFactory INSTANCE;
	private AccionListener accion = null;

	public static AccionFactory getInstance()
	{
		if (INSTANCE == null)
		{

			return new AccionFactory();
		}
		return INSTANCE;
	}
}
