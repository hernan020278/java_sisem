/*
 * Created on Aug 1, 2003
 */
package com.comun.motor;

import java.util.Map;

public interface AccionListener {

	public String getTareaNombre();

	public int getEstado();

	public void setEstado(int estado);

	public Object ejecutarAccion(Map peticion) throws Exception;
}
