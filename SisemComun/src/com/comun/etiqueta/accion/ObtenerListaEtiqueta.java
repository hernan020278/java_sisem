package com.comun.etiqueta.accion;

import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.utilidad.Auxiliar;

public class ObtenerListaEtiqueta extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			Auxiliar aux = Auxiliar.getInstance();
			if (!peticion.containsKey("consulta"))
			{
				if (peticion.containsKey("Etiqueta_campo") && peticion.containsKey("Etiqueta_idioma"))
				{
					peticion.put("consulta", "select * from etiqueta where idioma='" + (String) peticion.get("idioma") + "' and campo='" + (String) peticion.get("Etiqueta_campo") + "'");
				}
				else if (peticion.containsKey("Etiqueta_idioma"))
				{
					peticion.put("consulta", "select * from etiqueta where idioma='" + (String) peticion.get("idioma") + "'");
				}
			}
			peticion.put("respuesta", Respuesta.LISTAENTIDAD);
			peticion.put("entidad", aux.eti);

			List listResult = dbc.ejecutarConsulta(peticion);
			result = listResult;

			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}
