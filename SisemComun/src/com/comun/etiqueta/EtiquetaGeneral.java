package com.comun.etiqueta;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.comun.entidad.Etiqueta;
import com.comun.etiqueta.accion.ObtenerListaEtiqueta;
import com.comun.utilidad.Util;

/**
 * 
 * @author Alexander
 * 
 */
public class EtiquetaGeneral {

	protected static EtiquetaGeneral instance = null;
	protected HashMap<String, EtiquetaDiccionario> bundleList = null;

	/**
	 *
	 */
	protected EtiquetaGeneral() {
		this.bundleList = new HashMap<String, EtiquetaDiccionario>();
	}

	/**
	 * 
	 * @return
	 */
	public static EtiquetaGeneral getInstance() {
		if (EtiquetaGeneral.instance == null) {
			EtiquetaGeneral.instance = new EtiquetaGeneral();
		}
		return EtiquetaGeneral.instance;
	}

	/**
	 * 
	 * @param organizationId
	 * @param idioma
	 * @return
	 */
	public EtiquetaDiccionario getLabelsInstance(String organizationId, String idioma) {
		String key = organizationId + idioma;
		EtiquetaDiccionario labels = bundleList.get(key);
		if (labels == null) {
			labels = getLabels(organizationId, idioma);
			bundleList.put(key, labels);
		}

		return labels;
	}

	/**
	 * 
	 * @param organizationId
	 * @param idioma
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private EtiquetaDiccionario getLabels(String organizationId, String idioma) {

		Map peticion = new HashMap();
		peticion.put("Pagina_organizacionIde", organizationId);
		peticion.put("Etiqueta_idioma", idioma);
		List list = null;

		try
		{
			List listaEtiqueta = (List) (new ObtenerListaEtiqueta()).ejecutarAccion(peticion);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		Map<String, Etiqueta> mapaEtiqueta = new HashMap<String, Etiqueta>();
		for (int i = 0; i < list.size(); i++) {
			Etiqueta label = (Etiqueta) list.get(i);
			String tipo = label.getTipo();
			String modulo = label.getModulo();
			String campo = label.getCampo();

			String key = null;
			StringBuffer keyBuffer = new StringBuffer();
			if (!Util.isEmpty(tipo) && !Util.isEmpty(modulo)) {
				key = keyBuffer.append(tipo).append("-").append(modulo).append("-").append(campo).toString().toUpperCase();

			}
			else if (!Util.isEmpty(modulo)) {
				key = keyBuffer.append(modulo).append("-").append(campo).toString().toUpperCase();

			}
			else {
				key = campo.toUpperCase();
			}

			mapaEtiqueta.put(key, label);
		}

		EtiquetaDiccionario labels = new EtiquetaDiccionario(organizationId, idioma, mapaEtiqueta);

		return labels;
	}

	/**
	 *
	 */
	@SuppressWarnings("unchecked")
	public void refresh(String organizationId) {
		if (organizationId != null && organizationId.length() > 0) {
			Set keys = bundleList.keySet();
			if (keys != null && keys.size() > 0) {
				Iterator iterator = keys.iterator();
				String organization = organizationId.toUpperCase();
				while (iterator.hasNext()) {
					String key = (String) iterator.next();
					if (key.indexOf(organization) > -1) {
						this.bundleList.put(key, null);
					}
				}
			}
		}
	}

	/**
	 *
	 */
	public void refresh() {
		this.bundleList = new HashMap<String, EtiquetaDiccionario>();
	}

	public static String getFieldAbreviation(String parOid, String parModuleType, String parLabelName) {

		Etiqueta labelObject = null;
		String abreviation = "";
		String campo = "";
		EtiquetaGeneral labelInMemoryManager = EtiquetaGeneral.getInstance();
		EtiquetaDiccionario dictionary = labelInMemoryManager.getLabelsInstance(parOid, "");

		if (!Util.isEmpty(parModuleType)) {
			StringBuffer labelKeyBuffer = new StringBuffer();
			labelKeyBuffer.append(parModuleType).append("-").append(parLabelName);
			labelObject = dictionary.getLabel(labelKeyBuffer.toString().toUpperCase());
		}

		if (labelObject == null) {
			labelObject = dictionary.getLabel(parLabelName.toUpperCase());
		}

		if (labelObject == null) {
			int i = parLabelName.indexOf("-");
			if (i > 0) {
				campo = parLabelName.substring(i + 1, parLabelName.length());
				if (!Util.isEmpty(campo)) {

					labelObject = dictionary.getLabel(campo.toUpperCase());

				}
			}
		}

		if (labelObject != null) {

			abreviation = labelObject.getAbreviacion();

		}// Fin de if(labels.getModuleAccess().equals(parModuleAccess) && labels.getLabelCode().equals(parLabelCode))
		else {

			abreviation = parLabelName;

		}

		return abreviation;

	}// Fin de metodo
}