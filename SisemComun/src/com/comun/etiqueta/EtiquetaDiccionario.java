package com.comun.etiqueta;

import java.util.Map;

import com.comun.entidad.Etiqueta;
import com.comun.utilidad.Util;

/**
 * 
 * @author Alexander
 *
 */
public class EtiquetaDiccionario {

	private String language;
	private String organizationId;
	private Map<String, Etiqueta> labels;
	
	/**
	 * 
	 * @param bundleName
	 * @param organizationId
	 * @param language
	 */
	public EtiquetaDiccionario(String organizationId, String language, Map<String, Etiqueta> labels) {
        this.language = language;
        this.organizationId = organizationId;
        this.labels = labels;
    }

	
	/**
	 * 
	 * @param moduleType
	 * @param module
	 * @param labelCode
	 * @return
	 */
	public Etiqueta getLabel(String labelKey) {
		Etiqueta label = labels.get(labelKey);
		return label;
	}
	
	/**
	 * 
	 * @param moduleType
	 * @param module
	 * @param labelCode
	 * @return
	 */
	public Etiqueta getLabel(String moduleType, String module, String labelCode) {
		
		if (Util.isEmpty(labelCode)) {
			return null;
		}
		
		StringBuffer keyBuffer = new StringBuffer();
		String key = null;
		if (!Util.isEmpty(moduleType) && !Util.isEmpty(module)) {
			key = keyBuffer.append(moduleType).append("-")
							.append(module).append("-")
							.append(labelCode).toString().toUpperCase();
			
		} else if (!Util.isEmpty(module)) {
			key = keyBuffer.append(module).append("-").append(labelCode).toString().toUpperCase();
			
		} else  {
			key = labelCode;
		}
		
		Etiqueta label = labels.get(key);
		
		return label;
	}
	
	
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public Map<String, Etiqueta> getLabels() {
		return labels;
	}

	public void setLabels(Map<String, Etiqueta> labels) {
		this.labels = labels;
	}
}
