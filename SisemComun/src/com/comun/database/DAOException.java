package com.comun.database;

@SuppressWarnings("serial")
public class DAOException extends RuntimeException {

    public DAOException() {
    }

    public DAOException(String arg0) {

        super(arg0);

    }

    public DAOException(Throwable arg0) {

        super(arg0);

    }

    public DAOException(String arg0, Throwable arg1) {

        super(arg0, arg1);

    }
}//Fin de DAOExcepcion