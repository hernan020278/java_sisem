package com.comun.database;

import com.comun.entidad.EntidadListener;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;

import javax.swing.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConeccion implements DBConeccionListener {

	private String oid;

	private Connection connection = null;
	private boolean oldStateOfAutoCommit = false;
	private int oldStateOfTransactionIsolation = Connection.TRANSACTION_READ_COMMITTED;
	private boolean transactionStarted = false;
	private int estado = Estado.SUCCEEDED;

	private long iniConeccion;

	private long finConeccion;

	public DBConeccion(String organizacionIde) throws Exception
	{
		oid = organizacionIde;
		this.setConnection(null, organizacionIde);
	}

	public DBConeccion(Connection connection, String organizacionIde) throws Exception
	{
		this.setConnection(connection, organizacionIde);
	}

	@Override
	public void commit() {
		try {

			this.connection.commit();

		} catch (SQLException e) {

			Log.error(this, e);

		}
	} // commit.

	@Override
	public void end() {
		try {

			connection.setAutoCommit(oldStateOfAutoCommit);
			connection.setTransactionIsolation(oldStateOfTransactionIsolation);
			connection.close();

		} catch (SQLException e) {

			Log.error(this, e);

		}
	} // end.

	@Override
	public void rollback() {
		try {

			this.connection.rollback();

		} catch (SQLException e) {

			Log.error(this, e);

		}
	} // rollback.

	@Override
	public void start() {
		try {

			if (connection == null) {

				crearNuevaTransaction();

			}// Fin de if(connection == null)
			else if (connection.isClosed()) {

				crearNuevaTransaction();

			}// Fin de if (connection.isClosed())

			if (this.connection.getAutoCommit()) {

				// En caso que la conexion se commite de forma
				// automatica, guardamos el estado anterior e
				// indicamos que no deseamos el commit automatico.
				this.connection.setAutoCommit(false);
				this.oldStateOfAutoCommit = true;

			}// Fin de if (this.connection.getAutoCommit())
		} catch (SQLException e) {
			// throw new DBConnectionException(e);
		}
	} // start.

	private void setConnection(Connection connection, String organizacionIde)
	{
		if (connection == null)
		{
			try
			{
				if (!Util.isEmpty(organizacionIde) && organizacionIde.equalsIgnoreCase("SISEM"))
				{
					organizacionIde = "";
				}
				connection = DBConfiguracion.getInstance(organizacionIde).getConnection();
				this.connection = connection;
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public List query(String query) throws Exception
	{
		List lst = null;
		return lst;
	}

	public List ejecutarConsulta(Map peticion) {

		List result = null;

		try {

			String consulta = (String) peticion.get("consulta");
			String respuesta = (String) peticion.get("respuesta");
			String entidadNombre = (String) peticion.get("entidadNombre");
			EntidadListener entidad;

			if (respuesta.equals(Respuesta.ENTIDAD))
			{
				result = new ArrayList();
				entidad = obtenerEntidad(entidadNombre, consulta);
				if (entidad != null)
				{
					result.add(entidad);
				}
			}
			else if (respuesta.equals(Respuesta.LISTAENTIDAD))
			{
				result = (List) obtenerListaEntidad(entidadNombre, consulta);
			}// Fin de if(respuesta.equals("ENTIDAD")){
			else if (respuesta.equals(Respuesta.LISTAMIXTA))
			{
				result = (List) obtenerListaMixta(consulta, (String[]) peticion.get("listaColumnaTipo"), (String[]) peticion.get("listaColumnaAlias"));
			}// Fin de if(respuesta.equals("ENTIDAD")){

			peticion.put("tipoSql","");
			peticion.put("consulta","");
			peticion.put("entidadObjeto","");
			peticion.put("where","");
			peticion.put("delete","");
			peticion.put("sql","");
			peticion.put("respuesta","");
			peticion.put("entidadNombre","");

			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception exception)
		{
			this.setEstado(Estado.FAILED);
			throw new UnsupportedOperationException("Not supported yet.");
		}
		return result;
	}

	public int ejecutarCommit(Object objeto) {

		int afectados = 0;
		PreparedStatement pstm = null;
		Map peticion = (Map) objeto;
		String sql = "";

		try {
			if (peticion.containsKey("tipoSql") && !Util.isEmpty((String)peticion.get("tipoSql")))
			{
				String tipoSql = (String) peticion.get("tipoSql");
				if(tipoSql.equals(TipoSql.BATCH) && (peticion.containsKey("consulta") && !Util.isEmpty((String)peticion.get("consulta"))))
				{
					sql = (String) peticion.get("consulta");
					pstm = obtenerConeccion().prepareStatement(sql.toLowerCase());
					afectados = pstm.executeUpdate();
				}
				else if(peticion.containsKey("entidadObjeto"))
				{
					EntidadListener entidadObjeto = (EntidadListener) peticion.get("entidadObjeto");
					if (tipoSql.equals(TipoSql.INSERT) || tipoSql.equals(TipoSql.UPDATE) || tipoSql.equals(TipoSql.DELETE))
					{
						Map respuesta = obtenerSql(entidadObjeto, tipoSql);
						if(peticion.containsKey("delete") && !Util.isEmpty((String) peticion.get("delete")))
						{
							sql = (String) peticion.get("delete");	
						}
						else
						{
							sql = (String) respuesta.get("sql");
						}
						String campos = (String) respuesta.get("campos");
						String where = "";
						if(peticion.containsKey("where") && !Util.isEmpty((String) peticion.get("where")))
						{
							where = " " + (String) peticion.get("where");	
						}
						sql = sql + where;
						System.out.println("SQL " + tipoSql + ": " + sql);
						pstm = obtenerPreparedStatement(entidadObjeto, campos, sql, tipoSql);
						afectados = pstm.executeUpdate();
					}					
				}
				if (afectados > 0){commit();}else{rollback();}

				peticion.put("tipoSql","");
				peticion.put("consulta","");
				peticion.put("entidadObjeto","");
				peticion.put("where","");
				peticion.put("delete","");
				peticion.put("sql","");
				peticion.put("respuesta","");
				peticion.put("entidadNombre","");

				cerrarConeccion(pstm, null, sql);

				this.setEstado(Estado.SUCCEEDED);
			}
			else
			{
				this.setEstado(Estado.FAILED);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return afectados;
	}

	/*
	 * Crea un nuevo delegado para las transacciones
	 */
	public void crearNuevaTransaction() {
		try {

			connection = DBConfiguracion.getInstance(oid).getConnection();
			oldStateOfTransactionIsolation = connection.getTransactionIsolation();
			connection.setTransactionIsolation(oldStateOfTransactionIsolation);
			transactionStarted = true;

		} catch (SQLException e) {

			Log.error(this, e);

		}

	} // JDBCTransactionDelegateImpl.

	/**
	 * 
	 * Para obtener la coneccion que se creado al momento de registrarNuevo Delegado
	 * 
	 * @return Connection
	 * 
	 */
	public Connection obtenerConeccion() {

		iniConeccion = System.nanoTime();
		return connection;

	} // obtenerConeccion.

	public void cerrarConeccion(PreparedStatement pstm, ResultSet rest, String query) {
			try {
				if(pstm != null && !pstm.isClosed()) {
					pstm.close();
				}
				if(rest != null && !rest.isClosed()) {
					rest.close();
				}

				finConeccion = System.nanoTime();
				double tiempoConeccion = (double) (finConeccion - iniConeccion) / 1000000000.0;
				System.out.println(this.getClass().getSimpleName() + " Tiempo coneccion: " + tiempoConeccion + " Query: " + query);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
	} // obtenerConeccion.

	public Map obtenerSql(Object objeto, String tipo)
	{
		Field[] lisField = objeto.getClass().getDeclaredFields();
		String sql = "";
		if (tipo.equals(TipoSql.INSERT))
		{
			sql = "insert into " + objeto.getClass().getSimpleName();
		}
		else if (tipo.equals(TipoSql.UPDATE))
		{
			sql = "update " + objeto.getClass().getSimpleName();
		}
		else if (tipo.equals(TipoSql.DELETE))
		{
			sql = "delete from " + objeto.getClass().getSimpleName();
		}

		String campos = "";
		String values = "";
		int iteFld = 1;

		Map peticion = new HashMap();

		for (Field iteField : lisField)
		{
			String fieldName = iteField.getName();
			if (tipo.equals(TipoSql.INSERT))
			{
				campos = campos + fieldName + ",";
				values = values + "?,";
			}
			else if (tipo.equals(TipoSql.UPDATE) && !fieldName.contains("_cod"))
			{
				campos = campos + fieldName + ",";
				values = values + fieldName + "=?,";
			}
			iteFld++;
		}// for (Field iteField : lisField) {
		if (tipo.equals(TipoSql.DELETE))
		{
			campos = campos + ",";
			values = values + ",";
		}

		campos = campos.substring(0, campos.length() - 1);
		values = values.substring(0, values.length() - 1);

		if (tipo.equals(TipoSql.INSERT))
		{
			sql = sql + "(" + campos + ") values(" + values + ")";
		}
		else if (tipo.equals(TipoSql.UPDATE))
		{
			sql = sql + " set " + values;
		}

		peticion.put("sql", sql);
		peticion.put("campos", campos);

		return peticion;
	}

	public PreparedStatement obtenerPreparedStatement(Object objeto, String campos, String sql, String tipoSql) {

		Field[] lisField = objeto.getClass().getDeclaredFields();
		int iteFld = 1;
		PreparedStatement pstm = null;
		String[] listaCampos = (campos.contains(","))? campos.split(",") : null;
		String values = "\n";
		try {

			pstm = obtenerConeccion().prepareStatement(sql.toLowerCase());

			if(listaCampos != null)
			{
				for (String iteLisCam : listaCampos)
				{
					for (Field iteField : lisField)
					{
						String fieldName = iteField.getName();
						String methodNameSet = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
						Field field;

						if (iteLisCam.equals(fieldName))
						{
							try {
								field = objeto.getClass().getDeclaredField(fieldName);
								if (field.getType().equals(String.class))
								{
									String valor = (String) objeto.getClass().getMethod(methodNameSet, null).invoke(objeto, null);
									pstm.setString(iteFld, valor);
									values += fieldName + " : " + valor + " : String\n";
								}
								if (field.getType().equals(int.class))
								{
									int valor = (Integer) objeto.getClass().getMethod(methodNameSet, null).invoke(objeto, null);
									if (fieldName.equals("version") || fieldName.contains("_ver"))
									{
										if (tipoSql.equals(TipoSql.INSERT))
										{
											valor = 1;
											pstm.setInt(iteFld, valor);
										}
										else if (tipoSql.equals(TipoSql.UPDATE))
										{
											valor += 1;
											pstm.setInt(iteFld, valor);
										}
									}
									else
									{
										pstm.setInt(iteFld, valor);
									}
									values += fieldName + " : " + valor + " : int\n";
								}
								if (field.getType().equals(BigDecimal.class))
								{
									BigDecimal valor = (BigDecimal) objeto.getClass().getMethod(methodNameSet, null).invoke(objeto, null);
									if (fieldName.equals("version"))
									{
										if (tipoSql.equals(TipoSql.INSERT))
										{
											pstm.setBigDecimal(iteFld, new BigDecimal("1"));
										}
										else if (tipoSql.equals(TipoSql.UPDATE))
										{
											pstm.setBigDecimal(iteFld, valor.add(new BigDecimal(1)));
										}
									}
									else
									{
										pstm.setBigDecimal(iteFld, Util.getInst().getBigDecimalFormatted(valor, 5));
									}
									values += fieldName + " : " + valor + " : BigDecimal\n";
								}
								if (field.getType().equals(double.class))
								{
									double valor = (Double) objeto.getClass().getMethod(methodNameSet, null).invoke(objeto, null);
									pstm.setDouble(iteFld, valor);
									values += fieldName + " : " + valor + " : double\n";
								}
								if (field.getType().equals(boolean.class))
								{
									boolean valor = (Boolean) objeto.getClass().getMethod(methodNameSet, null).invoke(objeto, null);
									pstm.setBoolean(iteFld, valor);
									values += fieldName + " : " + valor + " : boolean\n";
								}
								if (field.getType().equals(Date.class))
								{
									Date valor =(Date) objeto.getClass().getMethod(methodNameSet, null).invoke(objeto, null);
									pstm.setDate(iteFld, valor);
									values += fieldName + " : " + valor + " : Date\n";
								}
								if (field.getType().equals(Time.class))
								{
									Time valor =(Time) objeto.getClass().getMethod(methodNameSet, null).invoke(objeto, null);
									pstm.setTime(iteFld, valor);
									values += fieldName + " : " + valor + " : Time\n";
								}
								if (field.getType().equals(Timestamp.class))
								{
									Timestamp valor =(Timestamp) objeto.getClass().getMethod(methodNameSet, null).invoke(objeto, null);
									pstm.setTimestamp(iteFld, valor);
									values += fieldName + " : " + valor + " : Timestamp\n";
								}
								
								iteFld++;
							} catch (SecurityException e) {
								e.printStackTrace();
							} catch (NoSuchFieldException e) {
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							break;
						}// if (iteField.equals(fieldName))
					}// for (Field iteField : lisField) {
				}// for (String iteLisCam : listaCampos)		
				Log.debug(this, values);
				System.out.println(this.getClass().getSimpleName() + " SQL VALORES: " + values);
//				Log.debug(this, values);
//				Log.debug(this, tipos);
			}//if(listaCampos != null))

		}// try {
		catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pstm;
	}// Fin de limpiarInstancia()

	public Object setResultsetEntidad(ResultSet resSql, Object objeto) {

		Class clase = objeto.getClass();
		Field[] lisField = clase.getDeclaredFields();
		for (Field iteField : lisField) {

			String fieldName = iteField.getName();
			String methodNameSet = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
			Field field;

			try {

				field = clase.getDeclaredField(fieldName);
				if (field.getType().equals(String.class)) {
					clase.getMethod(methodNameSet, String.class).invoke(objeto, resSql.getString(fieldName));
				}
				if (field.getType().equals(int.class)) {
					clase.getMethod(methodNameSet, int.class).invoke(objeto, resSql.getInt(fieldName));
				}
				if (field.getType().equals(double.class)) {
					clase.getMethod(methodNameSet, double.class).invoke(objeto, resSql.getDouble(fieldName));
				}
				if (field.getType().equals(BigDecimal.class)) {
					clase.getMethod(methodNameSet, BigDecimal.class).invoke(objeto, resSql.getBigDecimal(fieldName));
				}
				if (field.getType().equals(boolean.class)) {
					clase.getMethod(methodNameSet, boolean.class).invoke(objeto, resSql.getBoolean(fieldName));
				}
				if (field.getType().equals(Date.class)) {
					clase.getMethod(methodNameSet, Date.class).invoke(objeto, resSql.getDate(fieldName));
				}
				if (field.getType().equals(Time.class)) {
					clase.getMethod(methodNameSet, Time.class).invoke(objeto, resSql.getTime(fieldName));
				}
				if (field.getType().equals(Timestamp.class)) {
					clase.getMethod(methodNameSet, Timestamp.class).invoke(objeto, resSql.getTimestamp(fieldName));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return objeto;

	}// Fin de limpiarInstancia()

	public EntidadListener obtenerEntidad(String strEntidad, String consulta) {

		PreparedStatement pstm;
		ResultSet resSql;
		EntidadListener entidad = null;

		try {

			pstm = obtenerConeccion().prepareStatement(consulta);
			resSql = pstm.executeQuery();

			if (resSql.next()) {
				entidad = (EntidadListener) Class.forName("com.comun.entidad." + strEntidad).newInstance();
				Util.getInst().limpiarEntidad(entidad,true);
				setResultsetEntidad(resSql, entidad);
			}

			cerrarConeccion(pstm, resSql, consulta);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return entidad;
	}

	public Object obtenerListaEntidad(String entidad, String consulta) {

		PreparedStatement pstm;
		ResultSet resSql;
		List lista = new ArrayList();
		try {

			pstm = obtenerConeccion().prepareStatement(consulta);
			resSql = pstm.executeQuery();

			while (resSql.next()) {

				EntidadListener entidadObjeto = (EntidadListener) Class.forName("com.comun.entidad." + entidad).newInstance();
				Util.getInst().limpiarEntidad(entidadObjeto,true);
				lista.add(setResultsetEntidad(resSql, entidadObjeto));
			}

			cerrarConeccion(pstm, resSql, consulta);

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return lista;
	}

	public Object obtenerListaMixta(String consulta, String[] listaColumnaTipo, String[] listaColumnaAlias) {

		PreparedStatement pstm;
		ResultSet resSql;
		List lista = new ArrayList();
		String colAli = null;
		String colTip = null;
		try
		{
			pstm = obtenerConeccion().prepareStatement(consulta);
			resSql = pstm.executeQuery();

			while (resSql.next())
			{
				Object[] objeto = new Object[listaColumnaAlias.length];

				for (int iteCol = 0; iteCol < listaColumnaAlias.length; iteCol++)
				{
					colAli = listaColumnaAlias[iteCol];
					colTip = listaColumnaTipo[iteCol];
					if (colTip.equals("STRING") || colTip.equals("TIME"))
					{
						objeto[iteCol] = resSql.getString(colAli)==null ? "" : resSql.getString(colAli);
					}
					if (colTip.equals("INTEGER"))
					{
						objeto[iteCol] = resSql.getInt(colAli);
					}
					if (colTip.equals("BOOLEAN"))
					{
						objeto[iteCol] = resSql.getBoolean(colAli);
					}
					if (colTip.equals("DOUBLE"))
					{
						objeto[iteCol] = resSql.getDouble(colAli);
					}
					if (colTip.equals("BIGDECIMAL"))
					{
						objeto[iteCol] = resSql.getBigDecimal(colAli)==null ? BigDecimal.ZERO : resSql.getBigDecimal(colAli);
					}
					if (colTip.equals("DATE"))
					{
						objeto[iteCol] = resSql.getDate(colAli)==null ? new Date(1900 - 1900, 1, 1) : resSql.getDate(colAli);
					}
				}
				lista.add(objeto);
			}

			cerrarConeccion(pstm, resSql, consulta);

		} catch (SQLException e) {
			System.out.println(listaColumnaAlias.toString());
			Log.error(this, "El alias - " + colAli + " - No existe \n En la consulta");
			throw new RuntimeException(e);
		}
		return lista;
	}

	public int obtenerCantidadRegistros(String consulta) {

		PreparedStatement pstm;
		ResultSet resSql;
		int cantidad = 0;
		try
		{
			pstm = obtenerConeccion().prepareStatement(consulta, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			resSql = pstm.executeQuery();
			if (resSql.last())
			{
				cantidad = resSql.getRow();
			}

			cerrarConeccion(pstm, resSql, consulta);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return cantidad;
	}

	public int agregar(String sql) {

		PreparedStatement pstm;
		int afectados = 0;

		try {

			pstm = obtenerConeccion().prepareStatement(sql);
			afectados = pstm.executeUpdate();

			cerrarConeccion(pstm, null, sql);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return afectados;
	}

	public boolean isTransactionStarted() {
		return transactionStarted;
	}

	public void setTransactionStarted(boolean transactionStarted) {
		this.transactionStarted = transactionStarted;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run()
			{
				try {

					DBConeccion dbc = new DBConeccion("SISEM");
					Connection connection = dbc.obtenerConeccion();

					PreparedStatement pstm = connection.prepareStatement("INSERT INTO asictrlasistencia(kyctrlasistencia, numerodni, version) VALUES(?, ?, ?)");
					pstm.setBigDecimal(1, new BigDecimal("23423432"));
					pstm.setString(2, "10793205");
					pstm.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now()));

					int row = pstm.executeUpdate();

					// rows affected
					System.out.println(row); //1
					System.out.println("Cerrada : " + connection.isClosed());
					connection.close();

				} catch (SQLException e) {
					System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
