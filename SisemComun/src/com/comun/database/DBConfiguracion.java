package com.comun.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.dbcp.BasicDataSource;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.referencia.DriverJdbc;
import com.comun.utilidad.AplicacionGeneral;

/**
 * @author JEFF
 */
public class DBConfiguracion {

	private static DBConfiguracion INSTANCE;
	private DataSource dataSource;
	private Connection coneccion = null;
	private String oid;
	private String driverJdbc;

	private DBConfiguracion(String oid) {

		this.oid = oid;
	}
	public static DBConfiguracion getInstance()
	{
		return DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo());
	}
	public static DBConfiguracion getInstance(String oid)
	{
		if (INSTANCE == null)
		{
			INSTANCE = new DBConfiguracion(oid);
		}
		return INSTANCE;
	}

	public Connection getConnection()
	{
		try {
			if (this.coneccion == null || coneccion.isClosed())
			{
				coneccion = crearConeccion(DriverJdbc.POOL, AplicacionGeneral.getInstance().obtenerConfig("gestor-database", DriverJdbc.SQLSERVER));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return coneccion;
	}

	public Connection crearConeccionWeb() 
	{
		try {
			// Obtenemos el Pool de Conexiones
			Context initCtx = new InitialContext();
			dataSource = (DataSource) initCtx.lookup("JDBCPoolSQLServerDBChentty");
			coneccion = dataSource.getConnection();
			return coneccion;
		} catch (NamingException ex) {
			Logger.getLogger(DBConfiguracion.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SQLException ex) {
			Logger.getLogger(DBConfiguracion.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public Connection crearConeccionPool(String driver, String urldbc, String usuario, String clave) 
	{
		try {
			BasicDataSource basicDataSource = new BasicDataSource();
			basicDataSource.setDriverClassName(driver);
			basicDataSource.setUrl(urldbc);
			basicDataSource.setUsername(usuario);
			basicDataSource.setPassword(clave);
			basicDataSource.setValidationQuery("select 1");
			dataSource = basicDataSource;
			return dataSource.getConnection();
		} catch (SQLException ex) {
			Logger.getLogger(DBConfiguracion.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public Connection crearConeccionJdbc(String driver, String urldbc, String usuario, String clave)
	{
		try
		{
			if (coneccion == null || coneccion.isClosed())
			{
				Class.forName(driver);
				coneccion = DriverManager.getConnection(urldbc, usuario, clave);
				coneccion.setAutoCommit(true);
			}
			return coneccion;
		} catch (ClassNotFoundException e) {
			JOptionPane.showConfirmDialog(null,
					"No se encontro el controlador de Base de Datos!!!",
					"Error de archivo", JOptionPane.PLAIN_MESSAGE,
					JOptionPane.INFORMATION_MESSAGE);
			throw new RuntimeException(e);
		} catch (SQLException e) {
			JOptionPane.showConfirmDialog(null, "Error de Sentencia SQL - 4!!!\n" + e.getMessage(),
					"Error de conexion", JOptionPane.PLAIN_MESSAGE,
					JOptionPane.INFORMATION_MESSAGE);
			throw new RuntimeException(e);
		}
	}

	public Connection crearConeccion(String tipo, String gestor) {

		setDriverJdbc(gestor);
		String servidor = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("servidor-database", "");
		String database = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("nombre-database", "");
		String usuario = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("usuario-database", "");
		String clave = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("clave-database", "");
		String port = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("servidor-port", "");
		String driver = "";
		String urldbc = "";
		try
		{
			if (coneccion == null || coneccion.isClosed())
			{
				if (gestor.equals(DriverJdbc.MYSQL))
				{
					driver = "org.gjt.mm.mysql.Driver";
//					urldbc = "jdbc:mysql://" + servidor + ":3306/" + database;
					urldbc = "jdbc:mysql://" + servidor + ":" + port + "/" + database;
				}
				else if (gestor.equals(DriverJdbc.ORACLE))
				{
					driver = "oracle.jdbc.OracleDriver";
//					urldbc = "jdbc:oracle:thin:@" + servidor + ":1521:XE";
					urldbc = "jdbc:oracle:thin:@" + servidor + ":" + port + ":XE";
				}
				else if (gestor.equals(DriverJdbc.SQLSERVER))
				{
					driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
//					urldbc = "jdbc:sqlserver://" + servidor + ":1433;databaseName=" + database;
					urldbc = "jdbc:sqlserver://" + servidor + ":" + port + ";databaseName=" + database;
				}
				if (tipo.equals(DriverJdbc.POOL))
				{
					return crearConeccionPool(driver, urldbc, usuario, clave);
				}
				else if (tipo.equals(DriverJdbc.JDBC))
				{
					return crearConeccionJdbc(driver, urldbc, usuario, clave);
				}
				else if (tipo.equals(DriverJdbc.WEB))
				{
					return crearConeccionWeb();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getDriverJdbc() {
		return driverJdbc;
	}

	public void setDriverJdbc(String driverJdbc) {
		this.driverJdbc = driverJdbc;
	}

	public static void main(final String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				Connection cnxDB = null;
				try {
					System.out.println("organizacion : " + args[0]);
					OrganizacionGeneral.getInstance(args[0]);
					cnxDB = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection();
					System.out.println("Cerrada : " + cnxDB.isClosed());
					cnxDB.close();
				}
				catch (SQLException ex)
				{
					Logger.getLogger(DBConfiguracion.class.getName()).log(Level.SEVERE, null, ex);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
}
