package com.comun.database;


/**
 * Created on March 15, 2005
 * @author Kelli
 * project: Foundation
 * com.tsagate.foundation.exceptions.DatabaseConnectionException.java
 * 
 */
public class DBConnectionException extends Exception {

	public DBConnectionException() {
		super();
	}
	
	public DBConnectionException(String message) {
		super(message);
	}
	
	public DBConnectionException(String message, Exception e) {
		e.printStackTrace();
	}

	public DBConnectionException(Throwable cause) {
		super(cause);
	}

	public DBConnectionException(String message, Throwable cause) {
		super(message, cause);
	}
}