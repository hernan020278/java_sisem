package com.comun.database;

import java.util.Map;

/**
 *
 * Clase encargada de encapsular el manejo de transacciones, es decir se le
 * delega la transaccionabilidad de las consultas.
 *
 * @author jsanca
 *
 */
public interface DBConeccionListener {

    /**
     *
     * Invocar a este metodo cuando inicia la transaccion. Deberia colocar todas
     * las precondiciones necesarias para ejecutar la transaccion.
     *
     */
    public void start();

    /**
    *
    * Realiza el commit de la transaccion.
    *
    */
   public Object ejecutarConsulta(Map objeto);

   /**
   *
   * Realiza el commit de la transaccion.
   *
   */
   public void commit();

    /**
     *
     * Realiza el rollback de la transaccion
     * @throws DBConnectionException 
     *
     */
    public void rollback() throws DBConnectionException;

    /**
     *
     * Invocar a este metodo para finalizar la transaccion, suceda o no un
     * error.
     *
     * El mismo sera el encargado de volver al estado anterior a la transaccion
     *
     * y cerrar los recursos solicitados al inicio de la transaccion.
     *
     */
    public void end();
} // E:O:F:TransactionDelegate.   