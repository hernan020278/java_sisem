package com.comun.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.DiccionarioGeneral;

public class ConexionControlBackup {

	public static Connection crearConexion() {
		Connection coneccion;
		String servidor = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("servidor-database", "");
		String database = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("control-database", "");
		String usuario = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("usuario-database", "");
		String clave = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("clave-database", "");
		String port = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("servidor-port", "");

		try
		{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//			coneccion = DriverManager.getConnection("jdbc:sqlserver://" + servidor + ":1433;databaseName=" + database, usuario, clave);
			coneccion = DriverManager.getConnection("jdbc:sqlserver://" + servidor + ":" + port + ";databaseName=" + database, usuario, clave);
			return coneccion;
		}
		catch (ClassNotFoundException e)
		{
			JOptionPane.showConfirmDialog(null,
					"No se encontro el controlador de SQLServer!!!",
					"Error de archivo controlbackup",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			throw new RuntimeException(e);
		}
		catch (SQLException e)
		{
			JOptionPane.showConfirmDialog(null,
					"Error de Sentencia SQL - 2!!!",
					"Error coneccion controlbackup",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) throws SQLException {

		Connection c = ConexionControlBackup.crearConexion();

		System.out.println("Cerrada : " + c.isClosed());
		c.close();

	}
}
