package com.comun.propiedad.accion;

import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.utilidad.Auxiliar;

public class ObtenerListaPropiedad extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{

			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String organizationId = (String) peticion.get("organizacionIde");
			peticion.put("consulta", "select * from propiedad order by seccion");
			peticion.put("respuesta", Respuesta.LISTAENTIDAD);
			peticion.put("entidadNombre", "Propiedad");

			List listResult = dbc.ejecutarConsulta(peticion);
			if (listResult != null && listResult.size() > 0) {
				result = listResult;
			}
			peticion.put("listaPropiedad", result);
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
}
