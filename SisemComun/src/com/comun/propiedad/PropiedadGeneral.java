/*
 * Created on Sep 30, 2003
 */
package com.comun.propiedad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.database.DBConeccion;
import com.comun.entidad.Propiedad;
import com.comun.etiqueta.accion.ObtenerListaEtiqueta;
import com.comun.motor.AccionListener;
import com.comun.propiedad.accion.ObtenerListaPropiedad;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;

public class PropiedadGeneral
{
	protected Map listaSeccion;
	protected List propertiesToAdd;
	protected List propertiesToUpdate;
	private static String oid;

	private static HashMap properties = new HashMap();

	public static PropiedadGeneral getInstance(String oid)
	{
		if (PropiedadGeneral.properties.get(oid) == null)
		{
			PropiedadGeneral.properties.put(oid, new PropiedadGeneral(oid));
		}
		return (PropiedadGeneral) PropiedadGeneral.properties.get(oid);
	}

	private PropiedadGeneral(String oid)
	{
		this.propertiesToAdd = new ArrayList();
		this.propertiesToUpdate = new ArrayList();
		this.cargarPropiedades(oid);
	}

	public void cargarPropiedades(String oid)
	{
		try
		{
			if (!Util.isEmpty(oid))
			{

				Map peticion = new HashMap();
				DBConeccion dbConeccion = new DBConeccion(oid);
				peticion.put("dbConeccion", dbConeccion);

				List listResult = (List) (new ObtenerListaPropiedad()).ejecutarAccion(peticion);
				this.listaSeccion = new HashMap();
				if (listResult != null && listResult.size() > 0)
				{
					for (Iterator it = listResult.iterator(); it.hasNext();)
					{
						this.crearListaSeccion(it.next());
					}
				}
				this.setOrganizationId(oid);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void crearListaSeccion(Object next)
	{
		Propiedad propiedad = (Propiedad) next;
		Map propsTemp = null;

		if (this.listaSeccion.containsKey(propiedad.getSeccion()))
		{
			propsTemp = (HashMap) this.listaSeccion.get(propiedad.getSeccion());
		}
		else
		{
			propsTemp = new HashMap();
		}
		propsTemp.put(propiedad.getPropiedad(),propiedad.getValor());
		this.listaSeccion.put(propiedad.getSeccion(), propsTemp);
	}

	public String getProperty(String seccion, String propiedad, String defecto)
	{
		String value = defecto;
		if (this.listaSeccion != null)
		{
			Map sectionMap = (Map) this.listaSeccion.get(seccion.toUpperCase());
			if (sectionMap != null)
			{
				value = (String) sectionMap.get(propiedad.toUpperCase());
				if (Util.isEmpty(value))
				{
					value = defecto;
				}
			}
		}

		return value;
	}

	public Map getSection(String propiedad)
	{
		Map sectionMap = (Map) this.listaSeccion.get(propiedad.toUpperCase());
		if (sectionMap == null)
		{
			sectionMap = new HashMap();
		}
		return sectionMap;
	}

	public void setProperty(String seccion, String propiedad, String value)
	{
		Propiedad propiedadEntidad = new Propiedad();
		if (propiedad != null)
		{
			propiedad = propiedadEntidad.getSeccion().toUpperCase();
		}
		if (propiedad != null)
		{
			propiedad = propiedadEntidad.getSeccion().toUpperCase();
		}

		propiedadEntidad.setSeccion(seccion);
		propiedadEntidad.setPropiedad(propiedad);

		Map sectionMap = (Map) this.listaSeccion.get(seccion);
		if (sectionMap != null && sectionMap.containsKey(propiedad))
		{
			this.propertiesToUpdate.add(propiedad);
		}
		else
		{
			this.propertiesToAdd.add(propiedad);
		}
		this.crearListaSeccion(propiedad);
	}

	public List getPropertiesToAdd()
	{
		return this.propertiesToAdd;
	}

	public void clearPropertiesToAdd()
	{
		this.propertiesToAdd.clear();
	}

	public List getPropertiesToUpdate()
	{
		return this.propertiesToUpdate;
	}

	public void clearPropertiesToUpdate()
	{
		this.propertiesToUpdate.clear();
	}

	private static String getOrganizationIde() {
		return PropiedadGeneral.oid;
	}

	private void setOrganizationId(String oid) {
		PropiedadGeneral.oid = oid;
	}

	public static void removeOID(String o) {
		PropiedadGeneral propertiesManager = (PropiedadGeneral) properties.get(o);
		if (propertiesManager != null) {
			propertiesManager.clear();
			properties.remove(o);
		}
	}

	public void clear() {
		if (listaSeccion != null) {
			listaSeccion.clear();
		}

		if (propertiesToAdd != null) {
			propertiesToAdd.clear();
		}

		if (propertiesToUpdate != null) {
			propertiesToUpdate.clear();
		}
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				PropiedadGeneral prop = PropiedadGeneral.getInstance("sisem");

			}
		});

	}
}
