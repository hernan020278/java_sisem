/*
 * Created on Aug 4, 2004
 *
 * @author  * renzo
 * project: TsaProperties
 * package com.properties.DiccionarioGeneral.java
 *
 */
package com.comun.propiedad;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Util;

public class DiccionarioGeneral
{
	protected static DiccionarioGeneral instance = null;
	protected HashMap listaDiccionario = null;
	protected HashMap listaOrganizacion = null;

	protected DiccionarioGeneral()
	{
		this.listaDiccionario = new HashMap();
		this.listaOrganizacion = new HashMap();
	}

	public static DiccionarioGeneral getInstance()
	{
		if (DiccionarioGeneral.instance == null)
		{
			DiccionarioGeneral.instance = new DiccionarioGeneral();
		}

		return DiccionarioGeneral.instance;
	}

	public static void getInstance(boolean getNew)
	{
		DiccionarioGeneral.instance = new DiccionarioGeneral();
	}

	public static void refresh()
	{
		System.out.println("Refreshing dictionaries!");
		DiccionarioGeneral.instance = null;
	}

	@SuppressWarnings("unchecked")
	public void refreshLabels(String orgIde)
	{
		System.out.println("Refreshing labels for " + orgIde + "!");
		if (orgIde != null && orgIde.length() > 0) {

			String labelOrganization = "labels" + orgIde;
			labelOrganization = labelOrganization.toUpperCase();
			Set keys = listaDiccionario.keySet();
			if (keys != null && keys.size() > 0) {
				Iterator iterator = keys.iterator();
				while (iterator.hasNext()) {
					String key = (String) iterator.next();
					if (key.indexOf(labelOrganization) > -1) {
						this.listaDiccionario.put(key, null);
					}
				}
			}
		}
	}

	public void refresh(String orgIde, String nombreDiccionario)
	{
		System.out.println("refreshing " + nombreDiccionario + " dictionaries for " + orgIde + "!");
		String name = "";
		if (nombreDiccionario.length() > 0)
		{
			name = nombreDiccionario + orgIde;
		}
		name = name.toUpperCase();
		this.listaDiccionario.put(name, null);
	}

	public static DiccionarioPropiedad getLabelsInstance(String orgIde, String language)
	{
		return DiccionarioGeneral.getInstance().getLabelsBundle(orgIde, language);

	}// end getLabelsInstance

	public DiccionarioPropiedad getLabelsBundle(String orgIde, String language)
	{
		String name = "";
		name = "labels" + orgIde + language;
		name = name.toUpperCase();
		// System.out.println("DiccionarioGeneral looking for: " + name);

		DiccionarioPropiedad diccionario = (DiccionarioPropiedad) this.listaDiccionario.get(name);

		if (diccionario == null)
		{
			diccionario = DiccionarioFabrica.getDiccionarioEtiquetas(orgIde, language);
			this.listaDiccionario.put(name, diccionario);
			// System.out.println("*** " + name + " ***");
		}
		return diccionario;
	}// END getLabelsBundle

	public static Diccionario getInstance(String paquete, String oid)
	{
		return DiccionarioGeneral.getInstance().getPaquete(paquete, oid);
	}

	public Diccionario getPaquete(String nombreDiccionario, String oid)
	{
		Diccionario diccionario = (Diccionario) this.listaDiccionario.get(nombreDiccionario);

		if (diccionario == null) {

			diccionario = DiccionarioFabrica.getDiccionario(nombreDiccionario, oid);

			this.listaDiccionario.put(nombreDiccionario, diccionario);

		}
		return diccionario;
	}

	public Diccionario getPaquete(String nombreDiccionario)
	{
		Diccionario diccionario = (Diccionario) this.listaDiccionario.get(nombreDiccionario);

		if (diccionario == null)
		{
			diccionario = new Diccionario(nombreDiccionario);
			this.listaDiccionario.put(nombreDiccionario, diccionario);
		}
		return diccionario;
	}

	public Diccionario getPaquete(String paquete, boolean getNew)
	{
		Diccionario dictionary = (Diccionario) this.listaDiccionario.get(paquete);

		if (dictionary == null || getNew)
		{
			dictionary = new Diccionario(paquete);
			this.listaDiccionario.put(paquete, dictionary);
		}

		return dictionary;
	}

	public static String getReportsDisplay(String organzacionIde)
	{
		return getReportsUrl(organzacionIde);
	}

	public static String getReportsUrl(String organzacionIde)
	{
		String url = getSisemAplicacionUrl(organzacionIde);
		if (url.contains("http://"))
		{
			url = url + "/sisemreports/out";
		}
		else if (url.contains("C:\\") || url.contains("Y:\\"))
		{
			url = url + "\\reportes\\out";
		}

		return url;
	}

	public static String getSigUrl(String organizationId) {
		String url = getReportsImgUrl(organizationId) + "/sigs";
		return url;
	}

	public static String getSisemAplicacionUrl(String organzacionIde)
	{
		PropiedadGeneral propiedadGeneral = PropiedadGeneral.getInstance(organzacionIde);
		String url = propiedadGeneral.getProperty("APPLICATION", "URL BASE", "");
		if (Util.isEmpty(url))
		{
			url = propiedadGeneral.getProperty("APPLICATION", "URL", "");
			url = url.replace("/sisem", "");
		}
		return url;
	}

	public static String getReportsImgUrl(String organizacionIde)
	{
		String url = getSisemAplicacionUrl(organizacionIde) + "/sisemimages";
		return url;
	}

	public static void main(String[] args)
	{
		// DiccionarioGeneral properties = DiccionarioGeneral.getInstance("tsagate", "axa");
		// System.out.println(properties.getPaquete("tsagate").getProperty("default-database-configuration", "default"));
		Diccionario dic = DiccionarioGeneral.getInstance("host", "tsagate");
		// System.out.println(dic.getProperty("steps-xml-path", "axa"));
	}

	public String toString()
	{
		return this.listaDiccionario.toString();
	}

	public static void agregaOrganizacion(String id)
	{
		if (!DiccionarioGeneral.getInstance().listaOrganizacion.containsKey(id))
			DiccionarioGeneral.getInstance().listaOrganizacion.put(id, id);
	}

}
