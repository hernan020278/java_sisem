/*
 * Created on Aug 4, 2004
 *
 * @author  * renzo
 * project: TsaProperties
 * package com.properties;.ResourceLoader.java
 *
 */
package com.comun.propiedad;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;

public class Diccionario
{
	public ResourceBundle paquete = null;
	private String nombrePaquete = "properties.host";
	private String orgIde = OrganizacionGeneral.getOrgcodigo();

	public Diccionario()
	{
		this.setName("properties.host");
		this.setPaquete(nombrePaquete, null);
	}

	public Diccionario(String nombrePaquete)
	{
		this.setPaquete(nombrePaquete, null);
	}

	public Diccionario(String paquete, String parLocal)
	{
		// Locale local = new Locale("en", "US", parLocal);
		if (parLocal == null || parLocal.length() < 1)
		{
			this.setPaquete(paquete, null);
		}
		else
		{
			Locale local = new Locale(parLocal);
			this.setPaquete(paquete, local);
		}
	}

	private void setPaquete(String name, Locale parLocal)
	{
		
		if (name == null)
		{
			System.out.println("No paquete specified, using host!");
			name = this.nombrePaquete;
		}
		try
		{
			if (name.indexOf("properties") < 0)
			{
				name = "properties." + name;
			}
			if (parLocal == null)
			{
				this.paquete = ResourceBundle.getBundle(name);
			}
			else
			{
				this.paquete = ResourceBundle.getBundle(name, parLocal);
			}
		} catch (Exception e)
		{
			System.out.println("Error obtaining properties from: " + name + " for parLocal: " + parLocal);
			e.printStackTrace();
		}
	}

	public String getPropiedad(String propiedad, String defaultProp)
	{
		String propertyValue = "";
		try
		{
			propertyValue = this.paquete.getString(propiedad);

		} catch (Exception e)
		{

			propertyValue = defaultProp;

		}
		return propertyValue;
	}

	public String getPropiedad(String propiedad)
	{
		return this.getPropiedad(propiedad, "");
	}

	public Map obtenerMapaPropiedad()
	{
		HashMap propertyMap = new HashMap();
		Enumeration _enum = this.paquete.getKeys();

		while (_enum != null && _enum.hasMoreElements())
		{
			String key = (String) _enum.nextElement();
			String value = this.paquete.getString(key);

			propertyMap.put(key, value);
		}
		return propertyMap;
	}

	/**
	 * @return Returns the nombrePaquete.
	 */
	public String getName()
	{
		return nombrePaquete;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String ret = this.getName();
		ret += this.obtenerMapaPropiedad();
		return ret;
	}

	/**
	 * @param nombrePaquete
	 *            The nombrePaquete to set.
	 */
	protected void setName(String nombrePaquete)
	{
		this.nombrePaquete = nombrePaquete;
	}

	public String getOrgIde() {
		return orgIde;
	}

	public void setOrgIde(String orgIde) {
		this.orgIde = orgIde;
	}

}
