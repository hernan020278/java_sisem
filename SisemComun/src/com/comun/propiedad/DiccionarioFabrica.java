/*
 * Created on Aug 10, 2004
 *
 * @author  * renzo
 * project: TsaProperties
 * package com.properties;.DictionaryFactory.java
 *
 */
package com.comun.propiedad;

public class DiccionarioFabrica
{
	public static Diccionario getDiccionario(String nombreDiccionario, String orgIde)
	{
		Diccionario diccionario = null;
		if (nombreDiccionario.equalsIgnoreCase("host"))
		{
			diccionario = new Diccionario(nombreDiccionario, orgIde);
		}
		else
		{
			diccionario = new DiccionarioPropiedad(nombreDiccionario, orgIde);
		}

		return diccionario;
	}

	public static DiccionarioPropiedad getDiccionarioEtiquetas(String orgIde, String language)
	{
		return new DiccionarioPropiedad("propiedades", orgIde, language);
	}
}
