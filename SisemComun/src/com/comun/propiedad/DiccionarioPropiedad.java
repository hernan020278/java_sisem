/*
 * Created on Aug 10, 2004
 *
 * @author  * renzo
 * project: TsaProperties
 * package com.properties;.LabelsDictionary.java
 *
 */
package com.comun.propiedad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.PropertyResourceBundle;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;

/**
 * @author renzo
 */
public class DiccionarioPropiedad extends Diccionario
{
	public static String DEFAULT_DIRECTORY = "SISEM";
	public String language;

	public DiccionarioPropiedad()
	{
		this("propiedades", "sisem");
	}

	/**
	 * @param nombreDiccionario
	 */
	public DiccionarioPropiedad(String orgIde)
	{
		this("propiedades", orgIde);
	}

	/**
	 * @param nombreDiccionario
	 * @param orgIde
	 */
	public DiccionarioPropiedad(String nombreDiccionario, String orgIde)
	{
		this.setName(nombreDiccionario);
		this.setOrgIde(orgIde);
		this.setDiccionario(nombreDiccionario, orgIde);
	}

	public DiccionarioPropiedad(String nombreDiccionario, String orgIde, String language)
	{
		this.setName(nombreDiccionario);
		this.setOrgIde(orgIde);
		this.setDiccionario(nombreDiccionario, orgIde, language);
	}

	private String checkorgIde(String orgIde)
	{
		if (orgIde == null)
		{
			orgIde = DiccionarioPropiedad.DEFAULT_DIRECTORY;
		}
		else
		{
			if (orgIde.length() < 0)
			{
				orgIde = DiccionarioPropiedad.DEFAULT_DIRECTORY;
			}
		}

		return orgIde;
	}

	private void setDiccionario(String nombreDiccionario, String orgIde)
	{
		this.setDiccionario(nombreDiccionario, orgIde, "");
	}

	private void setDiccionario(String nombreDiccionario, String orgIde, String language)
	{
		orgIde = this.checkorgIde(orgIde);
		String path = DiccionarioGeneral.getInstance("host", "").getPropiedad("ruta-" + nombreDiccionario.toLowerCase(), "properties.etiquetas");
		String propertiesName = "";
		try
		{
			propertiesName = this.getPropertiesFileName(path, orgIde, nombreDiccionario, language);

			// System.out.println("LabelsDictionary.setDiccionario propsName = " + propertiesName);
			File props = new File(propertiesName);

			if (!props.exists())
			{
				propertiesName = this.getPropertiesFileName(path, orgIde, nombreDiccionario, "");
				props = new File(propertiesName);
				if (!props.exists() && !nombreDiccionario.equalsIgnoreCase("missinglabels"))
				{
					propertiesName = this.getPropertiesFileName(path, DiccionarioPropiedad.DEFAULT_DIRECTORY, nombreDiccionario, "");
					props = new File(propertiesName);

					if (!props.exists())
					{
						throw new UnsupportedOperationException("Not supported yet.");
					}
				}
			}

			if (props.exists()) {
				FileInputStream fis = new FileInputStream(props);
				this.paquete = new PropertyResourceBundle(fis);
			} else {
				this.paquete = null;
			}
		} catch (Exception e)
		{
			System.out.println("File: " + propertiesName + " was not found!");
			e.printStackTrace();
		}
	}

	/**
	 * This method just builds the pathName. It does NOT check if file exists.
	 * 
	 * @param defaultPath
	 *            path to properties directory
	 * @param orgIde
	 *            orgIde
	 * @param nombreDiccionario
	 *            properties File Name
	 * @return path to propertiesFilename.
	 */
	private String getPropertiesFileName(String defaultPath, String orgIde, String nombreDiccionario, String language)
	{
		StringBuffer sb = new StringBuffer();
		sb.append(defaultPath);
		if(!orgIde.equalsIgnoreCase("SISEM"))
		{
			sb.append(orgIde.toLowerCase());
			sb.append(File.separator);
		}
		sb.append(nombreDiccionario.toLowerCase());
		if (language != null && language.length() > 0)
		{
			sb.append("_" + language.toLowerCase());
		}
		sb.append(".properties");

		return sb.toString();
	}

	/**
	 * @param labelName
	 * @param defaultValue
	 * @return
	 */
	public String getPropiedad(String labelName, String defaultValue)
	{
		String label = super.getPropiedad(labelName, defaultValue);
		return label;
	}
}
