/**
 *
 */
package com.comun.organizacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Organizacion;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.utilidad.Util;

/**
 * @author renzo
 * 
 */
public class OrganizacionGeneral {
	private static OrganizacionGeneral instance = null;
	private static String orgcodigo = "";
	private HashMap organizaciones = new HashMap();

	public static OrganizacionGeneral getInstance() {
		if (OrganizacionGeneral.instance == null) {
			OrganizacionGeneral.instance = new OrganizacionGeneral();
		}
		return OrganizacionGeneral.instance;
	}

	public static OrganizacionGeneral getInstance(String codigo) {
		setOrgcodigo(codigo);
		return getInstance();
	}

	private OrganizacionGeneral() {

		super();
		try {
			Map peticion = new HashMap();
			DBConeccion dbConeccion = new DBConeccion(getOrgcodigo());
			peticion.put("dbConeccion", dbConeccion);

			peticion.put("consulta", "select * from organizacion");
			peticion.put("entidadNombre", "Organizacion");
			List listaOrganizacion = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			if (listaOrganizacion != null) 
			{
				for (int i = 0; i < listaOrganizacion.size(); i++) 
				{
					Organizacion organizacion = (Organizacion) listaOrganizacion.get(i);
					String orgcodigo = organizacion.getOrgcodigo();
					if (!Util.isEmpty(orgcodigo)) 
					{
						DiccionarioGeneral.agregaOrganizacion(orgcodigo);
						organizaciones.put(orgcodigo.toUpperCase(),organizacion);

						// try {
						// // Reload labels.properties file
						// ProcesoCargar procesoCargar = new
						// ProcesoCargar(orgcodigo);
						// Proceso labelProcess =
						// procesoCargar.cargarProceso("labels-load.xml");
						// HashMap requestParams = new HashMap();
						// requestParams.put("organizacionId", orgcodigo);
						// labelProcess.ejecutarProceso(requestParams);
						// }
						// catch (Exception e1) {
						// Log.error(this, "Etiqueta were not loaded for " +
						// orgcodigo);
						// Log.error(this, e1.getMessage());
						// }
					}
				}
			}
		} catch (Exception e) {
			// throw e;
		}
	}

	public static String getOrgcodigo() {
		return orgcodigo;
	}

	public static void setOrgcodigo(String orgcodigo) {
		OrganizacionGeneral.orgcodigo = orgcodigo;
	}

	public List getListaOrganizacion() {
		List list = new ArrayList();
		if (this.organizaciones != null) {
			Map organizationMap = new TreeMap(organizaciones);
			list.addAll(organizationMap.values());
		}
		return list;
	}
}
