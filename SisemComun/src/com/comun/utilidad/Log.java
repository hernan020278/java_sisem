/*
 * Created on Nov 21, 2003
 */
package com.comun.utilidad;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.comun.propiedad.DiccionarioGeneral;

/**
 * @author renzo
 *         project: test
 *         com.tsagate.commonLog4jTest.java
 */
public class Log
{
	static Logger logger = null;
	static boolean isInitialized = false;

	@SuppressWarnings("unchecked")
	public static void logger(Object o)
	{
		Class c = o.getClass();
		Log.logger = Logger.getLogger(c);

		if (!isInitialized) {
			String filename = DiccionarioGeneral.getInstance("host", "").getPropiedad("ruta-log");
			PropertyConfigurator.configure(filename);
			isInitialized = true;
		}
	}

	public static void warn(Object o, String msg)
	{
		Log.logger(o);
		Log.logger.warn(msg);
	}

	public static void error(Object o, String msg)
	{
		Log.logger(o);
		Log.logger.error(msg);
	}

	public static void error(Object o, Exception e)
	{
		Log.logger(o);
		Log.logger.error(o, e);
	}

	public static void error(Object o, Throwable t)
	{
		Log.logger(o);
		Log.logger.error(o, t);
	}

	public static void fatal(Object o, Throwable t)
	{
		Log.logger(o);
		Log.logger.fatal(o, t);
	}

	public static void fatal(Object o, String msg)
	{
		Log.logger(o);
		Log.logger.fatal(msg);
	}

	public static void debug(Object o, String msg)
	{
		Log.logger(o);
		Log.logger.debug(msg);
	}

//	public static void trace(Object o, String msg)
//	{
//		Log.logger(o);
//		Log.logger.trace(msg);
//	}

	public static void info(Object o, String msg)
	{
		Log.logger(o);
		Log.logger.info(msg);
	}

	public static void reset()
	{
		Log.logger = null;
		Log.isInitialized = false;
	}

	public static void main(String[] args)
	{
		Log.fatal(Log.class, "Fatal Message!");
		Log.error(Log.class, "Error Message!");
		Log.warn(Log.class, "Warn Message!");
		Log.info(Log.class, "Info Message!");
		Log.debug(Log.class, "Debug Message");
//		Log.trace(Log.class, "Trace Message!");
//		Log.all(Log.class, "Fatal Message!");
//		Log.off(Log.class, "Fatal Message!");
	}
}
