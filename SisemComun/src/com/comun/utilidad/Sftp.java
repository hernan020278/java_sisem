package com.comun.utilidad;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;



/**
 * Nombre de clase = Sftp
 * Descripci�n = Clase encarga de gestionar transacciones por el protocolo sftp
 * @version 1.0
 */

public class Sftp {

	/**
	 *Atributos de Clase
	 **/
	private JSch jsch;
	private Session session;
	private ChannelSftp sftp;

	//---------------------------------------------------------------------

	/**
	 * M�todo que establece la conexi�n con el ftp
	 * @return boolean
	 * @exception IOException
	 */
	public boolean conectar(){		
		boolean conect=true;

		try {
			// Connect to an SFTP server on port 22
			jsch = new JSch();
			session = jsch.getSession("usuario", "host", 22);
			session.setPassword("password");
			// El protocolo SFTP requiere un intercambio de claves
			// al asignarle esta propiedad le decimos que acepte la clave
			// sin pedir confirmaci�n
			Properties prop = new Properties();
			prop.put("StrictHostKeyChecking", "no");
			session.setConfig(prop);			 
			session.connect();			 
			// Abrimos el canal de sftp y conectamos
			sftp = (ChannelSftp) session.openChannel("sftp");
			sftp.connect();		
		} catch (JSchException e) {
			System.out.println("JSchException ="+e.getMessage());
			conect=false;
		}	    
		return conect;
	}

	//-------------------------------------------------------------------
	/**
	 * M�todo que cambia de directorio ,siendo este en el que queremos trabajar
	 * @param String directorio
	 * @return boolean
	 * @exception IOException
	 */
	public boolean cd(String directorio){
		try {
			sftp.cd(directorio);
			return true;
		} catch (SftpException e) {
			System.out.println("SftpException ="+e.getMessage());
		}
		return false;
	}
	//-------------------------------------------------------------------
	/**
	 * M�todo que crea un fichero en el sftp
	 * @param String rutaFicheroSftp (ruta remota)
	 * @param String rutaFichero (ruta local donde esta el fichero)
	 * @return boolean
	 * @exception IOException
	 */
	public boolean crearFichero(String rutaFicheroSftp,String rutaFichero){
		try {
			sftp.put(rutaFichero, rutaFicheroSftp);
			return true;
		} catch (SftpException e) {
			System.out.println("SftpException ="+e.getMessage());
		}
		return false;
	}
	//-------------------------------------------------------------------
	/**
	 * M�todo que elimina un fichero del SFTP
	 * @param String rutaFichero = ruta del fichero en el SFTP
	 * @return boolean
	 * @exception IOException
	 */
	public boolean eliminarFichero(String rutaFichero){
		try {
			sftp.rm(rutaFichero);
			return true;
		} catch (SftpException e) {
			System.out.println("SftpException ="+e.getMessage());
		}
		return false;
	}
	//-----------------------------------------------------------------
	/**
	 * M�todo que cierra la conexion con el FTP
	 * @return boolean
	 * @exception IOException
	 */
	public boolean desconectar(){
		sftp.exit();
		sftp.disconnect();
		session.disconnect();
		return true;		
	}
	//-----------------------------------------------------------------
	/**
	 * M�todo que obtiene el directorio actual, comando pwd
	 * @return String
	 * @exception IOException
	 */
	public String directorioActual(){
		String ruta = null;
		try {
			ruta = sftp.pwd();
		} catch (SftpException e) {
			System.out.println("SftpException ="+e.getMessage());
		}
		return ruta;
	}
	//-----------------------------------------------------------------
	/**
	 * M�todo que obtiene un fichero del SFTP y lo crea en un otro directorio local
	 * @param String rutaFichero = ruta del fichero en el SFTP
	 * @param String rutaLocal = ruta en donde se creara el fichero
	 * @return boolean
	 * @exception IOException
	 */
	public boolean getFichero(String rutaFichero, String rutaLocal){
		boolean retorno=true;
		try {
			OutputStream os = new BufferedOutputStream(new FileOutputStream(rutaLocal));
			// Iniciamos la transferencia
			sftp.get(rutaFichero, os);
		} catch (IOException e) {
			System.out.println("IOException ="+e.getMessage());
			retorno = false;
		} catch (SftpException e) {
			System.out.println("SftpException ="+e.getMessage());
			retorno = false;
		}
		return retorno;		

	}
	//-----------------------------------------------------------------
}
