package com.comun.utilidad;

import java.io.File;
import java.io.FilenameFilter;

public class EvaluarExtensionArchivo implements FilenameFilter {

    @Override
    public boolean accept(File dir, String extension) {
        
        return dir.getName().endsWith(extension);
        
    }
    
    public static void main(String[] args) {
        
        EvaluarExtensionArchivo evaluar = new EvaluarExtensionArchivo();
        
//      String path = "D:\\Proyectos\\Proyecto_Molinos\\Archivos/";
        String path = "D:/Proyectos/Proyecto_Molinos/Archivos/";

        File fi = new File(path);
        File[] listaObjetos = fi.listFiles();
        for (int i = 0; i < listaObjetos.length; i++) {
            
            System.out.println("Archivo : " + listaObjetos[i].getName());
            if (evaluar.accept(listaObjetos[i], ".pdf")) {

                System.out.println("Se encontro archivos pdf: " + listaObjetos[i].getPath());

            }//Fin de if (evaluar.accept(listaObjetos[i], ".pdf"))
        }//Fin de for (int i = 0; i < listaObjetos.length; i++) 
    }//Fin de metodo main
}//Fin de Clase Principal
