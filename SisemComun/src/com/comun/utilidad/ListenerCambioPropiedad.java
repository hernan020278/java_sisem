package com.comun.utilidad;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.table.TableColumn;

public class ListenerCambioPropiedad implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        TableColumn objCol = (TableColumn) evt.getSource();
            System.out.println("Columna : " + objCol.getModelIndex() + " Ancho " + objCol.getWidth());
    }
}//Fin de clase principal
