package com.comun.utilidad.formato;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MaskFormatter;

public class FormatoCombinado extends MaskFormatter {
    public FormatoCombinado(String maskFormat){        
        try {
            super.setMask(maskFormat);
            super.setPlaceholderCharacter('X');
        } catch (ParseException ex) {
            Logger.getLogger(FormatoCombinado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}//Fin de Clase Principal EjemJTextfieldFormattedFecha
