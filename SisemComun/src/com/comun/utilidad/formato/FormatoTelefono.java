package com.comun.utilidad.formato;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MaskFormatter;
@SuppressWarnings("serial")
public class FormatoTelefono extends MaskFormatter {

    public FormatoTelefono(int numCar){        
        try {
            StringBuilder tamTexto = new StringBuilder();
            for (int i = 1; i < numCar; i++) {
                if(i == 4){
                    tamTexto.append("-");
                }
                tamTexto.append("#");
            }
            super.setMask(tamTexto.toString());
            super.setPlaceholderCharacter('0');
        } catch (ParseException ex) {
            Logger.getLogger(FormatoTelefono.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}//Fin de Clase Principal EjemJTextfieldFormattedFecha
