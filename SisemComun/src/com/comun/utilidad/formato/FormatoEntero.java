package com.comun.utilidad.formato;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.text.NumberFormatter;

import com.comun.utilidad.Util;

public class FormatoEntero extends NumberFormatter {

    public FormatoEntero(int numDig) {

        int numMax;
        String cadNumMaxDig = "";
        for (int i = 0; i < (numDig); i++) {

            cadNumMaxDig = cadNumMaxDig + "9";

        }

        NumberFormat formatoDecimal = new DecimalFormat("#0");

        cadNumMaxDig = (Util.isEmpty(cadNumMaxDig)) ? "0" : cadNumMaxDig;
        numMax = Integer.parseInt(cadNumMaxDig);

        setFormat(formatoDecimal);
        setValueClass(Integer.class);
        setMinimum(new Integer(0));
        setMaximum(new Integer(numMax));
        setAllowsInvalid(false);

    }//Fin de FormatoEntero(int numCar)
}//Fin de Clase principal