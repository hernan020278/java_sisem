package com.comun.utilidad.formato;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MaskFormatter;
//Character  	Description
//# - Cualquier Digito
//' - Escape de caracter, used to escape any of the special formatting characters.
//U - Cualquier letra las letras minusculas se convierten a mayusculas.
//L - Cualquier letra las letras mayusculas se convierten a minusculas.
//A - Cualquier caracter o numero.
//? - Cualquier caracter
//* - Cualquier cosa
//H - Any hex character (0-9, a-f or A-F).

public class FormatoFecha extends MaskFormatter {
    //public enum estilo{MAYUSCULAS, MINUSCULAS};

    public FormatoFecha() {
        try {
            
            String tamTexto = "##/##/####";
            super.setMask(tamTexto);
            super.setPlaceholderCharacter('-');
            
        } catch (ParseException ex) {
            Logger.getLogger(FormatoFecha.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//Fin de metodo constructor FormatoTexto    
}