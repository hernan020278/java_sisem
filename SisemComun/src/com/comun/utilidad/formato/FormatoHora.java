package com.comun.utilidad.formato;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MaskFormatter;
/*"yyyy.MM.dd G 'at' HH:mm:ss z"    2001.07.04 AD at 12:08:56 PDT
  "EEE, MMM d, ''yy"                Wed, Jul 4, '01
  "h:mm a"                          12:08 PM
  "hh 'o''clock' a, zzzz"           12 o'clock PM, Pacific Daylight Time
  "K:mm a, z"                       0:08 PM, PDT
  "yyyyy.MMMMM.dd GGG hh:mm aaa"    02001.July.04 AD 12:08 PM
  "EEE, d MMM yyyy HH:mm:ss Z"      Wed, 4 Jul 2001 12:08:56 -0700
  "yyMMddHHmmssZ"                   010704120856-0700
  "yyyy-MM-dd'T'HH:mm:ss.SSSZ"      2001-07-04T12:08:56.235-0700*/
@SuppressWarnings("serial")
public class FormatoHora extends MaskFormatter {

    public FormatoHora(String formato) {
        try {
            
            String tamTexto = formato != null ? formato : "##:##:##";
            super.setMask(tamTexto);
            super.setPlaceholderCharacter('0');
            
        } catch (ParseException ex) {
            Logger.getLogger(FormatoFecha.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//Fin de metodo constructor FormatoTexto
}//Fin de Clase Principal EjemJTextfieldFormattedFecha
