package com.comun.utilidad.formato;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.swing.text.NumberFormatter;

import com.comun.utilidad.Util;

public class FormatoDecimal extends NumberFormatter {

    public FormatoDecimal(int numDig, int numDec) {

        double numMax;
        String cadNumMaxDig = "", cadNumMaxDec = "";
        String cadNumDec = "";
        for (int i = 0; i < (numDig - numDec); i++) {

            cadNumMaxDig = cadNumMaxDig + "9";

        }

        for (int i = 0; i < numDec; i++) {

            cadNumDec = cadNumDec + ((i <= 1) ? "0" : "#");
            cadNumMaxDec = cadNumMaxDec + "9";

        }

        DecimalFormat formatoDecimal = new DecimalFormat("###0." + cadNumDec, DecimalFormatSymbols.getInstance(new Locale("en", "US")));

        cadNumMaxDig = (Util.isEmpty(cadNumMaxDig)) ? "0" : cadNumMaxDig;
        cadNumMaxDec = (Util.isEmpty(cadNumMaxDec)) ? "0" : cadNumMaxDec;
        numMax = Double.parseDouble(cadNumMaxDig + "." + cadNumMaxDec);

        setFormat(formatoDecimal);
        setValueClass(Double.class);
        setMinimum(new Double(0.0));
        setMaximum(new Double(numMax));
        setAllowsInvalid(false);

    }//Fin de metodo constructor    
}//Fin de clase principal
