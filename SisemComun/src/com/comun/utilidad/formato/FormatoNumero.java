package com.comun.utilidad.formato;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MaskFormatter;

public class FormatoNumero extends MaskFormatter{
    
    public FormatoNumero(int numCar){        
        try {
            StringBuilder tamTexto = new StringBuilder();
            for (int i = 1; i <= numCar; i++) {
                tamTexto.append("#");
            }
            super.setMask(tamTexto.toString());
            super.setPlaceholderCharacter('0');
        } catch (ParseException ex) {
            Logger.getLogger(FormatoNumero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}//Fin de clase principal
