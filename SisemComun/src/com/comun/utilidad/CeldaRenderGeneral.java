/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comun.utilidad;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class CeldaRenderGeneral extends DefaultTableCellRenderer {

	private Color AZUL = new Color(0, 51, 255);
	private Color VERDE = new Color(0, 153, 51);
	private Color VERDECLARO = new Color(196, 237, 69);
	private Color ROJO = new Color(204, 0, 0);
	private Color NARANJA = new Color(255, 153, 0);
	private Color MARRON = new Color(153, 102, 0);
	private Color CREMA = new Color(255, 255, 153);
	private Color AMARILLO = new Color(255, 255, 0);
	private Color NEGRO = new Color(0, 0, 0);
	private Color BLANCO = new Color(255, 255, 255);

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

		Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		if (value instanceof Boolean || value instanceof BigDecimal || value instanceof Integer || value instanceof Date || value instanceof Time) {
			
			super.setHorizontalAlignment(SwingConstants.CENTER);
			
		} else if (value instanceof String) {
			if (Util.getInst().isDate(String.valueOf(value)) || Util.getInst().isTime(String.valueOf(value))) {

				super.setHorizontalAlignment(SwingConstants.CENTER);

			}// Fin de if(isDate(String.valueOf(value))
			else {

				super.setHorizontalAlignment(SwingConstants.LEFT);

			}
		} else if (value instanceof Double) {

			super.setHorizontalAlignment(SwingConstants.RIGHT);

		}

		if (isSelected) {// CELDA ESTA SELECCIONADO
			if (hasFocus) {// Celda que tiene el Foco

				cell.setBackground(MARRON);

				if (value instanceof Integer) {
					Integer importe = (Integer) value;
					if (importe < 0) {

						cell.setForeground(ROJO);

					} else if (importe == 0) {

						cell.setForeground(MARRON);

					} else if (importe > 0) {

						cell.setForeground(CREMA);

					}

				} else if (value instanceof Double) {
					Double importe = (Double) value;
					if (importe < 0.00) {

						cell.setForeground(ROJO);

					} else if (importe == 0.00) {

						cell.setForeground(MARRON);

					} else if (importe > 0.00) {

						cell.setForeground(CREMA);
					}
				} else if (value instanceof BigDecimal) {
					BigDecimal importe = (BigDecimal) value;
					if (importe.doubleValue() < 0) {

						cell.setForeground(ROJO);

					} else if (importe.doubleValue() == 0.00) {

						cell.setForeground(MARRON);

					} else if (importe.doubleValue() > 0.00) {

						cell.setForeground(CREMA);
					}
				} else {
					cell.setForeground(CREMA);
				}

			}// if (hasFocus) {// Celda que tiene el Foco
			else {// NO TIENE EL FOCO

				cell.setBackground(CREMA);
				if (value instanceof Integer) {
					Integer importe = (Integer) value;
					if (importe < 0) {

						cell.setForeground(ROJO);

					} else if (importe == 0) {

						cell.setForeground(CREMA);

					} else if (importe > 0) {

						cell.setForeground(MARRON);

					}

				} else if (value instanceof Double) {
					Double importe = (Double) value;
					if (importe < 0.00) {

						cell.setForeground(ROJO);

					} else if (importe == 0.00) {

						cell.setForeground(CREMA);

					} else if (importe > 0.00) {

						cell.setForeground(MARRON);

					}
				} else if (value instanceof BigDecimal) {
					BigDecimal importe = (BigDecimal) value;
					if (importe.doubleValue() < 0) {

						cell.setForeground(ROJO);

					} else if (importe.doubleValue() == 0.00) {

						cell.setForeground(CREMA);

					} else if (importe.doubleValue() > 0.00) {

						cell.setForeground(MARRON);
					}
        } else if (Util.getInst().isTime(String.valueOf(value))){
          cell.setForeground(ROJO);
        } else {
          cell.setForeground(MARRON);
				}
			}// else {// NO TIENE EL FOCO
		}// if (isSelected) {//CELDA ESTA SELECCIONADO
		else {

			cell.setBackground(BLANCO);
			if (table.getColumnCount() == 11) {
				if (table.getValueAt(row, 10) instanceof String) {
					String ven_estado = (String) table.getValueAt(row, 10);
					if (ven_estado.equals("1000")) {
						cell.setBackground(VERDECLARO);
					}
				}
			}
			if (value instanceof Integer) {

				Integer importe = (Integer) value;
				if (importe < 0.00) {

					cell.setForeground(ROJO);

				} else if (importe == 0.00) {

					cell.setForeground(BLANCO);

				} else if (importe > 0.00) {

					cell.setForeground(VERDE);

				}
			} else if (value instanceof Double) {
				Double importe = (Double) value;
				if (importe < 0.00) {

					cell.setForeground(ROJO);

				} else if (importe == 0.00) {

					cell.setForeground(BLANCO);

				} else if (importe > 0.00) {

					cell.setForeground(AZUL);

				}
			} else if (value instanceof BigDecimal) {
				BigDecimal importe = (BigDecimal) value;
				if (importe.doubleValue() < 0) {

					cell.setForeground(ROJO);

				} else if (importe.doubleValue() == 0.00) {

					cell.setForeground(BLANCO);

				} else if (importe.doubleValue() > 0.00) {

					cell.setForeground(AZUL);
				}

			} else if (value instanceof String) 
			{
				if (Util.getInst().isDate(String.valueOf(value)) || Util.getInst().isTime(String.valueOf(value))) 
				{
					cell.setForeground(ROJO);
				}
				else 
				{
					cell.setForeground(MARRON);
				}
			} else if (value instanceof Date || value instanceof Time) {
				if(value != null){
//				  System.out.println(this.getClass().getSimpleName() +  value.getClass() + " : " + value);
				}
				cell.setForeground(ROJO);
			} else if (value instanceof JComboBox) {

				cell.setForeground(NARANJA);

			}
		}
		return cell;
	}
}
