package com.comun.utilidad;

import java.awt.Color;
import java.awt.Component;
import java.awt.Toolkit;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.border.LineBorder;

public class ItemListaRender extends DefaultListCellRenderer
{
	private ImageIcon imgIco = new ImageIcon();
	private String ruta;
	
	public ItemListaRender()
	{
		ruta = AplicacionGeneral.getInstance().obtenerRutaImagenes();
	}
	private ImageIcon obtenerImagen(String imagen)
	{
		imgIco.setImage(Toolkit.getDefaultToolkit().getImage(ruta + imagen));
		return imgIco;
	}

	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
	{
		// for default cell renderer behavior
		JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		String valor = (String) value;
		String[] matrizValor = null;
		if (valor.contains("#"))
		{
			matrizValor = valor.split("#");
		}
		if (matrizValor != null)
		{
			ImageIcon imgIco = obtenerImagen(matrizValor[1]);
			label.setText(matrizValor[0]);
			label.setIcon(imgIco);
			// imgIco.setImageObserver(label);
		}
		else
		{
			label.setText(valor);
		}
		if(isSelected)
		{
			label.setOpaque(true);
			label.setBackground(Color.gray);
		}
		else
		{
			label.setOpaque(false);
		}
		return label;
	}
}
