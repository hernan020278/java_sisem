/*
 * Created on Aug 7, 2003
 */
package com.comun.utilidad;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Random;
import java.util.Stack;

import javax.swing.SwingUtilities;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import com.comun.referencia.Estado;

/**
 * @author Administrator
 */
public class KeyGenerator {
	private Stack stack = null;

	private static KeyGenerator SINGLETON = new KeyGenerator();

	private KeyGenerator() {
		initStack();
	}

	public static KeyGenerator getInst() {
		if (SINGLETON == null) {
			SINGLETON = new KeyGenerator();
		}
		return SINGLETON;
	}

	private void initStack() {
		stack = new Stack();
		for (int i = 99; i > -1; i--) {
			stack.push(new Long(i));
		}
	}

	public synchronized Long getUniqueKey() {
		Long key = null;
		key = new Long(getMillisecondsPortion());
		String keyString = key.toString();
		String stackNumber = getNextNumberFromStack().toString();
		if (stackNumber.length() == 1) {
			StringBuffer sb = new StringBuffer();
			sb.append("0");
			sb.append(stackNumber);
			stackNumber = sb.toString();
		}
		keyString += stackNumber;
		return Long.valueOf(keyString);
	}

	public synchronized BigDecimal getKyTabla() {
		Long key = null;
		key = new Long(getMillisecondsPortion());
		String keyString = key.toString();
		String stackNumber = getNextNumberFromStack().toString();
		if (stackNumber.length() == 1) {
			StringBuffer sb = new StringBuffer();
			sb.append("0");
			sb.append(stackNumber);
			stackNumber = sb.toString();
		}
		keyString += stackNumber;
		return new BigDecimal(keyString);
	}

	private Long getNextNumberFromStack() {
		Long numberFromStack = null;
		try {
			if (stack.empty()) {
				initStack();
			}
			numberFromStack = (Long) stack.pop();
		} catch (Exception e) {
			// system.out.println(e.toString());
		} finally {
			return numberFromStack;
		}
	}

	public Long getMillisecondsPortion() {
		long milliseconds = 0;
		try {
			Thread.sleep(10);
			Calendar cal = Calendar.getInstance();
			Calendar current = Calendar.getInstance();
			cal.set(2003, Calendar.SEPTEMBER, 05, 00, 00, 01);
			cal.set(cal.MILLISECOND, 0);
			long time = cal.getTimeInMillis();
			long currentTime = current.getTimeInMillis();
			milliseconds = currentTime - time;
		} catch (Exception e) {
			// system.out.println(e.toString());
		} finally {
			return milliseconds;
		}
	}

	public String obtenerCodigoBarra() {

		String codigoBarra = getMillisecondsPortion().toString();
		if (codigoBarra.length() > 12) {
			codigoBarra.substring(0, 12);
		}
		return codigoBarra;
	}

	public Long obtenerLongCodigo() {

		String codigoBarra = getMillisecondsPortion().toString();
		if (codigoBarra.length() > 12) {
			codigoBarra.substring(0, 12);
		}
		return Long.valueOf(codigoBarra);
	}
	
	public String getCadenaAlfanumAleatoria(int longitud) {
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while (i < longitud) {
			char c = (char) r.nextInt(255);
			if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
				cadenaAleatoria += c;
				i++;
			}
		}
		return cadenaAleatoria;
	}

	public String encriptar(String cadena, String clave)
	{
		StandardPBEStringEncryptor spe = new StandardPBEStringEncryptor();
		spe.setPassword(clave);
		return spe.encrypt(cadena);
	}

	public String desencriptar(String cadena, String clave)
	{
		StandardPBEStringEncryptor s = new StandardPBEStringEncryptor();
		s.setPassword(clave);
		String devuelve = "";
		try
		{
			devuelve = s.decrypt(cadena);
		} catch (Exception e)
		{
		}
		return devuelve;
	}
	
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				KeyGenerator ukg = KeyGenerator.getInst();
//				Long porcion = ukg.getMillisecondsPortion();
				PropiedadesExterna props = new PropiedadesExterna("config");
				props.setPropiedades("sistema-restriccion", ukg.encriptar("17/03/2014", Estado.CLAVE));
				System.out.println("Codigo : " + ukg.encriptar("17/03/2014", Estado.CLAVE));

			}
		});

	}
}
