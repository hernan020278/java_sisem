package com.comun.utilidad;

import java.util.List;

import com.comun.entidad.Agrupacion;
import com.comun.entidad.Area;
import com.comun.entidad.Asiasistencia;
import com.comun.entidad.Asictrlasistencia;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asdetferiado;
import com.comun.entidad.Asiplantillahorario;
import com.comun.entidad.Asiplantillaturno;
import com.comun.entidad.Asirepositorio;
import com.comun.entidad.Asiturno;
import com.comun.entidad.Clase;
import com.comun.entidad.Cliente;
import com.comun.entidad.Componente;
import com.comun.entidad.CtrlTabla;
import com.comun.entidad.DetComponente;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Etiqueta;
import com.comun.entidad.FicControl;
import com.comun.entidad.Ficha;
import com.comun.entidad.Grupo;
import com.comun.entidad.Historia;
import com.comun.entidad.Indestado;
import com.comun.entidad.Indicador;
import com.comun.entidad.Mensaje;
import com.comun.entidad.Muestra;
import com.comun.entidad.Organizacion;
import com.comun.entidad.Pedido;
import com.comun.entidad.PerfilSeguridad;
import com.comun.entidad.Produccion;
import com.comun.entidad.Producto;
import com.comun.entidad.Propiedad;
import com.comun.entidad.Red;
import com.comun.entidad.RegistroCaso;
import com.comun.entidad.RegistroConsulta;
import com.comun.entidad.Seguridad;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;

public class Auxiliar {

	public String organizacionIde = "";
	public String orgGen;
	public Componente cmp;
	public Usuario usuSes;
	public Etiqueta eti;
	public Propiedad pro;
	public PerfilSeguridad perfSeg;
	public Organizacion org;
	public Seguridad seg;
	/*
	 * OBJETOS DEL MODEL RELACIONAL
	 */
	public CtrlTabla ctrlTab;
	public Cliente cli;
	public Pedido ped;
	public DetPedido detPed;
	public DetComponente detCmp;
	public DetPedido detPedTmp;
	public Producto prdGen;
	public Producto prd;
	public Producto mod;
	public Producto tal;
	public Producto prdPad;
	public Grupo gru;
	public Clase cla;
	public Clase claCol;
	public Grupo gruCol;
	public Grupo gruMod;
	public Grupo gruPrd;
	public Grupo gruTal;
	public Area are;
	public SubArea subAre;
	public Ficha fic;
	public FicControl ficCtrl;
	public Usuario usu;
	public Usuario ope;
	public Produccion prc;
	public Asiasistencia asiasis;
	public Asiasistencia asiAsisLast;
	public Asictrlasistencia asictrlasi;
	public Asdetferiado asdfer;
  public Asiplantillahorario asiplanhora;
  public Asiplantillaturno asiplanturn;
	public Asirepositorio asirepo;
	public Asiturno asiturn;
	public Asihorario asihora;
	public List lisUsu;
	public List lisOrg;
	public List lisSeg;
	public List lisPerfSeg;
	public List lisAsturn;
	public List lista;
	public Grid grid;
	public Mensaje msg;
	public Agrupacion agru;
	public String bookMarkBarraFicha;
	public String bookMarkBarraSubArea;
	public int contadorMarcador;
	/**
	 * lista de variables para sistema asesor
	 */
	public Indicador indDia;
	public List lisRca;
	public List lisInd;
	public RegistroCaso rgcDia;
	public Indicador indAct;
	public RegistroCaso rgcAct;
	public RegistroCaso rgcTra;
	public Usuario pac;
	public Muestra mue;
	public Historia his;
	public List lisRed;
	public List lisRco;
	public RegistroConsulta rcoAct;
	public Usuario psi;
	public List lisIes;
	public List lisRef;
	public Indestado est;
	public Red red;
	public Indicador indSin;
	public Indicador indTra;
	public RegistroCaso rgcSin;
	public Pagina pagNew;
	public Pagina pagAct;

	protected static Auxiliar INSTANCE = null;

	public static Auxiliar getInstance()
	{

		if(Auxiliar.INSTANCE == null)
		{
			Auxiliar.INSTANCE = new Auxiliar();
		}
		return Auxiliar.INSTANCE;
	}

	public Auxiliar()
	{

		msg = new Mensaje();
	}
}
