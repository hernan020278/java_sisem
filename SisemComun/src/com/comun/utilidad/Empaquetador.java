package com.comun.utilidad;

/*
 * Zip.java
 *
 * Created on 17 de enero de 2008, 05:04 PM
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Empaquetador {

	public static void main(String[] args) {

		try {
			comprimir(new File("C:\\sisem_05032014.bak"));
		} catch (IOException ex) {
			Logger.getLogger(Empaquetador.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void comprimir(File file) throws IOException {

		byte[] b = new byte[512];
		FileOutputStream out = new FileOutputStream(file.getName() + ".zip");
		ZipOutputStream zout = new ZipOutputStream(out);
		InputStream in = new FileInputStream(file);
		ZipEntry e = new ZipEntry(file.getName());
		zout.putNextEntry(e);
		int len = 0;
		while ((len = in.read(b)) != -1) {
			zout.write(b, 0, len);
		}
		zout.closeEntry();
		print(e);
		zout.close();
	}

	public static void print(ZipEntry e) {

		PrintStream err = System.err;
		err.print("added " + e.getName());
		if(e.getMethod() == ZipEntry.DEFLATED) {
			long size = e.getSize();
			if(size > 0) {
				long csize = e.getCompressedSize();
				long ratio = ((size - csize) * 100) / size;
				err.println(" (deflated " + ratio + "%)");
			} else {
				err.println(" (deflated 0%)");
			}
		} else {
			err.println(" (stored 0%)");
		}
	}
}
