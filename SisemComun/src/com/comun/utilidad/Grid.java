package com.comun.utilidad;

import java.util.List;

public class Grid {

	private int page_rows;
	private Paging paging;
	private List items;

	public int getPage_rows() {

		return page_rows;
	}

	public void setPage_rows(int page_rows) {

		this.page_rows = page_rows;
	}

	public Paging getPaging() {

		return paging;
	}

	public void setPaging(Paging paging) {

		this.paging = paging;
	}

	public List getItems() {

		return items;
	}

	public void setItems(List items) {

		this.items = items;
	}
}
