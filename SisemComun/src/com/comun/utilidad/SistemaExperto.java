package com.comun.utilidad;

import smile.Network;
import smile.SMILEException;
import smile.learning.DataMatch;
import smile.learning.DataSet;
import smile.learning.EM;

public class SistemaExperto {

	private double[] varTR_HPS = {0.50, 0.50, 0.50, 0.50};

	public SistemaExperto() {

	}

	/**
	 * METODO 1: APRENDIENDO UNA RED BAYESIANA
	 */
	public void aprendiendoRedBayes(String fuente, String destino)
	{

		try
		{
			DataSet dataset = new DataSet();
			dataset.readFile(fuente);
			Network network = new Network();
			network.readFile(destino);
			DataMatch[] datamatch = dataset.matchNetwork(network);
			EM em = new EM();
			em.learn(dataset, network, datamatch);
			network.writeFile(destino);
			System.out.println("Mineria de datos completado satisfacoriamente...!");
		} catch (SMILEException e)
		{
			System.out.println(e.getMessage());
		}
	}

	/**
	 * METODO 1: ACTUALIZAR RED BAYESIANA
	 */
	public void actualizarRedBayes(String parIdeNod, double[] parDefMat) {

		/*
		 * DEFINICION.- ES EL VALOR DE LA MATRIS DE UN NODO
		 * 
		 * DELIRIOS SOMATICOS
		 * ==================
		 * TRUE FALSE
		 * TRUE 0.3 0.6
		 * FALSE 0.7 0.4
		 */
		try
		{
			Network net = new Network();
			net.readFile("C:\\sisem\\bin\\RedPersonalidad.xdsl");
			net.setNodeDefinition(parIdeNod, parDefMat);
			net.updateBeliefs();
			/*
			 * double[] valDefIdeNod = net.getNodeDefinition(parIdeNod);
			 * for (int outcomeIndex = 0; outcomeIndex < valDefIdeNod.length; outcomeIndex++) {
			 * 
			 * System.out.println("Valor : " + valDefIdeNod[outcomeIndex]);
			 * 
			 * }
			 */
			net.writeFile("C:\\sisem\\bin\\RedPersonalidad.xdsl");
		} catch (SMILEException e) {
			System.out.println(e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------------------
	/**
	 * METODO 1: INICIALIZAR RED BAYESIANA
	 */
	public void inicializarRed()
	{

		String[] lisIdeNod;
		try {
			Network net = new Network();
			net.readFile("C:\\sisem\\bin\\RedPersonalidad.xdsl");
			net.updateBeliefs();
			lisIdeNod = net.getAllNodeIds();
			for(int iteIdeNod = 0; iteIdeNod < lisIdeNod.length; iteIdeNod++)
			{
				String nodo = lisIdeNod[iteIdeNod];
				String[] estados = net.getOutcomeIds(nodo);
				double[] valores = net.getNodeValue(nodo);
				int arrayValores = estados.length;
				String[] padres = net.getParentIds(nodo);
				for(int itePad = 0; itePad < padres.length; itePad++)
				{
					String padreNodo = padres[itePad];
					String[] estadosPadre = net.getOutcomeIds(padreNodo);
					arrayValores = arrayValores * estadosPadre.length;
				}
				double[] aValoresIniciales = new double[arrayValores];
				double valor1 = 0.00;
				double valor2 = 0.00;
				int ideEst = 0;
				for(int iteValIni = 0; iteValIni < aValoresIniciales.length; iteValIni++)
				{
					if(estados.length == 2)
					{
						ideEst = ideEst + 1;
						if(ideEst == 1)
						{
							boolean bucle = true;
							while (bucle)
							{
								valor1 = Double.parseDouble(String.valueOf(Util.getInst().aleatorio(1, 1000000))) / 1000000;
								if(valor1 < 1)
								{
									aValoresIniciales[iteValIni] = valor1;
									bucle = false;
								}
							}
						}
						if(ideEst == 2)
						{
							aValoresIniciales[iteValIni] = 1 - valor1;
							ideEst = 0;
						}
					}
					else if(estados.length == 3)
					{
						ideEst = ideEst + 1;
						if(ideEst == 1)
						{
							boolean bucle = true;
							while (bucle)
							{
								valor1 = Double.parseDouble(String.valueOf(Util.getInst().aleatorio(1, 1000000))) / 1000000;
								if(valor1 < 1)
								{
									aValoresIniciales[iteValIni] = valor1;
									bucle = false;
								}
							}
						}
						if(ideEst == 2)
						{
							boolean bucle = true;
							while (bucle)
							{
								valor2 = Double.parseDouble(String.valueOf(Util.getInst().aleatorio(1, 1000000))) / 1000000;
								if((valor2 + valor1) < 1)
								{
									aValoresIniciales[iteValIni] = valor2;
									bucle = false;
								}
							}
						}
						if(ideEst == 3)
						{
							aValoresIniciales[iteValIni] = 1 - valor1 - valor2;
							ideEst = 0;
						}
					}
				}
				net.setNodeDefinition(lisIdeNod[iteIdeNod], aValoresIniciales);
			}
			net.updateBeliefs();
			net.writeFile("C:\\sisem\\bin\\RedPersonalidad.xdsl");
			System.out.println("Archivo generado con exito...!!!");
		} catch (SMILEException e) {
			System.out.println(e.getMessage());
		}
	}

	public static double[] obtenerValoresNodo(String nodoIdentificador)
	{

		double[] arrayValores = null;
		try
		{
			Network net = new Network();
			net.readFile("C:\\sisem\\bin\\RedPersonalidad.xdsl");
			net.updateBeliefs();
			arrayValores = net.getNodeValue(nodoIdentificador);
		} catch (SMILEException e) {
			System.out.println(e.getMessage());
		}
		return arrayValores;
	}

	public static String[] obtenerEstadosNodo(String nodoIdentificador)
	{

		String[] arrayEstados = null;
		try {
			Network net = new Network();
			net.readFile("C:\\sisem\\bin\\RedPersonalidad.xdsl");
			// net.updateBeliefs();
			arrayEstados = net.getOutcomeIds(nodoIdentificador);
		} catch (SMILEException e) {
			System.out.println(e.getMessage());
		}
		return arrayEstados;
	}

	public void inferirRedBayesiana(String parNodoSintoma) {

		try {
			Network net = new Network();
			net.readFile("C:\\sisem\\bin\\RedPersonalidad.xdsl");
			// ---- We want to compute P("Forecast" = Moderate) ----
			// Updating the network:
			net.updateBeliefs();
			// Creating the node "Forecast":
			net.getNode(parNodoSintoma);
			// obtenemos el indice para la salida tru
			String[] outcomeNodo = net.getOutcomeIds(parNodoSintoma);
			int outcomeIndex;
			for(outcomeIndex = 0; outcomeIndex < outcomeNodo.length; outcomeIndex++) {
				if("TRUE".equals(outcomeNodo[outcomeIndex])) {
					break;
					// Getting the value of the probability:
				}
			}
			double[] aValues = net.getNodeValue("Forecast");
			double P_ForecastIsModerate = aValues[outcomeIndex];
			System.out.println("P(\"Forecast\" = Moderate) = " + P_ForecastIsModerate);
			// ---- We want to compute P("Success" = Failure | "Forecast" = Good) ----
			// Introducing the evidence in node "Forecast":
			net.setEvidence("Forecast", "Good");
			// Updating the network:
			net.updateBeliefs();
			// Getting the index of the "Failure" outcome:
			String[] aSuccessOutcomeIds = net.getOutcomeIds("Success");
			for(outcomeIndex = 0; outcomeIndex < aSuccessOutcomeIds.length; outcomeIndex++) {
				if("Failure".equals(aSuccessOutcomeIds[outcomeIndex])) {
					break;
					// Getting the value of the probability:
				}
			}
			aValues = net.getNodeValue("Success");
			double P_SuccIsFailGivenForeIsGood = aValues[outcomeIndex];
			System.out.println("P(\"Success\" = Failure | \"Forecast\" = Good) = " + P_SuccIsFailGivenForeIsGood);
			// ---- We want to compute P("Success" = Success | "Forecast" = Poor) ----
			// Clearing the evidence in node "Forecast":
			net.clearEvidence("Forecast");
			// Introducing the evidence in node "Forecast":
			net.setEvidence("Forecast", "Good");
			// Updating the network:
			net.updateBeliefs();
			// Getting the index of the "Failure" outcome:
			aSuccessOutcomeIds = net.getOutcomeIds("Success");
			for(outcomeIndex = 0; outcomeIndex < aSuccessOutcomeIds.length; outcomeIndex++) {
				if("Failure".equals(aSuccessOutcomeIds[outcomeIndex])) {
					break;
					// Getting the value of the probability:
				}
			}
			aValues = net.getNodeValue("Success");
			double P_SuccIsSuccGivenForeIsPoor = aValues[outcomeIndex];
			System.out.println("P(\"Success\" = Success | \"Forecast\" = Poor) = " + P_SuccIsSuccGivenForeIsPoor);
		} catch (SMILEException e) {
			System.out.println(e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------------------
	public static void main(String[] args) {

		SistemaExperto redBayCon = new SistemaExperto();
		redBayCon.inicializarRed();
		// redBayCon.aprendiendoRedBayes("C:\\sisem\\bin\\RedPersonalidad.txt", "C:\\sisem\\bin\\RedPersonalidad.xdsl");
	}
}
