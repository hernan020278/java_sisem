package com.comun.utilidad;

import java.awt.GridBagConstraints;
import java.awt.Insets;

public class UbicarObjeto extends GridBagConstraints{
	private static final long serialVersionUID = 1L;
    protected GridBagConstraints gbc;
    
    public void ubicarObj(int posX, int posY, int numCeldaX, int numCeldaY, 
                                     boolean estirarX, boolean estirarY, int ancho, int alto, 
                                     String ubicacion, int bordeSup, int bordeIzq, int bordeInf, int bordeDer) {
         gbc = new GridBagConstraints();
        /********************************************************
         * Ubicar un objeto en las coordenadas de una cuadricula*
         ********************************************************/
        gbc.gridx = posX;
        gbc.gridy = posY;
        
        /***************************************************
         * Determinar cuantas celdas va ha ocupar un objeto*
         ***************************************************/
        gbc.gridwidth = numCeldaX;
        gbc.gridheight = numCeldaY;

        /**********************************************************
         * Establecer valores iniciales para estirar en X o Y     *
         **********************************************************/
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        
        /***************************************************************
         * Establecer que un objeto se estire en el eje X o en el eje Y*
         ***************************************************************/
        if (estirarX == true && estirarY == true) {
            
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.fill = GridBagConstraints.BOTH;
            
        } else if (estirarX == false && estirarY == false) {
            gbc.weightx = 0.0;
            gbc.weighty = 0.0;
            gbc.fill = GridBagConstraints.NONE;
            /*********************************************************
             * Ubicar un objeto ARRIBA - ABAJO - DERECHA - IZQUIERDA *
             *********************************************************/
            if (ubicacion.equals("izquierda")) {
                gbc.anchor = GridBagConstraints.WEST;
            } else if (ubicacion.equals("derecha")) {
                gbc.anchor = GridBagConstraints.EAST;
            } else if (ubicacion.equals("arriba")) {
                gbc.anchor = GridBagConstraints.NORTH;
            } else if (ubicacion.equals("abajo")) {
                gbc.anchor = GridBagConstraints.SOUTH;
            } else if (ubicacion.equals("centro")) {
                gbc.anchor = GridBagConstraints.CENTER;
            }
        } else if (estirarX == true) {
            gbc.weightx = 1.0;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            /*********************************************************
             * Ubicar un objeto ARRIBA - ABAJO - DERECHA - IZQUIERDA *
             *********************************************************/
            if (ubicacion.equals("north")) {
                gbc.anchor = GridBagConstraints.NORTH;
            } else if (ubicacion.equals("south")) {
                gbc.anchor = GridBagConstraints.SOUTH;
            } else if (ubicacion.equals("center")) {
                gbc.anchor = GridBagConstraints.CENTER;
            }
        } else if (estirarY == true) {
            gbc.weighty = 1.0;
            gbc.fill = GridBagConstraints.VERTICAL;
            /*********************************************************
             * Ubicar un objeto ARRIBA - ABAJO - DERECHA - IZQUIERDA *
             *********************************************************/
            if (ubicacion.equals("east")) {
                gbc.anchor = GridBagConstraints.EAST;
            } else if (ubicacion.equals("west")) {
                gbc.anchor = GridBagConstraints.WEST;
            } else if (ubicacion.equals("center")) {
                gbc.anchor = GridBagConstraints.CENTER;
            }
        }

        /********************************************************
         * Establecer los Bordes respecto a la celda ubicada    *
         ********************************************************/
        gbc.insets = new Insets(bordeSup, bordeIzq, bordeInf, bordeDer);

        /********************************************************
         * Numero de Celdas que va ha ocupar                    *
         ********************************************************/
        gbc.ipadx = ancho;
        gbc.ipady = alto;
    }//Fin de Metodo Constructor de CrearGridBag    
    
    public GridBagConstraints getGBC(){    
        return gbc;
    }
}
