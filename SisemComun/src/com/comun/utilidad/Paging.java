package com.comun.utilidad;


public class Paging {

	private int page;
	private int total_items;
	private int items_page;
	private int total_pages;
	private int total_page_items;

	public int getPage() {

		return page;
	}

	public void setPage(int page) {

		this.page = page;
	}

	public int getTotal_items() {

		return total_items;
	}

	public void setTotal_items(int total_items) {

		this.total_items = total_items;
	}

	public int getItems_page() {

		return items_page;
	}

	public void setItems_page(int items_page) {

		this.items_page = items_page;
	}

	public int getTotal_pages() {

		return total_pages;
	}

	public void setTotal_pages(int total_pages) {

		this.total_pages = total_pages;
	}

	public int getTotal_page_items() {

		return total_page_items;
	}

	public void setTotal_page_items(int total_page_items) {

		this.total_page_items = total_page_items;
	}
}
