package com.comun.utilidad;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class sftpex {

	public static void main(String[] args) {

		// TODO code application logic here
		String user = "testuser";
		String server = "testserver.example.com";
		String pass = "testpass";
		String khfile = "C:\\known_hosts";
		String identityfile = "C:\\id_rsa";
		JSch jsch = null;
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			jsch = new JSch();
			session = jsch.getSession(user, server, 22);
			session.setPassword(pass);
			jsch.setKnownHosts(khfile);
			jsch.addIdentity(identityfile);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			System.out.println("Starting File Upload:");
			String fsrc = "/tmp/abc.txt", fdest = "/tmp/cde.txt";
			channelSftp.put(fsrc, fdest);
			channelSftp.get(fdest, "/tmp/testfile.bin");
		} catch (Exception e) {
			e.printStackTrace();
		}
		channelSftp.disconnect();
		session.disconnect();
	}
}