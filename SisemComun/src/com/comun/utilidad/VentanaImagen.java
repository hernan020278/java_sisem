package com.comun.utilidad;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.utilidad.swing.PanelImgFoto;

public final class VentanaImagen extends JComponent {

	private ImageIcon imgIco;
	private Image img;
	private URL urlClassPath;

	public VentanaImagen(ImageIcon parImgIco) {

		if(parImgIco == null)
		{
			String orgIde = OrganizacionGeneral.getOrgcodigo();
			String ruta = AplicacionGeneral.getInstance(OrganizacionGeneral.getOrgcodigo()).obtenerRutaImagenes();

			File file = new File(ruta + "fondo.png");
			if (file.exists()) {
				imgIco = new ImageIcon(file.getPath());
			}
			else {
				imgIco = AplicacionGeneral.getInstance().obtenerImagen("fondo.png");
			}

			img = imgIco.getImage();
		}//if(parImgIco == null)
		else
		{
			img = parImgIco.getImage();
		}
	}// Fin de Constructor

	@Override
	public void paintComponent(Graphics graphics) {
		Graphics2D graphics2D = (Graphics2D) graphics;

		// creamos un gradiente translucido
		// Color[] colors = new Color[]{
		// new Color(0, 0, 0, 0), new Color(0.3f, 0.3f, 0.3f, 1f), new Color(0.3f, 0.3f, 0.3f, 1f), new Color(0, 0, 0, 0)};
		// float[] stops = new float[]{0, 0.2f, 0.8f, 1f};
		// LinearGradientPaint paint = new LinearGradientPaint(
		// new Point(0, 0),
		// new Point(500, 0),
		// stops, colors);

		Dimension dimensionPanel = getSize();

		if (img != null) {

			graphics2D.drawImage(img, 0, 0, dimensionPanel.width, dimensionPanel.height, this);

		}// Fin de Boque Verdadero si hay alguna imagen para dibujar
	}// Fin de paintComponent(Graphics graphics)

	public void setImagen(ImageIcon parImgIco) {

		this.imgIco = parImgIco;
		try {

			img = parImgIco.getImage();

		}
		catch (Exception e) {
			setImagen(AplicacionGeneral.getInstance().obtenerImagen("fondo.png"));

		}// Fin de Metoo Catch
	}// Fin de Metodo SetImagen para colocar una Imagen en al Panmel
}// Fin d eClase Principal
