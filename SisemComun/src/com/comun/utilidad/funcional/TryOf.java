package com.comun.utilidad.funcional;

import io.vavr.control.Try;

public class TryOf {
  public static void main(String[] args) {

    System.out.println(Util.getArrayString()[0]);

  }

  static class Util {
    public static void imprimirSuccess() {
      System.out.println("Evaluacion satisfactorio");
    }

    public static void imprimirError() {
      System.out.println("Ocurrio un error");
    }

    public static String[] getArrayString() {
      String prueba1 = "UNO-DOS";
      String[] prueba2 = {"TRES", "CUATRO"};
      String test = null;
      return Try.of(() -> {
            return prueba1.split("-");
          })
          .onSuccess(succ -> {
            Util.imprimirSuccess();
          })
          .onFailure(err -> {
            Util.imprimirError();
          })
          .getOrElse(prueba2);
    }

  }
}
