package com.comun.utilidad.funcional;

import java.util.Arrays;
import java.util.List;

public class PeekForeach {
  public static void main(String[] args) {
    List<String> lista= Arrays.asList("uno","dos" ,"tres");
    lista.stream()
        .peek((cadena)-> {
          System.out.println("PEEK : " + cadena);
        })
        .map(cadena -> "FORE : " + cadena)
        .forEach(System.out::println);
  }
}
