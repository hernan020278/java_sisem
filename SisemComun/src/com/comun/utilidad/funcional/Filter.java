package com.comun.utilidad.funcional;

import com.comun.referencia.Acceso;
import reactor.core.publisher.Mono;

import java.util.Arrays;

public class Filter {

  public static void main(String[] args) {

    filterTest3("ACTUALIZAR")
      .doOnSuccess(System.out::println)
        .subscribe();


  }

  private static String filterTest1(){

    String[] valores = {"UNO", "DOS"};
    return Arrays.stream(valores)
        .filter(fil -> fil.equals("TRES"))
        .findFirst()
        .orElse("OTRO");

  }

  private static Mono<String> filterTest2(String valor){
    boolean existe = false;

    return Mono.just(valor)
      .filter(fil -> fil.equals(Acceso.INICIAR))
      .map(resp -> {
        return resp;
      })
      .switchIfEmpty(Mono.just(valor))
      .filter(fil -> fil.equals(Acceso.ACTUALIZAR))
      .map(resp -> {
        return resp;
      });
  }

  private static Mono<String> filterTest3(String valor){

    return Mono.fromCallable(() -> valor)
        .filter(fil -> fil.equals(Acceso.INICIAR))
        .map(resp -> {
          return resp;
        })
        .switchIfEmpty(Mono.just(valor))
        .filter(fil -> fil.equals(Acceso.ACTUALIZAR))
        .map(resp -> {
          return resp;
        });
  }

  private static Mono<Boolean> filterTest3(){
    boolean existe = false;

    return Mono.just(existe)
        .filter(fil -> fil)
        .switchIfEmpty(Mono.just(false));
  }

  private static Mono<String> evaluarAccesoIniciar(String valor){
    return Mono.just(valor)
        .filter(filtro -> filtro.equals(Acceso.INICIAR));
  }
}
