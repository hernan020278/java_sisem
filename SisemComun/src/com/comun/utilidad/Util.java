package com.comun.utilidad;

import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;

import io.vavr.Function2;
import io.vavr.control.Try;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jdom.Document;
import org.jdom.input.DOMBuilder;

import com.comun.database.DBConfigBackup;
import com.comun.database.DBConfiguracion;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.referencia.FechaFormato;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import reactor.core.publisher.Mono;

public class Util {

  public EvaluarExtensionArchivo evaluarArchivo;
  public Calendar fecha;
  public Calendar fechaTemp = GregorianCalendar.getInstance();
  // Obtenemos el valor del a?o, mes, d?a,
  // hora, minuto y segundo del sistema
  // usando el m?todo get y el par?metro correspondiente
  public int ano;
  public int mes;
  public int dia;
  public int hora;
  public int minuto;
  public int segundo;

  public java.sql.Date fechaSql;
  public java.sql.Time horaSql;
  public java.sql.Timestamp fechaHoraSql;
  protected static Util INSTANCE = null;

  public static Util getInst() {

    if (INSTANCE == null) {
      INSTANCE = new Util();
    }
    return INSTANCE;
  }

  public Util() {

  }

  public static final String[] strDia = { "DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO" };
  public static final String[] strMes = { "0", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO",
      "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" };
  public static String strServicioWebPHP = "";

  public void setEvaluarArchivo(EvaluarExtensionArchivo evaluarArchivo) {

    this.evaluarArchivo = evaluarArchivo;
  }

  public Timestamp getFechaHora() {

    fecha = GregorianCalendar.getInstance();
    
    ano = fecha.get(Calendar.YEAR);
    mes = fecha.get(Calendar.MONTH);
    dia = fecha.get(Calendar.DATE);
    hora = fecha.get(Calendar.HOUR_OF_DAY);
    minuto = fecha.get(Calendar.MINUTE);
    segundo = fecha.get(Calendar.SECOND);

    fechaHoraSql = null;
    fechaHoraSql = new Timestamp(ano - 1900, mes, dia, hora, minuto, segundo, 0);
    // System.out.println("Fecha Hora : "+fechaHoraSql.toString());
    return fechaHoraSql;
  }

  public Time getHora() {

    fecha = GregorianCalendar.getInstance();

    ano = fecha.get(Calendar.YEAR);
    mes = fecha.get(Calendar.MONTH);
    dia = fecha.get(Calendar.DATE);
    hora = fecha.get(Calendar.HOUR_OF_DAY);
    minuto = fecha.get(Calendar.MINUTE);
    segundo = fecha.get(Calendar.SECOND);

    horaSql = null;
    horaSql = new Time(hora, minuto, segundo);
    // System.out.println("Hora : "+horaSql.toString());
    return horaSql;
  }

  public java.sql.Date getFechaSql() {

    fecha = GregorianCalendar.getInstance();

    ano = fecha.get(Calendar.YEAR);
    mes = fecha.get(Calendar.MONTH);
    dia = fecha.get(Calendar.DATE);
    hora = fecha.get(Calendar.HOUR_OF_DAY);
    minuto = fecha.get(Calendar.MINUTE);
    segundo = fecha.get(Calendar.SECOND);

    fechaSql = null;
    fechaSql = new java.sql.Date(ano - 1900, mes, dia);
    // System.out.println("Fecha : "+fechaSql.toString());
    return fechaSql;
  }

  public String getCodTab(String nomTab, int numReg, int canCar) {

    numReg = numReg + 1;
    String nueNumReg = String.valueOf(numReg);
    int numCero = canCar - (nomTab.length() + nueNumReg.length());
    for (int i = 1; i <= numCero; i++) {
      nueNumReg = "0" + nueNumReg;
    }
    nueNumReg = nomTab + nueNumReg;
    return nueNumReg;
  }// Fin de fuencion para sacar el codigo de una tabla

  public String getMonedaToString(double parValEva) {

    int numInt = ((int) parValEva);
    int numDec = (int) Math.round((parValEva - numInt) * 100);
    /*
     * System.out.println("entero : " + numInt); System.out.println("decimal : "
     * + numDec);
     */
    String resNumConv = numeroLetra(numInt) + " con " + ((numDec < 10) ? "0" + numDec : numDec) + "/100 nuevos soles";
    return resNumConv.trim();
  }

  public static String numeroLetra(int numero) {

    String cadena = new String();
    // Aqui identifico si lleva millones
    if ((numero / 1000000) > 0) {
      if ((numero / 1000000) == 1) {
        cadena = " Un Millon " + numeroLetra(numero % 1000000);
      } else {
        cadena = numeroLetra(numero / 1000000) + " Millones " + numeroLetra(numero % 1000000);
      }
    } else {
      // Aqui identifico si lleva Miles
      if ((numero / 1000) > 0) {
        if ((numero / 1000) == 1) {
          cadena = " Mil " + numeroLetra(numero % 1000);
        } else {
          cadena = numeroLetra(numero / 1000) + " Mil " + numeroLetra(numero % 1000);
        }
      } else {
        // Aqui identifico si lleva cientos
        if ((numero / 100) > 0) {
          if ((numero / 100) == 1) {
            if ((numero % 100) == 0) {
              cadena = " Cien ";
            } else {
              cadena = " Ciento " + numeroLetra(numero % 100);
            }
          } else {
            if ((numero / 100) == 5) {
              cadena = " Quinientos " + numeroLetra(numero % 100);
            } else {
              if ((numero / 100) == 9) {
                cadena = " Novecientos " + numeroLetra(numero % 100);
              } else {
                cadena = numeroLetra(numero / 100) + "cientos" + numeroLetra(numero % 100);
              }
            }
          }
        } // Aqui se identifican las Decenas
        else {
          if ((numero / 10) > 0) {
            switch (numero / 10) {
            case 1:
              switch (numero % 10) {
              case 0:
                cadena = " Diez ";
                break;
              case 1:
                cadena = " Once ";
                break;
              case 2:
                cadena = " Doce ";
                break;
              case 3:
                cadena = " Trece ";
                break;
              case 4:
                cadena = " Catorce ";
                break;
              case 5:
                cadena = " Quince ";
                break;
              default:
                cadena = " Diez y " + numeroLetra(numero % 10);
                break;
              }
              break;
            case 2:
              switch (numero % 10) {
              case 0:
                cadena = " Veinte ";
                break;
              default:
                cadena = " Veinti" + numeroLetra(numero % 10);
                break;
              }
              break;
            case 3:
              switch (numero % 10) {
              case 0:
                cadena = " Treinta ";
                break;
              default:
                cadena = " Treinta y " + numeroLetra(numero % 10);
                break;
              }
              break;
            case 4:
              switch (numero % 10) {
              case 0:
                cadena = " Cuarenta ";
                break;
              default:
                cadena = " Cuarenta y " + numeroLetra(numero % 10);
                break;
              }
              break;
            case 5:
              switch (numero % 10) {
              case 0:
                cadena = " Cincuenta ";
                break;
              default:
                cadena = " Cincuenta y " + numeroLetra(numero % 10);
                break;
              }
              break;
            case 6:
              switch (numero % 10) {
              case 0:
                cadena = " Sesenta ";
                break;
              default:
                cadena = " Sesenta y " + numeroLetra(numero % 10);
                break;
              }
              break;
            case 7:
              switch (numero % 10) {
              case 0:
                cadena = " Setenta ";
                break;
              default:
                cadena = " Setenta y " + numeroLetra(numero % 10);
                break;
              }
              break;
            case 8:
              switch (numero % 10) {
              case 0:
                cadena = " Ochenta ";
                break;
              default:
                cadena = " Ochenta y " + numeroLetra(numero % 10);
                break;
              }
              break;
            case 9:
              switch (numero % 10) {
              case 0:
                cadena = " Noventa ";
                break;
              default:
                cadena = " Noventa y " + numeroLetra(numero % 10);
                break;
              }
              break;
            }
          } else {
            switch (numero) {
            case 1:
              cadena = "Uno";
              break;
            case 2:
              cadena = "Dos";
              break;
            case 3:
              cadena = "Tres";
              break;
            case 4:
              cadena = "Cuatro";
              break;
            case 5:
              cadena = "Cinco";
              break;
            case 6:
              cadena = "Seis";
              break;
            case 7:
              cadena = "Siete";
              break;
            case 8:
              cadena = "Ocho";
              break;
            case 9:
              cadena = "Nueve";
              break;
            }
          }
        }
      }
    }
    return cadena;
  }// FIN DE FUNCION PARA CONVERTIR NUMERO A TEXTO

  public String getFillZero(int numReg, int canCar) {

    String nueNumReg = String.valueOf(numReg);
    int numCero = canCar - nueNumReg.length();
    for (int i = 1; i <= numCero; i++) {
      nueNumReg = "0" + nueNumReg;
    }
    return nueNumReg;
  }// Fin de fuencion para sacar el codigo de una tabla

  public boolean isDouble(String strNumOpe) {

    try {
      Double numOpe = Double.parseDouble(strNumOpe);
      return true;
    } catch (NumberFormatException e) {
      System.err.println("no es un double ");
      return false;
    }
  }

  public static boolean isInteger(String strNumOpe) {

    try {
      Integer numOpe = Integer.parseInt(strNumOpe);
      return true;
    } catch (NumberFormatException e) {
      System.err.println("no es un Entero ");
      return false;
    }
  }

  public static boolean isLong(String strNumOpe) {

    try {
      Long numOpe = Long.parseLong(strNumOpe);
      return true;
    } catch (NumberFormatException e) {
      System.err.println("no es un Long ");
      return false;
    }
  }

  public static int getInteger(String strNumOpe) {

    if (isInteger(strNumOpe)) {
      return Integer.parseInt(strNumOpe);
    } else {
      return 0;
    }
  }

  public Double redondearNumero(Double numOpe, Integer numDec) {

    int cifras = (int) Math.pow(10, numDec);
    return Math.rint(numOpe * cifras) / cifras;
  }

  public String quitarCaracter(String cadOpe, String caracter) {

    String nueCad = "";
    for (int i = 0; i < cadOpe.length(); i++) {
      if (cadOpe.substring(i, i + 1).equals(caracter) != true) {
        nueCad = nueCad + cadOpe.substring(i, i + 1);
      }
    }
    return nueCad;
  }

  public String getCodTab(String nomTab, long numReg, int canCar) {

    numReg = numReg + 1;
    String nueNumReg = String.valueOf(numReg);
    int numCero = canCar - (nomTab.length() + nueNumReg.length());
    for (int i = 1; i <= numCero; i++) {
      nueNumReg = "0" + nueNumReg;
    }
    nueNumReg = nomTab + nueNumReg;
    return nueNumReg;
  }
  
  public String isDateTimeObject(String strFecha) {
    String aStrDateTime[] = strFecha.split(" ");
    if (isTimestamp(strFecha)) {
      return "TIMESTAMP";
    } else if ((aStrDateTime[0].equals("1970-01-01") || aStrDateTime[0].equals("1970-01-02"))
        && !aStrDateTime[1].equals("00:00:00")) {
      return "TIME";
    } else if (!aStrDateTime[0].equals("1970-01-01") && Util.getInst().isDateFormato(strFecha, "yyyy-MM-dd")) {
      return "DATE";
    }
    return "OBJETO";
  }

  public boolean isTime(String strHora) {
    try {
      fromStrdateToDateThrows(strHora, FechaFormato.HHmmss);
    } catch (Exception exc1) {
      try {
        fromStrdateToDateThrows(strHora, FechaFormato.HHmm);
      } catch (Exception exc2) {
        return false;
      }
    }
    return true;
  }

  public boolean isTimestamp(String strHora) {
    SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
    try {
      format.parse(strHora);
      return true;
    } catch (ParseException e) {
      format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      try {
        format.parse(strHora);
        return true;
      } catch (ParseException ex) {
        return false;
      }
    }
  }

  public boolean isDate(String strFecha) {
    return isDateFormato(strFecha, "dd/MM/yyyy");
  }

  public boolean isDateFormato(String strFecha, String parFormato) 
  {
    return isDateHoraFormato(strFecha, parFormato);
  }

  public boolean isDateHoraFormato(String strFecha, String parFormato) 
  {
    SimpleDateFormat sdf = new SimpleDateFormat(parFormato);
    try {
      sdf.parse(strFecha);
      return true;
    } catch (ParseException ex) {
      return false;
    }
  }
  
  public java.sql.Date fromStrdateToDateSqlFormato(String strFecha, String parFormato) 
  {
    return fromDateToDateSql(fromStrdateToDate(strFecha, parFormato));
  }
  
  public Time fromStrdateToTime(String parStrDateTime) {
    return new Time(fromStrdateToDate(parStrDateTime).getTime());
  }

  public Timestamp fromStrdateToTimestamp(String parStrDateTime) {
    return new Timestamp(fromStrdateToDate(parStrDateTime).getTime());
  }
  
  public java.sql.Date fromStrdateToDateSql(String parStrDateTime) {
    return new java.sql.Date(fromStrdateToDate(parStrDateTime).getTime());
  }
  
  public String fromStrdateToString(String strFecha, String formato) {
    return fromDateToString(fromStrdateToDate(strFecha), formato);
    
  }

  public String fromStrdateToString(String strFecha) {
    return fromDateToString(fromStrdateToDate(strFecha), FechaFormato.yyyyMMdd);    
  }
  
  public Timestamp fromDateToTimestamp(Date dateFecha) {
    return new Timestamp(dateFecha.getTime());
  }

  public String fromTimestampToString(Time parTime) {
    return fromDateToString(new Date(parTime.getTime()), FechaFormato.HHmmss);
  }

  public String fromTimestampToString(Timestamp parTime) {
    return fromDateToString(new Date(parTime.getTime()), FechaFormato.yyyyMMddHHmmss);
  }


  public Date fromStrdateToDate(String strFecha, String formato){

    Date fecha = new Date();
    try {
      fecha = fromStrdateToDateThrows(strFecha, formato);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return fecha;
  }

  public Date fromStrdateToDate(String parFecha) {

    Date fechaHora = new Date();
    SimpleDateFormat dateFormat = null;
    try {
      return fromStrdateToDateThrows(parFecha, FechaFormato.yyyyMMddHHmmssSSS);
    } catch (Exception exp1) {
      try {
        return fromStrdateToDateThrows(parFecha, FechaFormato.yyyyMMddHHmmss);
      } catch (Exception exp2) {
        try {
          return fromStrdateToDateThrows(parFecha, FechaFormato.yyyyMMdd);
        } catch (Exception exp3) {
          try {
            return fromStrdateToDateThrows(parFecha, FechaFormato.ddMMyyyy);
          } catch (Exception exp4) {
            try {
              return fromStrdateToDateThrows(parFecha, FechaFormato.HHmmss);
            } catch (Exception exp5) {
              try {
                return fromStrdateToDateThrows(parFecha, FechaFormato.HHmm);
              } catch (Exception exp6) {
                exp6.printStackTrace();
              }
            }
          }
        }
      }
    }
    return fechaHora;
  }
  
  public Date fromStrdateToDateThrows(String strFecha, String formato) throws ParseException {

    SimpleDateFormat dateFormat = new SimpleDateFormat(formato);
    Date fechaHora = dateFormat.parse(strFecha);
    return fechaHora;
  }
  
  public java.util.Date fromDateSqlToDate(java.sql.Date parFecha) {
    return new java.util.Date(parFecha.getTime());
  }

  public java.sql.Date fromDateToDateSql(Date parFecha) {
    return new java.sql.Date(parFecha.getTime());
  }
  
  public String fromDateToString(Date strFecha, String formato) {

    if (strFecha != null) {
      SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formato);
      String fecha = formatoDelTexto.format(strFecha);
      return fecha;
    }
    return "";
  }

  public String getToFecHor(String fecHor) {
    String[] fechaHora = fecHor.split(" ");
    String[] fecha = fechaHora[0].split("-");
    String[] hora = fechaHora[1].split(":");

    String ano = fecha[0];
    String mes = fecha[1];
    String dia = fecha[2];

    String hor = hora[0];
    String min = hora[1];
    String sec = hora[2];
    return ano + "-" + mes + "-" + dia + " " + hor + ":" + min + ":" + sec.substring(0, 2);
  }

  public double getCantHorDec(Time horaIngreso, Time horaSalida) {
    boolean respuesta = false;
    Time horaZero = new Time(00, 00, 00);
    Time horaLast = new Time(23, 59, 59);
    // Time hora = new Time(00, 00, 00);
    // Time horaIngreso = new Time(19, 00, 00);
    // Time horaSalida = new Time(03, 59, 59);
    Time horaSalidaTmp = new Time(00, 00, 00);
    long totalMinuto = 0;
    if ((horaIngreso.getTime() == 18000000 || horaSalida.getTime() == 18000000)
        && horaIngreso.getTime() == horaSalida.getTime()) {
      totalMinuto = 0;
    }
    if (horaIngreso.getTime() < horaSalida.getTime()) {
      totalMinuto = horaSalida.getTime() - horaIngreso.getTime();
    } else if (horaIngreso.getTime() > horaSalida.getTime()) {
      totalMinuto = horaLast.getTime() - horaIngreso.getTime();
      totalMinuto = totalMinuto + (horaSalida.getTime() - horaZero.getTime());
    }
    double decimalMinuto = (totalMinuto > 0) ? ((totalMinuto / 1000.00) / 60.00) : 0.00;
    return (decimalMinuto > 0) ? ((decimalMinuto * 1.00) / 60) : 0.00;
  }

  public BigDecimal getCantHorDecBigDecimal(Time horaIngreso, Time horaSalida) {

    double res = getCantHorDec(horaIngreso, horaSalida);
    return new BigDecimal(res);
  }

  public BigDecimal getTotalHoraDecimal(Time horaInicio, Timestamp horaFinal) {
    return getTotalHoraDecimal(horaInicio, new Time(horaFinal.getTime()));
  }

  public BigDecimal getTotalHoraDecimal(Timestamp horaInicio, Time horaFinal) {
    return getTotalHoraDecimal(new Time(horaInicio.getTime()), horaFinal);
  }

  public BigDecimal getTotalHoraDecimal(Time horaInicio, Time horaFinal) {
    Date fecHorIni = new Date(horaInicio.getTime());
    Date fecHorFin = new Date(horaFinal.getTime());

    if (horaInicio.getTime() > horaFinal.getTime()) 
    {
      int dias = Util.getInst().diferenciaFechas(fecHorIni, new Date(), "DIAS");
      fecHorIni = Util.getInst().agregarDias(fecHorIni, dias);
      fecHorFin = Util.getInst().agregarDias(fecHorFin, dias + 1);
    }
    return getTotalHoraDecimal(new Timestamp(fecHorIni.getTime()), new Timestamp(fecHorFin.getTime()));
  }
  
  public BigDecimal getTotalHoraDecimal(Timestamp horaInicio, Timestamp horaFinal) {
    
    Date fecHorIni = new Date(horaInicio.getTime());
    Date fecHorFin = new Date(horaFinal.getTime());

    if (horaInicio.getTime() > horaFinal.getTime()) 
    {
      int dias = Util.getInst().diferenciaFechas(fecHorIni, new Date(), "DIAS");
      fecHorIni = Util.getInst().agregarDias(fecHorIni, dias);
      fecHorFin = Util.getInst().agregarDias(fecHorFin, dias + 1);
    }
    
    BigDecimal totalMinuto = new BigDecimal(fecHorFin.getTime() - fecHorIni.getTime());
    double totalMillis = totalMinuto.doubleValue();
    BigDecimal hor = new BigDecimal((int) ((totalMillis / 1000) / 3600));
    BigDecimal min = new BigDecimal((int) (((totalMillis / 1000) / 60) % 60));
    BigDecimal seg = new BigDecimal((int) ((totalMillis / 1000) % 60));

    totalMinuto = getBigDecimalFormatted(seg, 2).divide(new BigDecimal(3600), RoundingMode.HALF_UP);
    totalMinuto = totalMinuto.add(getBigDecimalFormatted(min, 2).divide(new BigDecimal(60), RoundingMode.HALF_UP));
    totalMinuto = totalMinuto.add(hor);
    return totalMinuto;
  }

  public BigDecimal getDecimalTimeFromBigDecimal(BigDecimal numero) {

    BigDecimal minuto = numero.remainder(new BigDecimal(1));
    minuto = minuto.multiply(new BigDecimal(60)).divide(new BigDecimal(100));
    int hora = numero.intValue();
    numero = new BigDecimal(hora + minuto.doubleValue());
    return getBigDecimalFormatted(numero, 2);
  }

  public BigDecimal getBigDecimalFromDecimalTime(BigDecimal numero) {

    BigDecimal minuto = numero.remainder(new BigDecimal(1));
    minuto = getBigDecimalFormatted(minuto, 2);
    minuto = minuto.divide(new BigDecimal(60), RoundingMode.HALF_UP).multiply(new BigDecimal(100));
    int hora = numero.intValue();
    numero = new BigDecimal(hora + minuto.doubleValue());
    return getBigDecimalFormatted(numero, 2);
  }

  public Time stringToTime(String parStrTime) {

    Time hora;
    if (isTime(parStrTime)) {
      String varField;
      String[] varCampo;
      varField = parStrTime;// Hora de Registro
      varField = (varField.contains(".0")) ? varField.replace(".0", "") : varField;
      varCampo = varField.split(":");
      hora = new Time(Integer.parseInt(varCampo[0]), Integer.parseInt(varCampo[1]), Integer.parseInt(varCampo[2]));
    } // Fin de if(isTime(parStrTime))
    else {
      hora = new Time(18000000);
    }
    return hora;
  }// Fin de stringoTime

  public void mostrarMensaje(String parMsg, String parNomClass) {

    JOptionPane.showConfirmDialog(null, parMsg + "\nClase : " + parNomClass, "Mensaje del Sistema",
        JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
  }

  public String obtenerMacAdress() {

    try {
      // Obtenemos la interface en la cual estamos conectados.
      NetworkInterface netInt = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
      // Obtenemos su MAC Address, pero nos devuelve un array de bytes
      // Por lo que hay que convertirlos a Hexadecimal
      String macAdress = "";
      byte[] byteMacAdress = netInt.getHardwareAddress();
      for (int ite = 0; ite < byteMacAdress.length; ite++) {
        // Tratamos los valores que devuelven < 0 normalmente son el "3 y 5 par"
        if (byteMacAdress[ite] < 0) {
          // Convertimos el byte a Hexadecimal con la clase Integer
          String tmpHex = Integer.toHexString(byteMacAdress[ite]);
          /*
           * Los numeros que son menores a cero al momento de convertirlo a
           * string nos devuelven una cadena de este tipo ffffffAA por lo que
           * unicamente tomamos los ultimos 2 caracteres que son lo que
           * buscamos. y obtenemos esos ultimos caracteres con substring
           */
          if ((ite + 1) == byteMacAdress.length) {
            macAdress = macAdress + tmpHex.substring(tmpHex.length() - 2).toUpperCase();
          } else {
            macAdress = macAdress + tmpHex.substring(tmpHex.length() - 2).toUpperCase() + "-";
          }
          continue;
        }
        // Aqui imprimimos directamente los bytes que son mayores a cero.
        if ((ite + 1) == byteMacAdress.length) {
          macAdress = macAdress + Integer.toHexString(byteMacAdress[ite]);
        } else {
          macAdress = macAdress + Integer.toHexString(byteMacAdress[ite]) + "-";
        }
      } // Fin de for (int ite = 0; ite < byteMacAdress.length; ite++) {
      return validarMacAdress(macAdress.toUpperCase());
    } catch (SocketException ex) {
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
    } catch (UnknownHostException ex) {
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
    }
    return "";
  }// Fin de metodo para Obtener la Mac Adrress del una pc

  private String validarMacAdress(String parMacAdress) {

    String newMacAdress = "";
    String[] parseMacAdress = parMacAdress.split("-");
    for (int iteMac = 0; iteMac < parseMacAdress.length; iteMac++) {
      if (parseMacAdress[iteMac].length() == 1) {
        if ((iteMac + 1) == parseMacAdress.length) {
          newMacAdress = newMacAdress + "0" + parseMacAdress[iteMac];
        } else {
          newMacAdress = newMacAdress + "0" + parseMacAdress[iteMac] + "-";
        }
      } // Fin de if(parseMacAdress[iteMac].equals("0")){
      else {
        if ((iteMac + 1) == parseMacAdress.length) {
          newMacAdress = newMacAdress + parseMacAdress[iteMac];
        } else {
          newMacAdress = newMacAdress + parseMacAdress[iteMac] + "-";
        }
      } // Fin de si el valor hexadecimmal es 0
    } // Fin de for(int iteMac = 0; iteMac< parseMacAdress.length; iteMac++){
    return newMacAdress;
  }// Fin de metodo para validarMacAdress

  public void ubicarCeldaTabla(JTable parTab, int parFil, int parCol) {

    if (!parTab.isRowSelected(parFil) || !parTab.isColumnSelected(parCol)) {
      parTab.changeSelection(parFil, parCol, true, false);
    } // Fin de if(!parTab.isRowSelected(parCol))
  }

  public void ubicarRegistroTabla(JTable parTab, String parValor, int parCol) {

    String varValor;
    for (int iteTab = 0; iteTab < parTab.getRowCount(); iteTab++) {
      varValor = (String) parTab.getValueAt(iteTab, parCol);// Codigo del
                                                            // Articulo
      if (varValor.equals(parValor)) {
        if (!parTab.isRowSelected(iteTab)) {
          parTab.changeSelection(iteTab, 0, true, false);
          break;
        } // Fin de if(!parTab.isRowSelected(parCol))
      } // Fin de if (varArtCod.equals(parArtCod))
    } // Fin de for (int iteTab = 0; iteTab < parTab.getRowCount(); iteTab++)
  }

  public void ubicarRegistroEnteroTabla(JTable parTab, int parValor, int parCol) {

    int varValor;
    for (int iteTab = 0; iteTab < parTab.getRowCount(); iteTab++) {
      varValor = (Integer) parTab.getValueAt(iteTab, parCol);// Codigo del
                                                             // Articulo
      if (varValor == parValor) {
        if (!parTab.isRowSelected(iteTab)) {
          parTab.changeSelection(iteTab, 0, true, false);
          break;
        } // Fin de if (!parTab.isRowSelected(parCol))
      } // Fin de if (varArtCod.equals(parArtCod))
    } // Fin de for (int iteTab = 0; iteTab < parTab.getRowCount(); iteTab++)
  }

  public void setSizeTexto(KeyEvent e, int parNumDig) {

    if (e.getSource() instanceof JTextArea) {
      JTextArea miText = (JTextArea) e.getSource();
      if (miText.getText().trim().length() > parNumDig) {
        miText.setText(miText.getText().substring(0, parNumDig));
      } // Fin de
    } // Fin de if (e.getSource() instanceof JTextArea)
    else if (e.getSource() instanceof JTextField) {
      JTextField miText = (JTextField) e.getSource();
      if (miText.getText().trim().length() > parNumDig) {
        miText.setText(miText.getText().substring(0, parNumDig));
      } // Fin de if (miText.getText().trim().length() > parNumDig)
    } // Fin de else if (e.getSource() instanceof JTextField)
  }// Fin de Metodo de SetSizeTexto

  public boolean verificarCarpetaArchivoWeb(String parRutaCarpeta, String parNomFile, String parRutaWeb) {

    File carpeta = new File(parRutaCarpeta);
    if (!carpeta.exists()) {
      carpeta.mkdirs();
    } // Fin de if (!carpeta.exists())
    if (carpeta.exists()) {
      return crearArchivoUrlWeb(parRutaCarpeta, parNomFile, parRutaWeb);
    } else {
      return false;
    }
  }// Fin de metodo para crear reporte

  public boolean crearCarpetaArchivo(String parRutaCarpeta, String parNomFile, String parRutaWeb) {

    File archivo = new File(parRutaCarpeta + parNomFile);
    if (!archivo.exists()) {
      try {
        URL urlFileRep = new URL(parRutaWeb + parNomFile);
        try {
          BufferedInputStream bis = new BufferedInputStream(urlFileRep.openStream());
          FileOutputStream fos = new FileOutputStream(parRutaCarpeta + parNomFile);
          BufferedOutputStream bos = new BufferedOutputStream(fos);
          boolean eof = false;
          int flujoByte;
          while (!eof) {
            flujoByte = bis.read();
            if (flujoByte != -1) {
              bos.write(flujoByte);
            } // [IF] Leyendo byte de archivo
            else {
              eof = true;
            } // [ELSE] Fin de archivo
          } // Fin de bucle para verificar el final del archivo
          bis.close();
          bos.close();
          fos.close();
          return true;
        } catch (Exception e) {
          System.err.println(e);
          return false;
        }
      } catch (IOException ex) {
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        return false;
      }
    }
    return true;
  }

  public boolean crearArchivoUrlWeb(String parCarpetaDestino, String parArchivoDestino, String parRutaWeb) {

    File archivo = new File(parCarpetaDestino + parArchivoDestino);
    if (!archivo.exists()) {
      try {
        URL urlFileRep = new URL(parRutaWeb + parArchivoDestino);
        try {
          BufferedInputStream bis = new BufferedInputStream(urlFileRep.openStream());
          FileOutputStream fos = new FileOutputStream(parCarpetaDestino + parArchivoDestino);
          BufferedOutputStream bos = new BufferedOutputStream(fos);
          boolean eof = false;
          int flujoByte;
          while (!eof) {
            flujoByte = bis.read();
            if (flujoByte != -1) {
              bos.write(flujoByte);
            } // [IF] Leyendo byte de archivo
            else {
              eof = true;
            } // [ELSE] Fin de archivo
          } // Fin de bucle para verificar el final del archivo
          bis.close();
          bos.close();
          fos.close();
          return true;
        } catch (Exception e) {
          System.err.println(e);
          return false;
        }
      } catch (IOException ex) {
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        return false;
      }
    }
    return true;
  }

  public void copiarArchivo(String parStrFileOrigen, String parStrFileDestino) {

    try {
      File fileOrigen = new File(parStrFileOrigen);
      File fileDestino = new File(parStrFileDestino);
      InputStream in = new FileInputStream(fileOrigen);
      OutputStream out = new FileOutputStream(fileDestino);
      byte[] buf = new byte[1024];
      int len;
      while ((len = in.read(buf)) > 0) {
        out.write(buf, 0, len);
      }
      in.close();
      out.close();
      System.out.println("File copied.");
    } catch (FileNotFoundException ex) {
      ex.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public boolean verificarCarpeta(String parCarpeta) {

    File carpeta = new File(parCarpeta);
    if (!carpeta.exists()) {
      carpeta.mkdirs();
      return true;
    } else {
      return true;
    }
  }// Fin de metodo para crear reporte

  public boolean verificarCarpetaArchivo(String parRutaCarpeta, String parNomFile, String parRutaWeb) {

    File carpeta = new File(parRutaCarpeta);
    if (!carpeta.exists()) {
      carpeta.mkdirs();
    } // Fin de if (!carpeta.exists())
    if (carpeta.exists()) {
      return crearCarpetaArchivo(parRutaCarpeta, parNomFile, parRutaWeb);
    } else {
      return false;
    }
  }// Fin de metodo para crear reporte

  public boolean verificarCarpetaArchivo(File fileOrigen, String parCarpeta, String parArchivo) {

    File carpeta = new File(parCarpeta);
    if (!carpeta.exists()) {
      carpeta.mkdirs();
    } // Fin de if (!carpeta.exists())
    File fileDestino = new File(parCarpeta + parArchivo);
    return verificarArchivo(fileOrigen, fileDestino);
  }// Fin de metodo para crear reporte

  public boolean verificarArchivo(File fileOrigen, File fileDestino) {

    if (!fileDestino.exists()) {
      try {
        FileInputStream fis = new FileInputStream(fileOrigen);
        FileOutputStream fos = new FileOutputStream(fileDestino);
        int dato;
        while ((dato = fis.read()) != -1) {
          fos.write(dato);
        }
        fis.close();
        fos.close();
        return true;
      } catch (IOException e) {
        System.err.println("Hubo un error de entrada/salida!!!");
      }
    } // Fin de if (!fileDestino.exists())
    return true;
  }// Fin de verificarArchivo(File fileOrigen, File fileDestino)

  public ImageIcon obtenerImagenIconArchivo(String parRutaCarpeta) {

    BufferedImage buffImg = obtenerBufferedImageArchivo(parRutaCarpeta);
    return (buffImg != null) ? new ImageIcon(buffImg) : null;
  }

  public BufferedImage obtenerBufferedImageArchivo(String parRutaCarpeta) {

    try {
      URL urlFile;
      File file;
      file = new File(parRutaCarpeta);
      if (!file.exists()) {
        file = new File(parRutaCarpeta + "imagen.jpg");
      }
      urlFile = file.toURL();
      BufferedImage bufImg = null;
      try {
        bufImg = ImageIO.read(urlFile);
      } catch (IOException ex) {
      }
      return bufImg;
    } catch (MalformedURLException ex) {
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
      return null;
    }
  }

  public void ejecutarArchivoDesktop(String comando) {

    Runtime aplicacion = Runtime.getRuntime();
    try {
      // aplicacion.exec("cmd.exe /K " + parametros );
      // aplicacion.waitFor();
      aplicacion.exec(comando);
    } catch (Exception e) {
      System.out.println(e);
    }
  }// Fin de abrirArchivoDesktop()

  public void abrirArchivoDesktop(String parPathFile, String parNomFile) {

    File file = new File(parPathFile + parNomFile);
    if (file.exists()) {
      try {
        Desktop.getDesktop().open(file);
      } catch (Exception e) {
        e.printStackTrace();
      }
    } // Fin de if(file.exists())
    else {
      JOptionPane.showConfirmDialog(null, "???ARCHIVO NO EXISTE O FUE BORRADO!!!" + "\n" + this.getClass().getName(),
          "Sistema", JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
    }
  }// Fin de abrirArchivoDesktop()

  public void abrirArchivoDesktop(String parNomFile) {

    if (parNomFile != null) {
      File file = new File(parNomFile);
      if (file.exists()) {
        try {
          Desktop.getDesktop().open(file);
        } catch (Exception e) {
          e.printStackTrace();
        }
      } // Fin de if(file.exists())
      else {
        JOptionPane.showConfirmDialog(null, "???ARCHIVO NO EXISTE O FUE BORRADO!!!" + "\n" + this.getClass().getName(),
            "Sistema", JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
      }
    }
  }// Fin de abrirArchivoDesktop()

  public void abrirArchivoNavegador(String url) {

    if (java.awt.Desktop.isDesktopSupported()) {
      try {
        Desktop dk = Desktop.getDesktop();
        dk.browse(new URI(url));
      } catch (Exception e) {
        System.out.println("Error al abrir URL: " + e.getMessage());
      }
    }
  }// Fin de abrirArchivoDesktop()

  public String obtenerNombreFromUrlJsp(String urlJsp) {

    String[] splitPaginHijo = urlJsp.split("/");
    String pagina = "";
    if (splitPaginHijo.length > 0) {
      pagina = splitPaginHijo[splitPaginHijo.length - 1];
      pagina = pagina.replace(".jsp", "");
      // "browse_form"
      String division = (pagina.contains("_")) ? "_" : "-";
      if (pagina.contains(division)) {
        String[] arrayPagina = pagina.split(division);
        pagina = "";
        if (division.equals("_")) {
          for (String iteArrPag : arrayPagina) {
            pagina = pagina + iteArrPag.substring(0, 1).toUpperCase() + iteArrPag.substring(1, iteArrPag.length());
          }
        } else if (division.equals("-")) {
          pagina = arrayPagina[0];
        }
      } else {
        pagina = pagina.substring(0, 1).toUpperCase() + pagina.substring(1, pagina.length());
      }
    }
    return "Manejador" + pagina;
  }

  public String obtenerCarpetaArchivo(String[] parLisExt, String parCarpeta) {

    JFileChooser escogerFile = new JFileChooser();
    // escogerFile.setCurrentDirectory(new
    // File(System.getProperty("user.dir")));//El Directorio Actual
    escogerFile.setCurrentDirectory(new File(parCarpeta));
    escogerFile.setFileSelectionMode(JFileChooser.FILES_ONLY);
    MyArchivoFiltro filtrado = new MyArchivoFiltro(parLisExt);
    escogerFile.addChoosableFileFilter(filtrado);
    int rpta = escogerFile.showDialog(null, "Seleccione un Archivo");
    // rpta = escogerFile.showOpenDialog(null);
    if (rpta == JFileChooser.APPROVE_OPTION) {
      if (filtrado.accept(escogerFile.getSelectedFile()) == true) {
        File fileOrigen = escogerFile.getSelectedFile();
        if (fileOrigen.getName().length() <= 100) {
          if (verificarCarpetaArchivo(fileOrigen, parCarpeta, fileOrigen.getName())) {
            return fileOrigen.getAbsolutePath();
          } else {
            JOptionPane.showConfirmDialog(null, "???NO SE PUDO CREAR EL ARCHIVO!!!" + "\n" + this.getClass().getName(),
                "Sistema", JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
          }
        } // Fin de if(fileOrigen.getName().length() <= 100)
        else {
          JOptionPane.showConfirmDialog(null,
              "???El Nombre de Archivo no debe de exceder los 100 Caracteres!!!" + "\n" + this.getClass().getName(),
              "Sistema", JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
        }
      } else {
        JOptionPane.showConfirmDialog(null, "???EXTENSION DE ARCHIVO INVALIDO!!!", "Systema de Cobranza",
            JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
      }
    } // Fin de Block True if (resVal == JFileChooser.APPROVE_OPTION)
    return "SIN ARCHIVO";
  }// Fin de crearObtenerNombreArchivo()

  public File obtenerFileFuente(String[] parLisExt, String parCarpeta) {

    JFileChooser escogerFile = new JFileChooser();
    // escogerFile.setCurrentDirectory(new
    // File(System.getProperty("user.dir")));//El Directorio Actual
    escogerFile.setCurrentDirectory(new File(parCarpeta));
    escogerFile.setFileSelectionMode(JFileChooser.FILES_ONLY);
    MyArchivoFiltro filtrado = new MyArchivoFiltro(parLisExt);
    escogerFile.addChoosableFileFilter(filtrado);
    int rpta = escogerFile.showDialog(null, "Seleccione un Archivo para DataMining");
    // rpta = escogerFile.showOpenDialog(null);
    if (rpta == JFileChooser.APPROVE_OPTION) {
      if (filtrado.accept(escogerFile.getSelectedFile()) == true) {
        File fileFuente = escogerFile.getSelectedFile();
        return fileFuente;
      } else {
        JOptionPane.showConfirmDialog(null, "???EXTENSION DE ARCHIVO INVALIDO!!!", "Systema de Cobranza",
            JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
      }
    } // Fin de Block True if (resVal == JFileChooser.APPROVE_OPTION)
    return null;
  }// Fin de crearObtenerNombreArchivo()

  public void borrarArchivo(String parCarpeta, String parArchivo) {

    File file = new File(parCarpeta + parArchivo);
    file.delete();
  }// Fin de borrarArchivo(String parCarpeta, String parArchivo)

  public static void borrarDirectorio(File directorio) {
    File[] ficheros = directorio.listFiles();
    for (int x = 0; x < ficheros.length; x++) {
      if (ficheros[x].isDirectory()) {
        if (!ficheros[x].getName().equals("data")) {
          borrarDirectorio(ficheros[x]);
        }
      }
      if (!ficheros[x].getName().equals("SisemApp.jar") && !ficheros[x].getName().equals("SisemControl.jar")
          && !ficheros[x].getName().equals("SisemComun.jar")) {
        ficheros[x].delete();
      }
    }
  }

  public int obtenerMeses(Date parInicio, Date parFinal) {

    Date fechaInicio = new java.sql.Date(parInicio.getYear(), parInicio.getMonth(), 1);
    Date fechaFinal = new java.sql.Date(parFinal.getYear(), parFinal.getMonth(), 1);
    int numMeses = 0;
    while (fechaFinal.compareTo(fechaInicio) == 1 || fechaFinal.compareTo(fechaInicio) == 0) {
      fechaInicio.setMonth(fechaInicio.getMonth() + 1);
      numMeses++;
    } // Fin de while (fechaFinal.compareTo(fechaInicial) == 1)
    return numMeses;
  }// Fin de obtenerMeses(Date fechaInicial, Date fechaFinal)

  public String obtenerPeriodo(Date parFecha) {

    int ano = parFecha.getYear() + 1900;
    int mes = parFecha.getMonth() + 1;
    int dia = parFecha.getDate();
    return mes + "-" + ano;
  }// Fin de obtenerMeses(Date fechaInicial, Date fechaFinal)

  public static Date getPrimerDiaDelMes() {

    Calendar cal = Calendar.getInstance();
    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMinimum(Calendar.DAY_OF_MONTH),
        cal.getMinimum(Calendar.HOUR_OF_DAY), cal.getMinimum(Calendar.MINUTE), cal.getMinimum(Calendar.SECOND));
    return cal.getTime();
  }

  public static Date getUltimoDiaDelMes() {

    Calendar cal = Calendar.getInstance();
    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMaximum(Calendar.DAY_OF_MONTH),
        cal.getMaximum(Calendar.HOUR_OF_DAY), cal.getMaximum(Calendar.MINUTE), cal.getMaximum(Calendar.SECOND));
    return cal.getTime();
  }

  public boolean existeItemJCombo(JComboBox parJCombo, Object parItem) {

    boolean existe = false;
    for (int ite = 0; ite < parJCombo.getItemCount(); ite++) {
      Object valor = parJCombo.getItemAt(ite);
      if (valor.equals(parItem)) {
        existe = true;
      }
    }
    return existe;
  }// Fin de existeItemJCombo(JComboBox parJCombo, Object parItem)

  public boolean escribirImagenIconDisk(ImageIcon parImgIco, String parCarpeta, String parNomFile) {

    try {
      if (parImgIco == null) {
        parImgIco = AplicacionGeneral.getInstance().obtenerImagen("logo.png");
      }
      Image image = parImgIco.getImage();
      RenderedImage rendered;
      if (image instanceof RenderedImage) {
        rendered = (RenderedImage) image;
      } else {
        BufferedImage buffered = new BufferedImage(parImgIco.getIconWidth(), parImgIco.getIconHeight(),
            BufferedImage.TYPE_INT_RGB);
        Graphics2D g = buffered.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        rendered = buffered;
      }
      ImageIO.write(rendered, "GIF", new File(parCarpeta + parNomFile));
      return true;
    } catch (IOException ex) {
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
    }
    return false;
  }

  private void verificarOrden(List<Integer> parLista) {

    int idxMen = 0;
    int menor = 0, mayor = 0;
    for (int idxUno = 0; idxUno < parLista.size(); idxUno++) {
      for (int idxDos = idxUno; idxDos < parLista.size(); idxDos++) {
        if (parLista.get(idxUno) > parLista.get(idxDos)) {
          menor = parLista.get(idxDos);
          mayor = parLista.get(idxUno);
          parLista.set(idxUno, menor);
          parLista.set(idxDos, mayor);
        }
      }
    }
    for (int idxUno = 0; idxUno < parLista.size(); idxUno++) {
      System.out.println("lista : " + parLista.get(idxUno));
    }
  }

  public List<Integer> obtenerListaPagina(String parTexto) {

    String letra;
    String item = "";
    boolean agregar = false;
    List<Integer> lista = new ArrayList<Integer>();
    for (int ite = 0; ite < parTexto.length(); ite++) {
      letra = parTexto.substring(ite, ite + 1);
      if (isInteger(letra)) {
        item = item + letra;
        if (isInteger(item) && !lista.contains(item) && (ite + 1 == parTexto.length())) {
          lista.add(Integer.parseInt(item));
        }
      } else {
        if ((isInteger(item) && !lista.contains(Integer.parseInt(item)))) {
          lista.add(Integer.parseInt(item));
          item = "";
        } else if (isInteger(item) && lista.contains(Integer.parseInt(item))) {
          lista.clear();
          break;
        }
      }
    } // Fin de for (int ite = 0; ite < parTexto.length(); ite++)
    return lista;
  }// Fin de obtenerListaPagina(String parTexto)

  public List<String> obtenerListaSigno(String parTexto) {

    String letra;
    String item = "";
    boolean copiar = false;
    List<String> lista = new ArrayList<String>();
    for (int ite = 0; ite < parTexto.length(); ite++) {
      letra = parTexto.substring(ite, ite + 1);
      if (!isInteger(letra) && copiar && (letra.equals(",") || letra.equals("-"))) {
        lista.add(letra);
        copiar = false;
      } else {
        if (isInteger(letra)) {
          copiar = true;
        }
      }
    } // Fin de for (int ite = 0; ite < parTexto.length(); ite++)
    return lista;
  }// Fin de obtenerListaPagina(String parTexto)

  public boolean verificarOrdenAsc(List<Integer> parLista) {

    boolean exito = true;
    for (int idxUno = 0; idxUno < parLista.size(); idxUno++) {
      for (int idxDos = idxUno; idxDos < parLista.size(); idxDos++) {
        if (parLista.get(idxUno) > parLista.get(idxDos)) {
          exito = false;
          break;
        }
      }
    }
    return exito;
  }

  public List<Integer> obtenerListaPaginas(List<Integer> lisPag, List<String> lisSig) {

    List<Integer> listaPagina = new ArrayList<Integer>();
    for (int ite = 0; ite < lisPag.size(); ite++) {
      if (lisSig.size() > 0 && ite < lisSig.size() && lisSig.get(ite).equals("-")) {
        for (int iteLis = lisPag.get(ite); iteLis <= lisPag.get(ite + 1); iteLis++) {
          listaPagina.add(iteLis);
        }
        ite = ite + 1;
      } else {
        listaPagina.add(lisPag.get(ite));
      }
    }
    return listaPagina;
  }

  public java.sql.Date ofStrToDate(String parStrDate) {

    if (isDate(parStrDate)) {
      String varField;
      String[] varCampo;
      varField = parStrDate;// Fecha de Registro
      varCampo = varField.split("-");
      java.sql.Date fecha = new java.sql.Date(Integer.parseInt(varCampo[2]) - 1900, Integer.parseInt(varCampo[1]) - 1,
          Integer.parseInt(varCampo[0]));
      return fecha;
    } else {
      return null;
    }
  }

  public List<String> parsearFieldJavaScript(Object parObj) {

    Field[] listaField = obtenerListaFields(parObj);
    Map<String, Object> mapFieldValue = obtenerMapaFields(parObj);
    String tagInput = "";
    List<String> listaTagInputHidden = new ArrayList<String>();
    for (Field iteListaField : listaField) {
      tagInput = "<input type='hidden' ";
      tagInput = tagInput + "name='" + iteListaField.getName() + "' ";
      tagInput = tagInput + "value='" + mapFieldValue.get(iteListaField.getName()) + "'/>";
      listaTagInputHidden.add(tagInput);
    } // Fin de for(String iteLis : listaField){
    return listaTagInputHidden;
  }

  public Field[] obtenerListaFields(Object parObj) {

    String registroFields = "";
    Class clase = parObj.getClass();
    Field listaField[] = clase.getDeclaredFields();
    return listaField;
  }

  public String obtenerListaFieldsParaSql(Object parObj) {

    String registroFields = "";
    Class clase = parObj.getClass();
    Field listaField[] = clase.getDeclaredFields();
    int numIte = 0;
    for (Field iteField : listaField) {
      registroFields = registroFields + iteField.getName() + (((numIte + 1) < listaField.length) ? "," : "");
      numIte++;
    }
    return registroFields;
  }

  public Map<String, Object> obtenerMapaFields(Object parObj) {

    String registroFields = "";
    Class clase = parObj.getClass();
    Field[] listaField = clase.getDeclaredFields();
    Map<String, Object> mapFieldValue = new HashMap<String, Object>();
    int numIte = 0;
    for (Field iteField : listaField) {
      // System.out.println("Field name = " + field.getName());
      // System.out.println("Field type = " + field.getType().getName());
      registroFields = registroFields + iteField.getName() + (((numIte + 1) < listaField.length) ? "#" : "");
      // header[numIte] = iteField.getName();
      String field = iteField.getName();
      String method = "get" + field.substring(0, 1).toUpperCase() + field.substring(1, field.length());
      mapFieldValue.put(iteField.getName(), getClassFieldValue(parObj, method));
      numIte++;
    }
    // System.out.println(registroFields + "\n");
    return mapFieldValue;
  }

  public Object setPeticionEntidad(Map peticion, Object objeto) {

    Class clase = objeto.getClass();
    Field[] listaField = clase.getDeclaredFields();
    for (Field iteField : listaField) {
      String fieldName = iteField.getName();
      String methodNameSet = "set" + fieldName.substring(0, 1).toUpperCase()
          + fieldName.substring(1, fieldName.length());
      String objetoField = objeto.getClass().getSimpleName() + "_" + fieldName;
      Field field;
      try {
        field = clase.getDeclaredField(fieldName);
        if (field.getType().equals(String.class)) {
          if (peticion.containsKey(objetoField)) {
            clase.getMethod(methodNameSet, String.class).invoke(objeto, (String) peticion.get(objetoField));
          } else if (peticion.containsKey(fieldName)) {
            clase.getMethod(methodNameSet, String.class).invoke(objeto, (String) peticion.get(fieldName));
          }
        }
        if (field.getType().equals(int.class)) {
          int valorInt = 0;
          if (peticion.containsKey(objetoField)) {
            Object valor = (Object) peticion.get(objetoField);
            if (valor instanceof Integer) {
              valorInt = ((Integer) valor);
            } else {
              valorInt = (Integer.parseInt((String) valor));
            }
            clase.getMethod(methodNameSet, int.class).invoke(objeto, valorInt);
          } else if (peticion.containsKey(fieldName)) {
            Object valor = (Object) peticion.get(fieldName);
            if (valor instanceof Integer) {
              valorInt = ((Integer) valor);
            } else {
              valorInt = (Integer.parseInt((String) valor));
            }
            clase.getMethod(methodNameSet, int.class).invoke(objeto, valorInt);
          }
        }
        if (field.getType().equals(double.class)) {
          double valorBigDecimal = 0.00;
          if (peticion.containsKey(objetoField)) {
            Object valor = (Object) peticion.get(objetoField);
            if (valor instanceof Double) {
              valorBigDecimal = ((Double) valor);
            } else {
              valorBigDecimal = Double.parseDouble((String) valor);
            }
            clase.getMethod(methodNameSet, double.class).invoke(objeto, valorBigDecimal);
          } else if (peticion.containsKey(fieldName)) {
            Object valor = (Object) peticion.get(fieldName);
            if (valor instanceof Double) {
              valorBigDecimal = ((Double) valor);
            } else {
              valorBigDecimal = Double.parseDouble((String) valor);
            }
            clase.getMethod(methodNameSet, double.class).invoke(objeto, valorBigDecimal);
          }
        }
        if (field.getType().equals(BigDecimal.class)) {
          BigDecimal valorBigDecimal = new BigDecimal("00000000000");
          if (peticion.containsKey(objetoField)) {
            Object valor = (Object) peticion.get(objetoField);
            if (valor instanceof String) {
              valorBigDecimal = new BigDecimal((String) valor);
            } else {
              valorBigDecimal = (BigDecimal) valor;
            }
            clase.getMethod(methodNameSet, BigDecimal.class).invoke(objeto, valorBigDecimal);
          } else if (peticion.containsKey(fieldName)) {
            Object valor = (Object) peticion.get(fieldName);
            if (valor instanceof String) {
              valorBigDecimal = new BigDecimal((String) valor);
            } else {
              valorBigDecimal = (BigDecimal) valor;
            }
            clase.getMethod(methodNameSet, BigDecimal.class).invoke(objeto, valorBigDecimal);
          }
        }
        if (field.getType().equals(boolean.class)) {
          boolean valorBoolean = false;
          if (peticion.containsKey(objetoField)) {
            Object valor = (Object) peticion.get(objetoField);
            if (valor instanceof Boolean) {
              valorBoolean = ((Boolean) valor);
            } else {
              valorBoolean = Boolean.parseBoolean((String) valor);
            }
            clase.getMethod(methodNameSet, boolean.class).invoke(objeto, valorBoolean);
          } else if (peticion.containsKey(fieldName)) {
            Object valor = (Object) peticion.get(fieldName);
            if (valor instanceof Boolean) {
              valorBoolean = ((Boolean) valor);
            } else {
              valorBoolean = Boolean.parseBoolean((String) valor);
            }
            clase.getMethod(methodNameSet, boolean.class).invoke(objeto, valorBoolean);
          }
        }
        if (field.getType().equals(Date.class)) {
          Date fecha = null;
          if (peticion.containsKey(objetoField)) {
            if (peticion.get(objetoField) instanceof String) {
              if (Util.getInst().isDateFormato((String) peticion.get(objetoField), "dd/MM/yyyy")) {
                fecha = Util.getInst().fromStrdateToDate((String) peticion.get(objetoField), "dd/MM/yyyy");
              }
            } else {
              fecha = (Date) peticion.get(objetoField);
            }
          } else if (peticion.containsKey(fieldName)) {
            if (peticion.get(fieldName) instanceof String) {
              if (Util.getInst().isDateFormato((String) peticion.get(fieldName), "dd/MM/yyyy")) {
                fecha = Util.getInst().fromStrdateToDate((String) peticion.get(fieldName), "dd/MM/yyyy");
              }
            } else {
              fecha = (Date) peticion.get(fieldName);
            }
          }
          if (fecha != null) {
            clase.getMethod(methodNameSet, Date.class).invoke(objeto, fecha);
          }
        }
        if (field.getType().equals(java.sql.Date.class)) {
          Date fecha = null;
          if (peticion.containsKey(objetoField)) {
            if (peticion.get(objetoField) instanceof String) {
              if (Util.getInst().isDateFormato((String) peticion.get(objetoField), "dd/MM/yyyy")) {
                fecha = Util.getInst().fromStrdateToDateSqlFormato((String) peticion.get(objetoField), "dd/MM/yyyy");
              }
            } else {
              fecha = (java.sql.Date) peticion.get(objetoField);
            }
          } else if (peticion.containsKey(fieldName)) {
            if (peticion.get(fieldName) instanceof String) {
              if (Util.getInst().isDateFormato((String) peticion.get(fieldName), "dd/MM/yyyy")) {
                fecha = Util.getInst().fromStrdateToDateSqlFormato((String) peticion.get(fieldName), "dd/MM/yyyy");
              }
            } else {
              fecha = (java.sql.Date) peticion.get(fieldName);
            }
          }
          if (fecha != null) {
            clase.getMethod(methodNameSet, java.sql.Date.class).invoke(objeto, fecha);
          }
        }
        if (field.getType().equals(java.sql.Time.class)) {
          Time hora = null;
          if (peticion.containsKey(objetoField)) {
            if (peticion.get(objetoField) instanceof String) {
              if (Util.getInst().isTime((String) peticion.get(objetoField))) {
                hora = Util.getInst().fromStrdateToTime((String) peticion.get(objetoField));
              }
            } else {
              hora = (java.sql.Time) peticion.get(objetoField);
            }
          } else if (peticion.containsKey(fieldName)) {
            if (peticion.get(fieldName) instanceof String) {
              if (Util.getInst().isTime((String) peticion.get(objetoField))) {
                hora = Util.getInst().fromStrdateToTime((String) peticion.get(objetoField));
              }
            } else {
              hora = (java.sql.Time) peticion.get(objetoField);
            }
          }
          if (hora != null) {
            clase.getMethod(methodNameSet, java.sql.Time.class).invoke(objeto, hora);
          }
        }
        if (field.getType().equals(java.sql.Timestamp.class)) {
          Timestamp fechaHora = null;
          if (peticion.containsKey(objetoField)) {
            if (peticion.get(objetoField) instanceof String) {
              if (Util.getInst().isTimestamp((String) peticion.get(objetoField))) {
                fechaHora = Util.getInst().fromStrdateToTimestamp((String) peticion.get(objetoField));
              }
            } else {
              fechaHora = (java.sql.Timestamp) peticion.get(objetoField);
            }
          } else if (peticion.containsKey(fieldName)) {
            if (peticion.get(fieldName) instanceof String) {
              if (Util.getInst().isTimestamp((String) peticion.get(objetoField))) {
                fechaHora = Util.getInst().fromStrdateToTimestamp((String) peticion.get(objetoField));
              }
            } else {
              fechaHora = (java.sql.Timestamp) peticion.get(objetoField);
            }
          }
          if (fechaHora != null) {
            clase.getMethod(methodNameSet, java.sql.Timestamp.class).invoke(objeto, fechaHora);
          }
        }
      } catch (SecurityException e) {
        e.printStackTrace();
      } catch (NoSuchFieldException e) {
        e.printStackTrace();
      } catch (IllegalArgumentException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return objeto;
  }// Fin de limpiarInstancia()

  public String parsearCampoObjeto(String campoObjeto) {

    String valor = "";
    if (campoObjeto.contains("_")) {
      valor = campoObjeto.substring(campoObjeto.indexOf("_") + 1, campoObjeto.length());
    } else {
      valor = campoObjeto;
    }
    return valor;
  }

  public Object obtenerDatoObjeto(Object objeto, String campo) {

    campo = parsearCampoObjeto(campo);
    Class clase = objeto.getClass();
    Field[] listaField = clase.getDeclaredFields();
    String value = "";
    for (Field iteField : listaField) {
      String fieldName = iteField.getName();
      if (campo.equals(fieldName)) {
        String methodNameGet = "get" + fieldName.substring(0, 1).toUpperCase()
            + fieldName.substring(1, fieldName.length());
        String objetoField = objeto.getClass().getSimpleName() + "_" + fieldName;
        Field field;
        try {
          field = clase.getDeclaredField(fieldName);
          if (field.getType().equals(String.class)) {
            value = (String) clase.getMethod(methodNameGet, null).invoke(objeto, null);
          }
          if (field.getType().equals(int.class)) {
            int respuesta = (Integer) clase.getMethod(methodNameGet, null).invoke(objeto, null);
            value = String.valueOf(respuesta);
          }
          if (field.getType().equals(double.class)) {
            double respuesta = (Double) clase.getMethod(methodNameGet, null).invoke(objeto, null);
            value = String.valueOf(respuesta);
          }
          if (field.getType().equals(boolean.class)) {
            methodNameGet = methodNameGet.replace("get", "is");
            boolean respuesta = (Boolean) clase.getMethod(methodNameGet, null).invoke(objeto, null);
            value = (respuesta) ? "true" : "false";
          }
          if (field.getType().equals(Date.class)) {
            Date respuesta = (Date) clase.getMethod(methodNameGet, null).invoke(objeto, null);
            value = fromDateToString(fromDateToDateSql(respuesta), FechaFormato.ddMMyyyy);
          }
          if (field.getType().equals(BigDecimal.class)) {
            BigDecimal respuesta = (BigDecimal) clase.getMethod(methodNameGet, null).invoke(objeto, null);
            value = String.valueOf(respuesta);
          }
          if (field.getType().equals(Timestamp.class)) {
            Timestamp respuesta = (Timestamp) clase.getMethod(methodNameGet, null).invoke(objeto, null);
            value = respuesta.toString();
          }
          break;
        } catch (SecurityException e) {
          e.printStackTrace();
        } catch (NoSuchFieldException e) {
          e.printStackTrace();
        } catch (IllegalArgumentException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (IllegalAccessException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (NoSuchMethodException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (InvocationTargetException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
    return value;
  }

  public String obtenerValorLista(List lista, String valor) {

    String resultado = "";
    for (int iteLis = 0; iteLis < lista.size(); iteLis++) {
      String itemLista = (String) lista.get(iteLis);
      if (itemLista.contains("#")) {
        String[] arrayItemLista = itemLista.split("#");
        if (arrayItemLista[1].equals(valor)) {
          resultado = arrayItemLista[0];
          break;
        }
      }
    }
    return resultado;
  }

  public Object obtenerItemLista(List lista, String entidad, String campo, String valor) {

    Object result = null;
    String valorObjeto = "";
    try {
      for (int iteLisInd = 0; iteLisInd < lista.size(); iteLisInd++) {
        Object objeto = lista.get(iteLisInd);
        valorObjeto = (String) obtenerDatoObjeto(objeto, campo);
        // System.out.println("valor buscado : " + valor + " valoro objeto ; " +
        // valorObjeto);
        if (valorObjeto.equals(valor)) {
          return objeto;
        }
      }
      result = Class.forName("com.comun.entidad." + entidad).newInstance();
      Util.getInst().limpiarEntidad(result, true);
    } catch (InstantiationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return result;
  }// Fin de obtenerDatoTabDetKar(int selectedRow)

  public Object actualizarItemLista(List lista, String entidad, String campo, String valor) {

    Object result = null;
    String valorObjeto = "";
    try {
      for (int iteLisInd = 0; iteLisInd < lista.size(); iteLisInd++) {
        Object objeto = lista.get(iteLisInd);
        valorObjeto = (String) obtenerDatoObjeto(objeto, campo);
        // System.out.println("valor buscado : " + valor + " valoro objeto ; " +
        // valorObjeto);
        if (valorObjeto.equals(valor)) {
          return objeto;
        }
      }
      result = Class.forName("com.comun.entidad." + entidad).newInstance();
      Util.getInst().limpiarEntidad(result, true);
    } catch (InstantiationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return result;
  }// Fin de obtenerDatoTabDetKar(int selectedRow)

  public Object getClassFieldValue(Object parObj, String parMethod) {

    // no paramater
    Class noparams[] = {};
    Object result = null;
    try {
      if (parObj != null) {
        Class objClass = parObj.getClass();
        Method method = objClass.getMethod(parMethod, null);
        result = method.invoke(parObj, null);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return result;
  }

  public Object setClassFieldValue(Object parObj, String parMethod) {

    // no paramater
    Class noparams[] = {};
    Object result = null;
    try {
      if (parObj != null) {
        Class objClass = parObj.getClass();
        Method method = objClass.getMethod(parMethod, null);
        result = method.invoke(parObj, null);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return result;
  }

  public String parsearFieldJava(Object parObj) {

    Field[] listaField = obtenerListaFields(parObj);
    Map<String, Object> mapFieldValue = obtenerMapaFields(parObj);
    String registroObjeto = "";
    List<String> listaTagInputHidden = new ArrayList<String>();
    for (Field iteListaField : listaField) {
      registroObjeto = registroObjeto + iteListaField.getName() + " : " + mapFieldValue.get(iteListaField.getName());
    } // Fin de for(String iteLis : listaField){
    return registroObjeto;
  }

  public JsonArray prueba(Object objeto) {
    Gson gson = new Gson();
    JsonArray jsonArray = null;
    JsonElement element = gson.toJsonTree(objeto);
    if (element.isJsonArray()) {
      // fail appropriately
      jsonArray = element.getAsJsonArray();
    }
    return jsonArray;
  }

  public String getDataJson(Object objeto) {

    Gson gson = new Gson();
    String json = gson.toJson(objeto);
    // json = json.replaceAll("\\[", "\\{");
    // json = json.replaceAll("\\]", "\\}");
    json = json.replaceAll("\"", "\'");
    return json;
  }

  public StringBuilder obtenerDataJson(Object objeto) {

    Class clase = objeto.getClass();
    Field[] listaField = clase.getDeclaredFields();
    StringBuilder dataJson = new StringBuilder();
    int ite = 0;
    dataJson.append("{'" + objeto.getClass().getSimpleName() + "':{");
    for (Field iteField : listaField) {
      String fieldName = iteField.getName();
      String methodNameGet = "get" + fieldName.substring(0, 1).toUpperCase()
          + fieldName.substring(1, fieldName.length());
      String value = "";
      Field field;
      try {
        field = clase.getDeclaredField(fieldName);
        if (field.getType().equals(String.class)) {
          value = (String) clase.getMethod(methodNameGet, null).invoke(objeto, null);
        }
        if (field.getType().equals(int.class)) {
          int respuesta = (Integer) clase.getMethod(methodNameGet, null).invoke(objeto, null);
          value = String.valueOf(respuesta);
        }
        if (field.getType().equals(double.class)) {
          double respuesta = (Double) clase.getMethod(methodNameGet, null).invoke(objeto, null);
          value = String.valueOf(respuesta);
        }
        if (field.getType().equals(boolean.class)) {
          methodNameGet = methodNameGet.replace("get", "is");
          boolean respuesta = (Boolean) clase.getMethod(methodNameGet, null).invoke(objeto, null);
          value = (respuesta) ? "true" : "false";
        }
        if (field.getType().equals(Date.class)) {
          Date respuesta = (Date) clase.getMethod(methodNameGet, null).invoke(objeto, null);
          value = fromDateToString(fromDateToDateSql(respuesta), FechaFormato.ddMMyyyy);
        }
        if (field.getType().equals(BigDecimal.class)) {
          BigDecimal respuesta = (BigDecimal) clase.getMethod(methodNameGet, null).invoke(objeto, null);
          value = String.valueOf(respuesta);
        }
        if (field.getType().equals(Timestamp.class)) {
          Timestamp respuesta = (Timestamp) clase.getMethod(methodNameGet, null).invoke(objeto, null);
          value = respuesta.toString();
        }
      } catch (SecurityException e) {
        e.printStackTrace();
      } catch (NoSuchFieldException e) {
        e.printStackTrace();
      } catch (IllegalArgumentException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      ite++;
      if (ite == listaField.length) {
        dataJson.append("'" + fieldName + "' : '" + value + "'");
      } else {
        dataJson.append("'" + fieldName + "' : '" + value + "',");
      }
    }
    dataJson.append("}}");
    System.out.println(dataJson);
    return dataJson;
  }

  public void limpiarEntidad(Object objeto, boolean forzar) {

    if (objeto != null) {
      if (validarObjetoInicial(objeto) || forzar) {
        Class clase = objeto.getClass();
        Field[] listaField = clase.getDeclaredFields();
        for (Field iteField : listaField) {
          String fieldName = iteField.getName();
          String methodNameSet = "set" + fieldName.substring(0, 1).toUpperCase()
              + fieldName.substring(1, fieldName.length());
          // String methodNameGet = "get" + fieldName.substring(0,
          // 1).toUpperCase() + fieldName.substring(1, fieldName.length());
          Field field;
          try {
            // method = clase.getMethod(methodNameGet, null);
            // System.out.println("valor : " + method.invoke(objeto, null));
            field = clase.getDeclaredField(fieldName);
            if (field.getType().equals(String.class)) {
              clase.getMethod(methodNameSet, String.class).invoke(objeto, "");
            }
            if (field.getType().equals(int.class)) {
              clase.getMethod(methodNameSet, int.class).invoke(objeto, 0);
            }
            if (field.getType().equals(double.class)) {
              clase.getMethod(methodNameSet, double.class).invoke(objeto, 0.00);
            }
            if (field.getType().equals(BigDecimal.class)) {
              clase.getMethod(methodNameSet, BigDecimal.class).invoke(objeto, new BigDecimal("0"));
            }
            if (field.getType().equals(boolean.class)) {
              clase.getMethod(methodNameSet, boolean.class).invoke(objeto, false);
            }
            if (field.getType().equals(Date.class)) {
              clase.getMethod(methodNameSet, Date.class).invoke(objeto, Dates.getCurrentDate(""));
            }
            if (field.getType().equals(java.sql.Date.class)) {
              clase.getMethod(methodNameSet, java.sql.Date.class).invoke(objeto,
                  fromDateToDateSql(Dates.getCurrentDate("")));
            }
            if (field.getType().equals(java.sql.Time.class)) {
              clase.getMethod(methodNameSet, java.sql.Time.class).invoke(objeto, new Time(0, 0, 0));
            }
            if (field.getType().equals(java.sql.Timestamp.class)) {
              clase.getMethod(methodNameSet, java.sql.Timestamp.class).invoke(objeto, new Timestamp(18000000));
            }
          } catch (SecurityException e) {
            e.printStackTrace();
          } catch (NoSuchFieldException e) {
            e.printStackTrace();
          } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      } // if(validarObjetoInicial(objeto))
    } // if(objeto != null)
  }

  public void limpiarEntidad(Object objeto) {

    limpiarEntidad(objeto, false);
  }// Fin de limpiarInstancia()

  public void imprimiValores(Object objeto) {

    Class clase = objeto.getClass();
    Field[] listaField = clase.getDeclaredFields();
    String record = objeto.getClass().getSimpleName();
    for (Field iteField : listaField) {
      String fieldName = iteField.getName();
      String methodNameGet = "get" + fieldName.substring(0, 1).toUpperCase()
          + fieldName.substring(1, fieldName.length());
      if (iteField.getType().equals(boolean.class)) {
        methodNameGet = methodNameGet.replace("get", "is");
      }
      Field field;
      try {
        field = clase.getDeclaredField(fieldName);
        record = record + " : " + clase.getMethod(methodNameGet, null).invoke(objeto, null);
      } catch (SecurityException e) {
        e.printStackTrace();
      } catch (NoSuchFieldException e) {
        e.printStackTrace();
      } catch (IllegalArgumentException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    Log.debug(this, record);
  }

  public boolean validarObjetoInicial(Object objeto) {

    boolean esObjetoInicial = false;
    Class clase = objeto.getClass();
    Field[] listaField = clase.getDeclaredFields();
    Object record;
    for (Field iteField : listaField) {
      String fieldName = iteField.getName();
      if (fieldName.contains("_cod") || fieldName.contains("codigo")) {
        String methodNameGet = "get" + fieldName.substring(0, 1).toUpperCase()
            + fieldName.substring(1, fieldName.length());
        if (iteField.getType().equals(boolean.class)) {
          methodNameGet = methodNameGet.replace("get", "is");
        }
        Field field;
        try {
          field = clase.getDeclaredField(fieldName);
          record = clase.getMethod(methodNameGet, null).invoke(objeto, null);
          if (record == null
              && (field.getType().equals(String.class) || field.getType().equals(java.sql.Date.class)
                  || field.getType().equals(java.sql.Time.class) || field.getType().equals(Date.class)
                  || field.getType().equals(boolean.class) || field.getType().equals(int.class)
                  || field.getType().equals(double.class) || field.getType().equals(BigDecimal.class))
              || field.getType().equals(Timestamp.class)) {
            esObjetoInicial = true;
            break;
          }
        } catch (SecurityException e) {
          e.printStackTrace();
        } catch (NoSuchFieldException e) {
          e.printStackTrace();
        } catch (IllegalArgumentException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (IllegalAccessException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (NoSuchMethodException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (InvocationTargetException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      } // if(fieldName.contains("cod") || fieldName.contains("codigo"))
    }
    return esObjetoInicial;
  }

  public static void clonarEntidad(Object objetoFuente, Object objetoObjetivo) {

    Class claseFuente = objetoFuente.getClass();
    Class claseObjetivo = objetoObjetivo.getClass();
    Field[] listaFieldFuente = claseFuente.getDeclaredFields();
    for (Field iteField : listaFieldFuente) {
      String fieldName = iteField.getName();
      String methodNameGet = "get" + fieldName.substring(0, 1).toUpperCase()
          + fieldName.substring(1, fieldName.length());
      String methodNameSet = "set" + fieldName.substring(0, 1).toUpperCase()
          + fieldName.substring(1, fieldName.length());
      Field field;
      try {
        field = claseFuente.getDeclaredField(fieldName);
        if (field.getType().equals(String.class)) {
          String valor = (String) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, String.class).invoke(objetoObjetivo, valor);
        }
        if (field.getType().equals(int.class)) {
          int valor = (Integer) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, Integer.class).invoke(objetoObjetivo, valor);
        }
        if (field.getType().equals(double.class)) {
          double valor = (Double) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, Double.class).invoke(objetoObjetivo, valor);
        }
        if (field.getType().equals(boolean.class)) {
          boolean valor = (Boolean) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, Boolean.class).invoke(objetoObjetivo, valor);
        }
        if (field.getType().equals(Date.class)) {
          Date valor = (Date) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, Date.class).invoke(objetoObjetivo, valor);
        }
        if (field.getType().equals(java.sql.Date.class)) {
          java.sql.Date valor = (java.sql.Date) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, java.sql.Date.class).invoke(objetoObjetivo, valor);
        }
        if (field.getType().equals(Time.class)) {
          Time valor = (Time) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, Time.class).invoke(objetoObjetivo, valor);
        }
        if (field.getType().equals(Timestamp.class)) {
          Timestamp valor = (Timestamp) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, Timestamp.class).invoke(objetoObjetivo, valor);
        }
        if (field.getType().equals(BigDecimal.class)) {
          BigDecimal valor = (BigDecimal) claseFuente.getMethod(methodNameGet, null).invoke(objetoFuente);
          claseObjetivo.getMethod(methodNameSet, BigDecimal.class).invoke(objetoObjetivo, valor);
        }
      } catch (SecurityException e) {
        e.printStackTrace();
      } catch (NoSuchFieldException e) {
        e.printStackTrace();
      } catch (IllegalArgumentException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }// Fin de limpiarInstancia()

  public ImageIcon leerBlob(byte[] parResSql) {

    ImageIcon emp_logo;
    if (parResSql != null) {
      emp_logo = new ImageIcon(parResSql);
    } else {
      emp_logo = null;
    }
    return emp_logo;
  }

  public static boolean esVacio(String test) {

    boolean lbEmpty = false;
    if (test == null || test.trim().length() <= 0) {
      lbEmpty = true;
    }
    return lbEmpty;
  }

  /**
   * Copies the contents from a file to another. Method assumes that files
   * exist(all checks should be done before calling method).
   * 
   * @param originalFile
   *          - (File to copy from)
   * @param newFile
   *          - (File to copy to)
   */
  public static void copyFile(File originalFile, File newFile) {

    FileInputStream from = null;
    FileOutputStream to = null;
    try {
      from = new FileInputStream(originalFile);
      to = new FileOutputStream(newFile);
      byte buffer[] = new byte[from.available()];
      int bytesRead;
      while ((bytesRead = from.read(buffer)) > -1) {
        to.write(buffer, 0, bytesRead);
      }
      if (!newFile.exists()) {
        throw new IOException("The file could not be created.");
      }
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Error copying files, originalFile[" + originalFile.getAbsolutePath() + "], newFile["
          + newFile.getAbsolutePath() + "]");
    } finally {
      if (from != null) {
        try {
          from.close();
        } catch (IOException e) {
          System.out.println("Error copying files(closing from stream), originalFile[" + originalFile.getAbsolutePath()
              + "], newFile[" + newFile.getAbsolutePath() + "]");
        }
      }
      if (to != null) {
        try {
          to.close();
        } catch (IOException e) {
          System.out.println("Error copying files(closing to stream), originalFile[" + originalFile.getAbsolutePath()
              + "], newFile[" + newFile.getAbsolutePath() + "]");
        }
      }
    }
  }

  public static String getDateFormat(Object date, String pattern) {

    return Util.getDateFormat(date, pattern, null);
  }

  public static String getDateFormat(Object date, String pattern, String locale) {

    SimpleDateFormat formatter = null;
    if (Util.isEmpty(locale)) {
      formatter = new SimpleDateFormat(pattern);
    } else {
      if (Util.isEmpty(locale)) {
        locale = "es";
      }
      formatter = new SimpleDateFormat(pattern, new Locale(locale));
    }
    String objReturn = "";
    if (Util.isEmpty(pattern)) {
      pattern = "yyyy-MM-dd";
    }
    try {
      if (date == null || (date instanceof String && Util.isEmpty((String) date))) {
        date = new Date(Calendar.getInstance().getTimeInMillis());
      } else if (date instanceof String) {
        date = Dates.getDate((String) date);
      }
      objReturn = formatter.format(date);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return objReturn;
  }

  public static boolean isObjectEmpty(Object object) {

    boolean lbEmpty = false;
    if (object != null) {
      String objString = "";
      if (object instanceof String) {
        objString = (String) object;
      } else {
        objString = object.toString();
      }
      lbEmpty = Util.isEmpty(objString);
    } else {
      lbEmpty = true;
    }
    return lbEmpty;
  }

  public static boolean isEmail(String correo) {

    Pattern pat = null;
    Matcher mat = null;
    pat = Pattern
        .compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
    mat = pat.matcher(correo);
    if (mat.find()) {
      System.out.println("[" + mat.group() + "]");
      return true;
    } else {
      return false;
    }
  }

  public static boolean isEmpty(BigDecimal test) {
    return isEmpty(test.toString());
  }

  public static boolean isEmpty(String test) {

    boolean lbEmpty = false;
    if (test == null || test.equals("0")) {
      test = "";
    }
    if (test == null || test.trim().length() <= 0 || test.trim().length() == 0) {
      lbEmpty = true;
    }
    return lbEmpty;
  }

  public static boolean isEmpty(java.sql.Date test) {

    boolean lbEmpty = false;
    if (test == null) {
      lbEmpty = true;
    }
    return lbEmpty;
  }

  /**
   * ckNull Method
   * 
   * @param test
   *          String value to test - set to an empty string if null
   * @return String
   */
  public static String ckNull(String test) {

    if (test == null) {
      test = "";
    } else {
      if (test.length() <= 0) {
        test = "";
      }
    }
    return test;
  }

  public static String formatString(String sformat, String temp) throws Exception {

    String formatted = "";
    int ilen = 0;
    int itempLen = 0;
    char[] chTemp = temp.toCharArray();
    char[] chFormat = sformat.toCharArray();
    char[] chReturn = chFormat;
    ilen = chFormat.length;
    itempLen = chTemp.length;
    int icnt = 0;
    try {
      for (int i = itempLen - 1; i >= 0; i--) {
        chReturn[(ilen - 1) - icnt] = chTemp[i];
        icnt++;
      }
      formatted = new String(chReturn);
    } catch (IndexOutOfBoundsException iobe) {
      iobe.printStackTrace();
    } catch (Exception e) {
      throw e;
    }
    return formatted;
  }

  public static BigDecimal getBigDecimalFormatted(Object object, int decimalPlaces) {

    BigDecimal bd = new BigDecimal("0.00");
    try {
      if (object instanceof String) {
        String objectString = (String) object;
        if (!Util.isEmpty(objectString)) {
          bd = new BigDecimal(objectString);
        }
      } else {
        bd = (BigDecimal) object;
      }
      if (bd == null) {
        bd = new BigDecimal("0.00000");
      }
      bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bd;
  }

  public static String searchAndReplace(String originalString, String searchString, String replaceString,
      boolean ignoreCase) {

    String tempString = "";
    String tempSearch = "";
    if (ignoreCase) {
      tempString = originalString.toLowerCase();
      tempSearch = searchString.toLowerCase();
    } else {
      tempString = originalString;
      tempSearch = searchString;
    }
    StringBuffer lsbString = new StringBuffer(originalString);
    StringBuffer lsbStringtemp = new StringBuffer(tempString);
    int liStart = 0;
    int liEnd = 0;
    liStart = lsbStringtemp.toString().indexOf(tempSearch);
    liEnd = searchString.length() + liStart;
    while (liStart >= 0) {
      lsbString.replace(liStart, liEnd, replaceString);
      lsbStringtemp.replace(liStart, liEnd, replaceString);
      liStart = lsbStringtemp.toString().indexOf(searchString);
      liEnd = searchString.length() + liStart;
    }
    return lsbString.toString();
  }

  // returns the name of the month when given the numeric value
  public static String getMonthName(java.math.BigDecimal bd_month) {

    String month = "";
    final String monthNames[] = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
        "October", "November", "December" };
    if (bd_month != null) {
      bd_month = bd_month.subtract(new BigDecimal(1));
      month = monthNames[bd_month.intValue()];
    }
    return month;
  }

  /**
   * isAlphaCharacter Method Tests for an alphabetic character
   * 
   * @param test
   *          char
   * @return boolean
   */
  public static boolean isAlphaChar(char test) {

    String alphaCharacters = "AabBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
    boolean lbAlpha = false;
    lbAlpha = (alphaCharacters.indexOf(test) >= 0);
    return lbAlpha;
  }

  /**
   * numericFilter Method Filters to return only numeric characters
   * 
   * @param filterString
   *          String
   * @return String
   */
  public static String numericFilter(String filterString) {

    String numericCharacters = "0123456789";
    String returnString = "";
    String test = "";
    for (int i = 0; i < Util.ckNull(filterString).length(); i++) {
      test = filterString.substring(i, i + 1);
      if (numericCharacters.indexOf(test) >= 0) {
        returnString += test;
      }
    }
    return returnString;
  }

  /**
   * @param fileName
   * @param organizationId
   * @return File
   */
  public static File getOidFile(String fileName, String organizationId) {

    Log.debug(Util.class, "getOidFile: fileName: " + fileName + ", organizationId" + organizationId);
    File file = new File(fileName);
    File ret = null;
    File dir = file.getParentFile();
    try {
      if (dir.exists()) {
        Log.debug(Util.class, "directory exists");
        // look for file in organization's directory
        File tempDir = null;
        if (organizationId != null) {
          tempDir = new File(dir.getPath() + File.separator + organizationId.toString().toLowerCase() + File.separator
              + file.getName());
        }
        if (tempDir == null || !tempDir.exists()) {
          // look for file in sisem directory
          tempDir = new File(dir.getPath() + File.separator + file.getName());
          if (!tempDir.exists()) {
            // look for file in normal directory
            tempDir = new File(dir.getPath() + File.separator + file.getName());
            if (!tempDir.exists()) {
              tempDir = new File(file.getName());
              Log.error("Util", fileName + " could not be found!");
            }
          }
        }
        ret = tempDir;
      } else {
        Log.debug(Util.class, "directory does not exists");
        dir = dir.getParentFile();
        Log.debug(Util.class, "parent is: " + dir.getPath());
        if (dir.exists()) {
          File tempDir = new File(dir.getPath() + File.separator + file.getName());
          if (tempDir.exists()) {
            ret = tempDir;
          } else {
            Log.error(Util.class, tempDir.getPath() + " was not found");
          }
        } else {
          Log.error(Util.class, "Parent " + dir.getPath() + " was not found");
        }
      }
    } catch (Exception e) {
      Log.error(Util.class, fileName + " file was not found.");
    }
    Log.debug(Util.class, fileName + "found in [" + ret.getAbsolutePath() + "]");
    return ret;
  }

  public static String replaceAll(String replaceString, String originalString, String replacementString) {

    String returnStr = "";
    if (replacementString == null) {
      replacementString = "";
    }
    if (originalString.indexOf(replaceString) < 0) {
      return originalString;
    }
    try {
      // Compile regular expression
      Pattern pattern = Pattern.compile(replaceString);
      // Replace all occurrences of pattern in input
      Matcher matcher = pattern.matcher(originalString);
      int index = replacementString.indexOf("$");
      StringBuffer sb = new StringBuffer(replacementString);
      while (index > -1) {
        sb.replace(index, index + 1, "\\$");
        index = sb.indexOf("$", index + 3);
        replacementString = sb.toString();
      }
      returnStr = matcher.replaceAll(replacementString);
    } catch (Exception e) {
      returnStr = originalString;
      System.out.println("replaceString: " + replaceString);
      System.out.println("originalString: " + originalString);
      System.out.println("replacementString: " + replacementString);
      e.printStackTrace();
    } finally {
      // clean up
    }
    return returnStr;
  }

  public static Document loadXml(String fileName, String organizationId) {

    Document document = null;
    try {
      // File f = new File(ruleName);
      File f = Util.getOidFile(fileName, organizationId);
      if (f.exists()) {
        DOMBuilder docBuilder = new DOMBuilder();
        document = docBuilder.build(f);
      } else {
        throw new Exception("File not found for: " + fileName);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
      // system.out.println(exception.toString());
    }
    return document;
  }

  public static String nonAlphanumericFilter(String filterString) {

    String nonAlphanumericCharacters = "{}[],.<>:'?/|`~!@#$%^&*()_-+\\" + '"';
    String returnString = "";
    String test = "";
    for (int i = 0; i < Util.ckNull(filterString).length(); i++) {
      test = filterString.substring(i, i + 1);
      if (nonAlphanumericCharacters.indexOf(test) >= 0) {
        returnString += test;
      }
    }
    return returnString;
  }

  /**
   * ckNullBigDecimal Method
   * 
   * @param test
   *          BigDecimal value to test - set zero if null
   * @return BigDecimal
   */
  public static BigDecimal ckNull(BigDecimal test) {

    if (test == null) {
      test = new BigDecimal(0);
    }
    return test;
  }

  public static Integer ckNull(Integer test) {

    if (test == null) {
      test = new Integer(0);
    }
    return test;
  }

  public static java.util.Date ckNull(java.util.Date test) {

    if (test == null) {
      test = new java.util.Date();
    }
    return test;
  }

  public static java.sql.Date ckNull(java.sql.Date test) {

    if (test == null) {
      test = new java.sql.Date((new java.util.Date()).getTime());
    }
    return test;
  }

  public static java.util.List ckNull(java.util.List test) {

    if (test == null) {
      test = new ArrayList();
    }
    return test;
  }

  public static Object ckNull(Object o) {

    if (o == null) {
      if (o instanceof String) {
        o = "";
      } else if (o instanceof BigDecimal) {
        o = new BigDecimal(0);
      } else if (o instanceof Integer) {
        o = new Integer(0);
      } else if (o instanceof java.util.Date) {
        o = new java.util.Date();
      } else if (o instanceof java.sql.Date) {
        o = new java.sql.Date((new java.util.Date()).getTime());
      }
    }
    return o;
  }

  public static String encodeHtml(String myString) {

    if (myString == null) {
      return null;
    }
    int length = myString.length();
    StringBuffer encodedString = new StringBuffer(2 * length);
    for (int i = 0; i < length; i++) {
      char c = myString.charAt(i);
      if (c == '<') {
        encodedString.append("&lt;");
      } else if (c == '>') {
        encodedString.append("&gt;");
      } else if (c == '&') {
        encodedString.append("&amp;");
      } else if (c == '"') {
        encodedString.append("&quot;");
      } else if (c == '\'') {
        encodedString.append("'");
      } else if (c == '?') {
        encodedString.append("&deg");
      } else if (c == '?') {
        encodedString.append("");
      } else if (c == '?') {
        encodedString.append("");
      } else if (c == '?') {
        encodedString.append("?");
      } else {
        encodedString.append(c);
      }
    }
    return encodedString.toString();
  }

  public static String encodeForDisplay(String myString) {

    if (myString == null) {
      return null;
    }
    int length = myString.length();
    StringBuffer encodedString = new StringBuffer(2 * length);
    for (int i = 0; i < length; i++) {
      char c = myString.charAt(i);
      if (c == '<') {
        encodedString.append("&lt;");
      } else if (c == '>') {
        encodedString.append("&gt;");
      } else if (c == '&') {
        encodedString.append("&amp;");
      } else if (c == '"') {
        encodedString.append("&quot;");
      } else if (c == '\'') {
        encodedString.append("&#39;");
      } else {
        encodedString.append(c);
      }
    }
    return encodedString.toString();
  }

  public static String getFormattedCurrency(Object object, String currencyCode, String organizationId) {

    String formatted = object.toString();
    NumberFormat nf = null;
    nf.setMaximumFractionDigits(2);
    nf.setMinimumFractionDigits(2);
    formatted = nf.format(object);
    return formatted;
  }

  public int aleatorio(int min, int max) {

    max = (max) + 1;
    min = min;
    return ((int) (Math.random() * (max - min)) + min);
  }

  public String[] crearRegistroFiltro(String columna, String tipo, String operador, String expresion, String logico,
      String orden) {

    String[] registroFiltro = { columna, tipo, operador, expresion, logico, orden };
    return registroFiltro;
  }

  public void obtenerPeticionFiltrosBusqueda(Map peticion, List listaFiltro) {

    if (listaFiltro != null && listaFiltro.size() > 0) {
      int numFil = listaFiltro.size();
      String[] filtroColumna = new String[numFil];
      String[] filtroTipo = new String[numFil];
      String[] filtroOperador = new String[numFil];
      String[] filtroValor = new String[numFil];
      String[] filtroLogico = new String[numFil];
      String[] filtroOrden = new String[numFil];
      String[] filtroOriginal = new String[numFil];
      for (int iteFil = 0; iteFil < numFil; iteFil++) {
        String[] registroFiltro = (String[]) listaFiltro.get(iteFil);
        filtroColumna[iteFil] = registroFiltro[0];
        filtroTipo[iteFil] = registroFiltro[1];
        filtroOperador[iteFil] = registroFiltro[2];
        filtroValor[iteFil] = registroFiltro[3];
        filtroLogico[iteFil] = registroFiltro[4];
        filtroOrden[iteFil] = registroFiltro[5];
      }
      peticion.put("filtroColumna", filtroColumna);
      peticion.put("filtroTipo", filtroTipo);
      peticion.put("filtroOperador", filtroOperador);
      peticion.put("filtroValor", filtroValor);
      peticion.put("filtroLogico", filtroLogico);
      peticion.put("filtroOrden", filtroOrden);
      peticion.put("filtroOriginal", filtroOriginal);
    }
  }

  public static boolean uploadFileByFTP(String server, String user, String pass, String localPath, String remotePath) {

    try {
      URL url = new URL("ftp://" + user + ":" + pass + "@" + server + remotePath + ";type=i");
      URLConnection urlc = url.openConnection();
      OutputStream os = urlc.getOutputStream();
      // BufferedReader br = new BufferedReader(new FileReader(localPath));
      // BufferedReader br = new BufferedReader(new File(localPath));
      FileInputStream fil = new FileInputStream(new File(localPath));
      byte bytes[] = new byte[1024];
      int readCount = 0;
      while ((readCount = fil.read(bytes)) > 0) {
        os.write(bytes, 0, readCount);
      }
      os.flush();
      os.close();
      fil.close();
      // int c;
      // while ((c = br.read()) != -1) {
      // os.write(c);
      // }
      // os.flush();
      // os.close();
      // br.close();
      return true;
    } catch (Exception ex) {
      ex.printStackTrace();
      return false;
    }
  }

  public static boolean uploadFileBySFTP(String server, String user, String pass, String fileFuente,
      String fileDestino) {

    // String user = "testuser";
    // String server = "testserver.example.com";
    // String pass = "testpass";
    String khfile = "/home/testuser/.ssh/known_hosts";
    String identityfile = "/home/testuser/.ssh/id_rsa";
    JSch jsch = null;
    Session session = null;
    Channel channel = null;
    ChannelSftp channelSftp = null;
    try {
      jsch = new JSch();
      session = jsch.getSession(user, server, 22);

      session.setPassword(pass);
      khfile = "C:\\known_hosts";
      identityfile = "C:\\id_rsa";

      // jsch.setKnownHosts(khfile);
      // jsch.addIdentity(identityfile);

      Properties config = new Properties();
      config.put("StrictHostKeyChecking", "no");
      session.setConfig(config);

      session.connect();
      channel = session.openChannel("sftp");
      channel.connect();
      System.out.println("HostKey : " + channel.getSession().getHostKey());
      channelSftp = (ChannelSftp) channel;
    } catch (Exception e) {
      e.printStackTrace();
    }
    try {
      System.out.println("Starting File Upload:");
      System.out.println("File Source : " + fileFuente + "  FileDestino : " + fileDestino);
      // String fsrc = "/tmp/abc.txt", fdest = "/tmp/cde.txt";
      channelSftp.put(fileFuente, fileDestino);
      // channelSftp.get(fdest, "/tmp/testfile.bin");
      return true;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      channelSftp.disconnect();
      session.disconnect();
    }
    return false;
  }

  public static String servicioWebPHP(String urlSrvWeb, String metodo, String parametro, String valor) {
    Service service = new Service();
    Call call;
    String respuesta = "";
    try {
      call = (Call) service.createCall();
      call.setTargetEndpointAddress(urlSrvWeb);
      call.clearOperation();
      call.setOperationName(new QName("http://schemas.xmlsoap.org/wsdl/", metodo));
      QName QNAME_TYPE_STRING = new QName("string");
      call.addParameter(parametro, QNAME_TYPE_STRING, ParameterMode.IN);
      String[] params = { valor };
      call.setReturnType(QNAME_TYPE_STRING);
      respuesta = (String) call.invoke(params);
    } catch (ServiceException e) {
      e.printStackTrace();
    } catch (RemoteException e) {
      e.printStackTrace();
    }
    return respuesta;
  }

  public static void main(String[] args) {

    Function2<Integer, Integer, Integer> divide = (n1, n2) -> n1 / n2;
    System.out.println(Try.of(() -> divide.apply(10, 0)).isFailure());
    System.out.println(Try.of(() -> divide.apply(10, 0)).getOrElse(0));

    Try.of(() -> {
      String test = null;
      return test.split("-");
    })
    .onSuccess(succ -> {
      System.out.println("Evaluacion satisfactorio");
    })
    .onFailure(err -> {
      System.out.println("Ocurrio un error");
    });

    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {

        // Service service = new Service();
        // Call call;
        // try {
        // call = (Call) service.createCall();
        // String ws_url = "http://sisprom.freetzi.com/yurak/soap/server.php";
        // call.setTargetEndpointAddress(ws_url);
        // call.clearOperation();
        // call.setOperationName(new QName("http://schemas.xmlsoap.org/wsdl/",
        // "getAcceso"));
        // QName QNAME_TYPE_STRING = new QName("string");
        // call.addParameter("empresa", QNAME_TYPE_STRING, ParameterMode.IN);
        // String[] params = { "AMPATO" };
        // call.setReturnType(QNAME_TYPE_STRING);
        // String response = (String) call.invoke(params);
        // System.out.println(response);
        // } catch (ServiceException e) {
        // e.printStackTrace();
        // } catch (RemoteException e) {
        // e.printStackTrace();
        // }
      }
    });
  }

  public boolean crearBackupDatabase() {

    Date fechaActual = Util.getInst().getFechaSql();
    String rutaBackup = AplicacionGeneral.getInstance().obtenerRutaBackup();
    String nombreDatabase = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo())
        .getPropiedad("nombre-database", "");
    String nombreBackup = nombreDatabase + "_" + fechaActual.getDate() + (fechaActual.getMonth() + 1)
        + (fechaActual.getYear() + 1900);
    boolean exito = false;
    File backupFile = new File(rutaBackup + nombreBackup + ".bak");
    if (backupFile.exists()) {
      backupFile.delete();
    }
    if (!backupFile.exists()) {
      if (realizarBackup(backupFile.getPath(), nombreDatabase)) {
        exito = true;
      } // Fin de if(man.realizarBackup("dbbackup_20121208"))
    } // Fin de if(backupFile.exists())
    return exito;
  }// Fin de cmdRealizarBackupActionPerformed(ActionEvent arg0)

  public boolean realizarBackup(String archivoRuta, String nombreSistema) {

    String conSql;
    Connection conecction;
    CallableStatement cstm;
    boolean exito = false;
    try {
      conSql = "{call proc_backup_" + nombreSistema + "(?)}";
      conecction = DBConfigBackup.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection();
      cstm = conecction.prepareCall(conSql);
      cstm.setString(1, archivoRuta);
      cstm.execute();
      exito = true;
      cstm.close();
      conecction.close();
    } catch (SQLException e) {
      // util.mostrarMensaje("???Error en metodo agregar()",
      // this.getClass().getName());
      throw new RuntimeException(e);
    }
    return exito;
  }// Fin d e realizarBackup(String parNombreFile)

  public boolean generarBackupByFTP() {

    return crearBackupDatabase();
  }

  public boolean enviarBackupByFTP() {

    String rutaBackup = AplicacionGeneral.getInstance().obtenerRutaBackup();
    String nombreDatabase = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo())
        .getPropiedad("nombre-database", "");
    String nombreSistema = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo())
        .getPropiedad("nombre-sistema", "");
    String mes = Util.getInst().getCodTab("", (Util.getInst().getFechaSql().getMonth()), 2);
    String dia = Util.getInst().getCodTab("", (Util.getInst().getFechaSql().getDate() - 1), 2);
    String nombreBackup = nombreDatabase + "_" + dia + mes + (Util.getInst().getFechaSql().getYear() + 1900);
    String archivoRar = rutaBackup + nombreSistema + "\\" + nombreBackup + ".rar";
    boolean exito = false;
    File file = new File(archivoRar);
    while (!file.exists()) {
    }
    exito = Util.getInst().uploadFileByFTP("ftp.sisprom.com", "sispromc", "oXb6m5R79d",
        rutaBackup + nombreSistema + "\\" + nombreBackup + ".rar",
        "/backup/" + nombreSistema + "/" + nombreBackup + ".rar");
    return exito;
  }

  public boolean ejecutarProcedimientoSQLServer(String parProcedure) {

    String conSql;
    CallableStatement cstm;
    boolean exito = false;
    try {
      conSql = "{call " + parProcedure + "()}";
      cstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareCall(conSql);
      cstm.execute();
      exito = true;
      cstm.close();
    } catch (SQLException e) {
      // util.mostrarMensaje("???Error en metodo agregar()",
      // this.getClass().getName());
      throw new RuntimeException(e);
    }
    return exito;
  }// Fin d e realizarBackup(String parNombreFile)

  public void ejecutarSentencias(String archivo) {

    Statement stm = null;
    Connection conecction;
    List<String> listSql = leerArchivo(archivo);
    for (String iteSql : listSql) {
      conecction = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection();
      try {
        stm = conecction.createStatement();
        stm.execute(iteSql);
        conecction.close();
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    }
  }

  public String leerFuente() {

    String fuente = "";
    try {
      FileReader file = new FileReader("src/ManejoCaracter/LeerFuente.java");
      BufferedReader buff = new BufferedReader(file);
      boolean eof = false;
      while (!eof) {
        String line = buff.readLine();
        if (line == null)
          eof = true;
        else
          // System.out.println(line);
          fuente = fuente + line + "\n";
      }
      buff.close();
    } catch (IOException e) {
      System.out.println("Error -- " + e.toString());
    }
    return fuente;
  }

  public List<String> leerArchivo(String archivo) {

    String archivoSql = new String();
    List<String> listSql = new ArrayList<String>();
    try {
      File file = new File(archivo);
      BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
      String linea = "";
      try {
        while (bufferedReader.ready()) {
          linea = bufferedReader.readLine();
          if (linea.trim().toUpperCase().equals("GO")) {
            listSql.add(archivoSql);
            archivoSql = "";
          } else {
            archivoSql = archivoSql + linea + "\n";
          }
        }
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    System.out.println("Ejecutando sql... " + archivo);
    return listSql;
  }

  public int diferenciaFechas(Date fechaInicio, Date fechaFinal, String tipo) {

    long diff = fechaFinal.getTime() - fechaInicio.getTime();

    TimeUnit time = TimeUnit.DAYS;

    if (tipo.equals("DIAS")) {
      time = TimeUnit.DAYS;
    } else if (tipo.equals("HORAS")) {
      time = TimeUnit.HOURS;
    } else if (tipo.equals("MINUTOS")) {
      time = TimeUnit.MINUTES;
    } else if (tipo.equals("SEGUNDOS")) {
      time = TimeUnit.SECONDS;
    }
    long diffrence = time.convert(diff, TimeUnit.MILLISECONDS);

    return Integer.parseInt(String.valueOf(diffrence)) + 1;
  }

  private String findToken(StringBuffer sbdate) {
    String token = "";
    String sdate = sbdate.toString();

    if (sdate.indexOf("/") > 0) {
      token = "/";
    } else if (sdate.indexOf("-") > 0) {
      token = "-";
    } else if (sdate.indexOf("\\") > 0) {
      token = "/";
      sdate.replace('\\', '/');
    } else if (sdate.indexOf(".") > 0) {
      token = ".";
    } else {
    }

    sbdate.setLength(0);
    sbdate.append(sdate);

    return token;
  }

  public int obtenerDiasDelMes(Date fecha) {
    int ano = fecha.getYear() + 1900;
    int mes = fecha.getMonth() + 1;
    return obtenerDiasDelMes(mes, ano);
  }

  public int obtenerDiasDelMes(int mes, int ano) {
    switch (mes) {
    case 1: // Enero
    case 3: // Marzo
    case 5: // Mayo
    case 7: // Julio
    case 8: // Agosto
    case 10: // Octubre
    case 12: // Diciembre
      return 31;
    case 4: // Abril
    case 6: // Junio
    case 9: // Septiembre
    case 11: // Noviembre
      return 30;
    case 2: // Febrero
      if (((ano % 100 == 0) && (ano % 400 == 0)) || ((ano % 100 != 0) && (ano % 4 == 0)))
        return 29; // A?o Bisiesto
      else
        return 28;
    default:
      throw new java.lang.IllegalArgumentException("El mes debe estar entre 0 y 11");
    }
  }

  public java.sql.Date getSqlDate(String sDate) {
    if (sDate == null) {
      return null;
    } else {
      sDate = sDate.replaceAll("-", "/");
      java.util.Date date;

      if (sDate.length() < 1) {
        date = new Date();
      } else {
        date = new java.util.Date(sDate);
      }

      return new java.sql.Date(date.getTime());
    }
  }

  public java.sql.Date getSqlDate(String sDate, String format) {
    java.sql.Date sqlDate = null;

    if (sDate == null) {
      return null;
    } else {
      if (!Util.isEmpty(format) && format.length() == 10 || format.length() == 8) {
        format = format.replaceAll("Y", "y");
        format = format.replaceAll("D", "d");
      }

      sDate = sDate.replaceAll("-", "/");
      sDate = sDate.replaceAll("\\.", "/");
      format = format.replaceAll("-", "/");
      format = format.replaceAll("\\.", "/");

      if (sDate.length() < format.length())
        format = format.replaceAll("yyyy", "yy");

      java.util.Date date;

      if (sDate.length() < 1) {
        date = new Date();
      } else {
        String var = "";
        String nDate = "";
        if (sDate.indexOf(" ") > 0) {
          // if timestamp remove time
          sDate = sDate.substring(0, sDate.indexOf(" "));
        }
        StringTokenizer st = new StringTokenizer(sDate, "/");
        while (st.hasMoreTokens()) {
          var = st.nextToken();
          DecimalFormat df = new DecimalFormat("00");
          var = df.format(Long.parseLong(var));
          nDate = nDate + var;
          if (st.hasMoreTokens()) {
            nDate = nDate + "/";
          }
        }
        DateFormat dateFormatter = new SimpleDateFormat(format);
        dateFormatter.setLenient(false);
        try {
          date = dateFormatter.parse(nDate);
          sqlDate = new java.sql.Date(date.getTime());
        } catch (Exception e) {
          // Add msg here
          e.printStackTrace();
        }
      }
      return sqlDate;
    }
  }

  public boolean horaExisteEnRango(Time hora, Time horaIngreso, Time horaSalida) {
    boolean respuesta = false;
    Time horaZero = new Time(00, 00, 00);
    Time horaLast = new Time(23, 59, 59);
    // Time hora = new Time(00, 00, 00);
    // Time horaIngreso = new Time(19, 00, 00);
    // Time horaSalida = new Time(03, 59, 59);
    Time horaSalidaTmp = new Time(00, 00, 00);

    if (horaIngreso.getTime() < horaSalida.getTime()) {
      if (hora.getTime() >= horaIngreso.getTime() && hora.getTime() <= horaSalida.getTime()) {
        respuesta = true;
      }
    } else if (horaIngreso.getTime() > horaSalida.getTime()) {
      if ((hora.getTime() >= horaIngreso.getTime() && hora.getTime() <= horaLast.getTime())
          || hora.getTime() >= horaZero.getTime() && hora.getTime() <= horaSalida.getTime()) {
        respuesta = true;
      }
    }
    return respuesta;
  }

  public boolean existeHoraEnRangoConFecha(java.sql.Date fecha, Timestamp hora, Timestamp horaIngreso,
      Timestamp horaSalida) {
    boolean respuesta = false;

    java.sql.Date fechaCompare = new java.sql.Date(hora.getYear(), hora.getMonth(), hora.getDate());
    Timestamp horTemp = new Timestamp(18000000);
    horTemp.setHours(hora.getHours());
    horTemp.setMinutes(hora.getMinutes());
    horTemp.setSeconds(hora.getSeconds());

    if (fechaCompare.compareTo(fecha) == 1) {
      horTemp.setDate(hora.getDate() + 1);
    }

    if (horTemp.getTime() >= horaIngreso.getTime() && horTemp.getTime() <= horaSalida.getTime()) {
      respuesta = true;
    }
    return respuesta;
  }

  public boolean existeHoraEnRango(Timestamp fecHorReg, Time horaInicio, Time horaFinal) {
    boolean respuesta = false;
    if (fecHorReg.getTime() >= horaInicio.getTime() && fecHorReg.getTime() <= horaFinal.getTime()) {
      respuesta = true;
    }

    return respuesta;
  }

  public boolean existeHoraEnRango(Timestamp fecHorReg, Timestamp horaInicio, Timestamp horaFinal) {
    boolean respuesta = false;
    if (fecHorReg.getTime() >= horaInicio.getTime() && fecHorReg.getTime() <= horaFinal.getTime()) {
      respuesta = true;
    }

    return respuesta;
  }

  public boolean existeHoraEnRangoIngSal(Time fecHorRegIng, Time fecHorRegSal, Time horaInicio,
      Time horaFinal) {
    boolean respuesta = false;

    Timestamp fecHorTemp = new Timestamp(18000000);

    // horTemp.setDate(hora.getDate()+1);
    if (fecHorTemp.compareTo(fecHorRegSal) == 0) {// INGRESO
      Timestamp fecHorRegIngTmp = new Timestamp(18000000);
      fecHorRegIngTmp.setHours(fecHorRegIng.getHours());
      fecHorRegIngTmp.setMinutes(fecHorRegIng.getMinutes());
      fecHorRegIngTmp.setSeconds(fecHorRegIng.getSeconds());

      if (fecHorRegIngTmp.getTime() >= horaInicio.getTime() && fecHorRegIngTmp.getTime() <= horaFinal.getTime()) {
        respuesta = true;
      }
    } else {// SALIDA
      java.sql.Date fecRegIng = new java.sql.Date(fecHorRegIng.getYear(), fecHorRegIng.getMonth(),
          fecHorRegIng.getDate());
      java.sql.Date fecRegSal = new java.sql.Date(fecHorRegSal.getYear(), fecHorRegSal.getMonth(),
          fecHorRegSal.getDate());
      Timestamp fecHorRegSalTmp = new Timestamp(18000000);

      fecHorRegSalTmp.setHours(fecHorRegSal.getHours());
      fecHorRegSalTmp.setMinutes(fecHorRegSal.getMinutes());
      fecHorRegSalTmp.setSeconds(fecHorRegSal.getSeconds());

      if (fecRegSal.compareTo(fecRegIng) == 1) {
        fecHorRegSalTmp.setDate(fecHorRegSalTmp.getDate() + 1);
      }

      if (fecHorRegSalTmp.getTime() >= horaInicio.getTime() && fecHorRegSalTmp.getTime() <= horaFinal.getTime()) {
        respuesta = true;
      }
    }

    return respuesta;
  }

  public boolean menorHoraInicio(Time fecHorRegIng, Time fecHorRegSal, Time horaInicio) {
    boolean respuesta = false;

    Timestamp fecHorTemp = new Timestamp(18000000);

    // horTemp.setDate(hora.getDate()+1);
    if (fecHorTemp.compareTo(fecHorRegSal) == 0) {// INGRESO
      Timestamp fecHorRegIngTmp = new Timestamp(18000000);
      fecHorRegIngTmp.setHours(fecHorRegIng.getHours());
      fecHorRegIngTmp.setMinutes(fecHorRegIng.getMinutes());
      fecHorRegIngTmp.setSeconds(fecHorRegIng.getSeconds());

      if (fecHorRegIngTmp.getTime() < horaInicio.getTime()) {
        respuesta = true;
      }
    }

    return respuesta;
  }

  public boolean mayorHoraFinal(Timestamp fecHorRegIng, Timestamp fecHorRegSal, Timestamp horaFinal) {
    boolean respuesta = false;

    Timestamp fecHorTemp = new Timestamp(18000000);

    // horTemp.setDate(hora.getDate()+1);
    if (fecHorTemp.compareTo(fecHorRegSal) != 0) {// INGRESO
      java.sql.Date fecRegIng = new java.sql.Date(fecHorRegIng.getYear(), fecHorRegIng.getMonth(),
          fecHorRegIng.getDate());
      java.sql.Date fecRegSal = new java.sql.Date(fecHorRegSal.getYear(), fecHorRegSal.getMonth(),
          fecHorRegSal.getDate());
      Timestamp fecHorRegSalTmp = new Timestamp(18000000);

      fecHorRegSalTmp.setHours(fecHorRegSal.getHours());
      fecHorRegSalTmp.setMinutes(fecHorRegSal.getMinutes());
      fecHorRegSalTmp.setSeconds(fecHorRegSal.getSeconds());

      if (fecRegSal.compareTo(fecRegIng) == 1) {
        fecHorRegSalTmp.setDate(fecHorRegSalTmp.getDate() + 1);
      }
      if (fecHorRegSalTmp.getTime() > horaFinal.getTime()) {
        respuesta = true;
      }
    }

    return respuesta;
  }

  public Timestamp obtenerHoraAComparar(java.sql.Date fecha, Object hora) {
    Timestamp horaCambio = new Timestamp(18000000);
    if (hora instanceof Timestamp) {
      horaCambio = (Timestamp) hora;
    } else if (hora instanceof Time) {
      horaCambio.setTime(((Time) hora).getTime());
    } else if (hora instanceof String) {
      if (isTimestamp(hora.toString())) {
        horaCambio = fromStrdateToTimestamp(hora.toString());
      } else if (isTime(hora.toString())) {
        horaCambio.setTime(stringToTime(hora.toString()).getTime());
      }
    }

    boolean respuesta = false;
    java.sql.Date fechaCompare = new java.sql.Date(horaCambio.getYear(), horaCambio.getMonth(), horaCambio.getDate());
    Timestamp horTemp = new Timestamp(18000000);
    horTemp.setHours(horaCambio.getHours());
    horTemp.setMinutes(horaCambio.getMinutes());
    horTemp.setSeconds(horaCambio.getSeconds());

    if (fechaCompare.compareTo(fecha) == 1) {
      horTemp.setDate(horTemp.getDate() + 1);
    }
    return horTemp;
  }

  public Date agregarDias(Date fecha, int numDias) {

    if (numDias==0) return fecha;
    
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fecha); 
    calendar.add(Calendar.DAY_OF_YEAR, numDias);  
    return calendar.getTime(); 
    
  }

  public Timestamp agregarDiasTimestamp(java.sql.Date fechaActual, Timestamp fechaHora, int numDias) {

    fechaHora.setYear(fechaActual.getYear());
    fechaHora.setMonth(fechaActual.getMonth());
    fechaHora.setDate(fechaActual.getDate());

    Util.getInst().fechaTemp.setTimeInMillis(fechaHora.getTime());
    Util.getInst().fechaTemp.set(Calendar.DATE, Util.getInst().fechaTemp.get(Calendar.DATE) + numDias);
    fechaHora.setTime(Util.getInst().fechaTemp.getTimeInMillis());

    return fechaHora;
  }

  public Timestamp agregarDiasTimestamp(Timestamp fechaHora, int numDias) {

    Util.getInst().fechaTemp.setTimeInMillis(fechaHora.getTime());
    Util.getInst().fechaTemp.set(Calendar.DATE, Util.getInst().fechaTemp.get(Calendar.DATE) + numDias);
    fechaHora.setTime(Util.getInst().fechaTemp.getTimeInMillis());

    return fechaHora;
  }

  public Timestamp restarDiasTimestamp(Timestamp fechaHora, int numDias) {

    Util.getInst().fechaTemp.setTimeInMillis(fechaHora.getTime());
    Util.getInst().fechaTemp.set(Calendar.DATE, Util.getInst().fechaTemp.get(Calendar.DATE) - numDias);
    fechaHora.setTime(Util.getInst().fechaTemp.getTimeInMillis());

    return fechaHora;
  }

  public Date agregarMes(Date fecha, int numMes) {

    Date fecTmp = new Date(fecha.getYear(), fecha.getMonth(), 1);
    fecTmp.setMonth(fecha.getMonth() + numMes);
    int numDias = obtenerDiasDelMes(fecTmp);
    if (fecha.getDate() >= numDias) {
      fecTmp.setDate(numDias);
    } else {
      fecTmp.setDate(fecha.getDate());
    }

    return fecTmp;
  }

  public List leerArchivoExcel(String nombreArchivo) {
    /**
     * 
     * Crea una nueva instancia de listaFila
     */
    if (nombreArchivo.contains(".xlsx")) {
      return generarXLSX(nombreArchivo);
      // Imprimir_Consola(listaFila, false);
    } else if (nombreArchivo.contains(".xls")) {
      return generarXLS(nombreArchivo);
      // Imprimir_Consola(listaFila, true);
    }
    return null;
  }// public List leerArchivoExcel(String nombreArchivo)

  public Map getWorkBook(Map rqst) {
    try {
      String archivo = (String) rqst.get("archivo");
      XSSFWorkbook libroExcelXLSX = null;
      HSSFWorkbook libroExcelXLS = null;
      String extension = archivo.substring(archivo.indexOf(".XLS"), archivo.length());
      if (extension.toUpperCase().compareTo(".XLSX") == 0) {
        FileInputStream fileInputStream = new FileInputStream(archivo);
        libroExcelXLSX = new XSSFWorkbook(fileInputStream);
      } else if (extension.toUpperCase().compareTo(".XLS") == 0) {
        FileInputStream fileInputStream = new FileInputStream(archivo);
        POIFSFileSystem fsFileSystem = new POIFSFileSystem(fileInputStream);
        libroExcelXLS = new HSSFWorkbook(fsFileSystem);
      }
      rqst.put("libroExcelXLSX", libroExcelXLSX);
      rqst.put("libroExcelXLS", libroExcelXLS);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return rqst;
  }// public Map getWorkBook(Map rqst)

  private List generarXLSX(String nombreArchivo) {
    List listaFila = new ArrayList();
    try {
      /**
       * 
       * Crea una nueva instancia de la clase FileInputStream
       */
      FileInputStream fileInputStream = new FileInputStream(nombreArchivo);
      /**
       * 
       * Crea una nueva instancia de la clase XSSFWorkBook
       */
      XSSFWorkbook libroExcel = new XSSFWorkbook(fileInputStream);
      XSSFSheet hojaExcel = libroExcel.getSheetAt(0);
      /**
       * 
       * Iterar las filas y las celdas de la hoja de c?lculo para obtener
       * 
       * toda la rqst.
       */
      Iterator iteFila = hojaExcel.rowIterator();
      while (iteFila.hasNext()) {
        XSSFRow filExcel = (XSSFRow) iteFila.next();
        Iterator iteColumna = filExcel.cellIterator();
        List listaColumna = new ArrayList();
        while (iteColumna.hasNext()) {
          XSSFCell colExcel = (XSSFCell) iteColumna.next();
          listaColumna.add(colExcel);
        }
        listaFila.add(listaColumna);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return listaFila;
  }// private List generarXLSX(String nombreArchivo)

  private List generarXLS(String nombreArchivo) {
    List listaFila = new ArrayList();
    try {
      /**
       * 
       * Crea una nueva instancia de la clase FileInputStream
       */
      FileInputStream fileInputStream = new FileInputStream(nombreArchivo);
      /**
       * 
       * Crea una nueva instancia de la clase POIFSFileSystem
       */
      POIFSFileSystem fsFileSystem = new POIFSFileSystem(fileInputStream);
      /**
       * 
       * Crea una nueva instancia de la clase HSSFWorkBook
       */
      HSSFWorkbook libroExcel = new HSSFWorkbook(fsFileSystem);
      HSSFSheet hojaExcel = libroExcel.getSheetAt(0);

      /**
       * 
       * Iterar las filas y las celdas de la hoja de c?lculo para obtener
       * 
       * toda la rqst.
       */
      String valorCelda = "";
      Iterator iteFila = hojaExcel.rowIterator();
      listaFila.add("");
      while (iteFila.hasNext()) {
        List listaColumna = new ArrayList();
        HSSFRow filExcel = (HSSFRow) iteFila.next();
        Iterator iteColumna = filExcel.cellIterator();
        listaColumna.add("");
        while (iteColumna.hasNext()) {
          if (nombreArchivo.contains(".xls")) {
            HSSFCell hssfCell = (HSSFCell) iteColumna.next();
            valorCelda = hssfCell.toString();
          } else {
            XSSFCell hssfCell = (XSSFCell) iteColumna.next();
            valorCelda = hssfCell.toString();
          }
          listaColumna.add(valorCelda);
        }
        listaFila.add(listaColumna);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return listaFila;
  }// private List generarXLS(String nombreArchivo)

  public Map validarExcel(Map rqst) {
    try {
      Map msgBox = new LinkedHashMap();
      List lisBrwCol = new ArrayList();

      msgBox.put("type", "success");
      msgBox.put("text", "Inicio operacion!!!");
      rqst.put("msg", msgBox);
      rqst.put("exito", true);

      int numFil = (Integer) rqst.get("numFil");
      int numCol = (Integer) rqst.get("numCol");
      int rowIni = (Integer) rqst.get("rowIni");
      int maxRow = 1000;

      String value = "";
      List worksheet = (List) rqst.get("worksheet");

      for (int ite = 0; ite < (numCol + 1); ite++) {
        lisBrwCol.add(null);
      }
      for (int col = 1; col <= numCol; col++) {
        int colSiz = (int) Double.parseDouble(getValorLista(worksheet, 2, col));
        String colEst = getValorLista(worksheet, 3, col);
        String colTip = getValorLista(worksheet, 4, col);
        String colNom = getValorLista(worksheet, 5, col);
        String colAli = getValorLista(worksheet, 6, col);
        Map lisBrw = new LinkedHashMap();
        if (!isEmpty(colNom) && !isEmpty(colAli)) {
          lisBrw.put("tama", colSiz);
          lisBrw.put("esta", colEst);
          lisBrw.put("nomb", colTip);
          lisBrw.put("alis", colNom);
          lisBrw.put("tipo", colAli);
          lisBrw.put("ssiz", "{width: " + colSiz + "}");
          lisBrw.put("sest", "{value: '" + colEst + "', metadata: {style: headerStyle.id}}");
          lisBrw.put("stip", "{value: '" + colTip + "', metadata: {style: headerStyle.id}}");
          lisBrw.put("snmb", "{value: '" + colNom + "', metadata: {style: headerStyle.id}}");
          lisBrw.put("sali", "{value: '" + colAli + "', metadata: {style: headerStyle.id}}");

          lisBrwCol.set(col, lisBrw);
        } // if( !isEmpty(colNom) && !isEmpty(colAli) )
        else {
          break;
        }
      } // for(int col = 1; col <= numCol; col++)

      maxRow = ((numFil < maxRow) ? numFil : maxRow);

      for (int fil = rowIni; fil <= numFil; fil++) {
        int iteObl = 0;
        int iteVac = 0;
        for (int col = 1; col <= numCol; col++) {
          value = getValorLista(worksheet, fil, col);
          // value =
          // ((strpos($value,"=")==false)?trim($worksheet->getCellByColumnAndRow($col,
          // fil)->getCalculatedValue()):$value);
          iteVac = (isEmpty(value) ? iteVac + 1 : iteVac);
          if (iteVac == numCol) {
            numFil = fil - 1;
            break;
          } // if(iteVac == iteCol)
        } // for(int col = 1; col <= iteCol; col++)
      } // for (int fil = rowIni; fil <= numFil; fil++)

      // continuar

      List wrkLisFil = new ArrayList(numFil + 1);
      List rstDet = new ArrayList(numCol + 1);
      for (int ite = 0; ite < (numFil + 1); ite++) {
        wrkLisFil.add(null);
      }
      for (int ite = 0; ite < (numCol + 1); ite++) {
        rstDet.add(null);
      }
      for (int fil = rowIni; fil <= numFil; fil++) {
        System.out.println("");

        for (int col = 1; col < numCol; col++) {
          value = getValorLista(worksheet, fil, col);
          // value =
          // ((strpos($value,"=")==false)?trim($worksheet->getCellByColumnAndRow($col,
          // fil)->getCalculatedValue()):$value);
          Map lisBrw = (Map) lisBrwCol.get(col);
          String esta = (String) lisBrw.get("esta");
          if (esta.equals("R")) {
            if (isEmpty(value)) {
              msgBox.put("type", "error");
              msgBox.put("text", "Valor requerido fila : " + fil + " columna : " + col);
              rqst.put("exito", false);
              break;
            }
          }
          if (esta.equals("U")) {
            // int idx = existeEnLista((String[]) arrValUni[col], value);
            int[] filCol = buscarLista(wrkLisFil, value);
            if (filCol[0] > -1 && filCol[1] > -1) {
              msgBox.put("type", "error");
              msgBox.put("text", "Valor fila : " + fil + " columna : " + col + " se repite en fila " + filCol[0]
                  + " columna : " + filCol[1]);
              break;
            } else {
              rstDet.set(col, value);
              wrkLisFil.set(fil, rstDet);
            }
          } // if(isset($lisBrwCol[$col]['esta']) &&
            // $lisBrwCol[$col]['esta']=="U")
        } // for($col = 1; $col < 24; $col++)
      } // for (fil = 6; fil <= $numFil; fil++)

      if (msgBox.get("type").equals("error")) {
        System.out.println(msgBox.get("text"));
      }

      // rqst.put("iteCol", iteCol);
      // rqst.put("lisBrwCol", lisBrwCol);
      // rqst.put("numFil", numFil);
      //
      // rqst.put("msg", msgBox);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return rqst;
  }
  // public Map validarFormulario(Map rqst)
  // {
  // Map result=null;
  // try
  // {
  // Map msgBox = new LinkedHashMap();
  //// rqst.put("lisRes") ['lisRes']=Array();
  // msgBox.put("type","success");
  // msgBox.put("text","Los datos son validados correctamente");
  // /*
  // * VALIDAR ITEM UNICO
  // */
  //
  // foreach ($rqst['listaData'] as $keyData=>$rqst)
  // {
  // $lista=$this->entidades->getLista("SELECT", 0, 1,
  // "ent.ndoc='".$rqst['endo']."' or ent.nomb='".$rqst['enom']."'");
  // if(count($lista)==0)
  // {
  // $rqst["msg"]=array("type"=>"warning","text"=>"No se han registrado algunas
  // cuentas");
  // $rqst['lisRes'][$keyData]=$rqst['listaData'][$keyData];
  // $rqst['lisRes'][$keyData]['resp']="Entidad no existe";
  // unset($rqst['listaData'][$keyData]);
  // }
  // else
  // {
  // $rqst['listaData'][$keyData]['id_enti']=$lista[0]->id_enti;
  // $rqst['listaData'][$keyData]['enom']=$lista[0]->nomb;
  // $lisCco=$this->ctacorriente->get_lista("SELECT", 0, 1,
  // "cco.id_enti=".$rqst['listaData'][$keyData]['id_enti']." and
  // cco.banc='".$rqst['listaData'][$keyData]['banc']."' and
  // cco.tdoc='".$rqst['listaData'][$keyData]['tdoc']."' and
  // cco.ndoc='".$rqst['listaData'][$keyData]['ndoc']."'");
  // if(count($lisCco)>0)
  // {
  // $rqst["msg"]=array("type"=>"warning","text"=>"No se han registrado algunas
  // cuentas");
  // $rqst['lisRes'][$keyData]=$rqst['listaData'][$keyData];
  // $rqst['lisRes'][$keyData]['resp']="Cuenta corriente ya existe!!!";
  // unset($rqst['listaData'][$keyData]);
  // }
  // }
  // }//foreach ($rqst['rqst']['items'] as $col=>$item)
  // $rqst['listaData'] = $this->util->multiArrayUnique($rqst['listaData'],
  // $rqst['lisBrwCol']);
  // $rqst['lisRes'] = $this->util->multiArrayUnique($rqst['lisRes'],
  // $rqst['lisBrwCol']);
  // $result=$rqst;
  // }
  // catch (Exception $e)
  // {
  // throw $e;
  // }
  // return $result;
  // }

  public boolean validarPalabraClave(String clave) {
    boolean encontrado = false;
    try {
      List lisPalCla = new ArrayList<String>();
      lisPalCla.add("MODELO");
      lisPalCla.add("ESPECIFICACION");
      lisPalCla.add("DESCRIPCION");
      lisPalCla.add("GAUGE");
      lisPalCla.add("PRODUCTO");
      lisPalCla.add("DESC");
      lisPalCla.add("TALLA");
      lisPalCla.add("CANTIDAD");

      for (int ite = 0; ite < lisPalCla.size(); ite++) {
        String valor = (String) lisPalCla.get(ite);
        if (valor.toUpperCase().equalsIgnoreCase(clave)) {
          encontrado = true;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return encontrado;
  }// public boolean validarPalabraClave(Map rqst)

  public String getValorLibroExcel(Map rqst, int fil, int col, int idxTipo) {
    Object result = "";
    try {
      String[] arrTipo = { "STRING", "DATE", "NUMERIC", "INTEGER", "DEFAULT" };
      XSSFWorkbook libroExcelXLSX = (XSSFWorkbook) rqst.get("libroExcelXLSX");
      HSSFWorkbook libroExcelXLS = (HSSFWorkbook) rqst.get("libroExcelXLS");
      if (libroExcelXLSX != null) {
        XSSFSheet hojaExcelXLSX = libroExcelXLSX.getSheetAt(0);
        if (arrTipo[idxTipo].equals("STRING")) {
          result = hojaExcelXLSX.getRow(fil).getCell(col).getStringCellValue();
        } else if (arrTipo[idxTipo].equals("DATE")) {
          result = hojaExcelXLSX.getRow(fil).getCell(col).getDateCellValue();
          result = fromDateToString((Date) result, FechaFormato.ddMMyyyy);
        } else if (arrTipo[idxTipo].equals("NUMERIC")) {
          result = hojaExcelXLSX.getRow(fil).getCell(col).getNumericCellValue();
        } else if (arrTipo[idxTipo].equals("INTEGER")) {
          result = (int) hojaExcelXLSX.getRow(fil).getCell(col).getNumericCellValue();
        } else if (arrTipo[idxTipo].equals("DEFAULT")) {
          result = "";
        }
      } else if (libroExcelXLS != null) {
        HSSFSheet hojaExcelXLS = libroExcelXLS.getSheetAt(0);
        if (arrTipo[idxTipo].equals("STRING")) {
          result = hojaExcelXLS.getRow(fil).getCell(col).getStringCellValue();
        } else if (arrTipo[idxTipo].equals("DATE")) {
          result = hojaExcelXLS.getRow(fil).getCell(col).getDateCellValue();
          result = fromDateToString((Date) result, FechaFormato.ddMMyyyy);
        } else if (arrTipo[idxTipo].equals("NUMERIC")) {
          result = hojaExcelXLS.getRow(fil).getCell(col).getNumericCellValue();
        } else if (arrTipo[idxTipo].equals("INTEGER")) {
          result = (int) hojaExcelXLS.getRow(fil).getCell(col).getNumericCellValue();
        } else if (arrTipo[idxTipo].equals("DEFAULT")) {
          result = "";
        }
      }
    } catch (Exception e) {
      return getValorLibroExcel(rqst, fil, col, idxTipo + 1);
    }
    return result.toString();
  }

  public String getValorExcel(Map rqst, int fil, int col) {
    String valor = "";
    try {
      XSSFWorkbook libroExcelXLSX = (XSSFWorkbook) rqst.get("libroExcelXLSX");
      HSSFWorkbook libroExcelXLS = (HSSFWorkbook) rqst.get("libroExcelXLS");
      if (libroExcelXLSX != null) {
        XSSFSheet hojaExcelXLSX = libroExcelXLSX.getSheetAt(0);
        Iterator Iterador_de_Fila = hojaExcelXLSX.rowIterator();

        while (Iterador_de_Fila.hasNext()) {
          XSSFRow Fila_hssf = (XSSFRow) Iterador_de_Fila.next();
          if (Fila_hssf.getRowNum() == fil) {
            Iterator iterador = Fila_hssf.cellIterator();
            List Lista_celda_temporal = new ArrayList();
            while (iterador.hasNext()) {
              XSSFCell Celda_hssf = (XSSFCell) iterador.next();
              if (Celda_hssf.getColumnIndex() == col) {
                valor = Celda_hssf.toString();
              } // if(Celda_hssf.getColumnIndex() == col)
            }
          } // if(Fila_hssf.getRowNum() == fil)
        } // while (Iterador_de_Fila.hasNext())
      } // if(libroExcelXLSX!=null)
      else if (libroExcelXLS != null) {
        HSSFSheet hojaExcelXLS = libroExcelXLS.getSheetAt(0);
        Iterator Iterador_de_Fila = hojaExcelXLS.rowIterator();
        while (Iterador_de_Fila.hasNext()) {
          HSSFRow Fila_hssf = (HSSFRow) Iterador_de_Fila.next();
          if (Fila_hssf.getRowNum() == fil) {
            Iterator iterador = Fila_hssf.cellIterator();
            List Lista_celda_temporal = new ArrayList();
            while (iterador.hasNext()) {
              HSSFCell Celda_hssf = (HSSFCell) iterador.next();
              if (Celda_hssf.getColumnIndex() == col) {
                valor = Celda_hssf.toString();
              } // if(Celda_hssf.getColumnIndex() == col)
            } // while (iterador.hasNext())
          } // if(Fila_hssf.getRowNum() == fil)
        } // while (Iterador_de_Fila.hasNext())
      } // else if(libroExcelXLS!=null)
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (valor.length() >= 2) {
      // String test1 = valor.substring(valor.length()-2, valor.length());
      // String test2 = valor.substring(0, valor.length()-2);
      valor = ((valor.substring(valor.length() - 2, valor.length()).equals(".0"))
          ? valor.substring(0, valor.length() - 2) : valor);
    }
    valor = Util.getInst().removerCaracterEspecial(valor);
    valor = valor.replace(";", "");
    valor = valor.replace(":", "");
    valor = valor.replace(".", "");
    valor = valor.trim();
    return valor;
  }

  public Object setExcelEntidad(Map rsqt, Object objeto) {

    Class clase = objeto.getClass();
    Field[] lisField = clase.getDeclaredFields();
    for (Field iteField : lisField) {

      String fieldName = iteField.getName();
      String methodNameSet = "set" + fieldName.substring(0, 1).toUpperCase()
          + fieldName.substring(1, fieldName.length());
      Field field;
      try {
        if (rsqt.containsKey(fieldName)) {
          field = clase.getDeclaredField(fieldName);
          if (field.getType().equals(String.class)) {
            clase.getMethod(methodNameSet, String.class).invoke(objeto, rsqt.get(fieldName));
          }
          if (field.getType().equals(int.class)) {
            clase.getMethod(methodNameSet, int.class).invoke(objeto,
                (int) Double.parseDouble((String) rsqt.get(fieldName)));
          }
          if (field.getType().equals(double.class)) {
            clase.getMethod(methodNameSet, double.class).invoke(objeto,
                Double.parseDouble((String) rsqt.get(fieldName)));
          }
          if (field.getType().equals(boolean.class)) {
            clase.getMethod(methodNameSet, boolean.class).invoke(objeto,
                Boolean.parseBoolean((String) rsqt.get(fieldName)));
          }
          if (field.getType().equals(Date.class)) {
            clase.getMethod(methodNameSet, Date.class).invoke(objeto,
                fromStrdateToDate((String) rsqt.get(fieldName), "dd/mm/yyyy"));
          }
          if (field.getType().equals(java.sql.Date.class)) {
            clase.getMethod(methodNameSet, java.sql.Date.class).invoke(objeto, fromStrdateToDate((String) rsqt.get(fieldName), "dd/mm/yyyy"));
          }
        } // if(rsqt.containsKey(fieldName))
      } catch (SecurityException e) {
        e.printStackTrace();
      } catch (NoSuchFieldException e) {
        e.printStackTrace();
      } catch (IllegalArgumentException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
    return objeto;
  }// Fin de limpiarInstancia()
  /*
   * public Map guardarLibroExcel(Map rqst) { try { int numFil = (Integer)
   * rqst.get("numFil"); int numCol = (Integer) rqst.get("numCol"); int rowIni =
   * (Integer) rqst.get("rowIni"); List wrkLisFil = (List)
   * rqst.get("wrkLisFil"); String value = "";
   * 
   * Kardex kar = new Kardex();
   * 
   * Local locOri = new Local(); Persona per = new Persona();
   * 
   * Map rstKar = (Map)wrkLisFil.get(rowIni);
   * 
   * kar = (Kardex) setExcelEntidad(rstKar, kar); locOri = (Local)
   * setExcelEntidad(rstKar, locOri); per = (Persona) setExcelEntidad(rstKar,
   * per);
   * 
   * kar.setKar_fectras(kar.getKar_fecemi());
   * kar.setKar_numcom(kar.getKar_guia());
   * 
   * List<Producto> lisArtXls = new ArrayList<Articulo>(); // List<DetKardex>
   * lisDetkar = new ArrayList<DetKardex>();
   * 
   * for (int fil = rowIni; fil <= numFil; fil++) { Map rstArt =
   * (Map)wrkLisFil.get(fil);
   * 
   * if(rstArt!=null) {
   */

  /**********************************
   * Obtener Articulo y Detarticulo
   **********************************/
  /*
   * Articulo prd = new Articulo(); prd = (Articulo) setExcelEntidad(rstArt,
   * art); List<DetArticulo> lisDetart = new ArrayList<DetArticulo>();
   * 
   * // DetKardex detKar = new DetKardex(); // detKar = (DetKardex)
   * setExcelEntidad(rstArt, detKar); // List<SubDetKardex> lisSubdetkar = new
   * ArrayList<SubDetKardex>();
   * 
   * List wrkLisTal = (List) rstArt.get("wrkLisTal"); for (int tal = 0; tal <
   * wrkLisTal.size(); tal++) { Map rstDetart = (Map) wrkLisTal.get(tal);
   * if(rstDetart!=null) { DetArticulo detArt = new DetArticulo(); detArt =
   * (DetArticulo) setExcelEntidad(rstArt, detArt);
   * detArt.setDetart_nom((String)rstDetart.get("detart_nom"));
   * detArt.setDetart_serie((String)rstDetart.get("detart_serie"));
   * detArt.setDetart_cant((int)Double.parseDouble((String)rstDetart.get(
   * "detart_cant"))); lisDetart.add(detArt); // SubDetKardex subDetkar = new
   * SubDetKardex(); // subDetkar.setArt_cod(art.getArt_cod()); //
   * subDetkar.setDetart_cod(detArt.getDetart_cod()); //
   * subDetkar.setKar_cod(kar.getKar_cod()); //
   * subDetkar.setSubdetkar_nom((String)rstDetart.get("detart_nom")); //
   * subDetkar.setSubdetkar_can((int)Double.parseDouble((String)rstDetart.get(
   * "detart_cant"))); // lisSubdetkar.add(subDetkar); }//if(rstDetart!=null)
   * }//for (fil = 6; fil <= $numFil; fil++) art.setLisDetart(lisDetart);
   * lisArtXls.add(art);
   * 
   * // detKar.setLisSubdetkar(lisSubdetkar); // lisDetkar.add(detKar);
   * }//if(rstArt!=null) }//for (fil = 6; fil <= $numFil; fil++)
   * 
   * // kar.setLisDetkar(lisDetkar);
   * 
   * rqst.put("per", per); rqst.put("kar", kar); rqst.put("locOri", locOri);
   * 
   * rqst.put("lisArtXls", lisArtXls); } catch (Exception e) {
   * e.printStackTrace(); } return rqst; }
   */
  public int[] buscarLista(List listaFila, String valor) {
    String rptaValor = "";
    int[] rpta = { -1, -1 };
    finalizarBucles: for (int iteFil = 0; iteFil < listaFila.size(); iteFil++) {
      if (listaFila.get(iteFil) instanceof List) {
        List listaColumna = (List) listaFila.get(iteFil);

        for (int iteCol = 0; iteCol < listaColumna.size(); iteCol++) {
          if (listaColumna.get(iteCol) instanceof String) {
            rptaValor = (String) listaColumna.get(iteCol);
            if (rptaValor.equals(valor)) {
              rpta[0] = iteFil;
              rpta[1] = iteCol;
              break finalizarBucles;
            }
          }
        } // for (int iteCol = 0; iteCol <= colHoja.size(); iteCol++)
      } // if(listaFila.get(iteFil) instanceof List)
    } // for (int iteFil = 0; iteFil <= listaFila.size(); iteFil++)
    return rpta;
  }// public int[] buscarLista(List listaFila, String valor)

  public String getValorLista(List filHoja, int fil, int col) {
    String rpta = "";
    for (int iteFil = 0; iteFil < filHoja.size(); iteFil++) {
      if (filHoja.get(iteFil) instanceof List) {
        List colHoja = (List) filHoja.get(iteFil);

        for (int iteCol = 0; iteCol < colHoja.size(); iteCol++) {
          if (colHoja.get(iteCol) instanceof List) {

          }
          if (fil == iteFil && col == iteCol) {
            rpta = (String) colHoja.get(iteCol);
          } // if(fil == iteFil && col == iteCol)
        } // for (int iteCol = 0; iteCol <= colHoja.size(); iteCol++)
      } // if(filHoja.get(iteFil) instanceof List)
    } // for (int iteFil = 0; iteFil <= filHoja.size(); iteFil++)
    return rpta;
  }

  public static String removerCaracterEspecial(String input) {
    // Cadena de caracteres original a sustituir.
    String original = "??????????????u???????????????????";
    // Cadena de caracteres ASCII que reemplazar?n los originales.
    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
    String output = input;
    for (int i = 0; i < original.length(); i++) {
      // Reemplazamos los caracteres especiales.
      output = output.replace(original.charAt(i), ascii.charAt(i));
    } // for i
    return output;
  }// remove1

  public static Date obtenerFechaInternet() {
    String servidor = "0.north-america.pool.ntp.org";
    Date fechaRecibida = null;
    NTPUDPClient cliente = new NTPUDPClient();
    cliente.setDefaultTimeout(5000);
    try {
      InetAddress hostAddr = InetAddress.getByName(servidor);
      TimeInfo fecha = cliente.getTime(hostAddr);
      fechaRecibida = new Date(fecha.getMessage().getTransmitTimeStamp().getTime());
    } catch (Exception e) {
      System.err.println("Error " + e.getMessage());
    }
    cliente.close();
    return fechaRecibida;
  }

  public String get(Map data, String keyBus, String defecto) throws IOException {
    try {
      String result = defecto;

      // if(isset($data) && is_array($data) && count($data) > 0)
      {
        Set keys = data.keySet();
        if (keys != null && keys.size() > 0) {
          Iterator iterator = keys.iterator();
          while (iterator.hasNext()) {
            String key = (String) iterator.next();
            if (key.contains(keyBus)) {
              result = (String) data.get(keyBus);
              break;
            }
          }
        }
      } // if(count($data) > 0)
      return result;
    } catch (Exception e) {
      throw new IOException("Error al obtener valor de data");
    }
  }

  public static StringBuffer getFileContent(String filePath) {

    StringBuffer resp = new StringBuffer("");
    try {
      URL url = new URL(filePath);
      URLConnection connection = url.openConnection();
      int length = connection.getContentLength();
      InputStreamReader in = new InputStreamReader(connection.getInputStream());
      StringWriter out = new StringWriter(length);
      resp.append(out.toString());

    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return resp;
  }
}// Finde claseFuente principal