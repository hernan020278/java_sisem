package com.comun.utilidad;

import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingWorker;

/**
 * Clase SwingWorker para realizar todas las
 * cargas necesarias antes de mostrar la pantalla principal
 */
public class SwingWorkerJava extends SwingWorker<Boolean, Void> {

	public Map requestParameters = new HashMap();
	private String mensajeProceso = "";

	public SwingWorkerJava(Object object) {
		requestParameters = (Map) object;
	}

	@Override
	protected Boolean doInBackground() throws Exception {

//		ManejadorPadre man = (Control) requestParameters.get("manejador");
//		man.iniciarAccion(requestParameters);
//		this.setProgress(100);
		return new Boolean(true);
	}
}