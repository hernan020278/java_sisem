package com.comun.utilidad;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class MyArchivoFiltro extends FileFilter {

    private String[] lisExt;
    private String desExt = "";

    public MyArchivoFiltro(String[] parLisExt) {

        lisExt = parLisExt;
        for (int ite = 0; ite < lisExt.length; ite++) {

            desExt = desExt + "*." + lisExt[ite];
            desExt = desExt + ((ite != (lisExt.length - 1)) ? "," : "");

        }//Fin de for (int ite = 0; ite < lisExt.length; ite++)

    }//Fin de MyArchivoFiltro()

    @Override
    public boolean accept(File myFile) {

        if (myFile.isDirectory()) {
            return true;
        }//Fin de Bloque True de si es un directorio

        String nameFile = myFile.getName();
        int indexLetter = nameFile.lastIndexOf('.');
        if (indexLetter > 0 && indexLetter < nameFile.length() - 1) {
            for (int ite = 0; ite < lisExt.length; ite++) {
                if (nameFile.substring(indexLetter + 1).toLowerCase().equals(lisExt[ite].toLowerCase())) {

                    return true;

                }//Fin de if (nameFile.substring(indexLetter + 1).toLowerCase().equals(lisExt[ite].toLowerCase())) Compara con la lista de extensiones
            }//Fin de for (int ite = 0; ite < lisExt.length; ite++) 
        }//Fin de 
        return false;
    }//Fin de Metodo accpet

    @Override
    public String getDescription() {

        return "Archivos " + desExt;

    }//Fin de metodo getDescription
}//Fin de Metodo accept
