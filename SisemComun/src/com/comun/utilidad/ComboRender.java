package com.comun.utilidad;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;

public class ComboRender extends JLabel implements ListCellRenderer {

    public ComboRender() {
        setOpaque(true);
        setBorder(new EmptyBorder(1, 1, 1, 1));
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (isSelected) {
            
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
            
        } else {
            
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            
        }
        
        setFont(list.getFont());
        setText((value == null) ? "" : value.toString());
        return this;
        
    }
}