/**
 *
 */
package com.comun.utilidad;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.DiccionarioGeneral;

/**
 * @author renzo
 * 
 */
public class AplicacionGeneral
{

	private static AplicacionGeneral instance = null;
	private static String orgIde = OrganizacionGeneral.getOrgcodigo();

	public static AplicacionGeneral getInstance()
	{

		if(AplicacionGeneral.instance == null)
		{
			AplicacionGeneral.instance = new AplicacionGeneral();
		}
		return AplicacionGeneral.instance;
	}

	public static AplicacionGeneral getInstance(String parOrgIde) {

		orgIde = parOrgIde;
		return getInstance();
	}

	public String obtenerRutaSistema() {

		return obtenerRuta("ruta-sistema", "");
	}

	public String obtenerRutaPropiedad() {

		return obtenerRuta("ruta-propiedades", this.orgIde);
	}
	public String obtenerRutaImagenes() {

		return obtenerRuta("ruta-imagenes", this.orgIde);
	}

	public String obtenerRutaSonidos() {

		return obtenerRuta("ruta-sonidos", this.orgIde);
	}
	public String obtenerRutaPreviewReporte() {

		return obtenerRuta("ruta-reportpreview", this.orgIde);
	}

	public String obtenerRutaControl() {

		return obtenerRuta("ruta-control", this.orgIde);
	}

	public String obtenerRutaConfig() {

		return obtenerRuta("ruta-config", this.orgIde);
	}
	public String obtenerRutaBackup() {

		return obtenerRuta("ruta-backup", this.orgIde);
	}

	public String obtenerRuta(String etiqueta, String orgIde) {

		String ruta = "";
		ruta = DiccionarioGeneral.getInstance("host", "").getPropiedad(etiqueta, "");
		if(!Util.esVacio(orgIde) && !orgIde.equalsIgnoreCase("SISEM")) {
			ruta = ruta + orgIde + File.separator;
		}
		return ruta;
	}

	public String obtenerEtiqueta(String etiqueta, String defecto)
	{

		return DiccionarioGeneral.getInstance("etiqueta", this.orgIde).getPropiedad(etiqueta, defecto);
	}

	public String obtenerConfig(String etiqueta, String defecto)
	{

		return DiccionarioGeneral.getInstance("config", this.orgIde).getPropiedad(etiqueta, defecto);
	}

	public String obtenerRutaProcesoXml(String organizacionIde) {

		return AplicacionGeneral.getInstance().obtenerRutaXml() + "proceso" + File.separator + organizacionIde + File.separator;
	}

	public String obtenerRutaProcesoXml() {

		return AplicacionGeneral.getInstance().obtenerRutaXml() + "proceso" + File.separator;
	}

	public String obtenerRutaBrowseXml(String organizacionIde) {

		return AplicacionGeneral.getInstance().obtenerRutaXml() + "browse" + File.separator + organizacionIde + File.separator;
	}

	public String obtenerRutaBrowseXml() {

		return AplicacionGeneral.getInstance().obtenerRutaXml() + "browse" + File.separator;
	}

	public String obtenerRutaReporteXml(String organizacionIde) {

		return AplicacionGeneral.getInstance().obtenerRutaXml() + "reporte" + File.separator + organizacionIde + File.separator;
	}

	public String obtenerRutaReporteXml() {

		return AplicacionGeneral.getInstance().obtenerRutaXml() + "reporte" + File.separator;
	}

	public String obtenerRutaScheduleXml() {

		return AplicacionGeneral.getInstance().obtenerRutaXml() + "schedule" + File.separator;
	}

	public String obtenerRutaReporteJasper(String organizacionIde) {

		return obtenerRuta("ruta-reporte-jasper", organizacionIde);
	}

	public String obtenerRutaReporteJasper() {

		return obtenerRuta("ruta-reporte-jasper", this.orgIde);
	}
	public static String obtenerRutaSalidaReporte()
	{

		String reportsOutPath = "";
		try {
			boolean validPath = false;
			reportsOutPath = AplicacionGeneral.getInstance().obtenerRutaSistema() + "reportes" + File.separator + "out";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportsOutPath;
	}

	public String obtenerRutaXml() {

		String xmlPath = "";
		try {
			xmlPath = DiccionarioGeneral.getInstance("host", "").getPropiedad("xml-path", "");
			if(xmlPath == null) {
				// xmlPath = AplicacionGeneral.getInstance().getWebAppPath() + "WEB-INF" + File.separator + "xml" + File.separator;
			}
		} catch (Exception e) {
			Log.error(this, e);
		}
		if(!Util.esVacio(xmlPath) && !xmlPath.endsWith(File.separator)) {
			xmlPath += File.separator;
		}
		return xmlPath;
	}

	public ImageIcon obtenerImagen(String nombreImagen) 
	{
		return Util.getInst().obtenerImagenIconArchivo(obtenerRutaImagenes() + nombreImagen);
	}
	
	public BufferedImage obtenerBufferImagen(String nombreImagen) 
	{
		return Util.getInst().obtenerBufferedImageArchivo(obtenerRutaImagenes() + nombreImagen);
	}
	public static void main(String[] args)
	{

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				System.out.println(AplicacionGeneral.getInstance().obtenerRutaReporteXml());
//				if(java.awt.Desktop.isDesktopSupported()) {
//					try {
//						Desktop dk = Desktop.getDesktop();
//						dk.browse(new URI("http://www.google.com"));
//					}
//					catch (Exception e) {
//						System.out.println("Error al abrir URL: " + e.getMessage());
//					}
//				}
			}
		});
	}
}
