/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comun.utilidad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

/**
 *
 * @author BASVALGRAFICA
 */
public class PropiedadesExterna {

    private Properties props;
    private String propiedadExterna = "C:\\sisem\\propiedades\\ampato\\Propiedades.properties";

    public PropiedadesExterna() {

        props = new Properties();

    }//Fin de metodo constructor

    public PropiedadesExterna(String archivo) {
    	propiedadExterna = AplicacionGeneral.getInstance().obtenerRutaConfig() + archivo + ".properties";
        props = new Properties();

    }//Fin de metodo constructor
    public Properties getPropiedades() {
        try {
            //se leen el archivo .properties
        	FileInputStream filInpStr = new FileInputStream(propiedadExterna);
            props.load(filInpStr);
            //si el archivo de props NO esta vacio retornan las propiedes leidas
            if (!props.isEmpty()) {
                return props;
            } else {//sino  retornara NULL
                return null;
            }
        } catch (IOException ex) {
            return null;
        }
    }//Fin de metodo para obtener la lista de props

    public String getServidor(String parKey) {
        try {
            
        	FileInputStream filInpStr = new FileInputStream(propiedadExterna);
			props.load(filInpStr);
			String valor = props.getProperty(parKey);
			return (valor != null) ? valor : "";
        } catch (IOException ex) {
            Logger.getLogger(PropiedadesExterna.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public void setPropiedades(String parClave, String parValor) {
        try {
        	props = this.getPropiedades();
            //se leen el archivo .properties
            File file = new File(propiedadExterna);
            for (Enumeration enuProp = props.propertyNames(); enuProp.hasMoreElements();) {
                // Obtenemos el objeto
                Object objPropNom = enuProp.nextElement();
                props.setProperty(objPropNom.toString(), props.getProperty(objPropNom.toString()));

            }
            props.setProperty(parClave, parValor);
            //Actualiza el archivo
            props.store(new FileOutputStream(file), null);

        } catch (IOException ex) {
            Logger.getLogger(PropiedadesExterna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void eliminarPropieda(String parClave) {
        try {
            //se leen el archivo .properties
            URL urlClassPath = getClass().getResource(propiedadExterna);
            File file = new File(urlClassPath.toURI());
            //Actualiza el archivo
            props.remove(parClave);
            props.store(new FileOutputStream(file), null);

        } catch (IOException ex) {
            Logger.getLogger(PropiedadesExterna.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(PropiedadesExterna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void refrescarClave(List<String> parLis) {

        try {
            //se leen el archivo .properties
            URL urlClassPath = getClass().getResource(propiedadExterna);
            File file = new File(urlClassPath.toURI());
            String[] regLis;
            props.clear();
            for (int ite = 0; ite < parLis.size(); ite++) {

                regLis = parLis.get(ite).split("#");
                props.setProperty("FRMPAG" + (ite + 1), regLis[1]);
                System.out.println("FRMPAG" + (ite + 1) + " Val : " + regLis[1]);

            }
            //Actualiza el archivo
            props.store(new FileOutputStream(file), null);

        } catch (IOException ex) {
            Logger.getLogger(PropiedadesExterna.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(PropiedadesExterna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listarPropiedades() {
        try {
            //se leen el archivo .properties
            props.load(getClass().getResourceAsStream(propiedadExterna));
            for (Enumeration enuProp = props.propertyNames(); enuProp.hasMoreElements();) {
                // Obtenemos el objeto
                Object objPropNom = enuProp.nextElement();
                System.out.println("clave : " + objPropNom.toString() + " valor : " + props.getProperty(objPropNom.toString()));
            }
        } catch (IOException ex) {
            Logger.getLogger(PropiedadesExterna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                PropiedadesExterna testProp = new PropiedadesExterna("config");
                testProp.setPropiedades("sistema-restriccion", KeyGenerator.getInst().encriptar("02/02/1978", "020278"));
            }
        });
    }
}
