package com.comun.utilidad;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.print.PrinterJob;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;

public class CodigoBarras {

	public JPanel componenteSwing() {
		JPanel panel = new JPanel();

		// Always get a Barcode from the BarcodeFactory
		Barcode barcode = null;
		try {
			barcode = BarcodeFactory.createCode128B("My Barcode");
		} catch (BarcodeException e) {
			// Error handling
		}

		/*
		 * Because Barcode extends Component, you can use it just like any other
		 * Swing component. In this case, we can add it straight into a panel
		 * and it will be drawn and layed out according to the layout of the panel.
		 */
		panel.add(barcode);
		return panel;
	}

	public void drawingBarcodeDirectToGraphics() throws BarcodeException, OutputException {
		// Always get a Barcode from the BarcodeFactory
		Barcode barcode = BarcodeFactory.createCode128B("My Barcode");

		// We are created an image from scratch here, but for printing in Java, your
		// print renderer should have a Graphics internally anyway
		BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_BYTE_GRAY);
		// We need to cast the Graphics from the Image to a Graphics2D - this is OK
		Graphics2D g = (Graphics2D) image.getGraphics();

		// Barcode supports a direct draw method to Graphics2D that lets you position the
		// barcode on the canvas
		barcode.draw(g, 10, 56);

	}

	public ImageIcon obtenerImageIcon(String parCodigo) {

		Image image = null;
		ImageIcon imageIcon = null;
		// Always get a Barcode from the BarcodeFactory
		try {
			if (parCodigo != null && !parCodigo.equals("") && parCodigo.length() == 12 && Util.getInst().isLong(parCodigo)) {

				Barcode barcode = BarcodeFactory.createEAN13(parCodigo);
				// We are created an image from scratch here, but for printing in Java, your
				// print renderer should have a Graphics internally anyway
				barcode.setDrawingText(true);  
//				barcode.setBarHeight(50);  
//				barcode.setBarWidth(56);  
				image = BarcodeImageHandler.getImage(barcode);
				// TODO Auto-generated catch block
				imageIcon = new ImageIcon(image);

			}

			return imageIcon;

		} catch (OutputException e) {
			e.printStackTrace();
		} catch (BarcodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public Image obtenerImage(String parCodigo) {

		Image image = null;
		// Always get a Barcode from the BarcodeFactory
		try {
			if (parCodigo != null & !parCodigo.equals("")) {

				Barcode barcode = BarcodeFactory.createCode128B(parCodigo);
				// We are created an image from scratch here, but for printing in Java, your
				// print renderer should have a Graphics internally anyway
				image = BarcodeImageHandler.getImage(barcode);

			}

			return image;

		} catch (OutputException e) {
			e.printStackTrace();
		} catch (BarcodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public void crearArchivoPng() throws BarcodeException {
		// get a Barcode from the BarcodeFactory
		Barcode barcode = BarcodeFactory.createCode128B("My Barcode");

		try {
			File f = new File("mybarcode.png");

			// Let the barcode image handler do the hard work
			BarcodeImageHandler.savePNG(barcode, f);
		} catch (Exception e) {
			// Error handling here
		}
	}

	public void imprimirCodigoBarras() {
		try
		{
			Barcode b = BarcodeFactory.createCode128("Hello");
			PrinterJob job = PrinterJob.getPrinterJob();
			job.setPrintable(b);
			if (job.printDialog())
			{
				job.print();
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}// Fin de imprimircodigoBarras

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {

				// JFrame frame = new JFrame("Prueba de Codigo de Barras");
				// CodigoBarras example = new CodigoBarras();
				// JPanel panel = example.componenteSwing();
				//
				// frame.add(panel);
				// frame.setDefaultCloseOperation(1);
				// frame.setVisible(true);

				CodigoBarras example = new CodigoBarras();
				try {
					example.drawingBarcodeDirectToGraphics();
				} catch (BarcodeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OutputException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
