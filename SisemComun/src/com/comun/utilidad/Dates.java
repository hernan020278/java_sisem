package com.comun.utilidad;

import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * @author JEFF
 */
public class Dates {
	private int month, day, year;
	private int j, y, d, m;
	private String s_date;
	public java.sql.Date dates = null;

	/** Creates new date **/
	public Dates() {
	}

	public static java.sql.Date getDate(String sDate, String sDateFormat)
	{
		java.sql.Date sqlDate = null;
		try
		{
			Calendar calendar = Calendar.getInstance();
			if (!Util.isEmpty(sDate))
			{

				String[] dateFormateArray;
				String[] dateArray;
				int yearInd = 0;
				int monthInd = 0;
				int dayInd = 0;
				String strMonth = "";
				String strYear = "";
				String strDay = "";

				String[] strSplitter = { "-", " ", "/", "." };
				for (int i = 0; i < strSplitter.length; i++)
				{
					if (sDateFormat.indexOf(strSplitter[i]) != -1)
					{
						dateFormateArray = sDateFormat.split(strSplitter[i]);
						for (int ix = 0; ix < dateFormateArray.length; ix++)
						{
							if (dateFormateArray[ix].equalsIgnoreCase("MM")) {
								monthInd = ix;
							} else if (dateFormateArray[ix].equalsIgnoreCase("dd")) {
								dayInd = ix;
							} else if (dateFormateArray[ix].equalsIgnoreCase("yyyy")) {
								yearInd = ix;
							} else if (dateFormateArray[ix].equalsIgnoreCase("yy")) {
								yearInd = ix;
							}
						}
						break;
					}
				}

				for (int ia = 0; ia < strSplitter.length; ia++)
				{
					if (sDate.indexOf(strSplitter[ia]) != -1) {
						dateArray = sDate.split(strSplitter[ia]);
						strMonth = dateArray[monthInd];
						strDay = dateArray[dayInd];
						strYear = dateArray[yearInd];
					}
				}

				if (strDay.length() == 1)
					strDay = "0" + strDay;
				if (strMonth.length() == 1)
					strMonth = "0" + strMonth;
				if (strYear.length() == 2)
					strYear = "20" + strYear;

				int year = Integer.valueOf(strYear);
				int month = Integer.valueOf(strMonth);
				int day = Integer.valueOf(strDay);

				calendar.set(year, (month - 1), day);
			}
			sqlDate = new java.sql.Date(calendar.getTimeInMillis());
			System.out.println("date = " + sqlDate.toString());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return sqlDate;
	}

	public static java.sql.Date getDateWithoutHour(String sDate, String sDateFormat)
	{
		java.sql.Date sqlDate = null;
		try
		{
			Calendar calendar = Calendar.getInstance();
			if (!Util.isEmpty(sDate))
			{

				String[] dateFormateArray;
				String[] dateArray;
				int yearInd = 0;
				int monthInd = 0;
				int dayInd = 0;
				String strMonth = "";
				String strYear = "";
				String strDay = "";

				String[] strSplitter = { "-", " ", "/", "." };
				for (int i = 0; i < strSplitter.length; i++)
				{
					if (sDateFormat.indexOf(strSplitter[i]) != -1)
					{
						dateFormateArray = sDateFormat.split(strSplitter[i]);
						for (int ix = 0; ix < dateFormateArray.length; ix++)
						{
							if (dateFormateArray[ix].equalsIgnoreCase("MM")) {
								monthInd = ix;
							} else if (dateFormateArray[ix].equalsIgnoreCase("dd")) {
								dayInd = ix;
							} else if (dateFormateArray[ix].equalsIgnoreCase("yyyy")) {
								yearInd = ix;
							} else if (dateFormateArray[ix].equalsIgnoreCase("yy")) {
								yearInd = ix;
							}
						}
						break;
					}
				}

				for (int ia = 0; ia < strSplitter.length; ia++)
				{
					if (sDate.indexOf(strSplitter[ia]) != -1) {
						dateArray = sDate.split(strSplitter[ia]);
						strMonth = dateArray[monthInd];
						strDay = dateArray[dayInd];
						strYear = dateArray[yearInd];
					}
				}

				if (strDay.length() == 1)
					strDay = "0" + strDay;
				if (strMonth.length() == 1)
					strMonth = "0" + strMonth;
				if (strYear.length() == 2)
					strYear = "20" + strYear;

				int year = Integer.valueOf(strYear);
				int month = Integer.valueOf(strMonth);
				int day = Integer.valueOf(strDay);

				calendar.set(year, (month - 1), day);
			}
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);

			sqlDate = new java.sql.Date(calendar.getTimeInMillis());
			System.out.println("date = " + sqlDate.toString());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return sqlDate;
	}

	public static java.sql.Date getDate(String sDate)
	{
		java.sql.Date sqlDate = null;
		try
		{
			Date date = new Date();
			if (!Util.isEmpty(sDate))
			{
				sDate = sDate.replaceAll("-", "/").trim();
				int p = sDate.indexOf(" ");
				if (p >= 0) {
					sDate = sDate.substring(0, p);
				}
				date = new Date(sDate);
			}
			sqlDate = new java.sql.Date(date.getTime());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return sqlDate;
	}

	public static java.sql.Date getDateTime(String sDate) {
		java.sql.Date sqlDate = null;
		try
		{
			Date date = Calendar.getInstance().getTime();
			if (!Util.isEmpty(sDate))
			{
				sDate = sDate.replaceAll("-", "/");

				date = new Date(sDate);
			}
			sqlDate = new java.sql.Date(date.getTime());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return sqlDate;
	}

	public static java.util.Date getDateTimeHours(String sDate) {
		java.util.Date utilDate = null;
		try
		{
			Date date = Calendar.getInstance().getTime();
			// Calendar.AM_PM;
			// Date date = new Date();
			int year = date.getYear();
			int month = date.getMonth();
			int day = date.getDate();
			int hour = date.getHours();
			int minute = date.getMinutes();
			int second = date.getSeconds();
			java.sql.Timestamp timestamp = new java.sql.Timestamp(year, month, day, hour, minute, second, 0);
			if (!Util.isEmpty(sDate))
			{
				sDate = sDate.replaceAll("-", "/");
				date = new Date(sDate);
			}
			utilDate = timestamp;
		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{
			return utilDate;
		}
	}

	/** Creates new date */
	public Dates(int m, int d, int y) {
		month = m;
		day = d;
		year = y;
	}


	public static Date addWeeks(Date d_date, int num_weeks) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(d_date);

		cal.add(Calendar.DAY_OF_WEEK, Calendar.DAY_OF_WEEK * num_weeks);

		Date newDate = (Date) cal.getTime();

		return newDate;
	}

	// returns a new date given a date and a number of days to add or subtract from
	public static Date add(Date d_date, int num_days) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(d_date);

		cal.add(Calendar.DATE, num_days);

		return (Date) cal.getTime();
	}

	// returns a new date string given a date and a number of days to add or subtract from
	public String add(Dates d_date, int num_days)
	{
		// New calander date
		Calendar date = new GregorianCalendar();

		// Need this line to format to string
		DateFormat df = DateFormat.getDateInstance();

		// Clear the calander object
		date.clear();

		// Set the dates from the user input
		date.set(d_date.year, d_date.month - 1, d_date.day);

		// add days
		date.add(Calendar.DATE, num_days);

		// Get the date
		Date d = (Date) date.getTime();

		// Format to a string and return
		s_date = df.format(d);

		return s_date;

	}

	// returns a new date string given a date string and a number of days to add or subtract from
	public Date add(String s_date, int num_days, boolean bdate) {
		final String METHOD_NAME = "addDate ";

		y = 0;
		d = 0;
		m = 0;

		StringBuffer sb = new StringBuffer(s_date);
		String token = findToken(sb);

		s_date = sb.toString();

		StringTokenizer parser = new StringTokenizer(s_date, token);

		try {
			if (s_date.indexOf(token) < 4) {
				while (parser.hasMoreTokens()) {
					m = Integer.parseInt(parser.nextToken());
					d = Integer.parseInt(parser.nextToken());
					y = Integer.parseInt(parser.nextToken());
				}
			}
			else {
				while (parser.hasMoreTokens()) {
					y = Integer.parseInt(parser.nextToken());
					m = Integer.parseInt(parser.nextToken());
					d = Integer.parseInt(parser.nextToken());
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

		// New calander date
		Calendar date = new GregorianCalendar();

		// Clear the calander object
		date.clear();

		// Set the dates from the user input
		date.set(y, m - 1, d);

		// add days
		date.add(Calendar.DATE, num_days);

		// Get the date
		Date d = (Date) date.getTime();

		return d;
	}

	public Date add(String s_date, int num_days, boolean bdate, String format) {
		final String METHOD_NAME = "addDate ";

		y = 0;
		d = 0;
		m = 0;

		StringBuffer sb = new StringBuffer(s_date);
		String token = findToken(sb);

		s_date = sb.toString();

		StringTokenizer parser = new StringTokenizer(s_date, token);

		try {
			if (format.indexOf("d") < format.indexOf("M") && format.indexOf("M") < format.indexOf("y")) {
				while (parser.hasMoreTokens()) {
					d = Integer.parseInt(parser.nextToken());
					m = Integer.parseInt(parser.nextToken());
					y = Integer.parseInt(parser.nextToken());
				}
			}
			else if (format.indexOf("y") < format.indexOf("d") && format.indexOf("d") < format.indexOf("M")) {
				while (parser.hasMoreTokens()) {
					y = Integer.parseInt(parser.nextToken());
					d = Integer.parseInt(parser.nextToken());
					m = Integer.parseInt(parser.nextToken());
				}
			}
			else if (s_date.indexOf(token) < 4) {
				while (parser.hasMoreTokens()) {
					m = Integer.parseInt(parser.nextToken());
					d = Integer.parseInt(parser.nextToken());
					y = Integer.parseInt(parser.nextToken());
				}
			}
			else {
				while (parser.hasMoreTokens()) {
					y = Integer.parseInt(parser.nextToken());
					m = Integer.parseInt(parser.nextToken());
					d = Integer.parseInt(parser.nextToken());
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

		// New calander date
		Calendar date = new GregorianCalendar();

		// Clear the calander object
		date.clear();

		// Set the dates from the user input
		date.set(y, m - 1, d);

		// add days
		date.add(Calendar.DATE, num_days);

		// Get the date
		Date d = (Date) date.getTime();

		return d;
	}

	// returns a new date string given a date string and a number of days to add or subtract from
	public static String add(String s_date, int num_days) {
		return add(s_date, num_days, "MM-dd-yyyy");
	}

	// returns a new date string given a date string and a number of days to add or subtract from
	public static String add(String s_date, int num_days, String format) {
		Dates dates = new Dates();

		Date d = null;
		if (format.equals("MM-dd-yyyy"))
		{
			d = dates.add(s_date, num_days, true);
		}
		else
		{
			d = dates.add(s_date, num_days, true, format);
		}

		// Format to a string and return
		// SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
		SimpleDateFormat df = new SimpleDateFormat(format);

		s_date = df.format(d);

		return s_date;

	}


	// returns todays date as a string in default format if asFormat empty
	/**
	 * @param format
	 * @return
	 */
	public static String today(String format) {
		return today(format, "");
	}

	// returns todays date as a string in default format if format empty, default server time zone if timeZoneId is empty
	/**
	 * @param format
	 * @return
	 */
	public static String today(String format, String timeZoneId) {
		SimpleDateFormat formatter;
		String dateString;
		String dateFormat = "";
		String origFormat = "";

		if (Util.isEmpty(timeZoneId)) {
			// timeZoneId = Calendar.getInstance().getTimeZone().getID();
			timeZoneId = TimeZone.getDefault().getID();
		}

		if (!Util.isEmpty(format) && format.length() == 10 || format.length() == 8) {
			format = format.replaceAll("Y", "y");
			format = format.replaceAll("D", "d");
		}

		if (Util.isEmpty(format)) {
			dateFormat = "MM/dd/yyyy";

			origFormat = dateFormat;
			dateFormat = dateFormat.replace('-', '/');
		}
		else {
			dateFormat = format;
		}

		formatter = new SimpleDateFormat(dateFormat);
		formatter.setTimeZone(TimeZone.getTimeZone(timeZoneId));
		dateString = formatter.format(new Date());

		if (origFormat.indexOf("-") > 0) {
			dateString = dateString.replace('/', '-');
		}

		return dateString;
	}

	/**
	 * @param format
	 * @return
	 */
	public static java.util.Date getCurrentDate(String timeZoneId) {
		SimpleDateFormat formatter;
		java.util.Date today;

		if (Util.isEmpty(timeZoneId)) {
			timeZoneId = TimeZone.getDefault().getID();
		}

		formatter = new SimpleDateFormat("M/d/yy h:mm:ss a");
		formatter.setTimeZone(TimeZone.getTimeZone(timeZoneId));
		String dateString = formatter.format(new Date());

		try {
			// today = new Date(dateString);
			today = formatter.parse(dateString);
		} catch (Exception e) {
			today = new Date();
		}

		return today;
	}

	public static String getCurrentDate(String format, String organization, String timeZoneId) {
		SimpleDateFormat formatter;

		if (Util.isEmpty(timeZoneId)) {
			timeZoneId = TimeZone.getDefault().getID();
		}
		String locale = "en";
		if (organization.equals("b2b"))
		{
			locale = "es";
		}
		formatter = new SimpleDateFormat(format, new Locale(locale));
		formatter.setTimeZone(TimeZone.getTimeZone(timeZoneId));
		String stringToday = formatter.format(new Date());
		return stringToday;
	}

	public static String getDate(Date date, String format, String organization, String timeZoneId) {
		SimpleDateFormat formatter;

		if (Util.isEmpty(timeZoneId)) {
			timeZoneId = TimeZone.getDefault().getID();
		}
		String locale = "en";
		if (organization.equals("b2b"))
		{
			locale = "es";
		}
		formatter = new SimpleDateFormat(format, new Locale(locale));
		formatter.setTimeZone(TimeZone.getTimeZone(timeZoneId));
		String stringToday = "";
		if (date != null) {
			stringToday = formatter.format(date);
		}
		return stringToday;
	}

	public static String getFiscalYear(int beginday, int begin, String timeZoneId, String organizationId)
	{
		String fiscalYear = "";
		try {
			// fiscalYear = PlantCalendarManager.getInstance().getPlantCalendarFiscalYear(organizationId);
		} catch (Exception e) {
		}
		if (fiscalYear == null || fiscalYear.trim().length() <= 0) {
			fiscalYear = getFiscalYear(beginday, begin, timeZoneId);
		}
		return fiscalYear;
	}

	public static String getFiscalYear(int beginday, int begin, String timeZoneId)
	{
		Date today = Dates.getCurrentDate(timeZoneId);
		// Calendar cal = GregorianCalendar.getInstance();
		int day = today.getDate() + 1;
		// int month = cal.get(Calendar.DATE) + 1;

		int month = today.getMonth() + 1;
		// int month = cal.get(Calendar.MONTH) + 1;
		int year = today.getYear() + 1900;
		// int year = cal.get(Calendar.YEAR) + 1900;

		String fYear = null;

		if ((month > begin) || (begin == 1 && beginday == 1) || (month == begin && day >= beginday))
		{
			fYear = Integer.toString(year);
		}
		else
		{
			fYear = Integer.toString(year - 1);
		}

		return fYear;
	}

	public static Date getFiscalYearStartDate(int beginday, int begin, String timeZoneId, String organizationId)
	{
		Date today = new Date();
		int day = today.getDate() + 1;
		int month = today.getMonth() + 1;
		String fiscalYear = String.valueOf(month) + "-" + String.valueOf(day) + "-" + Dates.getFiscalYear(beginday, begin, timeZoneId, organizationId);

		return Dates.getDate(fiscalYear);
	}

	// Returns the number of seconds between two Time Strings
	// If dates are included in the string, they are ignored
	public int getSecondsBetween(String s_date1, String s_date2) {
		Time time1 = this.getTime(s_date1);
		Time time2 = this.getTime(s_date2);

		return this.getSecondsBetween(time1, time2);
	}

	// Returns the number of seconds between two Times
	public int getSecondsBetween(Time t1, Time t2) {
		int seconds1 = 0;
		int seconds2 = 0;
		int minutes = 0;
		int hours = 0;

		hours = t1.getHours();
		minutes = t1.getMinutes();
		seconds1 = t1.getSeconds();

		seconds1 = (hours * 3600) + (minutes * 60) + seconds1;

		hours = t2.getHours();
		minutes = t2.getMinutes();
		seconds2 = t2.getSeconds();

		seconds2 = (hours * 3600) + (minutes * 60) + seconds2;

		return (seconds2 - seconds1);
	}

	public static String convertFrom24to12(String time)
	{
		String time12 = "";
		try
		{
			SimpleDateFormat sdf24 = new SimpleDateFormat("hh:mm");
			SimpleDateFormat sdf12 = new SimpleDateFormat("hh:mm aa");
			time12 = sdf12.format(sdf24.parse("13:09"));

		} catch (ParseException e)
		{
			time12 = "";
			e.printStackTrace();
		}
		return time12;
	}

	public static void main(String[] args) {

		try
		{
			SimpleDateFormat sdf24 = new SimpleDateFormat("hh:mm");
			SimpleDateFormat sdf12 = new SimpleDateFormat("hh:mm aa");
			System.out.println(sdf12.format(sdf24.parse("13:09")));

			(new SimpleDateFormat("hh:mm")).format(((new SimpleDateFormat("hh:mm")).parse("13:09")));
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Date getFirstDayOfWeek(Date date)
	{
		Calendar cal = Calendar.getInstance();
		if (!Util.isObjectEmpty(date))
		{
			cal.setTime(date);
		}
		// cal.setFirstDayOfWeek(Calendar);

		return Dates.add(cal.getTime(), ((cal.get(Calendar.DAY_OF_WEEK) - cal.getFirstDayOfWeek())) * -1);
	}

	public static Date getFirstDayOfMonth(Date date)
	{
		Calendar cal = Calendar.getInstance();
		if (!Util.isObjectEmpty(date))
		{
			cal.setTime(date);
		}
		// cal.setFirstDayOfWeek(Calendar);
		cal.set(Calendar.DATE, 1);

		return cal.getTime();
	}

	public static Date getFirstDayOfYear(Date date)
	{
		Calendar cal = Calendar.getInstance();
		if (!Util.isObjectEmpty(date))
		{
			cal.setTime(date);
		}
		// cal.setFirstDayOfWeek(Calendar);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.MONTH, Calendar.JANUARY);

		return cal.getTime();
	}

	public static String getTimeString(String sdate) {
		String stime = "";
		int ibegin = 0;
		int iend = 0;

		sdate = sdate.trim();

		// strip out the date string
		if (sdate.indexOf(':') > 2) {
			ibegin = sdate.indexOf(':') - 2;
			stime = (sdate.substring(ibegin, sdate.length())).trim();
		}

		// strip out the milliseconds
		if (stime.lastIndexOf(':') > 5) {
			iend = stime.lastIndexOf(':');
			stime = (stime.substring(0, iend)).trim();
		}

		// just to be sure
		if (stime.length() > 8) {
			iend = 8;
			stime = (stime.substring(0, iend)).trim();
		}

		return stime;
	}

	public static String getNow(String time)
	{
		return getNow(time, null);
	}

	public static String getNow(String time, String timeZoneId)
	{
		String ret = null;
		if (!Util.isEmpty(time))
		{
			ret = Dates.getTimeString(time);
		}
		else
		{
			Date date = Dates.getCurrentDate(timeZoneId);

			ret = Dates.getTimeString(date.toString());
		}
		return ret;
	}

	public Time getTime(String sdate) {
		return Time.valueOf(getTimeString(sdate));
	}

	// Converts a String Date in the MM-dd-yyyy format
	// to the yyyy/mm/dd Format.
	public String reformatDate(String sdate) {
		/*
		 * String result = "";
		 * int ilength = sdate.length();
		 * String year = sdate.substring(ilength - 4, ilength);
		 * int islash = sdate.indexOf("/");
		 * String month = sdate.substring(0,islash);
		 * int islash2 = sdate.indexOf("/",islash+1);
		 * String day = sdate.substring(islash+1,islash2);
		 * result = year + "/" + month + "/" + day;
		 * return result;
		 */

		String result = "";

		DateFormat dateFormatter = new SimpleDateFormat("MM-dd-yyyy");

		result = dateFormatter.format(new Date(sdate));

		return result;
	}

	/**
	 * Method ofOffsetDays
	 * Determine the number of days to add to a passed in number
	 * of offset days, based on weekends/closed days
	 * 
	 * @param int offsetDays - Offset Days
	 * @param boolean bschedule - true if "Calendar Management", "Schedule", = Y in the ini file
	 * @return int
	 *         - number of offset days
	 */
	public int getOffsetDays(int offsetDays, boolean bschedule) {
		Date ddate = new Date();

		for (int i = 1; i <= offsetDays; i++) {
			ddate = add(ddate, 1);

			if (isWeekend(ddate)) {
				offsetDays++;
			}
			else if (bschedule) {
				// if (isClosedDate(ddate))
				// {
				// offsetDays++;
				// }
			}
		}

		return offsetDays;
	}

	/**
	 * Method isWeekend
	 * Determines if the Date passed in is a Saturday or Sunday
	 * 
	 * @param Date
	 *            - Date to be tested
	 * @return boolean
	 *         true - Date is a weekend
	 *         false - Date is NOT a weekend
	 */
	private boolean isWeekend(Date ddate) {
		final String METHOD_NAME = "isWeekend ";

		if (ddate == null) {
			return false;
		}

		// Check for a Sunday or Saturday
		if (ddate.getDay() == 0 || ddate.getDay() == 6) {
			// Yes, it is a WeekEnd
			return true;
		}

		// No, it must not be a weekend
		return false;
	}

	/**
	 * Method findToken
	 * Determines the token used in the date and replaces the token if found invalid
	 * 
	 * @param StringBuffer
	 *            - holds date value to be checked for the token
	 * @return String - token
	 */


	public static int getDaysAfter(String beginDate, String endDate, int stopDays) throws Exception {
		int daysAfter = 0;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			try {
				if (Util.isEmpty(beginDate)) {
					beginDate = formatter.format(new Date());
				} else {
					beginDate = beginDate.replace('/', '-');
					beginDate = formatter.format(formatter.parse(beginDate));
				}

				if (Util.isEmpty(endDate)) {
					endDate = formatter.format(new Date());
				} else {
					endDate = endDate.replace('/', '-');
					endDate = formatter.format(formatter.parse(endDate));
				}
			} catch (ParseException pe)
			{
				pe.printStackTrace();
				throw pe;
			}

			int yearBegin = Integer.valueOf(beginDate.substring(0, 4)).intValue();
			int monthBegin = Integer.valueOf(beginDate.substring(5, 7)).intValue();
			int dayBegin = Integer.valueOf(beginDate.substring(8, 10)).intValue();
			int yearEnd = Integer.valueOf(endDate.substring(0, 4)).intValue();
			int monthEnd = Integer.valueOf(endDate.substring(5, 7)).intValue();
			int dayEnd = Integer.valueOf(endDate.substring(8, 10)).intValue();

			Calendar begin = Calendar.getInstance();
			Calendar end = Calendar.getInstance();

			begin.set(yearBegin, monthBegin, dayBegin);
			end.set(yearEnd, monthEnd, dayEnd);

			while (begin.before(end) && ((stopDays == 0) || (daysAfter < stopDays)))
			{
				begin.add(Calendar.DAY_OF_MONTH, 1);
				daysAfter++;
			}
		} catch (Exception e)
		{
			throw e;
		}
		return daysAfter;
	}

	// returns a new date given a long date time and the number of minutes to add or subtract from
	public static Date addMinutes(Date d_date, int minutes) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(d_date);

		cal.add(Calendar.MINUTE, minutes);

		Date newDate = (Date) cal.getTime();

		return newDate;
	}

	public static Date addMonths(Date d_date, int months) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(d_date);

		cal.add(Calendar.MONTH, months);

		Date newDate = (Date) cal.getTime();

		return newDate;
	}

	// returns a new date given a long date time and the number of seconds to add or subtract from
	public static Date addSeconds(Date d_date, int seconds) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(d_date);

		cal.add(Calendar.SECOND, seconds);

		Date newDate = (Date) cal.getTime();

		return newDate;
	}

	public static String getUTCTimeZone(String timeZoneId) {
		String offset = "";
		try {
			TimeZone tz = TimeZone.getDefault();
			if (!Util.isEmpty(timeZoneId)) {
				tz = TimeZone.getTimeZone(timeZoneId);
			}

			int rawOffset = tz.getRawOffset();
			int hour = rawOffset / (60 * 60 * 1000);
			int min = Math.abs(rawOffset / (60 * 1000)) % 60;
			String smin = String.valueOf(min);
			String shour = String.valueOf(hour);
			if (hour == 0) {
				shour = " " + shour;
			} else if (hour > 0) {
				shour = "+" + shour;
			}
			if (smin.length() < 2) {
				smin = smin + "0";
			}
			offset = "UTC " + shour + ":" + smin;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return offset;
	}

	public static String getTimeZoneAbbreviation(String timeZoneId) {
		if (Util.isEmpty(timeZoneId)) {
			timeZoneId = TimeZone.getDefault().getID();
		}
		TimeZone tz = TimeZone.getTimeZone(timeZoneId);
		String abbreviation = tz.getDisplayName(false, TimeZone.SHORT);

		return abbreviation;
	}

	public static String getTimeZoneAbbreviation(String timeZoneId, String sdate) {
		Date date = Dates.getDate(sdate);
		return Dates.getTimeZoneAbbreviation(timeZoneId, date);
	}

	public static String getTimeZoneAbbreviation(String timeZoneId, Date date) {
		if (Util.isEmpty(timeZoneId)) {
			timeZoneId = TimeZone.getDefault().getID();
		}
		TimeZone tz = TimeZone.getTimeZone(timeZoneId);
		String abbreviation = tz.getDisplayName(tz.inDaylightTime(date), TimeZone.SHORT);

		return abbreviation;
	}

	public static boolean equalsPartDate(Date d1, Date d2) {
		int result = comparePartDate(d1, d2);
		return result == 0;
	}

	public static int comparePartDate(Date d1, Date d2) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(d1);
		int year1 = calendar1.get(Calendar.YEAR);
		int month1 = calendar1.get(Calendar.MONTH);
		int day1 = calendar1.get(Calendar.DAY_OF_MONTH);

		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(d2);
		int year2 = calendar2.get(Calendar.YEAR);
		int month2 = calendar2.get(Calendar.MONTH);
		int day2 = calendar2.get(Calendar.DAY_OF_MONTH);

		if (year1 != year2)
			return year1 - year2;
		if (month1 != month2)
			return month1 - month2;
		return day1 - day2;
	}


	private String findToken(StringBuffer sbdate) {
		String token = "";
		String sdate = sbdate.toString();

		if (sdate.indexOf("/") > 0) {
			token = "/";
		}
		else if (sdate.indexOf("-") > 0) {
			token = "-";
		}
		else if (sdate.indexOf("\\") > 0) {
			token = "/";
			sdate.replace('\\', '/');
		} else if (sdate.indexOf(".") > 0) {
			token = ".";
		}
		else {
		}

		sbdate.setLength(0);
		sbdate.append(sdate);

		return token;
	}
	
}