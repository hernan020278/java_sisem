package com.comun.utilidad.swing;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.comun.utilidad.AplicacionGeneral;

public class PanelImgMsg extends JPanel {

	/*
     *
     */
	private BufferedImage muestra;
	private javax.swing.Timer t;
	/*
     *
     */
	private Image varImg;
	public static Graphics2D g2d;
	/*
     *
     */
	private TitledBorder bordTitulo;

	public PanelImgMsg()
	{

		ImageIcon imageIcon = AplicacionGeneral.getInstance().obtenerImagen("pregunta.png");
		setImagen(imageIcon.getImage());
	}

	public void setImagen(Image parImg) {

		this.varImg = parImg;
	}// Fin de Metodo SetImagen para colocar una Imagen en al Panmel

	@Override
	public void paint(Graphics g) {

		// bordTitulo = BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED), "");
		// super.setBorder(bordTitulo);
		Dimension tamanio = getSize();
		if(varImg != null) {
			g.drawImage(varImg, 0, 0, tamanio.width, tamanio.height, this);
		}// Fin de Boque Verdadero si hay alguna imagen para dibujar
		super.setOpaque(false);
		super.paint(g);
	}// Fin de metodo de dibujad de Jpanel

	public static void main(String[] args) {

		URL urlClassPath;
		Image img;
		JFrame fr = new JFrame();
		PanelImgMsg panfot = new PanelImgMsg();
		panfot.setImagen(AplicacionGeneral.getInstance().obtenerBufferImagen("pregunta.png"));
		fr.add(panfot);
		fr.setVisible(true);
		fr.setSize(200, 200);
	}
}// Fin de clase principal Panel Foto
