package com.comun.utilidad.swing;

import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class PanelCheckBox extends JPanel{
    private JCheckBox[] checkFormat;
    
    public PanelCheckBox(String txtTitulo, String itemCheck[]){
        
        TitledBorder borTitulo = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),txtTitulo);        
        super.setBorder(borTitulo);
        super.setLayout(new GridLayout(1, 1));
            
            checkFormat = new JCheckBox[itemCheck.length];
        for(int numCheck = 0; numCheck < itemCheck.length; numCheck++){
            checkFormat[numCheck] = new JCheckBox(itemCheck[numCheck]);
            super.add(checkFormat[numCheck]);
        }        
    }//Finnde metodo constructor
    
    public JCheckBox[] getCheck(){
        return checkFormat;
    }
    
}//Fin de Clase principal PanelTexto
