package com.comun.utilidad.swing;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JComboBox;
import javax.swing.border.Border;

public class JComboBoxChanged extends JComboBox {

    Border borderTexto;

    public JComboBoxChanged() {


        JComboBox comboBorde = new JComboBox();
        borderTexto = comboBorde.getBorder();

        super.setBackground(new Color(212, 208, 200));
        super.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        super.setOpaque(false);

        super.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {

                JComboBox control = (JComboBox) e.getSource();
                control.setBorder(borderTexto);
                control.setBackground(Color.WHITE);
                control.setOpaque(true);

            }

            @Override
            public void focusLost(FocusEvent e) {

                JComboBox control = (JComboBox) e.getSource();
                control.setBackground(new Color(212, 208, 200));
                control.setBorder(javax.swing.BorderFactory.createEtchedBorder());
                control.setOpaque(false);

            }
        });
    }//Fin de constructor
}//Fin de Clase Principal
