package com.comun.utilidad.swing;

import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class PanelComboBox extends JPanel{
    protected JComboBox cmbFormateado;
    protected JScrollPane scrllList;
    
    public PanelComboBox(String txtTitulo, String itemCombo[], boolean edit){
        
        TitledBorder borTitulo = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),txtTitulo);        
        super.setBorder(borTitulo);
        super.setLayout(new GridLayout(1, 1));

        cmbFormateado = new JComboBox(itemCombo);
        cmbFormateado.setEditable(edit);
        
        super.add(cmbFormateado);
    }//Finnde metodo constructor
    
    public JComboBox getCombo(){
        return cmbFormateado;
    }
    
}//Fin de Clase principal PanelTexto
