package com.comun.utilidad.swing;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class PanelControlProceso extends JPanel
{
	private float alpha = 0.3f;
	private float diff = -0.02f;
	private BufferedImage img;
	public JProgressBar barraProgreso;
	public JLabel etiAccionProgreso;
	public JLabel etiEventoProgreso;

	public static PanelControlProceso INSTANCE;

	public static PanelControlProceso getInstance()
	{
		if (PanelControlProceso.INSTANCE == null)
		{
			PanelControlProceso.INSTANCE = new PanelControlProceso();
		}
		return PanelControlProceso.INSTANCE;
	}

	public PanelControlProceso() {
		try {
			img = ImageIO.read(new File("C:\\sisem\\img\\asesor\\Fondo.png"));
			// Timer timer = new Timer(40, new ActionListener() {
			// @Override
			// public void actionPerformed(ActionEvent e) {
			// alpha += diff;
			// if (alpha < 0) {
			// diff *= -1;
			// alpha = diff;
			// }
			// else if (alpha > 1f) {
			// diff *= -1;
			// alpha = 1f + diff;
			// }
			// repaint();
			// }
			// });
			// timer.setRepeats(true);
			// timer.setCoalesce(true);
			// timer.start();

		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void setTamano(int ancho, int alto)
	{
		super.setBounds(0, 0, ancho, alto);
		repaint();
	}

//	@Override
//	public Dimension getPreferredSize() {
//		return img == null ? super.getPreferredSize() : new Dimension(img.getWidth(), img.getHeight());
//	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.removeAll();

		System.out.println("ancho " + img.getWidth() + " alto : " + img.getHeight());
		int posIniX = ((super.getWidth() - 382) / 2);
		int posIniY = ((super.getHeight() - 120) / 2);

		if(etiEventoProgreso == null)
		{
			etiEventoProgreso = new JLabel("Procesos Ejecutandose por favor espere...");
			etiEventoProgreso.setHorizontalAlignment(SwingConstants.CENTER);
			etiEventoProgreso.setFont(new Font("Tahoma", Font.PLAIN, 14));
			etiEventoProgreso.setBounds(posIniX, posIniY, 382, 30);
			super.add(etiEventoProgreso);

			etiAccionProgreso = new JLabel("Llenando tabla de resultados");
			etiAccionProgreso.setFont(new Font("Tahoma", Font.PLAIN, 12));
			etiAccionProgreso.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
			etiAccionProgreso.setBounds(posIniX, posIniY + 40, 382, 30);
			super.add(etiAccionProgreso);

			barraProgreso = new JProgressBar();
			barraProgreso.setIndeterminate(true);
			barraProgreso.setBounds(posIniX, posIniY + 80, 382, 30);
			super.add(barraProgreso);
		}
		else
		{
			etiEventoProgreso.setBounds(posIniX, posIniY, 382, 30);
			etiAccionProgreso.setBounds(posIniX, posIniY + 40, 382, 30);
			barraProgreso.setBounds(posIniX, posIniY + 80, 382, 30);
		}

		super.paintComponent(g);
		if (img != null) {
			Graphics2D g2d = (Graphics2D) g.create();
			g2d.setComposite(AlphaComposite.SrcOver.derive(alpha));
			int x = getWidth() - img.getWidth();
			int y = getHeight() - img.getHeight();
			g2d.drawImage(img, x, y, this);
			g2d.dispose();
		}
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {

				JFrame frame = new JFrame("Testing");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setLayout(new BorderLayout());
				frame.add(new PanelControlProceso());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

}// class reloj

