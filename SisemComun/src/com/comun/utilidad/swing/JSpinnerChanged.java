package com.comun.utilidad.swing;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;

public class JSpinnerChanged extends JSpinner {
    
    JFormattedTextField textoBorde;
    JFormattedTextField control;
    int numCar;
    
    public JSpinnerChanged() {
        
        control = ((JSpinner.DefaultEditor) super.getEditor()).getTextField();
        control.setDisabledTextColor(Color.BLACK);
//        control.setFormatterFactory(new FormatoEntero(3));
        
        textoBorde = new JFormattedTextField();
        
        super.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                
                JSpinner controlSpinner = (JSpinner) evt.getSource();
                control = ((JSpinner.DefaultEditor) controlSpinner.getEditor()).getTextField();
                if (control.isEnabled()) {
                    
                    control.setBorder(textoBorde.getBorder());
//                    control.setBackground(Color.WHITE);
                    control.setOpaque(true);
                    
                } else if (!control.isEnabled()) {
                    
                    control.setBorder(javax.swing.BorderFactory.createEtchedBorder());
                    control.setBackground(textoBorde.getBackground());
                    control.setOpaque(false);
                    
                }
            }
        });
        
        control.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent evt) {
                
                control.setBackground(Color.YELLOW);
                control.setOpaque(true);
                
            }
            
            @Override
            public void focusLost(FocusEvent evt) {
                
                control.setBackground(Color.WHITE);
                control.setOpaque(true);
                
            }
        });
    }//Fin de Constructor    
}//Fin de Clase Principal

