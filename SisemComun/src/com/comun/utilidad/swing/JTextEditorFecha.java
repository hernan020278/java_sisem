package com.comun.utilidad.swing;

import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class JTextEditorFecha extends JTextFieldDateEditor {

	private static final long serialVersionUID = 1L;
	
	JTextFieldDateEditor textoBorde;
    int numCar;

    public JTextEditorFecha() {

        super.setDisabledTextColor(Color.BLACK);
        textoBorde = new JTextFieldDateEditor();

        super.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                JTextFieldDateEditor control = (JTextFieldDateEditor) evt.getSource();
                if (control.isEnabled()) {

                    control.setBorder(textoBorde.getBorder());
//                    control.setBackground(Color.WHITE);
                    control.setOpaque(true);

                } else if (!control.isEnabled()) {

                    control.setBorder(javax.swing.BorderFactory.createEtchedBorder());
                    control.setBackground(textoBorde.getBackground());
                    control.setOpaque(false);

                }
            }
        });

        super.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent evt) {

                JTextFieldDateEditor control = (JTextFieldDateEditor) evt.getSource();
				control.setText(control.getText());
				control.selectAll();
				control.setBackground(Color.YELLOW);
				control.setOpaque(true);
            }

            @Override
            public void focusLost(FocusEvent evt) {

                JTextFieldDateEditor control = (JTextFieldDateEditor) evt.getSource();
                control.setBackground(Color.WHITE);
                control.setOpaque(true);

            }
        });
    }//Fin de Constructor
}//Fin de Clase Principal
