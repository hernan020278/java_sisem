package com.comun.utilidad.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.MyArchivoFiltro;
import com.comun.utilidad.Util;

public final class PanelImgFoto extends JPanel {

	private static final long serialVersionUID = 1L;

	private double ancho, alto;
	/*
     *
     */
	private BufferedImage buffImg;
	private Graphics2D graf2D, buffImgGraf2D;
	/*
     *
     */
	private URL urlClassPath;
	private ImageIcon imgIco;
	private Image img = null;
	private File fil = null;
	private FileInputStream fis = null;
	private double prcConst;
	/*
     *
     */
	private String varSizeImg = "CONSTRAINT";
	private TitledBorder bordTitulo;
	private int coordY, coordX;

	public PanelImgFoto() {

		imgIco = Util.getInst().obtenerImagenIconArchivo(AplicacionGeneral.getInstance().obtenerRutaImagenes() + "pregunta.png");
		setImagen(imgIco, null, null, "CONSTRAINT");

	}// Fin de Constructor

	@Override
	public void paint(Graphics g) {

		bordTitulo = BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		super.setBorder(bordTitulo);

		Dimension dimensionPanel = getSize();

		if (img != null) {
			if (varSizeImg.equals("GRANDE")) {

				g.drawImage(img, 0, 0, dimensionPanel.width, dimensionPanel.height, this);

			} else if (varSizeImg.equals("CHICO")) {

				g.drawImage(img, 30, 40, dimensionPanel.width - 60, dimensionPanel.height - 80, this);

			} else if (varSizeImg.equals("CONSTRAINT")) {

				establecarConstraintImagen(dimensionPanel);
				g.drawImage(img, coordX, coordY, (int) ancho, (int) alto, this);

			}// else if (varSizeImg.equals("CONSTRAINT"))
		}// Fin de Boque Verdadero si hay alguna imagen para dibujar

		super.setOpaque(false);
		super.paint(g);

	}// Fin de metodo de dibujad de Jpanel

	private void establecarConstraintImagen(Dimension dimensionPanel) {
		if (img.getWidth(null) > img.getHeight(null)) {

			prcConst = (img.getHeight(null) * 100.00) / img.getWidth(null);
			ancho = dimensionPanel.getWidth();
			alto = dimensionPanel.getWidth() * (prcConst / 100.00);

			coordX = 0;
			coordY = (int) (dimensionPanel.getHeight() - alto) / 2;

		}// Fin de if(img.getWidth(null) > img.getHeight(null))
		else if (img.getHeight(null) > img.getWidth(null)) {

			prcConst = (img.getWidth(null) * 100.00) / img.getHeight(null);
			ancho = dimensionPanel.getHeight() * (prcConst / 100);
			alto = dimensionPanel.getHeight();

			coordX = (int) (dimensionPanel.getWidth() - ancho) / 2;
			coordY = 0;

		}// Fin de else if (img.getHeight(null) > img.getWidth(null))
		else {

			prcConst = 100.00;// 100 porciento de la imagen
			ancho = dimensionPanel.getWidth();
			alto = dimensionPanel.getHeight();
			coordX = 0;
			coordY = 0;

		}
		// System.out.println("ancho panel : " + dimensionPanel.width + " alto panel : " +
		// dimensionPanel.height
		// + "ancho imagen : " + img.getWidth(null) + " alto iamgen : " + img.getHeight(null) +
		// " ancho : " + ancho + " alto : " + alto);
	}// Fin de establecer el constraint de la imagen

	public void setImagen(ImageIcon parImgIco, File parFile, FileInputStream parFileInpStr, String parSizeImg) {

		this.fil = parFile;
		this.fis = parFileInpStr;
		this.imgIco = parImgIco;
		this.varSizeImg = parSizeImg;

		try {

			img = parImgIco.getImage();

		} catch (Exception e) {

			imgIco = Util.getInst().obtenerImagenIconArchivo(AplicacionGeneral.getInstance().obtenerRutaImagenes() + "pregunta.png");
//			File varFil = new File(urlClassPath.getPath());
			// FileInputStream varFis = new FileInputStream(varFil);

			setImagen(imgIco, null, null, "CONSTRAINT");

		}// Fin de Metoo Catch

	}// Fin de Metodo SetImagen para colocar una Imagen en al Panmel

	public ImageIcon getImagen() {
		return imgIco;
	}

	public File getFil() {
		return fil;
	}

	public FileInputStream getFis() {
		return fis;
	}

	public void pegarImagenClipboard() {

		Image image;
		// Obtiene el Clipboard del sistema
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

		// Obtiene el contenido
		Transferable transferable = clipboard.getContents(null);
		if (transferable == null) {
			return;
		}

		// Verifica si hay una imagen. Si es así, la pone en la
		// etiqueta.
		if (transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {
			try {

				image = (Image) transferable.getTransferData(DataFlavor.imageFlavor);
				ImageIO.write((BufferedImage) image, "png", new File("D://image.png"));

				ImageIcon imageIcon = new ImageIcon(image);
				File file = new File("D://image.png");
				FileInputStream fileInputStream = new FileInputStream(file);
				setImagen(imageIcon, file, fileInputStream, "CONSTRAINT");

			} catch (UnsupportedFlavorException ex) {
				Logger.getLogger(PanelImgFoto.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException ex) {
				Logger.getLogger(PanelImgFoto.class.getName()).log(Level.SEVERE, null, ex);
			}
		}// Fin de if (transferable.isDataFlavorSupported(DataFlavor.imageFlavor))
		else {

			JOptionPane.showConfirmDialog(this,
					"!!!Copie una imagen a la memoria!!!"
							+ "\n" + this.getClass().getName(),
					"Sistema",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
		}

	}// Fin de pegarImagen	

	public void borrarImagenClipboard(String nombreArchivo) {

		Image image;
		String ruta = AplicacionGeneral.getInstance().obtenerRutaImagenes();
		
		File file = new File(ruta + nombreArchivo);
		// Verifica si hay una imagen. Si es así, la pone en la
		// etiqueta.
			try {

				image = imgIco.getImage();
				ImageIO.write((BufferedImage) image, "png", file);

				ImageIcon imageIcon = new ImageIcon(image);
				FileInputStream varFis = new FileInputStream(file);
				Util.getInst().copiarArchivo(ruta + "pregunta.png", ruta + nombreArchivo + ".jpg");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}// Fin de pegarImagen	
	
	public void pegarImagenClipboard(String nombreArchivo) {

		Image image;
		String ruta = AplicacionGeneral.getInstance(OrganizacionGeneral.getOrgcodigo()).obtenerRutaImagenes();
		if(!Util.isEmpty(nombreArchivo))
		{
			if(!nombreArchivo.contains(".png"))
			{
				nombreArchivo = nombreArchivo + ".png";
			}
			File file = new File(ruta + nombreArchivo);
			// Obtiene el Clipboard del sistema
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

			// Obtiene el contenido
			Transferable transferable = clipboard.getContents(null);
			if (transferable == null) {
				return;
			}

			// Verifica si hay una imagen. Si es así, la pone en la
			// etiqueta.
			if (transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {
				try {

					image = (Image) transferable.getTransferData(DataFlavor.imageFlavor);
					ImageIO.write((BufferedImage) image, "png", file);

					ImageIcon imageIcon = new ImageIcon(image);
					FileInputStream varFis = new FileInputStream(file);

					setImagen(imageIcon, file, varFis, "CONSTRAINT");

				} catch (UnsupportedFlavorException ex) {

					Logger.getLogger(PanelImgFoto.class.getName()).log(Level.SEVERE, null, ex);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}// Fin de if (transferable.isDataFlavorSupported(DataFlavor.imageFlavor))
			else 
			{
				JOptionPane.showConfirmDialog(this,
						"���Copie una imagen a la memoria!!!"
								+ "\n" + this.getClass().getName(),
						"Sistema",
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			}			
		}//if(Util.isEmpty(nombreArchivo))
		else
		{
			JOptionPane.showConfirmDialog(this,
					"���La imagen del clipboard debe tener un nombre!!!"
							+ "\n" + this.getClass().getName(),
					"Sistema",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
		}

	}// Fin de pegarImagen

	public void pegarImage(Image image) {

		try {
			if (image != null) {

				ImageIO.write((BufferedImage) image, "png", new File("D://temp.png"));

				ImageIcon imageIcon = new ImageIcon(image);
				File varFil = new File("D://temp.png");
				FileInputStream varFis = null;
				setImagen(imageIcon, varFil, varFis, "CONSTRAINT");

			} else {
				try {

					imgIco = Util.getInst().obtenerImagenIconArchivo(AplicacionGeneral.getInstance().obtenerRutaImagenes() + "pregunta.png");
					File varFil = new File(urlClassPath.getFile());
					FileInputStream varFis = new FileInputStream(varFil);

					setImagen(imgIco, varFil, varFis, "CONSTRAINT");

				} // Fin de Metoo Catch
				catch (FileNotFoundException ex) {
					Logger.getLogger(PanelImgFoto.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}// Fin de pegarImagen

	public void agregarFoto() {

		JFileChooser escogerFile = new JFileChooser();
		escogerFile.setCurrentDirectory(new File(System.getProperty("user.dir")));// El Directorio
																					// Actual
		escogerFile.setFileSelectionMode(JFileChooser.FILES_ONLY);
		String[] lisExt = { "jpg", "gif", "png" };
		MyArchivoFiltro filtrado = new MyArchivoFiltro(lisExt);
		escogerFile.addChoosableFileFilter(filtrado);

		int rpta = escogerFile.showDialog(this, "Seleccione un Archivo");

		// rpta = escogerFile.showOpenDialog(null);
		if (rpta == JFileChooser.APPROVE_OPTION) {
			if (filtrado.accept(escogerFile.getSelectedFile()) == true) {

				ImageIcon imgIcoCliClog = null;
				fil = escogerFile.getSelectedFile();

				if (fil.length() <= 20480) {

					try {
						try {
							fis = new FileInputStream(fil.getAbsoluteFile());
						} catch (FileNotFoundException ex) {
							System.err.println("Problemas ctrlTab el flujo de Archivo de imagen");
						}

						// imgCliLog = ImageIO.read(fileImg.toURL());
						imgIco = new ImageIcon(fil.toURL());

					} catch (IOException ex) {
						Logger.getLogger(PanelImgFoto.class.getName()).log(Level.SEVERE, null, ex);
					}

					setImagen(imgIcoCliClog, fil, fis, "GRANDE");
				}// Verificar el tamaño del archivo
				else {
					JOptionPane.showConfirmDialog(null,
							"���El archivo no debe pasar los 20Kb!!!",
							"Sistema Cobranza"
									+ "\n" + this.getClass().getName(),
							JOptionPane.PLAIN_MESSAGE,
							JOptionPane.INFORMATION_MESSAGE);

				}// Fin de que el archivo es muy grande
			} else {
				JOptionPane.showConfirmDialog(this, "Archivo invalido", "Systema de Cobranza", JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			}
		}// Fin de Block True if (resVal == JFileChooser.APPROVE_OPTION)
	}// Fin de agregarFoto()

	public static void main(String[] args) {

		JFrame frm = new JFrame();
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final PanelImgFoto panfot = new PanelImgFoto();
		JButton cmd = new JButton("Cambiar");
		cmd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				URL urlClassPath;
				ImageIcon varImgIco;

				varImgIco = Util.getInst().obtenerImagenIconArchivo(AplicacionGeneral.getInstance().obtenerRutaImagenes() + "pregunta.png");
				panfot.setImagen(varImgIco, null, null, "CONSTRAINT");
			}
		});

		panfot.setImagen(null, null, null, "CONSTRAINT");

		frm.setLayout(new BorderLayout());
		frm.add(panfot, BorderLayout.CENTER);
		frm.add(cmd, BorderLayout.SOUTH);
		frm.setSize(200, 200);
		frm.setVisible(true);
	}// Fin de metodo main()
}// Fin de clase principal

