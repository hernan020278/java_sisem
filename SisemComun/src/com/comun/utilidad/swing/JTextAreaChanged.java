/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comun.utilidad.swing;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 *
 * @author HERNAN
 */
public class JTextAreaChanged extends JTextArea {

    Border borderTexto;
    JTextArea textoBorde;
    int numCar;

    public JTextAreaChanged(int parNumCar) {

        super.setDisabledTextColor(Color.BLACK);
        textoBorde = new JTextArea();
        borderTexto = textoBorde.getBorder();
        this.numCar = parNumCar;

        super.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                JTextArea control = (JTextArea) evt.getSource();
                if (control.isEnabled()) {

                    control.setBorder(textoBorde.getBorder());
//                    control.setBackground(Color.WHITE);
                    control.setOpaque(true);

                } else if (!control.isEnabled()) {

                    control.setBorder(javax.swing.BorderFactory.createEtchedBorder());
                    control.setBackground(textoBorde.getBackground());
                    control.setOpaque(false);

                }
            }
        });

        super.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

                JTextArea control = (JTextArea) e.getSource();
                control.setBackground(Color.YELLOW);
                control.setOpaque(true);

            }

            @Override
            public void focusLost(FocusEvent e) {

                JTextArea control = (JTextArea) e.getSource();
                control.setBackground(Color.WHITE);
                control.setOpaque(true);

            }
        });

        super.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {

                JTextArea control = (JTextArea) e.getSource();
                if (control.getText().length() == numCar) {

                    e.consume();

                }
            }//Fin de metodo keyTyped
            public void keyReleased(KeyEvent e) {
            	JTextArea control = (JTextArea) e.getSource();
                control.setText(control.getText().toUpperCase());
            }
        });
    }//Fin de Constructor
}