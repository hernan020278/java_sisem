package com.comun.utilidad.swing;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JButton;

public class JButtonTrasparente extends JButton {

    public JButtonTrasparente(String text) {
        super(text);
        setOpaque(true);
        setBorderPainted(true);
        setContentAreaFilled(true);
    }

    @Override
    public void paint(Graphics g) {

        Graphics2D g2 = (Graphics2D) g.create();
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
        super.paint(g2);
        g2.dispose();

    }
}
