package com.comun.utilidad.swing;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFormattedTextField;

import com.comun.utilidad.formato.FormatoEntero;

public class JTextFieldFormatoEntero extends JFormattedTextField {

    JFormattedTextField textoBorde;

    public JTextFieldFormatoEntero(int parNumMax) {

        super(new FormatoEntero(parNumMax));
        super.setDisabledTextColor(Color.BLACK);

        textoBorde = new JFormattedTextField();

        super.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                JFormattedTextField control = (JFormattedTextField) evt.getSource();
                if (control.isEnabled()) {

                    control.setBorder(textoBorde.getBorder());
//                    control.setBackground(Color.WHITE);
                    control.setOpaque(true);

                } else if (!control.isEnabled()) {

                    control.setBorder(javax.swing.BorderFactory.createEtchedBorder());
                    control.setBackground(textoBorde.getBackground());
                    control.setOpaque(false);

                }
            }
        });

        super.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

                JFormattedTextField control = (JFormattedTextField) e.getSource();
				control.setText(control.getText());
				control.selectAll();
				control.setBackground(Color.YELLOW);
				control.setOpaque(true);
            }

            @Override
            public void focusLost(FocusEvent e) {

                JFormattedTextField control = (JFormattedTextField) e.getSource();
                control.setBackground(Color.WHITE);
                control.setOpaque(true);

            }
        });
    }//Fin de Constructor    
}//Fin de Clase Principal
