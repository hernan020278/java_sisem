package com.comun.utilidad.swing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Util;

public class PanelImgReloj extends JPanel {

    private int horas;
    private int minutos;
    private int segundos;
    private static final int espacio = 0;
    private static final float tresPi = (float) (3.0 * Math.PI);
    private static final float rad = (float) (Math.PI / 30.0);
    private int tamano;
    private int xCentro;
    private int yCentro;
    private Color colorManHor = new Color(186,142,56);
    private Color colorManMin = new Color(232,213,135);
    private Color colorManSeg = new Color(255, 0, 0);
    /*
     *
     */
    private BufferedImage muestra;
    private javax.swing.Timer t;
    /*
     *
     */
    private URL urlClassPath;
    private ImageIcon miImagenIcono;
    private Image miImagen;
    public static Graphics2D g2d;
    /*
     *
     */
    private TitledBorder bordeTitulo;

    public PanelImgReloj() {
        t = new javax.swing.Timer(1000, new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                update();

            }
        });
    }

    public void update() {
        this.repaint();
    }

    public void start() {
        t.start();
    }

    public void stop() {
        t.stop();
    }

    @Override
    public void paint(Graphics g) {

        bordeTitulo = BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED), "");
        super.setBorder(bordeTitulo);

        g2d = (Graphics2D) g;
        super.paint(g2d);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        int ancho = this.getWidth();
        int alto = this.getHeight();

        tamano = ((ancho < alto) ? ancho : alto) - (2 * espacio);
        xCentro = tamano / 2 + espacio;
        yCentro = tamano / 2 + espacio;

        if (muestra == null || muestra.getWidth() != ancho || muestra.getHeight() != alto) {

            muestra = (BufferedImage) (this.createImage(ancho, alto));
            Graphics2D gc = muestra.createGraphics();
            gc.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            caraReloj(gc);

        }

        Calendar now = Calendar.getInstance();
        horas = now.get(Calendar.HOUR);
        minutos = now.get(Calendar.MINUTE);
        segundos = now.get(Calendar.SECOND);

        g2d.drawImage(muestra, null, 0, 0);

        Manecillas(g2d);
    }

    private void caraReloj(Graphics2D g) {

        miImagenIcono = Util.getInst().obtenerImagenIconArchivo(AplicacionGeneral.getInstance().obtenerRutaImagenes() + "reloj.gif");
        miImagen = miImagenIcono.getImage();

        if (miImagen != null) {

            g.drawImage(miImagen, espacio, espacio, tamano, tamano, this);

        }//Fin de Boque Verdadero si hay alguna imagen para dibujar

        /*        for (int seg = 0; seg < 60; seg++) {
        int inicio;
        if (seg % 5 == 0) {
        inicio = tamano / 2 - 10;
        } else {
        inicio = tamano / 2 - 5;
        }
        diseno(g, xCentro, yCentro, rad * seg, inicio, tamano / 2);
        }*/
    }

    private void Manecillas(Graphics2D g) {

        int radioSegundero = tamano / 3;
        int radioMinutero = radioSegundero * 4 / 4;
        int radioHora = radioSegundero * 2 / 4;

        float fsegundos = segundos;
        float anguloSegundero = tresPi - (rad * fsegundos);
        disenoSegundo(g, xCentro, yCentro, anguloSegundero, 0, radioSegundero);

        float fminutos = (float) (minutos + fsegundos / 60.0);
        float anguloMinutero = tresPi - (rad * fminutos);
        disenoMinuto(g, xCentro, yCentro, anguloMinutero, 0, radioMinutero);

        float fhours = (float) (horas + fminutos / 60.0);
        float anguloHora = tresPi - (5 * rad * fhours);
        disenoHora(g, xCentro, yCentro, anguloHora, 0, radioHora);
    }

    private void disenoHora(Graphics2D g, int x, int y, double angulo, int minRadius, int maxRadius) {

        float sine = (float) Math.sin(angulo);

        float cosine = (float) Math.cos(angulo);

        int dxmin = (int) (minRadius * sine);
        int dymin = (int) (minRadius * cosine);

        int dxmax = (int) (maxRadius * sine);
        int dymax = (int) (maxRadius * cosine);

        BasicStroke LinAncha = new BasicStroke(7.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        g.setStroke(LinAncha);
        g.setColor(colorManHor);
        g.drawLine(x + dxmin, y + dymin, x + dxmax, y + dymax);
    }

    private void disenoMinuto(Graphics2D g, int x, int y, double angulo, int minRadius, int maxRadius) {

        float sine = (float) Math.sin(angulo);

        float cosine = (float) Math.cos(angulo);

        int dxmin = (int) (minRadius * sine);
        int dymin = (int) (minRadius * cosine);

        int dxmax = (int) (maxRadius * sine);
        int dymax = (int) (maxRadius * cosine);

        BasicStroke LinAncha = new BasicStroke(3.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        g.setStroke(LinAncha);
        g.setColor(colorManMin);
        g.drawLine(x + dxmin, y + dymin, x + dxmax, y + dymax);
    }

    private void disenoSegundo(Graphics2D g, int x, int y, double angulo, int minRadius, int maxRadius) {

        float sine = (float) Math.sin(angulo);

        float cosine = (float) Math.cos(angulo);

        int dxmin = (int) (minRadius * sine);
        int dymin = (int) (minRadius * cosine);

        int dxmax = (int) (maxRadius * sine);
        int dymax = (int) (maxRadius * cosine);

        BasicStroke LinAncha = new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        g.setStroke(LinAncha);
        g.setColor(colorManSeg);
        g.drawLine(x + dxmin, y + dymin, x + dxmax, y + dymax);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                JFrame fr = new JFrame();
                PanelImgReloj panRel = new PanelImgReloj();
                fr.add(panRel);
                fr.setSize(200, 200);
                fr.setVisible(true);


            }
        });
    }
}// class reloj

