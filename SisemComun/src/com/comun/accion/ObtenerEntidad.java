package com.comun.accion;

import java.util.List;
import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;
import com.comun.utilidad.Util;

public class ObtenerEntidad extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			String entidadNombre = (String) peticion.get("entidadNombre");
			if (peticion.containsKey("consulta") && peticion.containsKey("entidadNombre"))
			{
				peticion.put("respuesta", Respuesta.ENTIDAD);
				List listResult = dbc.ejecutarConsulta(peticion);
				if (listResult != null && listResult.size() > 0)
				{
					result = listResult.get(0);
				}
				if (result == null)
				{
					result = Class.forName("com.comun.entidad." + entidadNombre).newInstance();
					Util.getInst().limpiarEntidad(result,true);
				}
			}

			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{

			this.setEstado(Estado.FAILED);
			throw e;

		}
		return result;
	}

}