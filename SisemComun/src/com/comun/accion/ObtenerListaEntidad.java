package com.comun.accion;

import java.util.Map;

import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Respuesta;

public class ObtenerListaEntidad extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;

		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			if (peticion.containsKey("consulta") && peticion.containsKey("entidadNombre"))
			{
				peticion.put("respuesta", Respuesta.LISTAENTIDAD);
				result = dbc.ejecutarConsulta(peticion);
			}
			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{

			this.setEstado(Estado.FAILED);
			throw e;

		}
		return result;
	}

}