/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comun.entidad;

import java.io.Serializable;

/**
 *
 * @author ROMULO
 */
public class Documento implements EntidadListener {

    private String doc_cod;
    private String doc_nom;
    private String doc_des;
    private String ope_ref;

    public String getDoc_cod() {
        return doc_cod;
    }

    public void setDoc_cod(String doc_cod) {
        this.doc_cod = doc_cod;
    }

    public String getDoc_des() {
        return doc_des;
    }

    public void setDoc_des(String doc_des) {
        this.doc_des = doc_des;
    }

    public String getDoc_nom() {
        return doc_nom;
    }

    public void setDoc_nom(String doc_nom) {
        this.doc_nom = doc_nom;
    }

    public String getOpe_ref() {
        return ope_ref;
    }

    public void setOpe_ref(String ope_ref) {
        this.ope_ref = ope_ref;
    }

    public void setObjeto(Documento parDoc) {

        doc_cod = parDoc.getDoc_cod();
        doc_nom = parDoc.getDoc_nom();
        doc_des = parDoc.getDoc_des();
        ope_ref = parDoc.getOpe_ref();

    }

    public void limpiarInstancia() {

        doc_cod = "";
        ope_ref = "";
        doc_nom = "";
        doc_des = "";

    }

    public void imprimirValores() {

        System.out.println(doc_cod + " : " + ope_ref + " : " + doc_nom + " : " + doc_des);
    }
}
