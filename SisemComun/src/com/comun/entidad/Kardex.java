package com.comun.entidad;

import java.sql.Date;

public class Kardex implements EntidadListener {

    private String kar_cod;
    private String vou_cod;
    private String loc_cod;
    private String ope_cod;
    private String doc_cod;
    private String per_cod;
    private String loc_ref;
    private String kar_tipcon;
    private String kar_nomdoc;
    private String kar_numdoc;
    private String kar_nomcom;
    private String kar_numcom;
    private double kar_imp;
    private Date kar_fecemi;
    private Date kar_fectras;
    private String kar_est;
    private String kar_destino;
    private String kar_destruc;
    private String kar_domlleg;
    private String kar_dompar;
    private String kar_marpla;
    private String kar_cerins;
    private String kar_liccon;
    private String kar_guia;
    private String kar_factura;
    private String kar_ref;

    public void setObjeto(Kardex parObj) {

        kar_cod = parObj.getKar_cod();
        vou_cod = parObj.getVou_cod();
        loc_cod = parObj.getLoc_cod();
        ope_cod = parObj.getOpe_cod();
        doc_cod = parObj.getDoc_cod();
        per_cod = parObj.getPer_cod();
        loc_ref = parObj.getLoc_ref();
        kar_tipcon = parObj.getKar_tipcon();
        kar_nomdoc = parObj.getKar_nomdoc();
        kar_numdoc = parObj.getKar_numdoc();
        kar_nomcom = parObj.getKar_nomcom();
        kar_numcom = parObj.getKar_numcom();
        kar_imp = parObj.getKar_imp();
        kar_fecemi = parObj.getKar_fecemi();
        kar_fectras = parObj.getKar_fectras();
        kar_est = parObj.getKar_est();
        kar_destino = parObj.getKar_destino();
        kar_destruc = parObj.getKar_destruc();
        kar_domlleg = parObj.getKar_domlleg();
        kar_dompar = parObj.getKar_dompar();
        kar_marpla = parObj.getKar_marpla();
        kar_cerins = parObj.getKar_cerins();
        kar_liccon = parObj.getKar_liccon();
        kar_guia = parObj.getKar_guia();
        kar_factura = parObj.getKar_factura();
        kar_ref = parObj.getKar_ref();

    }//Fin de limpiarInstancia()

    public void imprimirValores() {

        System.out.println(kar_cod + " : " + vou_cod + " : " + loc_cod + " : " + ope_cod + " : " + doc_cod + " : "
                + per_cod + " : " + loc_ref + " : " + kar_tipcon + " : "
                + kar_nomdoc + " : " + kar_numdoc + " : " + kar_nomcom + " : " + kar_numcom + " : " + kar_imp + " : "
                + kar_fecemi + " : " + kar_fectras + " : " + kar_est + " : " + kar_destino + " : " + kar_destruc + " : "
                + kar_domlleg + " : " + kar_dompar + " : " + kar_marpla + " : " + kar_cerins + " : " + kar_liccon + " : "
                + kar_guia + " : " + kar_factura + " : " + kar_ref);

    }

    public void limpiarInstancia() {

        kar_cod = "";
        vou_cod = "";
        loc_cod = "";
        ope_cod = "";
        doc_cod = "";
        per_cod = "";
        loc_ref = "";
        kar_tipcon = "";
        kar_nomdoc = "";
        kar_numdoc = "";
        kar_nomcom = "";
        kar_numcom = "";
        kar_imp = 0.00;
        kar_fecemi = null;
        kar_fectras = null;
        kar_est = "";
        kar_destino = "";
        kar_destruc = "";
        kar_domlleg = "";
        kar_dompar = "";
        kar_marpla = "";
        kar_cerins = "";
        kar_liccon = "";
        kar_guia = "";
        kar_factura = "";
        kar_ref = "";

    }//Fin de limpiarInstancia()

    public String getKar_nomdoc() {
        return kar_nomdoc;
    }

    public void setKar_nomdoc(String kar_nomdoc) {
        this.kar_nomdoc = kar_nomdoc;
    }

    public String getKar_nomcom() {
        return kar_nomcom;
    }

    public void setKar_nomcom(String kar_nomcom) {
        this.kar_nomcom = kar_nomcom;
    }

    public String getKar_numcom() {
        return kar_numcom;
    }

    public void setKar_numcom(String kar_numcom) {
        this.kar_numcom = kar_numcom;
    }

    public String getDoc_cod() {
        return doc_cod;
    }

    public void setDoc_cod(String doc_cod) {
        this.doc_cod = doc_cod;
    }

    public String getKar_cerins() {
        return kar_cerins;
    }

    public void setKar_cerins(String kar_cerins) {
        this.kar_cerins = kar_cerins;
    }

    public String getKar_cod() {
        return kar_cod;
    }

    public void setKar_cod(String kar_cod) {
        this.kar_cod = kar_cod;
    }

    public String getKar_destino() {
        return kar_destino;
    }

    public void setKar_destino(String kar_destino) {
        this.kar_destino = kar_destino;
    }

    public String getKar_destruc() {
        return kar_destruc;
    }

    public void setKar_destruc(String kar_destruc) {
        this.kar_destruc = kar_destruc;
    }

    public String getKar_domlleg() {
        return kar_domlleg;
    }

    public void setKar_domlleg(String kar_domlleg) {
        this.kar_domlleg = kar_domlleg;
    }

    public String getKar_dompar() {
        return kar_dompar;
    }

    public void setKar_dompar(String kar_dompar) {
        this.kar_dompar = kar_dompar;
    }

    public String getKar_est() {
        return kar_est;
    }

    public void setKar_est(String kar_est) {
        this.kar_est = kar_est;
    }

    public Date getKar_fecemi() {
        return kar_fecemi;
    }

    public void setKar_fecemi(Date kar_fecemi) {
        this.kar_fecemi = kar_fecemi;
    }

    public Date getKar_fectras() {
        return kar_fectras;
    }

    public void setKar_fectras(Date kar_fectras) {
        this.kar_fectras = kar_fectras;
    }

    public double getKar_imp() {
        return kar_imp;
    }

    public void setKar_imp(double kar_imp) {
        this.kar_imp = kar_imp;
    }

    public String getKar_liccon() {
        return kar_liccon;
    }

    public void setKar_liccon(String kar_liccon) {
        this.kar_liccon = kar_liccon;
    }

    public String getKar_marpla() {
        return kar_marpla;
    }

    public void setKar_marpla(String kar_marpla) {
        this.kar_marpla = kar_marpla;
    }

    public String getKar_numdoc() {
        return kar_numdoc;
    }

    public void setKar_numdoc(String kar_numdoc) {
        this.kar_numdoc = kar_numdoc;
    }

    public String getKar_tipcon() {
        return kar_tipcon;
    }

    public void setKar_tipcon(String kar_tipcon) {
        this.kar_tipcon = kar_tipcon;
    }

    public String getLoc_cod() {
        return loc_cod;
    }

    public void setLoc_cod(String loc_cod) {
        this.loc_cod = loc_cod;
    }

    public String getLoc_ref() {
        return loc_ref;
    }

    public void setLoc_ref(String loc_ref) {
        this.loc_ref = loc_ref;
    }

    public String getOpe_cod() {
        return ope_cod;
    }

    public void setOpe_cod(String ope_cod) {
        this.ope_cod = ope_cod;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public String getVou_cod() {
        return vou_cod;
    }

    public void setVou_cod(String vou_cod) {
        this.vou_cod = vou_cod;
    }

    public String getKar_factura() {
        return kar_factura;
    }

    public void setKar_factura(String kar_factura) {
        this.kar_factura = kar_factura;
    }

    public String getKar_guia() {
        return kar_guia;
    }

    public void setKar_guia(String kar_guia) {
        this.kar_guia = kar_guia;
    }

    public String getKar_ref() {
        return kar_ref;
    }

    public void setKar_ref(String kar_ref) {
        this.kar_ref = kar_ref;
    }
}//Fin de Clase Principal
