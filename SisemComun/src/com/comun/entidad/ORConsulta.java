package com.comun.entidad;

import java.io.Serializable;
import java.sql.Date;

public class ORConsulta implements EntidadListener {

    private int con_cod;
    private String enc_cod;
    private Date con_fec;
    private String con_diag;

    public int getCon_cod() {
        return con_cod;
    }

    public void setCon_cod(int con_cod) {
        this.con_cod = con_cod;
    }

    public String getCon_diag() {
        return con_diag;
    }

    public void setCon_diag(String con_diag) {
        this.con_diag = con_diag;
    }

    public Date getCon_fec() {
        return con_fec;
    }

    public void setCon_fec(Date con_fec) {
        this.con_fec = con_fec;
    }

    public String getEnc_cod() {
        return enc_cod;
    }

    public void setEnc_cod(String enc_cod) {
        this.enc_cod = enc_cod;
    }
}
