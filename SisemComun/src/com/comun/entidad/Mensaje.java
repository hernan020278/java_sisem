package com.comun.entidad;

public class Mensaje implements EntidadListener {

	private boolean mostrar;
	private String titulo;
	private String tipo;
	private String valor;

	public void setObjeto(Mensaje parObj) {
		if (parObj != null) {
			mostrar = parObj.isMostrar();
			titulo = parObj.getTitulo();
			tipo = parObj.getTipo();
			valor = parObj.getValor();
		}
	}// in de setObjeto

	public boolean isMostrar() {
		return mostrar;
	}

	public void setMostrar(boolean mostrar) {
		this.mostrar = mostrar;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}// Fin de Clase principal
