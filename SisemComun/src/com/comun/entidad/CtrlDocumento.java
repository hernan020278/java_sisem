package com.comun.entidad;

import java.io.File;
import java.io.FileInputStream;
import javax.swing.ImageIcon;

public class CtrlDocumento implements EntidadListener {

	private int ctrldoc_cod;
	private String loc_cod;
	private String doc_cod;
	private int ctrldoc_serie;

	public int getCtrldoc_cod() {
		return ctrldoc_cod;
	}

	public void setCtrldoc_cod(int ctrldoc_cod) {
		this.ctrldoc_cod = ctrldoc_cod;
	}

	public String getLoc_cod() {
		return loc_cod;
	}

	public void setLoc_cod(String loc_cod) {
		this.loc_cod = loc_cod;
	}

	public String getDoc_cod() {
		return doc_cod;
	}

	public void setDoc_cod(String doc_cod) {
		this.doc_cod = doc_cod;
	}

	public int getCtrldoc_serie() {
		return ctrldoc_serie;
	}

	public void setCtrldoc_serie(int ctrldoc_serie) {
		this.ctrldoc_serie = ctrldoc_serie;
	}

	public void imprimirValores() {

		System.out.println(ctrldoc_cod + " : " + loc_cod + " : " + doc_cod + " : " + ctrldoc_serie);

	}

	public void setObjeto(CtrlDocumento parObj) {

		ctrldoc_cod = parObj.getCtrldoc_cod();
		loc_cod = parObj.getLoc_cod();
		doc_cod = parObj.getDoc_cod();
		ctrldoc_serie = parObj.getCtrldoc_serie();

	}// Fin de setObjeto()

	public void limpiarInstancia() {

		ctrldoc_cod = 0;
		loc_cod = "";
		doc_cod = "";
		ctrldoc_serie = 0;

	}
}
