package com.comun.entidad;

public class Red implements EntidadListener {

	private String redcodigo;
	private String tracodigo;
	private String sincodigo;
	private String estado;
	private int version;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getRedcodigo() {
		return redcodigo;
	}

	public void setRedcodigo(String redcodigo) {
		this.redcodigo = redcodigo;
	}

	public String getTracodigo() {
		return tracodigo;
	}

	public void setTracodigo(String tracodigo) {
		this.tracodigo = tracodigo;
	}

	public String getSincodigo() {
		return sincodigo;
	}

	public void setSincodigo(String sincodigo) {
		this.sincodigo = sincodigo;
	}
}
