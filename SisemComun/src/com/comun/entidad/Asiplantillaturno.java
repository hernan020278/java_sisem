package com.comun.entidad;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;

@Getter
@Setter
public class Asiplantillaturno implements EntidadListener {

  private BigDecimal kyplantillaturno;
  private BigDecimal kyplantillahorario;
  private String horarionombre;
  private String turnonombre;
  private String turnodia;
  private String turnotipo;
  private Time entrada;
  private Time limentradauno;
  private Time limentradados;
  private Time limentrada;
  private Time limsalidauno;
  private Time limsalidados;
  private Time limsalida;
  private Time salida;
	private String estado;
	private Timestamp version;
}
