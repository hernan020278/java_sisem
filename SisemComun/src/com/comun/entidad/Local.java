package com.comun.entidad;

import java.io.Serializable;

public class Local implements EntidadListener {

    private String loc_cod;
    private String emp_cod;
    private String loc_mac;
    private String loc_tip;
    private String loc_des;
    private String loc_dir;
    private String loc_tel;

    public String getEmp_cod() {
        return emp_cod;
    }

    public void setEmp_cod(String emp_cod) {
        this.emp_cod = emp_cod;
    }

    public String getLoc_cod() {
        return loc_cod;
    }

    public void setLoc_cod(String loc_cod) {
        this.loc_cod = loc_cod;
    }

    public String getLoc_des() {
        return loc_des;
    }

    public void setLoc_des(String loc_des) {
        this.loc_des = loc_des;
    }

    public String getLoc_dir() {
        return loc_dir;
    }

    public void setLoc_dir(String loc_dir) {
        this.loc_dir = loc_dir;
    }

    public String getLoc_mac() {
        return loc_mac;
    }

    public void setLoc_mac(String loc_mac) {
        this.loc_mac = loc_mac;
    }

    public String getLoc_tel() {
        return loc_tel;
    }

    public void setLoc_tel(String loc_tel) {
        this.loc_tel = loc_tel;
    }

    public String getLoc_tip() {
        return loc_tip;
    }

    public void setLoc_tip(String loc_tip) {
        this.loc_tip = loc_tip;
    }

    public void setObjeto(Local parObj) {

        this.loc_cod = parObj.getLoc_cod();
        this.emp_cod = parObj.getEmp_cod();
        this.loc_mac = parObj.getLoc_mac();
        this.loc_tip = parObj.getLoc_tip();
        this.loc_des = parObj.getLoc_des();
        this.loc_dir = parObj.getLoc_dir();
        this.loc_tel = parObj.getLoc_tel();

    }

    public void imprimirValores() {

        System.out.print("Local : " + loc_cod);
        System.out.print(" - Empresa : " + emp_cod);
        System.out.print(" - Mac : " + loc_mac);
        System.out.print(" - Tipo : " + loc_tip);
        System.out.print(" - Descripcion : " + loc_des);
        System.out.print(" - Direccion : " + loc_dir);
        System.out.print(" - Telefono : " + loc_tel);
        System.out.println("-");

    }

    public void limpiarInstancia() {

        loc_cod = "";
        emp_cod = "";
        loc_mac = "";
        loc_tip = "";
        loc_des = "";
        loc_dir = "";
        loc_tel = "";

    }
}
