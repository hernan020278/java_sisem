package com.comun.entidad;

public class DetPedido  implements EntidadListener {

	private int detped_cod;
	private int ped_cod;
	private int prd_cod;
	private int gru_cod;
	private int detped_canped;
	private int detped_canfab;
	private int detped_canent;
	private double detped_pre;
	private double detped_imp;
	private String detped_est;
	private int detped_ver;

	public void setObjeto(DetPedido parObj) {

		detped_cod = parObj.getDetped_cod();
		ped_cod = parObj.getPed_cod();
		prd_cod = parObj.getPrd_cod();
		gru_cod = parObj.getGru_cod();
		detped_canped = parObj.getDetped_canped();
		detped_canfab = parObj.getDetped_canfab();
		detped_canent = parObj.getDetped_canent();
		detped_pre = parObj.getDetped_pre();
		detped_imp = parObj.getDetped_imp();
		detped_est = parObj.getDetped_est();
		detped_ver = parObj.getDetped_ver();

	}// Fin de public void setObjeto()

	public void imprimirValores() {

		System.out.println(
				detped_cod + " : "
						+ ped_cod + " : "
						+ prd_cod + " : "
						+ gru_cod + " : "
						+ detped_canped + " : "
						+ detped_canfab + " : "
						+ detped_canent + " : "
						+ detped_pre + " : "
						+ detped_imp + " : "
						+ detped_est + " : "
						+ detped_ver);
	}

	public void limpiarInstancia() {

		detped_cod = 0;
		ped_cod = 0;
		prd_cod = 0;
		gru_cod = 0;
		detped_canped = 0;
		detped_canfab = 0;
		detped_canent = 0;
		detped_pre = 0.00;
		detped_imp = 0.00;
		detped_est = "";
		detped_ver = 0;

	}// Fin de limpiarInstancia()

	public int getGru_cod() {
		return gru_cod;
	}

	public void setGru_cod(int gru_cod) {
		this.gru_cod = gru_cod;
	}

	public int getPrd_cod() {
		return prd_cod;
	}

	public void setPrd_cod(int prd_cod) {
		this.prd_cod = prd_cod;
	}

	public int getPed_cod() {
		return ped_cod;
	}

	public void setPed_cod(int ped_cod) {
		this.ped_cod = ped_cod;
	}

	public int getDetped_cod() {
		return detped_cod;
	}

	public void setDetped_cod(int detped_cod) {
		this.detped_cod = detped_cod;
	}

	public int getDetped_canped() {
		return detped_canped;
	}

	public void setDetped_canped(int detped_canped) {
		this.detped_canped = detped_canped;
	}

	public int getDetped_canfab() {
		return detped_canfab;
	}

	public void setDetped_canfab(int detped_canfab) {
		this.detped_canfab = detped_canfab;
	}

	public int getDetped_canent() {
		return detped_canent;
	}

	public void setDetped_canent(int detped_canent) {
		this.detped_canent = detped_canent;
	}

	public double getDetped_pre() {
		return detped_pre;
	}

	public void setDetped_pre(double detped_pre) {
		this.detped_pre = detped_pre;
	}

	public double getDetped_imp() {
		return detped_imp;
	}

	public void setDetped_imp(double detped_imp) {
		this.detped_imp = detped_imp;
	}

	public int getDetped_ver() {
		return detped_ver;
	}

	public void setDetped_ver(int detped_ver) {
		this.detped_ver = detped_ver;
	}

	public String getDetped_est() {
		return detped_est;
	}

	public void setDetped_est(String detped_est) {
		this.detped_est = detped_est;
	}

}
