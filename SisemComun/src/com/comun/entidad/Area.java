package com.comun.entidad;

import java.io.File;
import java.io.FileInputStream;

import javax.swing.ImageIcon;

public class Area  implements EntidadListener {

	private int are_cod;
	private String are_ide;
	private String are_nom;
	private String are_des;
	private String are_bar;
	private boolean are_act;
	private int are_ord;
	private int are_ver;

	public void setObjeto(Area parObj) {

		are_cod = parObj.getAre_cod();
		are_ide = parObj.getAre_ide();
		are_nom = parObj.getAre_nom();
		are_des = parObj.getAre_des();
		are_bar = parObj.getAre_bar();
		are_act = parObj.isAre_act();
		are_ord = parObj.getAre_ord();
		are_ver = parObj.getAre_ver();

	}// Fin de public void setObjeto()

	public void imprimirValores() {

		System.out.println(are_cod + " : " +
				are_ide + " : " +
				are_nom + " : " +
				are_des + " : " +
				are_bar + " : " +
				are_act + " : " +
				are_ord + " : " +
				are_ver);
	}

	public void limpiarInstancia() {

		are_cod = 0;
		are_ide = "";
		are_nom = "";
		are_des = "";
		are_bar = "";
		are_act = true;
		are_ord = 0;
		are_ver = 0;

	}// Fin de limpiarInstancia()


	
	public String getAre_ide() {
		return are_ide;
	}

	public void setAre_ide(String are_ide) {
		this.are_ide = are_ide;
	}

	public boolean isAre_act() {
		return are_act;
	}

	public void setAre_act(boolean are_act) {
		this.are_act = are_act;
	}

	public int getAre_ord() {
		return are_ord;
	}

	public void setAre_ord(int are_ord) {
		this.are_ord = are_ord;
	}

	public int getAre_cod() {
		return are_cod;
	}

	public void setAre_cod(int are_cod) {
		this.are_cod = are_cod;
	}

	public String getAre_nom() {
		return are_nom;
	}

	public void setAre_nom(String are_nom) {
		this.are_nom = are_nom;
	}

	public String getAre_des() {
		return are_des;
	}

	public void setAre_des(String are_des) {
		this.are_des = are_des;
	}

	public int getAre_ver() {
		return are_ver;
	}

	public void setAre_ver(int are_ver) {
		this.are_ver = are_ver;
	}

	public String getAre_bar() {
		return are_bar;
	}

	
	public void setAre_bar(String are_bar) {
		this.are_bar = are_bar;
	}

}
