package com.comun.entidad;

public class ORPersonal implements EntidadListener {

    private String per_cod;
    private String per_tip;
    private String per_nom;
    private String per_ape;
    private String per_dni;
    private String per_dir;
    private String per_cel;
    private double per_rem;
    private double per_afp;
    private String per_usu;
    private String per_pas;
    private String per_est;
    private String per_fot;

    public double getPer_afp() {
        return per_afp;
    }

    public void setPer_afp(double per_afp) {
        this.per_afp = per_afp;
    }

    public String getPer_ape() {
        return per_ape;
    }

    public void setPer_ape(String per_ape) {
        this.per_ape = per_ape;
    }

    public String getPer_cel() {
        return per_cel;
    }

    public void setPer_cel(String per_cel) {
        this.per_cel = per_cel;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public String getPer_dir() {
        return per_dir;
    }

    public void setPer_dir(String per_dir) {
        this.per_dir = per_dir;
    }

    public String getPer_dni() {
        return per_dni;
    }

    public void setPer_dni(String per_dni) {
        this.per_dni = per_dni;
    }

    public String getPer_est() {
        return per_est;
    }

    public void setPer_est(String per_est) {
        this.per_est = per_est;
    }

    public String getPer_fot() {
        return per_fot;
    }

    public void setPer_fot(String per_fot) {
        this.per_fot = per_fot;
    }

    public String getPer_nom() {
        return per_nom;
    }

    public void setPer_nom(String per_nom) {
        this.per_nom = per_nom;
    }

    public String getPer_pas() {
        return per_pas;
    }

    public void setPer_pas(String per_pas) {
        this.per_pas = per_pas;
    }

    public double getPer_rem() {
        return per_rem;
    }

    public void setPer_rem(double per_rem) {
        this.per_rem = per_rem;
    }

    public String getPer_tip() {
        return per_tip;
    }

    public void setPer_tip(String per_tip) {
        this.per_tip = per_tip;
    }

    public String getPer_usu() {
        return per_usu;
    }

    public void setPer_usu(String per_usu) {
        this.per_usu = per_usu;
    }

}
