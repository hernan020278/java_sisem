package com.comun.entidad;

public class Grupo  implements EntidadListener {

	private int gru_cod;
	private String gru_tipo;
	private String gru_nom;
	private String gru_des;
	private int gru_supuno;
	private int gru_supdos;
	private int gru_suptres;
	private int gru_supcua;
	private int gru_nivel;
	private int gru_ver;

	public void setObjeto(Grupo parObj) {

		gru_cod = parObj.getGru_cod();
		gru_tipo = parObj.getGru_tipo();
		gru_nom = parObj.getGru_nom();
		gru_des = parObj.getGru_des();
		gru_supuno = parObj.getGru_supuno();
		gru_supdos = parObj.getGru_supdos();
		gru_suptres = parObj.getGru_suptres();
		gru_supcua = parObj.getGru_supcua();
		gru_nivel = parObj.getGru_nivel();
		gru_ver = parObj.getGru_ver();

	}// Fin de public void setObjeto()

	public void imprimirValores() {

		System.out.println(gru_cod + " : " + gru_tipo + " : " + gru_nom + " : " + gru_des + " : "
				+ gru_supuno + " : " + gru_supdos+ " : " + gru_suptres + " : " + gru_supcua + " : "
				+ gru_nivel + " : " + gru_ver);
	}

	public void limpiarInstancia() {

		gru_cod = 0;
		gru_tipo = "";
		gru_nom = "";
		gru_des = "";
		gru_supuno = 0;
		gru_supdos = 0;
		gru_suptres = 0;
		gru_supcua = 0;
		gru_nivel = 0;
		gru_ver = 0;

	}// Fin de limpiarInstancia()

	public int getGru_cod() {
		return gru_cod;
	}

	public void setGru_cod(int gru_cod) {
		this.gru_cod = gru_cod;
	}

	public String getGru_tipo() {
		return gru_tipo;
	}

	public void setGru_tipo(String gru_tipo) {
		this.gru_tipo = gru_tipo;
	}

	public String getGru_nom() {
		return gru_nom;
	}

	public void setGru_nom(String gru_nom) {
		this.gru_nom = gru_nom;
	}

	public String getGru_des() {
		return gru_des;
	}

	public void setGru_des(String gru_des) {
		this.gru_des = gru_des;
	}

	public int getGru_supuno() {
		return gru_supuno;
	}

	public void setGru_supuno(int gru_supuno) {
		this.gru_supuno = gru_supuno;
	}

	public int getGru_supdos() {
		return gru_supdos;
	}

	public void setGru_supdos(int gru_supdos) {
		this.gru_supdos = gru_supdos;
	}

	public int getGru_suptres() {
		return gru_suptres;
	}

	public void setGru_suptres(int gru_suptres) {
		this.gru_suptres = gru_suptres;
	}

	public int getGru_supcua() {
		return gru_supcua;
	}

	public void setGru_supcua(int gru_supcua) {
		this.gru_supcua = gru_supcua;
	}

	public int getGru_nivel() {
		return gru_nivel;
	}

	public void setGru_nivel(int gru_nivel) {
		this.gru_nivel = gru_nivel;
	}

	public int getGru_ver() {
		return gru_ver;
	}

	public void setGru_ver(int gru_ver) {
		this.gru_ver = gru_ver;
	}

}
