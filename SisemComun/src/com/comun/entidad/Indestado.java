package com.comun.entidad;

public class Indestado implements EntidadListener {

	private String idecodigo;
	private String indcodigo;
	private String nombre;
	private String estado;
	private int version;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getIdecodigo() {
		return idecodigo;
	}

	public void setIdecodigo(String idecodigo) {
		this.idecodigo = idecodigo;
	}

	public String getIndcodigo() {
		return indcodigo;
	}

	public void setIndcodigo(String indcodigo) {
		this.indcodigo = indcodigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
