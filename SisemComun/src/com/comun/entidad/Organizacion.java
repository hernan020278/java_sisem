package com.comun.entidad;

import java.sql.Date;


public class Organizacion implements EntidadListener {

	private String orgcodigo;
	private Date fechafinal;
	private String nombre;
	private String estado;
	private int version;

	public void setObjeto(Organizacion parObj) {
		if (parObj != null) {
			orgcodigo = parObj.getOrgcodigo();
			fechafinal = parObj.getFechafinal();
			nombre = parObj.getNombre();
			estado = parObj.getEstado();
			version = parObj.getVersion();
		}
	}

	public String getOrgcodigo() {
		return orgcodigo;
	}

	public void setOrgcodigo(String orgcodigo) {
		this.orgcodigo = orgcodigo;
	}

	public Date getFechafinal() {
		return fechafinal;
	}

	public void setFechafinal(Date fechafinal) {
		this.fechafinal = fechafinal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}// Fin de setObjeto

}// Fin de Clase principal
