/**
 *
 */
package com.comun.entidad;

import java.io.Serializable;

import com.comun.utilidad.Util;

/**
 * @author kelli
 */

public class Etiqueta implements EntidadListener {
	/** identifier field */
	private String eticodigo;
	private String campo;
	private String acceso;
	private String modulo;
	private String tipo;
	private String valor;
	private String abreviacion;
	private String validar;
	private String nivelvalidar;
	private String mensajevalidar;
	private String linkvalidar;
	private String permitirvalidar;
	private String browse;
	private String editable;
	private String visible;
	private String requerido;
	private String sololectura;
	private String mensajehover;
	private int tamano;
	private String campotipo;
	private String campotabla;
	private String estado;
	private String idioma;
	private int version;

	public void setObjeto(Etiqueta parObj) {
		if (parObj != null) {
			eticodigo = parObj.getEticodigo();
			campo = parObj.getCampo();
			acceso = parObj.getAcceso();
			modulo = parObj.getModulo();
			tipo = parObj.getTipo();
			valor = parObj.getValor();
			abreviacion = parObj.getAbreviacion();
			validar = parObj.getValidar();
			nivelvalidar = parObj.getNivelvalidar();
			mensajevalidar = parObj.getMensajevalidar();
			linkvalidar = parObj.getLinkvalidar();
			permitirvalidar = parObj.getPermitirvalidar();
			browse = parObj.getBrowse();
			editable = parObj.getEditable();
			visible = parObj.getVisible();
			requerido = parObj.getRequerido();
			sololectura = parObj.getSololectura();
			mensajehover = parObj.getMensajehover();
			tamano = parObj.getTamano();
			campotipo = parObj.getCampotipo();
			campotabla = parObj.getCampotabla();
			estado = parObj.getEstado();
			idioma = parObj.getIdioma();
			version = parObj.getVersion();
		}
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getEticodigo() {
		return eticodigo;
	}

	public void setEticodigo(String eticodigo) {
		this.eticodigo = eticodigo;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getAcceso() {
		return acceso;
	}

	public void setAcceso(String acceso) {
		this.acceso = acceso;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getAbreviacion() {
		return abreviacion;
	}

	public void setAbreviacion(String abreviacion) {
		this.abreviacion = abreviacion;
	}

	public String getValidar() {
		return validar;
	}

	public void setValidar(String validar) {
		this.validar = validar;
	}

	public String getNivelvalidar() {
		return nivelvalidar;
	}

	public void setNivelvalidar(String nivelvalidar) {
		this.nivelvalidar = nivelvalidar;
	}

	public String getMensajevalidar() {
		return mensajevalidar;
	}

	public void setMensajevalidar(String mensajevalidar) {
		this.mensajevalidar = mensajevalidar;
	}

	public String getLinkvalidar() {
		return linkvalidar;
	}

	public void setLinkvalidar(String linkvalidar) {
		this.linkvalidar = linkvalidar;
	}

	public String getPermitirvalidar() {
		return permitirvalidar;
	}

	public void setPermitirvalidar(String permitirvalidar) {
		this.permitirvalidar = permitirvalidar;
	}

	public String getBrowse() {
		return browse;
	}

	public void setBrowse(String browse) {
		this.browse = browse;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getRequerido() {
		return requerido;
	}

	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}

	public String getSololectura() {
		return sololectura;
	}

	public void setSololectura(String sololectura) {
		this.sololectura = sololectura;
	}

	public String getMensajehover() {
		return mensajehover;
	}

	public void setMensajehover(String mensajehover) {
		this.mensajehover = mensajehover;
	}

	public int getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	public String getCampotipo() {
		return campotipo;
	}

	public void setCampotipo(String campotipo) {
		this.campotipo = campotipo;
	}

	public String getCampotabla() {
		return campotabla;
	}

	public void setCampotabla(String campotabla) {
		this.campotabla = campotabla;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}// in de setObjeto

}
