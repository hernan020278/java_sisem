package com.comun.entidad;

import java.sql.Date;
import java.sql.Time;

public class ORSuceso implements EntidadListener {

    private String ctrl_cod;
    private String per_cod;
    private String horcod;
    private String suc_tip;
    private String suc_cod;
    private Date suc_fecreg;
    private Time suc_horreg;
    private String suc_des;
    private String suc_obs;
    private String suc_est;

    public String getCtrl_cod() {
        return ctrl_cod;
    }

    public void setCtrl_cod(String ctrl_cod) {
        this.ctrl_cod = ctrl_cod;
    }

    public String getHor_cod() {
        return horcod;
    }

    public void setHor_cod(String horcod) {
        this.horcod = horcod;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public String getSuc_cod() {
        return suc_cod;
    }

    public void setSuc_cod(String suc_cod) {
        this.suc_cod = suc_cod;
    }

    public String getSuc_des() {
        return suc_des;
    }

    public void setSuc_des(String suc_des) {
        this.suc_des = suc_des;
    }

    public String getSuc_est() {
        return suc_est;
    }

    public void setSuc_est(String suc_est) {
        this.suc_est = suc_est;
    }

    public Date getSuc_fecreg() {
        return suc_fecreg;
    }

    public void setSuc_fecreg(Date suc_fecreg) {
        this.suc_fecreg = suc_fecreg;
    }

    public Time getSuc_horreg() {
        return suc_horreg;
    }

    public void setSuc_horreg(Time suc_horreg) {
        this.suc_horreg = suc_horreg;
    }

    public String getSuc_obs() {
        return suc_obs;
    }

    public void setSuc_obs(String suc_obs) {
        this.suc_obs = suc_obs;
    }

    public String getSuc_tip() {
        return suc_tip;
    }

    public void setSuc_tip(String suc_tip) {
        this.suc_tip = suc_tip;
    }
}
