package com.comun.entidad;

public class Agrupacion implements EntidadListener {

	private String segcodigo;
	private String usuario;
	private int version;

	public void setObjeto(Agrupacion parObj) {

		usuario = parObj.getUsuario();
		segcodigo = parObj.getSegcodigo();
		version = parObj.getVersion();

	}// Fin de setObjeto

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSegcodigo() {
		return segcodigo;
	}

	public void setSegcodigo(String segcodigo) {
		this.segcodigo = segcodigo;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}// Fin de Clase principal
