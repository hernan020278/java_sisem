package com.comun.entidad;

import java.io.Serializable;

public class Serie implements EntidadListener {

    private int ser_cod;
    private int cat_cod;
    private String ser_nom;

    public void setObjeto(Serie parObj) {

        this.ser_cod = parObj.getSer_cod();
        this.cat_cod = parObj.getCat_cod();
        this.ser_nom = parObj.getSer_nom();
    }

    public void imprimirValores() {

        System.out.println(ser_cod + " : " + cat_cod + " : " + ser_nom);

    }

    public void limpiarInstancia() {

        ser_cod = 0;
        cat_cod = 0;
        ser_nom = "";

    }

    public int getSer_cod() {
        return ser_cod;
    }

    public void setSer_cod(int ser_cod) {
        this.ser_cod = ser_cod;
    }

    public int getCat_cod() {
        return cat_cod;
    }

    public void setCat_cod(int cat_cod) {
        this.cat_cod = cat_cod;
    }

    public String getSer_nom() {
        return ser_nom;
    }

    public void setSer_nom(String ser_nom) {
        this.ser_nom = ser_nom;
    }
}
