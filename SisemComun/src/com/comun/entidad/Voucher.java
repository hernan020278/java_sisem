package com.comun.entidad;

import java.io.Serializable;
import java.sql.Date;

public class Voucher implements EntidadListener {

    private String vou_cod;
    private String loc_cod;
    private String ope_cod;
    private String doc_cod;
    private String vou_periodo;
    private Date vou_fecemi;
    private Date vou_fecven;
    private String vou_tipasi;
    private int vou_numasi;
    private String vou_numdoc;
    private String vou_glosa;
    private double vou_importe;

    public String getDoc_cod() {
        return doc_cod;
    }

    public void setDoc_cod(String doc_cod) {
        this.doc_cod = doc_cod;
    }

    public String getLoc_cod() {
        return loc_cod;
    }

    public void setLoc_cod(String loc_cod) {
        this.loc_cod = loc_cod;
    }

    public String getOpe_cod() {
        return ope_cod;
    }

    public void setOpe_cod(String ope_cod) {
        this.ope_cod = ope_cod;
    }

    public String getVou_cod() {
        return vou_cod;
    }

    public void setVou_cod(String vou_cod) {
        this.vou_cod = vou_cod;
    }

    public Date getVou_fecemi() {
        return vou_fecemi;
    }

    public void setVou_fecemi(Date vou_fecemi) {
        this.vou_fecemi = vou_fecemi;
    }

    public Date getVou_fecven() {
        return vou_fecven;
    }

    public void setVou_fecven(Date vou_fecven) {
        this.vou_fecven = vou_fecven;
    }

    public String getVou_glosa() {
        return vou_glosa;
    }

    public void setVou_glosa(String vou_glosa) {
        this.vou_glosa = vou_glosa;
    }

    public double getVou_importe() {
        return vou_importe;
    }

    public void setVou_importe(double vou_importe) {
        this.vou_importe = vou_importe;
    }

    public int getVou_numasi() {
        return vou_numasi;
    }

    public void setVou_numasi(int vou_numasi) {
        this.vou_numasi = vou_numasi;
    }

    public String getVou_numdoc() {
        return vou_numdoc;
    }

    public void setVou_numdoc(String vou_numdoc) {
        this.vou_numdoc = vou_numdoc;
    }

    public String getVou_periodo() {
        return vou_periodo;
    }

    public void setVou_periodo(String vou_periodo) {
        this.vou_periodo = vou_periodo;
    }

    public String getVou_tipasi() {
        return vou_tipasi;
    }

    public void setVou_tipasi(String vou_tipasi) {
        this.vou_tipasi = vou_tipasi;
    }

    public void setObjeto(Voucher parVou) {

        vou_cod = parVou.getVou_cod();
        loc_cod = parVou.getLoc_cod();
        ope_cod = parVou.getOpe_cod();
        doc_cod = parVou.getDoc_cod();
        vou_periodo = parVou.getVou_periodo();
        vou_fecemi = parVou.getVou_fecemi();
        vou_fecven = parVou.getVou_fecven();
        vou_tipasi = parVou.getVou_tipasi();
        vou_numasi = parVou.getVou_numasi();
        vou_numdoc = parVou.getVou_numdoc();
        vou_glosa = parVou.getVou_glosa();
        vou_importe = parVou.getVou_importe();

    }

    public void limpiarInstancia() {

        vou_cod = "";
        loc_cod = "";
        ope_cod = "";
        doc_cod = "";
        vou_periodo = "";
        vou_fecemi = null;
        vou_fecven = null;
        vou_tipasi = "";
        vou_numasi = 0;
        vou_numdoc = "";
        vou_glosa = "";
        vou_importe = 0.00;

    }

    public void imprimirValores() {

        System.out.println(vou_cod + " : " + loc_cod + " : " + ope_cod + " : " + doc_cod + " : " + vou_periodo + " : "
                + vou_fecemi + " : " + vou_fecven + " : " + vou_tipasi + " : " + vou_numasi + " : " + vou_numdoc + " : "
                + vou_glosa + " : " + vou_importe);

    }//Fin de metodo imprimirValores
}//Fin de clase principal
