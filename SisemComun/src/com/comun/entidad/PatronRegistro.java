package com.comun.entidad;

public class PatronRegistro implements EntidadListener {

    private String ind_cod;
    private String regcasest;

    public void imprimirValores() {

        System.out.println(ind_cod +  " : " + regcasest);

    }
   
	public String getInd_cod() {
        return ind_cod;
    }

    public void setInd_cod(String ind_cod) {
        this.ind_cod = ind_cod;
    }

    public String getRegcasest() {
        return regcasest;
    }

    public void setRegcasest(String regcasest) {
        this.regcasest = regcasest;
    }
}
