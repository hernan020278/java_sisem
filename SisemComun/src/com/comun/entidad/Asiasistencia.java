package com.comun.entidad;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
public class Asiasistencia implements EntidadListener {

	private BigDecimal kyasistencia;
	private BigDecimal kyusuario;
	private BigDecimal kyctrlasistencia;
	private BigDecimal kyhorario;
	private BigDecimal kyturno;
	private String numerodni;
	private String periodo;
	private String horarionombre;
	private Date fecha;
	private String turnonombre;
	private String turnodia;
	private String turnotipo;
	private Timestamp horaingreso;
	private String estadoingreso;
	private Timestamp horasalida;
	private String estadosalida;
	private BigDecimal horatrabajada;
	private BigDecimal horaconsumida;
	private BigDecimal horacomputada;
	private String modoregistro;
	private String foto;
}
