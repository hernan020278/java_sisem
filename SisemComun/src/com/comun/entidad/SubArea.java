package com.comun.entidad;


public class SubArea  implements EntidadListener {

	private int subare_cod;
	private int are_cod;
	private String subare_ide;
	private String subare_nom;
	private String subare_des;
	private String subare_int;
	private String subare_bar;
	private boolean subare_act;
	private int subare_ord;
	private int subare_ver;

	public void setObjeto(SubArea parObj) {

		subare_cod = parObj.getSubare_cod();
		are_cod = parObj.getAre_cod();
		subare_ide = parObj.getSubare_ide();
		subare_nom = parObj.getSubare_nom();
		subare_des = parObj.getSubare_des();
		subare_int = parObj.getSubare_int();
		subare_bar = parObj.getSubare_bar();
		subare_act = parObj.isSubare_act();
		subare_ord = parObj.getSubare_ord();
		subare_ver = parObj.getSubare_ver();

	}// Fin de public void setObjeto()

	public void imprimirValores() {

		System.out.println(subare_cod + " : " +
				are_cod + " : " +
				subare_ide + " : " +
				subare_nom + " : " +
				subare_des + " : " +
				subare_int + " : " +
				subare_bar + " : " +
				subare_act + " : " +
				subare_ord + " : " +
				subare_ver);
	}

	public void limpiarInstancia() {

		subare_cod = 0;
		are_cod = 0;
		subare_ide = "";
		subare_nom = "";
		subare_des = "";
		subare_int = "";
		subare_bar = "";
		subare_act = false;
		subare_ord = 0;
		subare_ver = 0;

	}// Fin de limpiarInstancia()

	public boolean isSubare_act() {
		return subare_act;
	}

	public void setSubare_act(boolean subare_act) {
		this.subare_act = subare_act;
	}

	public int getSubare_ord() {
		return subare_ord;
	}

	public void setSubare_ord(int subare_ord) {
		this.subare_ord = subare_ord;
	}

	public String getSubare_int() {
		return subare_int;
	}

	public void setSubare_int(String subare_int) {
		this.subare_int = subare_int;
	}

	public int getSubare_cod() {
		return subare_cod;
	}

	public void setSubare_cod(int subare_cod) {
		this.subare_cod = subare_cod;
	}

	public int getAre_cod() {
		return are_cod;
	}

	public void setAre_cod(int are_cod) {
		this.are_cod = are_cod;
	}

	public String getSubare_ide() {
		return subare_ide;
	}

	public void setSubare_ide(String subare_ide) {
		this.subare_ide = subare_ide;
	}

	public String getSubare_nom() {
		return subare_nom;
	}

	public void setSubare_nom(String subare_nom) {
		this.subare_nom = subare_nom;
	}

	public String getSubare_des() {
		return subare_des;
	}

	public void setSubare_des(String subare_des) {
		this.subare_des = subare_des;
	}

	public int getSubare_ver() {
		return subare_ver;
	}

	public void setSubare_ver(int subare_ver) {
		this.subare_ver = subare_ver;
	}

	public String getSubare_bar() {
		return subare_bar;
	}

	public void setSubare_bar(String subare_bar) {
		this.subare_bar = subare_bar;
	}

}
