package com.comun.entidad;

import java.sql.Date;

/**
 *
 * @author HERNAN MENDOZA
 */
public class ORCtrlAsistencia implements EntidadListener {

    private String ctrl_cod;
    private String per_cod;
    private String horcod;
    private String ctrl_fer;
    private Date ctrl_fecini;
    private Date ctrl_fecfin;
    private int ctrl_numtar;
    private int ctrl_falinj;
    private int ctrl_falinjper;
    private int ctrl_falnoLab;
    private double ctrl_horobl;
    private double ctrl_hortra;
    private double ctrl_hnotra;
    private double ctrl_horasi;
    private double ctrl_horina;
    private double ctrl_horrep;
    private double ctrl_horext;
    private double ctrl_horper;
    private boolean ctrl_estrecpos;
    private String ctrl_estper;

    public String getCtrl_cod() {
        return ctrl_cod;
    }

    public void setCtrl_cod(String ctrl_cod) {
        this.ctrl_cod = ctrl_cod;
    }

    public String getCtrl_estper() {
        return ctrl_estper;
    }

    public void setCtrl_estper(String ctrl_estper) {
        this.ctrl_estper = ctrl_estper;
    }

    public boolean isCtrl_estrecpos() {
        return ctrl_estrecpos;
    }

    public void setCtrl_estrecpos(boolean ctrl_estrecpos) {
        this.ctrl_estrecpos = ctrl_estrecpos;
    }

    public int getCtrl_falinj() {
        return ctrl_falinj;
    }

    public void setCtrl_falinj(int ctrl_falinj) {
        this.ctrl_falinj = ctrl_falinj;
    }

    public int getCtrl_falinjper() {
        return ctrl_falinjper;
    }

    public void setCtrl_falinjper(int ctrl_falinjper) {
        this.ctrl_falinjper = ctrl_falinjper;
    }

    public int getCtrl_falnoLab() {
        return ctrl_falnoLab;
    }

    public void setCtrl_falnoLab(int ctrl_falnoLab) {
        this.ctrl_falnoLab = ctrl_falnoLab;
    }

    public Date getCtrl_fecfin() {
        return ctrl_fecfin;
    }

    public void setCtrl_fecfin(Date ctrl_fecfin) {
        this.ctrl_fecfin = ctrl_fecfin;
    }

    public Date getCtrl_fecini() {
        return ctrl_fecini;
    }

    public void setCtrl_fecini(Date ctrl_fecini) {
        this.ctrl_fecini = ctrl_fecini;
    }

    public String getCtrl_fer() {
        return ctrl_fer;
    }

    public void setCtrl_fer(String ctrl_fer) {
        this.ctrl_fer = ctrl_fer;
    }

    public double getCtrl_hnotra() {
        return ctrl_hnotra;
    }

    public void setCtrl_hnotra(double ctrl_hnotra) {
        this.ctrl_hnotra = ctrl_hnotra;
    }

    public double getCtrl_horasi() {
        return ctrl_horasi;
    }

    public void setCtrl_horasi(double ctrl_horasi) {
        this.ctrl_horasi = ctrl_horasi;
    }

    public double getCtrl_horext() {
        return ctrl_horext;
    }

    public void setCtrl_horext(double ctrl_horext) {
        this.ctrl_horext = ctrl_horext;
    }

    public double getCtrl_horina() {
        return ctrl_horina;
    }

    public void setCtrl_horina(double ctrl_horina) {
        this.ctrl_horina = ctrl_horina;
    }

    public double getCtrl_horobl() {
        return ctrl_horobl;
    }

    public void setCtrl_horobl(double ctrl_horobl) {
        this.ctrl_horobl = ctrl_horobl;
    }

    public double getCtrl_horper() {
        return ctrl_horper;
    }

    public void setCtrl_horper(double ctrl_horper) {
        this.ctrl_horper = ctrl_horper;
    }

    public double getCtrl_hortra() {
        return ctrl_hortra;
    }

    public void setCtrl_hortra(double ctrl_hortra) {
        this.ctrl_hortra = ctrl_hortra;
    }

    public int getCtrl_numtar() {
        return ctrl_numtar;
    }

    public void setCtrl_numtar(int ctrl_numtar) {
        this.ctrl_numtar = ctrl_numtar;
    }

    public String getHor_cod() {
        return horcod;
    }

    public void setHor_cod(String horcod) {
        this.horcod = horcod;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public double getCtrl_horrep() {
        return ctrl_horrep;
    }

    public void setCtrl_horrep(double ctrl_horrep) {
        this.ctrl_horrep = ctrl_horrep;
    }
}
