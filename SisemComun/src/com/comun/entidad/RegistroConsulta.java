package com.comun.entidad;

import java.math.BigDecimal;

public class RegistroConsulta implements EntidadListener
{

	private BigDecimal rcocodigo;
	private String indcodigo;
	private String muecodigo;
	private BigDecimal paccodigo;
	private String descripcion;
	private double peso;
	private String estado;
	private int version;

	public BigDecimal getRcocodigo() {

		return rcocodigo;
	}

	public void setRcocodigo(BigDecimal rcocodigo) {

		this.rcocodigo = rcocodigo;
	}

	public String getIndcodigo() {

		return indcodigo;
	}

	public void setIndcodigo(String indcodigo) {

		this.indcodigo = indcodigo;
	}

	public String getMuecodigo() {

		return muecodigo;
	}

	public void setMuecodigo(String muecodigo) {

		this.muecodigo = muecodigo;
	}

	public BigDecimal getPaccodigo() {

		return paccodigo;
	}

	public void setPaccodigo(BigDecimal paccodigo) {

		this.paccodigo = paccodigo;
	}

	public String getDescripcion() {

		return descripcion;
	}

	public void setDescripcion(String descripcion) {

		this.descripcion = descripcion;
	}

	public double getPeso() {

		return peso;
	}

	public void setPeso(double peso) {

		this.peso = peso;
	}

	public String getEstado() {

		return estado;
	}

	public void setEstado(String estado) {

		this.estado = estado;
	}

	public int getVersion() {

		return version;
	}

	public void setVersion(int version) {

		this.version = version;
	}
}