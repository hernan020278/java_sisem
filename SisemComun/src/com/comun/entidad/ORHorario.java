package com.comun.entidad;

import java.sql.Time;

/*
 * ENTIDAD DE BASE DE DATOS
 */
public class ORHorario implements EntidadListener {

    private String horcod;
    private Time horiniman;
    private Time horlimtarman;
    private Time horlimentman;
    private Time horfinman;
    private Time horlimsalman;
    private Time horfinsab;
    private Time horinitar;
    private Time horlimtartar;
    private Time horlimenttar;
    private Time horfintar;
    private Time horlimsaltar;

    public String getHor_cod() {
        return horcod;
    }

    public void setHor_cod(String horcod) {
        this.horcod = horcod;
    }

    public Time getHor_finman() {
        return horfinman;
    }

    public void setHor_finman(Time horfinman) {
        this.horfinman = horfinman;
    }

    public Time getHor_finsab() {
        return horfinsab;
    }

    public void setHor_finsab(Time horfinsab) {
        this.horfinsab = horfinsab;
    }

    public Time getHor_fintar() {
        return horfintar;
    }

    public void setHor_fintar(Time horfintar) {
        this.horfintar = horfintar;
    }

    public Time getHor_iniman() {
        return horiniman;
    }

    public void setHor_iniman(Time horiniman) {
        this.horiniman = horiniman;
    }

    public Time getHor_initar() {
        return horinitar;
    }

    public void setHor_initar(Time horinitar) {
        this.horinitar = horinitar;
    }

    public Time getHor_limentman() {
        return horlimentman;
    }

    public void setHor_limentman(Time horlimentman) {
        this.horlimentman = horlimentman;
    }

    public Time getHor_limenttar() {
        return horlimenttar;
    }

    public void setHor_limenttar(Time horlimenttar) {
        this.horlimenttar = horlimenttar;
    }

    public Time getHor_limsalman() {
        return horlimsalman;
    }

    public void setHor_limsalman(Time horlimsalman) {
        this.horlimsalman = horlimsalman;
    }

    public Time getHor_limsaltar() {
        return horlimsaltar;
    }

    public void setHor_limsaltar(Time horlimsaltar) {
        this.horlimsaltar = horlimsaltar;
    }

    public Time getHor_limtarman() {
        return horlimtarman;
    }

    public void setHor_limtarman(Time horlimtarman) {
        this.horlimtarman = horlimtarman;
    }

    public Time getHor_limtartar() {
        return horlimtartar;
    }

    public void setHor_limtartar(Time horlimtartar) {
        this.horlimtartar = horlimtartar;
    }
}
