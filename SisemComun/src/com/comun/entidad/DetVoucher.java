package com.comun.entidad;

public class DetVoucher implements EntidadListener {

    private int detvou_cod;
    private String vou_cod;
    private String loc_cod;
    private String ope_cod;
    private String doc_cod;
    private int cta_numcta;
    private String detvou_tipreg;
    private double detvou_debe;
    private double detvou_haber;

    public int getCta_numcta() {
        return cta_numcta;
    }

    public void setCta_numcta(int cta_numcta) {
        this.cta_numcta = cta_numcta;
    }

    public int getDetvou_cod() {
        return detvou_cod;
    }

    public void setDetvou_cod(int detvou_cod) {
        this.detvou_cod = detvou_cod;
    }

    public double getDetvou_debe() {
        return detvou_debe;
    }

    public void setDetvou_debe(double detvou_debe) {
        this.detvou_debe = detvou_debe;
    }

    public double getDetvou_haber() {
        return detvou_haber;
    }

    public void setDetvou_haber(double detvou_haber) {
        this.detvou_haber = detvou_haber;
    }

    public String getDetvou_tipreg() {
        return detvou_tipreg;
    }

    public void setDetvou_tipreg(String detvou_tipreg) {
        this.detvou_tipreg = detvou_tipreg;
    }


    public String getDoc_cod() {
        return doc_cod;
    }

    public void setDoc_cod(String doc_cod) {
        this.doc_cod = doc_cod;
    }

    public String getLoc_cod() {
        return loc_cod;
    }

    public void setLoc_cod(String loc_cod) {
        this.loc_cod = loc_cod;
    }

    public String getOpe_cod() {
        return ope_cod;
    }

    public void setOpe_cod(String ope_cod) {
        this.ope_cod = ope_cod;
    }

    public String getVou_cod() {
        return vou_cod;
    }

    public void setVou_cod(String vou_cod) {
        this.vou_cod = vou_cod;
    }

    public void setObjeto(DetVoucher parDetVou) {

        detvou_cod = parDetVou.getDetvou_cod();
        vou_cod = parDetVou.getVou_cod();
        loc_cod = parDetVou.getLoc_cod();
        ope_cod = parDetVou.getOpe_cod();
        doc_cod = parDetVou.getDoc_cod();
        cta_numcta = parDetVou.getCta_numcta();
        detvou_tipreg = parDetVou.getDetvou_tipreg();
        detvou_debe = parDetVou.getDetvou_debe();
        detvou_haber = parDetVou.getDetvou_haber();

    }//Fin de public void setObjeto()

    public void imprimirValores() {

        System.out.println("VAlores de DetVenta");
        System.out.println("===================");
        System.out.println("detvou_cod : " + detvou_cod);
        System.out.println("vou_cod : " + vou_cod);
        System.out.println("loc_cod : " + loc_cod);
        System.out.println("ope_cod : " + ope_cod);
        System.out.println("doc_cod : " + doc_cod);
        System.out.println("cta_numcta : " + cta_numcta);
        System.out.println("detvou_tip : " + detvou_tipreg);
        System.out.println("detvou_debe : " + detvou_debe);
        System.out.println("detvou_haber : " + detvou_haber);
    }

    public void limpiarInstancia() {

        detvou_cod = 0;
        vou_cod = "";
        loc_cod = "";
        ope_cod = "";
        doc_cod = "";
        cta_numcta = 0;
        detvou_tipreg = "";
        detvou_debe = 0.00;
        detvou_haber = 0.00;

    }//Fin de limpiarInstancia()
}//Fin de Clase Principal
