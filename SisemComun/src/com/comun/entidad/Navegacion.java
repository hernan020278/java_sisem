package com.comun.entidad;

import java.io.Serializable;

public class Navegacion implements EntidadListener {

	private int orden;
	private String organizacionIde;
	private String tipoConeccion;
	private String paginaHijo;
	private String paginaPadre;
	private String modoAcceso;
	private String estadoPagina;
	private String evento;
	private String accion;
	private boolean navegar;

	public Navegacion() {
	}

	public boolean isNavegar() {
		return navegar;
	}

	public void setNavegar(boolean navegar) {
		this.navegar = navegar;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public String getOrganizacionIde() {
		return organizacionIde;
	}

	public void setOrganizacionIde(String organizacionIde) {
		this.organizacionIde = organizacionIde;
	}

	public String getTipoConeccion() {
		return tipoConeccion;
	}

	public void setTipoConeccion(String tipoConeccion) {
		this.tipoConeccion = tipoConeccion;
	}

	public String getPaginaHijo() {
		return paginaHijo;
	}

	public void setPaginaHijo(String paginaHijo) {
		this.paginaHijo = paginaHijo;
	}

	public String getPaginaPadre() {
		return paginaPadre;
	}

	public void setPaginaPadre(String paginaPadre) {
		this.paginaPadre = paginaPadre;
	}

	public String getModoAcceso() {
		return modoAcceso;
	}

	public void setModoAcceso(String modoAcceso) {
		this.modoAcceso = modoAcceso;
	}

	public String getEstadoPagina() {
		return estadoPagina;
	}

	public void setEstadoPagina(String estadoPagina) {
		this.estadoPagina = estadoPagina;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

}
