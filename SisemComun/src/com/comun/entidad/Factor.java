package com.comun.entidad;

public class Factor implements EntidadListener {

    private int fac_cod;
    private String kar_cod;
    private String fac_nom;
    private String fac_des;
    private String fac_tipdoc;
    private String fac_numdoc;
    private Double fac_imp;

    public int getFac_cod() {
        return fac_cod;
    }

    public void setFac_cod(int fac_cod) {
        this.fac_cod = fac_cod;
    }

    public String getFac_des() {
        return fac_des;
    }

    public void setFac_des(String fac_des) {
        this.fac_des = fac_des;
    }

    public Double getFac_imp() {
        return fac_imp;
    }

    public void setFac_imp(Double fac_imp) {
        this.fac_imp = fac_imp;
    }

    public String getFac_nom() {
        return fac_nom;
    }

    public void setFac_nom(String fac_nom) {
        this.fac_nom = fac_nom;
    }

    public String getFac_numdoc() {
        return fac_numdoc;
    }

    public void setFac_numdoc(String fac_numdoc) {
        this.fac_numdoc = fac_numdoc;
    }

    public String getFac_tipdoc() {
        return fac_tipdoc;
    }

    public void setFac_tipdoc(String fac_tipdoc) {
        this.fac_tipdoc = fac_tipdoc;
    }

    public String getKar_cod() {
        return kar_cod;
    }

    public void setKar_cod(String kar_cod) {
        this.kar_cod = kar_cod;
    }

    public void setObjeto(Factor parFac) {

        fac_cod = parFac.getFac_cod();
        kar_cod = parFac.getKar_cod();
        fac_nom = parFac.getFac_nom();
        fac_des = parFac.getFac_des();
        fac_tipdoc = parFac.getFac_tipdoc();
        fac_numdoc = parFac.getFac_numdoc();
        fac_imp = parFac.getFac_imp();

    }//Fin de public void setObjeto()

    public void limpiarInstancia() {

        fac_cod = 0;
        kar_cod = "";
        fac_nom = "";
        fac_des = "";
        fac_tipdoc = "";
        fac_numdoc = "";
        fac_imp = 0.00;

    }//Fin de limpiarInstancia()

    public void imprimirValores() {

        System.out.println("VAlores de DetKardex");
        System.out.println("====================");
        System.out.println("fac_cod : " + fac_cod);
        System.out.println("kar_cod : " + kar_cod);
        System.out.println("fac_nom : " + fac_nom);
        System.out.println("fac_des : " + fac_des);
        System.out.println("fac_tipdoc : " + fac_tipdoc);
        System.out.println("fac_numdoc : " + fac_numdoc);
        System.out.println("fac_imp : " + fac_imp);

    }//Fin de limpiarInstancia()
}//Fin de clase principal
