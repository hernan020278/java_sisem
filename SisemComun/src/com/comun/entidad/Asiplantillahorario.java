package com.comun.entidad;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;

@Getter
@Setter
public class Asiplantillahorario implements EntidadListener {

  private BigDecimal kyplantillahorario;
  private String descripcion;
  private String estado;
  private Timestamp version;
}
