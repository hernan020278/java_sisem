package com.comun.entidad;

import java.sql.Date;

public class PerfilSeguridad implements EntidadListener {

	private String tipo;
	private String nombre;
	private String segcodigo;
	private String nivel;
	private int acceso;
	private int version;

	public void setObjeto(PerfilSeguridad parObj) {

		tipo = parObj.getTipo();
		nombre = parObj.getNombre();
		segcodigo = parObj.getSegcodigo();
		nivel = parObj.getNivel();
		acceso = parObj.getAcceso();
		version = parObj.getVersion();

	}// Fin de setObjeto

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSegcodigo() {
		return segcodigo;
	}

	public void setSegcodigo(String segcodigo) {
		this.segcodigo = segcodigo;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public int getAcceso() {
		return acceso;
	}

	public void setAcceso(int acceso) {
		this.acceso = acceso;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}// Fin de Clase principal
