package com.comun.entidad;

import java.math.BigDecimal;

public class Cmpproducto implements EntidadListener {

	private BigDecimal cmpcodigo;
	private String prd_des;
	private BigDecimal valor;

	public void setObjeto(Cmpproducto parObj) {

		cmpcodigo = parObj.getCmpcodigo();
		prd_des = parObj.getPrd_des();
		valor = parObj.getValor();
	}// Fin de public void setObjeto()

	public String getPrd_des() {

		return prd_des;
	}

	public void setPrd_des(String prd_des) {

		this.prd_des = prd_des;
	}

	public BigDecimal getCmpcodigo() {

		return cmpcodigo;
	}

	public void setCmpcodigo(BigDecimal cmpcodigo) {

		this.cmpcodigo = cmpcodigo;
	}

	public BigDecimal getValor() {

		return valor;
	}

	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}
}
