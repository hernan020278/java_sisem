package com.comun.entidad;

import java.sql.Date;

/**
 *
 * @author HERNAN MENDOZA
 */
public class ORNomina implements EntidadListener {

    private String ctrl_cod;
    private String per_cod;
    private String horcod;
    private Date nom_fecemi;
    private int nom_idxprod;
    private double nom_imphortra;
    private double nom_imphorext;
    private double nom_imphorrep;
    private double nom_imptrabmal;
    private double nom_imptarde;
    private double nom_impfalta;
    private double nom_imphnotra;
    private double nom_imphorina;
    private double nom_suebas;
    private double nom_afp;
    private double nom_suebruto;
    private double nom_sueneto;
    private double nom_adeuno;
    private double nom_adedos;
    private double nom_saldo;
    private double nom_salant;

    public String getCtrl_cod() {
        return ctrl_cod;
    }

    public void setCtrl_cod(String ctrl_cod) {
        this.ctrl_cod = ctrl_cod;
    }

    public String getHor_cod() {
        return horcod;
    }

    public void setHor_cod(String horcod) {
        this.horcod = horcod;
    }

    public double getNom_adedos() {
        return nom_adedos;
    }

    public void setNom_adedos(double nom_adedos) {
        this.nom_adedos = nom_adedos;
    }

    public double getNom_adeuno() {
        return nom_adeuno;
    }

    public void setNom_adeuno(double nom_adeuno) {
        this.nom_adeuno = nom_adeuno;
    }

    public Date getNom_fecemi() {
        return nom_fecemi;
    }

    public void setNom_fecemi(Date nom_fecemi) {
        this.nom_fecemi = nom_fecemi;
    }

    public int getNom_idxprod() {
        return nom_idxprod;
    }

    public void setNom_idxprod(int nom_idxprod) {
        this.nom_idxprod = nom_idxprod;
    }

    public double getNom_impfalta() {
        return nom_impfalta;
    }

    public void setNom_impfalta(double nom_impfalta) {
        this.nom_impfalta = nom_impfalta;
    }

    public double getNom_imphnotra() {
        return nom_imphnotra;
    }

    public void setNom_imphnotra(double nom_imphnotra) {
        this.nom_imphnotra = nom_imphnotra;
    }

    public double getNom_imphorext() {
        return nom_imphorext;
    }

    public void setNom_imphorext(double nom_imphorext) {
        this.nom_imphorext = nom_imphorext;
    }

    public double getNom_imphorina() {
        return nom_imphorina;
    }

    public void setNom_imphorina(double nom_imphorina) {
        this.nom_imphorina = nom_imphorina;
    }

    public double getNom_imphorrep() {
        return nom_imphorrep;
    }

    public void setNom_imphorrep(double nom_imphorrep) {
        this.nom_imphorrep = nom_imphorrep;
    }

    public double getNom_imphortra() {
        return nom_imphortra;
    }

    public void setNom_imphortra(double nom_imphortra) {
        this.nom_imphortra = nom_imphortra;
    }

    public double getNom_imptarde() {
        return nom_imptarde;
    }

    public void setNom_imptarde(double nom_imptarde) {
        this.nom_imptarde = nom_imptarde;
    }

    public double getNom_imptrabmal() {
        return nom_imptrabmal;
    }

    public void setNom_imptrabmal(double nom_imptrabmal) {
        this.nom_imptrabmal = nom_imptrabmal;
    }

    public double getNom_suebruto() {
        return nom_suebruto;
    }

    public void setNom_suebruto(double nom_suebruto) {
        this.nom_suebruto = nom_suebruto;
    }

    public double getNom_saldo() {
        return nom_saldo;
    }

    public void setNom_saldo(double nom_saldo) {
        this.nom_saldo = nom_saldo;
    }

    public double getNom_sueneto() {
        return nom_sueneto;
    }

    public void setNom_sueneto(double nom_sueneto) {
        this.nom_sueneto = nom_sueneto;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public double getNom_salant() {
        return nom_salant;
    }

    public void setNom_salant(double nom_salant) {
        this.nom_salant = nom_salant;
    }

    public double getNom_afp() {
        return nom_afp;
    }

    public void setNom_afp(double nom_afp) {
        this.nom_afp = nom_afp;
    }

    public double getNom_suebas() {
        return nom_suebas;
    }

    public void setNom_suebas(double nom_suebas) {
        this.nom_suebas = nom_suebas;
    }

    public void imprimirValores() {

        System.out.println("Ctrl_codig" + ctrl_cod);
        System.out.println("Personal " + per_cod);
        System.out.println("Horario : " + horcod);
        System.out.println("Fecemision : " + nom_fecemi);
        System.out.println("Produccion" + nom_idxprod);
        System.out.println("Horas trabajadas" + nom_imphortra);
        System.out.println("Horas Extras : " + nom_imphorext);
        System.out.println("Repuestas : " + nom_imphorrep);
        System.out.println("Trabajo malogrado : " + nom_imptrabmal);
        System.out.println("Tardanza : " + nom_imptarde);
        System.out.println("Faltas : " + nom_impfalta);
        System.out.println("Hora no trabajada" + nom_imphnotra);
        System.out.println("Horas inasistidas : " + nom_imphorina);
        System.out.println("Sueldo Basico : " + nom_suebas);
        System.out.println("Afp : " + nom_afp);
        System.out.println("Sueldo Bruto : " + nom_suebruto);
        System.out.println("Sueldo neto : " + nom_sueneto);
        System.out.println("Adelanto uno : " + nom_adeuno);
        System.out.println("Adelanto Dos " + nom_adedos);
        System.out.println("Saldo : " + nom_saldo);
        System.out.println("Saldo Anterior  : " + nom_salant);


    }
}//Fin de clase ORNomina

