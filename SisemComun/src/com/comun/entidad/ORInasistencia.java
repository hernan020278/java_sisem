package com.comun.entidad;

import java.sql.Time;

public class ORInasistencia implements EntidadListener {

    private String ina_cod;
    private String det_cod;
    private Time ina_horreg;
    private String ina_tip;
    private double ina_horina;
    private double ina_horrep;
    private double ina_horrea;

    public String getDet_cod() {
        return det_cod;
    }

    public void setDet_cod(String det_cod) {
        this.det_cod = det_cod;
    }

    public String getIna_cod() {
        return ina_cod;
    }

    public void setIna_cod(String ina_cod) {
        this.ina_cod = ina_cod;
    }

    public double getIna_horina() {
        return ina_horina;
    }

    public void setIna_horina(double ina_horina) {
        this.ina_horina = ina_horina;
    }

    public double getIna_horrea() {
        return ina_horrea;
    }

    public void setIna_horrea(double ina_horrea) {
        this.ina_horrea = ina_horrea;
    }

    public Time getIna_horreg() {
        return ina_horreg;
    }

    public void setIna_horreg(Time ina_horreg) {
        this.ina_horreg = ina_horreg;
    }

    public double getIna_horrep() {
        return ina_horrep;
    }

    public void setIna_horrep(double ina_horrep) {
        this.ina_horrep = ina_horrep;
    }

    public String getIna_tip() {
        return ina_tip;
    }

    public void setIna_tip(String ina_tip) {
        this.ina_tip = ina_tip;
    }
}
