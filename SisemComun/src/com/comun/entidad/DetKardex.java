package com.comun.entidad;

public class DetKardex implements EntidadListener {

    private int detkar_cod;
    private String kar_cod;
    private String art_cod;
    private int detkar_can;
    private double detkar_cos;
    private double detkar_imp;
    private int detkar_sal;
    private double detkar_pre;
    private double detkar_tot;
    private String detkar_des;
    private String detkar_uni;
    private int detkar_ing;
    private int detkar_egr;

    public int getDetkar_egr() {
        return detkar_egr;
    }

    public void setDetkar_egr(int detkar_egr) {
        this.detkar_egr = detkar_egr;
    }

    public int getDetkar_ing() {
        return detkar_ing;
    }

    public void setDetkar_ing(int detkar_ing) {
        this.detkar_ing = detkar_ing;
    }

    public String getArt_cod() {
        return art_cod;
    }

    public void setArt_cod(String art_cod) {
        this.art_cod = art_cod;
    }

    public int getDetkar_can() {
        return detkar_can;
    }

    public void setDetkar_can(int detkar_can) {
        this.detkar_can = detkar_can;
    }

    public int getDetkar_cod() {
        return detkar_cod;
    }

    public void setDetkar_cod(int detkar_cod) {
        this.detkar_cod = detkar_cod;
    }

    public double getDetkar_cos() {
        return detkar_cos;
    }

    public void setDetkar_cos(double detkar_cos) {
        this.detkar_cos = detkar_cos;
    }

    public double getDetkar_imp() {
        return detkar_imp;
    }

    public void setDetkar_imp(double detkar_imp) {
        this.detkar_imp = detkar_imp;
    }

    public double getDetkar_pre() {
        return detkar_pre;
    }

    public void setDetkar_pre(double detkar_pre) {
        this.detkar_pre = detkar_pre;
    }

    public int getDetkar_sal() {
        return detkar_sal;
    }

    public void setDetkar_sal(int detkar_sal) {
        this.detkar_sal = detkar_sal;
    }

    public double getDetkar_tot() {
        return detkar_tot;
    }

    public void setDetkar_tot(double detkar_tot) {
        this.detkar_tot = detkar_tot;
    }

    public String getKar_cod() {
        return kar_cod;
    }

    public void setKar_cod(String kar_cod) {
        this.kar_cod = kar_cod;
    }

    public String getDetkar_des() {
        return detkar_des;
    }

    public void setDetkar_des(String detkar_des) {
        this.detkar_des = detkar_des;
    }

    public String getDetkar_uni() {
        return detkar_uni;
    }

    public void setDetkar_uni(String detkar_uni) {
        this.detkar_uni = detkar_uni;
    }

    public void setObjeto(DetKardex parDetKar) {

        detkar_cod = parDetKar.getDetkar_cod();
        art_cod = parDetKar.getArt_cod();
        kar_cod = parDetKar.getKar_cod();
        detkar_can = parDetKar.getDetkar_can();
        detkar_cos = parDetKar.getDetkar_cos();
        detkar_imp = parDetKar.getDetkar_imp();
        detkar_sal = parDetKar.getDetkar_sal();
        detkar_pre = parDetKar.getDetkar_pre();
        detkar_tot = parDetKar.getDetkar_tot();
        detkar_des = parDetKar.getDetkar_des();
        detkar_uni = parDetKar.getDetkar_uni();
        detkar_ing = parDetKar.getDetkar_ing();
        detkar_egr = parDetKar.getDetkar_egr();

    }//Fin de public void setObjeto()

    public void limpiarInstancia() {

        detkar_cod = 0;
        art_cod = "";
        kar_cod = "";
        detkar_can = 0;
        detkar_cos = 0.00;
        detkar_imp = 0.00;
        detkar_sal = 0;
        detkar_pre = 0.00;
        detkar_tot = 0.00;
        detkar_des = "";
        detkar_uni = "";
        detkar_ing = 0;
        detkar_egr = 0;

    }//Fin de limpiarInstancia()

    public void imprimirValores() {

        System.out.println(detkar_cod + " : " + kar_cod + " : " + art_cod + " : " + detkar_can + " : " + detkar_cos + " : "
                + detkar_imp + " : " + detkar_sal + " : " + detkar_pre + " : " + detkar_tot + " : " + detkar_des + " : " + detkar_uni
                + detkar_ing + " : " + detkar_egr);

    }//Fin de limpiarInstancia()
}
