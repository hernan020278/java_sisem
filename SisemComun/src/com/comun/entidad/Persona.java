/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comun.entidad;

import java.io.Serializable;

/**
 *
 * @author ROMULO
 */
public class Persona implements EntidadListener {

    private String per_cod;
    private String per_tip;
    private String per_tipdoc;
    private String per_numdoc;
    private String per_nom;
    private String per_dir;
    private String per_tel;
    private String per_contacto;

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public String getPer_contacto() {
        return per_contacto;
    }

    public void setPer_contacto(String per_contacto) {
        this.per_contacto = per_contacto;
    }

    public String getPer_dir() {
        return per_dir;
    }

    public void setPer_dir(String per_dir) {
        this.per_dir = per_dir;
    }

    public String getPer_nom() {
        return per_nom;
    }

    public void setPer_nom(String per_nom) {
        this.per_nom = per_nom;
    }

    public String getPer_numdoc() {
        return per_numdoc;
    }

    public void setPer_numdoc(String per_numdoc) {
        this.per_numdoc = per_numdoc;
    }

    public String getPer_tel() {
        return per_tel;
    }

    public void setPer_tel(String per_tel) {
        this.per_tel = per_tel;
    }

    public String getPer_tip() {
        return per_tip;
    }

    public void setPer_tip(String per_tip) {
        this.per_tip = per_tip;
    }

    public String getPer_tipdoc() {
        return per_tipdoc;
    }

    public void setPer_tipdoc(String per_tipdoc) {
        this.per_tipdoc = per_tipdoc;
    }

    public void imprimiValores() {


        System.out.println(per_cod + " : " + per_tip + " : " + per_tipdoc + " : " + per_numdoc + " : "
                + per_nom + " : " + per_dir + " : " + per_tel + " : " + per_contacto);
    }

    public void limpiarInstancia() {

        per_cod = "";
        per_tip = "";
        per_tipdoc = "";
        per_numdoc = "";
        per_nom = "";
        per_dir = "";
        per_tel = "";
        per_contacto = "";

    }//Fin de limpiarInstancia()
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Persona)) {
            return false;
        }

        return ((Persona) obj).getPer_cod().equals(this.per_cod);
    }

    @Override
    public int hashCode() {
        int hash = 1;
        return hash * 31 + per_nom.hashCode();
    }

    @Override
    public String toString() {
        return per_nom;
    }
}//Fin de Clase principal
