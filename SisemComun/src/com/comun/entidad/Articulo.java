package com.comun.entidad;

import java.io.File;
import java.io.FileInputStream;
import javax.swing.ImageIcon;

public class Articulo implements EntidadListener {

    private String art_cod;
    private String art_codint;
    private String art_cat;
    private String art_mar;
    private String art_und;
    private String art_des;
    private int art_stkmin;
    private int art_stkmax;
    private int art_stkact;
    private double art_precos;
    private double art_preliq;
    private double art_premay;
    private double art_preuni;
    private double art_imptot;
    private ImageIcon art_foto;
    private File filArt_foto;
    private FileInputStream fisArt_foto;
    private int art_idx;

    public String getArt_cat() {
        return art_cat;
    }

    public void setArt_cat(String art_cat) {
        this.art_cat = art_cat;
    }

    public String getArt_cod() {
        return art_cod;
    }

    public void setArt_cod(String art_cod) {
        this.art_cod = art_cod;
    }

    public String getArt_codint() {
        return art_codint;
    }

    public void setArt_codint(String art_codint) {
        this.art_codint = art_codint;
    }

    public String getArt_des() {
        return art_des;
    }

    public void setArt_des(String art_des) {
        this.art_des = art_des;
    }

    public ImageIcon getArt_foto() {
        return art_foto;
    }

    public void setArt_foto(ImageIcon art_foto) {
        this.art_foto = art_foto;
    }

    public double getArt_imptot() {
        return art_imptot;
    }

    public void setArt_imptot(double art_imptot) {
        this.art_imptot = art_imptot;
    }

    public String getArt_mar() {
        return art_mar;
    }

    public void setArt_mar(String art_mar) {
        this.art_mar = art_mar;
    }

    public double getArt_preliq() {
        return art_preliq;
    }

    public void setArt_preliq(double art_preliq) {
        this.art_preliq = art_preliq;
    }

    public double getArt_precos() {
        return art_precos;
    }

    public void setArt_precos(double art_precos) {
        this.art_precos = art_precos;
    }

    public double getArt_premay() {
        return art_premay;
    }

    public void setArt_premay(double art_premay) {
        this.art_premay = art_premay;
    }

    public double getArt_preuni() {
        return art_preuni;
    }

    public void setArt_preuni(double art_preuni) {
        this.art_preuni = art_preuni;
    }

    public int getArt_stkact() {
        return art_stkact;
    }

    public void setArt_stkact(int art_stkact) {
        this.art_stkact = art_stkact;
    }

    public int getArt_stkmax() {
        return art_stkmax;
    }

    public void setArt_stkmax(int art_stkmax) {
        this.art_stkmax = art_stkmax;
    }

    public int getArt_stkmin() {
        return art_stkmin;
    }

    public void setArt_stkmin(int art_stkmin) {
        this.art_stkmin = art_stkmin;
    }

    public String getArt_und() {
        return art_und;
    }

    public void setArt_und(String art_und) {
        this.art_und = art_und;
    }

    public File getFilArt_foto() {
        return filArt_foto;
    }

    public void setFilArt_foto(File filArt_foto) {
        this.filArt_foto = filArt_foto;
    }

    public FileInputStream getFisArt_foto() {
        return fisArt_foto;
    }

    public void setFisArt_foto(FileInputStream fisArt_foto) {
        this.fisArt_foto = fisArt_foto;
    }

    public int getArt_idx() {
        return art_idx;
    }

    public void setArt_idx(int art_idx) {
        this.art_idx = art_idx;
    }

    /**
     * Retorna un {@code Sdfsdafadsf} object representing this {@code Integer}'s
     * value. Ejemplo de comentario para java {@link
     * java.lang.Integer#toString(int)} enlace a otro metodo.
     *
     * @return Este es un resultado de lo que retorna
     */
    public void imprimirValores() {

        System.out.println(art_cod + " : " + art_codint + " : " + art_cat + " : " + art_mar + " : " + art_und + " : "
                + art_des + " : " + art_stkmin + " : " + art_stkmax + " : " + art_stkact + " : " + art_precos + " : "
                + art_preliq + " : " + art_premay + " : " + art_preuni + " : " + art_imptot + " : " + art_foto);

    }

//    public Articulo obtener(){
//    
//        return new Articulo();
//    }
//    
//    public void otraImpresion(){
//    
//        System.out.println("Mensaje " +this.getClass().getName());
//    }
    public void setObjeto(Articulo parObj) {

        art_cod = parObj.getArt_cod();
        art_codint = parObj.getArt_codint();
        art_cat = parObj.getArt_cat();
        art_mar = parObj.getArt_mar();
        art_und = parObj.getArt_und();
        art_des = parObj.getArt_des();
        art_stkmin = parObj.getArt_stkmin();
        art_stkmax = parObj.getArt_stkmax();
        art_stkact = parObj.getArt_stkact();
        art_precos = parObj.getArt_precos();
        art_preliq = parObj.getArt_preliq();
        art_premay = parObj.getArt_premay();
        art_preuni = parObj.getArt_preuni();
        art_imptot = parObj.getArt_imptot();
        art_foto = parObj.getArt_foto();
        filArt_foto = parObj.getFilArt_foto();
        fisArt_foto = parObj.getFisArt_foto();
        art_idx = parObj.getArt_idx();

    }//Fin de setObjeto()

    public void limpiarInstancia() {

        art_cod = "";
        art_codint = "";
        art_cat = "";
        art_mar = "";
        art_und = "";
        art_des = "";
        art_stkmin = 0;
        art_stkmax = 0;
        art_stkact = 0;
        art_precos = 0.00;
        art_preliq = 0.00;
        art_premay = 0.00;
        art_preuni = 0.00;
        art_imptot = 0.00;
        art_foto = null;
        filArt_foto = null;
        fisArt_foto = null;
        art_idx = 0;

    }
}
