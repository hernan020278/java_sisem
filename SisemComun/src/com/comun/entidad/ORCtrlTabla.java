package com.comun.entidad;

public class ORCtrlTabla implements EntidadListener {

    private String ctrl_nomtab;
    private int ctrl_numreg;
    private int ctrl_numser;

    public String getCtrl_nomtab() {
        return ctrl_nomtab;
    }

    public void setCtrl_nomtab(String ctrl_nomtab) {
        this.ctrl_nomtab = ctrl_nomtab;
    }

    public int getCtrl_numreg() {
        return ctrl_numreg;
    }

    public void setCtrl_numreg(int ctrl_numreg) {
        this.ctrl_numreg = ctrl_numreg;
    }

    public int getCtrl_numser() {
        return ctrl_numser;
    }

    public void setCtrl_numser(int ctrl_numser) {
        this.ctrl_numser = ctrl_numser;
    }
}//Fin de clase principal

