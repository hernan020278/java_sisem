package com.comun.entidad;

import java.io.Serializable;

public class CtrlTablaVenta implements Serializable {

	private int ctrl_cod;
	private String ctrl_nomtab;
	private int ctrl_numreg;
	private int ctrl_numser;
	private double ctrl_valor;

	public void setObjeto(CtrlTablaVenta parObj) {

		ctrl_cod = parObj.getCtrl_cod();
		ctrl_nomtab = parObj.getCtrl_nomtaba();
		ctrl_numreg = parObj.getCtrl_numreg();
		ctrl_numser = parObj.getCtrl_numser();
		ctrl_valor = parObj.getCtrl_valor();

	}

	public int getCtrl_cod() {
		return ctrl_cod;
	}

	public void setCtrl_cod(int ctrl_cod) {
		this.ctrl_cod = ctrl_cod;
	}

	public String getCtrl_nomtaba() {
		return ctrl_nomtab;
	}

	public void setCtrl_nomtaba(String ctrl_nomtab) {
		this.ctrl_nomtab = ctrl_nomtab;
	}

	public int getCtrl_numreg() {
		return ctrl_numreg;
	}

	public void setCtrl_numreg(int ctrl_numreg) {
		this.ctrl_numreg = ctrl_numreg;
	}

	public int getCtrl_numser() {
		return ctrl_numser;
	}

	public void setCtrl_numser(int ctrl_numser) {
		this.ctrl_numser = ctrl_numser;
	}

	public double getCtrl_valor() {
		return ctrl_valor;
	}

	public void setCtrl_valor(double ctrl_valor) {
		this.ctrl_valor = ctrl_valor;
	}

	public void limpiarInstancia() {

		ctrl_cod = 0;
		ctrl_nomtab = "";
		ctrl_numreg = 0;
		ctrl_numser = 0;
		ctrl_valor = 0.00;

	}// Fin de limpiarInstancia

	public void imprimirValores() {

		System.out.println(ctrl_cod + " : " + ctrl_nomtab + " : " + ctrl_numreg + " : " + ctrl_numser + " : " + ctrl_valor);

	}

}// Fin de clase principal
