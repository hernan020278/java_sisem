package com.comun.entidad;

import java.io.Serializable;

public class OREncuesta implements EntidadListener {

    private String enc_cod;
    private String enc_lug;
    private int enc_can;

    public int getEnc_can() {
        return enc_can;
    }

    public void setEnc_can(int enc_can) {
        this.enc_can = enc_can;
    }

    public String getEnc_cod() {
        return enc_cod;
    }

    public void setEnc_cod(String enc_cod) {
        this.enc_cod = enc_cod;
    }

    public String getEnc_lug() {
        return enc_lug;
    }

    public void setEnc_lug(String enc_lug) {
        this.enc_lug = enc_lug;
    }
}
