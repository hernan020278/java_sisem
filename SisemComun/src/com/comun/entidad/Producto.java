package com.comun.entidad;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.ImageIcon;

public class Producto implements EntidadListener {

	private int prd_cod;
	private int gru_cod;
	private String prd_ide;
	private String prd_gro;
	private String prd_tip;
	private String prd_mat;
	private String prd_gau;
	private String prd_col;
	private String prd_des;
	private String prd_par;
	private int prd_stkact;
	private int prd_supuno;
	private int prd_supdos;
	private int prd_suptres;
	private int prd_supcua;
	private int prd_nivel;
	private String prd_bar;
	private ImageIcon prd_foto;
	private File filPrd_foto;
	private FileInputStream fisPrd_foto;
	private int prd_ver;
	
	private int prd_ide_len = 30;
	private int prd_gro_len = 20;
	private int prd_tip_len = 20;
	private int prd_mat_len = 20;
	private int prd_gau_len = 20;
	private int prd_col_len = 20;
	private int prd_des_len = 50;
	private int prd_par_len = 12;
	private int prd_bar_len = 12;


	/**
	 * Retorna un {@code Sdfsdafadsf} object representing this {@code Integer}'s
	 * value. Ejemplo de comentario para java {@link java.lang.Integer#toString(int)} enlace a otro metodo.
	 * 
	 * @return Este es un resultado de lo que retorna
	 */
	public void imprimirValores() {

		System.out.println(
				prd_cod + " : " +
						gru_cod + " : " +
						prd_ide + " : " +
						prd_gro + " : " +
						prd_tip + " : " +
						prd_mat + " : " +
						prd_gau + " : " +
						prd_col + " : " +
						prd_des + " : " +
						prd_par + " : " +
						prd_stkact + " : " +
						prd_supuno + " : " +
						prd_supdos + " : " +
						prd_suptres + " : " +
						prd_supcua + " : " +
						prd_supcua + " : " +
						prd_nivel + " : " +
						prd_bar + " : " +
						prd_foto + " : " +
						prd_bar + " : " +
						prd_ver);
	}

	public void setObjeto(Producto parObj) {

		prd_cod = parObj.getPrd_cod();
		gru_cod = parObj.getGru_cod();
		prd_ide = parObj.getPrd_ide();
		prd_gro = parObj.getPrd_gro();
		prd_tip = parObj.getPrd_tip();
		prd_mat = parObj.getPrd_mat();
		prd_gau = parObj.getPrd_gau();
		prd_col = parObj.getPrd_col();
		prd_des = parObj.getPrd_des();
		prd_par = parObj.getPrd_par();
		prd_stkact = parObj.getPrd_stkact();
		prd_supuno = parObj.getPrd_supuno();
		prd_supdos = parObj.getPrd_supdos();
		prd_suptres = parObj.getPrd_suptres();
		prd_supcua = parObj.getPrd_supcua();
		prd_nivel = parObj.getPrd_nivel();
		prd_bar = parObj.getPrd_bar();
		prd_foto = parObj.getPrd_foto();
		prd_ver = parObj.getPrd_ver();
	}// Fin de setObjeto()

	public void limpiarInstancia() {

		gru_cod = 0;
		prd_cod = 0;
		prd_ide = "";
		prd_gro = "";
		prd_tip = "";
		prd_mat = "";
		prd_gau = "";
		prd_col = "";
		prd_des = "";
		prd_par = "";
		prd_stkact = 0;
		prd_supuno = 0;
		prd_supdos = 0;
		prd_suptres = 0;
		prd_supcua = 0;
		prd_nivel = 0;
		prd_bar = "";
		prd_foto = null;
		prd_ver = 0;
	}

	public String getPrd_gro() {

		return prd_gro;
	}

	public void setPrd_gro(String prd_gro) throws RuntimeException
	{
		if(prd_gro.length() <= this.getPrd_gro_len())
		{
			this.prd_gro = prd_gro;	
		}
		else
		{
			this.prd_gro = prd_gro.substring(0, this.getPrd_gro_len());
		}		
	}

	public int getGru_cod() {

		return gru_cod;
	}

	public void setGru_cod(int gru_cod) {

		this.gru_cod = gru_cod;
	}

	public int getPrd_nivel() {

		return prd_nivel;
	}

	public void setPrd_nivel(int prd_nivel) {

		this.prd_nivel = prd_nivel;
	}

	public int getPrd_supuno() {

		return prd_supuno;
	}

	public void setPrd_supuno(int prd_supuno) {

		this.prd_supuno = prd_supuno;
	}

	public int getPrd_supdos() {

		return prd_supdos;
	}

	public void setPrd_supdos(int prd_supdos) {

		this.prd_supdos = prd_supdos;
	}

	public int getPrd_suptres() {

		return prd_suptres;
	}

	public void setPrd_suptres(int prd_suptres) {

		this.prd_suptres = prd_suptres;
	}

	public int getPrd_supcua() {

		return prd_supcua;
	}

	public void setPrd_supcua(int prd_supcua) {

		this.prd_supcua = prd_supcua;
	}

	public String getPrd_tip() {

		return prd_tip;
	}

	public void setPrd_tip(String prd_tip) throws RuntimeException{
		if(prd_tip.length() <= this.getPrd_tip_len())
		{
			this.prd_tip = prd_tip;	
		}
		else
		{
			this.prd_tip = prd_tip.substring(0, this.getPrd_tip_len());
		}
	}

	public String getPrd_mat() {

		return prd_mat;
	}

	public void setPrd_mat(String prd_mat) throws RuntimeException{
		if(prd_mat.length() <= this.getPrd_mat_len())
		{
			this.prd_mat = prd_mat;	
		}
		else
		{
			this.prd_mat = prd_mat.substring(0, this.getPrd_mat_len());
		}
	}

	public String getPrd_col() {

		return prd_col;
	}

	public void setPrd_col(String prd_col) throws RuntimeException{
		if(prd_col.length() <= this.getPrd_col_len())
		{
			this.prd_col = prd_col;	
		}
		else
		{
			this.prd_col = prd_col.substring(0, this.getPrd_col_len());
		}
	}

	public int getPrd_cod() {

		return prd_cod;
	}

	public void setPrd_cod(int prd_cod) {

		this.prd_cod = prd_cod;
	}

	public String getPrd_ide() {

		return prd_ide;
	}

	public void setPrd_ide(String prd_ide) throws RuntimeException{
		if(prd_ide.length() <= this.getPrd_ide_len())
		{
			this.prd_ide = prd_ide;	
		}
		else
		{
			this.prd_ide = prd_ide.substring(0, this.getPrd_ide_len());
		}
	}

	public String getPrd_gau() {

		return prd_gau;
	}

	public void setPrd_gau(String prd_gau) throws RuntimeException{
		if(prd_gau.length() <= this.getPrd_gau_len())
		{
			this.prd_gau = prd_gau;	
		}
		else
		{
			this.prd_gau = prd_gau.substring(0, this.getPrd_gau_len());
		}
	}

	public String getPrd_des() {

		return prd_des;
	}

	public void setPrd_des(String prd_des) throws RuntimeException{
		if(prd_des.length() <= this.getPrd_des_len())
		{
			this.prd_des = prd_des;	
		}
		else
		{
			this.prd_des = prd_des.substring(0, this.getPrd_des_len());
		}
	}

	public String getPrd_par() {

		return prd_par;
	}

	public void setPrd_par(String prd_par) throws RuntimeException{
		if(prd_par.length() <= this.getPrd_par_len())
		{
			this.prd_par = prd_par;	
		}
		else
		{
			this.prd_par = prd_par.substring(0, this.getPrd_par_len());
		}
	}

	public int getPrd_stkact() {

		return prd_stkact;
	}

	public void setPrd_stkact(int prd_stkact) {

		this.prd_stkact = prd_stkact;
	}

	public ImageIcon getPrd_foto() {

		return prd_foto;
	}

	public void setPrd_foto(ImageIcon prd_foto) {

		this.prd_foto = prd_foto;
	}

	public File getFilPrd_foto() {

		return filPrd_foto;
	}

	public void setFilPrd_foto(File filPrd_foto) {

		this.filPrd_foto = filPrd_foto;
	}

	public FileInputStream getFisPrd_foto() {

		return fisPrd_foto;
	}

	public void setFisPrd_foto(FileInputStream fisPrd_foto) {

		this.fisPrd_foto = fisPrd_foto;
	}

	public String getPrd_bar() {

		return prd_bar;
	}

	public void setPrd_bar(String prd_bar) throws RuntimeException{
		if(prd_bar.length() <= this.getPrd_bar_len())
		{
			this.prd_bar = prd_bar;	
		}
		else
		{
			this.prd_bar = prd_bar.substring(0, this.getPrd_bar_len());
		}
	}

	public int getPrd_ver() {

		return prd_ver;
	}

	public void setPrd_ver(int prd_ver) {

		this.prd_ver = prd_ver;
	}

	public int getPrd_ide_len() {
		return prd_ide_len;
	}

	public int getPrd_gro_len() {
		return prd_gro_len;
	}

	public int getPrd_tip_len() {
		return prd_tip_len;
	}

	public int getPrd_mat_len() {
		return prd_mat_len;
	}

	public int getPrd_gau_len() {
		return prd_gau_len;
	}

	public int getPrd_col_len() {
		return prd_col_len;
	}

	public int getPrd_des_len() {
		return prd_des_len;
	}

	public int getPrd_par_len() {
		return prd_par_len;
	}

	public int getPrd_bar_len() {
		return prd_bar_len;
	}

	public void setPrd_ide_len(int prd_ide_len) {
		this.prd_ide_len = prd_ide_len;
	}

	public void setPrd_gro_len(int prd_gro_len) {
		this.prd_gro_len = prd_gro_len;
	}

	public void setPrd_tip_len(int prd_tip_len) {
		this.prd_tip_len = prd_tip_len;
	}

	public void setPrd_mat_len(int prd_mat_len) {
		this.prd_mat_len = prd_mat_len;
	}

	public void setPrd_gau_len(int prd_gau_len) {
		this.prd_gau_len = prd_gau_len;
	}

	public void setPrd_col_len(int prd_col_len) {
		this.prd_col_len = prd_col_len;
	}

	public void setPrd_des_len(int prd_des_len) {
		this.prd_des_len = prd_des_len;
	}

	public void setPrd_par_len(int prd_par_len) {
		this.prd_par_len = prd_par_len;
	}

	public void setPrd_bar_len(int prd_bar_len) {
		this.prd_bar_len = prd_bar_len;
	}
}
