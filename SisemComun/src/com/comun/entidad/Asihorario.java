package com.comun.entidad;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
public class Asihorario implements EntidadListener {

	private BigDecimal kyhorario;
	private BigDecimal kyusuario;
	private BigDecimal kyctrlasistencia;
	private String numerodni;
	private String periodo;
	private String horariotipo;
	private String horarionombre;
	private Timestamp fechainicio;
	private Timestamp fechafinal;
	private BigDecimal totalhora;
	private String observacion;
	private String autordni;
	private String aprobadordni;
	private String estatico;
	private String estado;
	private Timestamp version;
}
