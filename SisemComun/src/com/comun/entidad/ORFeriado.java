package com.comun.entidad;

public class ORFeriado implements EntidadListener {

    private String ctrl_fer;
    private double fer_horferest;
    private double fer_horferreg;
    private double fer_hortotmes;
    private double fer_horobltrab;

    public String getCtrl_fer() {
        return ctrl_fer;
    }

    public void setCtrl_fer(String ctrl_fer) {
        this.ctrl_fer = ctrl_fer;
    }

    public double getFer_horferest() {
        return fer_horferest;
    }

    public void setFer_horferest(double fer_horferest) {
        this.fer_horferest = fer_horferest;
    }

    public double getFer_horferreg() {
        return fer_horferreg;
    }

    public void setFer_horferreg(double fer_horferreg) {
        this.fer_horferreg = fer_horferreg;
    }

    public double getFer_horobltrab() {
        return fer_horobltrab;
    }

    public void setFer_horobltrab(double fer_horobltrab) {
        this.fer_horobltrab = fer_horobltrab;
    }

    public double getFer_hortotmes() {
        return fer_hortotmes;
    }

    public void setFer_hortotmes(double fer_hortotmes) {
        this.fer_hortotmes = fer_hortotmes;
    }
}
