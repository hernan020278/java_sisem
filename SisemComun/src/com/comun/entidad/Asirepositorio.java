package com.comun.entidad;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
public class Asirepositorio implements EntidadListener {

	private BigDecimal kyrepositorio;
	private BigDecimal kyusuario;
	private BigDecimal kyctrlasistencia;
	private BigDecimal kyhorario;
	private BigDecimal kyasistencia;
	private String numerodni;
	private String periodo;
	private String horarionombre;
	private String repositoriotipo;
	private String repositorionombre;
	private Timestamp fechahora;
	private BigDecimal horaregistrada;
	private BigDecimal horaconsumida;
	private BigDecimal horareales;
	private String observacion;
	private String estado;
	private Timestamp version;
}
