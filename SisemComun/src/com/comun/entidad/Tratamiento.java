package com.comun.entidad;

import java.sql.Date;

import com.comun.entidad.EntidadListener;

public class Tratamiento implements EntidadListener {

	private String tracodigo;
	private String hiscodigo;
	private String paccodigo;
	private Date fecinicio;
	private Date fecfinal;
	private String evainicial;
	private String evafinal;
	private String estado;
	private int version;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTracodigo() {
		return tracodigo;
	}

	public void setTracodigo(String tracodigo) {
		this.tracodigo = tracodigo;
	}

	public String getHiscodigo() {
		return hiscodigo;
	}

	public void setHiscodigo(String hiscodigo) {
		this.hiscodigo = hiscodigo;
	}

	public String getPaccodigo() {
		return paccodigo;
	}

	public void setPaccodigo(String paccodigo) {
		this.paccodigo = paccodigo;
	}

	public Date getFecinicio() {
		return fecinicio;
	}

	public void setFecinicio(Date fecinicio) {
		this.fecinicio = fecinicio;
	}

	public Date getFecfinal() {
		return fecfinal;
	}

	public void setFecfinal(Date fecfinal) {
		this.fecfinal = fecfinal;
	}

	public String getEvainicial() {
		return evainicial;
	}

	public void setEvainicial(String evainicial) {
		this.evainicial = evainicial;
	}

	public String getEvafinal() {
		return evafinal;
	}

	public void setEvafinal(String evafinal) {
		this.evafinal = evafinal;
	}
}
