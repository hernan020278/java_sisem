package com.comun.entidad;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

public class Ficha implements EntidadListener {

	private int fic_cod;
	private int subare_cod;
	private int are_cod;
	private int detped_cod;
	private int ped_cod;
	private int prd_cod;
	private int gru_cod;
	private String fic_ide;
	private int fic_canped;
	private int fic_canreg;
	private Date fic_fecing;
	private Time fic_horing;
	private Date fic_fecsal;
	private Time fic_horsal;
	private int fic_horcon;
	private String fic_obs;
	private String fic_estado;
	private String fic_bar;
	private int fic_ver;

	public void setObjeto(Ficha parObj) {

		fic_cod = parObj.getFic_cod();
		subare_cod = parObj.getSubare_cod();
		are_cod = parObj.getAre_cod();
		detped_cod = parObj.getDetped_cod();
		ped_cod = parObj.getPed_cod();
		prd_cod = parObj.getPrd_cod();
		gru_cod = parObj.getGru_cod();
		fic_ide = parObj.getFic_ide();
		fic_canped = parObj.getFic_canped();
		fic_canreg = parObj.getFic_canreg();
		fic_fecing = parObj.getFic_fecing();
		fic_horing = parObj.getFic_horing();
		fic_fecsal = parObj.getFic_fecsal();
		fic_horsal = parObj.getFic_horsal();
		fic_horcon = parObj.getFic_horcon();
		fic_obs = parObj.getFic_obs();
		fic_estado = parObj.getFic_estado();
		fic_bar = parObj.getFic_bar();
		fic_ver = parObj.getFic_ver();
	}// Fin de public void setObjeto()

	public int getFic_canreg() {

		return fic_canreg;
	}

	public void setFic_canreg(int fic_canreg) {

		this.fic_canreg = fic_canreg;
	}

	public int getFic_canped() {

		return fic_canped;
	}

	public void setFic_canped(int fic_canped) {

		this.fic_canped = fic_canped;
	}

	public void limpiarInstancia() {

		fic_cod = 0;
		subare_cod = 0;
		are_cod = 0;
		detped_cod = 0;
		ped_cod = 0;
		prd_cod = 0;
		gru_cod = 0;
		fic_ide = "";
		fic_canped = 0;
		fic_canreg = 0;
		fic_fecing = null;
		fic_horing = null;
		fic_fecsal = null;
		fic_horsal = null;
		fic_horcon = 0;
		fic_obs = "";
		fic_estado = "";
		fic_bar = "";
		fic_ver = 0;
	}

	public String getFic_ide() {

		return fic_ide;
	}

	public void setFic_ide(String fic_ide) {

		this.fic_ide = fic_ide;
	}

	public int getFic_horcon() {

		return fic_horcon;
	}

	public void setFic_horcon(int fic_horcon) {

		this.fic_horcon = fic_horcon;
	}

	public Date getFic_fecing() {

		return fic_fecing;
	}

	public void setFic_fecing(Date fic_fecing) {

		this.fic_fecing = fic_fecing;
	}

	public Time getFic_horing() {

		return fic_horing;
	}

	public void setFic_horing(Time fic_horing) {

		this.fic_horing = fic_horing;
	}

	public Date getFic_fecsal() {

		return fic_fecsal;
	}

	public void setFic_fecsal(Date fic_fecsal) {

		this.fic_fecsal = fic_fecsal;
	}

	public Time getFic_horsal() {

		return fic_horsal;
	}

	public void setFic_horsal(Time fic_horsal) {

		this.fic_horsal = fic_horsal;
	}

	public int getFic_cod() {

		return fic_cod;
	}

	public void setFic_cod(int fic_cod) {

		this.fic_cod = fic_cod;
	}

	public int getSubare_cod() {

		return subare_cod;
	}

	public void setSubare_cod(int subare_cod) {

		this.subare_cod = subare_cod;
	}

	public int getAre_cod() {

		return are_cod;
	}

	public void setAre_cod(int are_cod) {

		this.are_cod = are_cod;
	}

	public int getDetped_cod() {

		return detped_cod;
	}

	public void setDetped_cod(int detped_cod) {

		this.detped_cod = detped_cod;
	}

	public int getPed_cod() {

		return ped_cod;
	}

	public void setPed_cod(int ped_cod) {

		this.ped_cod = ped_cod;
	}

	public int getPrd_cod() {

		return prd_cod;
	}

	public void setPrd_cod(int prd_cod) {

		this.prd_cod = prd_cod;
	}

	public int getGru_cod() {

		return gru_cod;
	}

	public void setGru_cod(int gru_cod) {

		this.gru_cod = gru_cod;
	}

	public String getFic_obs() {

		return fic_obs;
	}

	public void setFic_obs(String fic_obs) {

		this.fic_obs = fic_obs;
	}

	public String getFic_estado() {

		return fic_estado;
	}

	public void setFic_estado(String fic_estado) {

		this.fic_estado = fic_estado;
	}

	public int getFic_ver() {

		return fic_ver;
	}

	public void setFic_ver(int fic_ver) {

		this.fic_ver = fic_ver;
	}// Fin de limpiarInstancia()

	public String getFic_bar() {

		return fic_bar;
	}

	public void setFic_bar(String fic_bar) {

		this.fic_bar = fic_bar;
	}
}
