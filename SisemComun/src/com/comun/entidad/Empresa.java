package com.comun.entidad;

import java.io.Serializable;

public class Empresa implements EntidadListener {

    private String emp_cod;
    private String emp_ruc;
    private String emp_nombre;
    private String emp_descripcion;
    private String emp_contacto;

    public String getEmp_cod() {
        return emp_cod;
    }

    public void setEmp_cod(String emp_cod) {
        this.emp_cod = emp_cod;
    }

    public String getEmp_contacto() {
        return emp_contacto;
    }

    public void setEmp_contacto(String emp_contacto) {
        this.emp_contacto = emp_contacto;
    }

    public String getEmp_descripcion() {
        return emp_descripcion;
    }

    public void setEmp_descripcion(String emp_descripcion) {
        this.emp_descripcion = emp_descripcion;
    }

    public String getEmp_nombre() {
        return emp_nombre;
    }

    public void setEmp_nombre(String emp_nombre) {
        this.emp_nombre = emp_nombre;
    }

    public String getEmp_ruc() {
        return emp_ruc;
    }

    public void setEmp_ruc(String emp_ruc) {
        this.emp_ruc = emp_ruc;
    }

    public void limpiarInstancia() {

        emp_cod = "";
        emp_ruc = "";
        emp_nombre = "";
        emp_descripcion = "";
        emp_contacto = "";

    }
}
