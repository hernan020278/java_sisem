package com.comun.entidad;

import java.sql.Date;

public class Credito implements EntidadListener {

    private String cre_cod;
    private String ven_cod;
    private String cre_condpag;
    private Date cre_fecini;
    private Date cre_fecfin;
    private double cre_total;
    private double cre_acumulado;
    private double cre_saldo;
    private String cre_estado;

    public double getCre_acumulado() {
        return cre_acumulado;
    }

    public void setCre_acumulado(double cre_acumulado) {
        this.cre_acumulado = cre_acumulado;
    }

    public String getCre_cod() {
        return cre_cod;
    }

    public void setCre_cod(String cre_cod) {
        this.cre_cod = cre_cod;
    }

    public String getCre_condpag() {
        return cre_condpag;
    }

    public void setCre_condpag(String cre_condpag) {
        this.cre_condpag = cre_condpag;
    }

    public String getCre_estado() {
        return cre_estado;
    }

    public void setCre_estado(String cre_estado) {
        this.cre_estado = cre_estado;
    }

    public Date getCre_fecfin() {
        return cre_fecfin;
    }

    public void setCre_fecfin(Date cre_fecfin) {
        this.cre_fecfin = cre_fecfin;
    }

    public Date getCre_fecini() {
        return cre_fecini;
    }

    public void setCre_fecini(Date cre_fecini) {
        this.cre_fecini = cre_fecini;
    }

    public double getCre_saldo() {
        return cre_saldo;
    }

    public void setCre_saldo(double cre_saldo) {
        this.cre_saldo = cre_saldo;
    }

    public double getCre_total() {
        return cre_total;
    }

    public void setCre_total(double cre_total) {
        this.cre_total = cre_total;
    }

    public String getVen_cod() {
        return ven_cod;
    }

    public void setVen_cod(String ven_cod) {
        this.ven_cod = ven_cod;
    }

    public void setObjeto(Credito parCre) {
        
        cre_cod = parCre.getCre_cod();
        ven_cod=parCre.getVen_cod();
        cre_condpag = parCre.getCre_condpag();
        cre_fecini = parCre.getCre_fecini();
        cre_fecfin = parCre.getCre_fecfin();
        cre_total = parCre.getCre_total();
        cre_acumulado = parCre.getCre_acumulado();
        cre_saldo = parCre.getCre_saldo();
        cre_estado = parCre.getCre_estado();
        
    }//Fin de 
    
    public void imprimirValores() {
        System.out.println("Codigo Credito : " + cre_cod);
        System.out.println("Codigo Venta : " + ven_cod);
        System.out.println("Condiciones de pago : " + cre_condpag);
        System.out.println("Fecha inicio : " + cre_fecini);
        System.out.println("Fecha final : " + cre_fecfin);
        System.out.println("Total : " + cre_total);
        System.out.println("Acumulado : " + cre_acumulado);
        System.out.println("Saldo : " + cre_saldo);
        System.out.println("Estado : " + cre_estado);
    }

    public void limpiarInstancia() {

        cre_cod = "";
        ven_cod = "";
        cre_condpag = "";
        cre_fecini = null;
        cre_fecfin = null;
        cre_total = 0.00;
        cre_acumulado = 0.00;
        cre_saldo = 0.00;
        cre_estado = "";

    }//Fin de limpiarInstancia

}
