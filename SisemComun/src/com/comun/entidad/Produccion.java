package com.comun.entidad;

import java.math.BigDecimal;
import java.sql.Date;

public class Produccion implements EntidadListener {

	private BigDecimal prccodigo;
	private String identidad;
	private BigDecimal usucodigo;
	private String operario;
	private Date fecregistro;
	private Date fecelaboracion;
	private BigDecimal tmpesperado;
	private BigDecimal tmpmuerto;
	private BigDecimal tmprealizado;
	private BigDecimal factor;
	private BigDecimal muerto;
	private BigDecimal produccion;
	private String estado;
	private BigDecimal version;

	public Produccion()
	{

	}

	public BigDecimal getPrccodigo() {

		return prccodigo;
	}

	public void setPrccodigo(BigDecimal prccodigo) {

		this.prccodigo = prccodigo;
	}

	public String getIdentidad() {

		return identidad;
	}

	public void setIdentidad(String identidad) {

		this.identidad = identidad;
	}

	public BigDecimal getUsucodigo() {

		return usucodigo;
	}

	public void setUsucodigo(BigDecimal usucodigo) {

		this.usucodigo = usucodigo;
	}

	public String getOperario() {

		return operario;
	}

	public void setOperario(String operario) {

		this.operario = operario;
	}

	public Date getFecregistro() {

		return fecregistro;
	}

	public void setFecregistro(Date fecregistro) {

		this.fecregistro = fecregistro;
	}

	public Date getFecelaboracion() {

		return fecelaboracion;
	}

	public void setFecelaboracion(Date fecelaboracion) {

		this.fecelaboracion = fecelaboracion;
	}

	public BigDecimal getTmpesperado() {

		return tmpesperado;
	}

	public void setTmpesperado(BigDecimal tmpesperado) {

		this.tmpesperado = tmpesperado;
	}

	public BigDecimal getTmpmuerto() {

		return tmpmuerto;
	}

	public void setTmpmuerto(BigDecimal tmpmuerto) {

		this.tmpmuerto = tmpmuerto;
	}

	public BigDecimal getTmprealizado() {

		return tmprealizado;
	}

	public void setTmprealizado(BigDecimal tmprealizado) {

		this.tmprealizado = tmprealizado;
	}

	public BigDecimal getFactor() {

		return factor;
	}

	public void setFactor(BigDecimal factor) {

		this.factor = factor;
	}

	public BigDecimal getMuerto() {

		return muerto;
	}

	public void setMuerto(BigDecimal muerto) {

		this.muerto = muerto;
	}

	public BigDecimal getProduccion() {

		return produccion;
	}

	public void setProduccion(BigDecimal produccion) {

		this.produccion = produccion;
	}

	public String getEstado() {

		return estado;
	}

	public void setEstado(String estado) {

		this.estado = estado;
	}

	public BigDecimal getVersion() {

		return version;
	}

	public void setVersion(BigDecimal version) {

		this.version = version;
	}
}// Fin de Clase principal
