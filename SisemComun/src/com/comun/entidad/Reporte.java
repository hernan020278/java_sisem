package com.comun.entidad;

import java.sql.Date;

public class Reporte implements EntidadListener {

	private String titulo;
	private String datawindow;
	private String modulo;
	private String descripcion;
	private String indicador;
	private String xml;

	public void setObjeto(Reporte parObj) {

		if(parObj != null) {
			titulo = parObj.getTitulo();
			datawindow = parObj.getDatawindow();
			modulo = parObj.getModulo();
			descripcion = parObj.getDescripcion();
			indicador = parObj.getIndicador();
			xml = parObj.getXml();
		}
	}// in de setObjeto

	public String getTitulo() {

		return titulo;
	}

	public void setTitulo(String titulo) {

		this.titulo = titulo;
	}

	public String getDatawindow() {

		return datawindow;
	}

	public void setDatawindow(String datawindow) {

		this.datawindow = datawindow;
	}

	public String getModulo() {

		return modulo;
	}

	public void setModulo(String modulo) {

		this.modulo = modulo;
	}

	public String getDescripcion() {

		return descripcion;
	}

	public void setDescripcion(String descripcion) {

		this.descripcion = descripcion;
	}

	public String getIndicador() {

		return indicador;
	}

	public void setIndicador(String indicador) {

		this.indicador = indicador;
	}

	public String getXml() {

		return xml;
	}

	public void setXml(String xml) {

		this.xml = xml;
	}
}// Fin de Clase principal
