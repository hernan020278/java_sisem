package com.comun.entidad;

import java.math.BigDecimal;
import java.sql.Date;

public class Usuario implements EntidadListener {

	private BigDecimal usucodigo;
	private String orgcodigo;
	private BigDecimal arecodigo;
	private BigDecimal subarecodigo;
	private String identidad;
	private String mail;
	private String nombre;
	private String apellido;
	private String cargo;
	private String telefono;
	private String propietario;
	private Date fechaingreso;
	private Date fechasalida;
	private String usuario;
	private String clave;
	private String bloqueado;
	private String pregseguridad;
	private String resseguridad;
	private String mailactivo;
	private String foto;
	private String udf1;
	private String udf2;
	private String udf3;
	private String udf4;
	private String udf5;
	private String estado;
	private BigDecimal version;

	public void setObjeto(Usuario parObj) {

		if(parObj != null) {
			usucodigo = parObj.getUsucodigo();
			orgcodigo = parObj.getOrgcodigo();
			arecodigo = parObj.getArecodigo();
			subarecodigo = parObj.getSubarecodigo();
			identidad = parObj.getIdentidad();
			mail = parObj.getMail();
			nombre = parObj.getNombre();
			apellido = parObj.getApellido();
			cargo = parObj.getCargo();
			telefono = parObj.getTelefono();
			propietario = parObj.getPropietario();
			fechaingreso = parObj.getFechaingreso();
			fechasalida = parObj.getFechasalida();
			clave = parObj.getClave();
			bloqueado = parObj.getBloqueado();
			pregseguridad = parObj.getPregseguridad();
			resseguridad = parObj.getResseguridad();
			mailactivo = parObj.getMailactivo();
			foto = parObj.getFoto();
			udf1 = parObj.getUdf1();
			udf2 = parObj.getUdf2();
			udf3 = parObj.getUdf3();
			udf4 = parObj.getUdf4();
			udf5 = parObj.getUdf5();
			estado = parObj.getEstado();
			version = parObj.getVersion();
		}
	}// in de setObjeto

	public String getFoto() {

		return foto;
	}

	public void setFoto(String foto) {

		this.foto = foto;
	}

	public String getOrgcodigo() {

		return orgcodigo;
	}

	public void setOrgcodigo(String orgcodigo) {

		this.orgcodigo = orgcodigo;
	}

	public BigDecimal getArecodigo() {

		return arecodigo;
	}

	public void setArecodigo(BigDecimal arecodigo) {

		this.arecodigo = arecodigo;
	}

	public BigDecimal getSubarecodigo() {

		return subarecodigo;
	}

	public void setSubarecodigo(BigDecimal subarecodigo) {

		this.subarecodigo = subarecodigo;
	}

	public String getIdentidad() {

		return identidad;
	}

	public void setIdentidad(String identidad) {

		this.identidad = identidad;
	}

	public String getMail() {

		return mail;
	}

	public void setMail(String mail) {

		this.mail = mail;
	}

	public String getNombre() {

		return nombre;
	}

	public void setNombre(String nombre) {

		this.nombre = nombre;
	}

	public String getApellido() {

		return apellido;
	}

	public void setApellido(String apellido) {

		this.apellido = apellido;
	}

	public String getCargo() {

		return cargo;
	}

	public void setCargo(String cargo) {

		this.cargo = cargo;
	}

	public String getTelefono() {

		return telefono;
	}

	public void setTelefono(String telefono) {

		this.telefono = telefono;
	}

	public String getPropietario() {

		return propietario;
	}

	public void setPropietario(String propietario) {

		this.propietario = propietario;
	}

	public Date getFechaingreso() {

		return fechaingreso;
	}

	public void setFechaingreso(Date fechaingreso) {

		this.fechaingreso = fechaingreso;
	}

	public Date getFechasalida() {

		return fechasalida;
	}

	public void setFechasalida(Date fechasalida) {

		this.fechasalida = fechasalida;
	}

	public String getClave() {

		return clave;
	}

	public void setClave(String clave) {

		this.clave = clave;
	}

	public String getBloqueado() {

		return bloqueado;
	}

	public void setBloqueado(String bloqueado) {

		this.bloqueado = bloqueado;
	}

	public String getPregseguridad() {

		return pregseguridad;
	}

	public void setPregseguridad(String pregseguridad) {

		this.pregseguridad = pregseguridad;
	}

	public String getResseguridad() {

		return resseguridad;
	}

	public void setResseguridad(String resseguridad) {

		this.resseguridad = resseguridad;
	}

	public String getMailactivo() {

		return mailactivo;
	}

	public void setMailactivo(String mailactivo) {

		this.mailactivo = mailactivo;
	}

	public String getUsuario() {

		return usuario;
	}

	public void setUsuario(String usuario) {

		this.usuario = usuario;
	}

	public String getEstado() {

		return estado;
	}

	public void setEstado(String estado) {

		this.estado = estado;
	}

	public BigDecimal getUsucodigo() {

		return usucodigo;
	}

	public String getUdf1() {

		return udf1;
	}

	public void setUdf1(String udf1) {

		this.udf1 = udf1;
	}

	public String getUdf2() {

		return udf2;
	}

	public void setUdf2(String udf2) {

		this.udf2 = udf2;
	}

	public String getUdf3() {

		return udf3;
	}

	public void setUdf3(String udf3) {

		this.udf3 = udf3;
	}

	public String getUdf4() {

		return udf4;
	}

	public void setUdf4(String udf4) {

		this.udf4 = udf4;
	}

	public String getUdf5() {

		return udf5;
	}

	public void setUdf5(String udf5) {

		this.udf5 = udf5;
	}

	public void setUsucodigo(BigDecimal usucodigo) {

		this.usucodigo = usucodigo;
	}

	public BigDecimal getVersion() {

		return version;
	}

	public void setVersion(BigDecimal version) {

		this.version = version;
	}
}// Fin de Clase principal
