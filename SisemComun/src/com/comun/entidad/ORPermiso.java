package com.comun.entidad;

import java.sql.Time;
import java.sql.Date;

public class ORPermiso implements EntidadListener {

    private String prm_cod;
    private String ctrl_cod;
    private String per_cod;
    private String horcod;
    private Date prm_fecreg;
    private Time prm_horreg;
    private String prm_tip;
    private Date prm_feceje;
    private String prm_turneje;
    private Time prm_horini;
    private Time prm_horfin;
    private double prm_horprm;
    private String prm_motivo;
    private String prm_autor;
    private String prm_aprob;
    private String prm_estado;

    public String getCtrl_cod() {
        return ctrl_cod;
    }

    public void setCtrl_cod(String ctrl_cod) {
        this.ctrl_cod = ctrl_cod;
    }

    public String getHor_cod() {
        return horcod;
    }

    public void setHor_cod(String horcod) {
        this.horcod = horcod;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public String getPrm_aprob() {
        return prm_aprob;
    }

    public void setPrm_aprob(String prm_aprob) {
        this.prm_aprob = prm_aprob;
    }

    public String getPrm_autor() {
        return prm_autor;
    }

    public void setPrm_autor(String prm_autor) {
        this.prm_autor = prm_autor;
    }

    public String getPrm_cod() {
        return prm_cod;
    }

    public void setPrm_cod(String prm_cod) {
        this.prm_cod = prm_cod;
    }

    public String getPrm_estado() {
        return prm_estado;
    }

    public void setPrm_estado(String prm_estado) {
        this.prm_estado = prm_estado;
    }

    public Date getPrm_feceje() {
        return prm_feceje;
    }

    public void setPrm_feceje(Date prm_feceje) {
        this.prm_feceje = prm_feceje;
    }

    public Date getPrm_fecreg() {
        return prm_fecreg;
    }

    public void setPrm_fecreg(Date prm_fecreg) {
        this.prm_fecreg = prm_fecreg;
    }

    public Time getPrm_horfin() {
        return prm_horfin;
    }

    public void setPrm_horfin(Time prm_horfin) {
        this.prm_horfin = prm_horfin;
    }

    public Time getPrm_horini() {
        return prm_horini;
    }

    public void setPrm_horini(Time prm_horini) {
        this.prm_horini = prm_horini;
    }

    public double getPrm_horprm() {
        return prm_horprm;
    }

    public void setPrm_horprm(double prm_horprm) {
        this.prm_horprm = prm_horprm;
    }

    public Time getPrm_horreg() {
        return prm_horreg;
    }

    public void setPrm_horreg(Time prm_horreg) {
        this.prm_horreg = prm_horreg;
    }

    public String getPrm_motivo() {
        return prm_motivo;
    }

    public void setPrm_motivo(String prm_motivo) {
        this.prm_motivo = prm_motivo;
    }

    public String getPrm_tip() {
        return prm_tip;
    }

    public void setPrm_tip(String prm_tip) {
        this.prm_tip = prm_tip;
    }

    public String getPrm_turneje() {
        return prm_turneje;
    }

    public void setPrm_turneje(String prm_turneje) {
        this.prm_turneje = prm_turneje;
    }

    public static void main(String[] args) {
        ORPermiso or = new ORPermiso();
    }
}
