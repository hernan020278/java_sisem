package com.comun.entidad;

import java.sql.Date;

public class DetCredito implements EntidadListener {

    private int detcre_cod;
    private String cre_cod;
    private String ven_cod;
    private Date detcre_fec;
    private double detcre_imp;
    private double detcre_sal;
    private String detcre_obs;

    public String getCre_cod() {
        return cre_cod;
    }

    public void setCre_cod(String cre_cod) {
        this.cre_cod = cre_cod;
    }

    public int getDetcre_cod() {
        return detcre_cod;
    }

    public void setDetcre_cod(int detcre_cod) {
        this.detcre_cod = detcre_cod;
    }

    public Date getDetcre_fec() {
        return detcre_fec;
    }

    public void setDetcre_fec(Date detcre_fec) {
        this.detcre_fec = detcre_fec;
    }

    public double getDetcre_imp() {
        return detcre_imp;
    }

    public void setDetcre_imp(double detcre_imp) {
        this.detcre_imp = detcre_imp;
    }

    public String getDetcre_obs() {
        return detcre_obs;
    }

    public void setDetcre_obs(String detcre_obs) {
        this.detcre_obs = detcre_obs;
    }

    public double getDetcre_sal() {
        return detcre_sal;
    }

    public void setDetcre_sal(double detcre_sal) {
        this.detcre_sal = detcre_sal;
    }

    public String getVen_cod() {
        return ven_cod;
    }

    public void setVen_cod(String ven_cod) {
        this.ven_cod = ven_cod;
    }

    public void setObjeto(DetCredito parDetCre) {

        detcre_cod = parDetCre.getDetcre_cod();
        cre_cod = parDetCre.getCre_cod();
        ven_cod = parDetCre.getVen_cod();
        detcre_fec = parDetCre.getDetcre_fec();
        detcre_imp = parDetCre.getDetcre_imp();
        detcre_sal = parDetCre.getDetcre_sal();
        detcre_obs = parDetCre.getDetcre_obs();

    }//Fin de public void setObjeto()

    public void imprimirValores() {

        System.out.println("Valores de DetCredito");
        System.out.println("=====================");
        System.out.println("detcre_cod : " + detcre_cod);
        System.out.println("cre_cod : " + cre_cod);
        System.out.println("ven_cod : " + ven_cod);
        System.out.println("detcre_fec : " + detcre_fec);
        System.out.println("detcre_imp : " + detcre_imp);
        System.out.println("detcre_sal : " + detcre_sal);
        System.out.println("detcre_obs : " + detcre_obs);

    }

    public void limpiarInstancia() {

        detcre_cod = 0;
        cre_cod = "";
        ven_cod = "";
        detcre_fec = null;
        detcre_imp = 0.00;
        detcre_sal = 0.00;
        detcre_obs = "";

    }//Fin de limpiarInstancia()
}
