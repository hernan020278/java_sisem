package com.comun.entidad;

import java.io.Serializable;

import java.sql.Date;

public class Muestra implements EntidadListener {

	private String muecodigo;
	private String lugar;
	private Date fecinicio;
	private Date fecfinal;
	private int cantidad;
	private String estado;
	private int version;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getMuecodigo() {
		return muecodigo;
	}

	public void setMuecodigo(String muecodigo) {
		this.muecodigo = muecodigo;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public Date getFecinicio() {
		return fecinicio;
	}

	public void setFecinicio(Date fecinicio) {
		this.fecinicio = fecinicio;
	}

	public Date getFecfinal() {
		return fecfinal;
	}

	public void setFecfinal(Date fecfinal) {
		this.fecfinal = fecfinal;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
}
