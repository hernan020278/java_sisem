package com.comun.entidad;

import java.sql.Time;
import java.sql.Date;

public class ORExtra implements EntidadListener {

    private String ext_cod;
    private String ctrl_cod;
    private String per_cod;
    private String horcod;
    private Date ext_fecreg;
    private Time ext_horreg;
    private Date ext_feceje;
    private String ext_turneje;
    private Time ext_horini;
    private Time ext_horfin;
    private double ext_horext;
    private double ext_prehor;
    private int ext_prchor;
    private double ext_impext;
    private String ext_desext;
    private String ext_autor;
    private String ext_aprob;
    private String ext_estado;

    public String getCtrl_cod() {
        return ctrl_cod;
    }

    public void setCtrl_cod(String ctrl_cod) {
        this.ctrl_cod = ctrl_cod;
    }

    public String getExt_aprob() {
        return ext_aprob;
    }

    public void setExt_aprob(String ext_aprob) {
        this.ext_aprob = ext_aprob;
    }

    public String getExt_autor() {
        return ext_autor;
    }

    public void setExt_autor(String ext_autor) {
        this.ext_autor = ext_autor;
    }

    public String getExt_cod() {
        return ext_cod;
    }

    public void setExt_cod(String ext_cod) {
        this.ext_cod = ext_cod;
    }

    public String getExt_desext() {
        return ext_desext;
    }

    public void setExt_desext(String ext_desext) {
        this.ext_desext = ext_desext;
    }

    public String getExt_estado() {
        return ext_estado;
    }

    public void setExt_estado(String ext_estado) {
        this.ext_estado = ext_estado;
    }

    public Date getExt_feceje() {
        return ext_feceje;
    }

    public void setExt_feceje(Date ext_feceje) {
        this.ext_feceje = ext_feceje;
    }

    public Date getExt_fecreg() {
        return ext_fecreg;
    }

    public void setExt_fecreg(Date ext_fecreg) {
        this.ext_fecreg = ext_fecreg;
    }

    public double getExt_horext() {
        return ext_horext;
    }

    public void setExt_horext(double ext_horext) {
        this.ext_horext = ext_horext;
    }

    public Time getExt_horfin() {
        return ext_horfin;
    }

    public void setExt_horfin(Time ext_horfin) {
        this.ext_horfin = ext_horfin;
    }

    public Time getExt_horini() {
        return ext_horini;
    }

    public void setExt_horini(Time ext_horini) {
        this.ext_horini = ext_horini;
    }

    public Time getExt_horreg() {
        return ext_horreg;
    }

    public void setExt_horreg(Time ext_horreg) {
        this.ext_horreg = ext_horreg;
    }

    public double getExt_impext() {
        return ext_impext;
    }

    public void setExt_impext(double ext_impext) {
        this.ext_impext = ext_impext;
    }

    public int getExt_prchor() {
        return ext_prchor;
    }

    public void setExt_prchor(int ext_prchor) {
        this.ext_prchor = ext_prchor;
    }

    public double getExt_prehor() {
        return ext_prehor;
    }

    public void setExt_prehor(double ext_prehor) {
        this.ext_prehor = ext_prehor;
    }

    public String getExt_turneje() {
        return ext_turneje;
    }

    public void setExt_turneje(String ext_turneje) {
        this.ext_turneje = ext_turneje;
    }

    public String getHor_cod() {
        return horcod;
    }

    public void setHor_cod(String horcod) {
        this.horcod = horcod;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }
}
