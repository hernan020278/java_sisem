package com.comun.entidad;

public class Clase  implements EntidadListener {

	private int cla_cod;
	private int gru_cod;
	private String cla_nom;
	private String cla_des;
	private int cla_ver;

	public void setObjeto(Clase parObj) {

		cla_cod = parObj.getCla_cod();
		gru_cod = parObj.getGru_cod();
		cla_nom = parObj.getCla_nom();
		cla_des = parObj.getCla_des();
		cla_ver = parObj.getCla_ver();

	}// Fin de public void setObjeto()

	public void imprimirValores() {

		System.out.println(cla_cod + " : " + gru_cod + " : " + cla_nom + " : " + cla_des + " : "
				+ cla_des + " : " + cla_ver);
	}

	public void limpiarInstancia() {

		cla_cod = 0;
		gru_cod = 0;
		cla_nom = "";
		cla_des = "";
		cla_ver = 0;

	}// Fin de limpiarInstancia()

	public int getCla_cod() {
		return cla_cod;
	}

	public void setCla_cod(int cla_cod) {
		this.cla_cod = cla_cod;
	}

	public int getGru_cod() {
		return gru_cod;
	}

	public void setGru_cod(int gru_cod) {
		this.gru_cod = gru_cod;
	}

	public String getCla_nom() {
		return cla_nom;
	}

	public void setCla_nom(String cla_nom) {
		this.cla_nom = cla_nom;
	}

	public String getCla_des() {
		return cla_des;
	}

	public void setCla_des(String cla_des) {
		this.cla_des = cla_des;
	}

	public int getCla_ver() {
		return cla_ver;
	}

	public void setCla_ver(int cla_ver) {
		this.cla_ver = cla_ver;
	}

}
