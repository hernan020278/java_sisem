package com.comun.entidad;

import java.io.Serializable;

public class Categoria implements Serializable {

    private int cat_cod;
    private String cat_nom;

    public void setObjeto(Categoria parObj) {

        this.cat_cod = parObj.getCat_cod();
        this.cat_nom = parObj.getCat_nom();
    }

    public void imprimirValores() {

        System.out.println(cat_cod + " : " + cat_nom);

    }

    public void limpiarInstancia() {

        cat_cod = 0;
        cat_nom = "";

    }

    public int getCat_cod() {
        return cat_cod;
    }

    public void setCat_cod(int cat_cod) {
        this.cat_cod = cat_cod;
    }

    public String getCat_nom() {
        return cat_nom;
    }

    public void setCat_nom(String cat_nom) {
        this.cat_nom = cat_nom;
    }
}
