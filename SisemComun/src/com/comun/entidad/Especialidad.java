package com.comun.entidad;

public class Especialidad implements EntidadListener {

	private String espcodigo;
	private String descripcion;
	private String observacion;
	private String estado;
	private int version;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getEspcodigo() {
		return espcodigo;
	}

	public void setEspcodigo(String espcodigo) {
		this.espcodigo = espcodigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}
