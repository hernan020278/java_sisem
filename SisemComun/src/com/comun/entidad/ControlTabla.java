package com.comun.entidad;

public class ControlTabla implements EntidadListener {

    private String tab_nom;
    private int tab_reg;

    public String getTab_nom() {
        return tab_nom;
    }

    public void setTab_nom(String tab_nom) {
        this.tab_nom = tab_nom;
    }

    public int getTab_reg() {
        return tab_reg;
    }

    public void setTab_reg(int tab_reg) {
        this.tab_reg = tab_reg;
    }
}//Fin de clase principal

