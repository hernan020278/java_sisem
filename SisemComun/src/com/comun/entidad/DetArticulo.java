package com.comun.entidad;

public class DetArticulo implements EntidadListener {

    private int detart_cod;
    private String art_cod;
    private String detart_tipo;
    private String detart_serie;
    private boolean detart_mix;
    private String detart_nom;
    private int detart_stkact;

    public void setObjeto(DetArticulo parObj) {

        detart_cod = parObj.getDetart_cod();
        art_cod = parObj.getArt_cod();
        detart_tipo = parObj.getDetart_tipo();
        detart_serie = parObj.getDetart_serie();
        detart_mix = parObj.isDetart_mix();
        detart_nom = parObj.getDetart_nom();
        detart_stkact = parObj.getDetart_stkact();

    }//Fin de public void setObjeto()

    public void limpiarInstancia() {

        detart_cod = 0;
        art_cod = "";
        detart_tipo = "";
        detart_serie = "";
        detart_mix = false;
        detart_nom = "";
        detart_stkact = 0;

    }//Fin de limpiarInstancia()

    public void imprimirValores() {

        System.out.println(detart_cod + " : " + art_cod + "�: " + detart_tipo + " : "
                + detart_serie + " : " + detart_mix + " : " + detart_nom + " : " + detart_stkact);

    }//Fin de limpiarInstancia()

    public boolean isDetart_mix() {
        return detart_mix;
    }

    public void setDetart_mix(boolean detart_mix) {
        this.detart_mix = detart_mix;
    }

    public String getArt_cod() {
        return art_cod;
    }

    public void setArt_cod(String art_cod) {
        this.art_cod = art_cod;
    }

    public int getDetart_cod() {
        return detart_cod;
    }

    public void setDetart_cod(int detart_cod) {
        this.detart_cod = detart_cod;
    }

    public String getDetart_nom() {
        return detart_nom;
    }

    public void setDetart_nom(String detart_nom) {
        this.detart_nom = detart_nom;
    }

    public int getDetart_stkact() {
        return detart_stkact;
    }

    public void setDetart_stkact(int detart_stkact) {
        this.detart_stkact = detart_stkact;
    }

    public String getDetart_tipo() {
        return detart_tipo;
    }

    public void setDetart_tipo(String detart_tipo) {
        this.detart_tipo = detart_tipo;
    }

    public String getDetart_serie() {
        return detart_serie;
    }

    public void setDetart_serie(String detart_serie) {
        this.detart_serie = detart_serie;
    }
}//Fin de Clase Principal
