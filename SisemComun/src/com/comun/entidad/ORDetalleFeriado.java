package com.comun.entidad;

import java.sql.Date;

public class ORDetalleFeriado implements EntidadListener {

    private String ctrl_fer;
    private String dtf_tip;
    private Date dtf_fec;
    private String dtf_turn;
    private double dtf_hor;
    private String dtf_des;

    public String getCtrl_fer() {
        return ctrl_fer;
    }

    public void setCtrl_fer(String ctrl_fer) {
        this.ctrl_fer = ctrl_fer;
    }

    public String getDtf_des() {
        return dtf_des;
    }

    public void setDtf_des(String dtf_des) {
        this.dtf_des = dtf_des;
    }

    public Date getDtf_fec() {
        return dtf_fec;
    }

    public void setDtf_fec(Date dtf_fec) {
        this.dtf_fec = dtf_fec;
    }

    public double getDtf_hor() {
        return dtf_hor;
    }

    public void setDtf_hor(double dtf_hor) {
        this.dtf_hor = dtf_hor;
    }

    public String getDtf_tip() {
        return dtf_tip;
    }

    public void setDtf_tip(String dtf_tip) {
        this.dtf_tip = dtf_tip;
    }

    public String getDtf_turn() {
        return dtf_turn;
    }

    public void setDtf_turn(String dtf_turn) {
        this.dtf_turn = dtf_turn;
    }
}
