package com.comun.entidad;

public class Respuesta implements EntidadListener
{
	private int rsp_kyrsp;
	private String rsp_codi;
	private String rsp_dscr;
	private String rsp_foto;
	private String rsp_esta;
	private int rsp_vers;
	public int getRsp_kyrsp() {
		return rsp_kyrsp;
	}
	public void setRsp_kyrsp(int rsp_kyrsp) {
		this.rsp_kyrsp = rsp_kyrsp;
	}
	public String getRsp_codi() {
		return rsp_codi;
	}
	public void setRsp_codi(String rsp_codi) {
		this.rsp_codi = rsp_codi;
	}
	public String getRsp_dscr() {
		return rsp_dscr;
	}
	public void setRsp_dscr(String rsp_dscr) {
		this.rsp_dscr = rsp_dscr;
	}
	public String getRsp_foto() {
		return rsp_foto;
	}
	public void setRsp_foto(String rsp_foto) {
		this.rsp_foto = rsp_foto;
	}
	public String getRsp_esta() {
		return rsp_esta;
	}
	public void setRsp_esta(String rsp_esta) {
		this.rsp_esta = rsp_esta;
	}
	public int getRsp_vers() {
		return rsp_vers;
	}
	public void setRsp_vers(int rsp_vers) {
		this.rsp_vers = rsp_vers;
	}
}
