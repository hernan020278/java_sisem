package com.comun.entidad;

import java.io.Serializable;

public class Detalle implements Serializable {

    private int det_cod;
    private int ser_cod;
    private int cat_cod;
    private String det_nom;
    private boolean det_mix;

    public void setObjeto(Detalle parObj) {

        this.det_cod = parObj.getDet_cod();
        this.ser_cod = parObj.getSer_cod();
        this.cat_cod = parObj.getCat_cod();
        this.det_nom = parObj.getDet_nom();
        this.det_mix = parObj.isDet_mix();

    }

    public void imprimirValores() {

        System.out.println(det_cod + " : " + ser_cod + " : " + cat_cod + " : " + det_nom + " : " + det_mix);

    }

    public void limpiarInstancia() {

        det_cod = 0;
        ser_cod = 0;
        cat_cod = 0;
        det_nom = "";
        det_mix = true;

    }

    public int getDet_cod() {
        return det_cod;
    }

    public void setDet_cod(int det_cod) {
        this.det_cod = det_cod;
    }

    public int getSer_cod() {
        return ser_cod;
    }

    public void setSer_cod(int ser_cod) {
        this.ser_cod = ser_cod;
    }

    public int getCat_cod() {
        return cat_cod;
    }

    public void setCat_cod(int cat_cod) {
        this.cat_cod = cat_cod;
    }

    public String getDet_nom() {
        return det_nom;
    }

    public void setDet_nom(String det_nom) {
        this.det_nom = det_nom;
    }

    public boolean isDet_mix() {
        return det_mix;
    }

    public void setDet_mix(boolean det_mix) {
        this.det_mix = det_mix;
    }

}//Fin de Clase Principal
