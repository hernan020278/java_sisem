package com.comun.entidad;

import java.sql.Date;
import java.sql.Time;

public class Cita implements EntidadListener {

	private String citcodigo;
	private String espcodigo;
	private String tracodigo;
	private String doccodigo;
	private String hiscodigo;
	private String paccodigo;
	private Date fecha;
	private Time hora;
	private String evolucion;
	private String motivo;
	private String problema;
	private String estado;
	private int version;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getCitcodigo() {
		return citcodigo;
	}

	public void setCitcodigo(String citcodigo) {
		this.citcodigo = citcodigo;
	}

	public String getEspcodigo() {
		return espcodigo;
	}

	public void setEspcodigo(String espcodigo) {
		this.espcodigo = espcodigo;
	}

	public String getTracodigo() {
		return tracodigo;
	}

	public void setTracodigo(String tracodigo) {
		this.tracodigo = tracodigo;
	}

	public String getDoccodigo() {
		return doccodigo;
	}

	public void setDoccodigo(String doccodigo) {
		this.doccodigo = doccodigo;
	}

	public String getHiscodigo() {
		return hiscodigo;
	}

	public void setHiscodigo(String hiscodigo) {
		this.hiscodigo = hiscodigo;
	}

	public String getPaccodigo() {
		return paccodigo;
	}

	public void setPaccodigo(String paccodigo) {
		this.paccodigo = paccodigo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Time getHora() {
		return hora;
	}

	public void setHora(Time hora) {
		this.hora = hora;
	}

	public String getEvolucion() {
		return evolucion;
	}

	public void setEvolucion(String evolucion) {
		this.evolucion = evolucion;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getProblema() {
		return problema;
	}

	public void setProblema(String problema) {
		this.problema = problema;
	}
}
