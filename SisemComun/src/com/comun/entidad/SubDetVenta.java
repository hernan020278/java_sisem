package com.comun.entidad;

public class SubDetVenta implements EntidadListener {

    private int subdetven_cod;
    private int detven_cod;
    private String ven_cod;
    private int detart_cod;
    private String art_cod;
    private String subdetven_nom;
    private int subdetven_can;

    public String getArt_cod() {
        return art_cod;
    }

    public void setArt_cod(String art_cod) {
        this.art_cod = art_cod;
    }

    public int getDetart_cod() {
        return detart_cod;
    }

    public void setDetart_cod(int detart_cod) {
        this.detart_cod = detart_cod;
    }

    public int getDetven_cod() {
        return detven_cod;
    }

    public void setDetven_cod(int detven_cod) {
        this.detven_cod = detven_cod;
    }

    public int getSubdetven_can() {
        return subdetven_can;
    }

    public void setSubdetven_can(int subdetven_can) {
        this.subdetven_can = subdetven_can;
    }

    public int getSubdetven_cod() {
        return subdetven_cod;
    }

    public void setSubdetven_cod(int subdetven_cod) {
        this.subdetven_cod = subdetven_cod;
    }

    public String getSubdetven_nom() {
        return subdetven_nom;
    }

    public void setSubdetven_nom(String subdetven_nom) {
        this.subdetven_nom = subdetven_nom;
    }

    public String getVen_cod() {
        return ven_cod;
    }

    public void setVen_cod(String ven_cod) {
        this.ven_cod = ven_cod;
    }

    public void setObjeto(SubDetVenta parObj) {

        subdetven_cod = parObj.subdetven_cod;
        detven_cod = parObj.getDetven_cod();
        ven_cod = parObj.getVen_cod();
        detart_cod = parObj.getDetart_cod();
        art_cod = parObj.getArt_cod();
        subdetven_nom = parObj.getSubdetven_nom();
        subdetven_can = parObj.getSubdetven_can();

    }//Fin de public void setObjeto()

    public void limpiarInstancia() {

        subdetven_cod = 0;
        detven_cod = 0;
        ven_cod = "";
        detart_cod = 0;
        art_cod = "";
        subdetven_nom = "";
        subdetven_can = 0;

    }//Fin de limpiarInstancia()

    public void imprimirValores() {

        System.out.println("subdetven_cod : " + subdetven_cod + " detven_cod : " + detven_cod
                + " ven_cod : " + ven_cod + " detart_cod : " + detart_cod + " art_cod : " + art_cod
                + " subdetven_nom : " + subdetven_nom + " subdetven_can : " + subdetven_can);
    }//Fin de limpiarInstancia()
}
