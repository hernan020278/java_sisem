package com.comun.entidad;

import java.sql.Date;

public class Venta implements EntidadListener {

    private String ven_cod;
    private String vou_cod;
    private String loc_cod;
    private String ope_cod;
    private String doc_cod;
    private String per_cod;
    private String ven_nomdoc;
    private String ven_numdoc;
    private String ven_nomcom;
    private String ven_numcom;
    private String ven_nom;
    private String ven_dir;
    private String ven_docide;
    private String ven_numide;
    private Date ven_fecemi;
    private String ven_docref;
    private String ven_tipven;
    private String ven_formpag;
    private double ven_impdcto;
    private double ven_impbruto;
    private double ven_impigv;
    private double ven_imptot;
    private String ven_desimp;
    private double ven_liqimpbruto;
    private double ven_liqimpigv;
    private double ven_liqimptot;
    private String ven_liqdesimp;
    private String ven_estado;
    private String ven_modo;

    public void imprimirValores() {
        System.out.println(ven_cod + " : " + vou_cod + " : " + loc_cod + " : " + ope_cod + " : " + doc_cod + " : "
                + per_cod + " : " + ven_nomdoc + " : " + ven_numdoc + " : " + ven_nomcom + " : " + ven_numcom + " : "
                + ven_nom + " : " + ven_dir + " : " + ven_docide + " : " + ven_numide + " : " + ven_fecemi + " : "
                + ven_docref + " : " + ven_tipven + " : " + ven_formpag + " : " + ven_impdcto + " : " + ven_impbruto + " : "
                + ven_impigv + " : " + ven_imptot + " : " + ven_desimp + " : " + ven_liqimpbruto + " : "
                + ven_liqimpigv + " : " + ven_liqimptot + " : " + ven_liqdesimp + " : " + ven_estado + " : " + ven_modo);
    }

    public void limpiarInstancia() {

        ven_cod = "";
        vou_cod = "";
        loc_cod = "";
        ope_cod = "";
        doc_cod = "";
        per_cod = "";
        ven_nomdoc = "";
        ven_numdoc = "";
        ven_nomcom = "";
        ven_numcom = "";
        ven_nom = "";
        ven_dir = "";
        ven_docide = "";
        ven_numide = "";
        ven_fecemi = null;
        ven_docref = "";
        ven_tipven = "";
        ven_formpag = "";
        ven_impdcto = 0.00;
        ven_impbruto = 0.00;
        ven_impigv = 0.00;
        ven_imptot = 0.00;
        ven_desimp = "";
        ven_liqimpbruto = 0.00;
        ven_liqimpigv = 0.00;
        ven_liqimptot = 0.00;
        ven_liqdesimp = "";
        ven_estado = "";
        ven_modo = "";
    }//Fin de limpiarInstancia

    public void setObjeto(Venta parObj) {

        ven_cod = parObj.getVen_cod();
        vou_cod = parObj.getVou_cod();
        loc_cod = parObj.getLoc_cod();
        ope_cod = parObj.getOpe_cod();
        doc_cod = parObj.getDoc_cod();
        per_cod = parObj.getPer_cod();
        ven_nomdoc = parObj.getVen_nomdoc();
        ven_numdoc = parObj.getVen_numdoc();
        ven_nomcom = parObj.getVen_nomcom();
        ven_numcom = parObj.getVen_numcom();
        ven_nom = parObj.getVen_nom();
        ven_dir = parObj.getVen_dir();
        ven_docide = parObj.getVen_docide();
        ven_numide = parObj.getVen_numide();
        ven_fecemi = parObj.getVen_fecemi();
        ven_docref = parObj.getVen_docref();
        ven_tipven = parObj.getVen_tipven();
        ven_formpag = parObj.getVen_formpag();
        ven_impdcto = parObj.getVen_impdcto();
        ven_impbruto = parObj.getVen_impbruto();
        ven_impigv = parObj.getVen_impigv();
        ven_imptot = parObj.getVen_imptot();
        ven_desimp = parObj.getVen_desimp();
        ven_liqimpbruto = parObj.getVen_liqimpbruto();
        ven_liqimpigv = parObj.getVen_liqimpigv();
        ven_liqimptot = parObj.getVen_liqimptot();
        ven_liqdesimp = parObj.getVen_liqdesimp();
        ven_estado = parObj.getVen_estado();
        ven_modo = parObj.getVen_modo();
    }//Fin de limpiarInstancia

    
    public String getVen_tipven() {
        return ven_tipven;
    }

    public void setVen_tipven(String ven_tipven) {
        this.ven_tipven = ven_tipven;
    }

    public String getDoc_cod() {
        return doc_cod;
    }

    public String getVen_modo() {
		return ven_modo;
	}

	public void setVen_modo(String ven_modo) {
		this.ven_modo = ven_modo;
	}

	public void setDoc_cod(String doc_cod) {
        this.doc_cod = doc_cod;
    }

    public String getLoc_cod() {
        return loc_cod;
    }

    public void setLoc_cod(String loc_cod) {
        this.loc_cod = loc_cod;
    }

    public String getOpe_cod() {
        return ope_cod;
    }

    public void setOpe_cod(String ope_cod) {
        this.ope_cod = ope_cod;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public String getVen_cod() {
        return ven_cod;
    }

    public void setVen_cod(String ven_cod) {
        this.ven_cod = ven_cod;
    }

    public String getVen_desimp() {
        return ven_desimp;
    }

    public void setVen_desimp(String ven_desimp) {
        this.ven_desimp = ven_desimp;
    }

    public String getVen_dir() {
        return ven_dir;
    }

    public void setVen_dir(String ven_dir) {
        this.ven_dir = ven_dir;
    }

    public String getVen_docide() {
        return ven_docide;
    }

    public void setVen_docide(String ven_docide) {
        this.ven_docide = ven_docide;
    }

    public String getVen_docref() {
        return ven_docref;
    }

    public void setVen_docref(String ven_docref) {
        this.ven_docref = ven_docref;
    }

    public Date getVen_fecemi() {
        return ven_fecemi;
    }

    public void setVen_fecemi(Date ven_fecemi) {
        this.ven_fecemi = ven_fecemi;
    }

    public String getVen_formpag() {
        return ven_formpag;
    }

    public void setVen_formpag(String ven_formpag) {
        this.ven_formpag = ven_formpag;
    }

    public double getVen_impbruto() {
        return ven_impbruto;
    }

    public void setVen_impbruto(double ven_impbruto) {
        this.ven_impbruto = ven_impbruto;
    }

    public double getVen_impigv() {
        return ven_impigv;
    }

    public void setVen_impigv(double ven_impigv) {
        this.ven_impigv = ven_impigv;
    }

    public double getVen_imptot() {
        return ven_imptot;
    }

    public void setVen_imptot(double ven_imptot) {
        this.ven_imptot = ven_imptot;
    }

    public String getVen_liqdesimp() {
        return ven_liqdesimp;
    }

    public void setVen_liqdesimp(String ven_liqdesimp) {
        this.ven_liqdesimp = ven_liqdesimp;
    }

    public double getVen_liqimpbruto() {
        return ven_liqimpbruto;
    }

    public void setVen_liqimpbruto(double ven_liqimpbruto) {
        this.ven_liqimpbruto = ven_liqimpbruto;
    }

    public double getVen_liqimpigv() {
        return ven_liqimpigv;
    }

    public void setVen_liqimpigv(double ven_liqimpigv) {
        this.ven_liqimpigv = ven_liqimpigv;
    }

    public double getVen_liqimptot() {
        return ven_liqimptot;
    }

    public void setVen_liqimptot(double ven_liqimptot) {
        this.ven_liqimptot = ven_liqimptot;
    }

    public String getVen_nom() {
        return ven_nom;
    }

    public void setVen_nom(String ven_nom) {
        this.ven_nom = ven_nom;
    }

    public String getVen_numdoc() {
        return ven_numdoc;
    }

    public void setVen_numdoc(String ven_numdoc) {
        this.ven_numdoc = ven_numdoc;
    }

    public String getVen_numide() {
        return ven_numide;
    }

    public void setVen_numide(String ven_numide) {
        this.ven_numide = ven_numide;
    }

    public String getVen_estado() {
        return ven_estado;
    }

    public void setVen_estado(String ven_estado) {
        this.ven_estado = ven_estado;
    }

    public String getVou_cod() {
        return vou_cod;
    }

    public void setVou_cod(String vou_cod) {
        this.vou_cod = vou_cod;
    }

    public double getVen_impdcto() {
        return ven_impdcto;
    }

    public void setVen_impdcto(double ven_impdcto) {
        this.ven_impdcto = ven_impdcto;
    }

    public String getVen_nomdoc() {
        return ven_nomdoc;
    }

    public void setVen_nomdoc(String ven_nomdoc) {
        this.ven_nomdoc = ven_nomdoc;
    }

    public String getVen_nomcom() {
        return ven_nomcom;
    }

    public void setVen_nomcom(String ven_nomcom) {
        this.ven_nomcom = ven_nomcom;
    }

    public String getVen_numcom() {
        return ven_numcom;
    }

    public void setVen_numcom(String ven_numcom) {
        this.ven_numcom = ven_numcom;
    }
}
