/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comun.entidad;

import java.io.Serializable;

/**
 *
 * @author ROMULO
 */
public class Operacion implements EntidadListener {

    private String ope_cod;
    private String ope_nom;
    private String ope_des;

    public String getOpe_cod() {
        return ope_cod;
    }

    public void setOpe_cod(String ope_cod) {
        this.ope_cod = ope_cod;
    }

    public String getOpe_des() {
        return ope_des;
    }

    public void setOpe_des(String ope_des) {
        this.ope_des = ope_des;
    }

    public String getOpe_nom() {
        return ope_nom;
    }

    public void setOpe_nom(String ope_nom) {
        this.ope_nom = ope_nom;
    }

    public void imprimirValores() {

        System.out.println("ope_cod : " + ope_cod);
        System.out.println("ope_nom : " + ope_nom);
        System.out.println("ope_des : " + ope_des);
        
    }

    public void setObjeto(Operacion parObj) {

        ope_cod = parObj.getOpe_cod();
        ope_nom = parObj.getOpe_nom();
        ope_des = parObj.getOpe_des();

    }

    public final void limpiarInstancia() {

        ope_cod = "";
        ope_nom = "";
        ope_des = "";

    }
}
