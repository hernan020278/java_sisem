/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comun.entidad;

public class Cliente  implements EntidadListener {

	private int cli_cod;
	private String cli_ide;
	private String cli_doc;
	private String cli_num;
	private String cli_nom;
	private String cli_dir;
	private String cli_tel;
	private String cli_email;
	private String cli_web;
	private String cli_act;
	private int cli_ver;

	public void imprimiValores() {

		System.out.println(cli_cod + " : " + cli_ide + " : " + cli_doc + " : " + cli_num + " : "
				+ cli_nom + " : " + cli_dir + " : " + cli_tel + " : " + cli_email + " : " + cli_web + " : "
				+ cli_act + " : " + cli_ver);
	}

	public void setObjeto(Cliente parObj){
		
		cli_cod = parObj.getCli_cod();
		cli_ide =parObj.getCli_ide();
		cli_doc =parObj.getCli_doc();
		cli_num = parObj.getCli_num();
		cli_nom = parObj.getCli_nom();
		cli_dir = parObj.getCli_dir();
		cli_tel = parObj.getCli_tel();
		cli_email = parObj.getCli_email();
		cli_web = parObj.getCli_web();
		cli_act = parObj.getCli_act();
		cli_ver = parObj.getCli_ver();

	}
	public void limpiarInstancia() {

		cli_cod = 0;
		cli_ide = "";
		cli_doc = "";
		cli_num = "";
		cli_nom = "";
		cli_dir = "";
		cli_tel = "";
		cli_email = "";
		cli_web = "";
		cli_act = "";
		cli_ver = 0;

	}// Fin de limpiarInstancia()

	public String getCli_ide() {
		return cli_ide;
	}

	public void setCli_ide(String cli_ide) {
		this.cli_ide = cli_ide;
	}

	public String getCli_doc() {
		return cli_doc;
	}

	public void setCli_doc(String cli_doc) {
		this.cli_doc = cli_doc;
	}

	public String getCli_num() {
		return cli_num;
	}

	public void setCli_num(String cli_num) {
		this.cli_num = cli_num;
	}

	public String getCli_nom() {
		return cli_nom;
	}

	public void setCli_nom(String cli_nom) {
		this.cli_nom = cli_nom;
	}

	public String getCli_dir() {
		return cli_dir;
	}

	public void setCli_dir(String cli_dir) {
		this.cli_dir = cli_dir;
	}

	public String getCli_tel() {
		return cli_tel;
	}

	public void setCli_tel(String cli_tel) {
		this.cli_tel = cli_tel;
	}

	public String getCli_email() {
		return cli_email;
	}

	public void setCli_email(String cli_email) {
		this.cli_email = cli_email;
	}

	public String getCli_web() {
		return cli_web;
	}

	public void setCli_web(String cli_web) {
		this.cli_web = cli_web;
	}

	public String getCli_act() {
		return cli_act;
	}

	public void setCli_act(String cli_act) {
		this.cli_act = cli_act;
	}

	public int getCli_cod() {
		return cli_cod;
	}

	public void setCli_cod(int cli_cod) {
		this.cli_cod = cli_cod;
	}

	public int getCli_ver() {
		return cli_ver;
	}

	public void setCli_ver(int cli_ver) {
		this.cli_ver = cli_ver;
	}

}// Fin de Clase principal
