package com.comun.entidad;

import java.io.Serializable;

public class MatrizEstado implements EntidadListener {

    @SuppressWarnings("compatibility:-2739550615336447052")
    private static final long serialVersionUID = 1L;
    private String ind_cod;
    private String matest_est;
    private int matest_ref;
    private int matest_idx;
    private int matest_pac;
    private double matest_prob;


    public void setInd_cod(String ind_cod) {
        this.ind_cod = ind_cod;
    }

    public String getInd_cod() {
        return ind_cod;
    }

    public void setMatest_est(String matest_est) {
        this.matest_est = matest_est;
    }

    public String getMatest_est() {
        return matest_est;
    }

    public void setMatest_ref(int matest_ref) {
        this.matest_ref = matest_ref;
    }

    public int getMatest_ref() {
        return matest_ref;
    }

    public void setMatest_idx(int matest_idx) {
        this.matest_idx = matest_idx;
    }

    public int getMatest_idx() {
        return matest_idx;
    }

    public void setMatest_pac(int matest_pac) {
        this.matest_pac = matest_pac;
    }

    public int getMatest_pac() {
        return matest_pac;
    }

    public void setMatest_prob(double matest_prob) {
        this.matest_prob = matest_prob;
    }

    public double getMatest_prob() {
        return matest_prob;
    }
}
