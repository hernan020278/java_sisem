package com.comun.entidad;

import java.math.BigDecimal;
import java.sql.Date;

public class FicControl implements EntidadListener {

	private BigDecimal ficcodigo;
	private BigDecimal prdcodigo;
	private BigDecimal pedcodigo;
	private BigDecimal detpedcodigo;
	private String identidad;
	private BigDecimal arecodigo;
	private BigDecimal subarecodigo;
	private BigDecimal usucodigo;
	private BigDecimal cmpcodigo;
	private BigDecimal prccodigo;
	private Date fecingreso;
	private Date fecsalida;
	private String unidad;
	private BigDecimal canpedido;
	private BigDecimal canfallado;
	private BigDecimal canrealizado;
	private BigDecimal cancontabilizado;
	private BigDecimal tcmpesperado;
	private BigDecimal tcmprealizado;
	private BigDecimal tcmpcontabilizado;
	private String observacion;
	private String barra;
	private BigDecimal ficnivel;
	private BigDecimal ficsupuno;
	private BigDecimal ficsupdos;
	private BigDecimal ficsuptres;
	private BigDecimal ficsupcua;
	private String estado;
	private BigDecimal version;

	public void setObjeto(FicControl parObj) {

		ficcodigo = parObj.getFiccodigo();
		prdcodigo = parObj.getPrdcodigo();
		pedcodigo = parObj.getPedcodigo();
		detpedcodigo = parObj.getDetpedcodigo();
		identidad = parObj.getIdentidad();
		arecodigo = parObj.getArecodigo();
		subarecodigo = parObj.getSubarecodigo();
		usucodigo = parObj.getUsucodigo();
		cmpcodigo = parObj.getCmpcodigo();
		prccodigo = parObj.getPrccodigo();
		fecingreso = parObj.getFecingreso();
		fecsalida = parObj.getFecsalida();
		unidad = parObj.getUnidad();
		canpedido = parObj.getCanpedido();
		canfallado = parObj.getCanfallado();
		canrealizado = parObj.getCanrealizado();
		cancontabilizado = parObj.getCancontabilizado();
		tcmpesperado = parObj.getTcmpesperado();
		tcmprealizado = parObj.getTcmprealizado();
		tcmpcontabilizado = parObj.getTcmpcontabilizado();
		observacion = parObj.getObservacion();
		barra = parObj.getBarra();
		ficnivel = parObj.getFicnivel();
		ficsupuno = parObj.getFicsupuno();
		ficsupdos = parObj.getFicsupdos();
		ficsuptres = parObj.getFicsuptres();
		ficsupcua = parObj.getFicsupcua();
		estado = parObj.getEstado();
		version = parObj.getVersion();
	}// Fin de public void setObjeto()

	public BigDecimal getUsucodigo() {

		return usucodigo;
	}

	public void setUsucodigo(BigDecimal usucodigo) {

		this.usucodigo = usucodigo;
	}

	public BigDecimal getPrccodigo() {

		return prccodigo;
	}

	public void setPrccodigo(BigDecimal prccodigo) {

		this.prccodigo = prccodigo;
	}

	public BigDecimal getFicnivel() {

		return ficnivel;
	}

	public void setFicnivel(BigDecimal ficnivel) {

		this.ficnivel = ficnivel;
	}

	public BigDecimal getFiccodigo() {

		return ficcodigo;
	}

	public void setFiccodigo(BigDecimal ficcodigo) {

		this.ficcodigo = ficcodigo;
	}

	public BigDecimal getPrdcodigo() {

		return prdcodigo;
	}

	public void setPrdcodigo(BigDecimal prdcodigo) {

		this.prdcodigo = prdcodigo;
	}

	public BigDecimal getPedcodigo() {

		return pedcodigo;
	}

	public void setPedcodigo(BigDecimal pedcodigo) {

		this.pedcodigo = pedcodigo;
	}

	public BigDecimal getDetpedcodigo() {

		return detpedcodigo;
	}

	public void setDetpedcodigo(BigDecimal detpedcodigo) {

		this.detpedcodigo = detpedcodigo;
	}

	public String getIdentidad() {

		return identidad;
	}

	public void setIdentidad(String identidad) {

		this.identidad = identidad;
	}

	public BigDecimal getArecodigo() {

		return arecodigo;
	}

	public void setArecodigo(BigDecimal arecodigo) {

		this.arecodigo = arecodigo;
	}

	public BigDecimal getSubarecodigo() {

		return subarecodigo;
	}

	public void setSubarecodigo(BigDecimal subarecodigo) {

		this.subarecodigo = subarecodigo;
	}

	public BigDecimal getCmpcodigo() {

		return cmpcodigo;
	}

	public void setCmpcodigo(BigDecimal cmpcodigo) {

		this.cmpcodigo = cmpcodigo;
	}

	public Date getFecingreso() {

		return fecingreso;
	}

	public void setFecingreso(Date fecingreso) {

		this.fecingreso = fecingreso;
	}

	public Date getFecsalida() {

		return fecsalida;
	}

	public void setFecsalida(Date fecsalida) {

		this.fecsalida = fecsalida;
	}

	public String getUnidad() {

		return unidad;
	}

	public void setUnidad(String unidad) {

		this.unidad = unidad;
	}

	public BigDecimal getCanpedido() {

		return canpedido;
	}

	public void setCanpedido(BigDecimal canpedido) {

		this.canpedido = canpedido;
	}

	public BigDecimal getCanfallado() {

		return canfallado;
	}

	public void setCanfallado(BigDecimal canfallado) {

		this.canfallado = canfallado;
	}

	public BigDecimal getCanrealizado() {

		return canrealizado;
	}

	public void setCanrealizado(BigDecimal canrealizado) {

		this.canrealizado = canrealizado;
	}

	public BigDecimal getCancontabilizado() {

		return cancontabilizado;
	}

	public void setCancontabilizado(BigDecimal cancontabilizado) {

		this.cancontabilizado = cancontabilizado;
	}

	public BigDecimal getTcmpesperado() {

		return tcmpesperado;
	}

	public void setTcmpesperado(BigDecimal tcmpesperado) {

		this.tcmpesperado = tcmpesperado;
	}

	public BigDecimal getTcmprealizado() {

		return tcmprealizado;
	}

	public void setTcmprealizado(BigDecimal tcmprealizado) {

		this.tcmprealizado = tcmprealizado;
	}

	public BigDecimal getTcmpcontabilizado() {

		return tcmpcontabilizado;
	}

	public void setTcmpcontabilizado(BigDecimal tcmpcontabilizado) {

		this.tcmpcontabilizado = tcmpcontabilizado;
	}

	public String getObservacion() {

		return observacion;
	}

	public void setObservacion(String observacion) {

		this.observacion = observacion;
	}

	public String getBarra() {

		return barra;
	}

	public void setBarra(String barra) {

		this.barra = barra;
	}

	public BigDecimal getFicsupuno() {

		return ficsupuno;
	}

	public void setFicsupuno(BigDecimal ficsupuno) {

		this.ficsupuno = ficsupuno;
	}

	public BigDecimal getFicsupdos() {

		return ficsupdos;
	}

	public void setFicsupdos(BigDecimal ficsupdos) {

		this.ficsupdos = ficsupdos;
	}

	public BigDecimal getFicsuptres() {

		return ficsuptres;
	}

	public void setFicsuptres(BigDecimal ficsuptres) {

		this.ficsuptres = ficsuptres;
	}

	public BigDecimal getFicsupcua() {

		return ficsupcua;
	}

	public void setFicsupcua(BigDecimal ficsupcua) {

		this.ficsupcua = ficsupcua;
	}

	public String getEstado() {

		return estado;
	}

	public void setEstado(String estado) {

		this.estado = estado;
	}

	public BigDecimal getVersion() {

		return version;
	}

	public void setVersion(BigDecimal version) {

		this.version = version;
	}
}
