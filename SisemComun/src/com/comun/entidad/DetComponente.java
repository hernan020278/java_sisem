package com.comun.entidad;

import java.math.BigDecimal;

public class DetComponente implements EntidadListener {

	private BigDecimal detcmpcodigo;
	private BigDecimal prdcodigo;
	private BigDecimal cmpcodigo;
	private BigDecimal pedcodigo;
	private BigDecimal detpedcodigo;
	private String unidad;
	private BigDecimal cantidad;
	private String estado;
	private BigDecimal version;

	public void setObjeto(DetComponente parObj) {

		if(parObj != null) 
		{
			detcmpcodigo = parObj.getDetcmpcodigo();
			prdcodigo = parObj.getPrdcodigo();
			cmpcodigo = parObj.getCmpcodigo();
			pedcodigo = parObj.getPedcodigo();
			detpedcodigo = parObj.getDetpedcodigo();
			unidad = parObj.getUnidad();
			cantidad = parObj.getCantidad();
			estado = parObj.getEstado();
			version = parObj.getVersion();
		}
	}

	public BigDecimal getDetcmpcodigo() {

		return detcmpcodigo;
	}

	public void setDetcmpcodigo(BigDecimal detcmpcodigo) {

		this.detcmpcodigo = detcmpcodigo;
	}

	public BigDecimal getPrdcodigo() {

		return prdcodigo;
	}

	public void setPrdcodigo(BigDecimal prdcodigo) {

		this.prdcodigo = prdcodigo;
	}

	public BigDecimal getCmpcodigo() {

		return cmpcodigo;
	}

	public void setCmpcodigo(BigDecimal cmpcodigo) {

		this.cmpcodigo = cmpcodigo;
	}

	public BigDecimal getPedcodigo() {

		return pedcodigo;
	}

	public void setPedcodigo(BigDecimal pedcodigo) {

		this.pedcodigo = pedcodigo;
	}

	public BigDecimal getDetpedcodigo() {

		return detpedcodigo;
	}

	public void setDetpedcodigo(BigDecimal detpedcodigo) {

		this.detpedcodigo = detpedcodigo;
	}

	public String getUnidad() {

		return unidad;
	}

	public void setUnidad(String unidad) {

		this.unidad = unidad;
	}

	public BigDecimal getCantidad() {

		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {

		this.cantidad = cantidad;
	}

	public String getEstado() {

		return estado;
	}

	public void setEstado(String estado) {

		this.estado = estado;
	}

	public BigDecimal getVersion() {

		return version;
	}

	public void setVersion(BigDecimal version) {

		this.version = version;
	}
}// Fin de Clase principal
