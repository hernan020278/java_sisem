package com.comun.entidad;

public class Pregunta implements EntidadListener
{
	private int prg_kyprg;
	private int rsp_kyrsp;
	private String prg_codi;
	private String prg_tipo;
	private String prg_clas;
	private String prg_foto;
	private String prg_esta;
	private int prg_vers;
	public int getPrg_kyprg() {
		return prg_kyprg;
	}
	public void setPrg_kyprg(int prg_kyprg) {
		this.prg_kyprg = prg_kyprg;
	}
	public int getRsp_kyrsp() {
		return rsp_kyrsp;
	}
	public void setRsp_kyrsp(int rsp_kyrsp) {
		this.rsp_kyrsp = rsp_kyrsp;
	}
	public String getPrg_codi() {
		return prg_codi;
	}
	public void setPrg_codi(String prg_codi) {
		this.prg_codi = prg_codi;
	}
	public String getPrg_tipo() {
		return prg_tipo;
	}
	public void setPrg_tipo(String prg_tipo) {
		this.prg_tipo = prg_tipo;
	}
	public String getPrg_clas() {
		return prg_clas;
	}
	public void setPrg_clas(String prg_clas) {
		this.prg_clas = prg_clas;
	}
	public String getPrg_foto() {
		return prg_foto;
	}
	public void setPrg_foto(String prg_foto) {
		this.prg_foto = prg_foto;
	}
	public String getPrg_esta() {
		return prg_esta;
	}
	public void setPrg_esta(String prg_esta) {
		this.prg_esta = prg_esta;
	}
	public int getPrg_vers() {
		return prg_vers;
	}
	public void setPrg_vers(int prg_vers) {
		this.prg_vers = prg_vers;
	}
}
