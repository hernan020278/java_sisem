package com.comun.entidad;

public class SubDetKardex implements EntidadListener {

    private int subdetkar_cod;
    private int detkar_cod;
    private String kar_cod;
    private int detart_cod;
    private String art_cod;
    private String subdetkar_nom;
    private int subdetkar_can;
    private int subdetkar_sal;
    private int subdetkar_ing;
    private int subdetkar_egr;

    public String getArt_cod() {
        return art_cod;
    }

    public void setArt_cod(String art_cod) {
        this.art_cod = art_cod;
    }

    public int getDetart_cod() {
        return detart_cod;
    }

    public void setDetart_cod(int detart_cod) {
        this.detart_cod = detart_cod;
    }

    public int getDetkar_cod() {
        return detkar_cod;
    }

    public void setDetkar_cod(int detkar_cod) {
        this.detkar_cod = detkar_cod;
    }

    public String getKar_cod() {
        return kar_cod;
    }

    public void setKar_cod(String kar_cod) {
        this.kar_cod = kar_cod;
    }

    public int getSubdetkar_can() {
        return subdetkar_can;
    }

    public void setSubdetkar_can(int subdetkar_can) {
        this.subdetkar_can = subdetkar_can;
    }

    public int getSubdetkar_cod() {
        return subdetkar_cod;
    }

    public void setSubdetkar_cod(int subdetkar_cod) {
        this.subdetkar_cod = subdetkar_cod;
    }

    public String getSubdetkar_nom() {
        return subdetkar_nom;
    }

    public void setSubdetkar_nom(String subdetkar_nom) {
        this.subdetkar_nom = subdetkar_nom;
    }

    public int getSubdetkar_egr() {
        return subdetkar_egr;
    }

    public void setSubdetkar_egr(int subdetkar_egr) {
        this.subdetkar_egr = subdetkar_egr;
    }

    public int getSubdetkar_ing() {
        return subdetkar_ing;
    }

    public void setSubdetkar_ing(int subdetkar_ing) {
        this.subdetkar_ing = subdetkar_ing;
    }

    public int getSubdetkar_sal() {
        return subdetkar_sal;
    }

    public void setSubdetkar_sal(int subdetkar_sal) {
        this.subdetkar_sal = subdetkar_sal;
    }

    public void setObjeto(SubDetKardex parObj) {

        subdetkar_cod = parObj.subdetkar_cod;
        detkar_cod = parObj.getDetkar_cod();
        kar_cod = parObj.getKar_cod();
        detart_cod = parObj.getDetart_cod();
        art_cod = parObj.getArt_cod();
        subdetkar_nom = parObj.getSubdetkar_nom();
        subdetkar_can = parObj.getSubdetkar_can();
        subdetkar_sal = parObj.getSubdetkar_sal();
        subdetkar_ing = parObj.getSubdetkar_ing();
        subdetkar_egr = parObj.getSubdetkar_egr();

    }//Fin de public void setObjeto()

    public void limpiarInstancia() {

        subdetkar_cod = 0;
        detkar_cod = 0;
        kar_cod = "";
        detart_cod = 0;
        art_cod = "";
        subdetkar_nom = "";
        subdetkar_can = 0;
        subdetkar_sal = 0;
        subdetkar_ing = 0;
        subdetkar_egr = 0;

    }//Fin de limpiarInstancia()

    public void imprimirValores() {

        System.out.println("subdetkar_cod : " + subdetkar_cod + " detkar_cod : " + detkar_cod + " kar_cod : "
                + kar_cod + " detart_cod : " + detart_cod + " art_cod : " + art_cod + " subdetkar_can : "
                + subdetkar_can + " subdetkar_sal : " + subdetkar_sal + "·subdetkar_nom : " + subdetkar_nom 
                + " subdetkar_ing : " + subdetkar_ing + " subdetkar_egr " + subdetkar_egr);
    }//Fin de limpiarInstancia()
}
