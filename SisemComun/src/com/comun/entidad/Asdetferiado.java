package com.comun.entidad;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.Date;

import javax.swing.ImageIcon;

public class Asdetferiado implements EntidadListener {

	private BigDecimal codferiado;
	private BigDecimal turcodigo;
	private BigDecimal tipo;
	private Date fecha;
	private String turno;
	private BigDecimal horas;
	private String descripcion;

	public BigDecimal getCodferiado() {

		return codferiado;
	}

	public void setCodferiado(BigDecimal codferiado) {

		this.codferiado = codferiado;
	}

	public BigDecimal getTurcodigo() {

		return turcodigo;
	}

	public void setTurcodigo(BigDecimal turcodigo) {

		this.turcodigo = turcodigo;
	}

	public BigDecimal getTipo() {

		return tipo;
	}

	public void setTipo(BigDecimal tipo) {

		this.tipo = tipo;
	}

	public Date getFecha() {

		return fecha;
	}

	public void setFecha(Date fecha) {

		this.fecha = fecha;
	}

	public String getTurno() {

		return turno;
	}

	public void setTurno(String turno) {

		this.turno = turno;
	}

	public BigDecimal getHoras() {

		return horas;
	}

	public void setHoras(BigDecimal horas) {

		this.horas = horas;
	}

	public String getDescripcion() {

		return descripcion;
	}

	public void setDescripcion(String descripcion) {

		this.descripcion = descripcion;
	}
}
