package com.comun.entidad;

import java.sql.Date;

public class ORRepositorio implements EntidadListener {

    private String ctrl_cod;
    private String per_cod;
    private String horcod;
    private String rep_tip;
    private double rep_horrep;
    private double rep_horuso;
    private String rep_est;

    public String getCtrl_cod() {
        return ctrl_cod;
    }

    public void setCtrl_cod(String ctrl_cod) {
        this.ctrl_cod = ctrl_cod;
    }

    public String getHor_cod() {
        return horcod;
    }

    public void setHor_cod(String horcod) {
        this.horcod = horcod;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }

    public String getRep_est() {
        return rep_est;
    }

    public void setRep_est(String rep_est) {
        this.rep_est = rep_est;
    }

    public double getRep_horrep() {
        return rep_horrep;
    }

    public void setRep_horrep(double rep_horrep) {
        this.rep_horrep = rep_horrep;
    }

    public double getRep_horuso() {
        return rep_horuso;
    }

    public void setRep_horuso(double rep_horuso) {
        this.rep_horuso = rep_horuso;
    }

    public String getRep_tip() {
        return rep_tip;
    }

    public void setRep_tip(String rep_tip) {
        this.rep_tip = rep_tip;
    }
}
