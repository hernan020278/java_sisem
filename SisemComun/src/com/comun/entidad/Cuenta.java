package com.comun.entidad;

public class Cuenta implements EntidadListener {

    private int cta_numcta;
    private int cta_supuno;
    private int cta_supdos;
    private int cta_suptres;
    private int cta_nivel;
    private String cta_rubro;
    private String cta_tiprub;
    private String cta_grupo;
    private String cta_nom;
    private String cta_des;
    private int cta_numord;

    public String getCta_des() {
        return cta_des;
    }

    public void setCta_des(String cta_des) {
        this.cta_des = cta_des;
    }

    public String getCta_grupo() {
        return cta_grupo;
    }

    public void setCta_grupo(String cta_grupo) {
        this.cta_grupo = cta_grupo;
    }

    public int getCta_nivel() {
        return cta_nivel;
    }

    public void setCta_nivel(int cta_nivel) {
        this.cta_nivel = cta_nivel;
    }

    public String getCta_nom() {
        return cta_nom;
    }

    public void setCta_nom(String cta_nom) {
        this.cta_nom = cta_nom;
    }

    public int getCta_numcta() {
        return cta_numcta;
    }

    public void setCta_numcta(int cta_numcta) {
        this.cta_numcta = cta_numcta;
    }

    public String getCta_rubro() {
        return cta_rubro;
    }

    public void setCta_rubro(String cta_rubro) {
        this.cta_rubro = cta_rubro;
    }

    public int getCta_supdos() {
        return cta_supdos;
    }

    public void setCta_supdos(int cta_supdos) {
        this.cta_supdos = cta_supdos;
    }

    public int getCta_suptres() {
        return cta_suptres;
    }

    public void setCta_suptres(int cta_suptres) {
        this.cta_suptres = cta_suptres;
    }

    public int getCta_supuno() {
        return cta_supuno;
    }

    public void setCta_supuno(int cta_supuno) {
        this.cta_supuno = cta_supuno;
    }

    public String getCta_tiprub() {
        return cta_tiprub;
    }

    public void setCta_tiprub(String cta_tiprub) {
        this.cta_tiprub = cta_tiprub;
    }

    public int getCta_numord() {
        return cta_numord;
    }

    public void setCta_numord(int cta_numord) {
        this.cta_numord = cta_numord;
    }

    
    
    public void imprimirValores() {

        System.out.println("cta_numcta : " + cta_numcta + "\n"
                + "cta_supuno  : " + cta_supuno + "\n"
                + "cta_supdos : " + cta_supdos + "\n"
                + "cta_suptres : " + cta_suptres + "\n"
                + "cta_nivel : " + cta_nivel + "\n"
                + "cta_rubro : " + cta_rubro + "\n"
                + "cta_tiprub : " + cta_tiprub + "\n"
                + "cta_grupo  : " + cta_grupo + "\n"
                + "cta_nom : " + cta_nom + "\n"
                + "cta_des : " + cta_des + "\n"
                + "cta_numord : " + cta_numord);

    }

    public void setObjeto(Cuenta parObj) {

        cta_numcta = parObj.getCta_numcta();
        cta_supuno = parObj.getCta_supuno();
        cta_supdos = parObj.getCta_supdos();
        cta_suptres = parObj.getCta_suptres();
        cta_nivel = parObj.getCta_nivel();
        cta_rubro = parObj.getCta_rubro();
        cta_tiprub = parObj.getCta_tiprub();
        cta_grupo = parObj.getCta_grupo();
        cta_nom = parObj.getCta_nom();
        cta_des = parObj.getCta_des();
        cta_numord = parObj.getCta_numord();

    }//Fin de setObjeto()

    public void limpiarInstancia() {

        cta_numcta = 0;
        cta_supuno = 0;
        cta_supdos = 0;
        cta_suptres = 0;
        cta_nivel = 0;
        cta_rubro = "";
        cta_tiprub = "";
        cta_grupo = "";
        cta_nom = "";
        cta_des = "";
        cta_numord =0;

    }
}
