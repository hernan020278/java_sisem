package com.comun.entidad;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ObjetoNodo {

	private Map nodoCodigo;
	private String nodoDescripcion;
	private List browseFilters = new ArrayList();
	private String browseTabla;

	public ObjetoNodo(Map nodoCodigo, String nodoDescripcion, List browseFilter, String browseTabla) {
		this.nodoCodigo = nodoCodigo;
		this.nodoDescripcion = nodoDescripcion;
		this.browseFilters = browseFilter;
		this.browseTabla = browseTabla;
	}

	public Map getNodoCodigo() {
		return nodoCodigo;
	}

	public void setNodoCodigo(Map nodoCodigo) {
		this.nodoCodigo = nodoCodigo;
	}

	public String getNodoDescripcion() {
		return nodoDescripcion;
	}

	public void setNodoDescripcion(String nodoDescripcion) {
		this.nodoDescripcion = nodoDescripcion;
	}

	public List getBrowseFilters() {
		return browseFilters;
	}

	public void setBrowseFilters(List browseFilters) {
		this.browseFilters = browseFilters;
	}

	public String getBrowseTabla() {
		return browseTabla;
	}

	public void setBrowseTabla(String browseTabla) {
		this.browseTabla = browseTabla;
	}

	public String toString()
	{
		return this.nodoDescripcion;
	}

}
