package com.comun.entidad;

import java.math.BigDecimal;
import java.sql.Date;

public class Historia implements EntidadListener {

	private BigDecimal hiscodigo;
	private BigDecimal paccodigo;
	private Date fecinscripcion;
	private String diagnostico;
	private String estado;
	private BigDecimal version;
	
	public BigDecimal getHiscodigo() {
	
		return hiscodigo;
	}
	
	public void setHiscodigo(BigDecimal hiscodigo) {
	
		this.hiscodigo = hiscodigo;
	}
	
	public BigDecimal getPaccodigo() {
	
		return paccodigo;
	}
	
	public void setPaccodigo(BigDecimal paccodigo) {
	
		this.paccodigo = paccodigo;
	}
	
	public Date getFecinscripcion() {
	
		return fecinscripcion;
	}
	
	public void setFecinscripcion(Date fecinscripcion) {
	
		this.fecinscripcion = fecinscripcion;
	}
	
	public String getDiagnostico() {
	
		return diagnostico;
	}
	
	public void setDiagnostico(String diagnostico) {
	
		this.diagnostico = diagnostico;
	}
	
	public String getEstado() {
	
		return estado;
	}
	
	public void setEstado(String estado) {
	
		this.estado = estado;
	}
	
	public BigDecimal getVersion() {
	
		return version;
	}
	
	public void setVersion(BigDecimal version) {
	
		this.version = version;
	}
}
