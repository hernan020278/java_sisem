package com.comun.entidad;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
public class Asictrlasistencia implements EntidadListener {

	private BigDecimal kyctrlasistencia;
	private BigDecimal kyusuario;
	private String numerodni;
	private String periodo;
	private Date fechainicio;
	private Date fechafinal;
	private BigDecimal totlimentuno;
	private BigDecimal totlimentdos;
	private BigDecimal totlimentrada;
	private BigDecimal totlimsaluno;
	private BigDecimal totlimsaldos;
	private BigDecimal totlimsalida;
	private BigDecimal horaobligatoria;
	private BigDecimal horatrabajada;
	private BigDecimal horanotrabajada;
	private BigDecimal horaasistencia;
	private BigDecimal horainasistencia;
	private BigDecimal horarepositorio;
	private BigDecimal horaextrauno;
	private BigDecimal horaextrados;
	private BigDecimal horapermiso;
	private String estado;
	private Timestamp version;
}
