package com.comun.entidad;

import java.sql.Date;
import java.sql.Time;

public class ORAsistencia implements EntidadListener {

    private String det_cod;
    private String ctrl_cod;
    private String per_cod;
    private String horcod;
    private Date det_fec;
    private String det_turn;
    private Time det_horing;
    private String det_esting;
    private Time det_horsal;
    private String det_estsal;
    private double det_hortrab;
    private double det_horcons;
    private double det_horcont;
    private String det_modreg;
    private String det_fot;

    public String getCtrl_cod() {
        return ctrl_cod;
    }

    public void setCtrl_cod(String ctrl_cod) {
        this.ctrl_cod = ctrl_cod;
    }

    public String getDet_cod() {
        return det_cod;
    }

    public void setDet_cod(String det_cod) {
        this.det_cod = det_cod;
    }

    public String getDet_esting() {
        return det_esting;
    }

    public void setDet_esting(String det_esting) {
        this.det_esting = det_esting;
    }

    public String getDet_estsal() {
        return det_estsal;
    }

    public void setDet_estsal(String det_estsal) {
        this.det_estsal = det_estsal;
    }

    public Date getDet_fec() {
        return det_fec;
    }

    public void setDet_fec(Date det_fec) {
        this.det_fec = det_fec;
    }

    public String getDet_fot() {
        return det_fot;
    }

    public void setDet_fot(String det_fot) {
        this.det_fot = det_fot;
    }

    public double getDet_horcons() {
        return det_horcons;
    }

    public void setDet_horcons(double det_horcons) {
        this.det_horcons = det_horcons;
    }

    public double getDet_horcont() {
        return det_horcont;
    }

    public void setDet_horcont(double det_horcont) {
        this.det_horcont = det_horcont;
    }

    public Time getDet_horing() {
        return det_horing;
    }

    public void setDet_horing(Time det_horing) {
        this.det_horing = det_horing;
    }

    public Time getDet_horsal() {
        return det_horsal;
    }

    public void setDet_horsal(Time det_horsal) {
        this.det_horsal = det_horsal;
    }

    public double getDet_hortrab() {
        return det_hortrab;
    }

    public void setDet_hortrab(double det_hortrab) {
        this.det_hortrab = det_hortrab;
    }

    public String getDet_modreg() {
        return det_modreg;
    }

    public void setDet_modreg(String det_modreg) {
        this.det_modreg = det_modreg;
    }

    public String getDet_turn() {
        return det_turn;
    }

    public void setDet_turn(String det_turn) {
        this.det_turn = det_turn;
    }

    public String getHor_cod() {
        return horcod;
    }

    public void setHor_cod(String horcod) {
        this.horcod = horcod;
    }

    public String getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(String per_cod) {
        this.per_cod = per_cod;
    }
}
