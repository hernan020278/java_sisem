package com.comun.entidad;

public class Unidad  implements EntidadListener {

	private int und_cod;
	private String und_alias;
	private String und_nombre;
	private int und_pakcan;
	private int und_supuno;
	private int und_supdos;
	private int und_suptres;
	private int und_supcua;
	private int und_nivel;
	private int und_orden;
	private int und_ver;

	public void setObjeto(Unidad parObj) {

		und_cod = parObj.getUnd_cod();
		und_alias = parObj.getUnd_alias();
		und_nombre = parObj.getUnd_nombre();
		und_pakcan = parObj.getUnd_pakcan();
		und_supuno = parObj.getUnd_supuno();
		und_supdos = parObj.getUnd_supdos();
		und_suptres = parObj.getUnd_suptres();
		und_supcua = parObj.getUnd_supcua();
		und_nivel = parObj.getUnd_nivel();
		und_orden = parObj.getUnd_orden();
		und_ver = parObj.getUnd_ver();

	}// Fin de public void setObjeto()

	public void imprimirValores() {

		System.out.println(und_cod + " : " + und_alias + " : " + und_nombre + " : " + und_pakcan + " : "
				+ und_supuno + " : " + und_supdos + " : " + und_suptres + " : " + und_supcua + " : "
				+ und_nivel + " : " + und_orden + " : " + und_ver);
	}

	public void limpiarInstancia() {

		und_cod = 0;
		und_alias = "";
		und_nombre = "";
		und_pakcan = 0;
		und_supuno = 0;
		und_supdos = 0;
		und_suptres = 0;
		und_supcua = 0;
		und_nivel = 0;
		und_orden = 0;
		und_ver = 0;

	}// Fin de limpiarInstancia()

	public int getUnd_cod() {
		return und_cod;
	}

	public void setUnd_cod(int und_cod) {
		this.und_cod = und_cod;
	}

	public String getUnd_alias() {
		return und_alias;
	}

	public void setUnd_alias(String und_alias) {
		this.und_alias = und_alias;
	}

	public String getUnd_nombre() {
		return und_nombre;
	}

	public void setUnd_nombre(String und_nombre) {
		this.und_nombre = und_nombre;
	}

	public int getUnd_pakcan() {
		return und_pakcan;
	}

	public void setUnd_pakcan(int und_pakcan) {
		this.und_pakcan = und_pakcan;
	}

	public int getUnd_supuno() {
		return und_supuno;
	}

	public void setUnd_supuno(int und_supuno) {
		this.und_supuno = und_supuno;
	}

	public int getUnd_supdos() {
		return und_supdos;
	}

	public void setUnd_supdos(int und_supdos) {
		this.und_supdos = und_supdos;
	}

	public int getUnd_suptres() {
		return und_suptres;
	}

	public void setUnd_suptres(int und_suptres) {
		this.und_suptres = und_suptres;
	}

	public int getUnd_supcua() {
		return und_supcua;
	}

	public void setUnd_supcua(int und_supcua) {
		this.und_supcua = und_supcua;
	}

	public int getUnd_nivel() {
		return und_nivel;
	}

	public void setUnd_nivel(int und_nivel) {
		this.und_nivel = und_nivel;
	}

	public int getUnd_orden() {
		return und_orden;
	}

	public void setUnd_orden(int und_orden) {
		this.und_orden = und_orden;
	}

	public int getUnd_ver() {
		return und_ver;
	}

	public void setUnd_ver(int und_ver) {
		this.und_ver = und_ver;
	}

}
