package com.comun.entidad;

import java.math.BigDecimal;

public class Componente implements EntidadListener {

	private BigDecimal cmpcodigo;
	private String identidad;
	private String nombre;
	private String tipo;
	private String descripcion;
	private BigDecimal factor;
	private String estado;
	private BigDecimal version;

	public void setObjeto(Componente parObj) {

		if(parObj != null) {
			cmpcodigo = parObj.getCmpcodigo();
			identidad = parObj.getIdentidad();
			nombre = parObj.getNombre();
			tipo = parObj.getTipo();
			descripcion = parObj.getDescripcion();
			factor = parObj.getFactor();
			estado = parObj.getEstado();
			version = parObj.getVersion();
		}
	}

	public BigDecimal getFactor() {

		return factor;
	}

	public void setFactor(BigDecimal factor) {

		this.factor = factor;
	}

	public String getIdentidad() {

		return identidad;
	}

	public void setIdentidad(String identidad) {

		this.identidad = identidad;
	}

	public String getNombre() {

		return nombre;
	}

	public void setNombre(String nombre) {

		this.nombre = nombre;
	}

	public String getTipo() {

		return tipo;
	}

	public void setTipo(String tipo) {

		this.tipo = tipo;
	}

	public String getDescripcion() {

		return descripcion;
	}

	public void setDescripcion(String descripcion) {

		this.descripcion = descripcion;
	}

	public String getEstado() {

		return estado;
	}

	public void setEstado(String estado) {

		this.estado = estado;
	}

	public BigDecimal getCmpcodigo() {

		return cmpcodigo;
	}

	public void setCmpcodigo(BigDecimal cmpcodigo) {

		this.cmpcodigo = cmpcodigo;
	}

	public BigDecimal getVersion() {

		return version;
	}

	public void setVersion(BigDecimal version) {

		this.version = version;
	}
}// Fin de Clase principal
