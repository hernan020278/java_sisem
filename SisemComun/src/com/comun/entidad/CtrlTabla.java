package com.comun.entidad;

import java.io.Serializable;

public class CtrlTabla implements EntidadListener {

    private String ctrl_nomtab;
    private int ctrl_numreg;
    private int ctrl_numser;
    private double ctrl_valor;

    public String getCtrl_nomtab() {
        return ctrl_nomtab;
    }

    public void setCtrl_nomtab(String ctrl_nomtab) {
        this.ctrl_nomtab = ctrl_nomtab;
    }

    public int getCtrl_numreg() {
        return ctrl_numreg;
    }

    public void setCtrl_numreg(int ctrl_numreg) {
        this.ctrl_numreg = ctrl_numreg;
    }

    public int getCtrl_numser() {
        return ctrl_numser;
    }

    public void setCtrl_numser(int ctrl_numser) {
        this.ctrl_numser = ctrl_numser;
    }

    public double getCtrl_valor() {
        return ctrl_valor;
    }

    public void setCtrl_valor(double ctrl_valor) {
        this.ctrl_valor = ctrl_valor;
    }

    public void limpiarInstancia() {

        ctrl_nomtab = "";
        ctrl_numreg = 0;
        ctrl_numser = 0;
        ctrl_valor = 0.00;

    }//Fin de limpiarInstancia
}//Fin de clase principal
