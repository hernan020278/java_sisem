package com.comun.entidad;

public class DetVenta implements EntidadListener {

    private int detven_cod;
    private String ven_cod;
    private String art_cod;
    private int detven_can;
    private String detven_und;
    private String detven_des;
    private double detven_pre;
    private double detven_dcto;
    private double detven_predcto;
    private double detven_imp;
    private double detven_liqpre;
    private double detven_liqimp;

    public String getArt_cod() {
        return art_cod;
    }

    public void setArt_cod(String art_cod) {
        this.art_cod = art_cod;
    }

    public int getDetven_can() {
        return detven_can;
    }

    public void setDetven_can(int detven_can) {
        this.detven_can = detven_can;
    }

    public int getDetven_cod() {
        return detven_cod;
    }

    public void setDetven_cod(int detven_cod) {
        this.detven_cod = detven_cod;
    }

    public String getDetven_des() {
        return detven_des;
    }

    public void setDetven_des(String detven_des) {
        this.detven_des = detven_des;
    }

    public double getDetven_imp() {
        return detven_imp;
    }

    public void setDetven_imp(double detven_imp) {
        this.detven_imp = detven_imp;
    }

    public double getDetven_liqimp() {
        return detven_liqimp;
    }

    public void setDetven_liqimp(double detven_liqimp) {
        this.detven_liqimp = detven_liqimp;
    }

    public double getDetven_liqpre() {
        return detven_liqpre;
    }

    public void setDetven_liqpre(double detven_liqpre) {
        this.detven_liqpre = detven_liqpre;
    }

    public double getDetven_pre() {
        return detven_pre;
    }

    public void setDetven_pre(double detven_pre) {
        this.detven_pre = detven_pre;
    }

    public String getDetven_und() {
        return detven_und;
    }

    public void setDetven_und(String detven_und) {
        this.detven_und = detven_und;
    }

    public String getVen_cod() {
        return ven_cod;
    }

    public void setVen_cod(String ven_cod) {
        this.ven_cod = ven_cod;
    }

    public double getDetven_dcto() {
        return detven_dcto;
    }

    public void setDetven_dcto(double detven_dcto) {
        this.detven_dcto = detven_dcto;
    }

    public double getDetven_predcto() {
        return detven_predcto;
    }

    public void setDetven_predcto(double detven_predcto) {
        this.detven_predcto = detven_predcto;
    }
    
    

    public void setObjeto(DetVenta parDetVen) {

        detven_cod = parDetVen.getDetven_cod();
        art_cod = parDetVen.getArt_cod();
        ven_cod = parDetVen.getVen_cod();
        detven_can = parDetVen.getDetven_can();
        detven_und = parDetVen.getDetven_und();
        detven_des = parDetVen.getDetven_des();
        detven_pre = parDetVen.getDetven_pre();
        detven_dcto = parDetVen.getDetven_dcto();
        detven_predcto = parDetVen.getDetven_predcto();
        detven_imp = parDetVen.getDetven_imp();
        detven_liqpre = parDetVen.getDetven_liqpre();
        detven_liqimp = parDetVen.getDetven_liqimp();

    }//Fin de public void setObjeto()

    public void imprimirValores() {

        System.out.println("VAlores de DetVenta");
        System.out.println("===================");
        System.out.println("detven_cod : " + detven_cod);
        System.out.println("art_cod : " + art_cod);
        System.out.println("ven_cod : " + ven_cod);
        System.out.println("detven_can : " + detven_can);
        System.out.println("detven_und : " + detven_und);
        System.out.println("detven_des : " + detven_des);
        System.out.println("detven_pre : " + detven_pre);
        System.out.println("detven_dcto : " + detven_dcto);
        System.out.println("detven_predcto : " + detven_predcto);
        System.out.println("detven_imp : " + detven_imp);
        System.out.println("detven_liqpre : " + detven_liqpre);
        System.out.println("detven_liqimp : " + detven_liqimp);
    }

    public void limpiarInstancia() {

        detven_cod = 0;
        art_cod = "";
        ven_cod = "";
        detven_can = 0;
        detven_und = "";
        detven_des = "";
        detven_pre = 0.00;
        detven_dcto = 0.00;
        detven_predcto = 0.00;
        detven_imp = 0.00;
        detven_liqpre = 0.00;
        detven_liqimp = 0.00;

    }//Fin de limpiarInstancia()
}//Fin de clase Principal