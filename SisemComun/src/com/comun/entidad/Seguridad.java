package com.comun.entidad;

import java.sql.Date;

public class Seguridad implements EntidadListener {

	private String segcodigo;
	private String descripcion;
	private String propietario;
	private String estado;
	private Date fechaingreso;
	private Date fechafinal;
	private String sesionminuto;
	private int version;

	public void setObjeto(Seguridad parObj) {

		segcodigo = parObj.getSegcodigo();
		descripcion = parObj.getDescripcion();
		propietario = parObj.getPropietario();
		estado = parObj.getEstado();
		fechaingreso = parObj.getFechaingreso();
		fechafinal = parObj.getFechafinal();
		sesionminuto = parObj.getSesionminuto();
		version = parObj.getVersion();

	}// Fin de setObjeto

	public String getSegcodigo() {
		return segcodigo;
	}

	public void setSegcodigo(String segcodigo) {
		this.segcodigo = segcodigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(Date fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public Date getFechafinal() {
		return fechafinal;
	}

	public void setFechafinal(Date fechafinal) {
		this.fechafinal = fechafinal;
	}

	public String getSesionminuto() {
		return sesionminuto;
	}

	public void setSesionminuto(String sesionminuto) {
		this.sesionminuto = sesionminuto;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}// Fin de Clase principal
