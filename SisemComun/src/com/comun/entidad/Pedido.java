package com.comun.entidad;

import java.sql.Date;

public class Pedido implements EntidadListener {

	private int ped_cod;
	private int cli_cod;
	private String ped_nomdoc;
	private String ped_numdoc;
	private String ped_nom;
	private String ped_dir;
	private String ped_docide;
	private String ped_numide;
	private Date ped_fecreg;
	private Date ped_fecent;
	private String ped_temp;
	private double ped_imptot;
	private String ped_est;
	private String ped_src;
	private int ped_ver;

	public void imprimirValores() {

		System.out.println(ped_cod + " : " + cli_cod + " : " + ped_nomdoc + " : " + ped_numdoc + " : "
				+ ped_nom + " : " + ped_dir + " : " + ped_docide + " : " + ped_numide + " : "
				+ ped_fecreg + " : " + ped_fecent + " : " + ped_temp + " : " + ped_imptot + " : " + ped_est + " : " + ped_src + " : "
				+ ped_ver);
	}

	public void limpiarInstancia() {

		ped_cod = 0;
		ped_nomdoc = "";
		ped_numdoc = "";
		ped_nom = "";
		ped_dir = "";
		ped_docide = "";
		ped_numide = "";
		ped_fecreg = null;
		ped_fecent = null;
		ped_temp = "0";
		ped_imptot = 0.00;
		ped_est = "";
		ped_src = "";
		ped_ver = 0;

	}// Fin de limpiarInstancia

	public void setObjeto(Pedido parObj) {

		ped_cod = parObj.getPed_cod();
		cli_cod = parObj.getCli_cod();
		ped_nomdoc = parObj.getPed_nomdoc();
		ped_numdoc = parObj.getPed_numdoc();
		ped_nom = parObj.getPed_nom();
		ped_dir = parObj.getPed_dir();
		ped_docide = parObj.getPed_docide();
		ped_numide = parObj.getPed_numide();
		ped_fecreg = parObj.getPed_fecreg();
		ped_fecent = parObj.getPed_fecent();
		ped_temp = parObj.getPed_temp();
		ped_imptot = parObj.getPed_imptot();
		ped_est = parObj.getPed_est();
		ped_src = parObj.getPed_src();
		ped_ver = parObj.getPed_ver();

	}

	
	public String getPed_temp() {
		return ped_temp;
	}

	public void setPed_temp(String ped_temp) {
		this.ped_temp = ped_temp;
	}

	public int getPed_cod() {
		return ped_cod;
	}

	public void setPed_cod(int ped_cod) {
		this.ped_cod = ped_cod;
	}

	public String getPed_dir() {
		return ped_dir;
	}

	public void setPed_dir(String ped_dir) {
		this.ped_dir = ped_dir;
	}

	public String getPed_docide() {
		return ped_docide;
	}

	public void setPed_docide(String ped_docide) {
		this.ped_docide = ped_docide;
	}

	public String getPed_est() {
		return ped_est;
	}

	public void setPed_est(String ped_est) {
		this.ped_est = ped_est;
	}

	public Date getPed_fecreg() {
		return ped_fecreg;
	}

	public void setPed_fecreg(Date ped_fecreg) {
		this.ped_fecreg = ped_fecreg;
	}

	public Date getPed_fecent() {
		return ped_fecent;
	}

	public void setPed_fecent(Date ped_fecent) {
		this.ped_fecent = ped_fecent;
	}

	public double getPed_imptot() {
		return ped_imptot;
	}

	public void setPed_imptot(double ped_imptot) {
		this.ped_imptot = ped_imptot;
	}

	public String getPed_nom() {
		return ped_nom;
	}

	public void setPed_nom(String ped_nom) {
		this.ped_nom = ped_nom;
	}

	public String getPed_numdoc() {
		return ped_numdoc;
	}

	public void setPed_numdoc(String ped_numdoc) {
		this.ped_numdoc = ped_numdoc;
	}

	public String getPed_numide() {
		return ped_numide;
	}

	public void setPed_numide(String ped_numide) {
		this.ped_numide = ped_numide;
	}

	public int getPed_ver() {
		return ped_ver;
	}

	public void setPed_ver(int ped_ver) {
		this.ped_ver = ped_ver;
	}

	public String getPed_nomdoc() {
		return ped_nomdoc;
	}

	public void setPed_nomdoc(String ped_nomdoc) {
		this.ped_nomdoc = ped_nomdoc;
	}

	public int getCli_cod() {
		return cli_cod;
	}

	public void setCli_cod(int cli_cod) {
		this.cli_cod = cli_cod;
	}

	public String getPed_src() {
		return ped_src;
	}

	public void setPed_src(String ped_src) {
		this.ped_src = ped_src;
	}
}// Fin de clase Principal
