/*
 * Created on Aug 26, 2003
 */
package com.comun.referencia;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.propiedad.DiccionarioGeneral;

/**
 * @author Administrator
 */
public class Peticion
{
	public static final String AJAX = "AJAX";
	public static final String SUBMIT = "SUBMIT";
	public static final String BROWSEAJAX = "BROWSEAJAX";
	public static final String BROWSESUBMIT = "BROWSESUBMIT";
	public static final String SUBMITPOPUP = "SUBMITPOPUP";
}
