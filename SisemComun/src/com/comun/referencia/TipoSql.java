/*
 * Created on Aug 26, 2003
 */
package com.comun.referencia;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.propiedad.DiccionarioGeneral;

/**
 * @author Administrator
 */
public class TipoSql
{
	public static final String INSERT = "INSERT";
	public static final String UPDATE = "UPDATE";
	public static final String BATCH = "BATCH";
	public static final String DELETE = "DELETE";
	public static final String SELECT = "SELECT";
}
