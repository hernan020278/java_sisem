/*
 * Created on Aug 26, 2003
 */
package com.comun.referencia;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.propiedad.DiccionarioGeneral;
import com.comun.utilidad.Util;

/**
 * @author Administrator
 */
public class PapeletaTipo extends GeneralType
{
	public static final String HORARIO = "HR";
	public static final String SUSPENSION = "SP";
	public static final String AMONESTACION = "AM";
	public static final String VACACIONES = "VC";
	public static final String PERMISO = "PR";
	public static final String SALIDA = "SL";
	public static final String DESCANSO_MEDICO = "DS";
	public static final String CITA_MEDICA = "CM";
	public static final String JUSTIFICACION_FALTA = "JF";
	public static final String REFRIGERIO = "RF";
	
	public static String toClave(String valor)
	{
		return (String) GeneralType.obtenerClavePropiedad("papeleta-tipo", "", valor);
	}

	public static String toValor(String clave)
	{
		return (String) GeneralType.obtenerValorPropiedad("papeleta-tipo", "", clave);
	}

	public static String toString(String status, String organizationId)
	{
		String docStatus = "";
		try
		{
			if (status == null || status.equals("null"))
			{
				return "";
			}
			else
			{
				docStatus = DiccionarioGeneral.getInstance("papeleta-tipo", organizationId).getPropiedad(status, "no hay");

			}
		}
		catch (Exception e) {
			throw new UnsupportedOperationException("Not supported yet.");
		}
		return docStatus;
	}

	public static List getAllValues(String organizationId)
	{
		return getAllValues("papeleta-tipo", organizationId);
	}

	public static Map getPropiedadMap(String organizationId, String startCode, String lastCode)
	{
		return getMapaPropiedadOrderedByIntKey("papeleta-tipo", organizationId, startCode, lastCode);
	}

	public static String getClave(String resource, String organizationId, String valor)
	{
		return obtenerClavePropiedad(resource, organizationId, valor);
	}

	public static void toValorTest()
	{
		System.out.println("//********** toValorTEST ***********************/");
		Class status = PapeletaTipo.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				System.out.println(PapeletaTipo.toString((String) fields[i].get(status), ""));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void printAllValues()
	{
		Class status = PapeletaTipo.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				System.out.println(fields[i].get(status) + " = " + fields[i].getName());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void toValueTest()
	{
		System.out.println("//********** TO VALUE TEST ***********************/");
		Class status = PapeletaTipo.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				String statusText = PapeletaTipo.toValor((String) fields[i].get(status));
				System.out.println(statusText + " = " + PapeletaTipo.toString(statusText, "chentty"));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static boolean comparar(String fuente, String destino)
	{
		if (fuente.equalsIgnoreCase(PapeletaTipo.toClave(destino)) || fuente.equalsIgnoreCase(PapeletaTipo.toValor(destino)) || fuente.equalsIgnoreCase(destino))
		{
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				// Estado.printAllValues();
				// Estado.toValorTest();
				
//				String a = "sss.sss.sss";
//				String ab = a.replaceFirst("\\.", "_");
//				System.out.println(ab);
//				System.out.println(PapeletaTipo.toClave("PERMANENTE"));
//				BigDecimal totalDiasUsuario = new BigDecimal(3);
//				BigDecimal totalProduccionUsuario = new BigDecimal(3978.246);
//				totalDiasUsuario = Util.getBigDecimalFormatted(totalDiasUsuario,2);
//				totalProduccionUsuario = Util.getBigDecimalFormatted(totalProduccionUsuario,2);
////				BigDecimal aa = ( totalDiasUsuario.compareTo(new BigDecimal(0)) ==1 && totalProduccionUsuario.compareTo(new BigDecimal(0)) == 1 && totalProduccionUsuario.compareTo(totalDiasUsuario) == 1) ? totalProduccionUsuario.divide(totalDiasUsuario): new BigDecimal(0);
//				System.out.println(totalProduccionUsuario.divide(totalDiasUsuario, BigDecimal.ROUND_HALF_UP));
				
				 List<String> listaEstados = getAllValues("sisem");
				 for (String valor : listaEstados) {
				
				 System.out.println("documento : " + valor);
				
				 }
			}
		});

	}	
}
