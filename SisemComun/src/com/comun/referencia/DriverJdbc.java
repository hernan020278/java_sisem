/*
 * Created on Aug 26, 2003
 */
package com.comun.referencia;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.propiedad.DiccionarioGeneral;

/**
 * @author Administrator
 */
public class DriverJdbc
{
	public static final String POOL = "POOL";
	public static final String WEB = "WEB";
	public static final String JDBC = "JDBC";
	public static final String MYSQL = "MYSQL";
	public static final String SQLSERVER = "SQLSERVER";
	public static final String ORACLE = "ORACLE";

	public static String toString(String status)
	{
		return DriverJdbc.toString(status, "chentty");
	}

	public static String toString(String status, String organizationId)
	{
		String docStatus = "";
		try
		{
			if (status == null || status.equals("null"))
			{
				return "";
			}
			else
			{
				docStatus = DiccionarioGeneral.getInstance("doc-status", organizationId).getPropiedad(status, "no hay");

			}
		}
		catch (Exception e) {
			throw new UnsupportedOperationException("Not supported yet.");
		}
		return docStatus;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// DriverJdbc.printAllValues();
				DriverJdbc.toStringTest();
				System.out.println(" valor : " + DriverJdbc.toString("0101", ""));
			}
		});

	}

	public static void toStringTest()
	{
		System.out.println("//********** TOSTRINGTEST ***********************/");
		Class status = DriverJdbc.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				System.out.println(DriverJdbc.toString((String) fields[i].get(status), ""));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void printAllValues()
	{
		Class status = DriverJdbc.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				System.out.println(fields[i].get(status) + " = " + fields[i].getName());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void toValueTest()
	{
		System.out.println("//********** TO VALUE TEST ***********************/");
		Class status = DriverJdbc.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				String statusText = DriverJdbc.toString((String) fields[i].get(status));
				System.out.println(statusText + " = " + DriverJdbc.toString(statusText, "chentty"));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}
