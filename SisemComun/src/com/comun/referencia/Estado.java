/*
 * Created on Aug 26, 2003
 */
package com.comun.referencia;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.propiedad.DiccionarioGeneral;
import com.comun.utilidad.Util;

/**
 * @author Administrator
 */
public class Estado extends GeneralType
{
	public static final int FAILED = 00;
	public static final int ABORTED = 10;
	public static final int ABORTING = 20;
	public static final int CANCELLED = 30;
	public static final int READY = 40;
	public static final int ACTIVE = 50;
	public static final int COMPLETING = 60;	
	public static final int COMPLETED = 70;
	public static final int SUCCEEDED = 80;
	public static final int DUPLICATED_REQUEST = 100;
	
	public static final String VENTA_ANULADA = "1000";
	public static final String VENTA_EMITIDA = "1010";
	public static final String VENTA_ESPERA = "1020";

	public static final String ALTA = "0101";
	public static final String NORMAL = "0102";
	public static final String BAJA = "0103";
	public static final String PRESENTE = "0104";
	public static final String AUSENTE = "0105";
	public static final String SINESTADO = "0106";
	public static final String HOSPITALIZACION = "0107";
	public static final String TRATAMIENTO = "0108";

	public static final String DELETED = "0000";
	public static final String TEMPORAL = "0001";
	public static final String PERMANENTE = "0002";
	public static final String INACTIVO = "0003";
	public static final String ENESPERA = "0004";
	public static final String REGISTRADO = "0005";

	public static final String INCOMPLETO = "1000";
	public static final String RECHAZADO = "1005";
	public static final String RELLAMADO = "1010";
	public static final String ENVIADO = "1015";
	public static final String PRODUCCION = "1025";
	public static final String TERMINADO = "1030";
	public static final String MODIFICAR = "1035";
	public static final String CLAVE = "020278";
	
  public static final String APROBADO = "APROBADO";
  public static final String DESAPROBADO = "DESAPROBADO";
  public static final String PENDIENTE = "PENDIENTE";
  public static final String MODIFICADO = "MODIFICADO";
  public static final String ACTIVO = "ACTIVO";
	public static final String GENERADO = "0201";
	public static final String EN_PROCESO = "0211";
	public static final String FINALIZADO = "0212";
	public static final String EN_ESPERA = "0213";
	public static final String VIGENTE = "0214";
	public static final String HORARIO = "HORARIO";
	public static final String HORARIO_GENERAL = "HORARIO_GENERAL";
	public static final String DESACTIVO = "INACTIVO";


	
	public static String toClave(String valor)
	{
		return (String) GeneralType.obtenerClavePropiedad("doc-status", "", valor);
	}

	public static String toValor(String clave)
	{
		return (String) GeneralType.obtenerValorPropiedad("doc-status", "", clave);
	}

	public static String toString(String status, String organizationId)
	{
		String docStatus = "";
		try
		{
			if (status == null || status.equals("null"))
			{
				return "";
			}
			else
			{
				docStatus = DiccionarioGeneral.getInstance("doc-status", organizationId).getPropiedad(status, "no hay");

			}
		}
		catch (Exception e) {
			throw new UnsupportedOperationException("Not supported yet.");
		}
		return docStatus;
	}

	public static List getAllValues(String organizationId)
	{
		return getAllValues("doc-status", organizationId);
	}

	public static Map getPropiedadMap(String organizationId, String startCode, String lastCode)
	{
		return getMapaPropiedadOrderedByIntKey("doc-status", organizationId, startCode, lastCode);
	}

	public static String getClave(String resource, String organizationId, String valor)
	{
		return obtenerClavePropiedad(resource, organizationId, valor);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				// Estado.printAllValues();
				// Estado.toValorTest();
				
				String a = "sss.sss.sss";
				String ab = a.replaceFirst("\\.", "_");
				System.out.println(ab);
				System.out.println(Estado.toClave("PERMANENTE"));
				BigDecimal totalDiasUsuario = new BigDecimal(3);
				BigDecimal totalProduccionUsuario = new BigDecimal(3978.246);
				totalDiasUsuario = Util.getBigDecimalFormatted(totalDiasUsuario,2);
				totalProduccionUsuario = Util.getBigDecimalFormatted(totalProduccionUsuario,2);
//				BigDecimal aa = ( totalDiasUsuario.compareTo(new BigDecimal(0)) ==1 && totalProduccionUsuario.compareTo(new BigDecimal(0)) == 1 && totalProduccionUsuario.compareTo(totalDiasUsuario) == 1) ? totalProduccionUsuario.divide(totalDiasUsuario): new BigDecimal(0);
				System.out.println(totalProduccionUsuario.divide(totalDiasUsuario, BigDecimal.ROUND_HALF_UP));
				
//				System.out.println(Estado.toClave("Permanente"));
				// List<String> listaEstados = getListaEstadosDocumento("doc-status");
				// for (String valor : listaEstados) {
				//
				// System.out.println("documento : " + valor);
				//
				// }
				// System.out.println("ddd : " + getClavePropiedad("doc-status", "", "Venta Anulada"));
			}
		});

	}

	public static void toValorTest()
	{
		System.out.println("//********** toValorTEST ***********************/");
		Class status = Estado.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				System.out.println(Estado.toString((String) fields[i].get(status), ""));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void printAllValues()
	{
		Class status = Estado.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				System.out.println(fields[i].get(status) + " = " + fields[i].getName());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void toValueTest()
	{
		System.out.println("//********** TO VALUE TEST ***********************/");
		Class status = Estado.class;
		Field fields[] = status.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				String statusText = Estado.toValor((String) fields[i].get(status));
				System.out.println(statusText + " = " + Estado.toString(statusText, "chentty"));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static boolean comparar(String fuente, String destino)
	{
		if (fuente.equalsIgnoreCase(Estado.toClave(destino)) || fuente.equalsIgnoreCase(Estado.toValor(destino)) || fuente.equalsIgnoreCase(destino))
		{
			return true;
		}
		return false;
	}

}
