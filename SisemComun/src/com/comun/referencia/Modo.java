/*
 * Created on Aug 26, 2003
 */
package com.comun.referencia;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.comun.propiedad.DiccionarioGeneral;

/**
 * @author Administrator
 */
public class Modo
{
	public static final String ADMINISTRANDO = "ADMINISTRANDO";
	public static final String VISUALIZANDO = "VISUALIZANDO";
	public static final String AGREGANDO = "AGREGANDO";
	public static final String MODIFICANDO = "MODIFICANDO";
	public static final String ELIMINANDO = "ELIMINANDO";
	public static final String BUSCANDO = "BUSCANDO";
	public static final String SUSPENDIDO = "SUSPENDIDO";
	public static final String GENERANDO = "GENERANDO";
	public static final String INICIAR_EVENTO = "INICIAR EVENTO";
	public static final String FINALIZANDO_EVENTO = "FINALIZANDO_EVENTO";
	public static final String EVENTO_ENESPERA = "EVENTO EN ESPERA";
	
	public static final String PERMISO = "PERMISO";
	public static final String FERIADO = "FERIADO";
	public static final String JUSTIFICADO = "JUSTIFICADO";
	public static final String SIN_REGISTRO = "SIN_REGISTRO";
	public static final String SIN_TURNO = "SIN_TURNO";
	
	public static final String INGRESO = "INGRESO";
//	public static final String INGRESO_PUNTUAL = "ING-PUNTUAL";
	public static final String INGRESO_LIM_1 = "TOLERANCIA";
	public static final String INGRESO_LIM_2 = "TARDE";
	public static final String INGRESO_LIM_3 = "ING-FALTA-NOLAB";
	public static final String INGRESO_LIM_4 = "ING-FALTA-SINPA";
	public static final String INGRESO_FUERA_TURNO = "ING-FUERA-TURNO";
	public static final String INGRESO_MULTIPLE = "ING-MULTIPLE";
	public static final String INGRESO_PERMISO = "ING-PERMISO";
	public static final String INGRESO_FERIADO = "ING-FERIADO";
	public static final String INGRESO_VACACION = "ING-VACACION";
	
	public static final String SALIDA = "SALIDA";
	public static final String SALESP = "SALESP";
//	public static final String SALIDA_PUNTUAL = "SAL-PUNTUAL";
	public static final String SALIDA_LIM_1 = "SAL-ANTES-HORA";
	public static final String SALIDA_LIM_2 = "SAL-ANTES-HORA";
	public static final String SALIDA_LIM_3 = "SAL-ANTES-HORA";
	public static final String SALIDA_LIM_4 = "SAL-ANTES-HORA";
	public static final String SALIDA_ANTES_HORA = "SAL-ANTES-HORA";
	public static final String SALIDA_FUERA_TURNO = "SAL-FUERA-TURNO";
	public static final String SALIDA_MULTIPLE = "SAL-MULTIPLE";
	public static final String SALIDA_PERMISO = "SAL-PERMISO";
	public static final String SALIDA_FERIADO = "SAL-FERIADO";
	public static final String SALIDA_VACACION = "SAL-VACACION";
	
	public static final String FALTA = "FALTA";

	public Modo toEnum(String modo) 
	{
		Class obj = Modo.class;
		Field fields[] = obj.getFields();
		for (int i = 0; i < fields.length; i++)
		{
			try
			{
				System.out.println(Estado.toString((String) fields[i].get(obj), ""));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return null;
		
	}
	
	public static void main(){
		
		Modo modo = new Modo();
//		Modo modo.toEnum(Modo.AGREGANDO);
		
		
		
	}

}
