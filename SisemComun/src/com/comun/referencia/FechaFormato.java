package com.comun.referencia;

public class FechaFormato {
  
  public static final String yyyyMMddHHmmssSSS = "yyyy-MM-dd HH:mm:ss.SSS";
  public static final String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
  public static final String yyyyMMddHHmm = "yyyy-MM-dd HH:mm";
  public static final String yyyyMMdd = "yyyy-MM-dd";
  public static final String ddMMyyyy = "dd/MM/yyyy";
  public static final String HHmmss = "HH:mm:ss";
  public static final String HHmm = "HH:mm";
}
