/**
 *
 */
package com.comun.referencia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.comun.propiedad.DiccionarioGeneral;

/**
 * @author Johnny Zapana
 */
public abstract class GeneralType
{
	protected static String toString(String resource, String type)
	{
		return "";
	}

	protected static String toString(String resource, String type, String organizationId)
	{
		return "";
	}

	protected static String toValue(String resource, String typeText, String organizationId)
	{
		Map propertyMap = DiccionarioGeneral.getInstance(resource, organizationId).obtenerMapaPropiedad();
		return (String) propertyMap.get(typeText);

	}

	protected static List getAllValues(String resource, String organizationId)
	{
		Map typeMap = DiccionarioGeneral.getInstance(resource, organizationId).obtenerMapaPropiedad();
		return new ArrayList(typeMap.values());
	}

	protected static Map getMapaPropiedadOrderedByValue(String resource, String organizationId)
	{
		Map orderedPropertyMap = new LinkedHashMap();

		Map propertyMap = DiccionarioGeneral.getInstance(resource, organizationId).obtenerMapaPropiedad();

		List keys = new ArrayList(propertyMap.keySet());

		List values = new ArrayList(propertyMap.values());

		Object[] arrayValues = values.toArray();

		Arrays.sort(arrayValues);

		for (int i = 0; i < arrayValues.length; i++)
		{
			orderedPropertyMap.put(keys.get(values.indexOf(arrayValues[i])), arrayValues[i]);
		}

		return orderedPropertyMap;
	}

	protected static Map getMapaPropiedadOrderedByKey(String resource, String organizationId)
	{
		Map orderedPropertyMap = new LinkedHashMap();

		Map propertyMap = DiccionarioGeneral.getInstance(resource, organizationId).obtenerMapaPropiedad();

		List keys = new ArrayList(propertyMap.keySet());

		List values = new ArrayList(propertyMap.values());

		Object[] arrayKeys = keys.toArray();

		Arrays.sort(arrayKeys);

		String key;

		for (int i = 0; i < arrayKeys.length; i++)
		{
			key = String.valueOf(arrayKeys[i]);

			if (key.indexOf('_') > 0)
			{
				key = key.replaceFirst("[a-z]_", "");
			}

			orderedPropertyMap.put(key, values.get(keys.indexOf(arrayKeys[i])));
		}

		return orderedPropertyMap;
	}

	protected static String obtenerClavePropiedad(String resource, String organizationId, String valor)
	{
		Map propertyMap = DiccionarioGeneral.getInstance(resource, organizationId).obtenerMapaPropiedad();

		List<String> keys = new ArrayList(propertyMap.keySet());
		List<String> values = new ArrayList(propertyMap.values());

		int indice = -1;
		for (int ite = 0; ite < values.size(); ite++) {
			if (values.get(ite).equalsIgnoreCase(valor)) {
				indice = ite;
				break;
			}
		}
		String resultado = "";
		if (indice > -1) {
			resultado = keys.get(indice);
		}
		return resultado;
	}
	protected static String obtenerValorPropiedad(String resource, String organizationId, String clave)
	{
		Map propertyMap = DiccionarioGeneral.getInstance(resource, organizationId).obtenerMapaPropiedad();

		List<String> keys = new ArrayList(propertyMap.keySet());
		List<String> values = new ArrayList(propertyMap.values());

		int indice = -1;
		for (int ite = 0; ite < values.size(); ite++) {
			if (keys.get(ite).equals(clave)) {
				indice = ite;
				break;
			}
		}
		String resultado = "";
		if (indice > -1) {
			resultado = values.get(indice);
		}
		return resultado;
	}

	protected static Map getMapaPropiedadOrderedByIntKey(String resource, String organizationId, String startCode, String lastCode)
	{
		Map orderedPropertyMap = new LinkedHashMap();

		Map propertyMap = DiccionarioGeneral.getInstance(resource, organizationId).obtenerMapaPropiedad();

		List keys = new ArrayList(propertyMap.keySet());

		List values = new ArrayList(propertyMap.values());

		Object[] arrayKeys = keys.toArray();

		Arrays.sort(arrayKeys, GeneralType.IntegerComparator);

		for (int i = 0; i < arrayKeys.length; i++)
		{
			if (Integer.parseInt((String) arrayKeys[i]) >= Integer.parseInt(startCode) && Integer.parseInt((String) arrayKeys[i]) <= Integer.parseInt(lastCode))
			{
				if (Integer.parseInt((String) arrayKeys[i]) != 3035 && Integer.parseInt((String) arrayKeys[i]) != 3040 && Integer.parseInt((String) arrayKeys[i]) != 3045
						&& Integer.parseInt((String) arrayKeys[i]) != 4000 && Integer.parseInt((String) arrayKeys[i]) != 4003) {
					orderedPropertyMap.put(arrayKeys[i], values.get(keys.indexOf(arrayKeys[i])));
				}
			}
		}

		return orderedPropertyMap;
	}

	private static Comparator IntegerComparator = new Comparator()
	{
		public int compare(Object key_1, Object key_2)
		{
			return Integer.parseInt((String) key_1) - Integer.parseInt((String) key_2);
		}
	};

}
