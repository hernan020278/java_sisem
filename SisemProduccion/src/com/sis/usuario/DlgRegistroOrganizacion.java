package com.sis.usuario;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Dates;
import com.comun.utilidad.Util;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.sis.main.PaginaGeneral;
import com.sis.main.SisemContextListener;
import com.sis.main.SisemListener;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;
import com.toedter.calendar.JDateChooser;
import java.awt.SystemColor;

public class DlgRegistroOrganizacion extends VistaAdatper implements VistaListener {

	public DlgRegistroOrganizacion(FrmInicio parent) {

		super(parent);
		setTitle("Administracion de las Areas");
		setResizable(false);
		initComponents();
		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		// Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));
		// Se pasa el conjunto de teclas al panel principal
		// super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);
		super.addWindowListener(new WindowAdapter() {

			@Override
			public void windowActivated(WindowEvent we) {

				frmAbierto = true;
			}
		});
	}

	private void initComponents() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);
		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);
		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 493, 236);
		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setBounds(341, 167, 130, 55);
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png")); // NOI18N
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdCerrar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdGuardar = new javax.swing.JButton();
		cmdGuardar.setBounds(23, 167, 142, 55);
		panelPrincipal.add(cmdGuardar);
		cmdGuardar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdGuardar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("login.png")); // NOI18N
		cmdGuardar.setText("Guardar");
		cmdGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdGuardar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		panFondo1 = new jcMousePanel.jcMousePanel();
		panFondo1.setBounds(377, 65, 94, 91);
		panelPrincipal.add(panFondo1);
		panFondo1.setIcon(AplicacionGeneral.getInstance().obtenerImagen("llave.png")); // NOI18N
		panFondo1.setLayout(null);
		jLabel1 = new javax.swing.JLabel();
		jLabel1.setBounds(23, 65, 68, 27);
		panelPrincipal.add(jLabel1);
		jLabel1.setFont(new Font("Tahoma", Font.BOLD, 14));
		jLabel1.setForeground(SystemColor.activeCaption);
		jLabel1.setText("Empresa");
		txtOrganiacionNombre = new JTextFieldChanged(60);
		txtOrganiacionNombre.setHorizontalAlignment(SwingConstants.LEFT);
		txtOrganiacionNombre.setBounds(91, 65, 276, 27);
		panelPrincipal.add(txtOrganiacionNombre);
		txtOrganiacionNombre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		JLabel lblEmail = new JLabel();
		lblEmail.setText("E-Mail");
		lblEmail.setForeground(SystemColor.activeCaption);
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEmail.setBounds(23, 95, 68, 27);
		panelPrincipal.add(lblEmail);
		JLabel lblRegistroDeEmpresa = new JLabel();
		lblRegistroDeEmpresa.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		lblRegistroDeEmpresa.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistroDeEmpresa.setText("Registro de Empresa");
		lblRegistroDeEmpresa.setForeground(SystemColor.activeCaption);
		lblRegistroDeEmpresa.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRegistroDeEmpresa.setBounds(23, 21, 448, 38);
		panelPrincipal.add(lblRegistroDeEmpresa);
		txtUsuarioEmail = new JTextFieldChanged(60);
		txtUsuarioEmail.setHorizontalAlignment(SwingConstants.LEFT);
		txtUsuarioEmail.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtUsuarioEmail.setBounds(91, 95, 276, 27);
		panelPrincipal.add(txtUsuarioEmail);
		txtUsuarioTelefono = new JTextFieldChanged(60);
		txtUsuarioTelefono.setHorizontalAlignment(SwingConstants.LEFT);
		txtUsuarioTelefono.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtUsuarioTelefono.setBounds(90, 125, 130, 27);
		panelPrincipal.add(txtUsuarioTelefono);
		JLabel lblTelefono = new JLabel();
		lblTelefono.setText("Telefono");
		lblTelefono.setForeground(SystemColor.activeCaption);
		lblTelefono.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTelefono.setBounds(22, 125, 68, 27);
		panelPrincipal.add(lblTelefono);
		cmdGuardar.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				cmdGuardarEvento();
			}
		});
		cmdCerrar.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				cmdCerrarEvento();
			}
		});
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {

			public void run() {

				try {
					DlgAcceso window = new DlgAcceso(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private javax.swing.JButton cmdCerrar;
	private javax.swing.JButton cmdGuardar;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JPanel panelPrincipal;
	private jcMousePanel.jcMousePanel panFondo1;
	private JTextFieldChanged txtOrganiacionNombre;
	private JTextFieldChanged txtUsuarioEmail;
	private JTextFieldChanged txtUsuarioTelefono;

	@Override
	public void iniciarFormulario() {

		try
		{
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR)) {
				frmAbierto = false;
				limpiarFormulario();
				activarFormulario(true);
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			else if(pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR)) {
				llenarFormulario();
			}
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setVisible(true);
	}

	@Override
	public void limpiarFormulario()
	{

		txtOrganiacionNombre.setText("");
		txtUsuarioEmail.setText("");
		txtUsuarioTelefono.setText("");
	}

	@Override
	public void llenarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void activarFormulario(boolean activo) {

		txtOrganiacionNombre.setEnabled(true);
		txtUsuarioEmail.setEnabled(true);
		txtUsuarioTelefono.setEnabled(true);
		cmdGuardar.setEnabled(true);
		cmdCerrar.setEnabled(true);
	}

	@Override
	public void refrescarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void obtenerDatoFormulario()
	{
		java.sql.Date fecha = new java.sql.Date(0);
		fecha.setYear(Util.getInst().getFechaSql().getYear());
		fecha.setMonth(Util.getInst().getFechaSql().getMonth());
		fecha.setDate(Util.getInst().getFechaSql().getDate() + 20);
		
		peticion.put("Organizacion_fechafinal", fecha);
		peticion.put("Organizacion_nombre", txtOrganiacionNombre.getText());
		peticion.put("Organizacion_", txtOrganiacionNombre.getText());
		peticion.put("Usuario_nombre", "USU-" + txtOrganiacionNombre.getText().substring(0, 4));
		peticion.put("Usuario_email", txtUsuarioEmail.getText());
		peticion.put("Usuario_telefono", txtUsuarioTelefono.getText());
		peticion.put("Usuario_cargo", "ORGANIZACION");
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario()
	{
		if(!Util.isEmpty(txtOrganiacionNombre.getText()))
		{
			if(!Util.isEmpty(txtUsuarioTelefono.getText()))
			{
				if(Util.isEmail(txtUsuarioEmail.getText()))
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	/*
	 * 
	 * 
	 * METODOS DESARROLLADOS POR EL USUARIO
	 */
	public void cmdGuardarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdGuardarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			if(!Util.isEmpty(aux.usu.getNombre()) && !Util.isEmpty(aux.org.getOrgcodigo()))
			{
				this.dispose();
				this.setVisible(false);
				new SisemListener().ejecutarModulo(OrganizacionGeneral.getOrgcodigo());
			}
		}
	}

	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			System.exit(0);
		}
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return null;
	}
}