package com.sis.browse;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseObject;
import com.browse.BrowseUtility;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.CeldaRenderGeneral;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;
import java.awt.event.MouseAdapter;

public class DlgBrowseTabla extends VistaAdatper implements VistaListener
{
	/*
	 * PARA CONTROLD E DATAOS DE LA BROWSE
	 */
	public int numeroBoton;
	/*
	 * CONTROLES DE JPANEL
	 */
	private JLabel etiTitulo;
	private JPanel panelPrincipal;
	private JButton cmdCerrar;
	private JButton cmdBuscar;
	private JPanel panelTabla;
	public PanelTablaGeneral panTabGen;
	private JPopupMenu popMenBrowse;
	private JMenuItem menAgregar;
	private JMenuItem menModificar;
	private JMenuItem menEliminar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {

			public void run() {

				try {
					DlgBrowseTabla window = new DlgBrowseTabla(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DlgBrowseTabla(FrmInicio parent)
	{

		super(parent);
		setTitle(this.getClass().getSimpleName() + " - Tabla General");
		setResizable(false);
		initialize();
		
		popMenBrowse = new JPopupMenu();

		menAgregar = new JMenuItem("Agregar Registro");
		menAgregar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				cmdAgregarEvento();
			}
		});
		menModificar = new JMenuItem("Modificar Registro");
		menModificar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
			}
		});
		menEliminar = new JMenuItem("Eliminar Registro");
		menEliminar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
			}
		});
		popMenBrowse.add(menAgregar);
		popMenBrowse.add(menModificar);
		popMenBrowse.add(menEliminar);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);
		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);
		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 900, 578);
		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);
		etiTitulo = new JLabel("Formulario de Opciones de Busqueda");
		etiTitulo.setForeground(new Color(102, 102, 0));
		etiTitulo.setBorder(new MatteBorder(1, 0, 1, 0, (Color) new Color(153, 102, 0)));
		etiTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		etiTitulo.setHorizontalTextPosition(SwingConstants.CENTER);
		etiTitulo.setFont(new Font("Bookman Old Style", Font.ITALIC, 26));
		etiTitulo.setBounds(5, 5, 885, 35);
		panelPrincipal.add(etiTitulo);
		cmdBuscar = new JButton("Buscar");
		cmdBuscar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdBuscar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));
		cmdBuscar.setMargin(new Insets(0, 0, 0, 0));
		cmdBuscar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdBuscarEvento();
			}
		});
		cmdBuscar.setBounds(5, 525, 117, 40);
		panelPrincipal.add(cmdBuscar);
		cmdCerrar = new JButton();
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdCerrarEvento();
			}
		});
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setToolTipText("Administre sus casos");
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setIconTextGap(0);
		cmdCerrar.setBounds(759, 525, 125, 40);
		panelPrincipal.add(cmdCerrar);
		panelTabla = new JPanel();
		panelTabla.setBorder(null);
		panelTabla.setBounds(5, 47, 885, 475);
		panelPrincipal.add(panelTabla);
		panelTabla.setLayout(null);
		
		panTabGen = new PanelTablaGeneral(panelTabla.getWidth(), panelTabla.getHeight());
		panTabGen.scrtablaBrowse.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent evt) {
				panTabGenMosuseReleased(evt);
			}
		});
		panelTabla.add(panTabGen);
		panTabGen.cmdAgregar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdAgregarEvento();
			}
		});
		panTabGen.tablaBrowse.addMouseListener(new java.awt.event.MouseAdapter() {

			public void mousePressed(java.awt.event.MouseEvent evt) {

				panTabGenTablaBrowseMousePressed(evt);
			}
			public void mouseReleased(java.awt.event.MouseEvent evt) {

				panTabGenMosuseReleased(evt);
			}
		});

		panTabGen.tablaBrowse.addKeyListener(new java.awt.event.KeyAdapter() {

			public void keyReleased(java.awt.event.KeyEvent evt) {

				panTabGenTablaBrowseKeyReleased(evt);
			}
		});
		panTabGen.txtPaginaActual.addKeyListener(new KeyAdapter()
		{

			@Override
			public void keyReleased(KeyEvent e) {

				txtPaginaActualEvento(e);
			}
		});
		panTabGen.cmbTamanoPagina.addItemListener(new ItemListener()
		{

			public void itemStateChanged(ItemEvent e) {

				cmbTamanoPaginaEvento(e);
			}
		});		
	}

	@Override
	public void iniciarFormulario()
	{

		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName() + "-" + pagGen.pagAct.getPaginaPadre();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				browse = (Browse) peticion.get("browse");
				browseObject = (BrowseObject) peticion.get("browseObject");
				pagGen.pagAct.setBrowseTabla(browse.getBrowseTabla());
				filters = browseObject.getBrowseFilters();
				browseColumns = browseObject.getBrowseColumns();
				etiTitulo.setText(browseObject.getTitle());

				limpiarFormulario();
				activarFormulario(true);
				refrescarFormulario();
				llenarFormulario();
				frmAbierto = true;
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			else if(pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
				browse = (Browse) peticion.get("browse");
				browseObject = (BrowseObject) peticion.get("browseObject");
				pagGen.pagAct.setBrowseTabla(browse.getBrowseTabla());
				filters = browseObject.getBrowseFilters();
				browseColumns = browseObject.getBrowseColumns();
				etiTitulo.setText(browseObject.getTitle());

				limpiarFormulario();
				activarFormulario(true);
				refrescarFormulario();

				crearTablaResultado();
				llenarTabla();
				refrescarPanelTablaGeneral();
				frmAbierto = true;
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			this.show();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario() {

	}

	@Override
	public void llenarFormulario()
	{
		crearModeloTabla();
		crearTablaResultado();
		llenarTabla();
		refrescarPanelTablaGeneral();
	}



	@Override
	public void activarFormulario(boolean activo)
	{

		panTabGen.txtPaginaActual.setEnabled(activo);
		panTabGen.cmbTamanoPagina.setEnabled(activo);
		panTabGen.tablaBrowse.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario()
	{

		if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			cmdBuscar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			cmdBuscar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
			cmdBuscar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.BUSCANDO))
		{
			cmdBuscar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.SUSPENDIDO))
		{
			cmdBuscar.setEnabled(false);
			cmdCerrar.setEnabled(false);
		}
	}

	@Override
	public void obtenerDatoFormulario()
	{
		peticion.put("Pagina_paginaHijo", paginaHijo);
		peticion.put("Pagina_paginaPadre", paginaPadre);
		peticion.put("Pagina_evento", paginaEvento);
		peticion.put("Pagina_accion", paginaAccion);
		peticion.put("Pagina_valor", paginaValor);
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		boolean validado = true;
		return validado;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void setPeticion(Map peticion) {

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	/*
	 * METODOS DE USUARIO PARA CAPTURARA LOS EVENTOS DEL LOS CONTROLES DEL FORMULARIO
	 */
	public void cmdBuscarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdBuscarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdAgregarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdAgregarEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}
	
	public void cmdModificarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdModificarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdEliminarEvento()
	{
		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdEliminarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	public void cmdSeleccionarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdSeleccionarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}
	
	public void refrescarRegistrosEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				BrowseUtility.obtenerDatosBrowseFilter(peticion, filters);
				peticion.put("currentPage", String.valueOf(browse.getCurrentPage()));
				peticion.put("pageSize", String.valueOf(browse.getPageSize()));
				ejecutar("refrescarRegistrosEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return null;
	}


	private void tablaBrowsePressedEvento(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_tabDetVenMousePressed

		if(panTabGen.tablaBrowse.getSelectedRow() > -1)
		{
			MouseEvent me = (MouseEvent) evt;
			int col = panTabGen.tablaBrowse.columnAtPoint(me.getPoint());
			int row = panTabGen.tablaBrowse.rowAtPoint(me.getPoint());
			String value = (String) panTabGen.tablaBrowse.getValueAt(row, col);
			System.out.println("valor : " + value);
		}// FIN DE VERIFICAR QUE EL REGISTRO SELECCIONADO ESTE EN LA TABLA
	}// GEN-LAST:event_tabDetVenMousePressed
	
	
	
	
	
	
	
	
	
	
	
	
/*
 * METODOS PARA CONTROLAR EL MODELO DE LA TABLA GENERAL	
 */

	
	
	
	
	private void refrescarPanelTablaGeneral()
	{
		frmAbierto = false;
		String botonAgregar = browseObject.getBotonAgregar();
		if(!botonAgregar.equals(""))
		{
			botonAgregar = botonAgregar.replace("javascript: abrirPagina(", "");
			botonAgregar = botonAgregar.replace("'", "");
			botonAgregar = botonAgregar.replace(");", "");
			String[] valores = botonAgregar.split(",");
			paginaHijo = valores[0];
			paginaPadre = pagGen.pagAct.getPaginaHijo();
			paginaEvento = valores[1];
			paginaAccion = valores[2];
			paginaValor = "";
		}
		boolean valor = (botonAgregar.equals("")) ? false : true;
		panTabGen.cmdAgregar.setEnabled((botonAgregar.equals("")) ? false : true);
		for(int iteCmbPag = 0; iteCmbPag < panTabGen.cmbTamanoPagina.getItemCount(); iteCmbPag++)
		{
			String valorCombo = (String) panTabGen.cmbTamanoPagina.getItemAt(iteCmbPag);
			if(Util.isInteger(valorCombo))
			{
				if(browseObject.getPageSize() == Integer.parseInt(valorCombo))
				{
					panTabGen.cmbTamanoPagina.setSelectedIndex(iteCmbPag);
					break;
				}
			}
		}
		panTabGen.txtPaginaActual.setText(String.valueOf(browseObject.getCurrentPage()));
		
		if(panTabGen.modeloTablaGeneral.getRowCount() > 0)
		{
			panTabGen.tablaBrowse.setEnabled(true);
			panTabGen.txtPaginaActual.setEnabled(true);
			panTabGen.cmbTamanoPagina.setEnabled(true);
			for (int ite = 0; ite < panTabGen.cmdArrayPag.length; ite++)
			{
				panTabGen.cmdArrayPag[ite].setEnabled(true);
			}
		}
		else
		{
			if (panTabGen.cmdArrayPag != null) {
				for (int ite = 0; ite < panTabGen.cmdArrayPag.length; ite++)
				{
					panTabGen.remove(panTabGen.cmdArrayPag[ite]);
				}
			}
			panTabGen.repaint();
			panTabGen.etiInfo.setText("");
			panTabGen.txtPaginaActual.setValue(0);
			panTabGen.cmbTamanoPagina.setSelectedIndex(-1);
			panTabGen.tablaBrowse.setEnabled(false);
			panTabGen.txtPaginaActual.setEnabled(false);
			panTabGen.cmbTamanoPagina.setEnabled(false);
		}
		frmAbierto = true;
	}
	
	private void crearModeloTabla() {

		panTabGen.modeloTablaGeneral = new ModeloTablaGeneral(browseObject.getBrowseColumns());
		panTabGen.tablaBrowse.setModel(panTabGen.modeloTablaGeneral);
		panTabGen.celdaRenderGeneral = new CeldaRenderGeneral();
		try
		{
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.String"), panTabGen.celdaRenderGeneral);
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.Integer"), panTabGen.celdaRenderGeneral);
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.Double"), panTabGen.celdaRenderGeneral);
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.math.BigDecimal"), panTabGen.celdaRenderGeneral);
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.sql.Date"), panTabGen.celdaRenderGeneral);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
		// panTabGen.tablaBrowse.addPropertyChangeListener(new ModtablaBrowsePropiedad(this, tablaBrowse));

		panTabGen.tablaBrowse.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		// panTabGen.tablaBrowse.getTableHeader().setPreferredSize(new Dimension(panTabGen.tablaBrowse.getTableHeader().getWidth(), 25));
		// panTabGen.tablaBrowse.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 13));
		panTabGen.tablaBrowse.setRowHeight(25);
		panTabGen.tablaBrowse.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		panTabGen.tablaBrowse.setShowGrid(true);

		panTabGen.columna = new TableColumn[browseColumns.length];
		for (int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			panTabGen.columna[iteBrwCol] = panTabGen.tablaBrowse.getColumn(browseColumn.getColumnName());
			panTabGen.columna[iteBrwCol].setResizable(true);
			panTabGen.columna[iteBrwCol].setPreferredWidth(browseColumn.getSize() * 9);
			panTabGen.columna[iteBrwCol].setHeaderValue(browseColumn.getLabel());
			panTabGen.columna[iteBrwCol].setCellRenderer(obtenerCellRender(browseColumn));
			panTabGen.columna[iteBrwCol].setCellEditor(obtenerCellEdit(panTabGen.tablaBrowse, browseColumns, browseColumn));
			// columna[iteBrwCol].addPropertyChangeListener(new ListenerCambioPropiedad());

			if (browseColumn.isHidden())
			{
				panTabGen.tablaBrowse.getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				panTabGen.tablaBrowse.getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
				panTabGen.tablaBrowse.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				panTabGen.tablaBrowse.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
			}// Fin de if (ancho[iteBrwCol] == 0)
		}// Recorrer la tablaBrowse de encabezados
	}// Fin de crearModeloTabla()

	public TableCellRenderer obtenerCellRender(BrowseColumn browseColumn)
	{
		if (browseColumn.isButtonInput())
		{
			return new CellRenderJButton(obtenerBoton(browseColumn.getAlias()));
		}
		return panTabGen.celdaRenderGeneral;
	}

	public TableCellEditor obtenerCellEdit(JTable tabla, BrowseColumn[] browseColumns, BrowseColumn browseColumn)
	{

		if(browseColumn.isButtonInput())
		{
			return new CellEditJButton(obtenerBoton(browseColumn.getAlias()), this);
		}
		if(browseColumn.isTextInput())
		{
			return new CellEditDecimalTextInput(this, tabla, browseColumns);
		}
		return null;
	}

	public JButton obtenerBoton(String nombre)
	{
		JButton boton = new JButton(nombre);
		boton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		boton.setMargin(new Insets(0, 0, 0, 0));
		return boton;
	}

	public void crearTablaResultado()
	{
		List listResult = (List) browse.getPageResults();
		panTabGen.totalRegistros = browse.getRowCount();
		panTabGen.tamanoPagina = browse.getPageSize();
		panTabGen.totalPaginas = browse.getPageCount();
		panTabGen.paginaActual = browse.getCurrentPage();

		if (listResult != null && listResult.size() > 0)
		{
			// totalRegistros = listResult.size();
			panTabGen.visiblePages = 5;
			panTabGen.totalPaginas = panTabGen.totalRegistros / panTabGen.tamanoPagina;
			panTabGen.totalPaginas = (panTabGen.totalPaginas < 0) ? 0 : panTabGen.totalPaginas;
			panTabGen.resto = panTabGen.totalRegistros % panTabGen.tamanoPagina;
			panTabGen.totalPaginas = (panTabGen.resto > 0) ? panTabGen.totalPaginas + 1 : panTabGen.totalPaginas;
			
			if(panTabGen.totalPaginas < panTabGen.visiblePages)
			{
				panTabGen.visiblePages = panTabGen.totalPaginas;
			}
			
			for(int itePag = 1; itePag <= panTabGen.totalPaginas; itePag++)
			{
				int paginaActualReferencia = panTabGen.paginaActual;
				int inicioPagina = panTabGen.visiblePages * (itePag - 1);
				int finPagina = panTabGen.visiblePages * itePag;
				if(paginaActualReferencia > inicioPagina && paginaActualReferencia <= finPagina)
				{
					panTabGen.iteIni = itePag;
					panTabGen.iteFin = itePag + panTabGen.visiblePages - 1;
					break;
				}
			}
			panTabGen.paginaActual = panTabGen.paginaActual;
//			panTabGen.iniItePag = panTabGen.paginaActual;
			
//			if (panTabGen.totalPaginas <= panTabGen.visiblePages) {
//				panTabGen.visiblePages = panTabGen.totalPaginas;
//			}

			if (panTabGen.cmdArrayPag != null) {
				for (int ite = 0; ite < panTabGen.cmdArrayPag.length; ite++)
				{
					panTabGen.remove(panTabGen.cmdArrayPag[ite]);
				}
				panTabGen.cmdArrayPag=null;
			}
			panTabGen.repaint();
			crearBotonesPaginacion();
			panTabGen.repaint();
		}// Fin de if(listResult != null)
	}

	public void crearBotonesPaginacion() {

		panTabGen.numeroBoton = panTabGen.visiblePages;
		panTabGen.iteCmd = 0;
		panTabGen.numeroBoton = panTabGen.visiblePages + 2;

		panTabGen.cmdArrayPag = new JButton[panTabGen.numeroBoton];
		
		//CREAMOS EL BOTON DE ATRAS
		panTabGen.coordX = (panTabGen.iteCmd * panTabGen.cmdPagWidth) + panTabGen.marginLeft;
		panTabGen.cmdArrayPag[0] = crearBoton(panTabGen.coordX, panTabGen.coordY, panTabGen.cmdPagWidth, panTabGen.cmdPagHeigth, 0);
		panTabGen.add(panTabGen.cmdArrayPag[0]);
		panTabGen.iteCmd++;
		
		//CREAMOS LOS BOTONES VISIBLES
		for (int ite = panTabGen.iteIni; ite <= panTabGen.iteFin; ite++) {

			panTabGen.coordX = (panTabGen.iteCmd * panTabGen.cmdPagWidth) + panTabGen.marginLeft;
			panTabGen.cmdArrayPag[panTabGen.iteCmd] = crearBoton(panTabGen.coordX, panTabGen.coordY, panTabGen.cmdPagWidth, panTabGen.cmdPagHeigth, panTabGen.iteCmd);
			panTabGen.add(panTabGen.cmdArrayPag[panTabGen.iteCmd]);
			panTabGen.iteCmd++;
//			panTabGen.iniItePag++;

		}
		//CREAMOS EL BOTON DE ADELANTE
		panTabGen.coordX = (panTabGen.iteCmd * panTabGen.cmdPagWidth) + panTabGen.marginLeft;
		panTabGen.cmdArrayPag[panTabGen.iteCmd] = crearBoton(panTabGen.coordX, panTabGen.coordY, panTabGen.cmdPagWidth, panTabGen.cmdPagHeigth, panTabGen.iteCmd);
		panTabGen.add(panTabGen.cmdArrayPag[panTabGen.iteCmd]);
		panTabGen.iteCmd++;

		if (panTabGen.etiInfo == null) {
			panTabGen.etiInfo = new JLabel();
			panTabGen.add(panTabGen.etiInfo);
		}
		panTabGen.coordX = (panTabGen.iteCmd * panTabGen.cmdPagWidth) + panTabGen.marginLeft;
		panTabGen.etiInfo.setBounds(panTabGen.coordX + panTabGen.marginLeft, panTabGen.coordY, 400, panTabGen.cmdPagHeigth);
		panTabGen.etiInfo.setText("Paginas : " + panTabGen.totalPaginas + " Registros : " + panTabGen.totalRegistros);

	}

	public JButton crearBoton(int coordX, int coordY, int cmdPagWidth, int cmdHeight, int parIndex) {

		JButton cmdPag = new JButton();
		cmdPag.setBounds(coordX, coordY, cmdPagWidth, cmdHeight);
		String nombreBoton = "";
		if (parIndex == 0) {
			nombreBoton = "atras_0";
			cmdPag.setText("<<");
		}
		else if ((parIndex - 1) == panTabGen.visiblePages) {
			nombreBoton = "adelante_0";
			cmdPag.setText(">>");
		}
		else {
			nombreBoton = "cmdArrayPag_" + String.valueOf(parIndex);
			cmdPag.setText(String.valueOf(parIndex));
		}

		cmdPag.setName(nombreBoton);
		cmdPag.setActionCommand(nombreBoton);
		cmdPag.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPag.setBounds(coordX, coordY, cmdPagWidth, cmdHeight);
		cmdPag.setForeground(panTabGen.naranja);
		// cmdPag.setBackground(colorFondo);
		// cmdPag.setForeground(colorTexto);
		cmdPag.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdPag.addActionListener(new cmdArrayPagListener(this));
//		System.out.println("iteini : " + panTabGen.iteIni + " itefin : " + panTabGen.iteFin + " itecmd : " + panTabGen.iteCmd + cmdPag.getName() + " : " + cmdPag.getActionCommand());
		return cmdPag;
	}// Fin de Metodo para crear botones de dias de calendario

	public void cmbTamanoPaginaEvento(ItemEvent e)
	{
		if (e.getStateChange() == ItemEvent.SELECTED)
		{
			if (frmAbierto == true)
			{
				if (panTabGen.cmbTamanoPagina.getSelectedIndex() > -1)
				{
					panTabGen.tamanoPagina = Integer.parseInt(panTabGen.cmbTamanoPagina.getSelectedItem().toString());
					browse.setCurrentPage(1);
					browseObject.setCurrentPage(1);
					browse.setPageSize(panTabGen.tamanoPagina);
					refrescarRegistrosEvento();
				}
			}
		}
	}

	public void setCurrentPage(int currentPage) {
		for (int ite = 0; ite < panTabGen.numeroBoton; ite++) {

			panTabGen.cmdArrayPag[ite].setForeground(panTabGen.naranja);
			String[] valCmd = panTabGen.cmdArrayPag[ite].getName().split("_");
			if (!valCmd[0].equals("atras") && !valCmd[0].equals("adelante")) {
				if ((Integer.parseInt(valCmd[1])) == currentPage) {
					panTabGen.cmdArrayPag[ite].setForeground(panTabGen.rojo);
				}
			}
		}
		panTabGen.paginaActual = currentPage;
	}

	public void txtPaginaActualEvento(KeyEvent e)
	{
		if (Util.isInteger(panTabGen.txtPaginaActual.getText()))
		{
			int pagina = Integer.parseInt(panTabGen.txtPaginaActual.getText());
			if (e.getKeyChar() == KeyEvent.VK_ENTER && pagina <= browse.getPageCount())
			{
				browse.setCurrentPage(Integer.parseInt(panTabGen.txtPaginaActual.getText()));
				refrescarRegistrosEvento();
			}
		}
	}
	
	public void llenarTabla()
	{
		panTabGen.paginaActual = browse.getCurrentPage();
		setCurrentPage(panTabGen.paginaActual);

		List listaRegistros = (List) browse.getPageResults();
		Object[] nuevaFila = new Object[browseColumns.length];

		for (int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			if (browseColumn.getType().equals("INTEGER")) {
				nuevaFila[iteBrwCol] = 0;
			}
			if (browseColumn.getType().equals("DOUBLE")) {
				nuevaFila[iteBrwCol] = 0.00;
			}
			if (browseColumn.getType().equals("STRING")) {
				nuevaFila[iteBrwCol] = "";
			}
			if (browseColumn.getType().equals("BOOLEAN")) {
				nuevaFila[iteBrwCol] = false;
			}
			if (browseColumn.getType().equals("BIGDECIMAL")) {
				nuevaFila[iteBrwCol] = new BigDecimal("000000000000.00");
			}
		}

		panTabGen.modeloTablaGeneral.setRowCount(0);
		if (listaRegistros != null && listaRegistros.size() > 0)
		{
			for (int iteFil = 0; iteFil < listaRegistros.size(); iteFil++)
			{
				Object[] lisCol = (Object[]) listaRegistros.get(iteFil);
				Object objetoRegistro = listaRegistros.get(iteFil);
				panTabGen.modeloTablaGeneral.addRow(nuevaFila);
				for (int iteCol = 0; iteCol < lisCol.length; iteCol++)
				{
					panTabGen.modeloTablaGeneral.setValueAt(BrowseUtility.obtenerValor(browse,browseColumns,objetoRegistro,iteCol), iteFil, iteCol);
				}
			}
		}
	}

	public Object obtenerValor(int idxCol, Object objetoRegistro)
	{
		try
		{
			BrowseColumn columna = browseColumns[idxCol];
			Object columnValue = BrowseUtility.getColumnObject(columna, objetoRegistro, "sisem", browseColumns, browse.getBrowseId(), "");
			String columnResult = "";
			String allowEdit = "true";
			if(columna.getType().equals("ConditionalLink")) {
				columnResult = " <div id=\"browse_" + columna.getColumnName() + "\">";
			}
			else if(columna.getLink() != null && columna.getLink().length() > 0)
			{
				columnResult = BrowseUtility.getColumnLinkObject(columna, objetoRegistro, browseColumns);
				columnResult = columnResult.replace("javascript: abrirPagina(", "");
				columnResult = columnResult.replace("'", "");
				columnResult = columnResult.replace(");", "");
			}
			columnValue = (columnResult.equals("")) ? columnValue : columnResult;
			return columnValue;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	private void panTabGenTablaBrowseMousePressed(java.awt.event.MouseEvent evt) 
	{// GEN-FIRST:event_tabSubDetVenMousePressed
		seleccionarRegistroPanTabGenTableBrowse();
	}// GEN-LAST:event_tabSubDetVenMousePressed

	private void panTabGenTablaBrowseKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_tabSubDetVenKeyReleased
		if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_PAGE_UP || evt.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_F2 || evt.getKeyCode() == KeyEvent.VK_TAB) {

			panTabGenTablaBrowseMousePressed(null);

		}
	}// GEN-LAST:event_tabSubDetVenKeyReleased
	
	private void seleccionarRegistroPanTabGenTableBrowse() 
	{
		if (panTabGen.tablaBrowse.isEditing()) 
		{
			pagGen.pagAct.setModo(Modo.MODIFICANDO);
		}
		else if (!panTabGen.tablaBrowse.isEditing()) 
		{
			pagGen.pagAct.setModo(Modo.VISUALIZANDO);
//			obtenerRegistroProducto();
//			llenarProducto();
			if (panTabGen.tablaBrowse.getSelectedRow() > -1) 
			{
			}
		}
		refrescarFormulario();
	}// Fin de seleccionarRegistroTablaSucursal()
	
	
	/*
	 * LISTENER DE CLICK
	 */
	private static class cmdArrayPagListener implements ActionListener {

		private DlgBrowseTabla dlgBrowseTabla;

		public cmdArrayPagListener(DlgBrowseTabla dlgBrowseTabla) {
			this.dlgBrowseTabla = dlgBrowseTabla;
		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JButton objCmd = (JButton) e.getSource();
			String[] valCmd = objCmd.getActionCommand().split("_");
			if (valCmd[0].equals("atras") || valCmd[0].equals("adelante")) 
			{
				if (valCmd[0].equals("adelante") && dlgBrowseTabla.browse.getCurrentPage() < dlgBrowseTabla.browse.getPageCount()) 
				{
					if (dlgBrowseTabla.panTabGen.paginaActual < dlgBrowseTabla.panTabGen.totalPaginas) 
					{
						dlgBrowseTabla.panTabGen.paginaActual++;
						dlgBrowseTabla.browse.setCurrentPage(dlgBrowseTabla.panTabGen.paginaActual);
						dlgBrowseTabla.browseObject.setCurrentPage(dlgBrowseTabla.browse.getCurrentPage());
						dlgBrowseTabla.refrescarRegistrosEvento();
					}
				}
				else if (valCmd[0].equals("atras")&& dlgBrowseTabla.browse.getCurrentPage() > 1) 
				{
					if ((dlgBrowseTabla.panTabGen.paginaActual) > 1) 
					{
						dlgBrowseTabla.panTabGen.paginaActual--;
						dlgBrowseTabla.browse.setCurrentPage(dlgBrowseTabla.panTabGen.paginaActual);
						dlgBrowseTabla.browseObject.setCurrentPage(dlgBrowseTabla.browse.getCurrentPage());
						dlgBrowseTabla.refrescarRegistrosEvento();
					}
				}
				else {
					dlgBrowseTabla.panTabGen.paginaActual = dlgBrowseTabla.panTabGen.paginaActual;
				}
				
//				for (int ite = 0; ite < dlgTablaGeneral.panTabGen.numeroBoton; ite++) {
//					dlgTablaGeneral.remove(dlgTablaGeneral.panTabGen.cmdArrayPag[ite]);
//				}
//				dlgTablaGeneral.panTabGen.repaint();
//				// DlgTablaGeneral.update(DlgTablaGeneral.getGraphics());
//				dlgTablaGeneral.crearBotonesPaginacion(valCmd[0]);
			}
			else
			{
				dlgBrowseTabla.browse.setCurrentPage(Integer.parseInt(valCmd[1]));
				dlgBrowseTabla.browseObject.setCurrentPage(dlgBrowseTabla.browse.getCurrentPage());
				dlgBrowseTabla.refrescarRegistrosEvento();
			}
			// System.out.println("bootn : " + objCmd.getActionCommand());
		}// Fin de Action performed del boton de la palanca
	}// fin de la clase estatica para escuchar las acciones de los botones de palanca
	private void panTabGenMosuseReleased(java.awt.event.MouseEvent evt) 
	{// GEN-FIRST:event_panArtFotoMouseReleased
		if(evt.getButton() == MouseEvent.BUTTON3) 
		{
			popMenBrowse.show(evt.getComponent(), evt.getX(), evt.getY());
			if(panTabGen.tablaBrowse.getSelectedRow() > -1)
			{
				menEliminar.setEnabled(true);
				menModificar.setEnabled(true);
			}
			else
			{
				menEliminar.setEnabled(false);
				menModificar.setEnabled(false);
			}
		}// Fin de if (evt.getButton() == MouseEvent.BUTTON3)
	}// GEN-LAST:event_panArtFotoMouseReleased
	
}
