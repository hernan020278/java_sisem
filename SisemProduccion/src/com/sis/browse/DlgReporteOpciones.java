package com.sis.browse;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseObject;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;

public class DlgReporteOpciones extends VistaAdatper implements VistaListener {

	private ButtonGroup grupoOpcReporte = new ButtonGroup();
	private JLabel etiHeader;
	private JTextField[] txtExpresion = new JTextField[5];
	private JComboBox[] cmbColumna = new JComboBox[5];
	private JComboBox[] cmbOperador = new JComboBox[5];
	private JComboBox[] cmbLogico = new JComboBox[5];
	private JComboBox[] cmbOrden = new JComboBox[5];

	private List listaCampo = new ArrayList();
	private List listaTipo = new ArrayList();
	private JPanel panelPrincipal;
	private JButton cmdAgregar;
	private JButton cmdModificar;
	private JButton cmdEliminar;
	private JButton cmdCerrar;
	private JButton cmdImprimir;
	BrowseColumn browseColumns[];
	private String reporteModulo;
	private JPanel panTipoArchivo;
	private JTextFieldChanged etiTituloReporte;
	private JRadioButton opcPdf;
	private JRadioButton opcXls;
	private JRadioButton opcCsv;
	private JRadioButton opcPrint;
	private JLabel etiAccion;
	private JProgressBar barraProgreso;

	private int idxBrwCol;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgReporteOpciones window = new DlgReporteOpciones(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DlgReporteOpciones(FrmInicio parent) {
		super(parent);
		setTitle(this.getClass().getSimpleName());
		setResizable(false);

		initialize();

		configurarCombos();

	}

	public void configurarCombos()
	{
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			cmbColumna[iteFil].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			cmbOperador[iteFil].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
//			cmbOperador[iteFil].setRenderer(new ItemComboRender());
			cmbLogico[iteFil].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			cmbOrden[iteFil].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

			cmbOperador[iteFil].removeAllItems();
			cmbOperador[iteFil].addItem("");
			cmbOperador[iteFil].addItem("=");
			cmbOperador[iteFil].addItem(">");
			cmbOperador[iteFil].addItem("<");
			cmbOperador[iteFil].addItem(">=");
			cmbOperador[iteFil].addItem("<=");
			cmbOperador[iteFil].addItem("<>");

			cmbLogico[iteFil].removeAllItems();
			cmbLogico[iteFil].addItem("");
			cmbLogico[iteFil].addItem("AND");
			cmbLogico[iteFil].addItem("OR");

			cmbOrden[iteFil].removeAllItems();
			cmbOrden[iteFil].addItem("");
			cmbOrden[iteFil].addItem("ASC");
			cmbOrden[iteFil].addItem("DESC");
			cmbOrden[iteFil].setEnabled(false);
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);

		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);

		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 673, 344);

		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);

		etiHeader = new JLabel("Titulo Reporte");
		etiHeader.setBorder(new LineBorder(null, 1, true));
		etiHeader.setHorizontalAlignment(SwingConstants.CENTER);
		etiHeader.setHorizontalTextPosition(SwingConstants.CENTER);
		etiHeader.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiHeader.setBounds(287, 11, 127, 30);
		panelPrincipal.add(etiHeader);

		JLabel etiCampo1 = new JLabel("Campo");
		etiCampo1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiCampo1.setHorizontalAlignment(SwingConstants.CENTER);
		etiCampo1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiCampo1.setBounds(10, 47, 127, 35);
		panelPrincipal.add(etiCampo1);

		JLabel etiOperador = new JLabel("Operador");
		etiOperador.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiOperador.setHorizontalAlignment(SwingConstants.CENTER);
		etiOperador.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiOperador.setBounds(137, 47, 81, 35);
		panelPrincipal.add(etiOperador);

		JLabel etiExpresion = new JLabel("Filtro Expression");
		etiExpresion.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiExpresion.setHorizontalAlignment(SwingConstants.CENTER);
		etiExpresion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiExpresion.setBounds(217, 47, 281, 35);
		panelPrincipal.add(etiExpresion);

		JLabel etiCondicion = new JLabel("Condicion");
		etiCondicion.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiCondicion.setHorizontalAlignment(SwingConstants.CENTER);
		etiCondicion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiCondicion.setBounds(498, 47, 81, 35);
		panelPrincipal.add(etiCondicion);

		JLabel lblOrden = new JLabel("Orden");
		lblOrden.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		lblOrden.setHorizontalAlignment(SwingConstants.CENTER);
		lblOrden.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblOrden.setBounds(580, 47, 81, 35);
		panelPrincipal.add(lblOrden);

		cmbColumna[0] = new JComboBox();
		cmbColumna[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[0].setBounds(10, 93, 125, 24);
		cmbColumna[0].addItem("Campo");
		panelPrincipal.add(cmbColumna[0]);

		cmbOperador[0] = new JComboBox();
		cmbOperador[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[0].setBounds(137, 93, 79, 24);
		cmbOperador[0].addItem("Operador");
		panelPrincipal.add(cmbOperador[0]);

		txtExpresion[0] = new JTextField();
		txtExpresion[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[0].setBounds(217, 93, 279, 24);
		panelPrincipal.add(txtExpresion[0]);
		txtExpresion[0].setText("Expresion");
		txtExpresion[0].setColumns(10);

		cmbLogico[0] = new JComboBox();
		cmbLogico[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[0].setBounds(498, 93, 81, 24);
		cmbLogico[0].addItem("Condicion");
		panelPrincipal.add(cmbLogico[0]);

		cmbOrden[0] = new JComboBox();
		cmbOrden[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[0].setBounds(580, 93, 81, 24);
		cmbOrden[0].addItem("Orden");
		panelPrincipal.add(cmbOrden[0]);

		cmbColumna[1] = new JComboBox();
		cmbColumna[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[1].setBounds(10, 123, 125, 24);
		panelPrincipal.add(cmbColumna[1]);

		cmbOperador[1] = new JComboBox();
		cmbOperador[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[1].setBounds(137, 123, 79, 24);
		panelPrincipal.add(cmbOperador[1]);

		txtExpresion[1] = new JTextField();
		txtExpresion[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[1].setColumns(10);
		txtExpresion[1].setBounds(217, 123, 279, 24);
		panelPrincipal.add(txtExpresion[1]);

		cmbLogico[1] = new JComboBox();
		cmbLogico[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[1].setBounds(498, 123, 81, 24);
		panelPrincipal.add(cmbLogico[1]);

		cmbOrden[1] = new JComboBox();
		cmbOrden[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[1].setBounds(580, 123, 81, 24);
		panelPrincipal.add(cmbOrden[1]);

		cmbColumna[2] = new JComboBox();
		cmbColumna[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[2].setBounds(10, 153, 125, 24);
		panelPrincipal.add(cmbColumna[2]);

		cmbOperador[2] = new JComboBox();
		cmbOperador[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[2].setBounds(137, 153, 79, 24);
		panelPrincipal.add(cmbOperador[2]);

		txtExpresion[2] = new JTextField();
		txtExpresion[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[2].setColumns(10);
		txtExpresion[2].setBounds(217, 153, 279, 24);
		panelPrincipal.add(txtExpresion[2]);

		cmbLogico[2] = new JComboBox();
		cmbLogico[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[2].setBounds(498, 153, 81, 24);
		panelPrincipal.add(cmbLogico[2]);

		cmbOrden[2] = new JComboBox();
		cmbOrden[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[2].setBounds(580, 153, 81, 24);
		panelPrincipal.add(cmbOrden[2]);

		cmbColumna[3] = new JComboBox();
		cmbColumna[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[3].setBounds(10, 185, 125, 24);
		panelPrincipal.add(cmbColumna[3]);

		cmbOperador[3] = new JComboBox();
		cmbOperador[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[3].setBounds(137, 185, 79, 24);
		panelPrincipal.add(cmbOperador[3]);

		txtExpresion[3] = new JTextField();
		txtExpresion[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[3].setColumns(10);
		txtExpresion[3].setBounds(217, 185, 279, 24);
		panelPrincipal.add(txtExpresion[3]);

		cmbLogico[3] = new JComboBox();
		cmbLogico[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[3].setBounds(498, 185, 81, 24);
		panelPrincipal.add(cmbLogico[3]);

		cmbOrden[3] = new JComboBox();
		cmbOrden[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[3].setBounds(580, 185, 81, 24);
		panelPrincipal.add(cmbOrden[3]);

		cmbColumna[4] = new JComboBox();
		cmbColumna[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[4].setBounds(10, 217, 125, 24);
		panelPrincipal.add(cmbColumna[4]);

		cmbOperador[4] = new JComboBox();
		cmbOperador[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[4].setBounds(137, 217, 79, 24);
		panelPrincipal.add(cmbOperador[4]);

		txtExpresion[4] = new JTextField();
		txtExpresion[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[4].setColumns(10);
		txtExpresion[4].setBounds(217, 217, 279, 24);
		panelPrincipal.add(txtExpresion[4]);

		cmbLogico[4] = new JComboBox();
		cmbLogico[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[4].setBounds(498, 217, 81, 24);
		panelPrincipal.add(cmbLogico[4]);

		cmbOrden[4] = new JComboBox();
		cmbOrden[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[4].setBounds(580, 217, 81, 24);
		panelPrincipal.add(cmbOrden[4]);

		cmdImprimir = new JButton("Imprimir");
		cmdImprimir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdImprimir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdImprimir.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));
		cmdImprimir.setMargin(new Insets(0, 0, 0, 0));
		cmdImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdImprimirEvento();
			}
		});
		cmdImprimir.setBounds(10, 284, 117, 46);
		panelPrincipal.add(cmdImprimir);

		cmdCerrar = new JButton();
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdCerrarEvento();
			}
		});
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setToolTipText("Administre sus casos");
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setIconTextGap(0);
		cmdCerrar.setBounds(536, 284, 125, 46);
		panelPrincipal.add(cmdCerrar);

		cmdAgregar = new JButton();
		cmdAgregar.setText("Agregar");
		cmdAgregar.setMargin(new Insets(0, 0, 0, 0));
		cmdAgregar.setIconTextGap(0);
		cmdAgregar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdAgregar.setBounds(714, 11, 122, 46);
		panelPrincipal.add(cmdAgregar);

		cmdModificar = new JButton();
		cmdModificar.setText("Modificar");
		cmdModificar.setMargin(new Insets(0, 0, 0, 0));
		cmdModificar.setIconTextGap(0);
		cmdModificar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdModificar.setBounds(714, 68, 122, 46);
		panelPrincipal.add(cmdModificar);

		cmdEliminar = new JButton();
		cmdEliminar.setText("Eliminar");
		cmdEliminar.setMargin(new Insets(0, 0, 0, 0));
		cmdEliminar.setIconTextGap(0);
		cmdEliminar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdEliminar.setBounds(714, 125, 122, 46);
		panelPrincipal.add(cmdEliminar);

		etiTituloReporte = new JTextFieldChanged(20);
		etiTituloReporte.setText("Mendoza Ticllahuanaco");
		etiTituloReporte.setHorizontalAlignment(SwingConstants.LEFT);
		etiTituloReporte.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiTituloReporte.setBounds(426, 10, 234, 30);
		panelPrincipal.add(etiTituloReporte);

		panTipoArchivo = new JPanel();
		panTipoArchivo.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panTipoArchivo.setBounds(10, 10, 269, 30);
		panelPrincipal.add(panTipoArchivo);
		panTipoArchivo.setLayout(null);

		opcPdf = new JRadioButton("PDF");
		opcPdf.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcPdf.setBounds(4, 4, 62, 23);
		panTipoArchivo.add(opcPdf);
		this.grupoOpcReporte.add(this.opcPdf);

		opcXls = new JRadioButton("XLS");
		opcXls.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcXls.setBounds(70, 4, 53, 23);
		panTipoArchivo.add(opcXls);
		this.grupoOpcReporte.add(this.opcXls);

		opcCsv = new JRadioButton("CSV");
		opcCsv.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcCsv.setBounds(127, 4, 53, 23);
		panTipoArchivo.add(opcCsv);
		this.grupoOpcReporte.add(this.opcCsv);

		opcPrint = new JRadioButton("PRINT");
		opcPrint.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcPrint.setBounds(184, 4, 77, 23);
		panTipoArchivo.add(opcPrint);
		this.grupoOpcReporte.add(this.opcPrint);
		
		etiAccion = new JLabel();
		etiAccion.setText("Eventos en espera");
		etiAccion.setHorizontalAlignment(SwingConstants.CENTER);
		etiAccion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiAccion.setBounds(361, 284, 172, 27);
		panelPrincipal.add(etiAccion);
		
		barraProgreso = new JProgressBar();
		barraProgreso.setFont(new Font("Tahoma", Font.PLAIN, 16));
		barraProgreso.setBounds(361, 310, 172, 20);
		panelPrincipal.add(barraProgreso);

		setSize(681, 365);
		setLocationRelativeTo(null);

		
		
		for (int iteTxt = 0; iteTxt < txtExpresion.length; iteTxt++)
		{
			txtExpresion[iteTxt].addFocusListener(new FocusListener() 
			{
				@Override
				public void focusLost(FocusEvent e) {
//					JTextField txtObj = (JTextField) e.getSource();
//					idxBrwCol = Integer.parseInt(txtObj.getName());
					// int coordX = (int) txtObj.getBounds().getX();
					// int coordY = (int) txtObj.getBounds().getY() + (int) txtObj.getBounds().getHeight();
					// int ancho = (int) txtObj.getBounds().getWidth();
					// int alto = (int) txtObj.getBounds().getHeight();

					// scrLisExpresion.setBounds(coordX, coordY, ancho, alto);
					// scrLisExpresion.setVisible(false);
				}

				@Override
				public void focusGained(FocusEvent e) {
//					JTextField txtObj = (JTextField) e.getSource();
//					idxBrwCol = Integer.parseInt(txtObj.getName());
//					int coordX = (int) txtObj.getBounds().getX();
//					int coordY = (int) txtObj.getBounds().getY() + (int) txtObj.getBounds().getHeight();
//					int ancho = (int) txtObj.getBounds().getWidth();
//					int alto = (int) txtObj.getBounds().getHeight();

//					scrLisExpresion.setBounds(coordX, coordY, ancho, alto * 5);
//					lisExpresion.setBounds(coordX, coordY, ancho, alto * 5);
				}
			});		
			txtExpresion[iteTxt].addKeyListener(new KeyAdapter() {

				@Override
				public void keyReleased(KeyEvent arg0) {

					txtFicControlIdentidadKeyReleased(arg0);
				}
			});

		}
	}

	@Override
	public void iniciarFormulario()
	{
		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				limpiarFormulario();
				llenarFormulario();
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))

			browse = (Browse) peticion.get("browse");
			browseObject = (BrowseObject) peticion.get("browseObject");
			pagGen.pagAct.setBrowseTabla(browseObject.getBrowseTabla());
			filters = browseObject.getBrowseFilters();
			browseColumns = browseObject.getBrowseColumns();
			reporteModulo = (String) peticion.get("reporteModulo");
			activarFormulario(true);
			refrescarFormulario();

			frmAbierto = true;
			if(!this.isVisible()){this.setVisible(true);}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario() {
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			cmbColumna[iteFil].removeAllItems();
			cmbOperador[iteFil].setSelectedIndex(0);
			txtExpresion[iteFil].setText("");
			cmbLogico[iteFil].setSelectedIndex(0);
			cmbOrden[iteFil].setSelectedIndex(0);
		}
	}

	@Override
	public void obtenerDatoFormulario()
	{
		peticion.put("organizacionIde", OrganizacionGeneral.getOrgcodigo());
		peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
		peticion.put("reportName", pagGen.pagAct.getBrowseTabla());
		peticion.put("reporteModulo", reporteModulo);
		peticion.put("webreport", "Y");
		peticion.put("format", (opcPdf.isSelected()?"pdf":(opcXls.isSelected()?"xls":"print")));
		boolean validado = true;
		int numFil = 0;
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			if ((cmbOperador[iteFil].getSelectedIndex() < 1 && txtExpresion[iteFil].getText().equals(""))
					||(cmbOperador[iteFil].getSelectedIndex() > 0 && txtExpresion[iteFil].getText().equals(""))
					||(cmbOperador[iteFil].getSelectedIndex() < 1 && !txtExpresion[iteFil].getText().equals("")))
			{
				break;
			}
			if (iteFil > 0)
			{
				if (cmbLogico[iteFil - 1].getSelectedIndex() == -1)
				{
					break;
				}
			}

			if (validado)
			{
				numFil++;
			}
		}
		String[] filtroColumna = new String[numFil];
		String[] filtroTipo = new String[numFil];
		String[] filtroOperador = new String[numFil];
		String[] filtroValor = new String[numFil];
		String[] filtroLogico = new String[numFil];
		String[] filtroOrden = new String[numFil];
		String[] filtroOriginal = new String[numFil];

		for (int iteFil = 0; iteFil < numFil; iteFil++)
		{
			filtroColumna[iteFil] = (cmbColumna[iteFil].getSelectedIndex() == -1) ? "" : Util.getInst().obtenerValorLista(listaCampo, (String) cmbColumna[iteFil].getSelectedItem());
			filtroTipo[iteFil] = (String) listaTipo.get(cmbColumna[iteFil].getSelectedIndex());
			filtroOperador[iteFil] = (cmbOperador[iteFil].getSelectedIndex() < 1) ? "" : (String) cmbOperador[iteFil].getSelectedItem();
			filtroValor[iteFil] = txtExpresion[iteFil].getText();
			filtroLogico[iteFil] = (cmbLogico[iteFil].getSelectedIndex() < 1) ? "" : (String) cmbLogico[iteFil].getSelectedItem();
			filtroOrden[iteFil] = (cmbOrden[iteFil].getSelectedIndex() < 1) ? "" : (String) cmbOrden[iteFil].getSelectedItem();
		}

		peticion.put("filtroColumna", filtroColumna);
		peticion.put("filtroTipo", filtroTipo);
		peticion.put("filtroOperador", filtroOperador);
		peticion.put("filtroValor", filtroValor);
		peticion.put("filtroLogico", filtroLogico);
		peticion.put("filtroOrden", filtroOrden);
		peticion.put("filtroOriginal", filtroOriginal);

	}	
	@Override
	public void llenarFormulario()
	{
		etiTituloReporte.setText((String) peticion.get("reportTitleName"));
		crearBrowseOptions();
	}

	public void crearBrowseOptions()
	{
		BrowseObject browseObject = (BrowseObject) peticion.get("browseObject");
		browseColumns = browseObject.getBrowseColumns();

		listaCampo.clear();
		listaTipo.clear();

		int idxFil = -1;
		int filterRows = 5;

		for (int iteFil = 0; iteFil < filterRows; iteFil++)
		{
			int columnCount = 0;
			boolean seleccion= true;
			for (int iteCol = 0; iteCol < browseColumns.length; iteCol++)
			{
				BrowseColumn column = browseColumns[iteCol];

				if (!column.getAlias().toUpperCase().equals("MODIFICAR") && !column.getAlias().toUpperCase().equals("ELIMINAR"))
				{
					if ((column.isHidden() && !column.getAllowFilter().equals("Y")) || column.getType().equalsIgnoreCase("checkbox"))
					{
//						if (idxFil == iteCol)
//							idxFil++;
						continue;
					}
					
//					if (iteCol  > idxFil)
//					{
						cmbColumna[iteFil].addItem(column.getLabel());
						cmbColumna[iteFil].setSelectedIndex(-1);
						if(iteFil == 0)
						{
							listaCampo.add(column.getColumnName() + "#" + column.getLabel());
							listaTipo.add(column.getType());
						}
//						if(seleccion)
//						{
//							idxFil=iteCol;
//							seleccion=false;
//						}
//					}
					columnCount++;
				}
			}// for (int iteCol=0; iteCol < browseColumns.length; iteCol++)
		}//for (int iteFil = 0; iteFil < filterRows; iteFil++)
	}
	@Override
	public void activarFormulario(boolean activo) {
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			cmbColumna[iteFil].setEnabled(activo);
			cmbOperador[iteFil].setEnabled(activo);
			txtExpresion[iteFil].setEnabled(activo);
			cmbLogico[iteFil].setEnabled(activo);
			cmbOrden[iteFil].setEnabled(false);
		}
		opcPdf.setEnabled(activo);
		opcPrint.setEnabled(activo);
		opcXls.setEnabled(activo);
		opcCsv.setEnabled(activo);
		cmdImprimir.setEnabled(activo);
		cmdCerrar.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario()
	{
		if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO)) {

			cmdAgregar.setText("Agregar");
			cmdModificar.setText("Modificar");

			cmdAgregar.setEnabled(false);
			cmdModificar.setEnabled(false);
			cmdEliminar.setEnabled(false);
			cmdImprimir.setEnabled(true);
			cmdCerrar.setEnabled(true);

		}
		else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
		}
		else if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
		}
		else if (pagGen.pagAct.getModo().equals(Modo.BUSCANDO))
		{
			cmdImprimir.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if (pagGen.pagAct.getModo().equals(Modo.SUSPENDIDO))
		{
			cmdAgregar.setEnabled(false);
			cmdModificar.setEnabled(false);
			cmdImprimir.setEnabled(false);
			cmdEliminar.setEnabled(false);
			cmdCerrar.setEnabled(false);
		}
	}



	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {

		boolean validado = false;
		if (opcPdf.isSelected() || opcXls.isSelected() || opcCsv.isSelected() || opcPrint.isSelected())
		{
			if(validarFiltros())
			{
				validado = true;
			}
			else
			{
				aux.msg.setTitulo("Error de Validacion");
				aux.msg.setTipo("info");
				aux.msg.setValor("Verifique los filtros seleccionados");
				aux.msg.setMostrar(true);
				mensaje();
			}
		}
		else
		{
			aux.msg.setTitulo("Error de Validacion");
			aux.msg.setTipo("info");
			aux.msg.setValor("Debe seleccionar al menuos una opcion");
			aux.msg.setMostrar(true);
			mensaje();
		}
		return validado;
	}

	private boolean validarFiltros()
	{
		boolean validado= true;
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			if (cmbOperador[iteFil].getSelectedIndex() < 1 && txtExpresion[iteFil].getText().equals(""))
			{
				validado = true;
				break;
			}
			if ((cmbOperador[iteFil].getSelectedIndex() > 0 && txtExpresion[iteFil].getText().equals(""))
					||(cmbOperador[iteFil].getSelectedIndex() < 1 && !txtExpresion[iteFil].getText().equals("")))
			{
				validado = false;
				break;
			}
			if (iteFil > 0)
			{
				if (cmbLogico[iteFil - 1].getSelectedIndex() < 1)
				{
					validado = false;
					break;
				}
			}
		}		
		return validado;
	}
	
	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPeticion(Map peticion) {
		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	/*
	 * 
	 * 
	 * METODOS DE USUARIO PARA CAPTURARA LOS EVENTOS DEL LOS CONTROLES DEL FORMULARIO
	 */

	public void cmdImprimirEvento()
	{

		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdImprimirEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			peticion.put("reporteModulo", reporteModulo);
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{
		return barraProgreso;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{
		return etiAccion;
	}
	
	private void txtFicControlIdentidadKeyReleased(KeyEvent evt) {

		JTextField txtExpression = (JTextField) evt.getSource();
		
		if(evt.getKeyCode() == KeyEvent.VK_ESCAPE) 
		{
//			aux.bookMarkBarraFicha = "";
//			aux.bookMarkBarraSubArea = "";
//			aux.contadorMarcador = 1;
//			// ponerImagenRegistro("REGISTRO");
//			cmdCerrarEvento();
		}
		else 
		{
			txtExpression.setText(txtExpression.getText().trim());
			if(txtExpression.getText().trim().length() == 13 && Util.isLong(txtExpression.getText())) {
				txtExpression.setText(txtExpression.getText().trim().substring(0, 12));
				txtExpression.setEditable(false);
				cmdImprimirEvento();
				txtExpression.setEditable(true);
			}
		}
	}
}