package com.sis.browse;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseFilter;
import com.browse.BrowseObject;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.JTextFieldFormatoDecimal;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;

public class DlgBrowseOpciones extends VistaAdatper implements VistaListener {

	private JLabel etiHeader;
	private JTextField[] txtExpresion = new JTextField[5];
	private JTextFieldFormatoDecimal[] txtExpDecimal = new JTextFieldFormatoDecimal[5];
	private JComboBox[] cmbColumna = new JComboBox[5];
	private JComboBox[] cmbOperador = new JComboBox[5];
	private JComboBox[] cmbLogico = new JComboBox[5];
	private JComboBox[] cmbOrden = new JComboBox[5];

	private List listaCampo = new ArrayList();
	private List listaTipo = new ArrayList();

	private JLabel lblResultadoPorPagina;
	private JComboBox cmbTamanoPagina;
	private JPanel panelPrincipal;
	private JButton cmdAgregar;
	private JButton cmdModificar;
	private JButton cmdEliminar;
	private JButton cmdCerrar;
	private JButton cmdBuscar;
	private BrowseColumn browseColumns[];
	private Browse browse;
	private BrowseObject browseObject;
	private List filters;
	private JList lisExpresion;
	private JScrollPane scrLisExpresion;
	private DefaultListModel modLisExpresion;
	private int idxBrwCol;
	private boolean busLisExpresion;
	private String valorExpresion = "";
	private JLabel etiAccion;
	private JProgressBar barraProgreso;
	
	private List listaFiltroColumna = new ArrayList();
	private List listaFiltroTipo = new ArrayList();
	private List listaFiltroOperador = new ArrayList();
	private List listaFiltroValor = new ArrayList();
	private List listaFiltroLogico = new ArrayList();
	private List listaFiltroOrden = new ArrayList();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgBrowseOpciones window = new DlgBrowseOpciones(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DlgBrowseOpciones(FrmInicio parent) {
		super(parent);
		setTitle(this.getClass().getSimpleName());
		setResizable(false);

		initialize();

		configurarCombos();

	}

	public void configurarCombos()
	{
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			cmbColumna[iteFil].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			cmbOperador[iteFil].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
//			cmbOperador[iteFil].setRenderer(new ItemComboRender());
			cmbLogico[iteFil].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			cmbOrden[iteFil].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

			cmbOperador[iteFil].removeAllItems();
			cmbOperador[iteFil].addItem("");
			cmbOperador[iteFil].addItem("=");
			cmbOperador[iteFil].addItem(">");
			cmbOperador[iteFil].addItem("<");
			cmbOperador[iteFil].addItem(">=");
			cmbOperador[iteFil].addItem("<=");
			cmbOperador[iteFil].addItem("<>");

			cmbLogico[iteFil].removeAllItems();
			cmbLogico[iteFil].addItem("");
			cmbLogico[iteFil].addItem("AND");
			cmbLogico[iteFil].addItem("OR");

			cmbOrden[iteFil].removeAllItems();
			cmbOrden[iteFil].addItem("");
			cmbOrden[iteFil].addItem("ASC");
			cmbOrden[iteFil].addItem("DESC");
			cmbOrden[iteFil].setEnabled(false);
		}
		cmbTamanoPagina.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbTamanoPagina.setModel(new DefaultComboBoxModel(new String[] {"10", "25", "50", "100" }));
		cmbTamanoPagina.setSelectedIndex(3);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);

		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);

		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 676, 348);

		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);

		etiHeader = new JLabel("Formulario de Opciones de Busqueda");
		etiHeader.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiHeader.setHorizontalAlignment(SwingConstants.CENTER);
		etiHeader.setHorizontalTextPosition(SwingConstants.CENTER);
		etiHeader.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiHeader.setBounds(10, 11, 651, 35);
		panelPrincipal.add(etiHeader);

		scrLisExpresion = new JScrollPane();
		scrLisExpresion.setBounds(714, 157, 149, 180);
		panelPrincipal.add(scrLisExpresion);

		modLisExpresion = new DefaultListModel();
		lisExpresion = new JList(modLisExpresion);
		lisExpresion.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent evt) {
				lisExpresionKeyReleased(evt);
			}
		});
		lisExpresion.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				lisExpresionMousePressed(e);

			}
		});
		scrLisExpresion.setViewportView(lisExpresion);
		lisExpresion.setVisible(false);
		scrLisExpresion.setVisible(false);

		JLabel etiCampo1 = new JLabel("Campo");
		etiCampo1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiCampo1.setHorizontalAlignment(SwingConstants.CENTER);
		etiCampo1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiCampo1.setBounds(10, 47, 127, 35);
		panelPrincipal.add(etiCampo1);

		JLabel etiOperador = new JLabel("Operador");
		etiOperador.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiOperador.setHorizontalAlignment(SwingConstants.CENTER);
		etiOperador.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiOperador.setBounds(137, 47, 81, 35);
		panelPrincipal.add(etiOperador);

		JLabel etiExpresion = new JLabel("Filtro Expression");
		etiExpresion.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiExpresion.setHorizontalAlignment(SwingConstants.CENTER);
		etiExpresion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiExpresion.setBounds(217, 47, 281, 35);
		panelPrincipal.add(etiExpresion);

		JLabel etiCondicion = new JLabel("Condicion");
		etiCondicion.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiCondicion.setHorizontalAlignment(SwingConstants.CENTER);
		etiCondicion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiCondicion.setBounds(498, 47, 81, 35);
		panelPrincipal.add(etiCondicion);

		JLabel lblOrden = new JLabel("Orden");
		lblOrden.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		lblOrden.setHorizontalAlignment(SwingConstants.CENTER);
		lblOrden.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblOrden.setBounds(580, 47, 81, 35);
		panelPrincipal.add(lblOrden);

		cmbColumna[0] = new JComboBox();
		cmbColumna[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[0].setBounds(10, 93, 125, 24);
		cmbColumna[0].addItem("Campo");
		panelPrincipal.add(cmbColumna[0]);

		cmbOperador[0] = new JComboBox();
		cmbOperador[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[0].setBounds(137, 93, 79, 24);
		cmbOperador[0].addItem("Operador");
		panelPrincipal.add(cmbOperador[0]);

		txtExpresion[0] = new JTextField();
		txtExpresion[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[0].setBounds(217, 93, 279, 24);
		panelPrincipal.add(txtExpresion[0]);
		txtExpresion[0].setText("Expresion");
		txtExpresion[0].setColumns(10);
		txtExpresion[0].setName("0");

		cmbLogico[0] = new JComboBox();
		cmbLogico[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[0].setBounds(498, 93, 81, 24);
		cmbLogico[0].addItem("Condicion");
		panelPrincipal.add(cmbLogico[0]);

		cmbOrden[0] = new JComboBox();
		cmbOrden[0].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[0].setBounds(580, 93, 81, 24);
		cmbOrden[0].addItem("Orden");
		panelPrincipal.add(cmbOrden[0]);

		cmbColumna[1] = new JComboBox();
		cmbColumna[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[1].setBounds(10, 123, 125, 24);
		panelPrincipal.add(cmbColumna[1]);

		cmbOperador[1] = new JComboBox();
		cmbOperador[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[1].setBounds(137, 123, 79, 24);
		panelPrincipal.add(cmbOperador[1]);

		txtExpresion[1] = new JTextField();
		txtExpresion[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[1].setColumns(10);
		txtExpresion[1].setBounds(217, 123, 279, 24);
		txtExpresion[1].setName("1");
		panelPrincipal.add(txtExpresion[1]);

		cmbLogico[1] = new JComboBox();
		cmbLogico[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[1].setBounds(498, 123, 81, 24);
		panelPrincipal.add(cmbLogico[1]);

		cmbOrden[1] = new JComboBox();
		cmbOrden[1].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[1].setBounds(580, 123, 81, 24);
		panelPrincipal.add(cmbOrden[1]);

		cmbColumna[2] = new JComboBox();
		cmbColumna[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[2].setBounds(10, 153, 125, 24);
		panelPrincipal.add(cmbColumna[2]);

		cmbOperador[2] = new JComboBox();
		cmbOperador[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[2].setBounds(137, 153, 79, 24);
		panelPrincipal.add(cmbOperador[2]);

		txtExpresion[2] = new JTextField();
		txtExpresion[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[2].setColumns(10);
		txtExpresion[2].setBounds(217, 153, 279, 24);
		txtExpresion[2].setName("2");
		panelPrincipal.add(txtExpresion[2]);

		cmbLogico[2] = new JComboBox();
		cmbLogico[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[2].setBounds(498, 153, 81, 24);
		panelPrincipal.add(cmbLogico[2]);

		cmbOrden[2] = new JComboBox();
		cmbOrden[2].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[2].setBounds(580, 153, 81, 24);
		panelPrincipal.add(cmbOrden[2]);

		cmbColumna[3] = new JComboBox();
		cmbColumna[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[3].setBounds(10, 185, 125, 24);
		panelPrincipal.add(cmbColumna[3]);

		cmbOperador[3] = new JComboBox();
		cmbOperador[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[3].setBounds(137, 185, 79, 24);
		panelPrincipal.add(cmbOperador[3]);

		txtExpresion[3] = new JTextField();
		txtExpresion[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[3].setColumns(10);
		txtExpresion[3].setBounds(217, 185, 279, 24);
		txtExpresion[3].setName("3");
		panelPrincipal.add(txtExpresion[3]);

		cmbLogico[3] = new JComboBox();
		cmbLogico[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[3].setBounds(498, 185, 81, 24);
		panelPrincipal.add(cmbLogico[3]);

		cmbOrden[3] = new JComboBox();
		cmbOrden[3].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[3].setBounds(580, 185, 81, 24);
		panelPrincipal.add(cmbOrden[3]);

		cmbColumna[4] = new JComboBox();
		cmbColumna[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbColumna[4].setBounds(10, 217, 125, 24);
		panelPrincipal.add(cmbColumna[4]);

		cmbOperador[4] = new JComboBox();
		cmbOperador[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOperador[4].setBounds(137, 217, 79, 24);
		panelPrincipal.add(cmbOperador[4]);

		txtExpresion[4] = new JTextField();
		txtExpresion[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtExpresion[4].setColumns(10);
		txtExpresion[4].setBounds(217, 217, 279, 24);
		txtExpresion[4].setName("4");
		panelPrincipal.add(txtExpresion[4]);

		cmbLogico[4] = new JComboBox();
		cmbLogico[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbLogico[4].setBounds(498, 217, 81, 24);
		panelPrincipal.add(cmbLogico[4]);

		cmbOrden[4] = new JComboBox();
		cmbOrden[4].setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbOrden[4].setBounds(580, 217, 81, 24);
		panelPrincipal.add(cmbOrden[4]);

		cmbTamanoPagina = new JComboBox();
		cmbTamanoPagina.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbTamanoPagina.setBounds(137, 252, 81, 24);
		panelPrincipal.add(cmbTamanoPagina);

		lblResultadoPorPagina = new JLabel("Resultado x pagina");
		lblResultadoPorPagina.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblResultadoPorPagina.setBounds(10, 252, 127, 24);
		panelPrincipal.add(lblResultadoPorPagina);

		cmdBuscar = new JButton("Buscar");
		cmdBuscar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdBuscar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));
		cmdBuscar.setMargin(new Insets(0, 0, 0, 0));
		cmdBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdBuscarEvento();
			}
		});
		cmdBuscar.setBounds(10, 284, 117, 46);
		panelPrincipal.add(cmdBuscar);

		cmdCerrar = new JButton();
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdCerrarEvento();
			}
		});
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setToolTipText("Administre sus casos");
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setIconTextGap(0);
		cmdCerrar.setBounds(536, 284, 125, 46);
		panelPrincipal.add(cmdCerrar);

		cmdAgregar = new JButton();
		cmdAgregar.setText("Agregar");
		cmdAgregar.setMargin(new Insets(0, 0, 0, 0));
		cmdAgregar.setIconTextGap(0);
		cmdAgregar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdAgregar.setBounds(714, 22, 122, 35);
		panelPrincipal.add(cmdAgregar);

		cmdModificar = new JButton();
		cmdModificar.setText("Modificar");
		cmdModificar.setMargin(new Insets(0, 0, 0, 0));
		cmdModificar.setIconTextGap(0);
		cmdModificar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdModificar.setBounds(714, 65, 122, 35);
		panelPrincipal.add(cmdModificar);

		cmdEliminar = new JButton();
		cmdEliminar.setText("Eliminar");
		cmdEliminar.setMargin(new Insets(0, 0, 0, 0));
		cmdEliminar.setIconTextGap(0);
		cmdEliminar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdEliminar.setBounds(714, 111, 122, 35);
		panelPrincipal.add(cmdEliminar);

		etiAccion = new JLabel();
		etiAccion.setText("Eventos en espera");
		etiAccion.setHorizontalAlignment(SwingConstants.CENTER);
		etiAccion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiAccion.setBounds(360, 284, 172, 27);
		panelPrincipal.add(etiAccion);

		barraProgreso = new JProgressBar();
		barraProgreso.setFont(new Font("Tahoma", Font.PLAIN, 16));
		barraProgreso.setBounds(360, 310, 172, 20);
		panelPrincipal.add(barraProgreso);

		setSize(678, 368);
		setLocationRelativeTo(null);

		modLisExpresion.addElement("Uno");
		modLisExpresion.addElement("Dos");
		modLisExpresion.addElement("Tres");
		modLisExpresion.addElement("Cuatro");
		for (int iteTxt = 0; iteTxt < txtExpresion.length; iteTxt++)
		{
			txtExpresion[iteTxt].addFocusListener(new FocusListener() {

				@Override
				public void focusLost(FocusEvent e) {
					JTextField txtObj = (JTextField) e.getSource();
					idxBrwCol = Integer.parseInt(txtObj.getName());
					// int coordX = (int) txtObj.getBounds().getX();
					// int coordY = (int) txtObj.getBounds().getY() + (int) txtObj.getBounds().getHeight();
					// int ancho = (int) txtObj.getBounds().getWidth();
					// int alto = (int) txtObj.getBounds().getHeight();

					// scrLisExpresion.setBounds(coordX, coordY, ancho, alto);
					// scrLisExpresion.setVisible(false);
				}

				@Override
				public void focusGained(FocusEvent e) {
					JTextField txtObj = (JTextField) e.getSource();
					idxBrwCol = Integer.parseInt(txtObj.getName());
					int coordX = (int) txtObj.getBounds().getX();
					int coordY = (int) txtObj.getBounds().getY() + (int) txtObj.getBounds().getHeight();
					int ancho = (int) txtObj.getBounds().getWidth();
					int alto = (int) txtObj.getBounds().getHeight();

					scrLisExpresion.setBounds(coordX, coordY, ancho, alto * 5);
					lisExpresion.setBounds(coordX, coordY, ancho, alto * 5);
				}
			});
//			txtExpresion[iteTxt].addKeyListener(new KeyAdapter() {
//				@Override
//				public void keyReleased(KeyEvent e) {
//					mostrarListaExpresion(e);
//				}
//			});
		}
	}

	@Override
	public void iniciarFormulario()
	{
		try
		{
			frmAbierto = false;
			busLisExpresion = true;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName() + "-" + pagGen.pagAct.getPaginaPadre();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));

			nodoNivel = (peticion.containsKey("nodoNivel") ? (Integer) peticion.get("nodoNivel") : 0);
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				limpiarFormulario();
				activarFormulario(true);
				refrescarFormulario();
				
				browse = (Browse) peticion.get("browse");
				browseObject = (BrowseObject) peticion.get("browseObject");
				pagGen.pagAct.setBrowseTabla(browseObject.getBrowseTabla());
				filters = browseObject.getBrowseFilters();
				browseColumns = browseObject.getBrowseColumns();
				
				llenarFormulario();
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			frmAbierto = true;
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				this.setVisible(true);
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario() {
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			cmbColumna[iteFil].removeAllItems();
			cmbOperador[iteFil].setSelectedIndex(-1);
			txtExpresion[iteFil].setText("");
			cmbLogico[iteFil].setSelectedIndex(-1);
			cmbOrden[iteFil].setSelectedIndex(-1);
		}
	}

	@Override
	public void llenarFormulario()
	{
		crearBrowseOptions();
		for(int iteCmbPag = 0; iteCmbPag < cmbTamanoPagina.getItemCount(); iteCmbPag++)
		{
			String valorCombo = (String) cmbTamanoPagina.getItemAt(iteCmbPag);
			if(Util.isInteger(valorCombo))
			{
				if(browseObject.getPageSize() == Integer.parseInt(valorCombo))
				{
					cmbTamanoPagina.setSelectedIndex(iteCmbPag);
					break;
				}
			}
		}		
//		cmbTamanoPagina.setSelectedIndex(0);
	}

	public void crearBrowseOptions()
	{
		BrowseObject browseObject = (BrowseObject) peticion.get("browseObject");
		browseColumns = browseObject.getBrowseColumns();

		listaCampo.clear();
		listaTipo.clear();

		int idxSel = -1;
		int filterRows = 5;

		for (int iteFil = 0; iteFil < filterRows; iteFil++)
		{
			int columnCount = 0;
			boolean seleccion= true;
			for (int iteCol = 0; iteCol < browseColumns.length; iteCol++)
			{
				BrowseColumn column = browseColumns[iteCol];

				if (!column.getAlias().toUpperCase().equals("MODIFICAR") && !column.getAlias().toUpperCase().equals("ELIMINAR"))
				{
					if ((column.isHidden() && !column.getAllowFilter().equals("Y")) || column.getType().equalsIgnoreCase("checkbox"))
					{
//						if (idxSel == iteCol)
//							idxSel++;
						continue;
					}

					if(iteFil==0)
					{
						listaCampo.add(column.getColumnName() + "#" + column.getLabel());
						listaTipo.add(column.getType());
					}
					cmbColumna[iteFil].addItem(column.getLabel());
					if ((!column.isHidden() && column.getAllowFilter().equals("Y")) && iteCol  > idxSel )
					{
						if(seleccion)
						{
							cmbColumna[iteFil].setSelectedIndex(columnCount);
							idxSel=iteCol;
							seleccion=false;
						}
					}
					columnCount++;
				}
			}// for (int iteCol=0; iteCol < browseColumns.length; iteCol++)
		}//for (int iteFil = 0; iteFil < filterRows; iteFil++)
	}

	public void obtenerFiltrosOriginales()
	{
		if (browseObject.getBrowseFilters() != null)
		{
			int numFil = browseObject.getBrowseFilters().size();

			for (int iteFil = 0; iteFil < numFil; iteFil++)
			{
				BrowseFilter filter = (BrowseFilter) filters.get(iteFil);
				listaFiltroColumna.add(filter.getColumnName());
				listaFiltroTipo.add(filter.getType());
				listaFiltroOperador.add(filter.getOperator());
				listaFiltroValor.add(filter.getValue());
				listaFiltroLogico.add(filter.getLogicalOperator());
				listaFiltroOrden.add(filter.getSort());
			}
		}
	}
	
	public void obtenerFiltrosDlgOpciones()
	{
		peticion.put("nodoNivel", nodoNivel);
		peticion.put("pageSize", (String) cmbTamanoPagina.getSelectedItem());
		peticion.put("organizacionIde", OrganizacionGeneral.getOrgcodigo());
		peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
		peticion.put("reportName", pagGen.pagAct.getBrowseTabla());

		boolean validado = true;
		int numFil = 0;

		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			if ((cmbOperador[iteFil].getSelectedIndex() < 1 && txtExpresion[iteFil].getText().equals(""))
					||(cmbOperador[iteFil].getSelectedIndex() > 0 && txtExpresion[iteFil].getText().equals(""))
					||(cmbOperador[iteFil].getSelectedIndex() < 1 && !txtExpresion[iteFil].getText().equals("")))
			{
				break;
			}
			if (iteFil > 0)
			{
				if (cmbLogico[iteFil - 1].getSelectedIndex() == -1)
				{
					break;
				}
			}

			if (validado)
			{
				numFil++;
			}
		}
		for (int iteFil = 0; iteFil < numFil; iteFil++)
		{
			listaFiltroColumna.add((cmbColumna[iteFil].getSelectedIndex() == -1) ? "" : Util.getInst().obtenerValorLista(listaCampo, (String) cmbColumna[iteFil].getSelectedItem()));
			listaFiltroTipo.add((String) listaTipo.get(cmbColumna[iteFil].getSelectedIndex()));
			listaFiltroOperador.add(((cmbOperador[iteFil].getSelectedIndex() == -1) ? "" : (String) cmbOperador[iteFil].getSelectedItem()));
			listaFiltroValor.add(txtExpresion[iteFil].getText());
			listaFiltroLogico.add((cmbLogico[iteFil].getSelectedIndex() == -1) ? "" : (String) cmbLogico[iteFil].getSelectedItem());
			listaFiltroOrden.add((cmbOrden[iteFil].getSelectedIndex() == -1) ? "" : (String) cmbOrden[iteFil].getSelectedItem());
		}
	}
	
	public void obtenerFiltrosBusqueda()
	{
		listaFiltroColumna.clear();
		listaFiltroTipo.clear();
		listaFiltroOperador.clear();
		listaFiltroValor.clear();
		listaFiltroLogico.clear();
		listaFiltroOrden.clear();

		obtenerFiltrosOriginales();
		obtenerFiltrosDlgOpciones();
		
		if (listaFiltroColumna != null && listaFiltroColumna.size() > 0)
		{
			int numFil = listaFiltroColumna.size();
			String[] filtroColumna = new String[numFil];
			String[] filtroTipo = new String[numFil];
			String[] filtroOperador = new String[numFil];
			String[] filtroValor = new String[numFil];
			String[] filtroLogico = new String[numFil];
			String[] filtroOrden = new String[numFil];
			String[] filtroOriginal = new String[numFil];

			for (int iteFil = 0; iteFil < numFil; iteFil++)
			{
				filtroColumna[iteFil] = (String)listaFiltroColumna.get(iteFil);
				filtroTipo[iteFil] = (String)listaFiltroTipo.get(iteFil);
				filtroOperador[iteFil] = (String) listaFiltroOperador.get(iteFil);
				filtroValor[iteFil] = (String) listaFiltroValor.get(iteFil);
				filtroLogico[iteFil] = (String) listaFiltroLogico.get(iteFil);
				filtroOrden[iteFil] = (String) listaFiltroOrden.get(iteFil);
			}

			peticion.put("filtroColumna", filtroColumna);
			peticion.put("filtroTipo", filtroTipo);
			peticion.put("filtroOperador", filtroOperador);
			peticion.put("filtroValor", filtroValor);
			peticion.put("filtroLogico", filtroLogico);
			peticion.put("filtroOrden", filtroOrden);
			peticion.put("filtroOriginal", filtroOriginal);

		}
	}	
	@Override
	public void activarFormulario(boolean activo) {
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			cmbColumna[iteFil].setEnabled(activo);
			cmbOperador[iteFil].setEnabled(activo);
			txtExpresion[iteFil].setEnabled(activo);
			cmbLogico[iteFil].setEnabled(activo);
			cmbOrden[iteFil].setEnabled(activo);
		}

		cmbTamanoPagina.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario()
	{
		if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO)) {

			cmdBuscar.setEnabled(true);
			cmdCerrar.setEnabled(true);

		}
		else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
		}
		else if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
		}
		else if (pagGen.pagAct.getModo().equals(Modo.BUSCANDO))
		{
			cmdBuscar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if (pagGen.pagAct.getModo().equals(Modo.SUSPENDIDO))
		{
			cmdAgregar.setEnabled(false);
			cmdModificar.setEnabled(false);
			cmdBuscar.setEnabled(false);
			cmdEliminar.setEnabled(false);
			cmdCerrar.setEnabled(false);
		}
	}

	@Override
	public void obtenerDatoFormulario()
	{}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {

		boolean validado = true;
		if(validarFiltros())
		{
			validado = true;
		}
		else
		{
			aux.msg.setTitulo("Error de Validacion");
			aux.msg.setTipo("info");
			aux.msg.setValor("Verifique los filtros seleccionados");
			aux.msg.setMostrar(true);
			mensaje();
		}
		return validado;
	}

	private boolean validarFiltros()
	{
		boolean validado= true;
		for (int iteFil = 0; iteFil < cmbColumna.length; iteFil++)
		{
			if (cmbOperador[iteFil].getSelectedIndex() < 1 && txtExpresion[iteFil].getText().equals(""))
			{
				validado = true;
				break;
			}
			if ((cmbOperador[iteFil].getSelectedIndex() > 0 && txtExpresion[iteFil].getText().equals(""))
					||(cmbOperador[iteFil].getSelectedIndex() < 1 && !txtExpresion[iteFil].getText().equals("")))
			{
				validado = false;
				break;
			}
			if (iteFil > 0)
			{
				if (cmbLogico[iteFil - 1].getSelectedIndex() < 1)
				{
					validado = false;
					break;
				}
			}
		}		
		return validado;
	}
	
	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPeticion(Map peticion) {
		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	/*
	 * 
	 * 
	 * METODOS DE USUARIO PARA CAPTURARA LOS EVENTOS DEL LOS CONTROLES DEL FORMULARIO
	 */

	private void lisExpresionMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisExpresion.setVisible(false);
		lisExpresion.setVisible(false);
		if (lisExpresion.getSelectedIndex() > -1) {

			txtExpresion[idxBrwCol].setText((String) lisExpresion.getSelectedValue());

		}
	}// GEN-LAST:event_lisPerNomMousePressed

	private void lisExpresionKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			lisExpresion.setVisible(false);
			txtExpresion[idxBrwCol].requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisExpresionMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	public void txtExpresionObtenerListaEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			BrowseColumn browseColumn = browseColumns[idxBrwCol];

			peticion.put("nombreColumna", browseColumn.getColumnName());
			peticion.put("consulta", browseColumn.getSubselect());
			peticion.put("filtro", "like '%" + txtExpresion[idxBrwCol].getText() + "%'");

			ejecutar("txtExpresionObtenerListaEvento", Peticion.AJAX, false);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			llenarListaExpresion();
		}
	}

	public void mostrarListaExpresion(KeyEvent evt)
	{
		if (txtExpresion[idxBrwCol].getText().trim().length() == 0) {

			scrLisExpresion.setVisible(false);
			lisExpresion.setVisible(false);
			busLisExpresion = true;

		}
		else if (txtExpresion[idxBrwCol].getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisExpresion = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (modLisExpresion.size() > 0) {

					scrLisExpresion.setVisible(true);
					lisExpresion.setVisible(true);
					busLisExpresion = false;
					lisExpresion.requestFocus();
					lisExpresion.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisExpresionMousePressed(null);

			}
			else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisExpresion == true)) {

				txtExpresionObtenerListaEvento();

				if (modLisExpresion.size() > 0) {

					scrLisExpresion.setVisible(true);
					lisExpresion.setVisible(true);
					busLisExpresion = true;

				}
				else {

					scrLisExpresion.setVisible(false);
					lisExpresion.setVisible(false);
					busLisExpresion = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}

	public void llenarListaExpresion()
	{
		List listaExpresion = (List) peticion.get("listaExpresion");
		modLisExpresion.removeAllElements();
		if (listaExpresion != null && listaExpresion.size() > 0)
		{
			for (int iteLis = 0; iteLis < listaExpresion.size(); iteLis++)
			{
				String expresion = (String) listaExpresion.get(iteLis);
				modLisExpresion.addElement(expresion);
			}
		}
	}

	public void cmdBuscarEvento()
	{

		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (validarFormulario())
			{
				obtenerFiltrosBusqueda();
				ejecutar("cmdBuscarEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{
		return barraProgreso;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{
		return etiAccion;
	}
}
