package com.sis.browse;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.browse.BrowseColumn;
import com.comun.referencia.Modo;
import com.sis.main.VistaAdatper;

public class CellEditBoolean extends AbstractCellEditor implements TableCellEditor {

	private VistaAdatper vistaAdapter;
    private JCheckBox checkBox;
    private int fil, col;
    private boolean valorCheck;
    private boolean evaluar = false;
	private JTable tabla;
	private BrowseColumn[] browseColumns; 

    public CellEditBoolean(VistaAdatper parVistaAdapter, JTable parTabla, BrowseColumn[] browseColumns) {

        this.vistaAdapter = parVistaAdapter;
		this.tabla = parTabla;
		this.browseColumns = browseColumns;
        
        checkBox = new JCheckBox();
        checkBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
            	stopCellEditing();
				if (evaluar) {
				}
            }//Fin de actionPerformed
        });
    }//TabDetKarIngEditorJTextEntero(DlgSeleccion parDlgArt)

    @Override
    public boolean stopCellEditing() {

		valorCheck = (Boolean) getCellEditorValue();
		if(vistaAdapter.frmAbierto) {
			if(tabla.isEditing()) {
				if(vistaAdapter.pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || vistaAdapter.pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)) 
				{
					evaluar = true;
				}// Fin de if(dlgSel.chkDetMix.isSelected())
				else 
				{
					evaluar = true;
					cancelCellEditing();
				}
			}// Fin de if (dlgSel.tabSer.isEditing())
		}// Fin de if(dlgSel.frmAbierto)
        return super.stopCellEditing();

    }//Fin stopCellEditing()

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        fil = row;
        col = column;
        checkBox.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        checkBox.setSelected((Boolean) value);

        return checkBox;

    }

    @Override
    public Object getCellEditorValue() {

        return checkBox.isSelected();

    }
}//Fin de clase principal