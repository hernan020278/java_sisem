package com.sis.browse;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseObject;
import com.browse.BrowseUtility;
import com.comun.utilidad.CeldaRenderGeneral;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.JTextFieldFormatoEntero;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;

public class PanelTablaGeneral extends JPanel
{
	public Color rojo = new Color(255, 51, 0);
	public Color celeste = new Color(153, 204, 255);
	public Color naranja = new Color(255, 153, 0);

	/*
	 * PARA CONTROLD E DATAOS DE LA BROWSE
	 */
	public int coordX = 5, coordY = 5;
	public int cmdPagWidth = 25, cmdPagHeigth = 25, marginLeft = 5, marginTop = 5;

	public int totalRegistros;
	public int tamanoPagina = 100;
	public int totalPaginas;
	public int paginaActual;
	public int visiblePages;
	public JButton[] cmdArrayPag;
//	public int iniItePag = 0;
	public int iteCmd;
	public int iteIni;
	public int iteFin;
//	public int idxPag = 0;
	public int resto;
	public int numeroBoton;

	/*
	 * CONTROLES DE JPANEL
	 */
	public JLabel etiTitulo;

	public CeldaRenderGeneral celdaRenderGeneral;
	public ModeloTablaGeneral modeloTablaGeneral;
	public TableColumn[] columna;
	public JTable tablaBrowse;
	public JScrollPane scrtablaBrowse;
	public JTextFieldFormatoEntero txtPaginaActual;
	public JComboBox cmbTamanoPagina;
	public JButton cmdAgregar;
	
	public JLabel etiInfo;
	private JLabel etiTamanoPagina;
	private JLabel etiPaginaActual;
	private boolean frmAbierto;
	public String paginaHijo;
	public String paginaPadre;
	public String paginaEvento;
	public String paginaAccion;
	public String paginaValor;

	public PanelTablaGeneral( int ancho, int alto)
	{
		super.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		super.setBounds(5, 5, ancho-10, alto-10);
		super.setLayout(null);

		etiInfo = new JLabel("Pagina");
		etiInfo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiInfo.setBounds(ancho - 550, 5, 150, 25);
		super.add(etiInfo);

		etiPaginaActual = new JLabel("Pagina Actual");
		etiPaginaActual.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiPaginaActual.setBounds(ancho - 395, 5, 90, 25);
		super.add(etiPaginaActual);

		txtPaginaActual = new JTextFieldFormatoEntero(3);
		txtPaginaActual.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPaginaActual.setBounds(ancho - 300, 5, 60, 25);
		super.add(txtPaginaActual);
		txtPaginaActual.setColumns(10);

		etiTamanoPagina = new JLabel("Reg x Pag");
		etiTamanoPagina.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiTamanoPagina.setBounds(ancho - 235, 5, 70, 25);
		super.add(etiTamanoPagina);

		cmbTamanoPagina = new JComboBox();
		cmbTamanoPagina.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbTamanoPagina.setModel(new DefaultComboBoxModel(new String[] {"10", "25", "50", "100", "1000", "5000" }));
		cmbTamanoPagina.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbTamanoPagina.setBounds(ancho - 160, 5, 70, 25);
		super.add(cmbTamanoPagina);

		cmdAgregar = new JButton("Agregar");
		cmdAgregar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAgregar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdAgregar.setMargin(new Insets(0, 0, 0, 0));
		cmdAgregar.setBounds(ancho - 85, 3, 70, 25);
		cmdAgregar.setEnabled(false);
		super.add(cmdAgregar);

		scrtablaBrowse = new javax.swing.JScrollPane();
		tablaBrowse = new javax.swing.JTable();
		tablaBrowse.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tablaBrowse.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		tablaBrowse.setOpaque(false);
		scrtablaBrowse.setViewportView(tablaBrowse);

		super.add(scrtablaBrowse);
		scrtablaBrowse.setBounds(5, 30, ancho - 20, alto - 45);
	}
	
	public int obtenerIndiceHeader(String header)
	{
		int indice = -1;
		for(int iteHea = 0; iteHea < tablaBrowse.getTableHeader().getColumnModel().getColumnCount(); iteHea++)
		{
			String tablaHeader  = (String) tablaBrowse.getTableHeader().getColumnModel().getColumn(iteHea).getHeaderValue();
			if(header.equalsIgnoreCase(tablaHeader))
			{
				indice = iteHea;
				break;
			}
		}
		
		return indice;
	}
	
	public void crearModeloTabla(BrowseColumn[] browseColumns, VistaAdatper vistaAdapter) 
	{
		modeloTablaGeneral = new ModeloTablaGeneral(browseColumns);
		
		tablaBrowse = new javax.swing.JTable(modeloTablaGeneral);
		tablaBrowse.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tablaBrowse.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tablaBrowse.setOpaque(false);
		scrtablaBrowse.setViewportView(tablaBrowse);
		
		celdaRenderGeneral = new CeldaRenderGeneral();
		try
		{
			tablaBrowse.setDefaultRenderer(Class.forName("java.lang.String"), celdaRenderGeneral);
			tablaBrowse.setDefaultRenderer(Class.forName("java.lang.Integer"), celdaRenderGeneral);
			tablaBrowse.setDefaultRenderer(Class.forName("java.lang.Double"), celdaRenderGeneral);
			tablaBrowse.setDefaultRenderer(Class.forName("java.math.BigDecimal"), celdaRenderGeneral);
			tablaBrowse.setDefaultRenderer(Class.forName("java.sql.Date"), celdaRenderGeneral);
			tablaBrowse.setDefaultRenderer(Class.forName("java.sql.Time"), celdaRenderGeneral);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
		// tablaBrowse.addPropertyChangeListener(new ModtablaBrowsePropiedad(this, tablaBrowse));
		tablaBrowse.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// tablaBrowse.getTableHeader().setPreferredSize(new Dimension(tablaBrowse.getTableHeader().getWidth(), 25));
		// tablaBrowse.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 13));
		tablaBrowse.setRowHeight(25);
		tablaBrowse.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaBrowse.setShowGrid(true);
		tablaBrowse.setAutoCreateRowSorter(true);
		columna = new TableColumn[browseColumns.length];

		for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			columna[iteBrwCol] = tablaBrowse.getColumn(browseColumn.getColumnName());
			columna[iteBrwCol].setResizable(true);
			columna[iteBrwCol].setPreferredWidth(browseColumn.getSize() * 9);
			columna[iteBrwCol].setHeaderValue(browseColumn.getLabel());
			columna[iteBrwCol].setCellRenderer(obtenerCellRender(browseColumn));
			columna[iteBrwCol].setCellEditor(obtenerCellEdit(tablaBrowse, browseColumns, browseColumn, vistaAdapter));
			// columna[iteBrwCol].addPropertyChangeListener(new ListenerCambioPropiedad());
			if(browseColumn.isHidden())
			{
				tablaBrowse.getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				tablaBrowse.getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
				tablaBrowse.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				tablaBrowse.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
			}// Fin de if (ancho[iteBrwCol] == 0)
		}// Recorrer la tablaBrowse de encabezados
	}// Fin de crearModeloTabla()
	
	private TableCellRenderer obtenerCellRender(BrowseColumn browseColumn)
	{
		if(browseColumn.isButtonInput())
		{
			return new CellRenderJButton(obtenerBoton(browseColumn.getAlias()));
		}
		if(browseColumn.isCheckbox())
		{
			return null;
		}
		return celdaRenderGeneral;
	}

	private TableCellEditor obtenerCellEdit(JTable tabla, BrowseColumn[] browseColumns, BrowseColumn browseColumn, VistaAdatper vistaAdapter)
	{

		if(browseColumn.isButtonInput())
		{
			return new CellEditJButton(obtenerBoton(browseColumn.getAlias()), vistaAdapter);
		}
		if(browseColumn.isTextInput() && (browseColumn.getType().equalsIgnoreCase("INTEGER") || browseColumn.getType().equalsIgnoreCase("DOUBLE") || browseColumn.getType().equalsIgnoreCase("BIGDECIMAL")))
		{
			return new CellEditDecimalTextInput(vistaAdapter, tabla, browseColumns);
		}
		if(browseColumn.isTextInput() && (browseColumn.getType().equalsIgnoreCase("STRING")))
		{
			return new CellEditTextInput(vistaAdapter, tabla, browseColumns);
		}
		if(browseColumn.isCheckbox())
		{
			return new CellEditBoolean(vistaAdapter, tabla, browseColumns);
		}
		return null;
	}

	public JButton obtenerBoton(String nombre)
	{

		JButton boton = new JButton(nombre);
		boton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		boton.setMargin(new Insets(0, 0, 0, 0));
		return boton;
	}	
	
	public void limpiarTabla()
	{
		if(this.modeloTablaGeneral != null)
		{
			this.modeloTablaGeneral.setRowCount(0);
		}
	}
	
	public void llenarRegistrosTabla(Browse browse, BrowseColumn[] browseColumns, List browseList)
	{
		
		Object[] nuevaFila = new Object[browseColumns.length];
		for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			if(browseColumn.getType().equals("INTEGER")) {
				nuevaFila[iteBrwCol] = 0;	
			}
			if(browseColumn.getType().equals("DOUBLE")) {
				nuevaFila[iteBrwCol] = 0.00;
			}
			if(browseColumn.getType().equals("STRING")) {
				nuevaFila[iteBrwCol] = "";
			}
			if(browseColumn.getType().equals("TIME")) {
				nuevaFila[iteBrwCol] = "";
			}
			if(browseColumn.getType().equals("BOOLEAN")) {
				nuevaFila[iteBrwCol] = false;
			}
			if(browseColumn.getType().equals("BIGDECIMAL")) {
				nuevaFila[iteBrwCol] = new BigDecimal("000000000000.00");
			}
		}
		modeloTablaGeneral.setRowCount(0);
		if(browseList != null && browseList.size() > 0)
		{
			for(int iteLisCmp = 0; iteLisCmp < browseList.size(); iteLisCmp++)
			{
				Object[] regCmp = (Object[]) browseList.get(iteLisCmp);
				Object objetoRegistro = browseList.get(iteLisCmp);
				modeloTablaGeneral.addRow(nuevaFila);
				for(int iteRegCmp = 0; iteRegCmp < regCmp.length; iteRegCmp++)
				{
					modeloTablaGeneral.setValueAt(BrowseUtility.obtenerValor(browse,browseColumns,objetoRegistro,iteRegCmp), iteLisCmp, iteRegCmp);
				}
			}
		}
	}		
	
	
	public void crearTablaResultado(Browse browse)
	{
//		List listResult = (List) browse.getPageResults();
		totalRegistros = browse.getRowCount();
		tamanoPagina = browse.getPageSize();
		totalPaginas = browse.getPageCount();
		paginaActual = browse.getCurrentPage();
		if(tamanoPagina > 0)
		{
			// totalRegistros = listResult.size();
			visiblePages = 5;
			totalPaginas = totalRegistros / tamanoPagina;
			totalPaginas = (totalPaginas < 0) ? 0 : totalPaginas;
			resto = totalRegistros % tamanoPagina;
			totalPaginas = (resto > 0) ? totalPaginas + 1 : totalPaginas;
			if(totalPaginas < visiblePages)
			{
				visiblePages = totalPaginas;
			}
			for(int itePag = 1; itePag <= totalPaginas; itePag++)
			{
				int paginaActualReferencia = paginaActual;
				int inicioPagina = visiblePages * (itePag - 1);
				int finPagina = visiblePages * itePag;
				if(paginaActualReferencia > inicioPagina && paginaActualReferencia <= finPagina)
				{
					iteIni = itePag;
					iteFin = itePag + visiblePages - 1;
					break;
				}
			}
			paginaActual = paginaActual;
			// iniItePag = paginaActual;
			// if (totalPaginas <= visiblePages) {
			// visiblePages = totalPaginas;
			// }
			if(cmdArrayPag != null) {
				for(int ite = 0; ite < cmdArrayPag.length; ite++)
				{
					remove(cmdArrayPag[ite]);
				}
				cmdArrayPag = null;
			}
			crearBotonesPaginacion(false,false, false);
//			repaint();
		}// Fin de if(listResult != null)
	}	

	public void crearBotonesPaginacion(boolean crearBotones, boolean paginaActual, boolean tamanoPagina) {

		iteCmd = 0;
		if(crearBotones)
		{
			numeroBoton = visiblePages;
			numeroBoton = visiblePages + 2;
			cmdArrayPag = new JButton[numeroBoton];
			// CREAMOS EL BOTON DE ATRAS
			coordX = (iteCmd * cmdPagWidth) + marginLeft;
			cmdArrayPag[0] = crearBoton(coordX, coordY, cmdPagWidth, cmdPagHeigth, 0);
			add(cmdArrayPag[0]);
			iteCmd++;
			// CREAMOS LOS BOTONES VISIBLES
			for(int ite = iteIni; ite <= iteFin; ite++) {
				coordX = (iteCmd * cmdPagWidth) + marginLeft;
				cmdArrayPag[iteCmd] = crearBoton(coordX, coordY, cmdPagWidth, cmdPagHeigth, iteCmd);
				add(cmdArrayPag[iteCmd]);
				iteCmd++;
				// iniItePag++;
			}
			// CREAMOS EL BOTON DE ADELANTE
			coordX = (iteCmd * cmdPagWidth) + marginLeft;
			cmdArrayPag[iteCmd] = crearBoton(coordX, coordY, cmdPagWidth, cmdPagHeigth, iteCmd);
			add(cmdArrayPag[iteCmd]);
			iteCmd++;
			if(etiInfo == null) {
				etiInfo = new JLabel();
				add(etiInfo);
			}			
		}//if(botones)

		coordX = (iteCmd * cmdPagWidth) + marginLeft;
		etiInfo.setBounds(coordX + marginLeft, coordY, 400, cmdPagHeigth);
		etiInfo.setText("Paginas : " + totalPaginas + " Registros : " + totalRegistros);
		
		etiPaginaActual.setVisible(paginaActual);
		txtPaginaActual.setVisible(paginaActual);
		etiTamanoPagina.setVisible(paginaActual);
		cmbTamanoPagina.setVisible(tamanoPagina);
	}

	public JButton crearBoton(int coordX, int coordY, int cmdPagWidth, int cmdHeight, int parIndex) {

		JButton cmdPag = new JButton();
		cmdPag.setBounds(coordX, coordY, cmdPagWidth, cmdHeight);
		String nombreBoton = "";
		if(parIndex == 0) {
			nombreBoton = "atras_0";
			cmdPag.setText("<<");
		}
		else if((parIndex - 1) == visiblePages) {
			nombreBoton = "adelante_0";
			cmdPag.setText(">>");
		}
		else {
			nombreBoton = "cmdArrayPag_" + String.valueOf(parIndex);
			cmdPag.setText(String.valueOf(parIndex));
		}
		cmdPag.setName(nombreBoton);
		cmdPag.setActionCommand(nombreBoton);
		cmdPag.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPag.setBounds(coordX, coordY, cmdPagWidth, cmdHeight);
		cmdPag.setForeground(naranja);
		// cmdPag.setBackground(colorFondo);
		// cmdPag.setForeground(colorTexto);
		cmdPag.setMargin(new java.awt.Insets(0, 0, 0, 0));
//		cmdPag.addActionListener(new cmdArrayPagListener(this));
		// System.out.println("iteini : " + iteIni + " itefin : " + iteFin + " itecmd : " + iteCmd + cmdPag.getName() + " : " + cmdPag.getActionCommand());
		return cmdPag;
	}// Fin de Metodo para crear botones de dias de calendario
	
	public void refrescarControlPanelTablaGeneral(BrowseObject browseObject, VistaAdatper vistaAdapter)
	{
		if(browseObject != null)
		{
			vistaAdapter.frmAbierto = false;
			String botonAgregar = browseObject.getBotonAgregar();
			if(!botonAgregar.equals(""))
			{
				botonAgregar = botonAgregar.replace("javascript: abrirPagina(", "");
				botonAgregar = botonAgregar.replace("'", "");
				botonAgregar = botonAgregar.replace(");", "");
				String[] valores = botonAgregar.split(",");
				vistaAdapter.paginaHijo = valores[0];
				vistaAdapter.paginaPadre = vistaAdapter.pagGen.pagAct.getPaginaHijo();
				vistaAdapter.paginaEvento = valores[1];
				vistaAdapter.paginaAccion = valores[2];
				vistaAdapter.paginaValor = valores[3];
			}
			boolean valor = (botonAgregar.equals("")) ? false : true;
			cmdAgregar.setEnabled((botonAgregar.equals("")) ? false : true);
			/*
			 * REFRESCAMOS EL COMBO DE PAGINACION DE LA TABLA GENERAL
			 */
			for(int iteCmbTamPag = 0; iteCmbTamPag < cmbTamanoPagina.getItemCount(); iteCmbTamPag++)
			{
				String valorCombo = (String) cmbTamanoPagina.getItemAt(iteCmbTamPag);
				if(Util.isInteger(valorCombo))
				{
					if(browseObject.getPageSize() == Integer.parseInt(valorCombo))
					{
						cmbTamanoPagina.setSelectedIndex(iteCmbTamPag);
						break;
					}
				}
			}
			txtPaginaActual.setText(String.valueOf(browseObject.getCurrentPage()));
			if(modeloTablaGeneral.getRowCount() > 0)
			{
				if(cmdArrayPag != null) {
					for(int iteCmdPag = 0; iteCmdPag < cmdArrayPag.length; iteCmdPag++)
					{
						cmdArrayPag[iteCmdPag].setEnabled(true);
					}
				}
				tablaBrowse.setEnabled(true);
				txtPaginaActual.setEnabled(true);
				cmbTamanoPagina.setEnabled(true);
			}
			else
			{
				if(cmdArrayPag != null) {
					for(int ite = 0; ite < cmdArrayPag.length; ite++)
					{
						remove(cmdArrayPag[ite]);
					}
				}
				repaint();
				etiInfo.setText("");
				txtPaginaActual.setValue(0);
				cmbTamanoPagina.setSelectedIndex(-1);
				tablaBrowse.setEnabled(false);
				txtPaginaActual.setEnabled(false);
				cmbTamanoPagina.setEnabled(false);
			}
			vistaAdapter.frmAbierto = false;
		}//if(browseObject != null)
	}//private void refrescarControlPanelTablaGeneral(BrowseObject browseObject, VistaAdatper vistaAdapter)
	
	public void refrescarControlPanTabGen(BrowseObject browseObject, PaginaGeneral pagGen)
	{
		if(browseObject != null)
		{
			frmAbierto = false;
			String botonAgregar = browseObject.getBotonAgregar();
			if(!botonAgregar.equals(""))
			{
				botonAgregar = botonAgregar.replace("javascript: abrirPagina(", "");
				botonAgregar = botonAgregar.replace("'", "");
				botonAgregar = botonAgregar.replace(");", "");
				String[] valores = botonAgregar.split(",");
				paginaHijo = valores[0];
				paginaPadre = pagGen.pagAct.getPaginaHijo();
				paginaEvento = valores[1];
				paginaAccion = valores[2];
				paginaValor = "";
			}
			boolean valor = (botonAgregar.equals("")) ? false : true;
//			cmdAgregar.setEnabled((botonAgregar.equals("")) ? false : true);
			/*
			 * REFRESCAMOS EL COMBO DE PAGINACION DE LA TABLA GENERAL
			 */
			for(int iteCmbTamPag = 0; iteCmbTamPag < cmbTamanoPagina.getItemCount(); iteCmbTamPag++)
			{
				String valorCombo = (String) cmbTamanoPagina.getItemAt(iteCmbTamPag);
				if(Util.isInteger(valorCombo))
				{
					if(browseObject.getPageSize() == Integer.parseInt(valorCombo))
					{
						cmbTamanoPagina.setSelectedIndex(iteCmbTamPag);
						break;
					}
				}
			}
			txtPaginaActual.setText(String.valueOf(browseObject.getCurrentPage()));
			if(modeloTablaGeneral.getRowCount() > 0)
			{
				if(cmdArrayPag != null) {
					for(int iteCmdPag = 0; iteCmdPag < cmdArrayPag.length; iteCmdPag++)
					{
						cmdArrayPag[iteCmdPag].setEnabled(true);
					}
				}
				tablaBrowse.setEnabled(true);
				txtPaginaActual.setEnabled(true);
				cmbTamanoPagina.setEnabled(true);
			}
			else
			{
				if(cmdArrayPag != null) {
					for(int ite = 0; ite < cmdArrayPag.length; ite++)
					{
						remove(cmdArrayPag[ite]);
					}
				}
				repaint();
				etiInfo.setText("");
				txtPaginaActual.setValue(0);
				cmbTamanoPagina.setSelectedIndex(-1);
				tablaBrowse.setEnabled(false);
				txtPaginaActual.setEnabled(false);
				cmbTamanoPagina.setEnabled(false);
			}
			frmAbierto = false;
		}//if(browseObject != null)
	}//private void refrescarControlPanelTablaGeneral(BrowseObject browseObject, VistaAdatper vistaAdapter)	
}
