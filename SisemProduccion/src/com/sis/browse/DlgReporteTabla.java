package com.sis.browse;


import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.browse.BrowseFilter;
import com.comun.entidad.Reporte;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.swing.PanelImgFoto;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;

public class DlgReporteTabla extends VistaAdatper implements VistaListener
{
	private static final Color rojo = new Color(255, 51, 0);
	private static final Color celeste = new Color(153, 204, 255);
	private static final Color naranja = new Color(255, 153, 0);

	/*
	 * PARA CONTROLD E DATAOS DE LA BROWSE
	 */

	private DefaultListModel modLisReporte;
	private DefaultListModel modLisXml;
	private DefaultListModel modLisModulo;

	public String paginaHijo = "";
	public String evento = "";
	public String accion = "";
	public String valor = "";
	/*
	 * VARIABLES GENERALES
	 */
	private JLabel etiTitulo;

	private JPanel panelPrincipal;
	private JButton cmdCerrar;
	private List filters;
	private JPanel panelTabla;
	private PanelImgFoto panImgReporte;
	private JList lisReporte;
	private JButton cmdAbrirReporte;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgBrowseTabla window = new DlgBrowseTabla(null);
					window.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DlgReporteTabla(FrmInicio parent)
	{
		super(parent);
		setTitle(this.getClass().getSimpleName());
		setResizable(false);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);

		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);

		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 899, 579);

		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);

		etiTitulo = new JLabel("Formulario de Opciones de Busqueda");
		etiTitulo.setForeground(new Color(102, 102, 0));
		etiTitulo.setBorder(new MatteBorder(1, 0, 1, 0, (Color) new Color(153, 102, 0)));
		etiTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		etiTitulo.setHorizontalTextPosition(SwingConstants.CENTER);
		etiTitulo.setFont(new Font("Bookman Old Style", Font.ITALIC, 26));
		etiTitulo.setBounds(5, 5, 885, 35);
		panelPrincipal.add(etiTitulo);

		cmdCerrar = new JButton();
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdCerrarEvento();
			}
		});
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setToolTipText("Administre sus casos");
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setIconTextGap(0);
		cmdCerrar.setBounds(759, 525, 125, 40);
		panelPrincipal.add(cmdCerrar);

		panelTabla = new JPanel();
		panelTabla.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelTabla.setBounds(5, 47, 885, 475);
		panelPrincipal.add(panelTabla);
		panelTabla.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 260, 453);
		panelTabla.add(scrollPane);

		modLisXml = new DefaultListModel();
		modLisModulo = new DefaultListModel();
		modLisReporte = new DefaultListModel();
		lisReporte = new JList(modLisReporte);
		lisReporte.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				lisReporteKeyReleasedEvento();
			}
		});
		lisReporte.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				lisReporteValueChangedEvento(e);
			}
		});
		lisReporte.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		scrollPane.setViewportView(lisReporte);

		panImgReporte = new PanelImgFoto();
		panImgReporte.setBounds(280, 11, 595, 453);
		panelTabla.add(panImgReporte);

		cmdAbrirReporte = new JButton();
		cmdAbrirReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdAbrirReporteEvento();
			}
		});
		cmdAbrirReporte.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		cmdAbrirReporte.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAbrirReporte.setToolTipText("Administre sus casos");
		cmdAbrirReporte.setText("Abrir Reporte");
		cmdAbrirReporte.setMargin(new Insets(0, 0, 0, 0));
		cmdAbrirReporte.setIconTextGap(0);
		cmdAbrirReporte.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdAbrirReporte.setBounds(5, 525, 143, 40);
		panelPrincipal.add(cmdAbrirReporte);

		setSize(906, 604);
		setLocationRelativeTo(null);

	}

	@Override
	public void iniciarFormulario()
	{
		try
		{
			
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				limpiarFormulario();
				activarFormulario(true);
				refrescarFormulario();

				llenarFormulario();
				this.setVisible(true);
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			frmAbierto = true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario() {
		modLisReporte.removeAllElements();
		modLisXml.removeAllElements();
		modLisModulo.removeAllElements();
		panImgReporte.setImagen(null, null, null, "CONSTRAINT");
	}

	@Override
	public void llenarFormulario()
	{
		llenarTabla();
	}

	public void obtenerFiltrosBusqueda()
	{
		if (filters != null)
		{
			int numFil = filters.size();
			String[] filtroColumna = new String[numFil];
			String[] filtroTipo = new String[numFil];
			String[] filtroOperador = new String[numFil];
			String[] filtroValor = new String[numFil];
			String[] filtroLogico = new String[numFil];
			String[] filtroOrden = new String[numFil];
			String[] filtroOriginal = new String[numFil];

			for (int iteFil = 0; iteFil < numFil; iteFil++)
			{
				StringBuffer sb = new StringBuffer("");
				BrowseFilter filter = (BrowseFilter) filters.get(iteFil);
				filtroColumna[iteFil] = filter.getColumnName();
				filtroTipo[iteFil] = filter.getType();
				filtroOperador[iteFil] = filter.getOperator();
				filtroValor[iteFil] = filter.getValue();
				filtroLogico[iteFil] = filter.getLogicalOperator();
				filtroOrden[iteFil] = filter.getSort();
			}

			peticion.put("filtroColumna", filtroColumna);
			peticion.put("filtroTipo", filtroTipo);
			peticion.put("filtroOperador", filtroOperador);
			peticion.put("filtroValor", filtroValor);
			peticion.put("filtroLogico", filtroLogico);
			peticion.put("filtroOrden", filtroOrden);
			peticion.put("filtroOriginal", filtroOriginal);

		}
	}

	public JButton obtenerBoton(String nombre)
	{
		JButton boton = new JButton(nombre);
		boton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		boton.setMargin(new Insets(0, 0, 0, 0));
		return boton;
	}

	@Override
	public void activarFormulario(boolean activo) 
	{
		lisReporte.setEnabled(activo);
		cmdAbrirReporte.setEnabled(activo);
		cmdCerrar.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario()
	{
		if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			cmdAbrirReporte.setEnabled(false);
			cmdCerrar.setEnabled(true);
		}
		else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
		}
		else if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
		}
		else if (pagGen.pagAct.getModo().equals(Modo.BUSCANDO))
		{
			cmdAbrirReporte.setEnabled(false);
			cmdCerrar.setEnabled(true);
		}
		else if (pagGen.pagAct.getModo().equals(Modo.SUSPENDIDO))
		{
			cmdCerrar.setEnabled(false);
		}
	}

	@Override
	public void obtenerDatoFormulario()
	{
		peticion.put("browseTabla", (String) modLisXml.getElementAt(lisReporte.getSelectedIndex()));
		peticion.put("jasperFile", (String) modLisXml.getElementAt(lisReporte.getSelectedIndex()));
		peticion.put("reportTitleName", (String) modLisReporte.getElementAt(lisReporte.getSelectedIndex()));
		peticion.put("reporteModulo", (String) modLisModulo.getElementAt(lisReporte.getSelectedIndex()));
	}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {
		boolean validado = true;
		return validado;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPeticion(Map peticion) {
		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	public void llenarTabla()
	{
		modLisReporte.removeAllElements();
		modLisXml.removeAllElements();
		modLisModulo.removeAllElements();
		List listaReporte = (List) peticion.get("listaReporte");
		if (listaReporte != null && listaReporte.size() > 0)
		{
			for (int iteFil = 0; iteFil < listaReporte.size(); iteFil++)
			{
				Reporte reporte = (Reporte) listaReporte.get(iteFil);
				modLisReporte.addElement(reporte.getDescripcion());
				modLisXml.addElement(reporte.getXml());
				modLisModulo.addElement(reporte.getModulo());
			}
		}
	}

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdAbrirReporteEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdAbrirReporteEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}

	}

	public void lisReporteValueChangedEvento(ListSelectionEvent e)
	{
		if (e.getValueIsAdjusting())
		{
			seleccionarListaReporte();
		}
	}

	public void seleccionarListaReporte()
	{
		cmdAbrirReporte.setEnabled(true);
		String rutaImagen = AplicacionGeneral.getInstance(OrganizacionGeneral.getOrgcodigo()).obtenerRutaImagenes() + "reportepreview\\" + ((String) modLisXml.getElementAt(lisReporte.getSelectedIndex())) + ".png";
		ImageIcon img = new ImageIcon(rutaImagen);
		panImgReporte.setImagen(img, null, null, "CONSTRAINT");
	}

	public void lisReporteKeyReleasedEvento()
	{
		seleccionarListaReporte();
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{
		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{
		return null;
	}
}