package com.sis.browse;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseFilter;
import com.browse.BrowseObject;
import com.browse.BrowseUtility;
import com.comun.entidad.ObjetoNodo;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.CeldaRenderGeneral;
import com.comun.utilidad.Dates;
import com.comun.utilidad.Util;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoEntero;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.comun.utilidad.swing.PanelImgFoto;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;
import com.toedter.calendar.JDateChooser;
import javax.swing.JRadioButton;

public class DlgTablaGeneral extends VistaAdatper implements VistaListener {

	private JPanel panelPrincipal;
	private JPanel panelContenido;
	private JSplitPane splitPane;
	private JScrollPane scrTree;
	private DefaultMutableTreeNode treeNivel1 = null;
	private DefaultMutableTreeNode treeNivel2 = null;
	private DefaultMutableTreeNode treeNivel3 = null;
	private DefaultMutableTreeNode treeNivel4 = null;
	private DefaultMutableTreeNode treeNivel5 = null;
	private DefaultMutableTreeNode treeNivel6 = null;
	private JTree tree;
	private DefaultMutableTreeNode raiz;
	private DefaultMutableTreeNode nodoActual;
	private ObjetoNodo objetoNodoActual;
	private JPopupMenu popMenTree;
	private JMenuItem menLlenarNodo;
	private JMenuItem menLimpiarNodo;
	private JMenuItem menRefrescarNodo;
	private JButton cmdCerrar;
	private JPanel panelDatosTablaGeneral;
	private JPanel panelDatosPedido;
	private JLabel etiAccion;
	private JProgressBar barraProgreso;
	private List listaFiltroColumna = new ArrayList();
	private List listaFiltroTipo = new ArrayList();
	private List listaFiltroOperador = new ArrayList();
	private List listaFiltroValor = new ArrayList();
	private List listaFiltroLogico = new ArrayList();
	private List listaFiltroOrden = new ArrayList();
	private JDateChooser cmbPedidoFecReg;
	private JDateChooser cmbPedidoFecEnt;
	private JTextEditorFecha txtEditPedidoFecReg;
	private JTextEditorFecha txtEditPedidoFecEnt;
	private JTextFieldFormatoEntero txtPedidoPedAno;
	private JTextFieldFormatoEntero txtPedidoPedTemp;
	private JTextFieldChanged txtPedidoPedNumdoc;
	private JTextFieldChanged txtPedidoPedEst;
	private JTextFieldChanged txtClienteCliCod;
	private JTextFieldChanged txtClienteCliNom;
	private JTextFieldChanged txtClienteCliDoc;
	private JTextFieldChanged txtClienteCliNum;
	private JTextFieldChanged txtClienteCliDir;
	private JTextFieldChanged txtModeloPrdIde;
	private JTextFieldChanged txtModeloPrdDes;
	private JTextFieldChanged txtModeloPrdMat;
	private JTextFieldChanged txtModeloPrdTip;
	private JTextFieldChanged txtModeloPrdGau;
	private JTable tabFicCtrl;
	private PanelImgFoto panModeloPrdFoto;
	private JTextFieldChanged txtModeloPrdGro;
	private JTabbedPane tabPanTablas;
	private JPanel panTabContenedorTabla;
	private int modelo, pedido;
	private String talla;
	private JScrollPane scrTabFicCtrl;
	private ModeloTablaGeneral modTabFicCtrl;
	private TableColumn[] colTabFicCtrl;
	private BrowseColumn[] browseColumnsFichaControl;
	private CeldaRenderGeneral celdaRenderGeneral;
	private JButton cmdImprimir;
	private Map tabCmp;
	private ButtonGroup gruOpc = new ButtonGroup();
	private JRadioButton opcPdf;
	private JRadioButton opcXls;
	
	public DlgTablaGeneral(FrmInicio parent) {

		super(parent);
		setTitle(this.getClass().getSimpleName());
		setResizable(false);
		initComponents();
		popMenTree = new JPopupMenu();
		menLlenarNodo = new JMenuItem("Llenar Nodo");
		menLlenarNodo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				menLlenarNodoEvento(null);
			}
		});
		menLimpiarNodo = new JMenuItem("Limpiar Nodo");
		menLimpiarNodo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		menRefrescarNodo = new JMenuItem("Refrescar Nodo");
		menRefrescarNodo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				menRefrescarNodoEvento(null);
			}
		});
		popMenTree.add(menLlenarNodo);
		popMenTree.add(menLimpiarNodo);
		popMenTree.add(menRefrescarNodo);
		super.addWindowListener(new WindowAdapter() {

			@Override
			public void windowActivated(WindowEvent we) {

				frmAbierto = true;
			}
		});
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	private void initComponents() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);
		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				tabFicCtrl.setEnabled(true);
			}
		});
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);
		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 1086, 599);
		splitPane = new JSplitPane();
		splitPane.setBounds(10, 11, 1067, 533);
		splitPane.setDividerLocation(0.2);
		panelPrincipal.add(splitPane);
		raiz = new DefaultMutableTreeNode("Operaciones");
		tree = new JTree(raiz);
		tree.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tree.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {

				treeMousePressedEvento(e);
			}
		});
		tree.addTreeSelectionListener(new TreeSelectionListener() {

			public void valueChanged(TreeSelectionEvent e) {

				treeValueChanged(e);
			}
		});
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setBounds(0, 0, 72, 64);
		scrTree = new JScrollPane(tree);
		scrTree.setBounds(0, 0, 594, 582);
		splitPane.setLeftComponent(scrTree);
		panelContenido = new JPanel();
		splitPane.setRightComponent(panelContenido);
		panelContenido.setLayout(null);
		panelDatosPedido = new JPanel();
		panelDatosPedido.setBounds(10, 11, 703, 59);
		panelContenido.add(panelDatosPedido);
		panelDatosPedido.setLayout(null);
		panelDatosPedido.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Pedido", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		JLabel lblPedido = new JLabel();
		lblPedido.setText("Pedido");
		lblPedido.setHorizontalAlignment(SwingConstants.CENTER);
		lblPedido.setForeground(Color.BLACK);
		lblPedido.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPedido.setBounds(10, 15, 129, 15);
		panelDatosPedido.add(lblPedido);
		txtPedidoPedNumdoc = new JTextFieldChanged(11);
		txtPedidoPedNumdoc.setText("001-56467564");
		txtPedidoPedNumdoc.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedidoPedNumdoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedidoPedNumdoc.setBounds(10, 32, 129, 20);
		panelDatosPedido.add(txtPedidoPedNumdoc);
		txtEditPedidoFecReg = new JTextEditorFecha();
		txtEditPedidoFecReg.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditPedidoFecReg.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		cmbPedidoFecReg = new JDateChooser(txtEditPedidoFecReg);
		cmbPedidoFecReg.setDateFormatString("dd/MM/yyyy");
		cmbPedidoFecReg.setBounds(168, 32, 115, 20);
		panelDatosPedido.add(cmbPedidoFecReg);
		JLabel label_1 = new JLabel();
		label_1.setText("Fecha Registro");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(51, 51, 51));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(168, 15, 115, 15);
		panelDatosPedido.add(label_1);
		JLabel label_2 = new JLabel();
		label_2.setText("Fecha Entrega");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setForeground(new Color(51, 51, 51));
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setBounds(304, 15, 115, 15);
		panelDatosPedido.add(label_2);
		txtEditPedidoFecEnt = new JTextEditorFecha();
		txtEditPedidoFecEnt.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditPedidoFecEnt.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		cmbPedidoFecEnt = new JDateChooser(txtEditPedidoFecEnt);
		cmbPedidoFecEnt.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbPedidoFecEnt.setDateFormatString("dd/MM/yyyy");
		cmbPedidoFecEnt.setBounds(304, 32, 115, 20);
		panelDatosPedido.add(cmbPedidoFecEnt);
		JLabel label_3 = new JLabel();
		label_3.setText("Estado");
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setForeground(new Color(51, 51, 51));
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setBounds(562, 15, 131, 15);
		panelDatosPedido.add(label_3);
		txtPedidoPedEst = new JTextFieldChanged(20);
		txtPedidoPedEst.setText("12345678901234567890");
		txtPedidoPedEst.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedidoPedEst.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedidoPedEst.setBounds(562, 32, 131, 20);
		panelDatosPedido.add(txtPedidoPedEst);
		txtPedidoPedTemp = new JTextFieldFormatoEntero(2);
		txtPedidoPedTemp.setText("4");
		txtPedidoPedTemp.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedidoPedTemp.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedidoPedTemp.setBounds(482, 32, 70, 20);
		panelDatosPedido.add(txtPedidoPedTemp);
		JLabel label_4 = new JLabel();
		label_4.setText("Temporada");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(482, 15, 70, 15);
		panelDatosPedido.add(label_4);
		JLabel label_5 = new JLabel();
		label_5.setText("A\u00F1o");
		label_5.setHorizontalAlignment(SwingConstants.CENTER);
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(429, 15, 49, 15);
		panelDatosPedido.add(label_5);
		txtPedidoPedAno = new JTextFieldFormatoEntero(4);
		txtPedidoPedAno.setText("4");
		txtPedidoPedAno.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedidoPedAno.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedidoPedAno.setBounds(429, 32, 49, 20);
		panelDatosPedido.add(txtPedidoPedAno);
		JPanel panelDatosCliente = new JPanel();
		panelDatosCliente.setBounds(10, 72, 703, 59);
		panelContenido.add(panelDatosCliente);
		panelDatosCliente.setLayout(null);
		panelDatosCliente.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		JLabel label_6 = new JLabel();
		label_6.setText("Nombre");
		label_6.setHorizontalAlignment(SwingConstants.LEFT);
		label_6.setForeground(new Color(51, 51, 51));
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_6.setBounds(105, 15, 209, 15);
		panelDatosCliente.add(label_6);
		txtClienteCliCod = new JTextFieldChanged(10);
		txtClienteCliCod.setText("AYK");
		txtClienteCliCod.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtClienteCliCod.setBounds(10, 32, 91, 20);
		panelDatosCliente.add(txtClienteCliCod);
		txtClienteCliNom = new JTextFieldChanged(50);
		txtClienteCliNom.setText("Hernan Mendoza Ticllahuanaco");
		txtClienteCliNom.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtClienteCliNom.setBounds(105, 32, 209, 20);
		panelDatosCliente.add(txtClienteCliNom);
		txtClienteCliDoc = new JTextFieldChanged(3);
		txtClienteCliDoc.setText("RUC");
		txtClienteCliDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtClienteCliDoc.setBounds(317, 32, 39, 20);
		panelDatosCliente.add(txtClienteCliDoc);
		txtClienteCliNum = new JTextFieldChanged(11);
		txtClienteCliNum.setText("12345678901");
		txtClienteCliNum.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtClienteCliNum.setBounds(358, 32, 114, 20);
		panelDatosCliente.add(txtClienteCliNum);
		txtClienteCliDir = new JTextFieldChanged(100);
		txtClienteCliDir.setText("Av. 28 de Julio 785 La Tomilla Cayma");
		txtClienteCliDir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtClienteCliDir.setBounds(475, 32, 218, 20);
		panelDatosCliente.add(txtClienteCliDir);
		JLabel label_7 = new JLabel();
		label_7.setText("Direccion");
		label_7.setHorizontalAlignment(SwingConstants.LEFT);
		label_7.setForeground(new Color(51, 51, 51));
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_7.setBounds(475, 15, 218, 15);
		panelDatosCliente.add(label_7);
		JLabel label_8 = new JLabel();
		label_8.setText("Codigo");
		label_8.setHorizontalAlignment(SwingConstants.LEFT);
		label_8.setForeground(new Color(51, 51, 51));
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_8.setBounds(10, 15, 91, 15);
		panelDatosCliente.add(label_8);
		JLabel label_9 = new JLabel();
		label_9.setText("DOC");
		label_9.setHorizontalAlignment(SwingConstants.CENTER);
		label_9.setForeground(new Color(51, 51, 51));
		label_9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_9.setBounds(317, 15, 39, 15);
		panelDatosCliente.add(label_9);
		JLabel label_10 = new JLabel();
		label_10.setText("Numero");
		label_10.setHorizontalAlignment(SwingConstants.LEFT);
		label_10.setForeground(new Color(51, 51, 51));
		label_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_10.setBounds(358, 15, 114, 15);
		panelDatosCliente.add(label_10);
		panelDatosTablaGeneral = new JPanel();
		panelDatosTablaGeneral.setBounds(10, 134, 833, 386);
		panelContenido.add(panelDatosTablaGeneral);
		panelDatosTablaGeneral.setLayout(null);
		panelDatosTablaGeneral.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Producto", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		JLabel label_11 = new JLabel("Modelo");
		label_11.setHorizontalAlignment(SwingConstants.CENTER);
		label_11.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_11.setBorder(null);
		label_11.setBounds(11, 15, 157, 15);
		panelDatosTablaGeneral.add(label_11);
		JLabel label_12 = new JLabel("Tipo");
		label_12.setHorizontalAlignment(SwingConstants.CENTER);
		label_12.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_12.setBorder(null);
		label_12.setBounds(386, 15, 137, 15);
		panelDatosTablaGeneral.add(label_12);
		JLabel label_13 = new JLabel("Material");
		label_13.setHorizontalAlignment(SwingConstants.CENTER);
		label_13.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_13.setBorder(null);
		label_13.setBounds(527, 15, 132, 15);
		panelDatosTablaGeneral.add(label_13);
		JLabel label_14 = new JLabel("Galga");
		label_14.setHorizontalAlignment(SwingConstants.CENTER);
		label_14.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_14.setBorder(null);
		label_14.setBounds(662, 15, 76, 15);
		panelDatosTablaGeneral.add(label_14);
		txtModeloPrdIde = new JTextFieldChanged(50);
		txtModeloPrdIde.setText("12345678901234567890");
		txtModeloPrdIde.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModeloPrdIde.setBounds(11, 32, 157, 20);
		panelDatosTablaGeneral.add(txtModeloPrdIde);
		txtModeloPrdTip = new JTextFieldChanged(50);
		txtModeloPrdTip.setText("12345678901234567890");
		txtModeloPrdTip.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModeloPrdTip.setBounds(386, 32, 137, 20);
		panelDatosTablaGeneral.add(txtModeloPrdTip);
		txtModeloPrdMat = new JTextFieldChanged(50);
		txtModeloPrdMat.setText("12345678901234567890");
		txtModeloPrdMat.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModeloPrdMat.setBounds(527, 32, 132, 20);
		panelDatosTablaGeneral.add(txtModeloPrdMat);
		txtModeloPrdGau = new JTextFieldChanged(50);
		txtModeloPrdGau.setText("12345678901234567890");
		txtModeloPrdGau.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModeloPrdGau.setBounds(662, 32, 76, 20);
		panelDatosTablaGeneral.add(txtModeloPrdGau);
		txtModeloPrdDes = new JTextFieldChanged(50);
		txtModeloPrdDes.setText("12345678901234567890");
		txtModeloPrdDes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModeloPrdDes.setBounds(179, 32, 203, 20);
		panelDatosTablaGeneral.add(txtModeloPrdDes);
		JLabel label_20 = new JLabel("Descripcion");
		label_20.setHorizontalAlignment(SwingConstants.CENTER);
		label_20.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_20.setBorder(null);
		label_20.setBounds(178, 14, 204, 15);
		panelDatosTablaGeneral.add(label_20);
		txtModeloPrdGro = new JTextFieldChanged(50);
		txtModeloPrdGro.setText("12345678901234567890");
		txtModeloPrdGro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModeloPrdGro.setBounds(741, 32, 82, 20);
		panelDatosTablaGeneral.add(txtModeloPrdGro);
		JLabel label_21 = new JLabel("Grosor");
		label_21.setHorizontalAlignment(SwingConstants.CENTER);
		label_21.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_21.setBorder(null);
		label_21.setBounds(741, 15, 82, 15);
		panelDatosTablaGeneral.add(label_21);
		tabPanTablas = new JTabbedPane(JTabbedPane.TOP);
		tabPanTablas.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabPanTablas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tabPanTablas.setBounds(10, 57, 813, 318);
		panelDatosTablaGeneral.add(tabPanTablas);
		panTabContenedorTabla = new JPanel();
		tabPanTablas.addTab("Lista de Registros", null, panTabContenedorTabla, null);
		panTabContenedorTabla.setLayout(null);
		panTabGen = new PanelTablaGeneral(tabPanTablas.getWidth(), tabPanTablas.getHeight() - 25);
		panTabContenedorTabla.add(panTabGen);
		panTabGen.setLayout(null);
		panTabGen.cmdAgregar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdAgregarEvento();
			}
		});
		panTabGen.tablaBrowse.addMouseListener(new java.awt.event.MouseAdapter() {

			public void mousePressed(java.awt.event.MouseEvent evt) {

				panTabGenTablaBrowseMousePressed(evt);
			}
		});
		panTabGen.tablaBrowse.addKeyListener(new java.awt.event.KeyAdapter() {

			public void keyReleased(java.awt.event.KeyEvent evt) {

				panTabGenTablaBrowseKeyReleased(evt);
			}
		});
		panTabGen.txtPaginaActual.addKeyListener(new KeyAdapter()
		{

			@Override
			public void keyReleased(KeyEvent e) {

				txtPaginaActualEvento(e);
			}
		});
		panTabGen.cmbTamanoPagina.addItemListener(new ItemListener()
		{

			public void itemStateChanged(ItemEvent e) {

				cmbTamanoPaginaEvento(e);
			}
		});
		JPanel panel_2 = new JPanel();
		panel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tabPanTablas.addTab("Ficha de Control", null, panel_2, null);
		panel_2.setLayout(null);
		scrTabFicCtrl = new JScrollPane();
		scrTabFicCtrl.setBounds(10, 11, 788, 229);
		panel_2.add(scrTabFicCtrl);
		tabFicCtrl = new JTable();
		tabFicCtrl.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent evt) {
				tabFicCtrlKeyReleased(evt);
			}
		});
		tabFicCtrl.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent evt) {
				tabFicCtrlMousePressed(evt);
			}
		});
		tabFicCtrl.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tabFicCtrl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		scrTabFicCtrl.setViewportView(tabFicCtrl);
		panModeloPrdFoto = new PanelImgFoto();
		panModeloPrdFoto.setBounds(715, 18, 124, 110);
		panelContenido.add(panModeloPrdFoto);
		GroupLayout gl_panModeloPrdFoto = new GroupLayout(panModeloPrdFoto);
		gl_panModeloPrdFoto.setHorizontalGroup(
				gl_panModeloPrdFoto.createParallelGroup(Alignment.LEADING)
						.addGap(0, 120, Short.MAX_VALUE)
						.addGap(0, 116, Short.MAX_VALUE)
				);
		gl_panModeloPrdFoto.setVerticalGroup(
				gl_panModeloPrdFoto.createParallelGroup(Alignment.LEADING)
						.addGap(0, 105, Short.MAX_VALUE)
						.addGap(0, 101, Short.MAX_VALUE)
				);
		panModeloPrdFoto.setLayout(gl_panModeloPrdFoto);
		cmdCerrar = new JButton();
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				cmdCerrarEvento();
			}
		});
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setName("Agregar");
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.setBounds(975, 548, 104, 40);
		panelPrincipal.add(cmdCerrar);
		etiAccion = new JLabel();
		etiAccion.setText("Eventos en espera");
		etiAccion.setHorizontalAlignment(SwingConstants.CENTER);
		etiAccion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiAccion.setBounds(10, 548, 213, 21);
		panelPrincipal.add(etiAccion);
		barraProgreso = new JProgressBar();
		barraProgreso.setFont(new Font("Tahoma", Font.PLAIN, 16));
		barraProgreso.setBounds(10, 568, 213, 20);
		panelPrincipal.add(barraProgreso);
		
		cmdImprimir = new JButton("Imprimir");
		cmdImprimir.setMargin(new Insets(0, 0, 0, 0));
		cmdImprimir.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		cmdImprimir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdImprimir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdImprimirEvento();
			}
		});
		cmdImprimir.setBounds(844, 548, 126, 40);
		panelPrincipal.add(cmdImprimir);
		
		opcPdf = new JRadioButton("PDF");
		opcPdf.setFont(new Font("Tahoma", Font.PLAIN, 16));
		opcPdf.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcPdf.setIcon(AplicacionGeneral.getInstance().obtenerImagen("radiounselected.png"));
		opcPdf.setSelectedIcon(AplicacionGeneral.getInstance().obtenerImagen("radioselected.png"));
		opcPdf.setBounds(685, 548, 73, 40);
		gruOpc.add(this.opcPdf);
		panelPrincipal.add(opcPdf);
		
		opcXls = new JRadioButton("XLS");
		opcXls.setFont(new Font("Tahoma", Font.PLAIN, 16));
		opcXls.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcXls.setIcon(AplicacionGeneral.getInstance().obtenerImagen("radiounselected.png"));
		opcXls.setSelectedIcon(AplicacionGeneral.getInstance().obtenerImagen("radioselected.png"));
		opcXls.setBounds(767, 551, 71, 37);
		gruOpc.add(this.opcXls);
		panelPrincipal.add(opcXls);
		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);
		setTitle(this.getClass().getSimpleName());
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the
		 * default look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DlgTablaGeneral.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DlgTablaGeneral.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DlgTablaGeneral.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DlgTablaGeneral.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		/*
		 * Create and display the dialog
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				DlgTablaGeneral dialog = new DlgTablaGeneral(null);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {

					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {

						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	/******************************************************
	 * FUNCTIONES DE LA INTERFAZ VISTALISTENER
	 ******************************************************/
	@Override
	public void iniciarFormulario() {

		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				limpiarFormulario();
				crearExplorador();
				refrescarFormulario();
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			else if(pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
				browse = (Browse) peticion.get("browse");
				browseObject = (BrowseObject) peticion.get("browseObject");
				pagGen.pagAct.setBrowseTabla(browse.getBrowseTabla());
				filters = browseObject.getBrowseFilters();
				browseColumns = browseObject.getBrowseColumns();
				nodoNivel = (peticion.containsKey("nodoNivel") ? (Integer) peticion.get("nodoNivel") : 0);
				if(pagGen.pagAct.getModo().equals(Modo.ELIMINANDO) || pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
				{
					refrescarRegistros();
				}
				else
				{
					llenarPanelExplorador();
					obtenerEntidadObjetoNodoActualEvento();
					llenarPanelTablaGeneral();
					refrescarFormulario();
				}
			}
			frmAbierto = true;
			if(!this.isVisible()){this.setVisible(true);}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void crearExplorador()
	{

		DefaultTreeModel treeModelo = (DefaultTreeModel) tree.getModel();
//		if(treeNivel1 == null)
//		{	
			raiz.removeAllChildren();
			treeNivel1 = new DefaultMutableTreeNode(new ObjetoNodo(null, "Fichas de Control", null, "pedfic-pedmod-temp"));
			raiz.add(treeNivel1);
			treeNivel1 = new DefaultMutableTreeNode(new ObjetoNodo(null, "Componentes por Modelo", null, "pedcmp-pedmod-temp"));
			raiz.add(treeNivel1);
			treeModelo.reload(raiz);
			if(panTabGen.modeloTablaGeneral != null){panTabGen.modeloTablaGeneral.setRowCount(0);}
//		}
	}

	@Override
	public void limpiarFormulario()
	{

		cmbPedidoFecEnt.setDate(null);
		cmbPedidoFecReg.setDate(null);
		txtPedidoPedAno.setValue(0);
		txtPedidoPedNumdoc.setText("");
		txtPedidoPedEst.setText("");
		txtPedidoPedTemp.setText("");
		txtClienteCliCod.setText("");
		txtClienteCliNom.setText("");
		txtClienteCliDoc.setText("");
		txtClienteCliNum.setText("");
		txtClienteCliDir.setText("");
		txtModeloPrdDes.setText("");
		txtModeloPrdGau.setText("");
		txtModeloPrdIde.setText("");
		txtModeloPrdMat.setText("");
		txtModeloPrdTip.setText("");
		txtModeloPrdGro.setText("");
		if(modTabFicCtrl != null){modTabFicCtrl.setRowCount(0);}
	}

	@Override
	public void llenarFormulario()
	{

		cmbPedidoFecEnt.setDate(Util.getInst().fromDateSqlToDate(aux.ped.getPed_fecent()));
		cmbPedidoFecReg.setDate(Util.getInst().fromDateSqlToDate(aux.ped.getPed_fecreg()));
		txtPedidoPedAno.setValue(aux.ped.getPed_fecreg().getYear() + 1900);
		txtPedidoPedNumdoc.setText(aux.ped.getPed_numdoc());
		txtPedidoPedEst.setText(aux.ped.getPed_est());
		txtPedidoPedTemp.setText(aux.ped.getPed_temp());
		txtClienteCliCod.setText(String.valueOf(aux.cli.getCli_cod()));
		txtClienteCliNom.setText(aux.cli.getCli_nom());
		txtClienteCliDoc.setText(aux.cli.getCli_doc());
		txtClienteCliNum.setText(aux.cli.getCli_num());
		txtClienteCliDir.setText(aux.cli.getCli_dir());
		txtModeloPrdDes.setText(aux.mod.getPrd_des());
		txtModeloPrdGau.setText(aux.mod.getPrd_gau());
		txtModeloPrdIde.setText(aux.mod.getPrd_ide());
		txtModeloPrdMat.setText(aux.mod.getPrd_mat());
		txtModeloPrdTip.setText(aux.mod.getPrd_tip());
		txtModeloPrdGro.setText(aux.mod.getPrd_gro());
	}

	@Override
	public void activarFormulario(boolean activo)
	{

		cmbPedidoFecEnt.setEnabled(false);
		cmbPedidoFecReg.setEnabled(false);
		txtPedidoPedAno.setEnabled(false);
		txtPedidoPedNumdoc.setEnabled(false);
		txtPedidoPedEst.setEnabled(false);
		txtPedidoPedTemp.setEnabled(false);
		txtClienteCliCod.setEnabled(false);
		txtClienteCliNom.setEnabled(false);
		txtClienteCliDoc.setEnabled(false);
		txtClienteCliNum.setEnabled(false);
		txtClienteCliDir.setEnabled(false);
		txtModeloPrdDes.setEnabled(false);
		txtModeloPrdGau.setEnabled(false);
		txtModeloPrdIde.setEnabled(false);
		txtModeloPrdMat.setEnabled(false);
		txtModeloPrdTip.setEnabled(false);
		txtModeloPrdGro.setEnabled(false);
		cmdImprimir.setEnabled(activo);
		opcPdf.setEnabled(activo);
		opcXls.setEnabled(activo);
		
		tree.setEnabled(activo);
		panTabGen.tablaBrowse.setEnabled(activo);
		tabFicCtrl.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario()
	{

		if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			llenarFormulario();
			activarFormulario(true);
			refrescarPanelTablaGeneral();
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			desactivarComponentes();
			if(browseObject.getBrowseTabla().equals("pedfic-pedmod"))
			{
				tabFicCtrl.setEnabled(true);
			}
			else
			{
				panTabGen.tablaBrowse.setEnabled(true);	
			}
		}
		else if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
			desactivarComponentes();
			cmdCerrar.setEnabled(true);
		}
	}

	@Override
	public void obtenerDatoFormulario()
	{

		peticion.put("Pagina_paginaHijo", paginaHijo);
		peticion.put("Pagina_paginaPadre", paginaPadre);
		peticion.put("Pagina_evento", paginaEvento);
		peticion.put("Pagina_accion", paginaAccion);
		peticion.put("Pagina_valor", paginaValor);
		peticion.put("currentPage", String.valueOf(browse.getCurrentPage()));
		peticion.put("pageSize", String.valueOf(browse.getPageSize()));
		peticion.put("nodoNivel", nodoActual.getLevel());
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			return true;
		}
		else if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return barraProgreso;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return etiAccion;
	}

	private void treeValueChanged(TreeSelectionEvent e)
	{
		nodoActual = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		tabPanTablas.setSelectedIndex(0);
		if(nodoActual!= null && nodoActual.getLevel() > 1)
		{
			if(browse != null)
			{
				browse.setCurrentPage(1);
			}
			refrescarRegistros();
		}
	}

	public void refrescarRegistros()
	{

		if(nodoActual == null)
		{
			return;
		}
		// System.out.println("Nivel : " + nodoActual.getLevel() + " Indice : " + nodoActual);
		if(nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
		{
			objetoNodoActual = (ObjetoNodo) nodoActual.getUserObject();
			if(objetoNodoActual.getBrowseTabla().equals("pedfic-pedmod-prd-tal-fic"))
			{
				tabPanTablas.setSelectedIndex(1);
				String[] valor = objetoNodoActual.getNodoDescripcion().split(" : ");
				
				pedido = Integer.parseInt(valor[1]);
				modelo = Integer.parseInt(valor[3]);
				
				recuperarRegistroParaFicControlEvento();
//				refrescarFormulario();
				
			}//if(objetoNodoActual.getBrowseTabla().equals("pedfic-pedmod"))
			else
			{
				tabPanTablas.setSelectedIndex(0);
				if(browse == null)
				{
					browse = new Browse();
					browse.setCurrentPage(1);
					browse.setPageSize(10);
				}
				if(!Util.isEmpty(objetoNodoActual.getBrowseTabla()))
				{
					pagGen.pagAct.setBrowseTabla(objetoNodoActual.getBrowseTabla());
					nodoActual.removeAllChildren();
					recuperarRegistrosDeBrowseTablaEvento();
				}
			}
		}
	}

	public void treeValueChangedEvento()
	{
		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();

			ejecutar("treeValueChangedEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}
	
	public void recuperarRegistrosDeBrowseTablaEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			obtenerDatosObjetoNodo();
			ejecutar("recuperarRegistrosDeBrowseTablaEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	public void obtenerDatosObjetoNodo()
	{
		obtenerFiltrosBusqueda();
		obtenerDatoFormulario();
	}

	public void obtenerFiltrosBusqueda()
	{
		obtenerObjetoNodoFiltrosCodigo();
		obtenerPeticionFiltrosBusqueda();
	}

	public void obtenerObjetoNodoActualFiltrosBusqueda()
	{

		if(nodoActual == null)
		{
			return;
		}
		listaFiltroColumna.clear();
		listaFiltroTipo.clear();
		listaFiltroOperador.clear();
		listaFiltroValor.clear();
		listaFiltroLogico.clear();
		listaFiltroOrden.clear();
		if(nodoActual.getRoot().getChildCount() >= 0)
		{
			if(nodoActual.getUserObject() instanceof ObjetoNodo)
			{
				ObjetoNodo objetotreeNodo = (ObjetoNodo) nodoActual.getUserObject();
				// System.out.println("Nodo : " + objetotreeNodo.getNodoDescripcion());
				if(objetotreeNodo.getBrowseFilters() != null)
				{
					int numFil = objetotreeNodo.getBrowseFilters().size();
					// for(int iteFil = 0; iteFil < numFil; iteFil++)
					// {
					// BrowseFilter filter = (BrowseFilter) objetotreeNodo.getBrowseFilters().get(iteFil);
					// listaFiltroColumna.add(filter.getColumnName());
					// listaFiltroTipo.add(filter.getType());
					// listaFiltroOperador.add(filter.getOperator());
					// listaFiltroValor.add(filter.getValue());
					// listaFiltroLogico.add(filter.getLogicalOperator());
					// listaFiltroOrden.add(filter.getSort());
					// }
					if(objetotreeNodo.getNodoCodigo() != null && objetotreeNodo.getNodoCodigo().size() > 0)
					{
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroColumna, "filtroColumna");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroTipo, "filtroTipo");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroOperador, "filtroOperador");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroValor, "filtroValor");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroLogico, "filtroLogico");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroOrden, "filtroOrden");
					}
				}
			}// if(nodeInfo instanceof ObjetoNodo)
		}
	}

	public void obtenerObjetoNodoFiltrosBusqueda()
	{

		if(nodoActual == null)
		{
			return;
		}
		listaFiltroColumna.clear();
		listaFiltroTipo.clear();
		listaFiltroOperador.clear();
		listaFiltroValor.clear();
		listaFiltroLogico.clear();
		listaFiltroOrden.clear();
		if(nodoActual.getRoot().getChildCount() >= 0)
		{
			Enumeration iteEnuNod = nodoActual.preorderEnumeration();
			while (iteEnuNod.hasMoreElements())
			{
				DefaultMutableTreeNode treeNodo = (DefaultMutableTreeNode) iteEnuNod.nextElement();
				if(treeNodo.getUserObject() instanceof ObjetoNodo)
				{
					ObjetoNodo objetotreeNodo = (ObjetoNodo) treeNodo.getUserObject();
					System.out.println("Nodo : " + objetotreeNodo.getNodoDescripcion());
					if(objetotreeNodo.getBrowseFilters() != null)
					{
						int numFil = objetotreeNodo.getBrowseFilters().size();
						for(int iteFil = 0; iteFil < numFil; iteFil++)
						{
							BrowseFilter filter = (BrowseFilter) objetotreeNodo.getBrowseFilters().get(iteFil);
							listaFiltroColumna.add(filter.getColumnName());
							listaFiltroTipo.add(filter.getType());
							listaFiltroOperador.add(filter.getOperator());
							listaFiltroValor.add(filter.getValue());
							listaFiltroLogico.add(filter.getLogicalOperator());
							listaFiltroOrden.add(filter.getSort());
						}
						if(objetotreeNodo.getNodoCodigo() != null && objetotreeNodo.getNodoCodigo().size() > 0)
						{
							obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroColumna, "filtroColumna");
							obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroTipo, "filtroTipo");
							obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroOperador, "filtroOperador");
							obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroValor, "filtroValor");
							obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroLogico, "filtroLogico");
							obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroOrden, "filtroOrden");
						}
					}
					if(nodoActual.equals(treeNodo))
					{
						break;
					}
				}// if(nodeInfo instanceof ObjetoNodo)
			}
		}
	}

	private void obtenerNodoFiltroCodigo(ObjetoNodo objetotreeNodo, List listaFiltro, String tipoFiltro)
	{

		List lista = (List) objetotreeNodo.getNodoCodigo().get(tipoFiltro);
		if(lista != null && lista.size() > 0)
		{
			for(int iteLis = 0; iteLis < lista.size(); iteLis++)
			{
				String filtro = (String) lista.get(iteLis);
				listaFiltro.add(filtro);
			}
		}
	}

	public void obtenerPeticionFiltrosBusqueda()
	{

		if(listaFiltroColumna != null && listaFiltroColumna.size() > 0)
		{
			int numFil = listaFiltroColumna.size();
			String[] filtroColumna = new String[numFil];
			String[] filtroTipo = new String[numFil];
			String[] filtroOperador = new String[numFil];
			String[] filtroValor = new String[numFil];
			String[] filtroLogico = new String[numFil];
			String[] filtroOrden = new String[numFil];
			String[] filtroOriginal = new String[numFil];
			for(int iteFil = 0; iteFil < numFil; iteFil++)
			{
				filtroColumna[iteFil] = (String) listaFiltroColumna.get(iteFil);
				filtroTipo[iteFil] = (String) listaFiltroTipo.get(iteFil);
				filtroOperador[iteFil] = (String) listaFiltroOperador.get(iteFil);
				filtroValor[iteFil] = (String) listaFiltroValor.get(iteFil);
				filtroLogico[iteFil] = (String) listaFiltroLogico.get(iteFil);
				filtroOrden[iteFil] = (String) listaFiltroOrden.get(iteFil);
			}
			peticion.put("filtroColumna", filtroColumna);
			peticion.put("filtroTipo", filtroTipo);
			peticion.put("filtroOperador", filtroOperador);
			peticion.put("filtroValor", filtroValor);
			peticion.put("filtroLogico", filtroLogico);
			peticion.put("filtroOrden", filtroOrden);
			peticion.put("filtroOriginal", filtroOriginal);
		}
	}

	public void obtenerEntidadObjetoNodoActualEvento()
	{

		nodoActual = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		if(nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
		{
			objetoNodoActual = (ObjetoNodo) nodoActual.getUserObject();
			if(objetoNodoActual.getNodoCodigo() != null && objetoNodoActual.getNodoCodigo().size() > 0)
			{
				List listaFiltroLink = (List) objetoNodoActual.getNodoCodigo().get("filtroLink");
				for(int iteLis = 0; iteLis < listaFiltroLink.size(); iteLis++)
				{
					String filtroLink = (String) listaFiltroLink.get(iteLis);
					if(filtroLink.contains(","))
					{
						String[] link = filtroLink.split(",");
						paginaHijo = (link[0] != null) ? link[0] : "";
						paginaPadre = pagGen.pagAct.getPaginaHijo();
						paginaEvento = (link[1] != null) ? link[1] : "";
						paginaAccion = (link[2] != null) ? link[2] : "";
						paginaValor = (link[3] != null) ? link[3] : "";
						ejecutarAccion(paginaHijo, paginaEvento, paginaAccion, paginaValor);
					}// if (filtroLink.contains(","))
				}// for (int iteLis = 0; iteLis < listaFiltroLink.size(); iteLis++)
			}// if(objetoNodoActual.getNodoCodigo() != null && objetoNodoActual.getNodoCodigo().size() > 0)
		}// if (nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
	}

	public void cmdObtenerEntidadEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			peticion.put("Pagina_paginaHijo", paginaHijo);
			peticion.put("Pagina_paginaPadre", paginaPadre);
			peticion.put("Pagina_evento", paginaEvento);
			peticion.put("Pagina_accion", paginaAccion);
			peticion.put("Pagina_valor", paginaValor);
			ejecutar("cmdObtenerEntidadEvento", Peticion.SUBMIT);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			limpiarFormulario();
			llenarFormulario();
		}
	}

	public void cmdEliminarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				pagGen.pagAct.setModo(Modo.ELIMINANDO);
				obtenerDatosObjetoNodo();
				ejecutar("cmdEliminarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	public void cmdActualizarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				pagGen.pagAct.setModo(Modo.ELIMINANDO);
				obtenerDatosObjetoNodo();
				ejecutar("cmdActualizarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	public void tabCelActualizarComponentePorModeloEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				peticion.put("prdcodigo", modelo);
				peticion.put("pedcodigo", pedido);

				pagGen.pagAct.setModo(Modo.ELIMINANDO);
				obtenerDatoFormulario();
				ejecutar("tabCelActualizarComponentePorModeloEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			refrescarFormulario();
			llenarRegistrosTablaFichaControl();
		}
	}
	
	public void cmdAgregarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				pagGen.pagAct.setModo(Modo.AGREGANDO);
				obtenerDatoFormulario();
				peticion.put("filtroNoInclude", "Y");
				ejecutar("cmdAgregarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			
		}
	}

	private void menLlenarNodoEvento(TreeExpansionEvent event)
	{
		if(nodoActual!= null && nodoActual.getLevel() == 1)
		{
			nodoActual.removeAllChildren();
			DefaultTreeModel treeModelo = (DefaultTreeModel) tree.getModel();
			treeModelo.reload(nodoActual);
			if(nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
			{
				objetoNodoActual = (ObjetoNodo) nodoActual.getUserObject();
				pagGen.pagAct.setBrowseTabla(objetoNodoActual.getBrowseTabla());
				treeValueChangedEvento();
			}			
		}//if(nodoActual.getLevel() == 1)
	}

	public void menLlenarNodoEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			obtenerDatosObjetoNodo();
			ejecutar("menLlenarNodoEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}

	private void menRefrescarNodoEvento(TreeExpansionEvent event)
	{
		refrescarRegistros();
	}

	public void menRefrescarNodoEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			obtenerDatosObjetoNodo();
			ejecutar("menRefrescarNodoEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void llenarPanelExplorador()
	{

		DefaultTreeModel treeModelo = (DefaultTreeModel) tree.getModel();
		// System.out.println("Nivel : " + nodoActual.getLevel() + " Indice : " + nodoActual);
		if(nodoActual == null)
		{
			return;
		}
		List listaObjetos = obtenerListaObjetos();
		if(nodoActual.getChildCount() == 0)
		{
			if(listaObjetos != null)
			{
				for(int iteLis = 0; iteLis < listaObjetos.size(); iteLis++)
				{
					ObjetoNodo objetoNodo = (ObjetoNodo) listaObjetos.get(iteLis);
					if(nodoActual.getLevel() == 1)
					{
						treeNivel2 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel2);
					}
					if(nodoActual.getLevel() == 2)
					{
						treeNivel3 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel3);
					}
					if(nodoActual.getLevel() == 3)
					{
						treeNivel4 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel4);
					}
					if(nodoActual.getLevel() == 4)
					{
						treeNivel5 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel5);
					}
					if(nodoActual.getLevel() == 5)
					{
						treeNivel6 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel6);
					}
				}// for (int iteLis = 1; iteLis <= 10; iteLis++)
			}// if(listaObjetos != null)
			treeModelo.reload(nodoActual);
		}// if(llenarTree)
	}

	public List obtenerListaObjetos()
	{

		List listaObjetos = new ArrayList();
		List listaRegistros = (List) browse.getPageResults();
		if(listaRegistros != null && listaRegistros.size() > 0)
		{
			for(int iteFil = 0; iteFil < listaRegistros.size(); iteFil++)
			{
				Map filtroCodigo = new HashMap();
				List nodoFiltroColumna = new ArrayList();
				List nodoFiltroTipo = new ArrayList();
				List nodoFiltroOperador = new ArrayList();
				List nodoFiltroValor = new ArrayList();
				List nodoFiltroLogico = new ArrayList();
				List nodoFiltroOrden = new ArrayList();
				List nodoFiltroLink = new ArrayList();
				String nodoCodigo = "";
				String nodoDescripcion = "";
				for(int iteRegCmp = 0; iteRegCmp < browseColumns.length; iteRegCmp++)
				{
					BrowseColumn browseColumn = browseColumns[iteRegCmp];
					Object[] listaCampos = (Object[]) listaRegistros.get(iteFil);
					Object objeto = (Object) listaCampos[iteRegCmp];
					if(objeto instanceof String) {
						nodoCodigo = (String) objeto;
					}
					else if(objeto instanceof Integer) {
						nodoCodigo = (String.valueOf((Integer) objeto));
					}
					if(!Util.isEmpty(browseColumn.getTreeRelationColumn()))
					{
						nodoFiltroColumna.add(browseColumn.getTreeRelationColumn());
						nodoFiltroTipo.add(browseColumn.getType());
						nodoFiltroOperador.add("=");
						nodoFiltroValor.add(nodoCodigo);
						nodoFiltroLogico.add("AND");
						nodoFiltroOrden.add("N");
					}
					if(!Util.isEmpty(browseColumn.getLink()) && browseColumn.getAlias().equalsIgnoreCase("OBTENER"))
					{
						nodoFiltroLink.add(obtenerValor(browseColumn, listaCampos));
					}
					if(browseColumn.getTreeDescription().equals("Y") && !browseColumn.isHidden())
					{
						if(objeto instanceof String)
						{
							nodoDescripcion = nodoDescripcion + (nodoDescripcion.equals("") ? "" : " : ") + (String) objeto;
						}
						if(objeto instanceof Integer)
						{
							nodoDescripcion = nodoDescripcion + (nodoDescripcion.equals("") ? "" : " : ") + (Integer) objeto;
						}
					}
				}
				filtroCodigo.put("filtroColumna", nodoFiltroColumna);
				filtroCodigo.put("filtroTipo", nodoFiltroTipo);
				filtroCodigo.put("filtroOperador", nodoFiltroOperador);
				filtroCodigo.put("filtroValor", nodoFiltroValor);
				filtroCodigo.put("filtroLogico", nodoFiltroLogico);
				filtroCodigo.put("filtroOrden", nodoFiltroOrden);
				filtroCodigo.put("filtroLink", nodoFiltroLink);
				listaObjetos.add(new ObjetoNodo(filtroCodigo, nodoDescripcion, browseObject.getBrowseFilters(), browseObject.getTreeRelation()));
			}
		}
		return listaObjetos;
	}

	public Object obtenerValor(BrowseColumn columna, Object objetoRegistro)
	{

		try
		{
			Object columnValue = BrowseUtility.getColumnObject(columna, objetoRegistro, "", browseColumns, browse.getBrowseId(), "");
			String columnResult = "";
			if(columna.getLink() != null && columna.getLink().length() > 0)
			{
				columnResult = BrowseUtility.getColumnLinkObject(columna, objetoRegistro, browseColumns);
				columnResult = columnResult.replace("javascript: abrirPagina(", "");
				columnResult = columnResult.replace("'", "");
				columnResult = columnResult.replace(");", "");
			}
			columnValue = (columnResult.equals("")) ? columnValue : columnResult;
			return columnValue;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	private void treeMousePressedEvento(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_panArtFotoMousePressed

		if(evt.getButton() == MouseEvent.BUTTON3) {
			popMenTree.show(evt.getComponent(), evt.getX(), evt.getY());
		}// Fin de if (evt.getButton() == MouseEvent.BUTTON3)
	}// GEN-LAST:event_panArtFotoMousePressed

	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdImprimirEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (validarFormulario())
			{
				if(nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
				{
					objetoNodoActual = (ObjetoNodo) nodoActual.getUserObject();
					if(objetoNodoActual.getBrowseTabla().equals("pedfic-pedmod-prd-tal-fic"))
					{
						String[] valor = objetoNodoActual.getNodoDescripcion().split(" : ");
						peticion.put("pedcodigo", Integer.parseInt(valor[1]));
						peticion.put("prdcodigo", Integer.parseInt(valor[3]));
					}//if(objetoNodoActual.getBrowseTabla().equals("pedfic-pedmod"))
				}				
				peticion.put("organizacionIde", OrganizacionGeneral.getOrgcodigo());
				peticion.put("browseTabla", "ped-mod");
				peticion.put("reportName", "ped-mod");
				peticion.put("webreport", "Y");
				peticion.put("format", ((opcPdf.isSelected()) ? "pdf" : "xls"));

				List listaFiltro = new ArrayList();
				listaFiltro.add(Util.getInst().crearRegistroFiltro("Pedido.ped_cod", "INTEGER", "=", peticion.get("pedcodigo").toString(), "AND",""));
				listaFiltro.add(Util.getInst().crearRegistroFiltro("Producto.prd_cod", "INTEGER", "=",  peticion.get("prdcodigo").toString(), "AND", ""));
				Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

				ejecutar("cmdImprimirEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			refrescarFormulario();
		}
	}

	public void obtenerObjetoNodoFiltrosCodigo()
	{

		if(nodoActual == null)
		{
			return;
		}
		listaFiltroColumna.clear();
		listaFiltroTipo.clear();
		listaFiltroOperador.clear();
		listaFiltroValor.clear();
		listaFiltroLogico.clear();
		listaFiltroOrden.clear();
		if(nodoActual.getRoot().getChildCount() >= 0)
		{
			TreeNode treeNodo = nodoActual;
			while (treeNodo!=null)
			{
				DefaultMutableTreeNode nodo = (DefaultMutableTreeNode)treeNodo;
				if(nodo.getUserObject() instanceof ObjetoNodo)
				{
					ObjetoNodo objetotreeNodo = (ObjetoNodo) nodo.getUserObject();
					if(objetotreeNodo.getNodoCodigo() != null && objetotreeNodo.getNodoCodigo().size() > 0)
					{
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroColumna, "filtroColumna");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroTipo, "filtroTipo");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroOperador, "filtroOperador");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroValor, "filtroValor");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroLogico, "filtroLogico");
						obtenerNodoFiltroCodigo(objetotreeNodo, listaFiltroOrden, "filtroOrden");
					}
				}// if(nodeInfo instanceof ObjetoNodo)
				treeNodo = treeNodo.getParent();
			}
		}
	}

	public void llenarPanelTablaGeneral()
	{
		crearModeloTabla(browseObject.getBrowseColumns());
		crearTablaResultado();
		llenarTabla();
	}

	// *****************************************
	// METODOS PARA CONTROLAR LA TABLA GENERAL
	// *****************************************
	private void refrescarPanelTablaGeneral()
	{
		if(browseObject != null)
		{
			frmAbierto = false;
			String botonAgregar = browseObject.getBotonAgregar();
			if(!botonAgregar.equals(""))
			{
				botonAgregar = botonAgregar.replace("javascript: abrirPagina(", "");
				botonAgregar = botonAgregar.replace("'", "");
				botonAgregar = botonAgregar.replace(");", "");
				String[] valores = botonAgregar.split(",");
				paginaHijo = valores[0];
				paginaPadre = pagGen.pagAct.getPaginaHijo();
				paginaEvento = valores[1];
				paginaAccion = valores[2];
				paginaValor = "";
			}
			boolean valor = (botonAgregar.equals("")) ? false : true;
			panTabGen.cmdAgregar.setEnabled((botonAgregar.equals("")) ? false : true);
			for(int iteCmbPag = 0; iteCmbPag < panTabGen.cmbTamanoPagina.getItemCount(); iteCmbPag++)
			{
				String valorCombo = (String) panTabGen.cmbTamanoPagina.getItemAt(iteCmbPag);
				if(Util.isInteger(valorCombo))
				{
					if(browseObject.getPageSize() == Integer.parseInt(valorCombo))
					{
						panTabGen.cmbTamanoPagina.setSelectedIndex(iteCmbPag);
						break;
					}
				}
			}
			panTabGen.txtPaginaActual.setText(String.valueOf(browseObject.getCurrentPage()));
			if(panTabGen.modeloTablaGeneral.getRowCount() > 0)
			{
				panTabGen.tablaBrowse.setEnabled(true);
				panTabGen.txtPaginaActual.setEnabled(true);
				panTabGen.cmbTamanoPagina.setEnabled(true);
				for (int ite = 0; ite < panTabGen.cmdArrayPag.length; ite++)
				{
					panTabGen.cmdArrayPag[ite].setEnabled(true);
				}
			}
			else
			{
				if(panTabGen.cmdArrayPag != null) {
					for(int ite = 0; ite < panTabGen.cmdArrayPag.length; ite++)
					{
						panTabGen.remove(panTabGen.cmdArrayPag[ite]);
					}
				}
				panTabGen.repaint();
				panTabGen.etiInfo.setText("");
				panTabGen.txtPaginaActual.setValue(0);
				panTabGen.cmbTamanoPagina.setSelectedIndex(-1);
				panTabGen.tablaBrowse.setEnabled(false);
				panTabGen.txtPaginaActual.setEnabled(false);
				panTabGen.cmbTamanoPagina.setEnabled(false);
			}
			frmAbierto = true;
		}//if(browseObject != null)
	}

	private void crearModeloTabla(BrowseColumn[] browseColumns) {

		panTabGen.modeloTablaGeneral = new ModeloTablaGeneral(browseColumns);
		panTabGen.tablaBrowse.setModel(panTabGen.modeloTablaGeneral);
		panTabGen.celdaRenderGeneral = new CeldaRenderGeneral();
		try
		{
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.String"), panTabGen.celdaRenderGeneral);
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.Integer"), panTabGen.celdaRenderGeneral);
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.Double"), panTabGen.celdaRenderGeneral);
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.math.BigDecimal"), panTabGen.celdaRenderGeneral);
			panTabGen.tablaBrowse.setDefaultRenderer(Class.forName("java.sql.Date"), panTabGen.celdaRenderGeneral);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
		// panTabGen.tablaBrowse.addPropertyChangeListener(new ModtablaBrowsePropiedad(this, tablaBrowse));
		panTabGen.tablaBrowse.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		// panTabGen.tablaBrowse.getTableHeader().setPreferredSize(new Dimension(panTabGen.tablaBrowse.getTableHeader().getWidth(), 25));
		// panTabGen.tablaBrowse.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 13));
		panTabGen.tablaBrowse.setRowHeight(25);
		panTabGen.tablaBrowse.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		panTabGen.tablaBrowse.setShowGrid(true);
		panTabGen.columna = new TableColumn[browseColumns.length];

		for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			panTabGen.columna[iteBrwCol] = panTabGen.tablaBrowse.getColumn(browseColumn.getColumnName());
			panTabGen.columna[iteBrwCol].setResizable(true);
			panTabGen.columna[iteBrwCol].setPreferredWidth(browseColumn.getSize() * 9);
			panTabGen.columna[iteBrwCol].setHeaderValue(browseColumn.getLabel());
			panTabGen.columna[iteBrwCol].setCellRenderer(obtenerCellRender(browseColumn));
			panTabGen.columna[iteBrwCol].setCellEditor(obtenerCellEdit(panTabGen.tablaBrowse, browseColumns, browseColumn));
			// columna[iteBrwCol].addPropertyChangeListener(new ListenerCambioPropiedad());
			if(browseColumn.isHidden())
			{
				panTabGen.tablaBrowse.getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				panTabGen.tablaBrowse.getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
				panTabGen.tablaBrowse.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				panTabGen.tablaBrowse.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
			}// Fin de if (ancho[iteBrwCol] == 0)
		}// Recorrer la tablaBrowse de encabezados
	}// Fin de crearModeloTabla()

	public TableCellRenderer obtenerCellRender(BrowseColumn browseColumn)
	{

		if(browseColumn.isButtonInput())
		{
			return new CellRenderJButton(obtenerBoton(browseColumn.getAlias()));
		}
		return panTabGen.celdaRenderGeneral;
	}

	public TableCellEditor obtenerCellEdit(JTable tabla, BrowseColumn[] browseColumns, BrowseColumn browseColumn)
	{

		if(browseColumn.isButtonInput())
		{
			return new CellEditJButton(obtenerBoton(browseColumn.getAlias()), this);
		}
		if(browseColumn.isTextInput())
		{
			return new CellEditDecimalTextInput(this, tabla, browseColumns);
		}
		return null;
	}

	public JButton obtenerBoton(String nombre)
	{

		JButton boton = new JButton(nombre);
		boton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		boton.setMargin(new Insets(0, 0, 0, 0));
		return boton;
	}

	public void crearTablaResultado()
	{

		List listResult = (List) browse.getPageResults();
		panTabGen.totalRegistros = browse.getRowCount();
		panTabGen.tamanoPagina = browse.getPageSize();
		panTabGen.totalPaginas = browse.getPageCount();
		panTabGen.paginaActual = browse.getCurrentPage();
		if(listResult != null && listResult.size() > 0)
		{
			// totalRegistros = listResult.size();
			panTabGen.visiblePages = 5;
			panTabGen.totalPaginas = panTabGen.totalRegistros / panTabGen.tamanoPagina;
			panTabGen.totalPaginas = (panTabGen.totalPaginas < 0) ? 0 : panTabGen.totalPaginas;
			panTabGen.resto = panTabGen.totalRegistros % panTabGen.tamanoPagina;
			panTabGen.totalPaginas = (panTabGen.resto > 0) ? panTabGen.totalPaginas + 1 : panTabGen.totalPaginas;
			if(panTabGen.totalPaginas < panTabGen.visiblePages)
			{
				panTabGen.visiblePages = panTabGen.totalPaginas;
			}
			for(int itePag = 1; itePag <= panTabGen.totalPaginas; itePag++)
			{
				int paginaActualReferencia = panTabGen.paginaActual;
				int inicioPagina = panTabGen.visiblePages * (itePag - 1);
				int finPagina = panTabGen.visiblePages * itePag;
				if(paginaActualReferencia > inicioPagina && paginaActualReferencia <= finPagina)
				{
					panTabGen.iteIni = itePag;
					panTabGen.iteFin = itePag + panTabGen.visiblePages - 1;
					break;
				}
			}
			panTabGen.paginaActual = panTabGen.paginaActual;
			// panTabGen.iniItePag = panTabGen.paginaActual;
			// if (panTabGen.totalPaginas <= panTabGen.visiblePages) {
			// panTabGen.visiblePages = panTabGen.totalPaginas;
			// }
			if(panTabGen.cmdArrayPag != null) {
				for(int ite = 0; ite < panTabGen.cmdArrayPag.length; ite++)
				{
					panTabGen.remove(panTabGen.cmdArrayPag[ite]);
				}
				panTabGen.cmdArrayPag = null;
			}
			panTabGen.repaint();
			crearBotonesPaginacion();
			panTabGen.repaint();
		}// Fin de if(listResult != null)
	}

	public void crearBotonesPaginacion() {

		panTabGen.numeroBoton = panTabGen.visiblePages;
		panTabGen.iteCmd = 0;
		panTabGen.numeroBoton = panTabGen.visiblePages + 2;
		panTabGen.cmdArrayPag = new JButton[panTabGen.numeroBoton];
		// CREAMOS EL BOTON DE ATRAS
		panTabGen.coordX = (panTabGen.iteCmd * panTabGen.cmdPagWidth) + panTabGen.marginLeft;
		panTabGen.cmdArrayPag[0] = crearBoton(panTabGen.coordX, panTabGen.coordY, panTabGen.cmdPagWidth, panTabGen.cmdPagHeigth, 0);
		panTabGen.add(panTabGen.cmdArrayPag[0]);
		panTabGen.iteCmd++;
		// CREAMOS LOS BOTONES VISIBLES
		for(int ite = panTabGen.iteIni; ite <= panTabGen.iteFin; ite++) {
			panTabGen.coordX = (panTabGen.iteCmd * panTabGen.cmdPagWidth) + panTabGen.marginLeft;
			panTabGen.cmdArrayPag[panTabGen.iteCmd] = crearBoton(panTabGen.coordX, panTabGen.coordY, panTabGen.cmdPagWidth, panTabGen.cmdPagHeigth, panTabGen.iteCmd);
			panTabGen.add(panTabGen.cmdArrayPag[panTabGen.iteCmd]);
			panTabGen.iteCmd++;
			// panTabGen.iniItePag++;
		}
		// CREAMOS EL BOTON DE ADELANTE
		panTabGen.coordX = (panTabGen.iteCmd * panTabGen.cmdPagWidth) + panTabGen.marginLeft;
		panTabGen.cmdArrayPag[panTabGen.iteCmd] = crearBoton(panTabGen.coordX, panTabGen.coordY, panTabGen.cmdPagWidth, panTabGen.cmdPagHeigth, panTabGen.iteCmd);
		panTabGen.add(panTabGen.cmdArrayPag[panTabGen.iteCmd]);
		panTabGen.iteCmd++;
		if(panTabGen.etiInfo == null) {
			panTabGen.etiInfo = new JLabel();
			panTabGen.add(panTabGen.etiInfo);
		}
		panTabGen.coordX = (panTabGen.iteCmd * panTabGen.cmdPagWidth) + panTabGen.marginLeft;
		panTabGen.etiInfo.setBounds(panTabGen.coordX + panTabGen.marginLeft, panTabGen.coordY, 400, panTabGen.cmdPagHeigth);
		panTabGen.etiInfo.setText("Paginas : " + panTabGen.totalPaginas + " Registros : " + panTabGen.totalRegistros);
	}

	public JButton crearBoton(int coordX, int coordY, int cmdPagWidth, int cmdHeight, int parIndex) {

		JButton cmdPag = new JButton();
		cmdPag.setBounds(coordX, coordY, cmdPagWidth, cmdHeight);
		String nombreBoton = "";
		if(parIndex == 0) {
			nombreBoton = "atras_0";
			cmdPag.setText("<<");
		}
		else if((parIndex - 1) == panTabGen.visiblePages) {
			nombreBoton = "adelante_0";
			cmdPag.setText(">>");
		}
		else {
			nombreBoton = "cmdArrayPag_" + String.valueOf(parIndex);
			cmdPag.setText(String.valueOf(parIndex));
		}
		cmdPag.setName(nombreBoton);
		cmdPag.setActionCommand(nombreBoton);
		cmdPag.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPag.setBounds(coordX, coordY, cmdPagWidth, cmdHeight);
		cmdPag.setForeground(panTabGen.naranja);
		// cmdPag.setBackground(colorFondo);
		// cmdPag.setForeground(colorTexto);
		cmdPag.setMargin(new java.awt.Insets(0, 0, 0, 0));
//		cmdPag.addActionListener(new cmdArrayPagListener(this));
		// System.out.println("iteini : " + panTabGen.iteIni + " itefin : " + panTabGen.iteFin + " itecmd : " + panTabGen.iteCmd + cmdPag.getName() + " : " + cmdPag.getActionCommand());
		return cmdPag;
	}// Fin de Metodo para crear botones de dias de calendario

	public void cmbTamanoPaginaEvento(ItemEvent e)
	{

		if(e.getStateChange() == ItemEvent.SELECTED)
		{
			if(frmAbierto == true)
			{
				if(panTabGen.cmbTamanoPagina.getSelectedIndex() > -1)
				{
					panTabGen.tamanoPagina = Integer.parseInt(panTabGen.cmbTamanoPagina.getSelectedItem().toString());
					browse.setCurrentPage(1);
					browseObject.setCurrentPage(1);
					browse.setPageSize(panTabGen.tamanoPagina);
					refrescarRegistros();
				}
			}
		}
	}

	public void setCurrentPage(int currentPage) {

		for(int ite = 0; ite < panTabGen.numeroBoton; ite++) {
			panTabGen.cmdArrayPag[ite].setForeground(panTabGen.naranja);
			String[] valCmd = panTabGen.cmdArrayPag[ite].getName().split("_");
			if(!valCmd[0].equals("atras") && !valCmd[0].equals("adelante")) {
				if((Integer.parseInt(valCmd[1])) == currentPage) {
					panTabGen.cmdArrayPag[ite].setForeground(panTabGen.rojo);
				}
			}
		}
		panTabGen.paginaActual = currentPage;
	}

	public void txtPaginaActualEvento(KeyEvent e)
	{

		if(Util.isInteger(panTabGen.txtPaginaActual.getText()))
		{
			int pagina = Integer.parseInt(panTabGen.txtPaginaActual.getText());
			if(e.getKeyChar() == KeyEvent.VK_ENTER && pagina <= browse.getPageCount())
			{
				browse.setCurrentPage(Integer.parseInt(panTabGen.txtPaginaActual.getText()));
				refrescarRegistros();
			}
		}
	}

	public void llenarTabla()
	{

		panTabGen.paginaActual = browse.getCurrentPage();
		setCurrentPage(panTabGen.paginaActual);
		List listaRegistros = (List) browse.getPageResults();
		Object[] nuevaFila = new Object[browseColumns.length];
		for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			if(browseColumn.getType().equals("INTEGER")) {
				nuevaFila[iteBrwCol] = 0;
			}
			if(browseColumn.getType().equals("DOUBLE")) {
				nuevaFila[iteBrwCol] = 0.00;
			}
			if(browseColumn.getType().equals("STRING")) {
				nuevaFila[iteBrwCol] = "";
			}
			if(browseColumn.getType().equals("BOOLEAN")) {
				nuevaFila[iteBrwCol] = false;
			}
			if(browseColumn.getType().equals("BIGDECIMAL")) {
				nuevaFila[iteBrwCol] = new BigDecimal("000000000000.00");
			}
		}
		panTabGen.modeloTablaGeneral.setRowCount(0);
		if(listaRegistros != null && listaRegistros.size() > 0)
		{
			for(int iteFil = 0; iteFil < listaRegistros.size(); iteFil++)
			{
				Object[] lisCol = (Object[]) listaRegistros.get(iteFil);
				Object objetoRegistro = listaRegistros.get(iteFil);
				panTabGen.modeloTablaGeneral.addRow(nuevaFila);
				for(int iteCol = 0; iteCol < lisCol.length; iteCol++)
				{
					panTabGen.modeloTablaGeneral.setValueAt(obtenerValor(iteCol, objetoRegistro, browseColumns), iteFil, iteCol);
				}
			}
		}
	}

	public Object obtenerValor(int idxCol, Object objetoRegistro, BrowseColumn[] browseColumns)
	{

		try
		{
			BrowseColumn columna = browseColumns[idxCol];
			Object columnValue = BrowseUtility.getColumnObject(columna, objetoRegistro, "sisem", browseColumns, browse.getBrowseId(), "");
			String columnResult = "";
			String allowEdit = "true";
			if(columna.getType().equals("ConditionalLink")) {
				columnResult = " <div id=\"browse_" + columna.getColumnName() + "\">";
			}
			else if(columna.getLink() != null && columna.getLink().length() > 0)
			{
				columnResult = BrowseUtility.getColumnLinkObject(columna, objetoRegistro, browseColumns);
				columnResult = columnResult.replace("javascript: abrirPagina(", "");
				columnResult = columnResult.replace("'", "");
				columnResult = columnResult.replace(");", "");
			}
			columnValue = (columnResult.equals("")) ? columnValue : columnResult;
			return columnValue;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	private void panTabGenTablaBrowseMousePressed(java.awt.event.MouseEvent evt)
	{// GEN-FIRST:event_tabSubDetVenMousePressed

//		seleccionarRegistroPanTabGenTableBrowse();
	}// GEN-LAST:event_tabSubDetVenMousePressed

	private void panTabGenTablaBrowseKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_tabSubDetVenKeyReleased

		if(evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_PAGE_UP || evt.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_F2 || evt.getKeyCode() == KeyEvent.VK_TAB) {
//			panTabGenTablaBrowseMousePressed(null);
		}
	}// GEN-LAST:event_tabSubDetVenKeyReleased

	private void seleccionarRegistroPanTabGenTableBrowse()
	{

		if(panTabGen.tablaBrowse.isEditing())
		{
			pagGen.pagAct.setModo(Modo.MODIFICANDO);
		}
		else if(!panTabGen.tablaBrowse.isEditing())
		{
			pagGen.pagAct.setModo(Modo.VISUALIZANDO);
			recuperarRegistroParaFicControl();
			if(panTabGen.tablaBrowse.getSelectedRow() > -1)
			{
			}
		}
		refrescarFormulario();
	}// Fin de seleccionarRegistroTablaSucursal()

	private void recuperarRegistroParaFicControl()
	{
		if(browseObject.getBrowseTabla().equals("pedfic-pedmod"))
		{
			if (panTabGen.tablaBrowse.getSelectedRow() > -1) 
			{
				int colMod = panTabGen.obtenerIndiceHeader("codmodelo");
				int colPed = panTabGen.obtenerIndiceHeader("pedcodigo");
				
				if(colMod > -1 && colPed > -1)
				{
					Object a = panTabGen.tablaBrowse.getValueAt(panTabGen.tablaBrowse.getSelectedRow(), colMod);// Codigo del modelo
					modelo = (Integer) panTabGen.tablaBrowse.getValueAt(panTabGen.tablaBrowse.getSelectedRow(), colMod);// Codigo del modelo
					pedido = (Integer) panTabGen.tablaBrowse.getValueAt(panTabGen.tablaBrowse.getSelectedRow(), colPed);// Codigo del pedido
					recuperarRegistroParaFicControlEvento();
				}
			}// Fin de if(parNumFil > -1)
		}
	}
	
	public void recuperarRegistroParaFicControlEvento()
	{
		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			
			peticion.put("prdcodigo", modelo);
			peticion.put("pedcodigo", pedido);
			
			ejecutar("recuperarRegistroParaFicControlEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			refrescarFormulario();
			generarTablaFichaControl();
		}
	}
	
	private void generarTablaFichaControl()
	{
		List listaTallas = (List) peticion.get("listaTallas");
		BrowseColumn[] browseColumnsFichaControl = obtenerBrowseColumnFichaControl(listaTallas);
		crearModeloTablaFichaControl(browseColumnsFichaControl);
		llenarRegistrosTablaFichaControl();
	}
	
	private BrowseColumn[] obtenerBrowseColumnFichaControl(List lista)
	{
		browseColumnsFichaControl = new BrowseColumn[lista.size() + 3];
		
		browseColumnsFichaControl[0] = new BrowseColumn();
		browseColumnsFichaControl[0].setColumnName("DetComponente_detcmpcodigo");
		browseColumnsFichaControl[0].setLabel("actualizar_detcomponente");
		browseColumnsFichaControl[0].setAlias("detcmpcodigo");
		browseColumnsFichaControl[0].setType("STRING");
		browseColumnsFichaControl[0].setEditable(false);
		browseColumnsFichaControl[0].setHidden(true);
		browseColumnsFichaControl[0].setLink("javascript: abrirPagina('DlgDetComponente','tabCelActualizarComponentePorModeloEvento','com.componente.accion.ActualizarComponentePorModelo','#DetComponente_detcmpcodigo^');");
		browseColumnsFichaControl[0].setSize(12);
		BrowseUtility.getColumnNamesFromLink(browseColumnsFichaControl[0]);

		browseColumnsFichaControl[1] = new BrowseColumn();
		browseColumnsFichaControl[1].setColumnName("Componente_cmpcodigo");
		browseColumnsFichaControl[1].setLabel("Codigo");
		browseColumnsFichaControl[1].setAlias("cmpcodigo");
		browseColumnsFichaControl[1].setType("BIGDECIMAL");
		browseColumnsFichaControl[1].setEditable(false);
		browseColumnsFichaControl[1].setHidden(true);
		browseColumnsFichaControl[1].setSize(12);

		browseColumnsFichaControl[2] = new BrowseColumn();
		browseColumnsFichaControl[2].setColumnName("Componente");
		browseColumnsFichaControl[2].setLabel("Componente");
		browseColumnsFichaControl[2].setAlias("componente");
		browseColumnsFichaControl[2].setType("STRING");
		browseColumnsFichaControl[2].setEditable(false);
		browseColumnsFichaControl[2].setSize(16);
		
		for(int iteLis = 0; iteLis < lista.size(); iteLis++)
		{
			Object[] registro = (Object[]) lista.get(iteLis);
			browseColumnsFichaControl[iteLis+3] = new BrowseColumn();
			browseColumnsFichaControl[iteLis+3].setColumnName((String)registro[2]);
			browseColumnsFichaControl[iteLis+3].setLabel((String)registro[2]);
			browseColumnsFichaControl[iteLis+3].setAlias((String)registro[2]);
			browseColumnsFichaControl[iteLis+3].setType("BIGDECIMAL");
			browseColumnsFichaControl[iteLis+3].setEditable(true);
			browseColumnsFichaControl[iteLis+3].setTextInput(true);
			browseColumnsFichaControl[iteLis+3].setSize(6);
			browseColumnsFichaControl[iteLis+3].setScriptCode("actualizar_detcomponente");
		}
		
		return browseColumnsFichaControl;
	}
	
	private void crearModeloTablaFichaControl(BrowseColumn[] browseColumns) {

		modTabFicCtrl = new ModeloTablaGeneral(browseColumns);
		tabFicCtrl.setModel(modTabFicCtrl);
		celdaRenderGeneral = new CeldaRenderGeneral();
		try
		{
			tabFicCtrl.setDefaultRenderer(Class.forName("java.lang.String"), celdaRenderGeneral);
			tabFicCtrl.setDefaultRenderer(Class.forName("java.lang.Integer"), celdaRenderGeneral);
			tabFicCtrl.setDefaultRenderer(Class.forName("java.lang.Double"), celdaRenderGeneral);
			tabFicCtrl.setDefaultRenderer(Class.forName("java.math.BigDecimal"), celdaRenderGeneral);
			tabFicCtrl.setDefaultRenderer(Class.forName("java.sql.Date"), celdaRenderGeneral);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
		// tabFicCtrl.addPropertyChangeListener(new ModtablaBrowsePropiedad(this, tablaBrowse));
		tabFicCtrl.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		// tabFicCtrl.getTableHeader().setPreferredSize(new Dimension(tabFicCtrl.getTableHeader().getWidth(), 25));
		// tabFicCtrl.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 13));
		tabFicCtrl.setRowHeight(25);
		tabFicCtrl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabFicCtrl.setShowGrid(true);
		colTabFicCtrl = new TableColumn[browseColumns.length];

		for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			colTabFicCtrl[iteBrwCol] = tabFicCtrl.getColumn(browseColumn.getColumnName());
			colTabFicCtrl[iteBrwCol].setResizable(true);
			colTabFicCtrl[iteBrwCol].setPreferredWidth(browseColumn.getSize() * 9);
			colTabFicCtrl[iteBrwCol].setHeaderValue(browseColumn.getLabel());
			colTabFicCtrl[iteBrwCol].setCellRenderer(obtenerCellRender(browseColumn));
			colTabFicCtrl[iteBrwCol].setCellEditor(obtenerCellEdit(tabFicCtrl, browseColumnsFichaControl, browseColumn));
			// columna[iteBrwCol].addPropertyChangeListener(new ListenerCambioPropiedad());
			if(browseColumn.isHidden())
			{
				tabFicCtrl.getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				tabFicCtrl.getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
				tabFicCtrl.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				tabFicCtrl.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
			}// Fin de if (ancho[iteBrwCol] == 0)
		}// Recorrer la tablaBrowse de encabezados
	}// Fin de crearModeloTabla()	
	
	private void llenarRegistrosTablaFichaControl()
	{
		
		List listaComponentes =  (List) peticion.get("listaComponentes");

		Object[] nuevaFila = new Object[browseColumnsFichaControl.length];
		for(int iteBrwCol = 0; iteBrwCol < browseColumnsFichaControl.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumnsFichaControl[iteBrwCol];
			if(browseColumn.getType().equals("INTEGER")) {
				nuevaFila[iteBrwCol] = 0;
			}
			if(browseColumn.getType().equals("DOUBLE")) {
				nuevaFila[iteBrwCol] = 0.00;
			}
			if(browseColumn.getType().equals("STRING")) {
				nuevaFila[iteBrwCol] = "";
			}
			if(browseColumn.getType().equals("BOOLEAN")) {
				nuevaFila[iteBrwCol] = false;
			}
			if(browseColumn.getType().equals("BIGDECIMAL")) {
				nuevaFila[iteBrwCol] = new BigDecimal("000000000000.00");
			}
		}
		modTabFicCtrl.setRowCount(0);
		if(listaComponentes != null && listaComponentes.size() > 0)
		{
			for(int iteLisCmp = 0; iteLisCmp < listaComponentes.size(); iteLisCmp++)
			{
				Object[] regCmp = (Object[]) listaComponentes.get(iteLisCmp);
				Object objetoRegistro = listaComponentes.get(iteLisCmp);
				modTabFicCtrl.addRow(nuevaFila);
				for(int iteRegCmp = 0; iteRegCmp < regCmp.length; iteRegCmp++)
				{
//					modTabFicCtrl.setValueAt(obtenerValor(iteRegCmp, objetoRegistro, browseColumnsFichaControl), iteLisCmp, iteRegCmp);
					modTabFicCtrl.setValueAt(BrowseUtility.obtenerValor(browse,browseColumnsFichaControl,objetoRegistro,iteRegCmp), iteLisCmp, iteRegCmp);
				}
			}
			llenarValoresTallaComponente();
		}
	}
	
	private void llenarValoresTallaComponente()
	{
		List listaTallaComponente = (List) peticion.get("listaTallaComponente");
		List lisHea = new ArrayList();
		List lisFil = new ArrayList();
		List lisRow = new ArrayList();
		
		tabCmp = new HashMap();
		for(int iteFil = 0; iteFil < tabFicCtrl.getRowCount(); iteFil++)
		{
			lisHea.add("Descripcion");
			for(int iteCol = 2; iteCol < tabFicCtrl.getColumnCount(); iteCol++)
			{
				BigDecimal cmpCodigo = (BigDecimal)tabFicCtrl.getValueAt(iteFil, 1);
				String talla = (String) tabFicCtrl.getTableHeader().getColumnModel().getColumn(iteCol).getHeaderValue();
				lisHea.add(talla);
				for(int iteLisTalCmp = 0; iteLisTalCmp < listaTallaComponente.size(); iteLisTalCmp++)
				{
					Object[] regLisTalCmp = (Object[]) listaTallaComponente.get(iteLisTalCmp);
//					System.out.println(cmpCodigo +  " : " + talla  + " : " + regLisTalCmp[0]+ " : " + regLisTalCmp[1]);
					if(regLisTalCmp[0].equals(cmpCodigo) && regLisTalCmp[1].equals(talla))
					{
						BigDecimal valor = (BigDecimal)regLisTalCmp[2];
						String qty_decimals = PropiedadGeneral.getInstance(OrganizacionGeneral.getOrgcodigo()).getProperty("MISC", "QTYDECIMALS", "2");
						valor = valor.setScale(Integer.valueOf(qty_decimals).intValue(), BigDecimal.ROUND_HALF_UP);
						
						modTabFicCtrl.setValueAt(valor, iteFil, iteCol);
						lisRow.add(valor);
						break;
					}
				}
				lisFil.add(lisRow);
			}
		}
		tabCmp.put("lisHea", lisHea);
		tabCmp.put("lisFil", lisFil);			
	}
	
	private void tabFicCtrlMousePressed(java.awt.event.MouseEvent evt)
	{// GEN-FIRST:event_tabSubDetVenMousePressed

		seleccionarRegistroTabFicCtrl();
	}// GEN-LAST:event_tabSubDetVenMousePressed

	private void tabFicCtrlKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_tabSubDetVenKeyReleased

		if(evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_PAGE_UP || evt.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_F2 || evt.getKeyCode() == KeyEvent.VK_TAB) {
			tabFicCtrlMousePressed(null);
		}
	}// GEN-LAST:event_tabSubDetVenKeyReleased

	private void seleccionarRegistroTabFicCtrl()
	{

		if(tabFicCtrl.isEditing())
		{
			pagGen.pagAct.setModo(Modo.MODIFICANDO);
		}
		else if(!tabFicCtrl.isEditing())
		{
			pagGen.pagAct.setModo(Modo.VISUALIZANDO);
//			llenarFormulario();
//			activarFormulario(true);
//			cmdCerrar.setEnabled(true);

//			llenarRegistrosTablaFichaControl();
//			recuperarRegistroParaFicControl();
//			if(panTabGen.tablaBrowse.getSelectedRow() > -1)
//			{
//			}
		}
		refrescarFormulario();
	}// Fin de seleccionarRegistroTablaSucursal()	
	
	
	/*
	 * LISTENER DE CLICK
	 */
	private static class cmdArrayPagListener implements ActionListener {

		private DlgTablaGeneral dlgTablaGeneral;

		public cmdArrayPagListener(DlgTablaGeneral dlgTablaGeneral) {

			this.dlgTablaGeneral = dlgTablaGeneral;
		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JButton objCmd = (JButton) e.getSource();
			String[] valCmd = objCmd.getActionCommand().split("_");
			if(valCmd[0].equals("atras") || valCmd[0].equals("adelante"))
			{
				if(valCmd[0].equals("adelante") && dlgTablaGeneral.browse.getCurrentPage() < dlgTablaGeneral.browse.getPageCount())
				{
					if(dlgTablaGeneral.panTabGen.paginaActual < dlgTablaGeneral.panTabGen.totalPaginas)
					{
						dlgTablaGeneral.panTabGen.paginaActual++;
						dlgTablaGeneral.browse.setCurrentPage(dlgTablaGeneral.panTabGen.paginaActual);
						dlgTablaGeneral.browseObject.setCurrentPage(dlgTablaGeneral.browse.getCurrentPage());
						dlgTablaGeneral.refrescarRegistros();
					}
				}
				else if(valCmd[0].equals("atras") && dlgTablaGeneral.browse.getCurrentPage() > 1)
				{
					if((dlgTablaGeneral.panTabGen.paginaActual) > 1)
					{
						dlgTablaGeneral.panTabGen.paginaActual--;
						dlgTablaGeneral.browse.setCurrentPage(dlgTablaGeneral.panTabGen.paginaActual);
						dlgTablaGeneral.browseObject.setCurrentPage(dlgTablaGeneral.browse.getCurrentPage());
						dlgTablaGeneral.refrescarRegistros();
					}
				}
				else {
					dlgTablaGeneral.panTabGen.paginaActual = dlgTablaGeneral.panTabGen.paginaActual;
				}
				// for (int ite = 0; ite < dlgTablaGeneral.panTabGen.numeroBoton; ite++) {
				// dlgTablaGeneral.remove(dlgTablaGeneral.panTabGen.cmdArrayPag[ite]);
				// }
				// dlgTablaGeneral.panTabGen.repaint();
				// // DlgTablaGeneral.update(DlgTablaGeneral.getGraphics());
				// dlgTablaGeneral.crearBotonesPaginacion(valCmd[0]);
			}
			else
			{
				dlgTablaGeneral.browse.setCurrentPage(Integer.parseInt(valCmd[1]));
				dlgTablaGeneral.browseObject.setCurrentPage(dlgTablaGeneral.browse.getCurrentPage());
				dlgTablaGeneral.refrescarRegistros();
			}
			// System.out.println("bootn : " + objCmd.getActionCommand());
		}// Fin de Action performed del boton de la palanca
	}// fin de la clase estatica para escuchar las acciones de los botones de palanca	
}
