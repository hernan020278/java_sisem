package com.sis.browse;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import com.comun.entidad.ObjetoNodo;

public class PanExplorador extends JPanel implements TreeSelectionListener, TreeExpansionListener
{
	private static final long serialVersionUID = 1L;
	private static JTree tree;
	private static DefaultMutableTreeNode treeNivel1 = null;
	private static DefaultMutableTreeNode treeNivel2 = null;
	private static DefaultMutableTreeNode treeNivel3 = null;
	private static DefaultMutableTreeNode treeNivel4 = null;
	private static DefaultMutableTreeNode treeNivel5 = null;
	private static DefaultMutableTreeNode treeNivel6 = null;

	private String[] nivel = { "Modelo", "Producto", "Talla", "Ficha", "Componente" };

	public PanExplorador()
	{
		super(new GridLayout(1, 0));

		DefaultMutableTreeNode raiz = new DefaultMutableTreeNode("Operaciones");

		treeNivel1 = new DefaultMutableTreeNode("Pedidos");
		raiz.add(treeNivel1);

		treeNivel2 = new DefaultMutableTreeNode("Modelos");
		treeNivel1.add(treeNivel2);

		treeNivel3 = new DefaultMutableTreeNode("Productos");
		treeNivel2.add(treeNivel3);

		treeNivel4 = new DefaultMutableTreeNode("Tallas");
		treeNivel3.add(treeNivel4);

		treeNivel5 = new DefaultMutableTreeNode("treeNivel5");
		treeNivel4.add(treeNivel5);

		treeNivel6 = new DefaultMutableTreeNode("Componentes");
		treeNivel5.add(treeNivel6);

		tree = new JTree(raiz);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

		tree.addTreeSelectionListener(this);
		tree.addTreeExpansionListener(this);

		JScrollPane treeView = new JScrollPane(tree);
		add(treeView);
	}

	private void crearTreeNode(DefaultMutableTreeNode padre, List listaHijos)
	{
		for (int iteLisHij = 0; iteLisHij < listaHijos.size(); iteLisHij++)
		{
			ObjetoNodo objetoNodo = (ObjetoNodo) listaHijos.get(iteLisHij);
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(objetoNodo);
			padre.add(node);
		}
	}

	/** Required by TreeSelectionListener interface. */
	public void valueChanged(TreeSelectionEvent e)
	{
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

		if (node == null)
			return;

		Object nodeInfo = node.getUserObject();
		if (node.isLeaf()) {
			if (nodeInfo instanceof ObjetoNodo)
			{
				ObjetoNodo objeto = (ObjetoNodo) nodeInfo;
				System.out.println("RAMA OBJETO : " + objeto.getNodoDescripcion());
			}
			else if (nodeInfo instanceof String)
			{
				System.out.println("RAMA STRING : " + nodeInfo);

			}
		}
		else
		{
			if (nodeInfo instanceof ObjetoNodo)
			{
				ObjetoNodo objeto = (ObjetoNodo) nodeInfo;
				System.out.println("CARPETA OBJETO : " + objeto.getNodoDescripcion());
			}
			else if (nodeInfo instanceof String)
			{
				System.out.println("CARPETA STRING : " + nodeInfo);

			}
		}
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event)
	{
		DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();
		System.out.println("Nivel : " + node.getLevel() + " Indice : " + node);
		if (node == null)
		{
			return;
		}
		List listaObjetos = new ArrayList();

		node.removeAllChildren();

		for (int iteLis = 1; iteLis <= 10; iteLis++)
		{
			ObjetoNodo objetoNodo = new ObjetoNodo(null, "Descripcion : " + iteLis, null, "Xml : " + iteLis);
			ObjetoNodo objetoSubNodo = new ObjetoNodo(null, "Descripcion : " + iteLis, null, "Xml : " + iteLis);
			if (node.getLevel() == 1)
			{
				treeNivel2 = new DefaultMutableTreeNode(objetoNodo);
				node.add(treeNivel2);

				treeNivel3 = new DefaultMutableTreeNode(objetoSubNodo);
				treeNivel2.add(treeNivel3);
			}
			if (node.getLevel() == 2)
			{
				treeNivel3 = new DefaultMutableTreeNode(objetoNodo);
				node.add(treeNivel3);

				treeNivel4 = new DefaultMutableTreeNode(objetoSubNodo);
				treeNivel3.add(treeNivel4);
			}
			if (node.getLevel() == 3)
			{
				treeNivel4 = new DefaultMutableTreeNode(objetoNodo);
				node.add(treeNivel4);

				treeNivel5 = new DefaultMutableTreeNode(objetoSubNodo);
				treeNivel4.add(treeNivel5);
			}
			if (node.getLevel() == 4)
			{
				treeNivel5 = new DefaultMutableTreeNode(objetoNodo);
				node.add(treeNivel5);

				treeNivel6 = new DefaultMutableTreeNode(objetoSubNodo);
				treeNivel5.add(treeNivel6);
			}
			if (node.getLevel() == 5)
			{
				treeNivel6 = new DefaultMutableTreeNode(objetoNodo);
				node.add(treeNivel6);
			}
		}// for (int iteLis = 1; iteLis <= 10; iteLis++)

		model.reload(node);

		Object nodeInfo = node.getUserObject();
		if (node.isLeaf()) {
			if (nodeInfo instanceof ObjetoNodo)
			{
				ObjetoNodo objeto = (ObjetoNodo) nodeInfo;
				System.out.println("RAMA OBJETO : " + objeto.getNodoDescripcion());
			}
			else if (nodeInfo instanceof String)
			{
				System.out.println("RAMA STRING : " + nodeInfo);

			}
		}
		else
		{
			if (nodeInfo instanceof ObjetoNodo)
			{
				ObjetoNodo objeto = (ObjetoNodo) nodeInfo;
				System.out.println("CARPETA OBJETO : " + objeto.getNodoDescripcion());
			}
			else if (nodeInfo instanceof String)
			{
				System.out.println("CARPETA STRING : " + nodeInfo);

			}
		}
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		// TODO Auto-generated method stub

	}

	/**
	 * Create the GUI and show it. For thread safety,
	 * this method should be invoked from the
	 * event dispatch thread.
	 */
	private static void createAndShowGUI() {

		JFrame frame = new JFrame("prueba");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLayout(new GridLayout(2, 1));
		// Add content to the window.
		frame.add(new PanExplorador());

		JButton boton = new JButton("Agregar");
		boton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ObjetoNodo objetoNodo = new ObjetoNodo(null, "Descripcion : ", null, "Xml : ");
				treeNivel3 = new DefaultMutableTreeNode(objetoNodo);
				treeNivel2.add(treeNivel3);

				treeNivel4 = new DefaultMutableTreeNode(objetoNodo);
				treeNivel3.add(treeNivel4);
				DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
				model.reload(treeNivel2);
			}
		});

		frame.add(boton);
		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
