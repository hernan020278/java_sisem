package com.sis.contenedor;

import java.sql.Date;
import java.sql.Time;
import java.util.HashMap;

import com.asis.main.*;
import com.comun.utilidad.CodigoBarras;
import com.comun.utilidad.Util;
import com.sis.browse.DlgBrowseOpciones;
import com.sis.browse.DlgBrowseTabla;
import com.sis.browse.DlgControlProceso;
import com.sis.browse.DlgReporteOpciones;
import com.sis.browse.DlgReporteTabla;
import com.sis.browse.DlgTablaGeneral;
import com.sis.browse.DlgTablaGeneralBrowse;
import com.sis.main.FrmSisem;
import com.sis.produccion.DlgComponente;
import com.sis.produccion.DlgControlComponente;
import com.sis.produccion.DlgControlPartida;
import com.sis.produccion.FrmInicio;
import com.sis.usuario.DlgAcceso;
import com.sis.usuario.DlgRegistroOrganizacion;
import com.sis.usuario.DlgUsuario;

public class Contenedor {

	static HashMap<String, Object> componentes = initMap();

	private static HashMap<String, Object> initMap() {

		componentes = new HashMap<String, Object>();
		// ---------------------------------------------
		// -
		// -
		// -
		// VISTA - DE SISTEMA ANTERIOR
		// -
		// -
		// -
		// ---------------------------------------------
		FrmSisem frmSisem = new FrmSisem();
		componentes.put("frmSisem", frmSisem);

		FrmInicio frmInicio = new FrmInicio();
		componentes.put("frmInicio", frmInicio);

		FrmAsistencia frmAsistencia = new FrmAsistencia();
		componentes.put("frmAsistencia", frmAsistencia);

		FrmMarcar frmMarcar = new FrmMarcar();
		componentes.put("frmMarcar", frmMarcar);

		DlgControlProceso dlgControlProceso = new DlgControlProceso(frmInicio);
		componentes.put("dlgControlProceso", dlgControlProceso);

		DlgAcceso dlgAcceso = new DlgAcceso(frmInicio);
		componentes.put("dlgAcceso", dlgAcceso);

		DlgRegistroOrganizacion dlgRegistroOrganizacion = new DlgRegistroOrganizacion(frmInicio);
		componentes.put("dlgRegistroOrganizacion", dlgRegistroOrganizacion);

		DlgBrowseOpciones dlgBrowseOpciones = new DlgBrowseOpciones(frmInicio);
		componentes.put("dlgBrowseOpciones", dlgBrowseOpciones);

		DlgBrowseTabla dlgBrowseTabla = new DlgBrowseTabla(frmInicio);
		componentes.put("dlgBrowseTabla", dlgBrowseTabla);

		DlgReporteTabla dlgReporteTabla = new DlgReporteTabla(frmInicio);
		componentes.put("dlgReporteTabla", dlgReporteTabla);

		DlgTablaGeneralBrowse dlgTablaGeneralBrowse = new DlgTablaGeneralBrowse(frmInicio);
		componentes.put("dlgTablaGeneralBrowse", dlgTablaGeneralBrowse);

		DlgReporteOpciones dlgReporteOpciones = new DlgReporteOpciones(frmInicio);
		componentes.put("dlgReporteOpciones", dlgReporteOpciones);

		DlgComponente dlgComponente = new DlgComponente(frmInicio);
		componentes.put("dlgComponente", dlgComponente);

		DlgUsuario dlgUsuario = new DlgUsuario(frmInicio);
		componentes.put("dlgUsuario", dlgUsuario);

		DlgTablaGeneral dlgTablaGeneral = new DlgTablaGeneral(frmInicio);
		componentes.put("dlgTablaGeneral", dlgTablaGeneral);

		DlgControlComponente dlgControlComponente = new DlgControlComponente(frmInicio);
		componentes.put("dlgControlComponente", dlgControlComponente);

		DlgControlPartida dlgControlPartida = new DlgControlPartida(frmInicio);
		componentes.put("dlgControlPartida", dlgControlPartida);

		DlgCtrlAsistencia dlgCtrlAsistencia = new DlgCtrlAsistencia(frmInicio);
		componentes.put("dlgCtrlAsistencia", dlgCtrlAsistencia);

    DlgAsctrlhorario dlgAsctrlhorario = new DlgAsctrlhorario(frmInicio);
    componentes.put("dlgAsctrlhorario", dlgAsctrlhorario);

    DlgAsihorario dlgAsihorario = new DlgAsihorario(frmInicio);
		componentes.put("dlgAsihorario", dlgAsihorario);

		DlgAsistencia dlgAsistencia = new DlgAsistencia(frmInicio);
		componentes.put("dlgAsistencia", dlgAsistencia);

		DlgAsiasistencia dlgAsasistencia = new DlgAsiasistencia(frmInicio);
		componentes.put("dlgAsasistencia", dlgAsasistencia);

		DlgAsiplantillahorario dlgAsiplantillahorario = new DlgAsiplantillahorario(frmInicio);
		componentes.put("dlgAsiplantillahorario", dlgAsiplantillahorario);
		
		DlgAsiplantillaturno dlgAsiturno = new DlgAsiplantillaturno(frmInicio);
		componentes.put("dlgAsiturno", dlgAsiturno);

		DlgAsrepositorio dlgAsrepositorio = new DlgAsrepositorio(frmInicio);
		componentes.put("dlgAsrepositorio", dlgAsrepositorio);

		DlgIngresarAsistencia dlgIngresarAsistencia = new DlgIngresarAsistencia(frmInicio);
		componentes.put("dlgIngresarAsistencia", dlgIngresarAsistencia);

		CodigoBarras codigoBarras = new CodigoBarras();
		componentes.put("codigoBarras", codigoBarras);
		// ---------------------------------------------
		// VARIABLES PARA LA MANIPULACION DEL SISTEMA-
		// ---------------------------------------------
		Date varFecActReg = new Date(2011, 1, 1);
		componentes.put("varFecAct", varFecActReg);
		Time varHorActReg = new Time(0, 0, 0);
		componentes.put("varHorAct", varHorActReg);
		Util utilidad = new Util();
		componentes.put("utilidad", utilidad);
		return componentes;
	}

	public static Object getComponent(String parCompName) {

		return componentes.get(parCompName);
	}
}
