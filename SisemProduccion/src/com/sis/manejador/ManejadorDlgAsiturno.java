package com.sis.manejador;

import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asiplantillahorario;
import com.comun.entidad.Asiturno;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgAsiturno extends Manejador {

  public Map manejarEvento(Map peticion) throws Exception {

    aux = Auxiliar.getInstance();
    pagGen = PaginaGeneral.getInstance();
    empezarDBConnection(peticion);
    procesarEvento(peticion);
    terminarDBConnection(peticion);
    return peticion;
  }

  @Override
  public void iniciarFormulario(Map peticion) {

    try {
      if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)) {
        iniciarInstancias();
        limpiarInstancias();
      }
      this.setEstado(Estado.SUCCEEDED);
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
  }// fin de llimpiarInstancias

  @Override
  public void iniciarInstancias() {

    if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))) {
      aux.asiplanhora = (aux.asiplanhora == null) ? new Asiplantillahorario() : aux.asiplanhora;
      aux.asiturn = (aux.asiturn == null) ? new Asiturno() : aux.asiturn;
    }
  }

  @Override
  public void limpiarInstancias() {

    if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))) {
      Util.getInst().limpiarEntidad(aux.asiplanhora);
      Util.getInst().limpiarEntidad(aux.asiturn);
    }
  }

  public void cmdGuardarEvento(Map peticion) {

    try {
      Auxiliar aux = Auxiliar.getInstance();
      String tipoSql = "";
      if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO)) {
        tipoSql = TipoSql.INSERT;
      } else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)) {
        tipoSql = TipoSql.UPDATE;
      }
      if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)) {
        DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
        List listaDia = (List) peticion.get("listaDia");
        List listaIdentidad = (List) peticion.get("listaIdentidad");

        for (int iteDia = 0; iteDia < listaDia.size(); iteDia++) {
          String dia = (String) listaDia.get(iteDia);

          aux.asiturn.setKyusuario(aux.usu.getUsucodigo());
          aux.asiturn.setKyctrlasistencia(aux.asictrlasi.getKyctrlasistencia());
          aux.asiturn.setKyhorario(aux.asihora.getKyhorario());
          aux.asiturn.setNumerodni(aux.usu.getIdentidad());
          aux.asiturn.setPeriodo(aux.asictrlasi.getPeriodo());
          aux.asiturn.setHorarionombre(aux.asihora.getHorarionombre());


          peticion.put("Asiturno_kyturno", aux.asiturn.getKyturno());
          peticion.put("Asiturno_kyusuario", aux.asiplanhora.getKyplantillahorario());
          peticion.put("Asiturno_kyctrlusuario", aux.usu.getUsucodigo());
          peticion.put("Asiturno_dia", dia);
          peticion.put("tipoSql", tipoSql);

          agregarModificarAsiturno(peticion);

        }// for(int iteDia = 0; iteDia < listaDia.size(); iteDia++)
        if (listaDia == null || listaDia.size() == 0) {
          peticion.put("tipoSql", tipoSql);
          agregarModificarAsiturno(peticion);
        }
      }
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
  }

  public void agregarModificarAsiturno(Map peticion) {
    try {
      Util.getInst().setPeticionEntidad(peticion, aux.asiturn);
      String tipoSql = (String) peticion.get("tipoSql");
      DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

      String entrada = aux.asiturn.getEntrada().toString();
      String salida = aux.asiturn.getSalida().toString();

      String sql = "SELECT * " +
          "FROM asiturno " +
          "WHERE ( ( turnonombre = '" + aux.asiturn.getTurnonombre() + "' AND turnodia = '" + aux.asiturn.getTurnodia() + "' ) " +
          "	OR ( turnonombre = '" + aux.asiturn.getTurnonombre() + "' AND turnodia = '" + aux.asiturn.getTurnodia() + "' " +
          "		AND ( ( '" + entrada + "' >= entrada AND '" + salida + "' <= salida ) " +
          "			OR ( '" + entrada + "' <= entrada  AND ( '" + salida + "' >= entrada AND '" + salida + "' <= salida ) ) " +
          "			OR ( '" + salida + "' >= salida  AND ( '" + entrada + "' >= entrada AND '" + entrada + "' <= salida ) ) " +
          "			OR ( '" + salida + "' <= entrada AND '" + salida + "' >= salida ) " +
          "		) " +
          "	) " +
          ") AND asiturno.kyusuario = " + aux.usu.getUsucodigo() +
          " AND asiturngo.kyctrlasistencia=" + aux.asictrlasi.getKyctrlasistencia() +
          " AND asiturngo.kyhorario=" + aux.asihora.getKyhorario();

      if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)) {
        sql = sql + " AND kyturno <> " + aux.asiturn.getKyturno();
      }
      peticion.put("consulta", sql);
      peticion.put("entidadNombre", "Asiturno");
      Asiturno asiturn = (Asiturno) (new ObtenerEntidad()).ejecutarAccion(peticion);

      if (Util.getInst().isEmpty(asiturn.getKyturno()))
      {
        peticion.put("tipoSql", tipoSql);
        peticion.put("entidadObjeto", aux.asiturn);
        peticion.put("WHERE", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "WHERE kyturno=" + aux.asiturn.getKyturno()));
        afectados = dbc.ejecutarCommit(peticion);
        pagGen.pagAct.setModo(Modo.VISUALIZANDO);
        msgBox("Agregar o modificar turno", "Registro guardado satisfactoriamente!!!", "error");
      }
      else
      {
        msgBox("Agregar o modificar turno", "Existe un turno en este dia y de este tipo", "error");
      }
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
  }

  public void cmdBrowseSeleccionarEvento(Map peticion) {
    try {
      String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
      String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
      String paginaAccion = (String) peticion.get("Pagina_accion");
      Util.getInst().setPeticionEntidad(peticion, aux.asiplanhora);
      peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
      abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
    } catch (Exception e) {
      this.setEstado(Estado.FAILED);
      e.printStackTrace();
    }
  }

  public void cmdCerrarEvento(Map peticion) {

    cerrarFormulario(peticion);
  }
}
