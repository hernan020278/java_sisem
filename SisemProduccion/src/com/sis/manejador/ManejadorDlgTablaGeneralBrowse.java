package com.sis.manejador;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseObject;
import com.browse.accion.GenerarBrowse;
import com.browse.accion.GenerarBrowseOpciones;
import com.browse.accion.GenerateBrowseObject;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Cliente;
import com.comun.entidad.Componente;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Ficha;
import com.comun.entidad.Pedido;
import com.comun.entidad.Producto;
import com.comun.entidad.Usuario;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.Respuesta;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.pedido.accion.ObtenerPedido;
import com.producto.accion.ObtenerDetPedido;
import com.producto.accion.ObtenerProducto;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.ObtenerCliente;

public class ManejadorDlgTablaGeneralBrowse extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
//		aux.cmp = (aux.cmp == null) ? new Componente() : aux.cmp;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR) || pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
//			Util.getInst().limpiarEntidad(aux.cmp,true);
		}
		else
		{
//			Util.getInst().limpiarEntidad(aux.cmp);
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

				Util.getInst().setPeticionEntidad(peticion, aux.cmp);

				peticion.put("tipoSql", tipoSql);
				peticion.put("entidadObjeto", aux.cmp);
				peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where cmpcodigo='" + aux.cmp.getCmpcodigo() + "'"));
				afectados = dbc.ejecutarCommit(peticion);

				cerrarFormulario(peticion);
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdObtenerEntidadEvento(Map peticion)
	{
		procesarMetodo(peticion);
	}
	
	public void cmdObtenerPedidoPorModelo(Map peticion)
	{
		try
		{
			obtenerObjetosPedidoDeModelo(peticion);
			actualizaPagina(Modo.VISUALIZANDO);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	private void obtenerObjetosPedidoDeModelo(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion.put("Pedido_ped_cod", (String) peticion.get("Pagina_valor"));
			aux.ped = (Pedido)(new ObtenerPedido()).ejecutarAccion(peticion);
			
			peticion.put("consulta", "select * from cliente where cli_cod=" + aux.ped.getCli_cod());
			aux.cli = (Cliente) (new ObtenerCliente()).ejecutarAccion(peticion);
			

			peticion.put("consulta", "select producto.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where pedido.ped_cod=" + aux.ped.getPed_cod() + " and producto.prd_nivel=1");
			aux.mod = (Producto) (new ObtenerProducto()).ejecutarAccion(peticion);

			peticion.put("consulta", "select detpedido.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where pedido.ped_cod=" + aux.ped.getPed_cod() + " and producto.prd_nivel=1");
			aux.detPed = (DetPedido) (new ObtenerDetPedido()).ejecutarAccion(peticion);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	

	public void cmdEliminarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			Util.getInst().setPeticionEntidad(peticion, aux.msg);

			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			(new GenerarBrowse()).ejecutarAccion(peticion);

			actualizaPagina(Modo.ELIMINANDO);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	public void cmdActualizarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			Util.getInst().setPeticionEntidad(peticion, aux.msg);

			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			(new GenerarBrowse()).ejecutarAccion(peticion);

			actualizaPagina(Modo.VISUALIZANDO);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void tabCelActualizarComponentePorModeloEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			
			this.recuperarRegistroParaFicControlEvento(peticion);
			
			actualizaPagina(Modo.VISUALIZANDO);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}	
	public void cmdObtenerPedidoPorProducto(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			String productoPrdCod = (String) peticion.get("Pagina_valor");
			String sql = "select producto.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + productoPrdCod + " and producto.prd_nivel=2";
			
			peticion.put("consulta", sql);
			Producto producto = (Producto) (new ObtenerProducto()).ejecutarAccion(peticion);
			peticion.put("producto", producto);
			
			sql = "select producto.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + producto.getPrd_supuno() + " and producto.prd_nivel=1";

			peticion.put("consulta", sql);
			Producto modelo = (Producto) (new ObtenerProducto()).ejecutarAccion(peticion);
			peticion.put("modelo", modelo);
			
			sql = "select pedido.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + modelo.getPrd_cod() + "";

			peticion.put("consulta", sql);
			Pedido pedido = (Pedido)(new ObtenerPedido()).ejecutarAccion(peticion);
			peticion.put("pedido", pedido);
			
			peticion.put("consulta", "select * from cliente where cli_cod=" + pedido.getCli_cod());
			Cliente cliente = (Cliente) (new ObtenerCliente()).ejecutarAccion(peticion);
			peticion.put("cliente", cliente);

			iniciarFormulario(peticion);
			
			actualizaPagina(Modo.VISUALIZANDO);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}	
	
	public void cmdObtenerPedidoPorTalla(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			String productoPrdCod = (String) peticion.get("Pagina_valor");
			String sql = "select producto.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + productoPrdCod + " and producto.prd_nivel=3";
			
			peticion.put("consulta", sql);
			Producto talla = (Producto) (new ObtenerProducto()).ejecutarAccion(peticion);
			peticion.put("talla", talla);
			
			sql = "select producto.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + talla.getPrd_supdos() + " and producto.prd_nivel=1";

			peticion.put("consulta", sql);
			Producto producto = (Producto) (new ObtenerProducto()).ejecutarAccion(peticion);
			peticion.put("producto", producto);
			
			sql = "select producto.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + talla.getPrd_supuno() + " and producto.prd_nivel=1";

			peticion.put("consulta", sql);
			Producto modelo = (Producto) (new ObtenerProducto()).ejecutarAccion(peticion);
			peticion.put("modelo", modelo);
			
			sql = "select pedido.* from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_cod=" + modelo.getPrd_cod() + "";

			peticion.put("consulta", sql);
			Pedido pedido = (Pedido)(new ObtenerPedido()).ejecutarAccion(peticion);
			peticion.put("pedido", pedido);
			
			peticion.put("consulta", "select * from cliente where cli_cod=" + pedido.getCli_cod());
			Cliente cliente = (Cliente) (new ObtenerCliente()).ejecutarAccion(peticion);
			peticion.put("cliente", cliente);

			iniciarFormulario(peticion);
			
			actualizaPagina(Modo.VISUALIZANDO);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}	
	
	public void recuperarRegistrosDeBrowseTablaEvento(Map peticion)
	{
		try
		{
			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			(new GenerarBrowse()).ejecutarAccion(peticion);

			actualizaPagina(Acceso.ACTUALIZAR, Modo.VISUALIZANDO);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void recuperarRegistroParaFicControlEvento(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			int prdcodigo = (Integer) peticion.get("prdcodigo");
			int pedcodigo = (Integer) peticion.get("pedcodigo");
			
			String sql = "select pedido.ped_cod pedcodigo, producto.prd_supuno modcodigo, producto.prd_des talla " +
					"from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"where producto.prd_supuno=" + prdcodigo + " and pedido.ped_cod=" + pedcodigo + " and producto.prd_nivel=3 " +
					"group by pedido.ped_cod, prd_supuno, producto.prd_des " +
					"order by dbo.funGetFillZero(producto.prd_des,5) asc";			
			
			String[] listaColumnaTipo = {"INTEGER","INTEGER","STRING"};
			String[] listaColumnaAlias = {"pedcodigo","modcodigo","talla"};
			
			peticion.put("listaColumnaTipo", listaColumnaTipo);
			peticion.put("listaColumnaAlias", listaColumnaAlias);
			peticion.put("respuesta", Respuesta.LISTAMIXTA);
			peticion.put("consulta", sql);
			
			List listaTallas = dbc.ejecutarConsulta(peticion);
			peticion.put("listaTallas", listaTallas);

			sql = "select detcomponente.detcmpcodigo actualizar, componente.cmpcodigo cmpcodigo, componente.nombre componente " +
					"from pedido inner join detpedido on pedido.ped_cod=detpedido.ped_cod inner join detcomponente " +
					"on detpedido.ped_cod=detcomponente.pedcodigo " +
					"and detpedido.detped_cod=detcomponente.detpedcodigo " +
					"inner join producto on producto.prd_cod=detcomponente.prdcodigo " +
					"inner join componente on componente.cmpcodigo=detcomponente.cmpcodigo " +
					"where pedido.ped_cod=" + pedcodigo + " and producto.prd_cod=" + prdcodigo + " and detcomponente.estado<>'0000'";
			
			String[] listaColumnaTipo1 = {"BIGDECIMAL","BIGDECIMAL","STRING"};
			String[] listaColumnaAlias1 = {"actualizar","cmpcodigo","componente"};
			
			peticion.put("listaColumnaTipo", listaColumnaTipo1);
			peticion.put("listaColumnaAlias", listaColumnaAlias1);
			peticion.put("respuesta", Respuesta.LISTAMIXTA);
			peticion.put("consulta", sql);
			
			List listaComponentes = dbc.ejecutarConsulta(peticion);
			peticion.put("listaComponentes", listaComponentes);
			
			sql = "select componente.cmpcodigo componente, producto.prd_des prd_des, ficcontrol.tcmpesperado tcmpesperado from pedido inner join detpedido " +
					"on pedido.ped_cod=detpedido.ped_cod " +
					"inner join producto on producto.prd_cod=detpedido.prd_cod " +
					"inner join ficcontrol on detpedido.prd_cod=ficcontrol.prdcodigo and detpedido.ped_cod=ficcontrol.pedcodigo " +
					"and detpedido.detped_cod=ficcontrol.detpedcodigo " +
					"inner join componente on componente.cmpcodigo=ficcontrol.cmpcodigo " +
					"where detpedido.ped_cod=" + pedcodigo + " " + 
					"group by componente.cmpcodigo, detpedido.detped_cod, producto.prd_des,ficcontrol.tcmpesperado";			
			
			String[] listaColumnaTipo2 = {"BIGDECIMAL", "STRING","BIGDECIMAL"};
			String[] listaColumnaAlias2 = {"componente", "prd_des", "tcmpesperado"};
			
			peticion.put("listaColumnaTipo", listaColumnaTipo2);
			peticion.put("listaColumnaAlias", listaColumnaAlias2);
			peticion.put("respuesta", Respuesta.LISTAMIXTA);
			peticion.put("consulta", sql);
			
			List listaTallaComponente = dbc.ejecutarConsulta(peticion);
			
			peticion.put("listaTallaComponente", listaTallaComponente);
			
			peticion.remove("consulta");
			peticion.put("Pagina_valor", String.valueOf(pedcodigo));
			obtenerObjetosPedidoDeModelo(peticion);
			
			actualizaPagina(Modo.VISUALIZANDO);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	

	public void cmdAgregarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina(paginaHijo, paginaPadre, Modo.AGREGANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void treeValueChangedEvento(Map peticion)
	{
		try
		{
			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
			peticion.put("browseObject", browseObject);

			abrirPagina("DlgBrowseOpciones-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void menLlenarNodoEvento(Map peticion) {

		try
		{
			peticion.put("browseTabla", pagGen.pagAct.getBrowseTabla());
			(new GenerarBrowseOpciones()).ejecutarAccion(peticion);
			// BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
			// peticion.put("browseObject", browseObject);

			abrirPagina("DlgBrowseOpciones-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
