package com.sis.manejador;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Agrupacion;
import com.comun.entidad.Organizacion;
import com.comun.entidad.PerfilSeguridad;
import com.comun.entidad.Seguridad;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Dates;
import com.comun.utilidad.Log;
import com.comun.utilidad.PropiedadesExterna;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;

public class ManejadorSisemAsistenciaListener extends Manejador
{

	public Map manejarEvento(Map peticion) throws Exception
	{

		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{

		try
		{
			iniciarInstancias();
			limpiarInstancias();
			boolean validado = false;
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			PropiedadesExterna testProp = new PropiedadesExterna("config");
			String sistemaEmpresa = testProp.getServidor("sistema-empresa");
			String sql = "select * from organizacion where orgcodigo='" + sistemaEmpresa + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Organizacion");
			aux.org = (Organizacion) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if(!Util.isEmpty(aux.org.getOrgcodigo()))
			{
				String sistemaRestriccion = testProp.getServidor("sistema-restriccion");
				String fecha = KeyGenerator.getInst().desencriptar(sistemaRestriccion, Estado.CLAVE);
				if(!Util.isEmpty(fecha))
				{
					if(Util.getInst().isDateFormato(fecha, "dd/MM/yyy"))
					{
						Date fechaRestriccion = Dates.getDate(fecha, "dd/MM/yyyy");
						Date fechaInicio = new Date(fechaRestriccion.getYear(), fechaRestriccion.getMonth(), fechaRestriccion.getDate() - 20);
						if((Util.getInst().getFechaSql().compareTo(fechaInicio) == 1 || Util.getInst().getFechaSql().compareTo(fechaInicio) == 0)
								&& (Util.getInst().getFechaSql().compareTo(fechaRestriccion) == -1 || Util.getInst().getFechaSql().compareTo(fechaRestriccion) == 0))
						{
							validado = true;
						}
					}
				}
				
				if(validado)
				{
					setAccion("Iniciamos Formulario");
					iniciarInstancias();
					limpiarInstancias();
					String organizacionIde = "";
					List organizationList = OrganizacionGeneral.getInstance().getListaOrganizacion();
					for(int i = 0; i < organizationList.size(); i++)
					{
						Organizacion organizacion = (Organizacion) organizationList.get(i);
						organizacionIde = organizacion.getOrgcodigo();
						Log.info(this, "Loading properties for the OID: " + organizacionIde);
						PropiedadGeneral.getInstance(organizacionIde.toLowerCase());
					}
					abrirPagina("FrmSisem", "SisemListener", Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
					peticion.put("panelActivo", "PANEL_INICIO");
					peticion.put("consulta", "select * from organizacion where orgcodigo='" + organizacionIde + "'");
					peticion.put("entidadNombre", "Organizacion");
					this.setAccion("Obteniendo Organizacion...");
					aux.org = (Organizacion) (new ObtenerEntidad()).ejecutarAccion(peticion);
				}
				else
				{
					Util.getInst().ejecutarArchivoDesktop("cmd.exe /K java -Dsun.java2d.d3d=false -jar C:\\sisem\\bin\\lib\\" + "SisemControl.jar EliminarSistema");
					System.exit(0);
				}
			}// if(!Util.isEmpty(aux.org.getOrgcodigo()))
			else
			{
				sql = "select * from organizacion where orgcodigo<>'SISEM'";
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Organizacion");
				List listaEmpresas = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
				
				if(listaEmpresas != null && listaEmpresas.size() > 0)
				{
					Util.getInst().ejecutarArchivoDesktop("cmd.exe /K java -Dsun.java2d.d3d=false -jar C:\\sisem\\bin\\lib\\" + "SisemControl.jar EliminarSistema");
					System.exit(0);
				}
				else
				{
					abrirPagina("DlgRegistroOrganizacion", "SisemListener", Modo.VISUALIZANDO, "");
				}
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{

		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
		aux.org = (aux.org == null) ? new Organizacion() : aux.org;
		aux.seg = (aux.seg == null) ? new Seguridad() : aux.seg;
		aux.perfSeg = (aux.perfSeg == null) ? new PerfilSeguridad() : aux.perfSeg;
		aux.agru = (aux.agru == null) ? new Agrupacion() : aux.agru;
		aux.lisOrg = (aux.lisOrg == null) ? new ArrayList<Organizacion>() : aux.lisOrg;
		aux.lisSeg = (aux.lisSeg == null) ? new ArrayList<Seguridad>() : aux.lisSeg;
		aux.lisPerfSeg = (aux.lisPerfSeg == null) ? new ArrayList<PerfilSeguridad>() : aux.lisPerfSeg;
	}// fin de llimpiarInstancias

	@Override
	public void limpiarInstancias()
	{

		if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			Util.getInst().limpiarEntidad(aux.usuSes);
			Util.getInst().limpiarEntidad(aux.org);
			Util.getInst().limpiarEntidad(aux.seg);
			Util.getInst().limpiarEntidad(aux.perfSeg);
			Util.getInst().limpiarEntidad(aux.agru);
		}
		aux.lisOrg.clear();
		aux.lisSeg.clear();
		aux.lisPerfSeg.clear();
	}// fin de llimpiarInstancias

	public static void main(String[] args)
	{

		Map peticion = new HashMap();
		try {
			// browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
			// Organizacion org = (Organizacion) (new ObtenerOrganizacion()).ejecutarAccion(peticion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
