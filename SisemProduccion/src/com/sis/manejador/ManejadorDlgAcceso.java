package com.sis.manejador;

import java.util.Map;

import com.browse.BrowseObject;
import com.browse.accion.GenerateBrowseObject;
import com.comun.accion.ObtenerEntidad;
import com.comun.entidad.Organizacion;
import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgAcceso extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			setAccion("Iniciamos Formulario");
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.org = (aux.org == null) ? new Organizacion() : aux.org;
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			Util.getInst().limpiarEntidad(aux.usuSes);
		}
	}

	public void cmdIniciarEvento(Map peticion) {

		try
		{
			String usuario = (String) peticion.get("Usuario_usuario");
			String clave = (String) peticion.get("Usuario_clave");

			String sql = "select * from usuario where usuario='" + usuario.toUpperCase() + "' and clave='" + clave + "'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Usuario");
			this.setAccion("Obteniendo Usuario...");
			aux.usuSes = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);

			if (!Util.isEmpty(aux.usuSes.getNombre()))
			{
				abrirPagina("FrmInicio", "ContextListener", Modo.VISUALIZANDO, pagGen.pagAct.getBrowseTabla());
			}
			else
			{
				aux.msg.setMostrar(true);
				aux.msg.setTitulo("Error de Validacion");
				aux.msg.setTipo("error");
				aux.msg.setValor("Usuario no validado");
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdModificarEvento(Map peticion) {

		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				pagGen.pagAct.setModo(Modo.MODIFICANDO);
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO) || pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				pagGen.pagAct.setAcceso(Acceso.INICIAR);
				pagGen.pagAct.setModo(Modo.VISUALIZANDO);
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdBuscarEvento(Map peticion) {

		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
				peticion.put("browseObject", browseObject);

				pagGen.pagAct.setTipoConeccion("transaccion");
				pagGen.pagAct.setPaginaHijo("/browse/browse_form.jsp");
				pagGen.pagAct.setPaginaPadre("/especialista/especialista.jsp");
				pagGen.pagAct.setAcceso(Acceso.INICIAR);
				pagGen.pagAct.setModo(Modo.VISUALIZANDO);
				pagGen.pagAct.setEvento("iniciarFormulario");
				pagGen.pagAct.setAccion("");
				pagGen.pagAct.setNavegar(true);
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
