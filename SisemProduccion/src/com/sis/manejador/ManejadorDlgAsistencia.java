package com.sis.manejador;

import com.asistencia.accion.GestionarCtrlAsistencia;
import com.asistencia.accion.MarcarAsistencia;
import com.comun.entidad.*;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.pedido.accion.GenerarViewAsistencia;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.ObtenerUsuarioPorIdentidad;

import java.util.Map;

public class ManejadorDlgAsistencia extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			aux.usu = (aux.usu == null) ? new Usuario() : aux.usu;
			aux.asictrlasi = (aux.asictrlasi == null) ? new Asictrlasistencia() : aux.asictrlasi;
			aux.asihora = (aux.asihora == null) ? new Asihorario() : aux.asihora;
			aux.asiplanhora = (aux.asiplanhora == null) ? new Asiplantillahorario() : aux.asiplanhora;
			aux.asiturn = (aux.asiturn == null) ? new Asiturno() : aux.asiturn;
		}
	}

	@Override
	public void limpiarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.usu);
			Util.getInst().limpiarEntidad(aux.asictrlasi);
			Util.getInst().limpiarEntidad(aux.asihora);
			Util.getInst().limpiarEntidad(aux.asiplanhora);
			Util.getInst().limpiarEntidad(aux.asiturn);
		}
	}

	public void txtUsuario_identidadKeyReleasedEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			
			Usuario usu = (Usuario) (new ObtenerUsuarioPorIdentidad()).ejecutarAccion(peticion);
			
			if(!Util.getInst().isEmpty(usu.getUsucodigo().toString())){
				
				(new GestionarCtrlAsistencia()).ejecutarAccion(peticion);

				if (!Util.getInst().isEmpty(aux.asictrlasi.getKyctrlasistencia()))
				{
					
					(new MarcarAsistencia()).ejecutarAccion(peticion);
					
					(new GenerarViewAsistencia()).ejecutarAccion(peticion);
					peticion.put("sonido", "registro");
					
				}else {
					//msgBox("Asistencia", "No existe este usuario", "error");
					peticion.put("sonido", "alerta");
				}
			}else {
				//msgBox("Asistencia", "No existe este usuario", "error");
				peticion.put("sonido", "alerta");
			}
		} catch (Exception e){
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
	
}
