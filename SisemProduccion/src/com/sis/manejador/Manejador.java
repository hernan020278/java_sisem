/*
 * Created on Oct 17, 2003 
 */
package com.sis.manejador;

import java.lang.reflect.Method;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.comun.database.DBConeccion;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;
import com.reporte.accion.ObtenerReportePorModulo;
import com.sis.main.Pagina;
import com.sis.main.PaginaGeneral;

public abstract class Manejador implements ManejadorListener
{
	private int estado = Estado.READY;
	protected Auxiliar aux;
	protected PaginaGeneral pagGen;
	protected int afectados = 0;
	protected Map peticionAsincrono;
	protected String accion;

	public Map manejarEvento(Map peticion) throws Exception {
		return null;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	@Override
	public void procesarEvento(Map peticion)
	{
		Object result = null;
		Class[] argTypes = new Class[] { Map.class };

		String evento = pagGen.pagAct.getEvento();
		Util.getInst().limpiarEntidad(aux.msg,true);
		try
		{
//			for(int ite = 0; ite < 10; ite++)
//			{
//				System.out.println(" ite : " + ite);
//				Thread.sleep(500);
//			}
			Method method = this.getClass().getMethod(evento, argTypes);
			result = method.invoke(this, peticion);
			peticion = (Map) result;
			System.out.println("Pagina Actual: " + pagGen.pagAct.toString());
		}
		catch (Exception exception)
		{
			Log.error(this, "Error en el metodo : " + evento);
			exception.printStackTrace();
		}
	}

	public void procesarMetodo(Map peticion)
	{
		Object result = null;
		Class[] argTypes = new Class[] { Map.class };

		String metodo = (String)peticion.get("Pagina_accion");
		Util.getInst().limpiarEntidad(aux.msg,true);
		try
		{
			Method method = this.getClass().getMethod(metodo, argTypes);
			result = method.invoke(this, peticion);
			peticion = (Map) result;
		}
		catch (Exception exception)
		{
			Log.error(this, "Error en el metodo : " + metodo);
			exception.printStackTrace();
		}
	}
	
	@Override
	public boolean empezarDBConnection(Map peticion) {
		this.setAccion("Iniciamos coneccion con la base de datos");
		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
		boolean success = true;
		try
		{
			if (pagGen.pagAct.getTipoConeccion().equalsIgnoreCase("coneccion"))
			{
				dbc = new DBConeccion(pagGen.pagAct.getOrganizacionIde());
			}
			if (pagGen.pagAct.getTipoConeccion().equalsIgnoreCase("transaccion"))
			{
				if (dbc == null)
				{
					dbc = new DBConeccion(pagGen.pagAct.getOrganizacionIde());
				}
				else if (!dbc.isTransactionStarted())
				{
					dbc.start();
				}
			}
			if (!pagGen.pagAct.getTipoConeccion().equalsIgnoreCase("None") && (dbc.getEstado() == Estado.FAILED))
			{
				success = false;
			}

			peticion.put("dbConeccion", dbc);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.error(this, "Error occured starting database!");
		}
		return success;
	}

	@Override
	public DBConeccion terminarDBConnection(Map peticion) {

		this.setAccion("Terminamos coneccion con la base de datos...");
		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
		boolean closeConnection = false;

		if (closeConnection) {
			if (dbc != null)
			{
				dbc.setEstado(this.getEstado());
				dbc.end();
			}

			peticion.put("dbConeccion", dbc);

		}

		return dbc;
	}

	@Override
	public void cerrarFormulario(Map peticion) {
		try {

			if (pagGen.pagAct.getPaginaPadre().contains("DlgTablaGeneral")
			 || pagGen.pagAct.getPaginaPadre().contains("DlgControlPartida")
			 || pagGen.pagAct.getPaginaPadre().contains("DlgTablaGeneralBrowse"))
			{
				Pagina pagTmp = pagGen.obtenerPagina(pagGen.pagAct.getPaginaPadre());
				peticion.put("browseTabla", pagTmp.getBrowseTabla());
				(new GenerarBrowse()).ejecutarAccion(peticion);
				cerrarPagina(Acceso.ACTUALIZAR);
			}
			else if (pagGen.pagAct.getPaginaPadre().contains("DlgControlComponente"))
			{
				cerrarPagina(Acceso.ACTUALIZAR);
			}
			else if(pagGen.pagAct.getPaginaHijo().equals("FrmInicio"))
			{
				limpiarInstancias();
				cerrarPagina();
			}
			else if(pagGen.pagAct.getPaginaPadre().contains("DlgBrowseTabla"))
			{
				Pagina pagTmp = pagGen.obtenerPagina(pagGen.pagAct.getPaginaPadre());
				peticion.put("browseTabla", pagTmp.getBrowseTabla());
				(new GenerarBrowse()).ejecutarAccion(peticion);
				cerrarPagina();
			}
			else if(pagGen.pagAct.getPaginaPadre().equals("DlgReporteTabla"))
			{
				peticion.put("browseTabla", peticion.get("browseTabla"));
				peticion.put("reporteModulo", peticion.get("reporteModulo"));
				(new ObtenerReportePorModulo()).ejecutarAccion(peticion);
				cerrarPagina();
			}
			else
			{
				cerrarPagina(Acceso.ACTUALIZAR);
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void abrirFormulario(Map peticion) {
	}

	@Override
	public void setPeticionAsincrono(Map peticion) {

		this.peticionAsincrono = peticion;

	}

	@Override
	public Map getPeticionASincrono() {
		return this.peticionAsincrono;
	}

	public void setAccion(String accion)
	{
		this.accion = accion;
		Log.debug(this, accion);
		// try {
		// Thread.sleep(1000);
		// }
		// catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	public String getAccion()
	{
		return this.accion;
	}

	@Override
	public void etiBuscarEvento(Map peticion) {

	}

	public void cerrarPagina(String acceso)
	{
		try
		{
			pagGen.pagNew.setObjeto(pagGen.obtenerPagina(pagGen.pagAct.getPaginaPadre()));
			String browseTabla = pagGen.pagNew.getBrowseTabla();
//			pagGen.pagNew.setOrganizacionIde(pagGen.pagAct.getOrganizacionIde());
//			pagGen.pagNew.setTipoConeccion(pagGen.pagAct.getTipoConeccion());
//			pagGen.pagNew.setPaginaPadre(pagGen.pagAct.getPaginaPadre());
//			pagGen.pagNew.setPaginaHijo(pagGen.pagAct.getPaginaHijo());
//			pagGen.pagNew.setPeticion(pagGen.pagAct.getPeticion());
			pagGen.pagNew.setAcceso((Util.isEmpty(acceso)) ? Acceso.INICIAR : acceso);
//			pagGen.pagNew.setModo(Modo.VISUALIZANDO);
			pagGen.pagNew.setEvento("iniciarFormulario");
			pagGen.pagNew.setAccion("");
			pagGen.pagNew.setNavegar(true);
			pagGen.pagNew.setBrowseTabla(browseTabla);
			pagGen.eliminarPaginaMapa(pagGen.pagAct.getPaginaHijo());
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void cerrarPagina()
	{
		cerrarPagina("");
	}

	public void abrirPagina(String paginaHijo, String paginaPadre, String modo, String acceso, String browseTabla)
	{
//		pagGen.pagNew.setObjeto(pagGen.pagAct);
		pagGen.pagNew.setOrganizacionIde(pagGen.pagAct.getOrganizacionIde());
		pagGen.pagNew.setTipoConeccion(pagGen.pagAct.getTipoConeccion());
		pagGen.pagNew.setPaginaPadre(paginaPadre);
		pagGen.pagNew.setPaginaHijo(paginaHijo);
		pagGen.pagNew.setPeticion(pagGen.pagAct.getPeticion());
		pagGen.pagNew.setAcceso(acceso.equals("")? Acceso.INICIAR: acceso);
		pagGen.pagNew.setModo(modo);
		pagGen.pagNew.setEvento("iniciarFormulario");
		pagGen.pagNew.setAccion("");
		pagGen.pagNew.setNavegar(true);
		pagGen.pagNew.setBrowseTabla(browseTabla);
	}
	public void abrirPagina(String paginaHijo, String paginaPadre, String modo, String browseTabla)
	{
		abrirPagina(paginaHijo, paginaPadre, modo, "", browseTabla);
	}

	public void actualizaPagina(String acceso, String modo)
	{
		pagGen.pagAct.setOrganizacionIde(pagGen.pagAct.getOrganizacionIde());
		pagGen.pagAct.setTipoConeccion(pagGen.pagAct.getTipoConeccion());
		pagGen.pagAct.setPaginaPadre(pagGen.pagAct.getPaginaPadre());
		pagGen.pagAct.setPaginaHijo(pagGen.pagAct.getPaginaHijo());
		pagGen.pagAct.setPeticion(pagGen.pagAct.getPeticion());
		pagGen.pagAct.setAcceso(acceso);
		pagGen.pagAct.setModo(modo);
		pagGen.pagAct.setEvento("iniciarFormulario");
		pagGen.pagAct.setAccion("");
		pagGen.pagAct.setNavegar(false);
		pagGen.pagAct.setBrowseTabla(pagGen.pagAct.getBrowseTabla());

		pagGen.establecerPaginaCache(pagGen.pagAct);
	}
	public void actualizaPagina(String modo)
	{
		actualizaPagina(pagGen.pagAct.getAcceso(),modo);
		
//		pagGen.pagAct.setOrganizacionIde(pagGen.pagAct.getOrganizacionIde());
//		pagGen.pagAct.setTipoConeccion(pagGen.pagAct.getTipoConeccion());
//		pagGen.pagAct.setPaginaPadre(pagGen.pagAct.getPaginaPadre());
//		pagGen.pagAct.setPaginaHijo(pagGen.pagAct.getPaginaHijo());
//		pagGen.pagAct.setPeticion(pagGen.pagAct.getPeticion());
//		pagGen.pagAct.setAcceso(Acceso.ACTUALIZAR);
//		pagGen.pagAct.setModo(modo);
//		pagGen.pagAct.setEvento("iniciarFormulario");
//		pagGen.pagAct.setAccion("");
//		pagGen.pagAct.setNavegar(false);
//		pagGen.pagAct.setBrowseTabla(pagGen.pagAct.getBrowseTabla());
//
//		pagGen.establecerPaginaCache(pagGen.pagAct);
	}
	public void msgBox(String titulo, String valor, String tipo)
	{
		Auxiliar aux = Auxiliar.getInstance();
		aux.msg.setMostrar(true);
		aux.msg.setTipo(tipo);
		aux.msg.setTitulo(titulo);
		aux.msg.setValor(valor);
		
	}	
}
