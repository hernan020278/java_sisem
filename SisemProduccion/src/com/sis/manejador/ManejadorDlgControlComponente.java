package com.sis.manejador;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.accion.GenerarBrowse;
import com.componente.accion.ActualizarTotalContabilizadoFicControl;
import com.componente.accion.ActualizarUsuarioEnFicControl;
import com.componente.accion.ActualizarUsuarioEnProduccion;
import com.componente.accion.DuplicarNuevaFicControl;
import com.componente.accion.EliminarComponentePorFichaSinProduccion;
import com.componente.accion.ObtenerPadresDeFicControl;
import com.componente.accion.SeleccionarComponentePorProduccion;
import com.componente.accion.TotalFicControlCanContabilizado;
import com.componente.accion.TotalFicControlTcmpContabilizado;
import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.DetComponente;
import com.comun.entidad.DetPedido;
import com.comun.entidad.FicControl;
import com.comun.entidad.Ficha;
import com.comun.entidad.Pedido;
import com.comun.entidad.Produccion;
import com.comun.entidad.Producto;
import com.comun.entidad.Usuario;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.CrearProduccion;
import com.usuario.accion.ObtenerUsuarioPorIdentidad;

public class ManejadorDlgControlComponente extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
			{
				iniciarInstancias();
				limpiarInstancias();
				generarRegistroProduccionPeriodoActual(peticion);
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.ped = (aux.ped == null) ? new Pedido() : aux.ped;
		aux.detPed = (aux.detPed == null) ? new DetPedido() : aux.detPed;
		aux.mod = (aux.mod == null) ? new Producto() : aux.mod;
		aux.prd = (aux.prd == null) ? new Producto() : aux.prd;
		aux.tal = (aux.tal == null) ? new Producto() : aux.tal;
		aux.fic = (aux.fic == null) ? new Ficha() : aux.fic;
		aux.ficCtrl = (aux.ficCtrl == null) ? new FicControl() : aux.ficCtrl;
		aux.detCmp = (aux.detCmp == null) ? new DetComponente() : aux.detCmp;
		aux.ope = (aux.ope == null) ? new Usuario() : aux.ope;
		aux.prc = (aux.prc == null) ? new Produccion() : aux.prc;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			Util.getInst().limpiarEntidad(aux.ped);
			Util.getInst().limpiarEntidad(aux.detPed);
			Util.getInst().limpiarEntidad(aux.mod);
			Util.getInst().limpiarEntidad(aux.prd);
			Util.getInst().limpiarEntidad(aux.tal);
			Util.getInst().limpiarEntidad(aux.fic);
			Util.getInst().limpiarEntidad(aux.ficCtrl);
			Util.getInst().limpiarEntidad(aux.detCmp);
			Util.getInst().limpiarEntidad(aux.ope);
			Util.getInst().limpiarEntidad(aux.prc);
		}
	}

	public void forzarLimpiarInstancias()
	{
		Util.getInst().limpiarEntidad(aux.ped, true);
		Util.getInst().limpiarEntidad(aux.detPed, true);
		Util.getInst().limpiarEntidad(aux.mod, true);
		Util.getInst().limpiarEntidad(aux.prd, true);
		Util.getInst().limpiarEntidad(aux.tal, true);
		Util.getInst().limpiarEntidad(aux.fic, true);
		Util.getInst().limpiarEntidad(aux.ficCtrl, true);
		Util.getInst().limpiarEntidad(aux.detCmp, true);
		Util.getInst().limpiarEntidad(aux.ope, true);
		Util.getInst().limpiarEntidad(aux.prc, true);
	}
	
	
	public void txtFicControlIdentidadKeyReleasedEvento(Map peticion)
	{
		try {

			String ficControlIdentidad = (String) peticion.get("FicControl_identidad");
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			BigDecimal newBigDecimal = new BigDecimal(0);
			if(Util.isEmpty(aux.prc.getPrccodigo().toString()))
			{
				obtenerRegistroProduccion(peticion);
			}
			/*
			 * BUSCAR REGISTRO DE FICHA DE CONTROL SI NO EXISTE SE CREA UNO EN RELACION AL DIA
			 */
			String sql = "select * from ficcontrol where identidad='" + ficControlIdentidad + "' and " +
					"usucodigo=" + aux.ope.getUsucodigo() + " and prccodigo= " + aux.prc.getPrccodigo() + " and estado<>'0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "FicControl");
			aux.ficCtrl = (FicControl) (new ObtenerEntidad()).ejecutarAccion(peticion);

//			if(!Util.isEmpty(aux.ficCtrl.getIdentidad()))
//			{
//					aux.ficCtrl.setPrccodigo(aux.prc.getPrccodigo());
//					aux.ficCtrl.setUsucodigo(aux.ope.getUsucodigo());
//					aux.ficCtrl.setEstado(Estado.PRODUCCION);
//					
//					peticion.put("tipoSql", TipoSql.UPDATE);
//					peticion.put("entidadObjeto", aux.ficCtrl);
//					peticion.put("where", "where ficcodigo=" + aux.ficCtrl.getFiccodigo());
//					afectados = dbc.ejecutarCommit(peticion);
//					
//					(new ObtenerPadresDeFicControl()).ejecutarAccion(peticion);
////					(new ActualizarUsuarioEnFicControl()).ejecutarAccion(peticion);
//			}
//			else
			if(Util.isEmpty(aux.ficCtrl.getIdentidad()))
			{
				sql = "select * from ficcontrol where identidad='" + ficControlIdentidad + "' and " +
						"usucodigo=0 and prccodigo=0 and estado<>'0000'";
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "FicControl");
				aux.ficCtrl = (FicControl) (new ObtenerEntidad()).ejecutarAccion(peticion);
				
				if(!Util.isEmpty(aux.ficCtrl.getIdentidad()))
				{
					BigDecimal totalCanContabilizado= (BigDecimal )(new TotalFicControlCanContabilizado()).ejecutarAccion(peticion);
					BigDecimal totalTcmpContabilizado= (BigDecimal )(new TotalFicControlTcmpContabilizado()).ejecutarAccion(peticion);
					if(totalCanContabilizado.compareTo(newBigDecimal) > 0)
					{
						aux.ficCtrl.setCanrealizado(newBigDecimal);
						aux.ficCtrl.setCancontabilizado(totalCanContabilizado);
						aux.ficCtrl.setTcmprealizado(newBigDecimal);
						aux.ficCtrl.setTcmpcontabilizado(totalTcmpContabilizado);
					}
					else
					{
						aux.ficCtrl.setCanrealizado(aux.ficCtrl.getCanpedido());
						aux.ficCtrl.setCancontabilizado(aux.ficCtrl.getCanpedido());
						aux.ficCtrl.setTcmprealizado(aux.ficCtrl.getTcmpesperado().multiply(aux.ficCtrl.getCanrealizado()));
						aux.ficCtrl.setTcmpcontabilizado(aux.ficCtrl.getCanpedido().multiply(aux.ficCtrl.getTcmpesperado()));
					}
					
					peticion.put("tipoSql", TipoSql.UPDATE);
					peticion.put("entidadObjeto", aux.ficCtrl);
					peticion.put("where", "where ficcodigo=" + aux.ficCtrl.getFiccodigo());
					afectados = dbc.ejecutarCommit(peticion);
					
					(new ActualizarTotalContabilizadoFicControl()).ejecutarAccion(peticion);
					
					aux.ficCtrl.setPrccodigo(aux.prc.getPrccodigo());
					aux.ficCtrl.setUsucodigo(aux.ope.getUsucodigo());
					aux.ficCtrl.setFecingreso(aux.prc.getFecregistro());
					aux.ficCtrl.setFecsalida(aux.prc.getFecelaboracion());
					
					peticion.put("tipoSql", TipoSql.UPDATE);
					peticion.put("entidadObjeto", aux.ficCtrl);
					peticion.put("where", "where ficcodigo=" + aux.ficCtrl.getFiccodigo());
					afectados = dbc.ejecutarCommit(peticion);
					
					(new DuplicarNuevaFicControl()).ejecutarAccion(peticion);
					(new ObtenerPadresDeFicControl()).ejecutarAccion(peticion);
//					(new ActualizarUsuarioEnFicControl()).ejecutarAccion(peticion);
				}//if(Util.isEmpty(aux.ficCtrl.getIdentidad()))
				else
				{
					sql = "select * from ficcontrol where identidad='" + ficControlIdentidad + "' and " +
							"usucodigo<>0 and prccodigo<>0 and estado<>'0000'";
					peticion.put("consulta", sql);
					peticion.put("entidadNombre", "FicControl");
					aux.ficCtrl = (FicControl) (new ObtenerEntidad()).ejecutarAccion(peticion);
					
					if(!Util.isEmpty(aux.ficCtrl.getIdentidad()))
					{
						(new DuplicarNuevaFicControl()).ejecutarAccion(peticion);
					}
					else
					{
						sql = "select * from ficcontrol where cmpcodigo in " +
								"(select cmpcodigo from componente where identidad='" + ficControlIdentidad + "') " +
								"and ficcontrol.usucodigo="+aux.ope.getUsucodigo()+ " and ficcontrol.prccodigo=" + aux.prc.getPrccodigo();
						
						peticion.put("consulta", sql);
						peticion.put("entidadNombre", "FicControl");
						aux.ficCtrl = (FicControl) (new ObtenerEntidad()).ejecutarAccion(peticion);
						if(Util.isEmpty(aux.ficCtrl.getFiccodigo().toString()))
						{
							peticion.put("Pagina_valor", ficControlIdentidad);
							(new SeleccionarComponentePorProduccion()).ejecutarAccion(peticion);
						}
						if(!Util.isEmpty(aux.ficCtrl.getFiccodigo().toString()) && aux.ficCtrl.getPrccodigo().compareTo(aux.prc.getPrccodigo())==0 && aux.ficCtrl.getUsucodigo().compareTo(aux.ope.getUsucodigo()) == 0)
						{
							(new ObtenerPadresDeFicControl()).ejecutarAccion(peticion);
						}
					}
				}
			}
			if(!Util.isEmpty(aux.prc.getPrccodigo().toString())
					&& !Util.isEmpty(aux.ope.getUsucodigo().toString())
					&& aux.prc.getUsucodigo().compareTo(aux.ope.getUsucodigo()) == 0)
			{
				(new ActualizarUsuarioEnProduccion()).ejecutarAccion(peticion);
				
				peticion.put("browseTabla", "lis-cmp-por-fic");
				
				List listaFiltro = new ArrayList();
				listaFiltro.add(Util.getInst().crearRegistroFiltro("ficcontrol.prccodigo", "BIGDECIMAL", "=", aux.prc.getPrccodigo().toString(),"",""));
				Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

				(new GenerarBrowse()).ejecutarAccion(peticion);
			}			
			
			actualizaPagina(Acceso.ACTUALIZAR, pagGen.pagAct.getModo());
			this.setEstado(Estado.SUCCEEDED);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	private void obtenerRegistroProduccion(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			/*
			 * BUSCAR REGISTRO DE PRODUCCION
			 */
			String codigoFecha = String.valueOf(String.valueOf(Util.getInst().getFechaSql().getDate() + String.valueOf(Util.getInst().getFechaSql().getMonth() + 1) + String.valueOf(Util.getInst().getFechaSql().getYear()+1900)));
			if(peticion.containsKey("Produccion_fecelaboracion"))
			{
				Date prdFecelab = (Date) peticion.get("Produccion_fecelaboracion");
				codigoFecha = String.valueOf(String.valueOf(prdFecelab.getDate() + String.valueOf(prdFecelab.getMonth() + 1) + String.valueOf(prdFecelab.getYear()+1900)));
			}
			String sql = "select * from produccion where identidad='" + codigoFecha + "' and usucodigo=" + aux.ope.getUsucodigo() + " and operario='" + aux.ope.getNombre() + "' and estado<>'0000'";

			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Produccion");
			aux.prc = (Produccion) (new ObtenerEntidad()).ejecutarAccion(peticion);

			/**
			 * Eliminamos los registros de control de produccion duplicados
			 */
			sql = "select * from produccion where identidad='" + codigoFecha + "' and usucodigo=" + aux.ope.getUsucodigo() + " and estado<>'0000'";
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Produccion");
			List listaProduccion = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
			if(listaProduccion != null && listaProduccion.size() > 1)
			{
				for(int itePrc = 0; itePrc < listaProduccion.size(); itePrc++)
				{
					Produccion produccion = (Produccion) listaProduccion.get(itePrc);
					peticion.put("tipoSql", TipoSql.DELETE);
					peticion.put("entidadNombre", "Produccion");
					peticion.put("entidadNombre", "where prccodigo = " + produccion.getPrccodigo());
					dbc.ejecutarCommit(peticion);
					
					peticion.put("FicControl_ficCodigo", aux.ficCtrl.getFiccodigo());
					peticion.put("FicControl_identidad", aux.ficCtrl.getIdentidad());
					(new EliminarComponentePorFichaSinProduccion()).ejecutarAccion(peticion);
				}//for(int itePrc = 0; itePrc < listaProduccion.size(); itePrc++)
			}
			
			if(Util.isEmpty(aux.prc.getIdentidad()))
			{
				(new CrearProduccion()).ejecutarAccion(peticion);
				if(!Util.isEmpty(aux.prc.getUsucodigo().toString())){
					peticion.put("tipoSql", TipoSql.INSERT);
					peticion.put("entidadObjeto", aux.prc);
					afectados = dbc.ejecutarCommit(peticion);
				}
			}// if(!Util.isEmpty(aux.prc.getIdentidad()))
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	private void generarRegistroProduccionPeriodoActual(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			/*
			 * BUSCAR REGISTRO DE PRODUCCION
			 */
			Date fecha = new Date(Util.getInst().getFechaSql().getYear(), Util.getInst().getFechaSql().getMonth(),Util.getInst().getFechaSql().getDate());
			int numDias = Util.getInst().obtenerDiasDelMes(fecha.getMonth()+1, fecha.getYear()+1900);
			for(int iteDia = 1; iteDia <= numDias; iteDia++)
			{
				fecha.setDate(iteDia);
				String codigoFecha = String.valueOf(String.valueOf(fecha.getDate() + String.valueOf(fecha.getMonth() + 1) + String.valueOf(fecha.getYear()+1900)));
				String sql = "select * from produccion where identidad='" + codigoFecha + "' and estado<>'0000'";
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Produccion");
				aux.prc = (Produccion) (new ObtenerEntidad()).ejecutarAccion(peticion);

				if(!Util.isEmpty(aux.ope.getUsucodigo().toString()) && Util.isEmpty(aux.prc.getIdentidad()))
				{
					peticion.put("Produccion_fecelaboracion", fecha);
					(new CrearProduccion()).ejecutarAccion(peticion);
					peticion.put("tipoSql", TipoSql.INSERT);
					peticion.put("entidadObjeto", aux.prc);
					afectados = dbc.ejecutarCommit(peticion);
				}// if(!Util.isEmpty(aux.prc.getIdentidad()))
				else
				{
					break;
				}
			}

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdSeleccionarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void txtOperarioIdentidadKeyReleasedEvento(Map peticion)
	{
		try
		{
			Util.getInst().limpiarEntidad(aux.ped, true);
			Util.getInst().limpiarEntidad(aux.detPed, true);
			Util.getInst().limpiarEntidad(aux.mod, true);
			Util.getInst().limpiarEntidad(aux.prd, true);
			Util.getInst().limpiarEntidad(aux.tal, true);
			Util.getInst().limpiarEntidad(aux.fic, true);
			Util.getInst().limpiarEntidad(aux.ficCtrl, true);
			Util.getInst().limpiarEntidad(aux.detCmp, true);
			Util.getInst().limpiarEntidad(aux.ope, true);
			Util.getInst().limpiarEntidad(aux.prc, true);
			
			aux.ope = (Usuario)(new ObtenerUsuarioPorIdentidad()).ejecutarAccion(peticion);
			obtenerRegistroProduccion(peticion);
			
			(new ActualizarUsuarioEnProduccion()).ejecutarAccion(peticion);
			(new ActualizarUsuarioEnFicControl()).ejecutarAccion(peticion);
			
			peticion.put("browseTabla", "lis-cmp-por-fic");
			
			List listaFiltro = new ArrayList();
			listaFiltro.add(Util.getInst().crearRegistroFiltro("ficcontrol.prccodigo", "BIGDECIMAL", "=", aux.prc.getPrccodigo().toString(),"",""));
			Util.getInst().obtenerPeticionFiltrosBusqueda(peticion, listaFiltro);

			(new GenerarBrowse()).ejecutarAccion(peticion);
			
			actualizaPagina(Acceso.ACTUALIZAR, pagGen.pagAct.getModo());
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdGuardarEvento(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			Util.getInst().setPeticionEntidad(peticion, aux.prc);
			
			peticion.put("tipoSql", TipoSql.UPDATE);
			peticion.put("entidadObjeto", aux.prc);
			peticion.put("where", "where prccodigo=" + aux.prc.getPrccodigo());
			afectados = dbc.ejecutarCommit(peticion);
			actualizaPagina(Acceso.ACTUALIZAR, pagGen.pagAct.getModo());
			cerrarFormulario(peticion);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdReiniciarFichaControlEvento(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			String sql = "delete from ficcontrol where estado <> '"  + Estado.PENDIENTE +  "' and ficcontrol.ficsupuno=" + aux.fic.getFic_bar();
			peticion.put("tipoSql", TipoSql.BATCH);
			peticion.put("consulta", sql);
			dbc.ejecutarCommit(peticion);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdActualizarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			actualizaPagina(Acceso.ACTUALIZAR,pagGen.pagAct.getModo());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	public void cmdEliminarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			actualizaPagina(Acceso.ACTUALIZAR,pagGen.pagAct.getModo());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
