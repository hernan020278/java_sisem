package com.sis.manejador;

import java.util.Map;

import com.browse.BrowseObject;
import com.browse.accion.GenerateBrowseObject;
import com.comun.database.DBConeccion;
import com.comun.entidad.Organizacion;
import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.PropiedadesExterna;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.CrearOrganizacion;
import com.usuario.accion.CrearUsuario;

public class ManejadorDlgRegistroOrganizacion extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			setAccion("Iniciamos Formulario");
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.org = (aux.org == null) ? new Organizacion() : aux.org;
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			Util.getInst().limpiarEntidad(aux.usuSes);
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			(new CrearOrganizacion()).ejecutarAccion(peticion);
			peticion.put("tipoSql", TipoSql.INSERT);
			peticion.put("entidadObjeto", aux.org);
			afectados = dbc.ejecutarCommit(peticion);
			
			if(afectados > 0)
			{
				aux.usu = (Usuario)(new CrearUsuario()).ejecutarAccion(peticion);
				aux.usu.setOrgcodigo(aux.org.getOrgcodigo());
				peticion.put("tipoSql", TipoSql.INSERT);
				peticion.put("entidadObjeto", aux.usu);
				afectados = dbc.ejecutarCommit(peticion);

				PropiedadesExterna testProp = new PropiedadesExterna("config");
				String fecha = Util.getInst().fromDateToString(aux.org.getFechafinal(), "dd/MM/yyyy");
                testProp.setPropiedades("sistema-restriccion", KeyGenerator.getInst().encriptar(fecha, "020278"));
                testProp.setPropiedades("sistema-empresa", aux.org.getOrgcodigo());
                cerrarFormulario(peticion);
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdModificarEvento(Map peticion) {

		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				pagGen.pagAct.setModo(Modo.MODIFICANDO);
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO) || pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				pagGen.pagAct.setAcceso(Acceso.INICIAR);
				pagGen.pagAct.setModo(Modo.VISUALIZANDO);
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdBuscarEvento(Map peticion) {

		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				BrowseObject browseObject = (BrowseObject) (new GenerateBrowseObject()).ejecutarAccion(peticion);
				peticion.put("browseObject", browseObject);

				pagGen.pagAct.setTipoConeccion("transaccion");
				pagGen.pagAct.setPaginaHijo("/browse/browse_form.jsp");
				pagGen.pagAct.setPaginaPadre("/especialista/especialista.jsp");
				pagGen.pagAct.setAcceso(Acceso.INICIAR);
				pagGen.pagAct.setModo(Modo.VISUALIZANDO);
				pagGen.pagAct.setEvento("iniciarFormulario");
				pagGen.pagAct.setAccion("");
				pagGen.pagAct.setNavegar(true);
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
}
