package com.sis.manejador;

import com.asis.accion.EvaluarAsistenciaTurnos;
import com.asistencia.accion.*;
import com.browse.accion.EjecutarEntidadReporte;
import com.browse.accion.GenerarBrowse;
import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Area;
import com.comun.entidad.Asictrlasistencia;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.motor.AccionListener;
import com.comun.motor.IAccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.pedido.accion.GenerarBrowseAsistencia;
import com.pedido.accion.GenerarBrowseAsrepositorio;
import com.pedido.accion.GenerarBrowseTotalHoraDeHorario;
import com.pedido.accion.GenerarBrowseTurno;
import com.pedido.accion.impl.GenerarBrowseAsihorario;
import com.reporte.datasource.EntityDataSource;
import com.reporte.datasource.HibernateQueryResultDataSource;
import com.sis.main.PaginaGeneral;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Map;


public class ManejadorDlgCtrlAsistencia extends Manejador
{
	private GenerarBrowseAsihorario generarBrowseAsihorario = new GenerarBrowseAsihorario();
	private GenerarBrowseAsistencia generarBrowseAsistencia = new GenerarBrowseAsistencia();
	private GenerarBrowseTotalHoraDeHorario generarBrowseTotalHoraDeHorario = new GenerarBrowseTotalHoraDeHorario();
	private GenerarBrowse generarBrowse = new GenerarBrowse();

	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{

		Mono.just(peticion)
			.filter(filIf -> false)
			.switchIfEmpty(Mono.just(peticion))
			.filter(fil -> pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			.flatMap(generarBrowseAsihorario::ejecutarAccion)
			.flatMap(generarBrowse::ejecutarAccion)
			.map(request -> {

				request.put("browseAsihorario", request.get("browse"));
				request.put("browseObjectAsihorario", request.get("browseObject"));
				request.put("browseListAsihorario", request.get("browseList"));

				return request;
			})
			.flatMap(generarBrowseAsistencia::ejecutarAccion)
			.flatMap(generarBrowseTotalHoraDeHorario::ejecutarAccion)
			.doOnError(System.out::println)
			.subscribe();
/*
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
				iniciarInstancias();
				limpiarInstancias();
				
				(new GenerarBrowseAsihorario()).ejecutarAccion(peticion);
				(new GenerarBrowseAsistencia()).ejecutarAccion(peticion);
				(new GenerarBrowseTotalHoraDeHorario()).ejecutarAccion(peticion);
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}*/
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.are = (aux.are == null) ? new Area() : aux.are;
		aux.subAre = (aux.subAre == null) ? new SubArea() : aux.subAre;
		aux.usu = (aux.usu == null) ? new Usuario() : aux.usu;
	}

	@Override
	public void limpiarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.are);
			Util.getInst().limpiarEntidad(aux.subAre);
			Util.getInst().limpiarEntidad(aux.usu);
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

				Util.getInst().setPeticionEntidad(peticion, aux.asictrlasi);

				peticion.put("tipoSql", tipoSql);
				peticion.put("entidadObjeto", aux.asictrlasi);
				afectados = dbc.ejecutarCommit(peticion);

				iniciarFormulario(peticion);
			}
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarCtrlhorarioEvento(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			String usucodigo = (String)peticion.get("Usuario_usucodigo");
			String sql = "select * from usuario where usucodigo = " + usucodigo;
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Usuario");
			Usuario usuario = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if (!Util.isEmpty(usuario.getIdentidad()))
			{
				aux.usu = usuario;
				
				String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
				String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
				String paginaAccion = (String) peticion.get("Pagina_accion");
				
				peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

				abrirPagina("DlgAsctrlhorario", pagGen.pagAct.getPaginaHijo(), Modo.AGREGANDO, (String) peticion.get("browseTabla"));
			}
			else
			{
				(new GenerarBrowseTurno()).ejecutarAccion(peticion);
				msgBox("Validacion", "Guarde un horario para agregar turno", "error");
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdBuscarCtrlasistenciaEvento(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			String identidad = (String)peticion.get("Usuario_identidad");
//			String periodo = (String)peticion.get("Asictrlasistencia_periodo");
			Date fecact = (Date)peticion.get("fechaActual");
			String sql = "select asictrlasistencia.* from usuario inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " +
			"where asictrlasistencia.fecinicio <= '" + fecact + "' and '" + fecact + "' <= asictrlasistencia.fecfinal " +
			"and usuario.identidad = '" + identidad + "'";

			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asictrlasistencia");
			Asictrlasistencia asictrlasistencia = (Asictrlasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if (Util.getInst().isEmpty(asictrlasistencia.getKyctrlasistencia()))
			{
				(new AgregarAsictrlasistencia()).ejecutarAccion(peticion);
				
				(new AgregarAsihorario()).ejecutarAccion(peticion);
				
				(new AgregarAsihorario()).ejecutarAccion(peticion);
			}
			else
			{
				aux.asictrlasi = asictrlasistencia;
			}
			iniciarFormulario(peticion);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarAsctrlhorarioEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina(paginaHijo, paginaPadre, Modo.AGREGANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdEliminarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			
			int afectados =(peticion.containsKey("afectados")) ? (Integer) peticion.get("afectados") : 0;
//			if(afectados > 0){msgBox("Eliminacion","Registro Eliminado","info");}
//			else{msgBox("Eliminacion","Error de eliminacion","error");}
			
			iniciarFormulario(peticion);
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdModificarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			
			abrirPagina(paginaHijo, paginaPadre, Modo.MODIFICANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdModificarRepositorioEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			
			abrirPagina(paginaHijo, paginaPadre, Modo.MODIFICANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarAsihorarioEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina(paginaHijo, paginaPadre, Modo.AGREGANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarAsasistenciaEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina(paginaHijo, paginaPadre, Modo.AGREGANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdSeleccionarAsistenciaEvento(Map peticion)
	{
		try
		{
			String asicodigo = (String) peticion.get("Pagina_valor");
			peticion.put("Asasistencia_asicodigo", asicodigo);
			(new GenerarBrowseAsrepositorio()).ejecutarAccion(peticion);
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdCalcularEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			
			Util.getInst().limpiarEntidad(aux.asiasis,true);
			Util.getInst().limpiarEntidad(aux.asirepo,true);
			
			BigDecimal totalLim1 = (BigDecimal)(new TotalLim1()).ejecutarAccion(peticion);
			BigDecimal totalLim2 = (BigDecimal)(new TotalLim2()).ejecutarAccion(peticion);
			BigDecimal totalLim3 = (BigDecimal)(new TotalLim3()).ejecutarAccion(peticion);
			BigDecimal totalLim4 = (BigDecimal)(new TotalLim4()).ejecutarAccion(peticion);
			BigDecimal totalInasistencia = (BigDecimal)(new TotalInasistencia()).ejecutarAccion(peticion);
			BigDecimal totalExtraUno = (BigDecimal)(new TotalExtraUno()).ejecutarAccion(peticion);
			BigDecimal totalExtraDos = (BigDecimal)(new TotalExtraDos()).ejecutarAccion(peticion);
			BigDecimal totalPapeleta = (BigDecimal) (new TotalHoraPapeleta()).ejecutarAccion(peticion);
			
			peticion.put("Asihorario_tipo", "");
			peticion.put("Asihorario_modo", "PERIODO");
			peticion.put("Asihorario_refturno", "TODOS");
			peticion.put("Asihorario_fechainicio", aux.asictrlasi.getFechainicio());
			peticion.put("Asihorario_fecejefinal", aux.asictrlasi.getFechafinal());

			BigDecimal totalHoraObl = (BigDecimal) (new TotalHorasPeriodo()).ejecutarAccion(peticion);;
			BigDecimal totalHoraTrab = (BigDecimal) (new TotalHoraTrabajadaPeriodo()).ejecutarAccion(peticion);;
			BigDecimal totalHoraNoTrab = (BigDecimal) (new TotalHoraNoTrabajadaPeriodo()).ejecutarAccion(peticion);;
			
			aux.asictrlasi.setHoraobligatoria(totalHoraObl);
			aux.asictrlasi.setHoratrabajada(totalHoraTrab);
			aux.asictrlasi.setHoranotrabajada(totalHoraNoTrab);
			aux.asictrlasi.setHorainasistencia(totalInasistencia);
			aux.asictrlasi.setHoraextrauno(totalExtraUno);
			aux.asictrlasi.setHoraextrados(totalExtraDos);
			
			aux.asictrlasi.setTotlimentuno(totalLim1);
			aux.asictrlasi.setTotlimentdos(totalLim2);
			aux.asictrlasi.setTotlimentrada(totalLim3);
    		
			aux.asictrlasi.setHorapermiso(totalPapeleta);

			(new EvaluarAsistenciaTurnos()).ejecutarAccion(peticion);
			
			iniciarFormulario(peticion);
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdImprimirEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			BigDecimal ctrlasicodigo = (BigDecimal) peticion.get("Asictrlasistencia_kyctrlasistencia");
			
			peticion.put("reportName", "rep-aspapeleta");
			peticion.put("reporteModulo", "REPPAPE");
			peticion.put("webreport", "Y");
			peticion.put("format", "pdf");
			peticion.put("noParams", "Y");
			
			String sql = "select * from asictrlasistencia where ctrlasicodigo = " + ctrlasicodigo.toString();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asictrlasistencia");
			Asictrlasistencia ctrlasis = (Asictrlasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);

			if (!Util.getInst().isEmpty(ctrlasis.getKyctrlasistencia()))
			{
				EntityDataSource datasource = new EntityDataSource(ctrlasis);
				peticion.put("datasource", datasource);
				
				sql = "select subarea.* " +
				"from area inner join subarea on area.are_cod=subarea.are_cod " +
				"inner join usuario on usuario.subarecodigo=subarea.subare_cod " +
				"inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " + 
				"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
				"inner join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
				"where asictrlasistencia.kyctrlasistencia = " + ctrlasis.getKyctrlasistencia();
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "SubArea");
				SubArea subAre = (SubArea) (new ObtenerEntidad()).ejecutarAccion(peticion);

				EntityDataSource subAreaDS = new EntityDataSource(subAre);
				peticion.put("subAreaDS", subAreaDS);

				sql = "select usuario.* " +
				"from area inner join subarea on area.are_cod=subarea.are_cod " +
				"inner join usuario on usuario.subarecodigo=subarea.subare_cod " +
				"inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " + 
				"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
				"inner join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
				"where asictrlasistencia.kyctrlasistencia = " + ctrlasis.getKyctrlasistencia();
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Usuario");
				Usuario usu = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);

				EntityDataSource usuarioDS = new EntityDataSource(usu);
				peticion.put("usuarioDS", usuarioDS);

				sql = "select aspapeleta.* " +
					"from usuario inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " +
					"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
					"left join ashorario on ashorario.horcodigo=asctrlhorario.horcodigo " +
					"left join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
					"where aspapeleta.estado = '0215' and asictrlasistencia.kyctrlasistencia = " + ctrlasis.getKyctrlasistencia();
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Asihorario");
				List listaHorario = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
				HibernateQueryResultDataSource listaPapeletaDS = new HibernateQueryResultDataSource(listaHorario);
				
				peticion.put("listaPapeletaDS", listaPapeletaDS);
				
				peticion.put("reportName", "rep-asictrlasistencia");
				peticion.put("reporteModulo", "REPPAPE");
				peticion.put("webreport", "Y");
				peticion.put("format", "pdf");
				peticion.put("noParams", "Y");
				
				String url = (String) (new EjecutarEntidadReporte()).ejecutarAccion(peticion);
				Util.getInst().abrirArchivoDesktop(url);
			}
			
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}		
	
	public void cmdImprimirAsistenciaEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			BigDecimal ctrlasicodigo = (BigDecimal) peticion.get("Asictrlasistencia_kyctrlasistencia");
			
			String sql = "select * from asictrlasistencia where ctrlasicodigo = " + ctrlasicodigo.toString();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asictrlasistencia");
			Asictrlasistencia ctrlasis = (Asictrlasistencia) (new ObtenerEntidad()).ejecutarAccion(peticion);

			if (!Util.getInst().isEmpty(ctrlasis.getKyctrlasistencia()))
			{
				EntityDataSource datasource = new EntityDataSource(ctrlasis);
				peticion.put("datasource", datasource);
				
				sql = "select subarea.* " +
				"from area inner join subarea on area.are_cod=subarea.are_cod " +
				"inner join usuario on usuario.subarecodigo=subarea.subare_cod " +
				"inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " + 
				"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
				"inner join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
				"where asictrlasistencia.kyctrlasistencia = " + ctrlasis.getKyctrlasistencia();
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "SubArea");
				SubArea subAre = (SubArea) (new ObtenerEntidad()).ejecutarAccion(peticion);

				EntityDataSource subAreaDS = new EntityDataSource(subAre);
				peticion.put("subAreaDS", subAreaDS);

				sql = "select usuario.* " +
				"from area inner join subarea on area.are_cod=subarea.are_cod " +
				"inner join usuario on usuario.subarecodigo=subarea.subare_cod " +
				"inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " + 
				"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
				"inner join aspapeleta on asctrlhorario.papcodigo=aspapeleta.papcodigo " +
				"where asictrlasistencia.kyctrlasistencia = " + ctrlasis.getKyctrlasistencia();
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Usuario");
				Usuario usu = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);

				EntityDataSource usuarioDS = new EntityDataSource(usu);
				peticion.put("usuarioDS", usuarioDS);

				sql = "select asasistencia.* from asictrlasistencia inner join asasistencia on asasistencia.kyctrlasistencia=asictrlasistencia.kyctrlasistencia " +
				"where asictrlasistencia.kyctrlasistencia = " + ctrlasis.getKyctrlasistencia() + " order by asasistencia.ingreso desc";
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Asiasistencia");
				List listaAsistencia = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
				HibernateQueryResultDataSource listaAsistenciaDS = new HibernateQueryResultDataSource(listaAsistencia);
				
				peticion.put("listaAsistenciaDS", listaAsistenciaDS);
				
				peticion.put("reportName", "rep-asasistencia");
				peticion.put("reporteModulo", "REPPAPE");
				peticion.put("webreport", "Y");
				peticion.put("format", "pdf");
				peticion.put("noParams", "Y");
				
				String url = (String) (new EjecutarEntidadReporte()).ejecutarAccion(peticion);
				Util.getInst().abrirArchivoDesktop(url);
			}
			
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}			
	public void cmdBrowseSeleccionarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			
			Util.getInst().setPeticionEntidad(peticion, aux.asiplanhora);
			
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
	
}
