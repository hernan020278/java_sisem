package com.sis.manejador;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Map;

import com.asistencia.accion.ActualizarAsihorario;
import com.asistencia.accion.ActualizarListaAsiturno;
import com.asistencia.accion.GetListaTurnosByDiaAndDiaAnt;
import com.browse.accion.EjecutarEntidadReporte;
import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Area;
import com.comun.entidad.Asictrlasistencia;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiplantillahorario;
import com.comun.entidad.Asiturno;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Log;
import com.comun.utilidad.Util;
import com.pedido.accion.impl.GenerarBrowseAsihorario;
import com.pedido.accion.GenerarBrowseAsiplantillahorario;
import com.pedido.accion.GenerarBrowseTurno;
import com.reporte.datasource.EntityDataSource;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.ExisteHorario;
import com.usuario.accion.getListaTurnosByHorario;

public class ManejadorDlgAsihorario extends Manejador
{

	public Map manejarEvento(Map peticion) throws Exception
	{

		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{

		try
		{
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				iniciarInstancias();
				limpiarInstancias();
				(new GenerarBrowseAsihorario()).ejecutarAccion(peticion);
			}
			else if(pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR) && aux.asiplanhora.getKyplantillahorario() != null)
			{
				(new ActualizarAsihorario()).ejecutarAccion(peticion);
				(new ActualizarListaAsiturno()).ejecutarAccion(peticion);
			}
			(new getListaTurnosByHorario()).ejecutarAccion(peticion);
			(new GenerarBrowseTurno()).ejecutarAccion(peticion);
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{

		aux.are = (aux.are == null) ? new Area() : aux.are;
		aux.subAre = (aux.subAre == null) ? new SubArea() : aux.subAre;
		aux.usu = (aux.usu == null) ? new Usuario() : aux.usu;
		aux.asictrlasi = (aux.asictrlasi == null) ? new Asictrlasistencia() : aux.asictrlasi;
		aux.asiplanhora = (aux.asiplanhora == null) ? new Asiplantillahorario() : aux.asiplanhora;
		aux.asihora = (aux.asihora == null) ? new Asihorario() : aux.asihora;
		aux.asiplanhora = (aux.asiplanhora == null) ? new Asiplantillahorario() : aux.asiplanhora;
	}

	@Override
	public void limpiarInstancias()
	{

		if((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.are);
			Util.getInst().limpiarEntidad(aux.subAre);
			Util.getInst().limpiarEntidad(aux.usu);
			Util.getInst().limpiarEntidad(aux.asictrlasi);
			Util.getInst().limpiarEntidad(aux.asiplanhora);
			Util.getInst().limpiarEntidad(aux.asihora);
			Util.getInst().limpiarEntidad(aux.asiplanhora);
		}
	}

	public void cmdGuardarEvento(Map<String, Object> peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			String tipoSql = "";
			if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
				
				peticion.put("tipoSql", tipoSql);
				peticion.put("Asihorario_horarionombre", Estado.HORARIO);
				boolean existePapelataRango = (Boolean) (new ExisteHorario().ejecutarAccion(peticion));
				boolean existeAprobado=true;
				if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO) && ((String)peticion.get("Asihorario_estado")).equals(Estado.MODIFICADO)){
					
					String tipo = (String) peticion.get("Asihorario_tipo");
					Date fechainicio = (Date) peticion.get("Asihorario_fechainicio");
					Date fechafinal = (Date) peticion.get("Asihorario_fechafinal");
					/*
					 * VERIFICAR QUE EXISTE AL MENOS UNA PAPELETA DE APROBADO PARA REALIZAR SU CONTROL DE ASISTENCIA
					 */
					if(tipo.equals("HORARIO")){
						String sqlPap = "select asihorario.* from asihorario " +
								"where asihorario.kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia()  + "' and asihorario.numerodni = '" + aux.usu.getIdentidad() + "' " +
								"and ( ( '" + fechainicio + "' >= asihorario.fechainicio and '" + fechainicio + "' <= asihorario.fechafinal ) " +
								"or ( '" + fechafinal + "' >= asihorario.fechainicio and '" + fechafinal + "' <= asihorario.fechafinal ) " +
								"or ( '" + fechainicio + "' < asihorario.fechainicio and '" + fechafinal + "' > asihorario.fechafinal ) ) " +
								"and asihorario.horariotipo = 'HORARIO' and asihorario.estado = 'APROBADO' and asihorario.estado <> '0000'" +
								"and asihorario.papcodigo <> '" + aux.asihora.getKyctrlasistencia() + "'";
					
						peticion.put("consulta", sqlPap);
						peticion.put("entidadNombre", "Asihorario");
						Asihorario asihora = (Asihorario) (new ObtenerEntidad()).ejecutarAccion(peticion);

						if (Util.getInst().isEmpty(asihora.getKyhorario())){existeAprobado=false; aux.asihora=asihora;}
					}
				}

				if(!existePapelataRango && existeAprobado)
				{
					Util.getInst().setPeticionEntidad(peticion, aux.asihora);
					
					peticion.put("tipoSql", tipoSql);
					peticion.put("entidadObjeto", aux.asihora);
					peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where kyhorario='" + aux.asihora.getKyhorario() + "'"));
					afectados = dbc.ejecutarCommit(peticion);
					Util.getInst().setPeticionEntidad(peticion, aux.asihora);

					if(afectados > 0 && aux.asihora.getHorariotipo().equals("HORARIO"))
					{
//						String sql = "update asihorario set estado = '0217' where papcodigo in (select asihorario.papcodigo " +
//								"from asictrlasistencia inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
//								"inner join asihorario on asihorario.papcodigo=asctrlhorario.papcodigo " +
//								"where asctrlhorario.kyctrlasistencia = '" + aux.asictrlasi.getKyctrlasistencia() + "' and asctrlhorario.usucodigo = '" + aux.usu.getUsucodigo() + "' " +
//								"and asihorario.papcodigo <> '" + aux.asihora.getPapcodigo() + "') and asihorario.tipo='HORARIO' and asihorario.estado <> '0000'";
//						peticion.put("consulta", sql);
//						peticion.put("tipoSql", TipoSql.BATCH);
//						dbc.ejecutarCommit(peticion);
					}
					(new ActualizarAsihorario()).ejecutarAccion(peticion);
					(new GenerarBrowseAsiplantillahorario()).ejecutarAccion(peticion);

					if(afectados > 0) {msgBox("Almacenamiento", "Se guardo el horario satisfactoriamente", "error");pagGen.pagAct.setModo(Modo.VISUALIZANDO);	}
					else {msgBox("Almacenamiento", "No se puede guardar asihoraleta verifique!!!", "error");}
					
				}//if(Util.getInst().isEmpty(asihora.getPapcodigo().toString()))
				else{
					msgBox("Almacenamiento", "Verifique que exista al menos una asihoraleta de tipo horario en estado APROBADO !!!", "error");
				}
				
			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCalcularEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			String tipo = (String) peticion.get("Asihorario_tipo");
			String modo = (String) peticion.get("Asihorario_modo");
			String turno = (String) peticion.get("Asihorario_refturno");
			Date fecinicio = (Date) peticion.get("Asihorario_fechainicio");
			Date fecfinal = (Date) peticion.get("Asihorario_fechafinal");
			Time horinicio = (Time) peticion.get("Asihorario_horinicio");
			Time horfinal = (Time) peticion.get("Asihorario_horfinal");
			
			BigDecimal totalHora = new BigDecimal(0);
			
			if(fecinicio.getTime() <= fecfinal.getTime())
			{
				Date fecactTmp = new Date(fecinicio.getYear(), fecinicio.getMonth(), fecinicio.getDate());
				int numDias = Util.getInst().diferenciaFechas(fecinicio, fecfinal, "DIAS");
				double sumHoras = 0.00;
				if(modo.equals("PERIODO") || modo.equals("TURNO"))
				{
					for(int iteDia = 0; iteDia <= numDias; iteDia++)
					{
						fecactTmp.setDate(fecinicio.getDate() + iteDia);
			        	Date fecantTmp = new Date(fecactTmp.getYear(), fecactTmp.getMonth() , fecactTmp.getDate() - 1);
			        	Date fecposTmp = new Date(fecactTmp.getYear(), fecactTmp.getMonth() , fecactTmp.getDate() + 1);
						peticion.put("fechaActual", fecactTmp);
						peticion.put("fecant", fecantTmp);
						peticion.put("fecpos", fecposTmp);
						
						(new GetListaTurnosByDiaAndDiaAnt()).ejecutarAccion(peticion);
						
						List listaTurno = (List) peticion.get("listaAsiturno");
						boolean agregarHora = true; 
						for(int iteLis = 0; iteLis < listaTurno.size(); iteLis++)
						{
							Asiturno asiturno = (Asiturno) listaTurno.get(iteLis);
							
							if(asiturno.getTurnotipo().equals("LABORABLE"))
							{
								if( !tipo.equals("REFRIGERIO") && ( ( modo.equals("PERIODO") && turno.equals("TODOS") ) || ( modo.equals("TURNO") && turno.equals("TODOS") ) ) )
								{
									totalHora = totalHora.add(Util.getInst().getTotalHoraDecimal(asiturno.getEntrada(), asiturno.getSalida()));
								}
								else if( !tipo.equals("REFRIGERIO") && ( ( modo.equals("PERIODO") && turno.equals(asiturno.getTurnonombre()) ) || ( modo.equals("TURNO") && turno.equals(asiturno.getTurnonombre()) ) ) )
								{
									totalHora = totalHora.add(Util.getInst().getTotalHoraDecimal(asiturno.getEntrada(), asiturno.getSalida()));
								}
								else if( tipo.equals("REFRIGERIO") && ( modo.equals("PERIODO") && turno.equals("TODOS") && agregarHora ) )
								{
									totalHora = totalHora.add(Util.getInst().getTotalHoraDecimal(horinicio, horfinal));
									agregarHora = false;
								}
								else if( tipo.equals("REFRIGERIO") && ( ( modo.equals("PERIODO") && turno.equals(asiturno.getTurnonombre()) ) || ( modo.equals("TURNO") && turno.equals(asiturno.getTurnonombre()) ) ) )
								{
									totalHora = totalHora.add(Util.getInst().getTotalHoraDecimal(horinicio, horfinal));
								}
							}//if(asiturno.getTipo().equals("LABORABLE"))
							Log.debug(this, fecactTmp.toString() + " : " + asiturno.getEntrada() + " - " + asiturno.getSalida() + " totalhora : " + totalHora.toString());
						}// for(int iteLis = 0; iteLis < listaTurno.size(); iteLis++)
					}//for(int iteDia = 0; iteDia <= numDias; iteDia++)
				}// if(modo.equals("PERIODO") || modo.equals("TURNO"))
			}// if (fecinicio.getTime() < fecfinal.getTime())
			peticion.put("totalHora", totalHora);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdImprimirEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();

			BigDecimal papcodigo = (BigDecimal) peticion.get("Asihorario_papcodigo");
			
			peticion.put("reportName", "rep-asihorario");
			peticion.put("reporteModulo", "REPPAPE");
			peticion.put("webreport", "Y");
			peticion.put("format", "pdf");
			peticion.put("noParams", "Y");
			
			String sql = "select * from asihorario where papcodigo = " + papcodigo.toString();
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asihorario");
			Asihorario asihora = (Asihorario) (new ObtenerEntidad()).ejecutarAccion(peticion);

			if (!Util.getInst().isEmpty(asihora.getKyhorario()))
			{
				EntityDataSource datasource = new EntityDataSource(asihora);
				peticion.put("datasource", datasource);
				
				sql = "select subarea.* " +
				"from area inner join subarea on area.are_cod=subarea.are_cod " +
				"inner join usuario on usuario.subarecodigo=subarea.subare_cod " +
				"inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " + 
				"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
				"inner join asihorario on asctrlhorario.papcodigo=asihorario.papcodigo " +
				"where asihorario.papcodigo = " + papcodigo.toString();
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "SubArea");
				SubArea subAre = (SubArea) (new ObtenerEntidad()).ejecutarAccion(peticion);

				EntityDataSource subAreaDS = new EntityDataSource(subAre);
				peticion.put("subAreaDS", subAreaDS);

				sql = "select usuario.* " +
				"from area inner join subarea on area.are_cod=subarea.are_cod " +
				"inner join usuario on usuario.subarecodigo=subarea.subare_cod " +
				"inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo " + 
				"inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia " +
				"inner join asihorario on asctrlhorario.papcodigo=asihorario.papcodigo " +
				"where asihorario.papcodigo = " + papcodigo.toString();
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Usuario");
				Usuario usu = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);

				EntityDataSource usuarioDS = new EntityDataSource(usu);
				peticion.put("usuarioDS", usuarioDS);

				String url = (String) (new EjecutarEntidadReporte()).ejecutarAccion(peticion);
				Util.getInst().abrirArchivoDesktop(url);
			}
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}	
	public void cmdAgregarAsctrlhorarioEvento(Map peticion)
	{

		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			abrirPagina(paginaHijo, paginaPadre, Modo.AGREGANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdModificarEvento(Map peticion)
	{

		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			abrirPagina(paginaHijo, paginaPadre, Modo.MODIFICANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdEliminarEvento(Map peticion)
	{

		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			int afectados = (peticion.containsKey("afectados")) ? (Integer) peticion.get("afectados") : 0;
			
			if(afectados > 0) {msgBox("Eliminacion", "Registro Eliminado", "info");}
			else {msgBox("Eliminacion", "El registro no puede ser eliminado", "error");}
			
			iniciarFormulario(peticion);
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdBrowseSeleccionarEvento(Map peticion)
	{

		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			Util.getInst().setPeticionEntidad(peticion, aux.asiplanhora);
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdCerrarEvento(Map peticion)
	{

		cerrarFormulario(peticion);
	}
}
