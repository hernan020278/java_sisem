package com.sis.manejador;

import java.util.Map;

import com.asistencia.accion.MarcarAsistencia;
import com.componente.accion.ObtenerListaUsarioPrueba;
import com.comun.entidad.Asiasistencia;
import com.comun.entidad.Asictrlasistencia;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiplantillahorario;
import com.comun.entidad.Asiturno;
import com.comun.entidad.Usuario;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.pedido.accion.GenerarViewAsistencia;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgAsasistencia extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
				iniciarInstancias();
				limpiarInstancias();
				
				(new ObtenerListaUsarioPrueba()).ejecutarAccion(peticion);
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			aux.usu = (aux.usu == null) ? new Usuario() : aux.usu;
			aux.asictrlasi = (aux.asictrlasi == null) ? new Asictrlasistencia() : aux.asictrlasi;
			aux.asihora = (aux.asihora == null) ? new Asihorario() : aux.asihora;
			aux.asiplanhora = (aux.asiplanhora == null) ? new Asiplantillahorario() : aux.asiplanhora;
			aux.asiturn = (aux.asiturn == null) ? new Asiturno() : aux.asiturn;
			aux.asiasis = (aux.asiasis == null) ? new Asiasistencia() : aux.asiasis;
		}
	}

	@Override
	public void limpiarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.usu);
			Util.getInst().limpiarEntidad(aux.asictrlasi);
			Util.getInst().limpiarEntidad(aux.asihora);
			Util.getInst().limpiarEntidad(aux.asiplanhora);
			Util.getInst().limpiarEntidad(aux.asiturn);
			Util.getInst().limpiarEntidad(aux.asiasis);
		}
	}

	public void txtUsuario_identidadKeyTypedEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			
			(new MarcarAsistencia()).ejecutarAccion(peticion);
			
			(new GenerarViewAsistencia()).ejecutarAccion(peticion);
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
	
}
