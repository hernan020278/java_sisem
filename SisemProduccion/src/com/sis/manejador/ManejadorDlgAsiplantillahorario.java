package com.sis.manejador;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiplantillahorario;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.pedido.accion.GenerarBrowseAsiplantillaturno;
import com.pedido.accion.GenerarBrowseTurno;
import com.sis.main.PaginaGeneral;
import com.usuario.accion.ExisteHorario;

public class ManejadorDlgAsiplantillahorario extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
				iniciarInstancias();
				limpiarInstancias();
				
				peticion.put("tipoBrowse", "BROWSE");
				(new GenerarBrowseAsiplantillaturno()).ejecutarAccion(peticion);
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.asihora = (aux.asihora == null) ? new Asihorario() : aux.asihora;
		aux.asiplanhora = (aux.asiplanhora == null) ? new Asiplantillahorario() : aux.asiplanhora;
	}

	@Override
	public void limpiarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.asihora);
			Util.getInst().limpiarEntidad(aux.asiplanhora);
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
				Util.getInst().setPeticionEntidad(peticion, aux.asihora);
				boolean existeHorario = false;
				if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
				{
					existeHorario = (Boolean)(new ExisteHorario()).ejecutarAccion(peticion);
				}
				if(!existeHorario)
				{
					peticion.put("tipoSql", tipoSql);
					peticion.put("entidadObjeto", aux.asihora);
					peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where kyhorario='" + aux.asihora.getKyhorario() + "'"));
					afectados = dbc.ejecutarCommit(peticion);
				}//if(existeHorario)

				(new GenerarBrowseTurno()).ejecutarAccion(peticion);
				if(afectados > 0){msgBox("Almacenamiento", "Se guardo el horario satisfactoriamente", "error");pagGen.pagAct.setModo(Modo.VISUALIZANDO);}
				else{msgBox("Almacenamiento", "El HORARIO BASE ya existe!!!", "error");}
			}
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarTurnoEvento(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			String horcodigo = (String)peticion.get("Ashorario_horcodigo");
			String sql = "select * from ashorario where horcodigo = " + horcodigo;
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asiplantillahorario");
			Asiplantillahorario asiPlanHora = (Asiplantillahorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if (!Util.getInst().isEmpty(asiPlanHora.getKyplantillahorario()))
			{
				aux.asiplanhora = asiPlanHora;
				
				String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
				String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
				String paginaAccion = (String) peticion.get("Pagina_accion");
				
				peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

				abrirPagina("DlgAsiturno", pagGen.pagAct.getPaginaHijo(), Modo.AGREGANDO, (String) peticion.get("browseTabla"));
			}
			else
			{
				(new GenerarBrowseTurno()).ejecutarAccion(peticion);
				msgBox("Validacion", "Guarde un horario para agregar turno", "error");
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	
	public void cmdBrowseSeleccionarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			
			Util.getInst().setPeticionEntidad(peticion, aux.asiplanhora);
			
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdEliminarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			
			int afectados =(peticion.containsKey("afectados")) ? (Integer) peticion.get("afectados") : 0;
			if(afectados > 0){msgBox("Eliminacion","Registro Eliminado","info");}
			else{msgBox("Eliminacion","Error de eliminacion","error");}
			
			iniciarFormulario(peticion);
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdModificarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			
			abrirPagina(paginaHijo, paginaPadre, Modo.MODIFICANDO, pagGen.pagAct.getBrowseTabla());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}

	
	
}
