package com.sis.manejador;

import com.comun.database.DBConeccion;
import com.comun.entidad.Asiasistencia;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asirepositorio;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;

import java.util.Map;

public class ManejadorDlgAsrepositorio extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			aux.asihora = (aux.asihora == null) ? new Asihorario() : aux.asihora;
			aux.asiasis = (aux.asiasis == null) ? new Asiasistencia() : aux.asiasis;
			aux.asirepo = (aux.asirepo == null) ? new Asirepositorio() : aux.asirepo;
		}
	}

	@Override
	public void limpiarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.asihora);
			Util.getInst().limpiarEntidad(aux.asiasis);
			Util.getInst().limpiarEntidad(aux.asirepo);
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO)){tipoSql = TipoSql.INSERT;}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)){tipoSql = TipoSql.UPDATE;}
			
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
				Util.getInst().setPeticionEntidad(peticion, aux.asirepo);

				peticion.put("tipoSql", tipoSql);
				peticion.put("entidadObjeto", aux.asirepo);
				peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where repcodigo='" + aux.asirepo.getKyrepositorio() + "'"));
				afectados = dbc.ejecutarCommit(peticion);
				
				if(afectados > 0)
				{
					msgBox("Almacenamiento", "Se guardo satisfactoriamente", "info");
					pagGen.pagAct.setModo(Modo.VISUALIZANDO);
				}//if (Util.isEmpty(usu.getIdentidad()))
				else
				{
					msgBox("Error", "Hubo un error al guardar", "error");
				}

			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}	
	
	public void cmdBrowseSeleccionarEvento(Map peticion)
	{

		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);
			abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
	
}
