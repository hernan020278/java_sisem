package com.sis.manejador;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Map;

import javax.swing.JOptionPane;

import com.browse.BrowseObject;
import com.browse.accion.GenerateBrowseObject;
import com.comun.database.ConexionControlBackup;
import com.comun.database.DBConfigBackup;
import com.comun.database.DBConfiguracion;
import com.comun.entidad.Agrupacion;
import com.comun.entidad.Organizacion;
import com.comun.entidad.PerfilSeguridad;
import com.comun.entidad.Seguridad;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.PropiedadesExterna;
import com.comun.utilidad.Util;
import com.reporte.accion.ObtenerReportePorModulo;
import com.sis.contenedor.Contenedor;
import com.sis.main.PaginaGeneral;

public class ManejadorFrmSisem extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.org = (aux.org == null) ? new Organizacion() : aux.org;
		aux.agru = (aux.agru == null) ? new Agrupacion() : aux.agru;
		aux.seg = (aux.seg == null) ? new Seguridad() : aux.seg;
		aux.perfSeg = (aux.perfSeg == null) ? new PerfilSeguridad() : aux.perfSeg;
		aux.usuSes = (aux.usuSes == null) ? new Usuario() : aux.usuSes;
	}

	@Override
	public void limpiarInstancias()
	{
		if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
		{
			// Util.getInstance().limpiarEntidad(aux.org);
			// Util.getInstance().limpiarEntidad(aux.mue);
		}
	}

	public void cmdAsistenciaEvento(Map peticion)
	{
		try
		{
			abrirPagina("DlgControlComponente", pagGen.pagAct.getPaginaHijo(), Modo.MODIFICANDO, (String) peticion.get("browseTabla"));
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdBackupEvento(Map peticion)
	{
		try
		{
			Date fechaActual = Util.getInst().getFechaSql();
			String rutaBackup = AplicacionGeneral.getInstance().obtenerRutaBackup();
			String nombreDatabase = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("nombre-database", "");
			String nombreBackup = nombreDatabase + "_" + fechaActual.getDate() + (fechaActual.getMonth() + 1) + (fechaActual.getYear() + 1900);
			String archivoBak = rutaBackup + nombreBackup + ".bak";
			String archivoRar = rutaBackup + nombreBackup + ".rar";
			
//			Util.getInstance().ejecutarArchivoDesktop("cmd.exe /K C:\\sisem\\bin\\generar_backup_by_ftp.bat");
			Util.getInst().ejecutarArchivoDesktop("cmd.exe /K C:\\sisem\\bin\\generar_backup_by_ftp.bat ");
		} 
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
	public static void main(String[] args)
	{
		OrganizacionGeneral.getInstance("");
		ManejadorFrmSisem a = new ManejadorFrmSisem();
//		System.out.println("Generando backup ... : " + a.generarBackupByFTP());
	}
}
