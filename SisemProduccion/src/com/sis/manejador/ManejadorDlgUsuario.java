package com.sis.manejador;

import java.math.BigDecimal;
import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Area;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgUsuario extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
			{
				iniciarInstancias();
				limpiarInstancias();
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.ope = (aux.ope == null) ? new Usuario() : aux.ope;
		aux.are = (aux.are == null) ? new Area() : aux.are;
		aux.subAre = (aux.subAre == null) ? new SubArea() : aux.subAre;
	}

	@Override
	public void limpiarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.ope);
			Util.getInst().limpiarEntidad(aux.are);
			Util.getInst().limpiarEntidad(aux.subAre);
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

				Util.getInst().setPeticionEntidad(peticion, aux.ope);
				aux.ope.setArecodigo(new BigDecimal(aux.are.getAre_cod()));
				aux.ope.setSubarecodigo(new BigDecimal(aux.subAre.getSubare_cod()));

				String sql = "select * from usuario where ( identidad='" + aux.ope.getIdentidad() + "' or (usuario = '" + aux.ope.getUsuario() + "' and clave = '" + aux.ope.getClave() + "' ) )";
				
				if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO)){sql = sql + " and usucodigo <> " + aux.ope.getUsucodigo();}
				
				peticion.put("consulta", sql);
				peticion.put("entidadNombre", "Usuario");

				Usuario usu = (Usuario) (new ObtenerEntidad()).ejecutarAccion(peticion);
				if (Util.isEmpty(usu.getIdentidad()))
				{
					peticion.put("tipoSql", tipoSql);
					peticion.put("entidadObjeto", aux.ope);
					peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where usucodigo='" + aux.ope.getUsucodigo() + "'"));
					afectados = dbc.ejecutarCommit(peticion);
					msgBox("Almacenamiento", "El usuario se guardo", "info");
					pagGen.pagAct.setModo(Modo.VISUALIZANDO);
				}//if (Util.isEmpty(usu.getIdentidad()))
				else
				{
					msgBox("Error", "Este usuario ya existe", "error");
				}

			}
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdBrowseSeleccionarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			
			Util.getInst().setPeticionEntidad(peticion, aux.ope);
			
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
	
}
