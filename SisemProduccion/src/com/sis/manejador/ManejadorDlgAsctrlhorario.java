package com.sis.manejador;

import java.util.Map;

import com.comun.accion.ObtenerEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.Area;
import com.comun.entidad.Asictrlasistencia;
import com.comun.entidad.Asihorario;
import com.comun.entidad.Asiplantillahorario;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.motor.AccionListener;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.pedido.accion.GenerarBrowseAsiplantillahorario;
import com.pedido.accion.GenerarBrowseTurno;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgAsctrlhorario extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			if (pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR))
			{
				iniciarInstancias();
				limpiarInstancias();
				
				(new GenerarBrowseAsiplantillahorario()).ejecutarAccion(peticion);
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.are = (aux.are == null) ? new Area() : aux.are;
		aux.subAre = (aux.subAre == null) ? new SubArea() : aux.subAre;
		aux.usu = (aux.usu == null) ? new Usuario() : aux.usu;
		aux.asictrlasi = (aux.asictrlasi == null) ? new Asictrlasistencia() : aux.asictrlasi;
		
		aux.asihora = (aux.asihora == null) ? new Asihorario() : aux.asihora;
		aux.asiplanhora = (aux.asiplanhora == null) ? new Asiplantillahorario() : aux.asiplanhora;
		aux.asihora = (aux.asihora == null) ? new Asihorario() : aux.asihora;
	}

	@Override
	public void limpiarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.are);
			Util.getInst().limpiarEntidad(aux.subAre);
			Util.getInst().limpiarEntidad(aux.usu);
			Util.getInst().limpiarEntidad(aux.asictrlasi);
			Util.getInst().limpiarEntidad(aux.asihora);
			Util.getInst().limpiarEntidad(aux.asiplanhora);
			Util.getInst().limpiarEntidad(aux.asihora);
		}
	}

	public void cmdGuardarEvento(Map peticion) {

		try
		{
			Auxiliar aux = Auxiliar.getInstance();
			String tipoSql = "";
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
			{
				tipoSql = TipoSql.INSERT;
			}
			else if (pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				tipoSql = TipoSql.UPDATE;
			}
			if (pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
			{
				DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");

				Util.getInst().setPeticionEntidad(peticion, aux.asiplanhora);

				peticion.put("tipoSql", tipoSql);
				peticion.put("entidadObjeto", aux.asiplanhora);
				peticion.put("where", ((tipoSql.equals(TipoSql.INSERT)) ? "" : "where horcodigo='" + aux.asiplanhora.getKyplantillahorario() + "'"));
				afectados = dbc.ejecutarCommit(peticion);

//				cerrarFormulario(peticion);
				(new GenerarBrowseTurno()).ejecutarAccion(peticion);
				if(afectados > 0){msgBox("Almacenamiento", "Se guardo el horario satisfactoriamente", "error");pagGen.pagAct.setModo(Modo.VISUALIZANDO);}
				else{msgBox("Almacenamiento", "Error al guardar datos", "error");}
			}
			
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdAgregarTurnoEvento(Map peticion)
	{
		try
		{
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			
			String horcodigo = (String)peticion.get("Ashorario_horcodigo");
			String sql = "select * from ashorario where horcodigo = " + horcodigo;
			peticion.put("consulta", sql);
			peticion.put("entidadNombre", "Asiplantillahorario");
			Asiplantillahorario ashorario = (Asiplantillahorario) (new ObtenerEntidad()).ejecutarAccion(peticion);
			if (!Util.getInst().isEmpty(ashorario.getKyplantillahorario()))
			{
				aux.asiplanhora = ashorario;
				
				String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
				String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
				String paginaAccion = (String) peticion.get("Pagina_accion");
				
				peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

				abrirPagina("DlgAsiturno", pagGen.pagAct.getPaginaHijo(), Modo.AGREGANDO, (String) peticion.get("browseTabla"));
			}
			else
			{
				(new GenerarBrowseTurno()).ejecutarAccion(peticion);
				msgBox("Validacion", "Guarde un horario para agregar turno", "error");
			}
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	
	public void cmdBrowseSeleccionarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			
			Util.getInst().setPeticionEntidad(peticion, aux.asiplanhora);
			
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
	
}
