package com.sis.manejador;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseObject;
import com.browse.accion.GenerarBrowse;
import com.comun.accion.ObtenerEntidad;
import com.comun.accion.ObtenerListaEntidad;
import com.comun.database.DBConeccion;
import com.comun.entidad.DetComponente;
import com.comun.entidad.Pedido;
import com.comun.entidad.Producto;
import com.comun.motor.AccionListener;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.referencia.TipoSql;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.pedido.accion.GenerarBrowseComponente;
import com.pedido.accion.GenerarBrowsePartida;
import com.reporte.accion.GenerarReporte;
import com.reporte.datasource.HibernateQueryResultDataSource;
import com.reporte.jasperreport.ReportUtils;
import com.sis.main.PaginaGeneral;

public class ManejadorDlgControlPartida extends Manejador
{
	public Map manejarEvento(Map peticion) throws Exception
	{
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		empezarDBConnection(peticion);
		procesarEvento(peticion);
		terminarDBConnection(peticion);
		return peticion;
	}

	@Override
	public void iniciarFormulario(Map peticion)
	{
		try
		{
			iniciarInstancias();
			limpiarInstancias();
			Browse browse = (Browse) peticion.get("browse");
			BrowseObject browseObject = (BrowseObject) peticion.get("browseObject");
			List browseList = (List) peticion.get("browseList");
			
			(new GenerarBrowsePartida()).ejecutarAccion(peticion);
			(new GenerarBrowseComponente()).ejecutarAccion(peticion);
			
			peticion.put("browse", browse);
			peticion.put("browseObject", browseObject);
			peticion.put("browseList", browseList);
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}// fin de llimpiarInstancias

	@Override
	public void iniciarInstancias()
	{
		aux.ped = (aux.ped == null) ? new Pedido() : aux.ped;
		aux.mod = (aux.mod == null) ? new Producto() : aux.mod;
		aux.detCmp = (aux.detCmp == null) ? new DetComponente() : aux.detCmp;
	}

	@Override
	public void limpiarInstancias()
	{
		if ((pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.FINALIZAR)))
		{
			Util.getInst().limpiarEntidad(aux.ped);
			Util.getInst().limpiarEntidad(aux.mod);
			Util.getInst().limpiarEntidad(aux.detCmp);
		}
	}

	public void cmdAgregarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaPadre = (String) peticion.get("Pagina_paginaPadre");
			String paginaAccion = (String) peticion.get("Pagina_accion");
			
			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			abrirPagina("DlgBrowseTabla-" + pagGen.pagAct.getPaginaHijo(), pagGen.pagAct.getPaginaHijo(), Modo.VISUALIZANDO, (String) peticion.get("browseTabla"));
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdImprimirEvento(Map peticion) {

		try
		{

			peticion.put("organizacionIde", OrganizacionGeneral.getInstance().getOrgcodigo());
			peticion.put("userId", "");
			String whereComponente = (String) peticion.get("whereComponente");
			String sql = "select componente.* from componente inner join detcomponente on componente.cmpcodigo=detcomponente.cmpcodigo " +
					"where detcomponente.prdcodigo=" + aux.mod.getPrd_cod() + " and detcomponente.estado<>'0000' and componente.tipo='PARTIDA' and " + whereComponente;
//			peticion.put("consulta", sql);
//			peticion.put("entidadNombre", "Componente");
//			List listaComponente = (List) (new ObtenerListaEntidad()).ejecutarAccion(peticion);
//			
//			peticion.put("listaComponenteDS", new HibernateQueryResultDataSource(listaComponente));
			
			peticion.put("consultaListaComponentes", sql);
			peticion.put("entidadNombreListaComponentes", "Componente");
			
			(new GenerarReporte()).ejecutarAccion(peticion);

			actualizaPagina(Acceso.ACTUALIZAR,pagGen.pagAct.getModo());

		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}

	public void cmdActualizarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			actualizaPagina(Acceso.ACTUALIZAR,pagGen.pagAct.getModo());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	public void cmdEliminarEvento(Map peticion)
	{
		try
		{
			String paginaHijo = (String) peticion.get("Pagina_paginaHijo");
			String paginaAccion = (String) peticion.get("Pagina_accion");

			peticion = (Map) ((AccionListener) Class.forName(paginaAccion).newInstance()).ejecutarAccion(peticion);

			iniciarFormulario(peticion);
			
			actualizaPagina(Acceso.ACTUALIZAR, pagGen.pagAct.getModo());
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			e.printStackTrace();
		}
	}
	
	
	public void cmdCerrarEvento(Map peticion)
	{
		cerrarFormulario(peticion);
	}
	
}
