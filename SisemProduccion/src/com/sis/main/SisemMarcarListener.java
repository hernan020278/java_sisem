package com.sis.main;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Log;
import com.comun.utilidad.SwingWorker;
import com.sis.browse.DlgControlProceso;
import com.sis.contenedor.Contenedor;

public class SisemMarcarListener {

	private static ServerSocket SERVER_SOCKET;
	private static String estadoEvento = Modo.EVENTO_ENESPERA;
	private static Map peticion;
	private static PaginaGeneral pagGen;
	private static SisemServletControlador sisem;
	private static String paginaFinal, paginaActual;
	private static DlgControlProceso controlProceso;

	public void ejecutarModulo(String orgIde)
	{
		try
		{
			OrganizacionGeneral.getInstance(orgIde);
			peticion = new HashMap();
			pagGen = PaginaGeneral.getInstance();
			pagGen.pagAct.setOrganizacionIde("sisem");
			pagGen.pagAct.setTipoConeccion("transaccion");
			pagGen.pagAct.setPaginaHijo("MarcarListener");
			pagGen.pagAct.setPaginaPadre("MarcarListener");
			pagGen.pagAct.setAcceso(Acceso.INICIAR);
			pagGen.pagAct.setModo(Modo.VISUALIZANDO);
			pagGen.pagAct.setEvento("iniciarFormulario");
			pagGen.pagAct.setAccion("");
			pagGen.pagAct.setPeticion(Peticion.AJAX);
			pagGen.pagAct.setNavegar(false);
			pagGen.pagAct.setBrowseTabla("");
			paginaActual = pagGen.pagAct.getPaginaHijo();
			sisem = new SisemServletControlador();
			SwingWorker swnWork = new SwingWorker()
			{

				@Override
				public Object construct()
				{

					estadoEvento = Modo.INICIAR_EVENTO;
					sisem.doPost(peticion);
					iniciarControlProceso();
					peticion = sisem.getPeticionAsincrono();
					finalizarPaginaEvento();
					Log.debug(this, "termino proceso uno");
					if(pagGen.pagNew.isNavegar() && !pagGen.pagNew.getPaginaHijo().equals(pagGen.pagAct.getPaginaHijo()))
					{
						SwingWorker swnWork = new SwingWorker()
						{

							@Override
							public Object construct()
							{

								try
								{
									pagGen.establecerPaginaCache(pagGen.pagNew);
									paginaFinal = pagGen.pagAct.getPaginaHijo();
									sisem.doPost(peticion);
									iniciarControlProceso();
									peticion = sisem.getPeticionAsincrono();
									if(paginaFinal.contains("-"))
									{
										paginaFinal = paginaFinal.substring(0, paginaFinal.indexOf("-"));
									}
									paginaFinal = paginaFinal.substring(0, 1).toLowerCase() + paginaFinal.subSequence(1, paginaFinal.length());
									finalizarControlProceso();
									SwingUtilities.invokeLater(new Runnable()
									{

										@Override
										public void run()
										{

											VistaListener paginaActual = (VistaListener) Contenedor.getComponent(paginaFinal);
											paginaActual.setPeticion(peticion);
											paginaActual.iniciarFormulario();
										}
									});
									// System.out.println("pagina Hijo : " + pagGen.pagAct.getPaginaHijo()
									// + "   pagina Padre : " + pagGen.pagAct.getPaginaPadre());
									Log.debug(this, "termino proceso dos");
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
								estadoEvento = Modo.EVENTO_ENESPERA;
								return "Finalizo el proceso";
							}
						};
						swnWork.start();
					}
					return "Finalizo el proceso";
				}
			};
			swnWork.start();
		}
		catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public static void main(final String[] args) {

		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					try 
					{
						OrganizacionGeneral.getInstance(args[0]);
					} 
					catch (ArrayIndexOutOfBoundsException ex)
					{
						OrganizacionGeneral.getInstance("SISEM");
					}
					String socket = AplicacionGeneral.getInstance().obtenerConfig("server-socket", "50000");
					SERVER_SOCKET = new ServerSocket(Integer.parseInt(socket));
					UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
					peticion = new HashMap();
					pagGen = PaginaGeneral.getInstance();
					pagGen.pagAct.setOrganizacionIde("sisem");
					pagGen.pagAct.setTipoConeccion("transaccion");
					pagGen.pagAct.setPaginaHijo("MarcarListener");
					pagGen.pagAct.setPaginaPadre("MarcarListener");
					pagGen.pagAct.setAcceso(Acceso.INICIAR);
					pagGen.pagAct.setModo(Modo.VISUALIZANDO);
					pagGen.pagAct.setEvento("iniciarFormulario");
					pagGen.pagAct.setAccion("");
					pagGen.pagAct.setPeticion(Peticion.AJAX);
					pagGen.pagAct.setNavegar(false);
					pagGen.pagAct.setBrowseTabla("");
					paginaActual = pagGen.pagAct.getPaginaHijo();
					sisem = new SisemServletControlador();
					SwingWorker swnWork = new SwingWorker()
					{

						@Override
						public Object construct()
						{

							estadoEvento = Modo.INICIAR_EVENTO;
							sisem.doPost(peticion);
							iniciarControlProceso();
							peticion = sisem.getPeticionAsincrono();
							finalizarPaginaEvento();
							Log.debug(this, "termino proceso uno");
							if(pagGen.pagNew.isNavegar() && !pagGen.pagNew.getPaginaHijo().equals(pagGen.pagAct.getPaginaHijo()))
							{
								SwingWorker swnWork = new SwingWorker()
								{

									@Override
									public Object construct()
									{

										try
										{
											pagGen.establecerPaginaCache(pagGen.pagNew);
											paginaFinal = pagGen.pagAct.getPaginaHijo();
											sisem.doPost(peticion);
											iniciarControlProceso();
											peticion = sisem.getPeticionAsincrono();
											if(paginaFinal.contains("-"))
											{
												paginaFinal = paginaFinal.substring(0, paginaFinal.indexOf("-"));
											}
											paginaFinal = paginaFinal.substring(0, 1).toLowerCase() + paginaFinal.subSequence(1, paginaFinal.length());
											finalizarControlProceso();
											SwingUtilities.invokeLater(new Runnable()
											{

												@Override
												public void run()
												{

													VistaListener paginaActual = (VistaListener) Contenedor.getComponent(paginaFinal);
													paginaActual.setPeticion(peticion);
													paginaActual.iniciarFormulario();
												}
											});
											// System.out.println("pagina Hijo : " + pagGen.pagAct.getPaginaHijo()
											// + "   pagina Padre : " + pagGen.pagAct.getPaginaPadre());
											Log.debug(this, "termino proceso dos");
										}
										catch (Exception e)
										{
											e.printStackTrace();
										}
										estadoEvento = Modo.EVENTO_ENESPERA;
										return "Finalizo el proceso";
									}
								};
								swnWork.start();
							}
							return "Finalizo el proceso";
						}
					};
					swnWork.start();
				} catch (IOException e) {
					JOptionPane.showConfirmDialog(null, "Cierre la aplicacion anterior" + "\n" + this.getClass().getName(), "Control de Acceso", JOptionPane.PLAIN_MESSAGE,
							JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				}
				catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}

	public static void iniciarControlProceso()
	{

		controlProceso = (DlgControlProceso) Contenedor.getComponent("dlgControlProceso");
		controlProceso.setVisible(true);
		while (!pagGen.pagAct.isStopedAllThread() || pagGen.pagAct.hilo.isHiloActivo())
		{
			controlProceso.etiEvento.setText(pagGen.pagAct.getEvento());
			controlProceso.etiAccion.setText(sisem.getAccionActual());
		}
	}

	public static void finalizarPaginaEvento()
	{

		try
		{
			estadoEvento = Modo.FINALIZANDO_EVENTO;
			finalizarControlProceso();
		} catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}

	public static void finalizarControlProceso()
	{

		if(controlProceso != null && controlProceso.isVisible())
		{
			controlProceso.dispose();
		}
	}
}
