/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sis.main;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 * 
 * @author Paolo
 */
public interface VistaListener {

	public void iniciarFormulario();

	public void limpiarFormulario();

	public void llenarFormulario();

	public void activarFormulario(boolean activo);

	public void refrescarFormulario();

	public void obtenerDatoFormulario();

	public void obtenerDatoBaseDato();

	public boolean validarFormulario();

	public boolean guardarDatoBaseDato(String modoGrabar);

	public void cerrarFormulario();

	public void setPeticion(Map peticion);

	public JProgressBar obtenerBarraProgreso();

	public JLabel obtenerEtiAccionProgreso();

}
