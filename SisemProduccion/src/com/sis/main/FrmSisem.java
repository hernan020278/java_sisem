package com.sis.main;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import com.comun.entidad.Agrupacion;
import com.comun.entidad.Organizacion;
import com.comun.entidad.PerfilSeguridad;
import com.comun.entidad.Seguridad;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.MoverVentanaJComponent;
import com.comun.utilidad.Util;
import com.comun.utilidad.VentanaImagen;
import com.textil.controlador.ManejadorTextil;

public class FrmSisem extends VistaAdatperInicio implements VistaListener {

	private JComponent contenedor;
	private JPanel panelPrincipal;

	// Variable del formulario Principal
	public FrmSisem() {

		addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {

				// contenedor.update(contenedor.getGraphics());
			}
		});
		contenedor = new VentanaImagen(AplicacionGeneral.getInstance().obtenerImagen("fondo_sisem.png"));
		this.setUndecorated(true);
		this.setContentPane(contenedor);
//		AWTUtilities.setWindowOpaque(this, false);
        this.setOpacity(1.0f);
		this.getRootPane().setOpaque(false);
		MoverVentanaJComponent moverVentana = new MoverVentanaJComponent(contenedor);
		this.addMouseListener(moverVentana);
		this.addMouseMotionListener(moverVentana);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		this.getContentPane().setLayout(new BorderLayout());
		this.setSize(new java.awt.Dimension(900, 600));
		this.setLocationRelativeTo(null);
		initComponents();
	}

	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setOpaque(false);
		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		panelPrincipal = new JPanel();
		panelPrincipal.setLayout(null);
		panelPrincipal.setOpaque(false);
		super.setTitle("Sistema de Control Produccion");
		getContentPane().add(panelPrincipal);
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_down.png")); // NOI18N
		cmdCerrar.setBorderPainted(false);
		cmdCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdCerrar.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_down.png")); // NOI18N
		cmdCerrar.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_up.png")); // NOI18N
		cmdCerrar.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				cmdCerrarEvento();
			}
		});
		cmdInicio = new javax.swing.JButton();
		cmdInicio.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdInicioEvento();
			}
		});
		cmdInicio.setOpaque(false);
		cmdInicio.setBounds(10, 490, 100, 100);
		panelPrincipal.add(cmdInicio);
		cmdInicio.setIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_down.png")); // NOI18N
		cmdInicio.setBorderPainted(false);
		cmdInicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdInicio.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_down.png")); // NOI18N
		cmdInicio.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_up.png")); // NOI18N
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.setBounds(700, 490, 100, 100);
		panMenuInicio = new JPanel();
		panMenuInicio.setOpaque(false);
		panMenuInicio.setLayout(null);
		panMenuInicio.setBounds(0, 0, 900, 600);
		panelPrincipal.add(panMenuInicio);
		cmdAsistencia = new JButton();
		cmdAsistencia.setMargin(new Insets(0, 0, 0, 0));
		cmdAsistencia.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

//				cmdAsistenciaEvento();
			}
		});
		cmdAsistencia.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAsistencia.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("asistencia_down.png"));
		cmdAsistencia.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("asistencia_up.png"));
		cmdAsistencia.setIcon(AplicacionGeneral.getInstance().obtenerImagen("asistencia_down.png"));
		cmdAsistencia.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdAsistencia.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdAsistencia.setText("Asistencia");
		cmdAsistencia.setOpaque(false);
		cmdAsistencia.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdAsistencia.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdAsistencia.setBorderPainted(false);
		cmdAsistencia.setBounds(149, 440, 140, 130);
		panMenuInicio.add(cmdAsistencia);
		cmdVenta = new JButton();
		cmdVenta.setMargin(new Insets(0, 0, 0, 0));
		cmdVenta.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

//				cmdVentaEvento();
			}
		});
		cmdVenta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdVenta.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonVentaDown.png"));
		cmdVenta.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonVentaUp.png"));
		cmdVenta.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonVentaDown.png"));
		cmdVenta.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdVenta.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdVenta.setToolTipText("");
		cmdVenta.setText("Venta");
		cmdVenta.setOpaque(false);
		cmdVenta.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdVenta.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdVenta.setBorderPainted(false);
		cmdVenta.setBounds(339, 440, 140, 130);
		panMenuInicio.add(cmdVenta);
		
		cmdProduccion = new JButton();
		cmdProduccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdProduccionEvento();
			}
		});
		cmdProduccion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdProduccion.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaDown.png"));
		cmdProduccion.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaUp.png"));
		cmdProduccion.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaDown.png"));
		cmdProduccion.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdProduccion.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdProduccion.setToolTipText("");
		cmdProduccion.setText("Produccion");
		cmdProduccion.setOpaque(false);
		cmdProduccion.setMargin(new Insets(0, 0, 0, 0));
		cmdProduccion.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdProduccion.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdProduccion.setBorderPainted(false);
		cmdProduccion.setBounds(530, 440, 140, 130);
		panMenuInicio.add(cmdProduccion);
		
		cmdUrlWeb = new JLabel("");
		cmdUrlWeb.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				cmdUrlWebEvento();
			}
		});
		cmdUrlWeb.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdUrlWeb.setBounds(295, 72, 408, 289);
		panMenuInicio.add(cmdUrlWeb);
		setSize(new java.awt.Dimension(900, 600));
		setLocationRelativeTo(null);
	}// </editor-fold>//GEN-END:initComponents

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			int resMsg = JOptionPane.showConfirmDialog(this, "���DESEA SALIR DEL DEL SISTEMA !!!", "Sistema de ", JOptionPane.YES_NO_OPTION);
			if(resMsg == JOptionPane.YES_OPTION)
			{
				ejecutar("cmdCerrarEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
//			Util.getInstance().abrirArchivoDesktop("C:\\sisem\\bin\\textil.bat");
			System.exit(0);
		}
	}// GEN-LAST:event_cmdCerrarActionPerformed

	public void cmdInicioEvento()
	{

		panMenuInicio.setVisible(true);
//		panMenuUsuario.setVisible(false);
	}
	
	public void cmdUsuariosEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdUsuariosEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting cFrmPrincipal">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName()))
				{
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(FrmSisem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(FrmSisem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(FrmSisem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(FrmSisem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		/*
		 * Create and display the form
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				new FrmSisem().setVisible(true);
			}
		});
	}

	private javax.swing.JButton cmdCerrar;
	private javax.swing.JButton cmdInicio;
	private JPanel panMenuInicio;
	private JButton cmdAsistencia;
	private JButton cmdVenta;
	private JButton cmdProduccion;
	private JLabel cmdUrlWeb;

	/*************************************
	 * METODOS DE LA INTERFAZ DE VISTA
	 *************************************/
	@Override
	public void iniciarFormulario() {

		frmAbierto = false;
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		paginaHijo = this.getClass().getSimpleName();
		pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
		if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
		{
			String panelInicio = "PANEL_INICIO";
			if(peticion.containsKey("panelActivo"))
			{
				panelInicio = (String) peticion.get("panelActivo");
			}
			if(panelInicio.equals("PANEL_USUARIO"))
			{
				panMenuInicio.setVisible(false);
//				panMenuUsuario.setVisible(true);
			}
			else if(panelInicio.equals("PANEL_PSICOLOGO"))
			{
				panMenuInicio.setVisible(false);
//				panMenuUsuario.setVisible(false);
			}
			else if(panelInicio.equals("PANEL_INDICADOR"))
			{
				panMenuInicio.setVisible(false);
//				panMenuUsuario.setVisible(false);
			}
			else
			{
				panMenuInicio.setVisible(true);
//				panMenuUsuario.setVisible(false);
			}
			limpiarFormulario();
			llenarFormulario();
			refrescarFormulario();
			super.setVisible(true);
		}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
		else if(pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR)) {
			llenarFormulario();
		}
	}

	@Override
	public void limpiarFormulario()
	{
		String ruta = AplicacionGeneral.getInstance(OrganizacionGeneral.getOrgcodigo()).obtenerRutaImagenes();
		ImageIcon imgIco = new ImageIcon(ruta + "logo.png");
	}

	@Override
	public void llenarFormulario()
	{

	}

	@Override
	public void activarFormulario(boolean activo) {

		// TODO Auto-generated method stub
	}

	@Override
	public void refrescarFormulario() {
	}

	@Override
	public void obtenerDatoFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		boolean validado = true;
		return validado;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return null;
	}

	public void cmdUrlWebEvento()
	{
		Util.getInst().abrirArchivoNavegador("www.sysem.org");
	}
	

	public void cmdProduccionEvento()
	{
		this.dispose();
		
		ManejadorTextil man = new ManejadorTextil();
		
		man.org = new Organizacion();
		man.agru = new Agrupacion();
		man.sec = new Seguridad();
		man.perfSec = new PerfilSeguridad();
		man.usuSes = new Usuario();
		
		Util.getInst().limpiarEntidad(man.org);
		Util.getInst().limpiarEntidad(man.agru);
		Util.getInst().limpiarEntidad(man.sec);
		Util.getInst().limpiarEntidad(man.perfSec);
		Util.getInst().limpiarEntidad(aux.usuSes);		
		man.org.setObjeto(aux.org);
		man.usuSes.setObjeto(aux.usuSes);
		man.agru.setObjeto(aux.agru);
		man.ejecutarModulo(OrganizacionGeneral.getOrgcodigo());
	}
}
