package com.sis.main;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.comun.utilidad.Util;

/**
 * Creation date: May 2005
 * 
 * @author: Kelli Knisely
 */
public class PaginaGeneral {
	private static PaginaGeneral INSTANCE;
	private static HashMap mapaPagina = new HashMap();
	public Pagina pagAct;
	public Pagina pagNew;

	private PaginaGeneral() {
		super();
		pagAct = new Pagina();
		pagNew = new Pagina();
	}

	public static PaginaGeneral getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PaginaGeneral();
		}
		return INSTANCE;
	}

	public Pagina obtenerPagina(String paginaHijo)
	{
		Pagina pagina = (Pagina) mapaPagina.get(paginaHijo);
		return pagina;
	}

	public Pagina obtenerPaginaActual()
	{
		return pagAct;
	}

	public Pagina obtenerPaginaAnterior()
	{
		return pagNew;
	}

	public Pagina establecerPaginaCache(Pagina pagina)
	{
		try
		{
			if (!mapaPagina.containsKey(pagina.getPaginaHijo()))
			{
				agregarPaginaCache(pagina);
			}
			else
			{
				actualizarPaginaCache(pagina);
			}
//			mostrarNavegacion();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return pagina;

	}

	public void actualizarPaginaCache(Pagina pagina)
	{
		Pagina pagTmp = new Pagina();
		pagTmp.setObjeto(pagina);
		String paginaOldHijo = pagTmp.getPaginaHijo();
		pagAct.setObjeto(pagTmp);
		mapaPagina.put(paginaOldHijo, pagTmp);
	}

	/**
	 * @param pagina
	 *            Browse
	 * @exception java.lang.Exception
	 */
	public void agregarPaginaCache(Pagina pagina) throws Exception {
		try
		{
			if (pagina != null)
			{
				Pagina newPagina = new Pagina();
				newPagina.setObjeto(pagina);
				pagAct.setObjeto(newPagina);
				mapaPagina.put(newPagina.getPaginaHijo(), newPagina);
			}
		}
		catch (Exception e) {
			throw e;
		}
	}

	/**
	 * @param codigoOrden
	 *            String
	 * @exception java.lang.Exception
	 */
	public void removerPaginaCache(String paginaHijo) throws Exception {
		try
		{
			Pagina pagina = obtenerPagina(paginaHijo);
			if (pagina != null)
			{
				this.pagAct.setObjeto(obtenerPagina(pagAct.getPaginaPadre()));
				this.pagNew.setObjeto(pagina);
			}
		}
		catch (Exception e) {
			throw e;
		}
	}

	public void eliminarPaginaMapa(String paginaActualHijo) {
		try
		{
			Pagina pagina = obtenerPagina(paginaActualHijo);
			if (pagina != null)
			{
				if (mapaPagina.containsKey(paginaActualHijo))
				{
					mapaPagina.remove(paginaActualHijo);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void eliminarNavegacionMapa() {
		try
		{
			Set keys = mapaPagina.keySet();
			Iterator i = keys.iterator();
			while (i.hasNext())
			{
				String key = (String) i.next();
				Pagina pagina = (Pagina) mapaPagina.get(key);
				if (!pagina.getPaginaHijo().equals("FrmInicio")) {
					eliminarPaginaMapa(pagina.getPaginaHijo());
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getPaginaActual() {
		return this.pagAct.getPaginaHijo();
	}

	public void mostrarNavegacion()
	{
		System.out.println("Pagina Hijo\t\t\tPagina Padre");
		System.out.println("===========\t\t\t============");
		String key = null;
		Object value = null;
		
		Set keys = mapaPagina.keySet();
		Iterator i = keys.iterator();
		while (i.hasNext())
		{
			key = (String) i.next();
			value = (Object) mapaPagina.get(key);
			if(value instanceof Pagina)
			{
				Pagina pagina = (Pagina) value;
				System.out.println(pagina.getPaginaHijo() + "\t\t\t"  + pagina.getPaginaPadre());
			}
		}
	}
	
}
