package com.sis.main;

import java.util.Map;

import com.comun.referencia.Peticion;
import com.comun.utilidad.Util;

public class SisemServletControlador {

	private boolean continuar = false;
	private PaginaGeneral pagGen;
	private SisemControlador controlador;
	private String paginaFinal;
	private Map respuesta;
	private Map peticion;

	public Map doPost(Map request)
	{
		try {

			peticion = request;
			pagGen = PaginaGeneral.getInstance();
			controlador = new SisemControlador();
			respuesta = controlador.manejarEvento(peticion);

			boolean validSession = true;
			boolean userAuthenticated = true;

			/*******************************************************
			 * VALIDACION DE SESSION DE USUARIO POR HACER
			 *******************************************************/

			paginaFinal = pagGen.pagAct.getPaginaHijo();

			if (!validSession)
			{
				paginaFinal = "/sistema/error.jsp";
				userAuthenticated = false;
			}
			if (Util.isEmpty(paginaFinal))
			{
				paginaFinal = "DlgInicio";
			}
			else
			{
				if (pagGen.pagAct.getPeticion().equals(Peticion.AJAX))
				{

				}
				else if (pagGen.pagAct.getPeticion().equals(Peticion.SUBMIT) || pagGen.pagAct.getPeticion().equals(Peticion.SUBMITPOPUP))
				{
//					if (!pagGen.pagAct.getPaginaHijo().equals(pagGen.obtenerPaginaActual().getPaginaHijo()))
//					{
//						ManejadorListener manejador = ManejadorFactory.getInstance().obtenerManejador(peticion);
//						manejador.manejarEvento(peticion);
//
//						paginaFinal = paginaFinal.substring(0, 1).toLowerCase() + paginaFinal.subSequence(1, paginaFinal.length());
//						VistaListener paginaActual = (VistaListener) Contenedor.getComponent(paginaFinal);
//						paginaActual.setPeticion(peticion);
//						paginaActual.iniciarFormulario();
//					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return peticion;
	}
	
	public String getAccionActual()
	{
		return controlador.getAccionActual();
	}
	public void setAccionActual(String accion)
	{
		controlador.setAccionActual(accion);
	}
	
	public Map getPeticionAsincrono()
	{
		return controlador.getPeticionAsincrono();
	}
	
}