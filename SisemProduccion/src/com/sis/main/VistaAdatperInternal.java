package com.sis.main;

import java.awt.Component;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseObject;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Log;
import com.comun.utilidad.SwingWorker;
import com.sis.browse.DlgControlProceso;
import com.sis.browse.PanelTablaGeneral;
import com.sis.contenedor.Contenedor;
import com.sis.produccion.FrmInicio;
import com.toedter.calendar.JDateChooser;

public class VistaAdatperInternal extends JInternalFrame {

	protected Map peticion;
	protected Auxiliar aux;
	public PaginaGeneral pagGen;
	protected String estadoEvento = Modo.EVENTO_ENESPERA;
	protected SisemServletControlador sisem;
	protected DlgControlProceso controlProceso;
	protected JProgressBar barraControlProceso;
	protected JLabel etiAccionProgreso;
	public boolean frmAbierto;
	public String paginaFinal;
	public String paginaHijo;
	public String paginaPadre;
	public String paginaEvento;
	public String paginaAccion;
	public String paginaValor;
	public PanelTablaGeneral panTabGen;
	protected int nodoNivel;
	public JPanel panelAdapter;
	public String paginaActual;
	
	/*
	 * VARIABLES GENERALES PARA LA BROWSE
	 */

	public Browse browse;
	public BrowseObject browseObject;
	public BrowseColumn browseColumns[];
	public List filters;
	
	public VistaAdatperInternal(FrmInicio parent) 
	{
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	public void ejecutar(String evento, String tipoPeticion)
	{
		ejecutar(evento, tipoPeticion, true);
	}

	public void ejecutar(String evento, String tipoPeticion, boolean desactivarControl)
	{

		if(desactivarControl)
		{
			desactivarComponentes();
		}
		sisem = new SisemServletControlador();
		pagGen.pagAct.setEvento(evento);
		pagGen.pagAct.setPeticion(tipoPeticion);
		paginaActual = pagGen.pagAct.getPaginaHijo();
		this.paginaEvento = evento;
		if(pagGen.pagAct.getPeticion().equals(Peticion.AJAX))
		{
			SwingWorker swnWork = new SwingWorker()
			{

				@Override
				public Object construct()
				{

					estadoEvento = Modo.INICIAR_EVENTO;
					sisem.doPost(peticion);
					iniciarControlProceso();
					peticion = sisem.getPeticionAsincrono();
					paginaFinal = pagGen.pagAct.getPaginaHijo();
					finalizarPaginaEvento();
					Log.debug(this, "termino proceso uno");
					mensaje();
					if(pagGen.pagNew.isNavegar() && !pagGen.pagNew.getPaginaHijo().equals(pagGen.pagAct.getPaginaHijo()))
					{
						SwingWorker swnWork = new SwingWorker()
						{
							@Override
							public Object construct()
							{
								try
								{
									pagGen.establecerPaginaCache(pagGen.pagNew);
									paginaFinal = pagGen.pagAct.getPaginaHijo();
									sisem.doPost(peticion);
									iniciarControlProceso();
									mensaje();
									peticion = sisem.getPeticionAsincrono();
									if(paginaFinal.contains("-"))
									{
										paginaFinal = paginaFinal.substring(0, paginaFinal.indexOf("-"));
									}
									paginaFinal = paginaFinal.substring(0, 1).toLowerCase() + paginaFinal.subSequence(1, paginaFinal.length());
									finalizarControlProceso();
									SwingUtilities.invokeLater(new Runnable()
									{
										@Override
										public void run()
										{
											VistaListener paginaActual = (VistaListener) Contenedor.getComponent(paginaFinal);
											paginaActual.setPeticion(peticion);
											paginaActual.iniciarFormulario();
										}
									});
//									System.out.println("Hijo : " + pagGen.pagAct.getPaginaHijo()
//											+ " - Padre : " + pagGen.pagAct.getPaginaPadre()
//											+ " - Acceso : " + pagGen.pagAct.getAcceso()
//											+ " - Modo : " + pagGen.pagAct.getModo());
									Log.debug(this, "termino proceso dos");
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
								return "Finalizo el proceso";
							}
						};
						swnWork.start();
					}// if (pagGen.pagAct.isNavegar() && !pagGen.pagAct.getPaginaHijo().equals(pagGen.pagAnt.getPaginaHijo()))
					return "Finalizo el proceso";
				}
			};
			swnWork.start();
		}
		else
		{
			sisem.doPost(peticion);
			mensaje();
			finalizarPaginaEvento();
			System.out.println("termino submit");
		}
	}

	public void finalizarPaginaEvento()
	{

		try
		{
			estadoEvento = Modo.FINALIZANDO_EVENTO;
			Method method = this.getClass().getMethod(paginaEvento);
			method.invoke(this);
			finalizarControlProceso();
		} catch (Exception exception)
		{
			Log.error(this, "Error en el metodo : " + paginaEvento);
			exception.printStackTrace();
		}
	}

	public void abrirPagina(String tipo)
	{

		try
		{
			Method method = this.getClass().getMethod(tipo);
			method.invoke(this);
		} catch (Exception exception)
		{
			Log.error(this, "Error en el metodo : Abrir pagina");
			exception.printStackTrace();
		}
	}

	public void abrirPagina(String paginaHijo, String paginaPadre, String evento, String accion, String valor, String browseTabla)
	{

		try
		{
			this.paginaHijo = paginaHijo;
			this.paginaPadre = paginaPadre;
			this.paginaEvento = evento;
			this.paginaAccion = accion;
			this.paginaValor = valor;
			pagGen.pagAct.setBrowseTabla(browseTabla);
			Method method = this.getClass().getMethod(evento);
			method.invoke(this);
		} catch (Exception exception)
		{
			Log.error(this, "Error en el metodo : Abrir pagina");
			exception.printStackTrace();
		}
	}

	public void abrirPagina(String paginaHijo, String evento, String accion, String valor)
	{

		try
		{
			this.paginaHijo = paginaHijo;
			this.paginaEvento = evento;
			this.paginaAccion = accion;
			this.paginaValor = valor;
			Method method = this.getClass().getMethod(evento);
			method.invoke(this);
		} catch (Exception exception)
		{
			Log.error(this, "Error en el metodo : Abrir pagina");
			exception.printStackTrace();
		}
	}

	public void ejecutarAccion(String paginaHijo, String evento, String accion, String valor)
	{

		try
		{
			this.paginaHijo = paginaHijo;
			this.paginaEvento = evento;
			this.paginaAccion = accion;
			this.paginaValor = valor;
			Method method = this.getClass().getMethod(evento);
			method.invoke(this);
		} catch (Exception exception)
		{
			Log.error(this, "No existe el metodo " + evento);
			exception.printStackTrace();
		}
	}

	public JProgressBar obtenerBarraProgreso()
	{

		try
		{
			Method method = this.getClass().getMethod("obtenerBarraProgreso");
			return (JProgressBar) method.invoke(this);
		} catch (Exception exception)
		{
			Log.error(this, "Error en la barra de progreso");
			exception.printStackTrace();
		}
		return null;
	}

	public JLabel obtenerEtiAccionProgreso()
	{

		try
		{
			Method method = this.getClass().getMethod("obtenerEtiAccionProgreso");
			return (JLabel) method.invoke(this);
		} catch (Exception exception)
		{
			Log.error(this, "Error en el etiAccion Progreso : ");
			exception.printStackTrace();
		}
		return null;
	}

	public void iniciarControlProceso()
	{

		barraControlProceso = obtenerBarraProgreso();
		etiAccionProgreso = obtenerEtiAccionProgreso();
		if(barraControlProceso != null)
		{
			barraControlProceso.setIndeterminate(true);
			while (!pagGen.pagAct.isStopedAllThread() || pagGen.pagAct.hilo.isHiloActivo())
			{
				etiAccionProgreso.setText(sisem.getAccionActual());
			}
		}
		else
		{
			controlProceso = (DlgControlProceso) Contenedor.getComponent("dlgControlProceso");
			controlProceso.setVisible(true);
			while (!pagGen.pagAct.isStopedAllThread() || pagGen.pagAct.hilo.isHiloActivo())
			{
				controlProceso.etiEvento.setText(pagGen.pagAct.getEvento());
				controlProceso.etiAccion.setText(sisem.getAccionActual());
			}
		}
	}

	public void finalizarControlProceso()
	{
		if(barraControlProceso != null)
		{
			barraControlProceso.setIndeterminate(false);
			etiAccionProgreso.setText("Eventos en espera");
		}
		else if(controlProceso != null && controlProceso.isVisible())
		{
			controlProceso.dispose();
		}
	}

	public void mensaje()
	{

		if(aux.msg.isMostrar() == true) {
			JOptionPane.showConfirmDialog(this,
					aux.msg.getValor()
							+ "\n" + this.getClass().getName(),
					aux.msg.getTitulo(),
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void desactivarComponentes()
	{

		desactivarComponentes(this.panelAdapter);
	}

	public void desactivarComponentes(Object objetoComponente)
	{

		Component[] lista = null;
		if(objetoComponente instanceof JPanel)
		{
			lista = ((JPanel) objetoComponente).getComponents();
		}
		else if(objetoComponente instanceof JTabbedPane)
		{
			lista = ((JTabbedPane) objetoComponente).getComponents();
		}
		else if(objetoComponente instanceof JSplitPane)
		{
			lista = ((JSplitPane) objetoComponente).getComponents();
		}
		else if(objetoComponente instanceof JScrollPane)
		{
			lista = ((JScrollPane) objetoComponente).getComponents();
		}
		else if(objetoComponente instanceof JViewport)
		{
			lista = ((JViewport) objetoComponente).getComponents();
		}
		for(Component componente : lista)
		{
			if(componente instanceof JPanel || componente instanceof JTabbedPane || componente instanceof JSplitPane || componente instanceof JScrollPane || componente instanceof JViewport)
			{
				desactivarComponentes(componente);
			}
			else
			{
				if(componente instanceof JButton
						|| componente instanceof JTable
						|| componente instanceof JList
						|| componente instanceof JComboBox
						|| componente instanceof JRadioButton
						|| componente instanceof JCheckBox
						|| componente instanceof JTextField
						|| componente instanceof JTextArea
						|| componente instanceof JDateChooser
						|| componente instanceof JFormattedTextField
						|| componente instanceof JTree)
				{
					componente.setEnabled(false);
				}
			}
		}
	}
}
