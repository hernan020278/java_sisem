package com.asis.main;

import java.awt.Component;

import javax.swing.RootPaneContainer;

public interface AbstractDialog extends RootPaneContainer {

	public abstract String getTitle();

	public abstract void dispose();

	public abstract void setResizable(boolean b);

	public abstract boolean isResizable();

	public abstract void setTitle(String title);

	public abstract void pack();

	public abstract void setVisible(boolean b);

	public abstract boolean isVisible();

	public abstract void setLocationRelativeTo(Component w);
}