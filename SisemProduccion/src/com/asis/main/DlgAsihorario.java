package com.asis.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import com.browse.Browse;
import com.browse.BrowseObject;
import com.comun.referencia.Acceso;
import com.comun.referencia.Estado;
import com.comun.referencia.FechaFormato;
import com.comun.referencia.Modo;
import com.comun.referencia.PapeletaTipo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextAreaChanged;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoDecimal;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.comun.utilidad.swing.JTextFieldFormatoHora;
import com.sis.browse.PanelTablaGeneral;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;
import com.toedter.calendar.JDateChooser;
import javax.swing.JCheckBox;

public class DlgAsihorario extends VistaAdatper implements VistaListener {

  private JPanel panelPrincipal;
  private PanelTablaGeneral panTabGenTurno;
  private JPopupMenu popMenPanTabGenAsihorario;
  private JMenuItem menAgregarPanTabGenAsihorario;
  private JMenuItem menModificarPanTabGenAsihorario;
  private JMenuItem menEliminarPanTabGenAsihorario;
  private JTextEditorFecha txtEditAsihorario_fecha;
  private JTextEditorFecha txtEditAsihorario_fechainicio;
  private JTextEditorFecha txtEditAsihorario_fechafinal;

  public DlgAsihorario(FrmInicio parent) {

    super(parent);
    setTitle(this.getClass().getSimpleName() + " - Administracion de Horario");
    setResizable(false);
    
    configurarFechas();
    initComponents();
    configurarMenupopup();
    
    // Conjunto de teclas que queremos que sirvan para pasar el foco
    // al siguiente campo de texto: ENTER y TAB
    // Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
    // teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
    // teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));
    // Se pasa el conjunto de teclas al panel principal
    // super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);
    super.addWindowListener(new WindowAdapter() {

      @Override
      public void windowActivated(WindowEvent we) {

        frmAbierto = true;
      }
    });
  }
  private void configurarFechas()
  {
    txtEditAsihorario_fecha = new JTextEditorFecha();
    txtEditAsihorario_fecha.setLocation(0, 92);
    txtEditAsihorario_fecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtEditAsihorario_fecha.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
    
    txtEditAsihorario_fechainicio = new JTextEditorFecha();
    txtEditAsihorario_fechainicio.setLocation(0, 92);
    txtEditAsihorario_fechainicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtEditAsihorario_fechainicio.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());

    txtEditAsihorario_fechafinal = new JTextEditorFecha();
    txtEditAsihorario_fechafinal.setLocation(0, 92);
    txtEditAsihorario_fechafinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtEditAsihorario_fechafinal.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
  }
  
  private void configurarMenupopup()
  {
    /*
     * CONFIGURACION DEL MENU PARA LA TABLA CTRLHORARIO
     */
    popMenPanTabGenAsihorario = new JPopupMenu();

    menAgregarPanTabGenAsihorario = new JMenuItem("Agregar Registro");
    menAgregarPanTabGenAsihorario.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) 
      {
//        cmdAgregarAsihorarioEvento();
      }
    });
    menModificarPanTabGenAsihorario = new JMenuItem("Modificar Registro");
    menModificarPanTabGenAsihorario.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) 
      {
      }
    });
    menEliminarPanTabGenAsihorario = new JMenuItem("Eliminar Registro");
    menEliminarPanTabGenAsihorario.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) 
      {
      }
    });
    popMenPanTabGenAsihorario.add(menAgregarPanTabGenAsihorario);
    popMenPanTabGenAsihorario.add(menModificarPanTabGenAsihorario);
    popMenPanTabGenAsihorario.add(menEliminarPanTabGenAsihorario);    
  }
  

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  private void initComponents() {

    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    getContentPane().setLayout(null);
    panelAdapter = new JPanel();
    panelAdapter.setLayout(null);
    panelAdapter.setOpaque(false);
    getContentPane().add(panelAdapter);
    panelPrincipal = new javax.swing.JPanel();
    panelPrincipal.setOpaque(false);
    panelPrincipal.setLayout(null);
    panelAdapter.add(panelPrincipal);
    panelPrincipal.setBounds(0, 0, 702, 599);
    panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
    setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
    setLocationRelativeTo(null);
    panConEsp = new javax.swing.JPanel();
    panConEsp.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    panConEsp.setLayout(null);
    panelPrincipal.add(panConEsp);
    panConEsp.setBounds(10, 11, 681, 235);
    cmbAsihorario_horariotipo = new JComboBox();
    cmbAsihorario_horariotipo.setName("aspapeleta");
    cmbAsihorario_horariotipo.setMaximumRowCount(12);
    cmbAsihorario_horariotipo.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        cmbAsihorario_tipoChangedEvento(e);
      }

    });
    cmbAsihorario_horariotipo.setModel(new DefaultComboBoxModel(new String[] {"SELECCIONE", "HORARIO", "SUSPENSION", "AMONESTACION", "VACACIONES", "PERMISO", "SALIDA", "DESCANSO MEDICO", "CITA MEDICA", "JUSTIFICACION FALTA"}));
    cmbAsihorario_horariotipo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmbAsihorario_horariotipo.setBounds(10, 92, 141, 23);
    panConEsp.add(cmbAsihorario_horariotipo);
    JLabel lblCargo = new JLabel();
    lblCargo.setText("Tipo");
    lblCargo.setHorizontalAlignment(SwingConstants.CENTER);
    lblCargo.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblCargo.setBounds(10, 69, 141, 20);
    panConEsp.add(lblCargo);
    cmdGuardar = new JButton();
    cmdGuardar.setBounds(234, 185, 100, 40);
    panConEsp.add(cmdGuardar);
    cmdGuardar.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {

        cmdGuardarEvento();
      }
    });
    cmdGuardar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmdGuardar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png"));
    cmdGuardar.setText("Guardar");
    cmdGuardar.setMargin(new Insets(0, 0, 0, 0));
    cmdGuardar.setFont(new Font("Tahoma", Font.PLAIN, 14));
    cmdCerrar = new javax.swing.JButton();
    cmdCerrar.setBounds(336, 185, 87, 40);
    panConEsp.add(cmdCerrar);
    cmdCerrar.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {

        cmdCerrarEvento();
      }
    });
    cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
    cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
    cmdCerrar.setText("Cerrar");
    
    JSeparator separator = new JSeparator();
    separator.setBounds(10, 66, 661, 8);
    panConEsp.add(separator);
    
    JLabel lblUsuario = new JLabel();
    lblUsuario.setText("Usuario");
    lblUsuario.setHorizontalAlignment(SwingConstants.LEFT);
    lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblUsuario.setBounds(85, 11, 249, 23);
    panConEsp.add(lblUsuario);
    
    txtUsuario_nombre = new JTextFieldChanged(50);
    txtUsuario_nombre.setText("Hernan Roberto");
    txtUsuario_nombre.setHorizontalAlignment(SwingConstants.LEFT);
    txtUsuario_nombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtUsuario_nombre.setBounds(85, 35, 249, 23);
    panConEsp.add(txtUsuario_nombre);
    
    txtAsictrlasistencia_periodo = new JTextFieldChanged(50);
    txtAsictrlasistencia_periodo.setText("6-2014");
    txtAsictrlasistencia_periodo.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsictrlasistencia_periodo.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsictrlasistencia_periodo.setBounds(10, 35, 72, 23);
    panConEsp.add(txtAsictrlasistencia_periodo);
    
    JLabel lblPeriodo = new JLabel();
    lblPeriodo.setText("Periodo");
    lblPeriodo.setHorizontalAlignment(SwingConstants.CENTER);
    lblPeriodo.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblPeriodo.setBounds(10, 11, 72, 23);
    panConEsp.add(lblPeriodo);
    
    JLabel lblFechaInicio = new JLabel();
    lblFechaInicio.setText("Fecha Inicio");
    lblFechaInicio.setHorizontalAlignment(SwingConstants.CENTER);
    lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblFechaInicio.setBounds(156, 69, 109, 20);
    panConEsp.add(lblFechaInicio);
    
    cmbAsihorario_fechainicio = new JDateChooser(txtEditAsihorario_fechainicio);
    cmbAsihorario_fechainicio.setName("aspapeleta");
    cmbAsihorario_fechainicio.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmbAsihorario_fechainicio.setDateFormatString("dd/MM/yyyy");
    cmbAsihorario_fechainicio.setBounds(156, 92, 109, 23);
    panConEsp.add(cmbAsihorario_fechainicio);
    
    cmbAsihorario_fechafinal = new JDateChooser(txtEditAsihorario_fechafinal);
    cmbAsihorario_fechafinal.setName("aspapeleta");
    cmbAsihorario_fechafinal.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmbAsihorario_fechafinal.setDateFormatString("dd/MM/yyyy");
    cmbAsihorario_fechafinal.setBounds(332, 92, 109, 23);
    panConEsp.add(cmbAsihorario_fechafinal);
    
    JLabel lblFechaFinal = new JLabel();
    lblFechaFinal.setText("Fecha Final");
    lblFechaFinal.setHorizontalAlignment(SwingConstants.CENTER);
    lblFechaFinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblFechaFinal.setBounds(332, 69, 109, 20);
    panConEsp.add(lblFechaFinal);
    
    txtAsihorario_totalhora = new JTextFieldFormatoDecimal(15, 5);
    txtAsihorario_totalhora.setText("100");
    txtAsihorario_totalhora.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsihorario_totalhora.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsihorario_totalhora.setBounds(508, 91, 54, 23);
    panConEsp.add(txtAsihorario_totalhora);
    
    JLabel lblTotalHoras = new JLabel();
    lblTotalHoras.setText("Horas");
    lblTotalHoras.setHorizontalAlignment(SwingConstants.CENTER);
    lblTotalHoras.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblTotalHoras.setBounds(508, 69, 54, 20);
    panConEsp.add(lblTotalHoras);
    
    JLabel lblObservacion = new JLabel();
    lblObservacion.setText("Observacion");
    lblObservacion.setHorizontalAlignment(SwingConstants.LEFT);
    lblObservacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblObservacion.setBounds(10, 122, 187, 23);
    panConEsp.add(lblObservacion);
    
    JLabel lblEstado = new JLabel();
    lblEstado.setForeground(new Color(153, 102, 0));
    lblEstado.setText("Estado");
    lblEstado.setHorizontalAlignment(SwingConstants.CENTER);
    lblEstado.setFont(new Font("Dialog", Font.PLAIN, 17));
    lblEstado.setBounds(360, 122, 54, 23);
    panConEsp.add(lblEstado);
    
    cmbAsihorario_estado = new JComboBox();
    cmbAsihorario_estado.setForeground(new Color(153, 102, 0));
    cmbAsihorario_estado.setFont(new Font("Dialog", Font.PLAIN, 17));
    cmbAsihorario_estado.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmbAsihorario_estado.setModel(new DefaultComboBoxModel(new String[] {"PENDIENTE", "APROBADO", "DESAPROBADO"}));
    cmbAsihorario_estado.setBounds(424, 122, 138, 23);
    panConEsp.add(cmbAsihorario_estado);
    
    etiAshorario_tipo = new JLabel();
    etiAshorario_tipo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    etiAshorario_tipo.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        abrirPagina("DlgBrowseTabla","cmdBrowseSeleccionarEvento","com.asistencia.accion.BrowseSeleccionarPlantillaHorario","SINCODIGO");
      }
    });
    etiAshorario_tipo.setText("<html><u>Horario<u><html>");
    etiAshorario_tipo.setHorizontalAlignment(SwingConstants.LEFT);
    etiAshorario_tipo.setForeground(Color.BLUE);
    etiAshorario_tipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
    etiAshorario_tipo.setBounds(340, 11, 331, 20);
    panConEsp.add(etiAshorario_tipo);
    
    txtAsihorario_horarionombre = new JTextFieldChanged(100);
    txtAsihorario_horarionombre.setHorizontalAlignment(SwingConstants.LEFT);
    txtAsihorario_horarionombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsihorario_horarionombre.setBounds(340, 34, 331, 23);
    panConEsp.add(txtAsihorario_horarionombre);
    
    txtAsihorario_horainicio = new JTextFieldFormatoHora();
    txtAsihorario_horainicio.setText("");
    txtAsihorario_horainicio.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsihorario_horainicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsihorario_horainicio.setBounds(267, 92, 62, 23);
    panConEsp.add(txtAsihorario_horainicio);
    
    txtAsihorario_horafinal = new JTextFieldFormatoHora();
    txtAsihorario_horafinal.setText("");
    txtAsihorario_horafinal.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsihorario_horafinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsihorario_horafinal.setBounds(443, 91, 62, 23);
    panConEsp.add(txtAsihorario_horafinal);
    
    lblInicio = new JLabel();
    lblInicio.setText("Inicio");
    lblInicio.setHorizontalAlignment(SwingConstants.CENTER);
    lblInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblInicio.setBounds(267, 69, 62, 20);
    panConEsp.add(lblInicio);
    
    JLabel lblFinal = new JLabel();
    lblFinal.setText("Final");
    lblFinal.setHorizontalAlignment(SwingConstants.CENTER);
    lblFinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblFinal.setBounds(443, 69, 62, 20);
    panConEsp.add(lblFinal);
    
    cmdCalcular = new JButton();
    cmdCalcular.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmdCalcular.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        cmdCalcularEvento(e);
      }

    });
    cmdCalcular.setText("Calcular");
    cmdCalcular.setMargin(new Insets(0, 0, 0, 0));
    cmdCalcular.setFont(new Font("Tahoma", Font.PLAIN, 14));
    cmdCalcular.setIcon(AplicacionGeneral.getInstance().obtenerImagen("ficha.png"));
    cmdCalcular.setBounds(10, 185, 115, 40);
    panConEsp.add(cmdCalcular);
    
    cmdImprimir = new JButton();
    cmdImprimir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmdImprimir.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        cmdImprimirEvento();
      }
    });
    cmdImprimir.setText("Imprimir");
    cmdImprimir.setMargin(new Insets(0, 0, 0, 0));
    cmdImprimir.setFont(new Font("Tahoma", Font.PLAIN, 14));
    cmdImprimir.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
    cmdImprimir.setBounds(130, 185, 100, 40);
    panConEsp.add(cmdImprimir);
    
    chkAsihorario_estatico = new JCheckBox("ESTATICO");
    chkAsihorario_estatico.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    chkAsihorario_estatico.setForeground(new Color(153, 102, 0));
    chkAsihorario_estatico.setFont(new Font("Dialog", Font.PLAIN, 17));
    chkAsihorario_estatico.setBounds(218, 122, 115, 23);
    panConEsp.add(chkAsihorario_estatico);
    
    txtAsihorario_observacion = new JTextFieldChanged(100);
    txtAsihorario_observacion.setHorizontalAlignment(SwingConstants.LEFT);
    txtAsihorario_observacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsihorario_observacion.setBounds(10, 151, 661, 23);
    panConEsp.add(txtAsihorario_observacion);
    
    panConTabTurno = new JPanel();
    panConTabTurno.setBounds(10, 247, 681, 342);
    panelPrincipal.add(panConTabTurno);
    panConTabTurno.setLayout(null);
    panConTabTurno.setBorder(null);
    
    panTabGenTurno = new PanelTablaGeneral(panConTabTurno.getWidth(), panConTabTurno.getHeight());
    panTabGenTurno.scrtablaBrowse.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseReleased(MouseEvent evt) 
      {
        panTabGenAsiturnoMosuseReleased(evt);
      }
    });
    panConTabTurno.add(panTabGenTurno);
//    panTabGenAsihorario.cmdAgregar.addActionListener(new ActionListener() 
//    {
//      public void actionPerformed(ActionEvent e) 
//      {
//        cmdAgregarAsihorarioEvento();
//      }
//    });
    panTabGenTurno.tablaBrowse.addMouseListener(new java.awt.event.MouseAdapter() 
    {
//      public void mousePressed(java.awt.event.MouseEvent evt) {
//
//        panTabGenTablaBrowseMousePressed(evt);
//      }
      public void mouseReleased(java.awt.event.MouseEvent evt) {

        panTabGenAsiturnoMosuseReleased(evt);
      }
    });   
    
    JLabel lblAutor = new JLabel();
    lblAutor.setBounds(949, 11, 72, 23);
    panelPrincipal.add(lblAutor);
    lblAutor.setText("Autor");
    lblAutor.setHorizontalAlignment(SwingConstants.RIGHT);
    lblAutor.setFont(new Font("Tahoma", Font.PLAIN, 14));
    
    txtAsihorario_autor = new JTextFieldChanged(50);
    txtAsihorario_autor.setBounds(949, 37, 72, 23);
    panelPrincipal.add(txtAsihorario_autor);
    txtAsihorario_autor.setText("Hernan Roberto");
    txtAsihorario_autor.setHorizontalAlignment(SwingConstants.LEFT);
    txtAsihorario_autor.setFont(new Font("Tahoma", Font.PLAIN, 14));
    
    JLabel lblAprobador = new JLabel();
    lblAprobador.setBounds(949, 61, 72, 23);
    panelPrincipal.add(lblAprobador);
    lblAprobador.setText("Aprobador");
    lblAprobador.setHorizontalAlignment(SwingConstants.RIGHT);
    lblAprobador.setFont(new Font("Tahoma", Font.PLAIN, 14));
    
    txtAsihorario_aprobador = new JTextFieldChanged(50);
    txtAsihorario_aprobador.setBounds(949, 91, 72, 23);
    panelPrincipal.add(txtAsihorario_aprobador);
    txtAsihorario_aprobador.setText("Hernan Roberto");
    txtAsihorario_aprobador.setHorizontalAlignment(SwingConstants.LEFT);
    txtAsihorario_aprobador.setFont(new Font("Tahoma", Font.PLAIN, 14));
    com.toedter.calendar.JDateChooser dateChooser = new com.toedter.calendar.JDateChooser((com.toedter.calendar.IDateEditor) null);
  }// </editor-fold>//GEN-END:initComponents

  /**
   * @param args
   *            the command line arguments
   */
  public static void main(String args[]) {

    /*
     * Set the Nimbus look and feel
     */
    // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /*
     * If Nimbus (introduced in Java SE 6) is not available, stay with the
     * default look and feel. For details see
     * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
     */
    try {
      for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(DlgAsihorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(DlgAsihorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(DlgAsihorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(DlgAsihorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    // </editor-fold>
    /*
     * Create and display the dialog
     */
    java.awt.EventQueue.invokeLater(new Runnable() {

      public void run() {

        DlgAsihorario dialog = new DlgAsihorario(null);
        dialog.addWindowListener(new java.awt.event.WindowAdapter() {

          @Override
          public void windowClosing(java.awt.event.WindowEvent e) {

            System.exit(0);
          }
        });
        dialog.setVisible(true);
      }
    });
  }

  public static javax.swing.JButton cmdCerrar;
  private javax.swing.JPanel panConEsp;
  private JButton cmdGuardar;
  private JComboBox cmbAsihorario_horariotipo;
  private List listaSubArea;
  private Browse browseAsihorario;
  private BrowseObject browseObjectAsihorario;
  private List browseListAsihorario;
  private Browse browseTurno;
  private BrowseObject browseObjectTurno;
  private List browseListTurno;
  private JPanel panConTabTurno;
  private JDateChooser cmbAsihorario_fechainicio;
  private JDateChooser cmbAsihorario_fechafinal;
  private JTextFieldFormatoDecimal txtAsihorario_totalhora;
  private JTextFieldChanged txtAsihorario_autor;
  private JTextFieldChanged txtAsihorario_aprobador;
  private JTextFieldChanged txtUsuario_nombre;
  private JTextFieldChanged txtAsictrlasistencia_periodo;
  private JComboBox cmbAsihorario_estado;
  private JTextFieldChanged txtAsihorario_horarionombre;
  private JLabel etiAshorario_tipo;
  private JLabel lblInicio;
  private JTextFieldFormatoHora txtAsihorario_horainicio;
  private JTextFieldFormatoHora txtAsihorario_horafinal;
  private JButton cmdCalcular;
  private JButton cmdImprimir;
  private JCheckBox chkAsihorario_estatico;
  private JTextFieldChanged txtAsihorario_observacion;
  

  /******************************************************
   * FUNCTIONES DE LA INTERFAZ VISTALISTENER
   ******************************************************/
  @Override
  public void iniciarFormulario() {

    try
    {
      frmAbierto = false;
      aux = Auxiliar.getInstance();
      pagGen = PaginaGeneral.getInstance();
      paginaHijo = this.getClass().getSimpleName();
      pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
      if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
      {
        limpiarFormulario();
        llenarFormulario();
        refrescarFormulario();
      }// Fin de if(acceso.equals(AccesoPagina.INICIAR))
      frmAbierto = true;
      this.show();
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  @Override
  public void limpiarFormulario()
  {
    txtUsuario_nombre.setText("");
    txtAsictrlasistencia_periodo.setText("");
    cmbAsihorario_horariotipo.setSelectedIndex(0);
    cmbAsihorario_fechainicio.setDate(Util.getInst().getFechaSql());
    cmbAsihorario_fechafinal.setDate(Util.getInst().getFechaSql());
    txtAsihorario_totalhora.setValue(0);
    txtAsihorario_autor.setText("");
    txtAsihorario_aprobador.setText("");
    txtAsihorario_observacion.setText("");    
    chkAsihorario_estatico.setSelected(false);
    txtAsihorario_horarionombre.setText("");
  }

  @Override
  public void llenarFormulario() 
  {
    llenarComboPapeleta();
    
    txtAsictrlasistencia_periodo.setText(aux.asictrlasi.getPeriodo());
    txtUsuario_nombre.setText(aux.usu.getNombre() + " " + aux.usu.getApellido());
    txtAsihorario_horarionombre.setText(aux.asihora.getHorarionombre());

    cmbAsihorario_horariotipo.setSelectedItem(aux.asihora.getHorariotipo());
    cmbAsihorario_fechainicio.setDate(aux.asihora.getFechainicio());
    txtAsihorario_horainicio.setText(Util.getInst().fromDateToString(aux.asihora.getFechainicio(), FechaFormato.HHmm));
    cmbAsihorario_fechafinal.setDate(aux.asihora.getFechafinal());
    txtAsihorario_horafinal.setText(Util.getInst().fromDateToString(aux.asihora.getFechafinal(), FechaFormato.HHmm));
    txtAsihorario_totalhora.setValue(aux.asihora.getTotalhora());
    chkAsihorario_estatico.setSelected((aux.asihora.getEstatico().equals("Y"))?true:false);
    cmbAsihorario_estado.setSelectedItem(aux.asihora.getEstado());    
    txtAsihorario_observacion.setText(aux.asihora.getObservacion());

    generarTablaTurno();
  }

  private void llenarComboPapeleta()
  {
    List<String> listaEstados = PapeletaTipo.getAllValues("sisem");
    cmbAsihorario_horariotipo.removeAllItems();
    cmbAsihorario_horariotipo.addItem("SELECCIONE");
    for(String valor : listaEstados)
    {
      cmbAsihorario_horariotipo.addItem(valor);
    }
  }
  
  private void generarTablaTurno()
  {
    browseTurno = (Browse) peticion.get("browseTurno");
    browseObjectTurno = (BrowseObject) peticion.get("browseObjectTurno");
    browseListTurno = (List) peticion.get("browseListTurno");
    panTabGenTurno.crearModeloTabla(browseObjectTurno.getBrowseColumns(), this);
    panTabGenTurno.crearTablaResultado(browseTurno);
    panTabGenTurno.llenarRegistrosTabla(browseTurno, browseObjectTurno.getBrowseColumns(), browseListTurno);
    panTabGenTurno.refrescarControlPanTabGen(browseObjectTurno, pagGen);
  }

  @Override
  public void activarFormulario(boolean activo)
  {
    txtAsictrlasistencia_periodo.setEnabled(false);
    txtUsuario_nombre.setEnabled(false);

    cmbAsihorario_horariotipo.setEnabled(activo);
    cmbAsihorario_fechainicio.setEnabled(activo);
    txtAsihorario_horainicio.setEnabled(activo);
    cmbAsihorario_fechafinal.setEnabled(activo);
    txtAsihorario_horafinal.setEnabled(activo);
    txtAsihorario_totalhora.setEnabled(false);
    chkAsihorario_estatico.setEnabled(activo);
    cmbAsihorario_estado.setEnabled(activo);
    txtAsihorario_observacion.setEnabled(activo);
    
    txtAsihorario_autor.setEnabled(false);
    txtAsihorario_aprobador.setEnabled(false);

    txtAsihorario_horarionombre.setEnabled(false);
    panTabGenTurno.scrtablaBrowse.setEnabled(activo);
    panTabGenTurno.tablaBrowse.setEnabled(activo);

    if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
    {
//      cmbAsihorario_fecha.setEnabled(false);
//      cmbAsihorario_fechainicio.setEnabled(activo);
//      cmbAsihorario_fecejefinal.setEnabled(activo);
    }
    else if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
    {
//      cmbAsihorario_fecha.setEnabled(false);
//      cmbAsihorario_fechainicio.setEnabled(false);
//      cmbAsihorario_fecejefinal.setEnabled(false);
    }
  }

  @Override
  public void refrescarFormulario()
  {

    if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
    {
      activarFormulario(false);
//      menAgregarPanTabGenAsihorario.setEnabled(true);
      
      cmdGuardar.setEnabled(false);
      cmdCalcular.setEnabled(false);
      cmdImprimir.setEnabled(true);
      cmdCerrar.setEnabled(true);
    }
    else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
    {
      activarFormulario(true);
//      menAgregarPanTabGenAsihorario.setEnabled(false);
      cmdGuardar.setEnabled(true);
      cmdCalcular.setEnabled(true);
      cmdImprimir.setEnabled(true);
      cmdCerrar.setEnabled(true);
    }
    else if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
    {
      activarFormulario(true);
//      menAgregarPanTabGenAsihorario.setEnabled(false);
      cmdGuardar.setEnabled(true);
      cmdCalcular.setEnabled(true);
      cmdImprimir.setEnabled(false);
      cmdCerrar.setEnabled(true);
    }
  }

  @Override
  public void obtenerDatoFormulario()
  {
    peticion.put("Pagina_paginaHijo", paginaHijo);
    peticion.put("Pagina_paginaPadre", paginaPadre);
    peticion.put("Pagina_evento", paginaEvento);
    peticion.put("Pagina_accion", paginaAccion);
    peticion.put("Pagina_valor", paginaValor);

    peticion.put("Asihorario_kyctrlasistencia", "");
    peticion.put("Asihorario_kyusuario", aux.usu.getUsucodigo());
    peticion.put("Asihorario_kyctrlasistencia", aux.asictrlasi.getKyctrlasistencia());
    peticion.put("Asihorario_numerodni", aux.asictrlasi.getKyctrlasistencia());
    peticion.put("Asihorario_periodo", aux.asictrlasi.getKyctrlasistencia());
    peticion.put("Asihorario_horariotipo", cmbAsihorario_horariotipo.getSelectedItem());
    peticion.put("Asihorario_horarionombre", txtAsihorario_horarionombre.getText());
    peticion.put("Asihorario_fechainicio", Util.getInst().fromDateToDateSql(cmbAsihorario_fechainicio.getDate()));
    peticion.put("Asihorario_fecejefinal", Util.getInst().fromDateToDateSql(cmbAsihorario_fechafinal.getDate()));
    peticion.put("Asihorario_totalhora", Util.getInst().getBigDecimalFormatted(txtAsihorario_totalhora.getText(),2));
    peticion.put("Asihorario_observacion", txtAsihorario_observacion.getText());
    peticion.put("Asihorario_estatico", (chkAsihorario_estatico.isSelected())?"Y":"N");
    peticion.put("Asihorario_estado", Estado.toClave(cmbAsihorario_estado.getSelectedItem().toString()));
  }

  @Override
  public void obtenerDatoBaseDato() 
  {
    // TODO Auto-generated method stub
  }

  @Override
  public boolean validarFormulario() 
  {
    if(paginaEvento.equals("cmdGuardarEvento"))
    {
      boolean exito =  validarControles("aspapeleta");
      if(!exito){setValoresObligatorios("aspapeleta"); return false;}
    }
    if(paginaEvento.equals("cmdEliminarEvento"))
    {
      int resp = JOptionPane.showConfirmDialog(this, "�Desea eliminar registro?", "Eliminar registro",JOptionPane.YES_NO_OPTION);
      if (resp==JOptionPane.YES_OPTION) { return true;}
      else{return false;}

    }
    
    if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
    {
      return true;
    }
    else if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
    {
      return true;
    }
    return false;
  }

  @Override
  public boolean guardarDatoBaseDato(String modoGrabar) {

    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void cerrarFormulario() {

  }

  
  private void cmbAsihorario_tipoChangedEvento(ItemEvent e) 
  {
    if (e.getStateChange() == ItemEvent.SELECTED)
    {
        frmAbierto = false;
        cmbAsihorario_fechainicio.setDate(Util.getInst().getFechaSql());
        cmbAsihorario_fechafinal.setDate(Util.getInst().getFechaSql());
        txtAsihorario_horainicio.setText("00:00:00");
        txtAsihorario_horafinal.setText("00:00:00");
        txtAsihorario_totalhora.setValue(0.00);
        
        if (cmbAsihorario_horariotipo.getSelectedItem().toString().equals("HORARIO"))
        {
          cmbAsihorario_fechainicio.setEnabled(true);
          cmbAsihorario_fechafinal.setEnabled(true);
          txtAsihorario_horainicio.setEnabled(false);
          txtAsihorario_horafinal.setEnabled(false);
          txtAsihorario_totalhora.setEnabled(false);
          
        }
        else if (cmbAsihorario_horariotipo.getSelectedItem().toString().equals("REFRIGERIO"))
        {
          cmbAsihorario_fechainicio.setEnabled(true);
          cmbAsihorario_fechafinal.setEnabled(true);
          txtAsihorario_horainicio.setEnabled(true);
          txtAsihorario_horafinal.setEnabled(true);
          txtAsihorario_totalhora.setEnabled(false);
          
        }
        else if (cmbAsihorario_horariotipo.getSelectedItem().toString().equals("SELECCIONE"))
        {
          cmbAsihorario_fechainicio.setEnabled(false);
          cmbAsihorario_fechafinal.setEnabled(false);
          txtAsihorario_horainicio.setEnabled(false);
          txtAsihorario_horafinal.setEnabled(false);
          txtAsihorario_totalhora.setEnabled(false);
        }
        else
        {
          cmbAsihorario_fechainicio.setEnabled(true);
          cmbAsihorario_fechafinal.setEnabled(true);
          txtAsihorario_horainicio.setEnabled(false);
          txtAsihorario_horafinal.setEnabled(false);
          txtAsihorario_totalhora.setEnabled(false);
        }
        frmAbierto = true;
    }
  }
  
  /*
   * EVENTOS DE LOS CONTROLES DEL FORMULARIO
   */
  public void cmdCerrarEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      ejecutar("cmdCerrarEvento", Peticion.AJAX);
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      this.dispose();
    }
  }

  public void cmdGuardarEvento()
  {
    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      obtenerDatoFormulario();
      ejecutarAjax("cmdGuardarEvento", Peticion.AJAX);
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      refrescarFormulario();
//      this.dispose();
    }
  }

  private void cmdCalcularEvento(ActionEvent e) 
  {
      cmdCalcularEvento();
      BigDecimal totalHora = (BigDecimal) peticion.get("totalHora");
      txtAsihorario_totalhora.setValue(Util.getInst().getBigDecimalFormatted(totalHora, 5));
  }
  
  public void cmdCalcularEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      if(validarFormulario())
      {
        obtenerDatoFormulario();
        ejecutar("cmdCalcularEvento", Peticion.AJAX);
      }
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      BigDecimal totalHora = (BigDecimal) peticion.get("totalHora");
      txtAsihorario_totalhora.setValue(Util.getInst().getBigDecimalFormatted(totalHora, 5));
      refrescarFormulario();
    }
  }

  public void cmdImprimirEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      ejecutarAjax("cmdImprimirEvento", Peticion.AJAX);
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      refrescarFormulario();
    }
  }
 
  public void cmdBrowseSeleccionarEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      if(validarFormulario())
      {
        obtenerDatoFormulario();
        ejecutar("cmdBrowseSeleccionarEvento", Peticion.AJAX);
      }
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      this.dispose();
    }
  }

  @Override
  public void setPeticion(Map peticion)
  {

    this.peticion = new HashMap();
    this.peticion = peticion;
  }

  @Override
  public JProgressBar obtenerBarraProgreso()
  {

    return null;
  }

  @Override
  public JLabel obtenerEtiAccionProgreso()
  {

    return null;
  }
  
  private void panTabGenAsiturnoMosuseReleased(java.awt.event.MouseEvent evt) 
  {// GEN-FIRST:event_panArtFotoMouseReleased
    if(evt.getButton() == MouseEvent.BUTTON3) 
    {
      popMenPanTabGenAsihorario.show(evt.getComponent(), evt.getX(), evt.getY());
      if(panTabGenTurno.tablaBrowse.getSelectedRow() > -1)
      {
        menEliminarPanTabGenAsihorario.setEnabled(true);
        menModificarPanTabGenAsihorario.setEnabled(true);
      }
      else
      {
        menEliminarPanTabGenAsihorario.setEnabled(false);
        menModificarPanTabGenAsihorario.setEnabled(false);
      }
    }// Fin de if (evt.getButton() == MouseEvent.BUTTON3)
  }// GEN-LAST:event_panArtFotoMouseReleased
  
  public void cmdModificarEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      if(validarFormulario())
      {
        obtenerDatoFormulario();
        ejecutar("cmdModificarEvento", Peticion.AJAX);
      }
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      this.dispose();
    }
  }
  
  public void cmdEliminarEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      if(validarFormulario())
      {
        obtenerDatoFormulario();
        ejecutar("cmdEliminarEvento", Peticion.AJAX);
      }
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      iniciarFormulario();
    }
  }

  public void cmdAgregarAsihorarioEvento()
  {
    if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      if (validarFormulario())
      {
        obtenerDatoFormulario();
        ejecutar("cmdAgregarAsihorarioEvento", Peticion.AJAX);
      }
    }
    else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      if(pagGen.pagAct.getPaginaHijo().equals(pagGen.pagNew.getPaginaHijo()))
      {
        refrescarFormulario();
      }
//      this.dispose();
    }
  }
}