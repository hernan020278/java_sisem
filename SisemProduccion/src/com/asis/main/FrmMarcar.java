package com.asis.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import com.comun.entidad.Agrupacion;
import com.comun.entidad.Organizacion;
import com.comun.entidad.PerfilSeguridad;
import com.comun.entidad.Seguridad;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.MoverVentanaJComponent;
import com.comun.utilidad.Util;
import com.comun.utilidad.VentanaImagen;
import com.comun.utilidad.swing.PanelImgFoto;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatperInicio;
import com.sis.main.VistaListener;
import com.textil.controlador.ManejadorTextil;

public class FrmMarcar extends VistaAdatperInicio implements VistaListener {

	private JComponent contenedor;
	private JPanel panelPrincipal;

	// Variable del formulario Principal
	public FrmMarcar() {

		addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {

				// contenedor.update(contenedor.getGraphics());
			}
		});
		contenedor = new VentanaImagen(AplicacionGeneral.getInstance().obtenerImagen("fondo_rendimiento.png"));
		this.setUndecorated(true);
		this.setContentPane(contenedor);
//		AWTUtilities.setWindowOpaque(this, false);
        this.setOpacity(1.0f);
		this.getRootPane().setOpaque(false);
		MoverVentanaJComponent moverVentana = new MoverVentanaJComponent(contenedor);
		this.addMouseListener(moverVentana);
		this.addMouseMotionListener(moverVentana);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		this.getContentPane().setLayout(new BorderLayout());
		this.setSize(new java.awt.Dimension(810, 597));
		this.setLocationRelativeTo(null);
		initComponents();
	}

	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setOpaque(false);
		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		panelPrincipal = new JPanel();
		panelPrincipal.setLayout(null);
		panelPrincipal.setOpaque(false);
		super.setTitle("Sistema de Control Produccion");
		getContentPane().add(panelPrincipal);
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_down.png")); // NOI18N
		cmdCerrar.setBorderPainted(false);
		cmdCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdCerrar.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_down.png")); // NOI18N
		cmdCerrar.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("salir_up.png")); // NOI18N
		cmdCerrar.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				cmdCerrarEvento();
			}
		});
		cmdInicio = new javax.swing.JButton();
		cmdInicio.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdInicioEvento();
			}
		});
		cmdInicio.setOpaque(false);
		cmdInicio.setBounds(10, 490, 100, 100);
		panelPrincipal.add(cmdInicio);
		cmdInicio.setIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_down.png")); // NOI18N
		cmdInicio.setBorderPainted(false);
		cmdInicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdInicio.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_down.png")); // NOI18N
		cmdInicio.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("inicio_up.png")); // NOI18N
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.setBounds(700, 490, 100, 100);
		JSeparator separator = new JSeparator();
		separator.setBounds(261, 444, 339, 8);
		panelPrincipal.add(separator);
		etiMuestraLugar = new JLabel();
		etiMuestraLugar.setBounds(261, 449, 153, 28);
		panelPrincipal.add(etiMuestraLugar);
		etiMuestraLugar.setText("Ciudad Arequipa");
		etiMuestraLugar.setForeground(new Color(153, 102, 0));
		etiMuestraLugar.setFont(new Font("Bookman Old Style", Font.PLAIN, 18));
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(261, 479, 339, 10);
		panelPrincipal.add(separator_1);
		etiMuestraCantidad = new JLabel();
		etiMuestraCantidad.setBounds(424, 449, 176, 28);
		panelPrincipal.add(etiMuestraCantidad);
		etiMuestraCantidad.setText("Total Encuestados : 50");
		etiMuestraCantidad.setHorizontalAlignment(SwingConstants.RIGHT);
		etiMuestraCantidad.setForeground(new Color(153, 102, 0));
		etiMuestraCantidad.setFont(new Font("Bookman Old Style", Font.PLAIN, 18));
		etiUsuarioNombre = new JLabel();
		etiUsuarioNombre.setBounds(261, 488, 339, 28);
		panelPrincipal.add(etiUsuarioNombre);
		etiUsuarioNombre.setText("Psicologo : Hernan Mendoza");
		etiUsuarioNombre.setForeground(new Color(153, 51, 0));
		etiUsuarioNombre.setFont(new Font("Bookman Old Style", Font.PLAIN, 18));
		etiUsuarioUdf1 = new JLabel();
		etiUsuarioUdf1.setBounds(261, 519, 339, 28);
		panelPrincipal.add(etiUsuarioUdf1);
		etiUsuarioUdf1.setText("Colegiado : 221312321");
		etiUsuarioUdf1.setForeground(new Color(0, 51, 51));
		etiUsuarioUdf1.setFont(new Font("Bookman Old Style", Font.PLAIN, 18));
		separator_2 = new JSeparator();
		separator_2.setBounds(261, 550, 339, 10);
		panelPrincipal.add(separator_2);
		panMenuUsuario = new javax.swing.JPanel();
		panMenuUsuario.setOpaque(false);
		panMenuUsuario.setLayout(null);
		panMenuInicio = new JPanel();
		panMenuInicio.setOpaque(false);
		panMenuInicio.setLayout(null);
		panMenuInicio.setBounds(0, 0, 810, 597);
		panelPrincipal.add(panMenuInicio);
		cmdComponentes = new JButton();
		cmdComponentes.setMargin(new Insets(0, 0, 0, 0));
		cmdComponentes.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdHorarioEvento();
			}
		});
		cmdComponentes.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdComponentes.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("admin_especialista_down.png"));
		cmdComponentes.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("admin_especialista_up.png"));
		cmdComponentes.setIcon(AplicacionGeneral.getInstance().obtenerImagen("admin_especialista_down.png"));
		cmdComponentes.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdComponentes.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdComponentes.setText("Horario");
		cmdComponentes.setOpaque(false);
		cmdComponentes.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdComponentes.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdComponentes.setBorderPainted(false);
		cmdComponentes.setBounds(62, 354, 140, 130);
		panMenuInicio.add(cmdComponentes);
		cmdUsuarios = new JButton();
		cmdUsuarios.setMargin(new Insets(0, 0, 0, 0));
		cmdUsuarios.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdUsuariosEvento();
			}
		});
		cmdUsuarios.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdUsuarios.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("especialista_down.png"));
		cmdUsuarios.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("especialista_up.png"));
		cmdUsuarios.setIcon(AplicacionGeneral.getInstance().obtenerImagen("especialista_down.png"));
		cmdUsuarios.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdUsuarios.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdUsuarios.setToolTipText("");
		cmdUsuarios.setText("Usuarios");
		cmdUsuarios.setOpaque(false);
		cmdUsuarios.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdUsuarios.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdUsuarios.setBorderPainted(false);
		cmdUsuarios.setBounds(631, 354, 140, 130);
		panMenuInicio.add(cmdUsuarios);
		cmdTablaGeneral = new JButton();
		cmdTablaGeneral.setMargin(new Insets(0, 0, 0, 0));
		cmdTablaGeneral.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdTablaGeneral.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdCtrlAsistenciaEvento();
			}
		});
		cmdTablaGeneral.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("casos_down.png"));
		cmdTablaGeneral.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("casos_up.png"));
		cmdTablaGeneral.setIcon(AplicacionGeneral.getInstance().obtenerImagen("casos_down.png"));
		cmdTablaGeneral.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdTablaGeneral.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdTablaGeneral.setText("Control Asistencia");
		cmdTablaGeneral.setOpaque(false);
		cmdTablaGeneral.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdTablaGeneral.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdTablaGeneral.setBorderPainted(false);
		cmdTablaGeneral.setBounds(62, 198, 140, 130);
		panMenuInicio.add(cmdTablaGeneral);
		
		cmdReportes = new JButton();
		cmdReportes.setMargin(new Insets(0, 0, 0, 0));
		cmdReportes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdReporteFichasEvento();
			}
		});
		cmdReportes.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar_down.png"));
		cmdReportes.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar_up.png"));
		cmdReportes.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar_down.png"));
		cmdReportes.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdReportes.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdReportes.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdReportes.setText("Reportes");
		cmdReportes.setOpaque(false);
		cmdReportes.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdReportes.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdReportes.setBorderPainted(false);
		cmdReportes.setBounds(62, 32, 140, 130);
		panMenuInicio.add(cmdReportes);
		panelLogo = new PanelImgFoto();
		panelLogo.setImagen(AplicacionGeneral.getInstance().obtenerImagen("logo_rendimiento.png"), null, null, "CONSTRAINT");
		panelLogo.setBounds(283, 165, 278, 270);
		panMenuInicio.add(panelLogo);
		
		cmdAsistencia = new JButton();
		cmdAsistencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdAsistenciaEvento();
			}
		});
		cmdAsistencia.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdAsistencia.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdAsistencia.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAsistencia.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("reporte_down.png"));
		cmdAsistencia.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("reporte_up.png"));
		cmdAsistencia.setIcon(AplicacionGeneral.getInstance().obtenerImagen("reporte_down.png"));
		cmdAsistencia.setToolTipText("");
		cmdAsistencia.setText("Asistencia");
		cmdAsistencia.setOpaque(false);
		cmdAsistencia.setMargin(new Insets(0, 0, 0, 0));
		cmdAsistencia.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdAsistencia.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdAsistencia.setBorderPainted(false);
		cmdAsistencia.setBounds(631, 198, 140, 130);
		panMenuInicio.add(cmdAsistencia);
		
		cmdAreas = new JButton();
		cmdAreas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdAreasEvento(e);
			}

		});
		cmdAreas.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdAreas.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdAreas.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAreas.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonProcesoDown.png"));
		cmdAreas.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonProcesoUp.png"));
		cmdAreas.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonProcesoDown.png"));
		cmdAreas.setText("Areas");
		cmdAreas.setOpaque(false);
		cmdAreas.setMargin(new Insets(0, 0, 0, 0));
		cmdAreas.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdAreas.setFont(new Font("Dialog", Font.PLAIN, 14));
		cmdAreas.setBorderPainted(false);
		cmdAreas.setBounds(631, 32, 140, 130);
		panMenuInicio.add(cmdAreas);
		panelPrincipal.add(panMenuUsuario);
		panMenuUsuario.setBounds(0, 0, 810, 597);
		setSize(new java.awt.Dimension(810, 597));
		setLocationRelativeTo(null);
	}// </editor-fold>//GEN-END:initComponents

	public void cmdCerrarEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			int resMsg = JOptionPane.showConfirmDialog(this, "���DESEA SALIR DEL DEL SISTEMA !!!", "Sistema de ", JOptionPane.YES_NO_OPTION);
			if(resMsg == JOptionPane.YES_OPTION)
			{
				ejecutar("cmdCerrarEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			System.exit(0);
		}
//		this.dispose();
//		
//		ManejadorTextil man = new ManejadorTextil();
//		
//		man.org = new Organizacion();
//		man.agru = new Agrupacion();
//		man.sec = new Seguridad();
//		man.perfSec = new PerfilSeguridad();
//		man.usuSes = new Usuario();
//		
//		Util.getInst().limpiarEntidad(man.org);
//		Util.getInst().limpiarEntidad(man.agru);
//		Util.getInst().limpiarEntidad(man.sec);
//		Util.getInst().limpiarEntidad(man.perfSec);
//		Util.getInst().limpiarEntidad(aux.usuSes);		
//		man.org.setObjeto(aux.org);
//		man.usuSes.setObjeto(aux.usuSes);
//		man.agru.setObjeto(aux.agru);
//		man.ejecutarModulo(OrganizacionGeneral.getOrgcodigo());
		
	}// GEN-LAST:event_cmdCerrarActionPerformed

	public void cmdInicioEvento()
	{

		panMenuInicio.setVisible(true);
		panMenuUsuario.setVisible(false);
	}
	
	public void cmdUsuariosEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdUsuariosEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}

	public void cmdAsistenciaEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdAsistenciaEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting cFrmPrincipal">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName()))
				{
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(FrmMarcar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(FrmMarcar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(FrmMarcar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(FrmMarcar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		/*
		 * Create and display the form
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				new FrmMarcar().setVisible(true);
			}
		});
	}

	private javax.swing.JButton cmdCerrar;
	private javax.swing.JButton cmdInicio;
	private javax.swing.JPanel panMenuUsuario;
	private JPanel panMenuInicio;
	private JButton cmdComponentes;
	private JButton cmdUsuarios;
	private JSeparator separator_2;
	private JLabel etiMuestraLugar;
	private JLabel etiMuestraCantidad;
	private JLabel etiUsuarioNombre;
	private PanelImgFoto panelLogo;
	private JLabel etiUsuarioUdf1;
	private JButton cmdTablaGeneral;
	private JButton cmdReportes;
	private JButton cmdAsistencia;
	private JButton cmdAreas;

	/*************************************
	 * METODOS DE LA INTERFAZ DE VISTA
	 *************************************/
	@Override
	public void iniciarFormulario() {

		frmAbierto = false;
		aux = Auxiliar.getInstance();
		pagGen = PaginaGeneral.getInstance();
		paginaHijo = this.getClass().getSimpleName();
		pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
		if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
		{
			String panelInicio = "PANEL_INICIO";
			if(peticion.containsKey("panelActivo"))
			{
				panelInicio = (String) peticion.get("panelActivo");
			}
			if(panelInicio.equals("PANEL_USUARIO"))
			{
				panMenuInicio.setVisible(false);
				panMenuUsuario.setVisible(true);
			}
			else if(panelInicio.equals("PANEL_PSICOLOGO"))
			{
				panMenuInicio.setVisible(false);
				panMenuUsuario.setVisible(false);
			}
			else if(panelInicio.equals("PANEL_INDICADOR"))
			{
				panMenuInicio.setVisible(false);
				panMenuUsuario.setVisible(false);
			}
			else
			{
				panMenuInicio.setVisible(true);
				panMenuUsuario.setVisible(false);
			}
			limpiarFormulario();
			llenarFormulario();
			refrescarFormulario();
			super.setVisible(true);
			cmdAsistenciaEvento();
		}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
		else if(pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR)) {
			llenarFormulario();
		}
	}

	@Override
	public void limpiarFormulario()
	{

		String ruta = AplicacionGeneral.getInstance(OrganizacionGeneral.getOrgcodigo()).obtenerRutaImagenes();
		ImageIcon imgIco = new ImageIcon(ruta + "logo.png");
		panelLogo.setImagen(imgIco, null, null, "CONSTRAINT");
		etiMuestraLugar.setText("");
		etiMuestraCantidad.setText("");
		etiUsuarioNombre.setText("");
		etiUsuarioUdf1.setText("");
	}

	@Override
	public void llenarFormulario()
	{

	}

	@Override
	public void activarFormulario(boolean activo) {

		// TODO Auto-generated method stub
	}

	@Override
	public void refrescarFormulario() {
		
//		if(!aux.agru.getSegcodigo().equals("ADMIN"))
//		{
//			cmdPedidos.setVisible(false);	
//			cmdControlComponente.setVisible(false);
//			cmdComponentes.setVisible(false);
//			cmdUsuarios.setVisible(false);
//		}
//		else
//		{
			cmdTablaGeneral.setVisible(true);	
//			cmdControlComponente.setVisible(true);
			cmdComponentes.setVisible(true);
			cmdUsuarios.setVisible(true);
			
//		}
	}

	@Override
	public void obtenerDatoFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		boolean validado = true;
		return validado;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

		// TODO Auto-generated method stub
	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return null;
	}
	
	private void cmdAreasEvento(ActionEvent e) {

		this.dispose();
		
		ManejadorTextil man = new ManejadorTextil();
		
		man.org = new Organizacion();
		man.agru = new Agrupacion();
		man.sec = new Seguridad();
		man.perfSec = new PerfilSeguridad();
		man.usuSes = new Usuario();
		
		Util.getInst().limpiarEntidad(man.org);
		Util.getInst().limpiarEntidad(man.agru);
		Util.getInst().limpiarEntidad(man.sec);
		Util.getInst().limpiarEntidad(man.perfSec);
		Util.getInst().limpiarEntidad(aux.usuSes);		
		man.org.setObjeto(aux.org);
		man.usuSes.setObjeto(aux.usuSes);
		man.agru.setObjeto(aux.agru);
		man.ejecutarModulo(OrganizacionGeneral.getOrgcodigo());

		
	}
	

	public void cmdReporteFichasEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdReporteFichasEvento", Peticion.AJAX);
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}// GEN-LAST:event_cmdConsultarEvento
	/*
	 * METODOS GENERADOS POR EL USUARIO
	 */
	public void cmdCtrlAsistenciaEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCtrlAsistenciaEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}
	

	public void cmdHorarioEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdHorarioEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}
}
