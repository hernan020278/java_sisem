package com.asis.main;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import com.browse.Browse;
import com.browse.BrowseColumn;
import com.browse.BrowseFilter;
import com.browse.BrowseObject;
import com.browse.BrowseUtility;
import com.comun.entidad.ObjetoNodo;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.CeldaRenderGeneral;
import com.comun.utilidad.Util;
import com.sis.browse.CellEditDecimalTextInput;
import com.sis.browse.CellEditJButton;
import com.sis.browse.CellRenderJButton;
import com.sis.browse.ModeloTablaGeneral;
import com.sis.browse.PanelTablaGeneral;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;
import javax.swing.JInternalFrame;

public class DlgTablaGeneralBrowse extends VistaAdatper implements VistaListener {

	private JPanel panelPrincipal;
	private JPanel panelContenido;
	private JSplitPane splitPane;
	private JScrollPane scrTree;
	private DefaultMutableTreeNode treeNivel1 = null;
	private DefaultMutableTreeNode treeNivel2 = null;
	private DefaultMutableTreeNode treeNivel3 = null;
	private DefaultMutableTreeNode treeNivel4 = null;
	private DefaultMutableTreeNode treeNivel5 = null;
	private DefaultMutableTreeNode treeNivel6 = null;
	private JTree tree;
	private DefaultMutableTreeNode raiz;
	private DefaultMutableTreeNode nodoActual;
	private ObjetoNodo objetoNodoActual;
	private JPopupMenu popMenTree;
	private JMenuItem menLlenarNodo;
	private JMenuItem menLimpiarNodo;
	private JMenuItem menRefrescarNodo;
	private JButton cmdCerrar;
	private JLabel etiAccion;
	private JProgressBar barraProgreso;
	private List listaFiltroColumna = new ArrayList();
	private List listaFiltroTipo = new ArrayList();
	private List listaFiltroOperador = new ArrayList();
	private List listaFiltroValor = new ArrayList();
	private List listaFiltroLogico = new ArrayList();
	private List listaFiltroOrden = new ArrayList();
	private int modelo, pedido;
	private String talla;
	private ModeloTablaGeneral modTabFicCtrl;
	private TableColumn[] colTabFicCtrl;
	private BrowseColumn[] browseColumnsFichaControl;
	private CeldaRenderGeneral celdaRenderGeneral;
	private JPanel panelTablaBrowse;
	private JPopupMenu popMenBrowse;
	private JMenuItem menAgregar;
	private JMenuItem menModificar;
	private JMenuItem menEliminar;
	private PanelTablaGeneral panTabGenBrowse;

	public DlgTablaGeneralBrowse(FrmInicio parent) {

		super(parent);
		setTitle("Administracion de las Areas");
		setResizable(false);
		initComponents();
		
		popMenBrowse = new JPopupMenu();

		menAgregar = new JMenuItem("Agregar Registro");
		menAgregar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				cmdAgregarEvento();
			}
		});
		menModificar = new JMenuItem("Modificar Registro");
		menModificar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
			}
		});
		menEliminar = new JMenuItem("Eliminar Registro");
		menEliminar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
			}
		});
		popMenBrowse.add(menAgregar);
		popMenBrowse.add(menModificar);
		popMenBrowse.add(menEliminar);
		
		popMenTree = new JPopupMenu();
		menLlenarNodo = new JMenuItem("Llenar Nodo");
		menLlenarNodo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				menLlenarNodoEvento(null);
			}
		});
		menLimpiarNodo = new JMenuItem("Limpiar Nodo");
		menLimpiarNodo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				DlgControlHorario dlgCtrlHor = new DlgControlHorario(null);
				panelTablaBrowse.add(dlgCtrlHor);
				dlgCtrlHor.setVisible(true);
			}
		});
		menRefrescarNodo = new JMenuItem("Refrescar Nodo");
		menRefrescarNodo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				menRefrescarNodoEvento(null);
			}
		});
		popMenTree.add(menLlenarNodo);
		popMenTree.add(menLimpiarNodo);
		popMenTree.add(menRefrescarNodo);
		super.addWindowListener(new WindowAdapter() {

			@Override
			public void windowActivated(WindowEvent we) {

				frmAbierto = true;
			}
		});
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	private void initComponents() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);
		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);
		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 1089, 599);
		splitPane = new JSplitPane();
		splitPane.setBounds(10, 11, 1067, 533);
		splitPane.setDividerLocation(0.2);
		panelPrincipal.add(splitPane);
		raiz = new DefaultMutableTreeNode("Operaciones");
		tree = new JTree(raiz);
		tree.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tree.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {

				treeMousePressedEvento(e);
			}
		});
		tree.addTreeSelectionListener(new TreeSelectionListener() {

			public void valueChanged(TreeSelectionEvent e) {

				treeValueChanged(e);
			}
		});
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setBounds(0, 0, 72, 64);
		scrTree = new JScrollPane(tree);
		scrTree.setBounds(0, 0, 594, 582);
		splitPane.setLeftComponent(scrTree);
		panelContenido = new JPanel();
		splitPane.setRightComponent(panelContenido);
		panelContenido.setLayout(null);
		
		panelTablaBrowse = new JPanel();
		panelTablaBrowse.setLayout(null);
		panelTablaBrowse.setBorder(null);
		panelTablaBrowse.setBounds(0, 0, 849, 531);
		panelContenido.add(panelTablaBrowse);
		cmdCerrar = new JButton();
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				cmdCerrarEvento();
			}
		});
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setName("Agregar");
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.setBounds(975, 548, 104, 40);
		panelPrincipal.add(cmdCerrar);
		etiAccion = new JLabel();
		etiAccion.setText("Eventos en espera");
		etiAccion.setHorizontalAlignment(SwingConstants.CENTER);
		etiAccion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		etiAccion.setBounds(10, 548, 213, 21);
		panelPrincipal.add(etiAccion);
		barraProgreso = new JProgressBar();
		barraProgreso.setFont(new Font("Tahoma", Font.PLAIN, 16));
		barraProgreso.setBounds(10, 568, 213, 20);
		panelPrincipal.add(barraProgreso);
		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);
		setTitle(this.getClass().getSimpleName());
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the
		 * default look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DlgTablaGeneralBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DlgTablaGeneralBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DlgTablaGeneralBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DlgTablaGeneralBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		/*
		 * Create and display the dialog
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				DlgTablaGeneralBrowse dialog = new DlgTablaGeneralBrowse(null);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {

					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {

						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	/******************************************************
	 * FUNCTIONES DE LA INTERFAZ VISTALISTENER
	 ******************************************************/
	@Override
	public void iniciarFormulario() {

		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
			{
				limpiarFormulario();
				crearExplorador();
				refrescarFormulario();
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			else if(pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
				browse = (Browse) peticion.get("browse");
				browseObject = (BrowseObject) peticion.get("browseObject");
				pagGen.pagAct.setBrowseTabla(browse.getBrowseTabla());
				filters = browseObject.getBrowseFilters();
				browseColumns = browseObject.getBrowseColumns();
				nodoNivel = (peticion.containsKey("nodoNivel") ? (Integer) peticion.get("nodoNivel") : 0);
				if(pagGen.pagAct.getModo().equals(Modo.ELIMINANDO) || pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
				{
					refrescarRegistros();
				}
				else
				{
					llenarPanelExplorador();
					obtenerEntidadObjetoNodoActualEvento();
					llenarPanelTablaGeneral();
					refrescarFormulario();
				}
			}
			frmAbierto = true;
			if(!this.isVisible()){this.setVisible(true);}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void crearExplorador()
	{

		DefaultTreeModel treeModelo = (DefaultTreeModel) tree.getModel();
//		if(treeNivel1 == null)
//		{	
			raiz.removeAllChildren();
			treeNivel1 = new DefaultMutableTreeNode(new ObjetoNodo(null, "Horario de Trabajo", null, "hor-areas"));
			raiz.add(treeNivel1);
			treeNivel1 = new DefaultMutableTreeNode(new ObjetoNodo(null, "Componentes por Modelo", null, "pedcmp-pedmod"));
			raiz.add(treeNivel1);
			treeModelo.reload(raiz);
//			if(panTabGenBrowse.modeloTablaGeneral != null){panTabGenBrowse.modeloTablaGeneral.setRowCount(0);}
//		}
	}

	@Override
	public void limpiarFormulario()
	{
		if(modTabFicCtrl != null){modTabFicCtrl.setRowCount(0);}
	}

	@Override
	public void llenarFormulario(){}

	@Override
	public void activarFormulario(boolean activo){
		tree.setEnabled(activo);
		panTabGenBrowse.tablaBrowse.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario()
	{

		if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			llenarFormulario();
			activarFormulario(true);
			refrescarPanelTablaGeneral();
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			desactivarComponentes();
			if(browseObject.getBrowseTabla().equals("pedfic-pedmod"))
			{
//				tabFicCtrl.setEnabled(true);
			}
			else
			{
				panTabGenBrowse.tablaBrowse.setEnabled(true);	
			}
		}
		else if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
			desactivarComponentes();
			cmdCerrar.setEnabled(true);
		}
	}

	@Override
	public void obtenerDatoFormulario()
	{

		peticion.put("Pagina_paginaHijo", paginaHijo);
		peticion.put("Pagina_paginaPadre", paginaPadre);
		peticion.put("Pagina_evento", paginaEvento);
		peticion.put("Pagina_accion", paginaAccion);
		peticion.put("Pagina_valor", paginaValor);
		peticion.put("currentPage", String.valueOf(browse.getCurrentPage()));
		peticion.put("pageSize", String.valueOf(browse.getPageSize()));
		peticion.put("nodoNivel", nodoActual.getLevel());
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			return true;
		}
		else if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return barraProgreso;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return etiAccion;
	}

	private void treeValueChanged(TreeSelectionEvent e)
	{
		nodoActual = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		if(nodoActual!= null && nodoActual.getLevel() > 1)
		{
			if(browse != null)
			{
				browse.setCurrentPage(1);
			}
			refrescarRegistros();
		}
	}

	public void refrescarRegistros()
	{

		if(nodoActual == null)
		{
			return;
		}
		// System.out.println("Nivel : " + nodoActual.getLevel() + " Indice : " + nodoActual);
		if(nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
		{
			objetoNodoActual = (ObjetoNodo) nodoActual.getUserObject();
			if(objetoNodoActual.getBrowseTabla().equals("pedfic-pedmod-prd-tal-fic"))
			{
//				tabPanTablas.setSelectedIndex(1);
				String[] valor = objetoNodoActual.getNodoDescripcion().split(" - ");
				
				pedido = Integer.parseInt(valor[1]);
				modelo = Integer.parseInt(valor[3]);
				
//				recuperarRegistroParaFicControlEvento();
//				refrescarFormulario();
				
			}//if(objetoNodoActual.getBrowseTabla().equals("pedfic-pedmod"))
			else
			{
//				tabPanTablas.setSelectedIndex(0);
				if(browse == null)
				{
					browse = new Browse();
					browse.setCurrentPage(1);
					browse.setPageSize(10);
				}
				if(!Util.isEmpty(objetoNodoActual.getBrowseTabla()))
				{
					pagGen.pagAct.setBrowseTabla(objetoNodoActual.getBrowseTabla());
					nodoActual.removeAllChildren();
					recuperarRegistrosDeBrowseTablaEvento();
				}
			}
		}
	}

	public void treeValueChangedEvento()
	{
		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();

			ejecutar("treeValueChangedEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}
	
	public void recuperarRegistrosDeBrowseTablaEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			obtenerDatosObjetoNodo();
			ejecutar("recuperarRegistrosDeBrowseTablaEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	public void obtenerDatosObjetoNodo()
	{
		obtenerFiltrosBusqueda();
		obtenerDatoFormulario();
	}

	public void obtenerFiltrosBusqueda()
	{

		obtenerObjetoNodoActualFiltrosBusqueda();
		obtenerPeticionFiltrosBusqueda();
	}

	public void obtenerObjetoNodoActualFiltrosBusqueda()
	{

		if(nodoActual == null)
		{
			return;
		}
		listaFiltroColumna.clear();
		listaFiltroTipo.clear();
		listaFiltroOperador.clear();
		listaFiltroValor.clear();
		listaFiltroLogico.clear();
		listaFiltroOrden.clear();
		if(nodoActual.getRoot().getChildCount() >= 0)
		{
			if(nodoActual.getUserObject() instanceof ObjetoNodo)
			{
				ObjetoNodo objetoNodoTmp = (ObjetoNodo) nodoActual.getUserObject();
				// System.out.println("Nodo : " + objetoNodoTmp.getNodoDescripcion());
				if(objetoNodoTmp.getBrowseFilters() != null)
				{
					int numFil = objetoNodoTmp.getBrowseFilters().size();
					// for(int iteFil = 0; iteFil < numFil; iteFil++)
					// {
					// BrowseFilter filter = (BrowseFilter) objetoNodoTmp.getBrowseFilters().get(iteFil);
					// listaFiltroColumna.add(filter.getColumnName());
					// listaFiltroTipo.add(filter.getType());
					// listaFiltroOperador.add(filter.getOperator());
					// listaFiltroValor.add(filter.getValue());
					// listaFiltroLogico.add(filter.getLogicalOperator());
					// listaFiltroOrden.add(filter.getSort());
					// }
					if(objetoNodoTmp.getNodoCodigo() != null && objetoNodoTmp.getNodoCodigo().size() > 0)
					{
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroColumna, "filtroColumna");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroTipo, "filtroTipo");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroOperador, "filtroOperador");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroValor, "filtroValor");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroLogico, "filtroLogico");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroOrden, "filtroOrden");
					}
				}
			}// if(nodeInfo instanceof ObjetoNodo)
		}
	}

	public void obtenerObjetoNodoFiltrosBusqueda()
	{

		if(nodoActual == null)
		{
			return;
		}
		listaFiltroColumna.clear();
		listaFiltroTipo.clear();
		listaFiltroOperador.clear();
		listaFiltroValor.clear();
		listaFiltroLogico.clear();
		listaFiltroOrden.clear();
		if(nodoActual.getRoot().getChildCount() >= 0)
		{
			Enumeration iteEnuNod = nodoActual.preorderEnumeration();
			while (iteEnuNod.hasMoreElements())
			{
				DefaultMutableTreeNode nodoTmp = (DefaultMutableTreeNode) iteEnuNod.nextElement();
				if(nodoTmp.getUserObject() instanceof ObjetoNodo)
				{
					ObjetoNodo objetoNodoTmp = (ObjetoNodo) nodoTmp.getUserObject();
					System.out.println("Nodo : " + objetoNodoTmp.getNodoDescripcion());
					if(objetoNodoTmp.getBrowseFilters() != null)
					{
						int numFil = objetoNodoTmp.getBrowseFilters().size();
						for(int iteFil = 0; iteFil < numFil; iteFil++)
						{
							BrowseFilter filter = (BrowseFilter) objetoNodoTmp.getBrowseFilters().get(iteFil);
							listaFiltroColumna.add(filter.getColumnName());
							listaFiltroTipo.add(filter.getType());
							listaFiltroOperador.add(filter.getOperator());
							listaFiltroValor.add(filter.getValue());
							listaFiltroLogico.add(filter.getLogicalOperator());
							listaFiltroOrden.add(filter.getSort());
						}
						if(objetoNodoTmp.getNodoCodigo() != null && objetoNodoTmp.getNodoCodigo().size() > 0)
						{
							obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroColumna, "filtroColumna");
							obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroTipo, "filtroTipo");
							obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroOperador, "filtroOperador");
							obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroValor, "filtroValor");
							obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroLogico, "filtroLogico");
							obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroOrden, "filtroOrden");
						}
					}
					if(nodoActual.equals(nodoTmp))
					{
						break;
					}
				}// if(nodeInfo instanceof ObjetoNodo)
			}
		}
	}

	private void obtenerNodoFiltroCodigo(ObjetoNodo objetoNodoTmp, List listaFiltro, String tipoFiltro)
	{

		List lista = (List) objetoNodoTmp.getNodoCodigo().get(tipoFiltro);
		if(lista != null && lista.size() > 0)
		{
			for(int iteLis = 0; iteLis < lista.size(); iteLis++)
			{
				String filtro = (String) lista.get(iteLis);
				listaFiltro.add(filtro);
			}
		}
	}

	public void obtenerPeticionFiltrosBusqueda()
	{

		if(listaFiltroColumna != null && listaFiltroColumna.size() > 0)
		{
			int numFil = listaFiltroColumna.size();
			String[] filtroColumna = new String[numFil];
			String[] filtroTipo = new String[numFil];
			String[] filtroOperador = new String[numFil];
			String[] filtroValor = new String[numFil];
			String[] filtroLogico = new String[numFil];
			String[] filtroOrden = new String[numFil];
			String[] filtroOriginal = new String[numFil];
			for(int iteFil = 0; iteFil < numFil; iteFil++)
			{
				filtroColumna[iteFil] = (String) listaFiltroColumna.get(iteFil);
				filtroTipo[iteFil] = (String) listaFiltroTipo.get(iteFil);
				filtroOperador[iteFil] = (String) listaFiltroOperador.get(iteFil);
				filtroValor[iteFil] = (String) listaFiltroValor.get(iteFil);
				filtroLogico[iteFil] = (String) listaFiltroLogico.get(iteFil);
				filtroOrden[iteFil] = (String) listaFiltroOrden.get(iteFil);
			}
			peticion.put("filtroColumna", filtroColumna);
			peticion.put("filtroTipo", filtroTipo);
			peticion.put("filtroOperador", filtroOperador);
			peticion.put("filtroValor", filtroValor);
			peticion.put("filtroLogico", filtroLogico);
			peticion.put("filtroOrden", filtroOrden);
			peticion.put("filtroOriginal", filtroOriginal);
		}
	}

	public void obtenerEntidadObjetoNodoActualEvento()
	{

		nodoActual = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		if(nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
		{
			objetoNodoActual = (ObjetoNodo) nodoActual.getUserObject();
			if(objetoNodoActual.getNodoCodigo() != null && objetoNodoActual.getNodoCodigo().size() > 0)
			{
				List listaFiltroLink = (List) objetoNodoActual.getNodoCodigo().get("filtroLink");
				for(int iteLis = 0; iteLis < listaFiltroLink.size(); iteLis++)
				{
					String filtroLink = (String) listaFiltroLink.get(iteLis);
					if(filtroLink.contains(","))
					{
						String[] link = filtroLink.split(",");
						paginaHijo = (link[0] != null) ? link[0] : "";
						paginaPadre = pagGen.pagAct.getPaginaHijo();
						paginaEvento = (link[1] != null) ? link[1] : "";
						paginaAccion = (link[2] != null) ? link[2] : "";
						paginaValor = (link[3] != null) ? link[3] : "";
						ejecutarAccion(paginaHijo, paginaEvento, paginaAccion, paginaValor);
					}// if (filtroLink.contains(","))
				}// for (int iteLis = 0; iteLis < listaFiltroLink.size(); iteLis++)
			}// if(objetoNodoActual.getNodoCodigo() != null && objetoNodoActual.getNodoCodigo().size() > 0)
		}// if (nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
	}

	public void cmdObtenerEntidadEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			peticion.put("Pagina_paginaHijo", paginaHijo);
			peticion.put("Pagina_paginaPadre", paginaPadre);
			peticion.put("Pagina_evento", paginaEvento);
			peticion.put("Pagina_accion", paginaAccion);
			peticion.put("Pagina_valor", paginaValor);
			ejecutar("cmdObtenerEntidadEvento", Peticion.SUBMIT);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			limpiarFormulario();
			llenarFormulario();
		}
	}

	public void cmdEliminarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				pagGen.pagAct.setModo(Modo.ELIMINANDO);
				obtenerDatosObjetoNodo();
				ejecutar("cmdEliminarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	public void cmdActualizarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				pagGen.pagAct.setModo(Modo.ELIMINANDO);
				obtenerDatosObjetoNodo();
				ejecutar("cmdActualizarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			iniciarFormulario();
		}
	}

	public void tabCelActualizarComponentePorModeloEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				peticion.put("prdcodigo", modelo);
				peticion.put("pedcodigo", pedido);

				pagGen.pagAct.setModo(Modo.ELIMINANDO);
				obtenerDatoFormulario();
				ejecutar("tabCelActualizarComponentePorModeloEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			refrescarFormulario();
			llenarRegistrosTablaFichaControl();
		}
	}
	
	public void cmdAgregarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				pagGen.pagAct.setModo(Modo.AGREGANDO);
				obtenerDatoFormulario();
				peticion.put("filtroNoInclude", "Y");
				ejecutar("cmdAgregarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			
		}
	}

	private void menLlenarNodoEvento(TreeExpansionEvent event)
	{
		if(nodoActual!= null && nodoActual.getLevel() == 1)
		{
			nodoActual.removeAllChildren();
			DefaultTreeModel treeModelo = (DefaultTreeModel) tree.getModel();
			treeModelo.reload(nodoActual);
			if(nodoActual.getUserObject() != null && nodoActual.getUserObject() instanceof ObjetoNodo)
			{
				objetoNodoActual = (ObjetoNodo) nodoActual.getUserObject();
				pagGen.pagAct.setBrowseTabla(objetoNodoActual.getBrowseTabla());
				treeValueChangedEvento();
			}			
		}//if(nodoActual.getLevel() == 1)
	}

	public void menLlenarNodoEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			obtenerDatosObjetoNodo();
			ejecutar("menLlenarNodoEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
		}
	}

	private void menRefrescarNodoEvento(TreeExpansionEvent event)
	{
		refrescarRegistros();
	}

	public void menRefrescarNodoEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			obtenerDatosObjetoNodo();
			ejecutar("menRefrescarNodoEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void llenarPanelExplorador()
	{

		DefaultTreeModel treeModelo = (DefaultTreeModel) tree.getModel();
		// System.out.println("Nivel : " + nodoActual.getLevel() + " Indice : " + nodoActual);
		if(nodoActual == null)
		{
			return;
		}
		List listaObjetos = obtenerListaObjetos();
		if(nodoActual.getChildCount() == 0)
		{
			if(listaObjetos != null)
			{
				for(int iteLis = 0; iteLis < listaObjetos.size(); iteLis++)
				{
					ObjetoNodo objetoNodo = (ObjetoNodo) listaObjetos.get(iteLis);
					if(nodoActual.getLevel() == 1)
					{
						treeNivel2 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel2);
					}
					if(nodoActual.getLevel() == 2)
					{
						treeNivel3 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel3);
					}
					if(nodoActual.getLevel() == 3)
					{
						treeNivel4 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel4);
					}
					if(nodoActual.getLevel() == 4)
					{
						treeNivel5 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel5);
					}
					if(nodoActual.getLevel() == 5)
					{
						treeNivel6 = new DefaultMutableTreeNode(objetoNodo);
						nodoActual.add(treeNivel6);
					}
				}// for (int iteLis = 1; iteLis <= 10; iteLis++)
			}// if(listaObjetos != null)
			treeModelo.reload(nodoActual);
		}// if(llenarTree)
	}

	public List obtenerListaObjetos()
	{

		List listaObjetos = new ArrayList();
		List listaRegistros = (List) browse.getPageResults();
		if(listaRegistros != null && listaRegistros.size() > 0)
		{
			for(int iteFil = 0; iteFil < listaRegistros.size(); iteFil++)
			{
				Map filtroCodigo = new HashMap();
				List nodoFiltroColumna = new ArrayList();
				List nodoFiltroTipo = new ArrayList();
				List nodoFiltroOperador = new ArrayList();
				List nodoFiltroValor = new ArrayList();
				List nodoFiltroLogico = new ArrayList();
				List nodoFiltroOrden = new ArrayList();
				List nodoFiltroLink = new ArrayList();
				String nodoCodigo = "";
				String nodoDescripcion = "";
				for(int iteRegCmp = 0; iteRegCmp < browseColumns.length; iteRegCmp++)
				{
					BrowseColumn browseColumn = browseColumns[iteRegCmp];
					Object[] listaCampos = (Object[]) listaRegistros.get(iteFil);
					Object objeto = (Object) listaCampos[iteRegCmp];
					if(objeto instanceof String) {
						nodoCodigo = (String) objeto;
					}
					else if(objeto instanceof Integer) {
						nodoCodigo = (String.valueOf((Integer) objeto));
					}
					if(!Util.isEmpty(browseColumn.getTreeRelationColumn()))
					{
						nodoFiltroColumna.add(browseColumn.getTreeRelationColumn());
						nodoFiltroTipo.add(browseColumn.getType());
						nodoFiltroOperador.add("=");
						nodoFiltroValor.add(nodoCodigo);
						nodoFiltroLogico.add("AND");
						nodoFiltroOrden.add("N");
					}
					if(!Util.isEmpty(browseColumn.getLink()) && browseColumn.getAlias().equalsIgnoreCase("OBTENER"))
					{
						nodoFiltroLink.add(obtenerValor(browseColumn, listaCampos));
					}
					if(browseColumn.getTreeDescription().equals("Y"))
					{
						if(objeto instanceof String)
						{
							nodoDescripcion = nodoDescripcion + (nodoDescripcion.equals("") ? "" : " - ") + (String) objeto;
						}
						if(objeto instanceof Integer)
						{
							nodoDescripcion = nodoDescripcion + (nodoDescripcion.equals("") ? "" : " - ") + (Integer) objeto;
						}
						if(objeto instanceof BigDecimal)
						{
							nodoDescripcion = nodoDescripcion + (nodoDescripcion.equals("") ? "" : " - ") + (BigDecimal) objeto;
						}
					}
				}
				filtroCodigo.put("filtroColumna", nodoFiltroColumna);
				filtroCodigo.put("filtroTipo", nodoFiltroTipo);
				filtroCodigo.put("filtroOperador", nodoFiltroOperador);
				filtroCodigo.put("filtroValor", nodoFiltroValor);
				filtroCodigo.put("filtroLogico", nodoFiltroLogico);
				filtroCodigo.put("filtroOrden", nodoFiltroOrden);
				filtroCodigo.put("filtroLink", nodoFiltroLink);
				listaObjetos.add(new ObjetoNodo(filtroCodigo, nodoDescripcion, browseObject.getBrowseFilters(), browseObject.getTreeRelation()));
			}
		}
		return listaObjetos;
	}

	public Object obtenerValor(BrowseColumn columna, Object objetoRegistro)
	{

		try
		{
			Object columnValue = BrowseUtility.getColumnObject(columna, objetoRegistro, "", browseColumns, browse.getBrowseId(), "");
			String columnResult = "";
			if(columna.getLink() != null && columna.getLink().length() > 0)
			{
				columnResult = BrowseUtility.getColumnLinkObject(columna, objetoRegistro, browseColumns);
				columnResult = columnResult.replace("javascript: abrirPagina(", "");
				columnResult = columnResult.replace("'", "");
				columnResult = columnResult.replace(");", "");
			}
			columnValue = (columnResult.equals("")) ? columnValue : columnResult;
			return columnValue;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	private void treeMousePressedEvento(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_panArtFotoMousePressed

		if(evt.getButton() == MouseEvent.BUTTON3) {
			popMenTree.show(evt.getComponent(), evt.getX(), evt.getY());
		}// Fin de if (evt.getButton() == MouseEvent.BUTTON3)
	}// GEN-LAST:event_panArtFotoMousePressed

	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void obtenerObjetoNodoFiltrosCodigo()
	{

		if(nodoActual == null)
		{
			return;
		}
		listaFiltroColumna.clear();
		listaFiltroTipo.clear();
		listaFiltroOperador.clear();
		listaFiltroValor.clear();
		listaFiltroLogico.clear();
		listaFiltroOrden.clear();
		if(nodoActual.getRoot().getChildCount() >= 0)
		{
			Enumeration iteEnuNod = nodoActual.preorderEnumeration();
			while (iteEnuNod.hasMoreElements())
			{
				DefaultMutableTreeNode nodoTmp = (DefaultMutableTreeNode) iteEnuNod.nextElement();
				if(nodoTmp.getUserObject() instanceof ObjetoNodo)
				{
					ObjetoNodo objetoNodoTmp = (ObjetoNodo) nodoTmp.getUserObject();
					System.out.println("Nodo : " + objetoNodoTmp.getNodoDescripcion());
					if(objetoNodoTmp.getNodoCodigo() != null && objetoNodoTmp.getNodoCodigo().size() > 0)
					{
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroColumna, "filtroColumna");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroTipo, "filtroTipo");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroOperador, "filtroOperador");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroValor, "filtroValor");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroLogico, "filtroLogico");
						obtenerNodoFiltroCodigo(objetoNodoTmp, listaFiltroOrden, "filtroOrden");
					}
					if(nodoActual.equals(nodoTmp))
					{
						break;
					}
				}// if(nodeInfo instanceof ObjetoNodo)
			}
		}
	}

	public void llenarPanelTablaGeneral()
	{
		crearModeloTabla(browseObject.getBrowseColumns());
		crearTablaResultado();
		llenarTabla();
	}

	// *****************************************
	// METODOS PARA CONTROLAR LA TABLA GENERAL
	// *****************************************
	private void refrescarPanelTablaGeneral()
	{
		if(browseObject != null)
		{
			frmAbierto = false;
			String botonAgregar = browseObject.getBotonAgregar();
			if(!botonAgregar.equals(""))
			{
				botonAgregar = botonAgregar.replace("javascript: abrirPagina(", "");
				botonAgregar = botonAgregar.replace("'", "");
				botonAgregar = botonAgregar.replace(");", "");
				String[] valores = botonAgregar.split(",");
				paginaHijo = valores[0];
				paginaPadre = pagGen.pagAct.getPaginaHijo();
				paginaEvento = valores[1];
				paginaAccion = valores[2];
				paginaValor = "";
			}
			boolean valor = (botonAgregar.equals("")) ? false : true;
			panTabGenBrowse.cmdAgregar.setEnabled((botonAgregar.equals("")) ? false : true);
			for(int iteCmbPag = 0; iteCmbPag < panTabGenBrowse.cmbTamanoPagina.getItemCount(); iteCmbPag++)
			{
				String valorCombo = (String) panTabGenBrowse.cmbTamanoPagina.getItemAt(iteCmbPag);
				if(Util.isInteger(valorCombo))
				{
					if(browseObject.getPageSize() == Integer.parseInt(valorCombo))
					{
						panTabGenBrowse.cmbTamanoPagina.setSelectedIndex(iteCmbPag);
						break;
					}
				}
			}
			panTabGenBrowse.txtPaginaActual.setText(String.valueOf(browseObject.getCurrentPage()));
			if(panTabGenBrowse.modeloTablaGeneral.getRowCount() > 0)
			{
				panTabGenBrowse.tablaBrowse.setEnabled(true);
				panTabGenBrowse.txtPaginaActual.setEnabled(true);
				panTabGenBrowse.cmbTamanoPagina.setEnabled(true);
				for (int ite = 0; ite < panTabGenBrowse.cmdArrayPag.length; ite++)
				{
					panTabGenBrowse.cmdArrayPag[ite].setEnabled(true);
				}
			}
			else
			{
				if(panTabGenBrowse.cmdArrayPag != null) {
					for(int ite = 0; ite < panTabGenBrowse.cmdArrayPag.length; ite++)
					{
						panTabGenBrowse.remove(panTabGenBrowse.cmdArrayPag[ite]);
					}
				}
				panTabGenBrowse.repaint();
				panTabGenBrowse.etiInfo.setText("");
				panTabGenBrowse.txtPaginaActual.setValue(0);
				panTabGenBrowse.cmbTamanoPagina.setSelectedIndex(-1);
				panTabGenBrowse.tablaBrowse.setEnabled(false);
				panTabGenBrowse.txtPaginaActual.setEnabled(false);
				panTabGenBrowse.cmbTamanoPagina.setEnabled(false);
			}
			frmAbierto = true;
		}//if(browseObject != null)
	}

	private void crearModeloTabla(BrowseColumn[] browseColumns) {

		panTabGenBrowse.modeloTablaGeneral = new ModeloTablaGeneral(browseColumns);
		panTabGenBrowse.tablaBrowse.setModel(panTabGenBrowse.modeloTablaGeneral);
		panTabGenBrowse.celdaRenderGeneral = new CeldaRenderGeneral();
		try
		{
			panTabGenBrowse.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.String"), panTabGenBrowse.celdaRenderGeneral);
			panTabGenBrowse.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.Integer"), panTabGenBrowse.celdaRenderGeneral);
			panTabGenBrowse.tablaBrowse.setDefaultRenderer(Class.forName("java.lang.Double"), panTabGenBrowse.celdaRenderGeneral);
			panTabGenBrowse.tablaBrowse.setDefaultRenderer(Class.forName("java.math.BigDecimal"), panTabGenBrowse.celdaRenderGeneral);
			panTabGenBrowse.tablaBrowse.setDefaultRenderer(Class.forName("java.sql.Date"), panTabGenBrowse.celdaRenderGeneral);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
		// panTabGenBrowse.tablaBrowse.addPropertyChangeListener(new ModtablaBrowsePropiedad(this, tablaBrowse));
		panTabGenBrowse.tablaBrowse.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		// panTabGenBrowse.tablaBrowse.getTableHeader().setPreferredSize(new Dimension(panTabGenBrowse.tablaBrowse.getTableHeader().getWidth(), 25));
		// panTabGenBrowse.tablaBrowse.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 13));
		panTabGenBrowse.tablaBrowse.setRowHeight(25);
		panTabGenBrowse.tablaBrowse.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		panTabGenBrowse.tablaBrowse.setShowGrid(true);
		panTabGenBrowse.columna = new TableColumn[browseColumns.length];

		for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			panTabGenBrowse.columna[iteBrwCol] = panTabGenBrowse.tablaBrowse.getColumn(browseColumn.getColumnName());
			panTabGenBrowse.columna[iteBrwCol].setResizable(true);
			panTabGenBrowse.columna[iteBrwCol].setPreferredWidth(browseColumn.getSize() * 9);
			panTabGenBrowse.columna[iteBrwCol].setHeaderValue(browseColumn.getLabel());
			panTabGenBrowse.columna[iteBrwCol].setCellRenderer(obtenerCellRender(browseColumn));
			panTabGenBrowse.columna[iteBrwCol].setCellEditor(obtenerCellEdit(panTabGenBrowse.tablaBrowse, browseColumns, browseColumn));
			// columna[iteBrwCol].addPropertyChangeListener(new ListenerCambioPropiedad());
			if(browseColumn.isHidden())
			{
				panTabGenBrowse.tablaBrowse.getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				panTabGenBrowse.tablaBrowse.getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
				panTabGenBrowse.tablaBrowse.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
				panTabGenBrowse.tablaBrowse.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
			}// Fin de if (ancho[iteBrwCol] == 0)
		}// Recorrer la tablaBrowse de encabezados
	}// Fin de crearModeloTabla()

	public TableCellRenderer obtenerCellRender(BrowseColumn browseColumn)
	{

		if(browseColumn.isButtonInput())
		{
			return new CellRenderJButton(obtenerBoton(browseColumn.getAlias()));
		}
		return panTabGenBrowse.celdaRenderGeneral;
	}

	public TableCellEditor obtenerCellEdit(JTable tabla, BrowseColumn[] browseColumns, BrowseColumn browseColumn)
	{

		if(browseColumn.isButtonInput())
		{
			return new CellEditJButton(obtenerBoton(browseColumn.getAlias()), this);
		}
		if(browseColumn.isTextInput())
		{
			return new CellEditDecimalTextInput(this, tabla, browseColumns);
		}
		return null;
	}

	public JButton obtenerBoton(String nombre)
	{

		JButton boton = new JButton(nombre);
		boton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		boton.setMargin(new Insets(0, 0, 0, 0));
		return boton;
	}

	public void crearTablaResultado()
	{

		List listResult = (List) browse.getPageResults();
		panTabGenBrowse.totalRegistros = browse.getRowCount();
		panTabGenBrowse.tamanoPagina = browse.getPageSize();
		panTabGenBrowse.totalPaginas = browse.getPageCount();
		panTabGenBrowse.paginaActual = browse.getCurrentPage();
		if(listResult != null && listResult.size() > 0)
		{
			// totalRegistros = listResult.size();
			panTabGenBrowse.visiblePages = 5;
			panTabGenBrowse.totalPaginas = panTabGenBrowse.totalRegistros / panTabGenBrowse.tamanoPagina;
			panTabGenBrowse.totalPaginas = (panTabGenBrowse.totalPaginas < 0) ? 0 : panTabGenBrowse.totalPaginas;
			panTabGenBrowse.resto = panTabGenBrowse.totalRegistros % panTabGenBrowse.tamanoPagina;
			panTabGenBrowse.totalPaginas = (panTabGenBrowse.resto > 0) ? panTabGenBrowse.totalPaginas + 1 : panTabGenBrowse.totalPaginas;
			if(panTabGenBrowse.totalPaginas < panTabGenBrowse.visiblePages)
			{
				panTabGenBrowse.visiblePages = panTabGenBrowse.totalPaginas;
			}
			for(int itePag = 1; itePag <= panTabGenBrowse.totalPaginas; itePag++)
			{
				int paginaActualReferencia = panTabGenBrowse.paginaActual;
				int inicioPagina = panTabGenBrowse.visiblePages * (itePag - 1);
				int finPagina = panTabGenBrowse.visiblePages * itePag;
				if(paginaActualReferencia > inicioPagina && paginaActualReferencia <= finPagina)
				{
					panTabGenBrowse.iteIni = itePag;
					panTabGenBrowse.iteFin = itePag + panTabGenBrowse.visiblePages - 1;
					break;
				}
			}
			panTabGenBrowse.paginaActual = panTabGenBrowse.paginaActual;
			// panTabGenBrowse.iniItePag = panTabGenBrowse.paginaActual;
			// if (panTabGenBrowse.totalPaginas <= panTabGenBrowse.visiblePages) {
			// panTabGenBrowse.visiblePages = panTabGenBrowse.totalPaginas;
			// }
			if(panTabGenBrowse.cmdArrayPag != null) {
				for(int ite = 0; ite < panTabGenBrowse.cmdArrayPag.length; ite++)
				{
					panTabGenBrowse.remove(panTabGenBrowse.cmdArrayPag[ite]);
				}
				panTabGenBrowse.cmdArrayPag = null;
			}
			panTabGenBrowse.repaint();
			crearBotonesPaginacion();
			panTabGenBrowse.repaint();
		}// Fin de if(listResult != null)
	}

	public void crearBotonesPaginacion() {

		panTabGenBrowse.numeroBoton = panTabGenBrowse.visiblePages;
		panTabGenBrowse.iteCmd = 0;
		panTabGenBrowse.numeroBoton = panTabGenBrowse.visiblePages + 2;
		panTabGenBrowse.cmdArrayPag = new JButton[panTabGenBrowse.numeroBoton];
		// CREAMOS EL BOTON DE ATRAS
		panTabGenBrowse.coordX = (panTabGenBrowse.iteCmd * panTabGenBrowse.cmdPagWidth) + panTabGenBrowse.marginLeft;
		panTabGenBrowse.cmdArrayPag[0] = crearBoton(panTabGenBrowse.coordX, panTabGenBrowse.coordY, panTabGenBrowse.cmdPagWidth, panTabGenBrowse.cmdPagHeigth, 0);
		panTabGenBrowse.add(panTabGenBrowse.cmdArrayPag[0]);
		panTabGenBrowse.iteCmd++;
		// CREAMOS LOS BOTONES VISIBLES
		for(int ite = panTabGenBrowse.iteIni; ite <= panTabGenBrowse.iteFin; ite++) {
			panTabGenBrowse.coordX = (panTabGenBrowse.iteCmd * panTabGenBrowse.cmdPagWidth) + panTabGenBrowse.marginLeft;
			panTabGenBrowse.cmdArrayPag[panTabGenBrowse.iteCmd] = crearBoton(panTabGenBrowse.coordX, panTabGenBrowse.coordY, panTabGenBrowse.cmdPagWidth, panTabGenBrowse.cmdPagHeigth, panTabGenBrowse.iteCmd);
			panTabGenBrowse.add(panTabGenBrowse.cmdArrayPag[panTabGenBrowse.iteCmd]);
			panTabGenBrowse.iteCmd++;
			// panTabGenBrowse.iniItePag++;
		}
		// CREAMOS EL BOTON DE ADELANTE
		panTabGenBrowse.coordX = (panTabGenBrowse.iteCmd * panTabGenBrowse.cmdPagWidth) + panTabGenBrowse.marginLeft;
		panTabGenBrowse.cmdArrayPag[panTabGenBrowse.iteCmd] = crearBoton(panTabGenBrowse.coordX, panTabGenBrowse.coordY, panTabGenBrowse.cmdPagWidth, panTabGenBrowse.cmdPagHeigth, panTabGenBrowse.iteCmd);
		panTabGenBrowse.add(panTabGenBrowse.cmdArrayPag[panTabGenBrowse.iteCmd]);
		panTabGenBrowse.iteCmd++;
		if(panTabGenBrowse.etiInfo == null) {
			panTabGenBrowse.etiInfo = new JLabel();
			panTabGenBrowse.add(panTabGenBrowse.etiInfo);
		}
		panTabGenBrowse.coordX = (panTabGenBrowse.iteCmd * panTabGenBrowse.cmdPagWidth) + panTabGenBrowse.marginLeft;
		panTabGenBrowse.etiInfo.setBounds(panTabGenBrowse.coordX + panTabGenBrowse.marginLeft, panTabGenBrowse.coordY, 400, panTabGenBrowse.cmdPagHeigth);
		panTabGenBrowse.etiInfo.setText("Paginas : " + panTabGenBrowse.totalPaginas + " Registros : " + panTabGenBrowse.totalRegistros);
	}

	public JButton crearBoton(int coordX, int coordY, int cmdPagWidth, int cmdHeight, int parIndex) {

		JButton cmdPag = new JButton();
		cmdPag.setBounds(coordX, coordY, cmdPagWidth, cmdHeight);
		String nombreBoton = "";
		if(parIndex == 0) {
			nombreBoton = "atras_0";
			cmdPag.setText("<<");
		}
		else if((parIndex - 1) == panTabGenBrowse.visiblePages) {
			nombreBoton = "adelante_0";
			cmdPag.setText(">>");
		}
		else {
			nombreBoton = "cmdArrayPag_" + String.valueOf(parIndex);
			cmdPag.setText(String.valueOf(parIndex));
		}
		cmdPag.setName(nombreBoton);
		cmdPag.setActionCommand(nombreBoton);
		cmdPag.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPag.setBounds(coordX, coordY, cmdPagWidth, cmdHeight);
		cmdPag.setForeground(panTabGenBrowse.naranja);
		// cmdPag.setBackground(colorFondo);
		// cmdPag.setForeground(colorTexto);
		cmdPag.setMargin(new java.awt.Insets(0, 0, 0, 0));
//		cmdPag.addActionListener(new cmdArrayPagListener(this));
		// System.out.println("iteini : " + panTabGenBrowse.iteIni + " itefin : " + panTabGenBrowse.iteFin + " itecmd : " + panTabGenBrowse.iteCmd + cmdPag.getName() + " : " + cmdPag.getActionCommand());
		return cmdPag;
	}// Fin de Metodo para crear botones de dias de calendario

	public void setCurrentPage(int currentPage) {

		for(int ite = 0; ite < panTabGenBrowse.numeroBoton; ite++) {
			panTabGenBrowse.cmdArrayPag[ite].setForeground(panTabGenBrowse.naranja);
			String[] valCmd = panTabGenBrowse.cmdArrayPag[ite].getName().split("_");
			if(!valCmd[0].equals("atras") && !valCmd[0].equals("adelante")) {
				if((Integer.parseInt(valCmd[1])) == currentPage) {
					panTabGenBrowse.cmdArrayPag[ite].setForeground(panTabGenBrowse.rojo);
				}
			}
		}
		panTabGenBrowse.paginaActual = currentPage;
	}

	public void llenarTabla()
	{

		panTabGenBrowse.paginaActual = browse.getCurrentPage();
		setCurrentPage(panTabGenBrowse.paginaActual);
		List listaRegistros = (List) browse.getPageResults();
		Object[] nuevaFila = new Object[browseColumns.length];
		for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumns[iteBrwCol];
			if(browseColumn.getType().equals("INTEGER")) {
				nuevaFila[iteBrwCol] = 0;
			}
			if(browseColumn.getType().equals("DOUBLE")) {
				nuevaFila[iteBrwCol] = 0.00;
			}
			if(browseColumn.getType().equals("STRING")) {
				nuevaFila[iteBrwCol] = "";
			}
			if(browseColumn.getType().equals("BOOLEAN")) {
				nuevaFila[iteBrwCol] = false;
			}
			if(browseColumn.getType().equals("BIGDECIMAL")) {
				nuevaFila[iteBrwCol] = new BigDecimal("000000000000.00");
			}
		}
		panTabGenBrowse.modeloTablaGeneral.setRowCount(0);
		if(listaRegistros != null && listaRegistros.size() > 0)
		{
			for(int iteFil = 0; iteFil < listaRegistros.size(); iteFil++)
			{
				Object[] lisCol = (Object[]) listaRegistros.get(iteFil);
				Object objetoRegistro = listaRegistros.get(iteFil);
				panTabGenBrowse.modeloTablaGeneral.addRow(nuevaFila);
				for(int iteCol = 0; iteCol < lisCol.length; iteCol++)
				{
					panTabGenBrowse.modeloTablaGeneral.setValueAt(obtenerValor(iteCol, objetoRegistro, browseColumns), iteFil, iteCol);
				}
			}
		}
	}

	public Object obtenerValor(int idxCol, Object objetoRegistro, BrowseColumn[] browseColumns)
	{

		try
		{
			BrowseColumn columna = browseColumns[idxCol];
			Object columnValue = BrowseUtility.getColumnObject(columna, objetoRegistro, "sisem", browseColumns, browse.getBrowseId(), "");
			String columnResult = "";
			String allowEdit = "true";
			if(columna.getType().equals("ConditionalLink")) {
				columnResult = " <div id=\"browse_" + columna.getColumnName() + "\">";
			}
			else if(columna.getLink() != null && columna.getLink().length() > 0)
			{
				columnResult = BrowseUtility.getColumnLinkObject(columna, objetoRegistro, browseColumns);
				columnResult = columnResult.replace("javascript: abrirPagina(", "");
				columnResult = columnResult.replace("'", "");
				columnResult = columnResult.replace(");", "");
			}
			columnValue = (columnResult.equals("")) ? columnValue : columnResult;
			return columnValue;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	private void seleccionarRegistroPanTabGenTableBrowse()
	{

		if(panTabGenBrowse.tablaBrowse.isEditing())
		{
			pagGen.pagAct.setModo(Modo.MODIFICANDO);
		}
		else if(!panTabGenBrowse.tablaBrowse.isEditing())
		{
			pagGen.pagAct.setModo(Modo.VISUALIZANDO);
			recuperarRegistroParaFicControl();
			if(panTabGenBrowse.tablaBrowse.getSelectedRow() > -1)
			{
			}
		}
		refrescarFormulario();
	}// Fin de seleccionarRegistroTablaSucursal()

	private void recuperarRegistroParaFicControl()
	{
		if(browseObject.getBrowseTabla().equals("pedfic-pedmod"))
		{
			if (panTabGenBrowse.tablaBrowse.getSelectedRow() > -1) 
			{
				int colMod = panTabGenBrowse.obtenerIndiceHeader("codmodelo");
				int colPed = panTabGenBrowse.obtenerIndiceHeader("pedcodigo");
				
				if(colMod > -1 && colPed > -1)
				{
					Object a = panTabGenBrowse.tablaBrowse.getValueAt(panTabGenBrowse.tablaBrowse.getSelectedRow(), colMod);// Codigo del modelo
					modelo = (Integer) panTabGenBrowse.tablaBrowse.getValueAt(panTabGenBrowse.tablaBrowse.getSelectedRow(), colMod);// Codigo del modelo
					pedido = (Integer) panTabGenBrowse.tablaBrowse.getValueAt(panTabGenBrowse.tablaBrowse.getSelectedRow(), colPed);// Codigo del pedido
					recuperarRegistroParaFicControlEvento();
				}
			}// Fin de if(parNumFil > -1)
		}
	}
	
	public void recuperarRegistroParaFicControlEvento()
	{
		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			
			peticion.put("prdcodigo", modelo);
			peticion.put("pedcodigo", pedido);
			
			ejecutar("recuperarRegistroParaFicControlEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			refrescarFormulario();
//			generarTablaFichaControl();
		}
	}
	
//	private void generarTablaFichaControl()
//	{
//		List listaTallas = (List) peticion.get("listaTallas");
//		BrowseColumn[] browseColumnsFichaControl = obtenerBrowseColumnFichaControl(listaTallas);
//		crearModeloTablaFichaControl(browseColumnsFichaControl);
//		llenarRegistrosTablaFichaControl();
//	}
	
	private BrowseColumn[] obtenerBrowseColumnFichaControl(List lista)
	{
		browseColumnsFichaControl = new BrowseColumn[lista.size() + 3];
		
		browseColumnsFichaControl[0] = new BrowseColumn();
		browseColumnsFichaControl[0].setColumnName("DetComponente_detcmpcodigo");
		browseColumnsFichaControl[0].setLabel("actualizar_detcomponente");
		browseColumnsFichaControl[0].setAlias("codigo");
		browseColumnsFichaControl[0].setType("STRING");
		browseColumnsFichaControl[0].setEditable(false);
		browseColumnsFichaControl[0].setHidden(true);
		browseColumnsFichaControl[0].setLink("javascript: abrirPagina('DlgDetComponente','tabCelActualizarComponentePorModeloEvento','com.componente.accion.ActualizarComponentePorModelo','#DetComponente_detcmpcodigo^');");
		browseColumnsFichaControl[0].setSize(12);
		BrowseUtility.getColumnNamesFromLink(browseColumnsFichaControl[0]);

		browseColumnsFichaControl[1] = new BrowseColumn();
		browseColumnsFichaControl[1].setColumnName("Componente_cmpcodigo");
		browseColumnsFichaControl[1].setLabel("Codigo");
		browseColumnsFichaControl[1].setAlias("codigo");
		browseColumnsFichaControl[1].setType("BIGDECIMAL");
		browseColumnsFichaControl[1].setEditable(false);
		browseColumnsFichaControl[1].setHidden(true);
		browseColumnsFichaControl[1].setSize(12);

		browseColumnsFichaControl[2] = new BrowseColumn();
		browseColumnsFichaControl[2].setColumnName("Componente");
		browseColumnsFichaControl[2].setLabel("Componente");
		browseColumnsFichaControl[2].setAlias("componente");
		browseColumnsFichaControl[2].setType("STRING");
		browseColumnsFichaControl[2].setEditable(false);
		browseColumnsFichaControl[2].setSize(16);
		
		for(int iteLis = 0; iteLis < lista.size(); iteLis++)
		{
			Object[] registro = (Object[]) lista.get(iteLis);
			browseColumnsFichaControl[iteLis+3] = new BrowseColumn();
			browseColumnsFichaControl[iteLis+3].setColumnName((String)registro[2]);
			browseColumnsFichaControl[iteLis+3].setLabel((String)registro[2]);
			browseColumnsFichaControl[iteLis+3].setAlias((String)registro[2]);
			browseColumnsFichaControl[iteLis+3].setType("BIGDECIMAL");
			browseColumnsFichaControl[iteLis+3].setEditable(true);
			browseColumnsFichaControl[iteLis+3].setTextInput(true);
			browseColumnsFichaControl[iteLis+3].setSize(6);
			browseColumnsFichaControl[iteLis+3].setScriptCode("actualizar_detcomponente");
		}
		
		return browseColumnsFichaControl;
	}
	
//	private void crearModeloTablaFichaControl(BrowseColumn[] browseColumns) {
//
//		modTabFicCtrl = new ModeloTablaGeneral(browseColumns);
//		tabFicCtrl.setModel(modTabFicCtrl);
//		celdaRenderGeneral = new CeldaRenderGeneral();
//		try
//		{
//			tabFicCtrl.setDefaultRenderer(Class.forName("java.lang.String"), celdaRenderGeneral);
//			tabFicCtrl.setDefaultRenderer(Class.forName("java.lang.Integer"), celdaRenderGeneral);
//			tabFicCtrl.setDefaultRenderer(Class.forName("java.lang.Double"), celdaRenderGeneral);
//			tabFicCtrl.setDefaultRenderer(Class.forName("java.math.BigDecimal"), celdaRenderGeneral);
//			tabFicCtrl.setDefaultRenderer(Class.forName("java.sql.Date"), celdaRenderGeneral);
//		} catch (ClassNotFoundException ex) {
//			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//		}
//		// tabFicCtrl.addPropertyChangeListener(new ModtablaBrowsePropiedad(this, tablaBrowse));
//		tabFicCtrl.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
//		// tabFicCtrl.getTableHeader().setPreferredSize(new Dimension(tabFicCtrl.getTableHeader().getWidth(), 25));
//		// tabFicCtrl.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 13));
//		tabFicCtrl.setRowHeight(25);
//		tabFicCtrl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//		tabFicCtrl.setShowGrid(true);
//		colTabFicCtrl = new TableColumn[browseColumns.length];
//
//		for(int iteBrwCol = 0; iteBrwCol < browseColumns.length; iteBrwCol++)
//		{
//			BrowseColumn browseColumn = browseColumns[iteBrwCol];
//			colTabFicCtrl[iteBrwCol] = tabFicCtrl.getColumn(browseColumn.getColumnName());
//			colTabFicCtrl[iteBrwCol].setResizable(true);
//			colTabFicCtrl[iteBrwCol].setPreferredWidth(browseColumn.getSize() * 9);
//			colTabFicCtrl[iteBrwCol].setHeaderValue(browseColumn.getLabel());
//			colTabFicCtrl[iteBrwCol].setCellRenderer(obtenerCellRender(browseColumn));
//			colTabFicCtrl[iteBrwCol].setCellEditor(obtenerCellEdit(tabFicCtrl, browseColumnsFichaControl, browseColumn));
//			// columna[iteBrwCol].addPropertyChangeListener(new ListenerCambioPropiedad());
//			if(browseColumn.isHidden())
//			{
//				tabFicCtrl.getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
//				tabFicCtrl.getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
//				tabFicCtrl.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMaxWidth(0);
//				tabFicCtrl.getTableHeader().getColumnModel().getColumn(iteBrwCol).setMinWidth(0);
//			}// Fin de if (ancho[iteBrwCol] == 0)
//		}// Recorrer la tablaBrowse de encabezados
//	}// Fin de crearModeloTabla()	
	
	private void llenarRegistrosTablaFichaControl()
	{
		
		List listaComponentes =  (List) peticion.get("listaComponentes");

		Object[] nuevaFila = new Object[browseColumnsFichaControl.length];
		for(int iteBrwCol = 0; iteBrwCol < browseColumnsFichaControl.length; iteBrwCol++)
		{
			BrowseColumn browseColumn = browseColumnsFichaControl[iteBrwCol];
			if(browseColumn.getType().equals("INTEGER")) {
				nuevaFila[iteBrwCol] = 0;
			}
			if(browseColumn.getType().equals("DOUBLE")) {
				nuevaFila[iteBrwCol] = 0.00;
			}
			if(browseColumn.getType().equals("STRING")) {
				nuevaFila[iteBrwCol] = "";
			}
			if(browseColumn.getType().equals("BOOLEAN")) {
				nuevaFila[iteBrwCol] = false;
			}
			if(browseColumn.getType().equals("BIGDECIMAL")) {
				nuevaFila[iteBrwCol] = new BigDecimal("000000000000.00");
			}
		}
		modTabFicCtrl.setRowCount(0);
		if(listaComponentes != null && listaComponentes.size() > 0)
		{
			for(int iteLisCmp = 0; iteLisCmp < listaComponentes.size(); iteLisCmp++)
			{
				Object[] regCmp = (Object[]) listaComponentes.get(iteLisCmp);
				Object objetoRegistro = listaComponentes.get(iteLisCmp);
				modTabFicCtrl.addRow(nuevaFila);
				for(int iteRegCmp = 0; iteRegCmp < regCmp.length; iteRegCmp++)
				{
//					modTabFicCtrl.setValueAt(obtenerValor(iteRegCmp, objetoRegistro, browseColumnsFichaControl), iteLisCmp, iteRegCmp);
					modTabFicCtrl.setValueAt(BrowseUtility.obtenerValor(browse,browseColumnsFichaControl,objetoRegistro,iteRegCmp), iteLisCmp, iteRegCmp);
				}
			}
//			llenarValoresTallaComponente();
		}
	}
	
//	private void llenarValoresTallaComponente()
//	{
//		List listaTallaComponente = (List) peticion.get("listaTallaComponente");
//		
//		for(int iteFil = 0; iteFil < tabFicCtrl.getRowCount(); iteFil++)
//		{
//			for(int iteCol = 2; iteCol < tabFicCtrl.getColumnCount(); iteCol++)
//			{
//				BigDecimal cmpCodigo = (BigDecimal)tabFicCtrl.getValueAt(iteFil, 1);
//				String talla = (String) tabFicCtrl.getTableHeader().getColumnModel().getColumn(iteCol).getHeaderValue();
//				
//				for(int iteLisTalCmp = 0; iteLisTalCmp < listaTallaComponente.size(); iteLisTalCmp++)
//				{
//					Object[] regLisTalCmp = (Object[]) listaTallaComponente.get(iteLisTalCmp);
////					System.out.println(cmpCodigo +  " : " + talla  + " : " + regLisTalCmp[0]+ " : " + regLisTalCmp[1]);
//					if(regLisTalCmp[0].equals(cmpCodigo) && regLisTalCmp[1].equals(talla))
//					{
//						BigDecimal valor = (BigDecimal)regLisTalCmp[2];
//						String qty_decimals = PropiedadGeneral.getInstance(OrganizacionGeneral.getOrgcodigo()).getProperty("MISC", "QTYDECIMALS", "2");
//						valor = valor.setScale(Integer.valueOf(qty_decimals).intValue(), BigDecimal.ROUND_HALF_UP);
//						
//						modTabFicCtrl.setValueAt(valor, iteFil, iteCol);
//						break;
//					}
//				}
//			}
//		}
//	}

//	private void seleccionarRegistroTabFicCtrl()
//	{
//
//		if(tabFicCtrl.isEditing())
//		{
//			pagGen.pagAct.setModo(Modo.MODIFICANDO);
//		}
//		else if(!tabFicCtrl.isEditing())
//		{
//			pagGen.pagAct.setModo(Modo.VISUALIZANDO);
////			llenarFormulario();
////			activarFormulario(true);
////			cmdCerrar.setEnabled(true);
//
////			llenarRegistrosTablaFichaControl();
////			recuperarRegistroParaFicControl();
////			if(panTabGenBrowse.tablaBrowse.getSelectedRow() > -1)
////			{
////			}
//		}
//		refrescarFormulario();
//	}// Fin de seleccionarRegistroTablaSucursal()	
//	
	
	/*
	 * LISTENER DE CLICK
	 */
	private static class cmdArrayPagListener implements ActionListener {

		private DlgTablaGeneralBrowse dlgTablaGeneral;

		public cmdArrayPagListener(DlgTablaGeneralBrowse dlgTablaGeneral) {

			this.dlgTablaGeneral = dlgTablaGeneral;
		}

		@Override
		public void actionPerformed(ActionEvent e) {

			JButton objCmd = (JButton) e.getSource();
			String[] valCmd = objCmd.getActionCommand().split("_");
			if(valCmd[0].equals("atras") || valCmd[0].equals("adelante"))
			{
				if(valCmd[0].equals("adelante") && dlgTablaGeneral.browse.getCurrentPage() < dlgTablaGeneral.browse.getPageCount())
				{
					if(dlgTablaGeneral.panTabGenBrowse.paginaActual < dlgTablaGeneral.panTabGenBrowse.totalPaginas)
					{
						dlgTablaGeneral.panTabGenBrowse.paginaActual++;
						dlgTablaGeneral.browse.setCurrentPage(dlgTablaGeneral.panTabGenBrowse.paginaActual);
						dlgTablaGeneral.browseObject.setCurrentPage(dlgTablaGeneral.browse.getCurrentPage());
						dlgTablaGeneral.refrescarRegistros();
					}
				}
				else if(valCmd[0].equals("atras") && dlgTablaGeneral.browse.getCurrentPage() > 1)
				{
					if((dlgTablaGeneral.panTabGenBrowse.paginaActual) > 1)
					{
						dlgTablaGeneral.panTabGenBrowse.paginaActual--;
						dlgTablaGeneral.browse.setCurrentPage(dlgTablaGeneral.panTabGenBrowse.paginaActual);
						dlgTablaGeneral.browseObject.setCurrentPage(dlgTablaGeneral.browse.getCurrentPage());
						dlgTablaGeneral.refrescarRegistros();
					}
				}
				else {
					dlgTablaGeneral.panTabGenBrowse.paginaActual = dlgTablaGeneral.panTabGenBrowse.paginaActual;
				}
				// for (int ite = 0; ite < dlgTablaGeneral.panTabGenBrowse.numeroBoton; ite++) {
				// dlgTablaGeneral.remove(dlgTablaGeneral.panTabGenBrowse.cmdArrayPag[ite]);
				// }
				// dlgTablaGeneral.panTabGenBrowse.repaint();
				// // DlgTablaGeneral.update(DlgTablaGeneral.getGraphics());
				// dlgTablaGeneral.crearBotonesPaginacion(valCmd[0]);
			}
			else
			{
				dlgTablaGeneral.browse.setCurrentPage(Integer.parseInt(valCmd[1]));
				dlgTablaGeneral.browseObject.setCurrentPage(dlgTablaGeneral.browse.getCurrentPage());
				dlgTablaGeneral.refrescarRegistros();
			}
			// System.out.println("bootn : " + objCmd.getActionCommand());
		}// Fin de Action performed del boton de la palanca
	}// fin de la clase estatica para escuchar las acciones de los botones de palanca
}
