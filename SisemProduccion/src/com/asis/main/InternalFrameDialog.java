package com.asis.main;

import java.awt.AWTEvent;
import java.awt.ActiveEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.MenuComponent;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class InternalFrameDialog extends JInternalFrame implements AbstractDialog {
	private static final long serialVersionUID = 1L;
		
	final BlockingWindow blocker = new BlockingWindow();
	
	public InternalFrameDialog() {
		blocker.setVisible(false);
		this.getLayeredPane().add(blocker, JLayeredPane.PALETTE_LAYER);
		this.getLayeredPane().add(this, JLayeredPane.MODAL_LAYER);
	}
		
	public void setLocationRelativeTo(Component w) {
		
	}
		
	@Override
	public void setVisible(boolean b) {
		if(b) {
			fixBackgroundColor(getContentPane());
			((JComponent)getContentPane()).setOpaque(true);
		}
		
		Dimension d = new Dimension(this.getWidth(),this.getHeight());
		if(blocker!=null) { //blocker is null during construction, and setVisible is called high up
			blocker.setSize(d);
			blocker.setVisible(b);
		}
		super.setVisible(b);
		if(b && blocker!=null) {
			startModal();
		}
	}

	private Color panelBackground = UIManager.getColor("Panel.background");
	private Color goodBackground = new Color(panelBackground.getRed(), 
			panelBackground.getGreen(), 
			panelBackground.getBlue());
	private void fixBackgroundColor(Component c) {
		//fixing a Max bug involving apple.laf.CColorPaintUIResource:
		if(c.getBackground().equals(panelBackground)) {
			c.setBackground(goodBackground);
		}
		if(c instanceof Container) {
			Container container = (Container)c;
			for(int a = 0; a < container.getComponentCount(); a++) {
				fixBackgroundColor( container.getComponent(a) );
			}
		}
	}
	
	private synchronized void startModal () {
		try {
			if (SwingUtilities.isEventDispatchThread()) {
				EventQueue theQueue = getToolkit().getSystemEventQueue();
				while (isVisible()) {
					AWTEvent event = theQueue.getNextEvent();
					Object source = event.getSource();
					if (event instanceof ActiveEvent) {
						((ActiveEvent) event).dispatch();
					} else if (source instanceof Component) {
						((Component) source).dispatchEvent(event);
					} else if (source instanceof MenuComponent) {
						((MenuComponent) source).dispatchEvent(event);
					} else {
						System.err.println("Unable to dispatch: " + event);
					}
				}
			} else {
				while (isVisible()) {
					wait();
				}
			}
		} catch (InterruptedException ignored) { }
	}	
}