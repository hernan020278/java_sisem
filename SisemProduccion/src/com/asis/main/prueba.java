package com.asis.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicCheckBoxUI;

public class prueba {

	Icon getIcon(int w, int h) {

		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = bi.createGraphics();
		g2.setColor(Color.YELLOW);
		g2.fillRect(0, 0, w, h);
		g2.setColor(Color.RED);
		g2.fillOval(3, 3, w - 6, h - 6);
		ImageIcon icon = new ImageIcon(bi);
		return icon;
	}

	void makeUI() {

		JFrame frame = new JFrame("CheckBox with extra icon");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new FlowLayout());
		frame.setSize(200, 100);
		frame.setLocationRelativeTo(null);
		IconCheckBox iconCheckBox = new IconCheckBox("CheckBox", getIcon(100, 100));
		frame.add(iconCheckBox);
		frame.setVisible(true);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {

				new prueba().makeUI();
			}
		});
	}
}

class IconCheckBox extends JCheckBox {

	Icon customIcon;
	IconCheckBox iconCheckBox = this;

	public IconCheckBox(String text, Icon icon) {

		setText(text);
		customIcon = icon;
		setUI(new BasicCheckBoxUI() {

			@Override
			protected void paintText(Graphics g, JComponent c, Rectangle textRect, String text) {

				int x = (int) textRect.getX();
				int y = (int) textRect.getY();
				Graphics scratchGraphics = g.create();
				scratchGraphics.setClip(x, y, customIcon.getIconWidth(), customIcon.getIconHeight());
				customIcon.paintIcon(iconCheckBox, scratchGraphics, x, y);
				scratchGraphics.dispose();
				super.paintText(g, c,
						new Rectangle(x + customIcon.getIconWidth() + iconCheckBox.getIconTextGap(), y,
								(int) textRect.width, (int) textRect.height), text);
			}
		});
	}

	@Override
	public Dimension getPreferredSize() {

		Dimension dim = super.getPreferredSize();
		return new Dimension(dim.width + customIcon.getIconWidth() + getIconTextGap(),
				Math.max(dim.height, customIcon.getIconHeight() + getIconTextGap()));
	}

	@Override
	public void setIcon(Icon icon) {

		if(customIcon == icon) {
			return;
		}
		customIcon = icon;
		repaint();
	}
}
