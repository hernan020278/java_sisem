package com.asis.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import com.browse.Browse;
import com.browse.BrowseObject;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.comun.utilidad.swing.JTextFieldFormatoHora;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;
import com.toedter.calendar.JDateChooser;
import com.comun.utilidad.swing.JTextFieldFormatoDecimal;

public class DlgAsiasistencia extends VistaAdatper implements VistaListener {

	private JPanel panelPrincipal;

	public DlgAsiasistencia(FrmInicio parent) {

		super(parent);
		setResizable(false);
		
		configurarFechas();
		initComponents();
		
		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		// Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));
		// Se pasa el conjunto de teclas al panel principal
		// super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);
		super.addWindowListener(new WindowAdapter() {

			@Override
			public void windowActivated(WindowEvent we) {

				frmAbierto = true;
			}
		});
	}

	private void configurarFechas()
	{
		txtEditAsasistencia_fecha = new JTextEditorFecha();
		txtEditAsasistencia_fecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditAsasistencia_fecha.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		
	}
	
	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	private void initComponents() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);
		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);
		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 529, 272);
		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);
		panConEsp = new javax.swing.JPanel();
		jLabel2 = new javax.swing.JLabel();
		jLabel2.setForeground(Color.BLACK);
		jLabel2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsasistencia_asicodigo = new JTextFieldChanged(11);
		txtAsasistencia_asicodigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		setTitle(this.getClass().getSimpleName() + " - Horarios");
		panConEsp.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		panConEsp.setLayout(null);
		jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel2.setText("Codigo");
		panConEsp.add(jLabel2);
		jLabel2.setBounds(8, 14, 121, 20);
		txtAsasistencia_asicodigo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		txtAsasistencia_asicodigo.setText("12345678901234");
		txtAsasistencia_asicodigo.addKeyListener(new java.awt.event.KeyAdapter() {

			public void keyReleased(java.awt.event.KeyEvent evt) {

			}
		});
		panConEsp.add(txtAsasistencia_asicodigo);
		txtAsasistencia_asicodigo.setBounds(8, 38, 121, 23);
		panelPrincipal.add(panConEsp);
		panConEsp.setBounds(10, 11, 497, 189);
		JLabel lblHoraInicio = new JLabel();
		lblHoraInicio.setText("Ingreso");
		lblHoraInicio.setHorizontalAlignment(SwingConstants.CENTER);
		lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblHoraInicio.setBounds(8, 72, 92, 23);
		panConEsp.add(lblHoraInicio);
		txtAsasistencia_ingreso = new JTextFieldFormatoHora();
		txtAsasistencia_ingreso.setText("12:10");
		txtAsasistencia_ingreso.setHorizontalAlignment(SwingConstants.CENTER);
		txtAsasistencia_ingreso.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsasistencia_ingreso.setBounds(8, 95, 92, 23);
		panConEsp.add(txtAsasistencia_ingreso);
		
		cmbAsasistencia_fecha = new JDateChooser(txtEditAsasistencia_fecha);
		cmbAsasistencia_fecha.setDateFormatString("dd/MM/yyyy");
		cmbAsasistencia_fecha.setBounds(293, 37, 115, 23);
		panConEsp.add(cmbAsasistencia_fecha);
		
		JLabel lblTurno = new JLabel();
		lblTurno.setText("Turno");
		lblTurno.setHorizontalAlignment(SwingConstants.CENTER);
		lblTurno.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTurno.setBounds(164, 14, 108, 20);
		panConEsp.add(lblTurno);
		
		txtAsasistencia_turno = new JTextFieldChanged(100);
		txtAsasistencia_turno.setHorizontalAlignment(SwingConstants.LEFT);
		txtAsasistencia_turno.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsasistencia_turno.setBounds(162, 37, 110, 23);
		panConEsp.add(txtAsasistencia_turno);
		
		JLabel lblSalida = new JLabel();
		lblSalida.setText("Salida");
		lblSalida.setHorizontalAlignment(SwingConstants.CENTER);
		lblSalida.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSalida.setBounds(257, 72, 92, 23);
		panConEsp.add(lblSalida);
		
		txtAsasistencia_salida = new JTextFieldFormatoHora();
		txtAsasistencia_salida.setText("12:10");
		txtAsasistencia_salida.setHorizontalAlignment(SwingConstants.CENTER);
		txtAsasistencia_salida.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsasistencia_salida.setBounds(257, 95, 92, 23);
		panConEsp.add(txtAsasistencia_salida);
		
		JLabel lblIngestado = new JLabel();
		lblIngestado.setText("Ing-Estado");
		lblIngestado.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngestado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIngestado.setBounds(112, 67, 108, 20);
		panConEsp.add(lblIngestado);
		
		txtAsasistencia_ingestado = new JTextFieldChanged(100);
		txtAsasistencia_ingestado.setHorizontalAlignment(SwingConstants.LEFT);
		txtAsasistencia_ingestado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsasistencia_ingestado.setBounds(110, 95, 110, 23);
		panConEsp.add(txtAsasistencia_ingestado);
		
		JLabel lblSalestado = new JLabel();
		lblSalestado.setText("Sal-Estado");
		lblSalestado.setHorizontalAlignment(SwingConstants.CENTER);
		lblSalestado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSalestado.setBounds(368, 67, 108, 20);
		panConEsp.add(lblSalestado);
		
		txtAsasistencia_salestado = new JTextFieldChanged(100);
		txtAsasistencia_salestado.setHorizontalAlignment(SwingConstants.LEFT);
		txtAsasistencia_salestado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsasistencia_salestado.setBounds(366, 95, 110, 23);
		panConEsp.add(txtAsasistencia_salestado);
		
		JLabel lblHoratrab = new JLabel();
		lblHoratrab.setText("Hora-Trab");
		lblHoratrab.setHorizontalAlignment(SwingConstants.CENTER);
		lblHoratrab.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblHoratrab.setBounds(8, 132, 69, 20);
		panConEsp.add(lblHoratrab);
		
		txtAsasistencia_hortrab = new JTextFieldFormatoDecimal(5, 3);
		txtAsasistencia_hortrab.setText("100");
		txtAsasistencia_hortrab.setHorizontalAlignment(SwingConstants.CENTER);
		txtAsasistencia_hortrab.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsasistencia_hortrab.setBounds(8, 155, 69, 23);
		panConEsp.add(txtAsasistencia_hortrab);
		
		JLabel lblHora = new JLabel();
		lblHora.setText("Hora-Cont");
		lblHora.setHorizontalAlignment(SwingConstants.CENTER);
		lblHora.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblHora.setBounds(87, 132, 69, 20);
		panConEsp.add(lblHora);
		
		txtAsasistencia_horcont = new JTextFieldFormatoDecimal(5, 3);
		txtAsasistencia_horcont.setText("100");
		txtAsasistencia_horcont.setHorizontalAlignment(SwingConstants.CENTER);
		txtAsasistencia_horcont.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsasistencia_horcont.setBounds(87, 155, 69, 23);
		panConEsp.add(txtAsasistencia_horcont);
		
		JLabel lblModo = new JLabel();
		lblModo.setText("Modo");
		lblModo.setHorizontalAlignment(SwingConstants.CENTER);
		lblModo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblModo.setBounds(173, 127, 108, 20);
		panConEsp.add(lblModo);
		
		JLabel lblFecha = new JLabel();
		lblFecha.setText("Fecha");
		lblFecha.setHorizontalAlignment(SwingConstants.CENTER);
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFecha.setBounds(293, 14, 108, 20);
		panConEsp.add(lblFecha);
		
		cmbAsasistencia_modreg = new JComboBox();
		cmbAsasistencia_modreg.setBounds(166, 157, 109, 23);
		panConEsp.add(cmbAsasistencia_modreg);
		
		cmbPrueba = new JComboBox();
		cmbPrueba.setBounds(293, 157, 109, 23);
		panConEsp.add(cmbPrueba);
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setBounds(405, 211, 102, 40);
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdCerrarEvento();
			}
		});
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setText("Cerrar");
		cmdGuardar = new JButton();
		cmdGuardar.setBounds(10, 211, 102, 40);
		panelPrincipal.add(cmdGuardar);
		cmdGuardar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdGuardarEvento();
			}
		});
		cmdGuardar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdGuardar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png"));
		cmdGuardar.setText("Guardar");
		cmdGuardar.setMargin(new Insets(0, 0, 0, 0));
		cmdGuardar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the
		 * default look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DlgAsiasistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DlgAsiasistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DlgAsiasistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DlgAsiasistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		/*
		 * Create and display the dialog
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				DlgAsiasistencia dialog = new DlgAsiasistencia(null);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {

					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {

						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	public static javax.swing.JButton cmdCerrar;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JPanel panConEsp;
	public JTextFieldChanged txtAsasistencia_asicodigo;
	private JButton cmdGuardar;
	private List listaSubArea;
	private JTextFieldFormatoHora txtAsasistencia_ingreso;
	private Browse browseTurno;
	private BrowseObject browseObjectTurno;
	private List browseListTurno;
	private JTextEditorFecha txtEditAsasistencia_fecha;
	private JDateChooser cmbAsasistencia_fecha;
	private JTextFieldChanged txtAsasistencia_turno;
	private JTextFieldFormatoHora txtAsasistencia_salida;
	private JTextFieldChanged txtAsasistencia_ingestado;
	private JTextFieldChanged txtAsasistencia_salestado;
	private JTextFieldFormatoDecimal txtAsasistencia_hortrab;
	private JTextFieldFormatoDecimal txtAsasistencia_horcont;
	private JComboBox cmbAsasistencia_modreg;
	private JComboBox cmbPrueba;

	/******************************************************
	 * FUNCTIONES DE LA INTERFAZ VISTALISTENER
	 ******************************************************/
	@Override
	public void iniciarFormulario() {

		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
				llenarFormulario();
				refrescarFormulario();
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			frmAbierto = true;
			if(!this.isVisible()){this.setVisible(true);}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario()
	{
		txtAsasistencia_asicodigo.setText("");
		cmbAsasistencia_fecha.setDate(Util.getInst().getFechaSql());
		txtAsasistencia_turno.setText("");
		txtAsasistencia_ingreso.setText("");
		txtAsasistencia_ingestado.setText("");
		txtAsasistencia_salida.setText("");
		txtAsasistencia_salestado.setText("");
		txtAsasistencia_hortrab.setValue(0.00);
		txtAsasistencia_horcont.setValue(0.00);
		cmbAsasistencia_modreg.setSelectedIndex(-1);
	}

	@Override
	public void llenarFormulario() 
	{
		txtAsasistencia_asicodigo.setText(String.valueOf(aux.asiasis.getKyasistencia()));
		cmbAsasistencia_fecha.setDate(aux.asiasis.getFecha());
		txtAsasistencia_turno.setText(aux.asiasis.getTurnonombre());
		txtAsasistencia_ingreso.setText(aux.asiasis.getHoraingreso().toString());
		txtAsasistencia_ingestado.setText(aux.asiasis.getEstadoingreso());
		txtAsasistencia_salida.setText(aux.asiasis.getHorasalida().toString());
		txtAsasistencia_salestado.setText(aux.asiasis.getEstadosalida());
		txtAsasistencia_hortrab.setValue(aux.asiasis.getHoratrabajada());
		txtAsasistencia_horcont.setValue(aux.asiasis.getHoracomputada());
		cmbAsasistencia_modreg.setSelectedItem(aux.asiasis.getModoregistro());
		llenarCombo();
		
	}
	
	public void llenarCombo()
	{
		List listaNombre = (List) peticion.get("listaNombre");
		cmbAsasistencia_modreg.removeAllItems();
		for(int ite = 0; ite < listaNombre.size(); ite++)
		{
			Object[] registro = (Object[]) listaNombre.get(ite);
			cmbAsasistencia_modreg.addItem(registro[0]);
		}
		
	}

	@Override
	public void activarFormulario(boolean activo)
	{
		txtAsasistencia_asicodigo.setEnabled(false);
		cmbAsasistencia_fecha.setEnabled(activo);
		txtAsasistencia_turno.setEnabled(activo);
		txtAsasistencia_ingreso.setEnabled(activo);
		txtAsasistencia_ingestado.setEnabled(activo);
		txtAsasistencia_salida.setEnabled(activo);
		txtAsasistencia_salestado.setEnabled(activo);
		txtAsasistencia_hortrab.setEnabled(activo);
		txtAsasistencia_horcont.setEnabled(activo);
		cmbAsasistencia_modreg.setEnabled(activo);
	}

	@Override
	public void refrescarFormulario()
	{

		if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			limpiarFormulario();
			activarFormulario(false);
			llenarFormulario();
			cmdGuardar.setEnabled(false);
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			activarFormulario(true);
			llenarFormulario();
			txtAsasistencia_asicodigo.requestFocus();
			cmdGuardar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
			limpiarFormulario();
			llenarFormulario();
			activarFormulario(true);
			txtAsasistencia_asicodigo.requestFocus();
			cmdGuardar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
	}

	@Override
	public void obtenerDatoFormulario()
	{

		peticion.put("Pagina_paginaHijo", paginaHijo);
		peticion.put("Pagina_paginaPadre", paginaPadre);
		peticion.put("Pagina_evento", paginaEvento);
		peticion.put("Pagina_accion", paginaAccion);
		peticion.put("Pagina_valor", paginaValor);
		
		peticion.put("Asasistencia_asicodigo", txtAsasistencia_asicodigo.getText());
		peticion.put("Asasistencia_turno", txtAsasistencia_turno.getText());
		peticion.put("Asasistencia_ingreso", txtAsasistencia_ingreso.getText());
		peticion.put("Asasistencia_ingestado", txtAsasistencia_ingestado.getText());
		peticion.put("Asasistencia_salida", txtAsasistencia_salida.getText());
		peticion.put("Asasistencia_salestado", txtAsasistencia_salestado.getText());
		peticion.put("Asasistencia_hortrab", txtAsasistencia_hortrab.getText());
		peticion.put("Asasistencia_horcont", txtAsasistencia_horcont.getText());
		peticion.put("Asasistencia_modreg", cmbAsasistencia_modreg.getSelectedItem());
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			return true;
		}
		else if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

	}

	/*
	 * EVENTOS DE LOS CONTROLES DEL FORMULARIO
	 */
	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdGuardarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdGuardarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			refrescarFormulario();
//			this.dispose();
		}
	}

	public void cmdBrowseSeleccionarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdBrowseSeleccionarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return null;
	}
	
	public void cmdAgregarTurnoEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdAgregarTurnoEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			if(pagGen.pagAct.getPaginaHijo().equals(pagGen.pagNew.getPaginaHijo()))
			{
				refrescarFormulario();
			}
//			this.dispose();
		}
	}
	
	public void cmdModificarTurnoEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdModificarTurnoEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}
}