package com.asis.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import com.browse.Browse;
import com.browse.BrowseObject;
import com.comun.referencia.Acceso;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextAreaChanged;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.comun.utilidad.swing.JTextFieldFormatoHora;
import com.sis.browse.PanelTablaGeneral;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;
import com.toedter.calendar.IDateEditor;
import com.toedter.calendar.JDateChooser;

public class DlgAsctrlhorario extends VistaAdatper implements VistaListener {

	private JPanel panelPrincipal;
	private PanelTablaGeneral panTabGenCtrlhorarioTurno;
	private JPopupMenu popMenPanTabGenTurno;
	private JMenuItem menAgregarPanTabGenTurno;
	private JMenuItem menModificarPanTabGenTurno;
	private JMenuItem menEliminarPanTabGenTurno;
	private JTextEditorFecha txtEditAsctrlhorario_fecinicio;
	private JTextEditorFecha txtEditAsctrlhorario_fecfinal;

	private JTextEditorFecha txtEditAsihorario_fecha;
	private JTextEditorFecha txtEditAsihorario_fechainicio;
	private JTextEditorFecha txtEditAsihorario_fecejefinal;

	public DlgAsctrlhorario(FrmInicio parent) {

		super(parent);
		setTitle(this.getClass().getSimpleName());
		setResizable(false);
		
		configurarFechas();
		initComponents();
		configurarMenupopup();
		
		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		// Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));
		// Se pasa el conjunto de teclas al panel principal
		// super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);
		super.addWindowListener(new WindowAdapter() {

			@Override
			public void windowActivated(WindowEvent we) {

				frmAbierto = true;
			}
		});
	}

	private void configurarFechas()
	{
		txtEditAsctrlhorario_fecinicio = new JTextEditorFecha();
		txtEditAsctrlhorario_fecinicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditAsctrlhorario_fecinicio.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		
		txtEditAsctrlhorario_fecfinal = new JTextEditorFecha();
		txtEditAsctrlhorario_fecfinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditAsctrlhorario_fecfinal.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());

		txtEditAsihorario_fecha = new JTextEditorFecha();
		txtEditAsihorario_fecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditAsihorario_fecha.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		
		txtEditAsihorario_fechainicio = new JTextEditorFecha();
		txtEditAsihorario_fechainicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditAsihorario_fechainicio.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		
		txtEditAsihorario_fecejefinal = new JTextEditorFecha();
		txtEditAsihorario_fecejefinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditAsihorario_fecejefinal.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
	}
	
	private void configurarMenupopup()
	{
		popMenPanTabGenTurno = new JPopupMenu();

		menAgregarPanTabGenTurno = new JMenuItem("Agregar Registro");
		menAgregarPanTabGenTurno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				cmdAgregarTurnoEvento();
			}
		});
		menModificarPanTabGenTurno = new JMenuItem("Modificar Registro");
		menModificarPanTabGenTurno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
			}
		});
		menEliminarPanTabGenTurno = new JMenuItem("Eliminar Registro");
		menEliminarPanTabGenTurno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
			}
		});
		popMenPanTabGenTurno.add(menAgregarPanTabGenTurno);
		popMenPanTabGenTurno.add(menModificarPanTabGenTurno);
		popMenPanTabGenTurno.add(menEliminarPanTabGenTurno);		
	}
	
	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	private void initComponents() {

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		panelAdapter = new JPanel();
		panelAdapter.setLayout(null);
		panelAdapter.setOpaque(false);
		getContentPane().add(panelAdapter);
		panelPrincipal = new javax.swing.JPanel();
		panelPrincipal.setOpaque(false);
		panelPrincipal.setLayout(null);
		panelAdapter.add(panelPrincipal);
		panelPrincipal.setBounds(0, 0, 686, 565);
		panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
		setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
		setLocationRelativeTo(null);
		panConEsp = new javax.swing.JPanel();
		setTitle(this.getClass().getSimpleName() + " - Control de Horarios");
		panConEsp.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		panConEsp.setLayout(null);
		panelPrincipal.add(panConEsp);
		panConEsp.setBounds(10, 11, 667, 237);
		
		cmbAsihorario_fechainicio = new JDateChooser(txtEditAsihorario_fechainicio);
		cmbAsihorario_fechainicio.setDateFormatString("dd/MM/yyyy");
		cmbAsihorario_fechainicio.setBounds(10, 210, 115, 23);
		cmbAsihorario_fechainicio.setDate(new Date());
		panConEsp.add(cmbAsihorario_fechainicio);
		
		txtAsihorario_tipo = new JTextFieldChanged(100);
		txtAsihorario_tipo.setHorizontalAlignment(SwingConstants.CENTER);
		txtAsihorario_tipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsihorario_tipo.setBounds(10, 150, 121, 23);
		panConEsp.add(txtAsihorario_tipo);
		
		txtAsihorario_modo = new JTextFieldChanged(100);
		txtAsihorario_modo.setHorizontalAlignment(SwingConstants.CENTER);
		txtAsihorario_modo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsihorario_modo.setBounds(139, 150, 121, 23);
		panConEsp.add(txtAsihorario_modo);
		
		JLabel lblModo = new JLabel();
		lblModo.setText("Modo");
		lblModo.setHorizontalAlignment(SwingConstants.CENTER);
		lblModo.setForeground(Color.BLACK);
		lblModo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblModo.setBounds(139, 126, 121, 20);
		panConEsp.add(lblModo);
		
		JLabel lblFecha = new JLabel();
		lblFecha.setText("Fecha");
		lblFecha.setHorizontalAlignment(SwingConstants.CENTER);
		lblFecha.setForeground(Color.BLACK);
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFecha.setBounds(270, 126, 121, 20);
		panConEsp.add(lblFecha);
		
		cmbAsihorario_fecha = new JDateChooser(txtEditAsihorario_fecha);
		cmbAsihorario_fecha.setDateFormatString("dd/MM/yyyy");
		cmbAsihorario_fecha.setBounds(270, 150, 115, 23);
		cmbAsihorario_fecha.setDate(new Date());
		panConEsp.add(cmbAsihorario_fecha);
		
		JLabel lblFechaInicio = new JLabel();
		lblFechaInicio.setText("Fecha Inicio");
		lblFechaInicio.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaInicio.setForeground(Color.BLACK);
		lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFechaInicio.setBounds(10, 184, 121, 20);
		panConEsp.add(lblFechaInicio);
		
		JLabel lblFechaFinal = new JLabel();
		lblFechaFinal.setText("Fecha Final");
		lblFechaFinal.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaFinal.setForeground(Color.BLACK);
		lblFechaFinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFechaFinal.setBounds(139, 184, 121, 20);
		panConEsp.add(lblFechaFinal);
		
		cmbAsihorario_fecejefinal = new JDateChooser(txtEditAsihorario_fecejefinal);
		cmbAsihorario_fecejefinal.setDateFormatString("dd/MM/yyyy");
		cmbAsihorario_fecejefinal.setBounds(133, 208, 115, 23);
		cmbAsihorario_fecejefinal.setDate(new Date());
		panConEsp.add(cmbAsihorario_fecejefinal);
		
		JLabel lblAutor = new JLabel();
		lblAutor.setText("Autor");
		lblAutor.setHorizontalAlignment(SwingConstants.CENTER);
		lblAutor.setForeground(Color.BLACK);
		lblAutor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAutor.setBounds(264, 184, 121, 20);
		panConEsp.add(lblAutor);
		
		txtAsihorario_autor = new JTextFieldChanged(100);
		txtAsihorario_autor.setHorizontalAlignment(SwingConstants.CENTER);
		txtAsihorario_autor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsihorario_autor.setBounds(264, 208, 121, 23);
		panConEsp.add(txtAsihorario_autor);
		
		JLabel lblAprobador = new JLabel();
		lblAprobador.setText("Aprobador");
		lblAprobador.setHorizontalAlignment(SwingConstants.CENTER);
		lblAprobador.setForeground(Color.BLACK);
		lblAprobador.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAprobador.setBounds(401, 184, 256, 20);
		panConEsp.add(lblAprobador);
		
		txtAsihorario_aprobador = new JTextFieldChanged(100);
		txtAsihorario_aprobador.setHorizontalAlignment(SwingConstants.LEFT);
		txtAsihorario_aprobador.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsihorario_aprobador.setBounds(401, 208, 256, 23);
		panConEsp.add(txtAsihorario_aprobador);
		
		JLabel lblEstado = new JLabel();
		lblEstado.setText("Estado");
		lblEstado.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstado.setForeground(Color.BLACK);
		lblEstado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEstado.setBounds(401, 126, 256, 20);
		panConEsp.add(lblEstado);
		
		txtAsihorario_estado = new JTextFieldChanged(100);
		txtAsihorario_estado.setHorizontalAlignment(SwingConstants.LEFT);
		txtAsihorario_estado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsihorario_estado.setBounds(401, 150, 256, 23);
		panConEsp.add(txtAsihorario_estado);
		
		JLabel lblpapeletas = new JLabel();
		lblpapeletas.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				abrirPagina("DlgBrowseTabla","cmdBrowseSeleccionarEvento","com.asistencia.accion.BrowseSeleccionarPapeleta","SINCODIGO");
			}
		});
		lblpapeletas.setText("<html><u>Papeleta<u><html>");
		lblpapeletas.setHorizontalAlignment(SwingConstants.CENTER);
		lblpapeletas.setForeground(Color.BLUE);
		lblpapeletas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblpapeletas.setBounds(10, 126, 121, 20);
		panConEsp.add(lblpapeletas);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(10, 123, 647, 5);
		panConEsp.add(separator_3);
		
		JLabel lblhorario = new JLabel();
		lblhorario.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				abrirPagina("DlgBrowseTabla","cmdBrowseSeleccionarEvento","com.asistencia.accion.BrowseSeleccionarHorario","SINCODIGO");
			}
		});
		lblhorario.setBounds(10, 8, 115, 20);
		panConEsp.add(lblhorario);
		lblhorario.setText("<html><u>Horario<u><html>");
		lblhorario.setHorizontalAlignment(SwingConstants.CENTER);
		lblhorario.setForeground(Color.BLUE);
		lblhorario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel lblNombre = new JLabel();
		lblNombre.setBounds(135, 8, 141, 20);
		panConEsp.add(lblNombre);
		lblNombre.setText("Nombre");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		JLabel lblHoraInicio = new JLabel();
		lblHoraInicio.setBounds(279, 8, 92, 20);
		panConEsp.add(lblHoraInicio);
		lblHoraInicio.setText("Hora Inicio");
		lblHoraInicio.setHorizontalAlignment(SwingConstants.CENTER);
		lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAshorario_horfinal = new JTextFieldFormatoHora();
		txtAshorario_horfinal.setBounds(279, 36, 92, 23);
		panConEsp.add(txtAshorario_horfinal);
		txtAshorario_horfinal.setText("");
		txtAshorario_horfinal.setHorizontalAlignment(SwingConstants.CENTER);
		txtAshorario_horfinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		JLabel lblApellido_1 = new JLabel();
		lblApellido_1.setBounds(279, 70, 92, 20);
		panConEsp.add(lblApellido_1);
		lblApellido_1.setText("Hora Final");
		lblApellido_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblApellido_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAshorario_horinicio = new JTextFieldFormatoHora();
		txtAshorario_horinicio.setBounds(279, 95, 92, 23);
		panConEsp.add(txtAshorario_horinicio);
		txtAshorario_horinicio.setText("");
		txtAshorario_horinicio.setHorizontalAlignment(SwingConstants.CENTER);
		txtAshorario_horinicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel lblFechaFinal_1 = new JLabel();
		lblFechaFinal_1.setBounds(135, 71, 115, 20);
		panConEsp.add(lblFechaFinal_1);
		lblFechaFinal_1.setText("Fecha Final");
		lblFechaFinal_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaFinal_1.setForeground(Color.BLACK);
		lblFechaFinal_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		cmbAsctrlhorario_fecfinal = new JDateChooser(txtEditAsctrlhorario_fecfinal);
		cmbAsctrlhorario_fecfinal.setDateFormatString("dd/MM/yyyy");
		cmbAsctrlhorario_fecfinal.setBounds(135, 95, 115, 23);
		cmbAsctrlhorario_fecfinal.setDate(new Date());
		panConEsp.add(cmbAsctrlhorario_fecfinal);
		
		JLabel lblFechaInicio_1 = new JLabel();
		lblFechaInicio_1.setBounds(10, 71, 115, 20);
		panConEsp.add(lblFechaInicio_1);
		lblFechaInicio_1.setText("Fecha Inicio");
		lblFechaInicio_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaInicio_1.setForeground(Color.BLACK);
		lblFechaInicio_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		cmbAsctrlhorario_fecinicio = new JDateChooser(txtEditAsctrlhorario_fecinicio);
		cmbAsctrlhorario_fecinicio.setDateFormatString("dd/MM/yyyy");
		cmbAsctrlhorario_fecinicio.setBounds(10, 95, 115, 23);
		cmbAsctrlhorario_fecinicio.setDate(new Date());
		panConEsp.add(cmbAsctrlhorario_fecinicio);
		
		scrTxaAshorario_descripcion = new JScrollPane();
		scrTxaAshorario_descripcion.setBounds(381, 34, 280, 84);
		panConEsp.add(scrTxaAshorario_descripcion);
		txaAshorario_descripcion = new JTextAreaChanged(50);
		scrTxaAshorario_descripcion.setViewportView(txaAshorario_descripcion);
		
		JLabel lblEstado_1 = new JLabel();
		lblEstado_1.setText("Estado Control Horario");
		lblEstado_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEstado_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEstado_1.setBounds(381, 8, 155, 20);
		panConEsp.add(lblEstado_1);
		
		txtAsctrlhorario_estado = new JTextFieldChanged(20);
		txtAsctrlhorario_estado.setHorizontalAlignment(SwingConstants.CENTER);
		txtAsctrlhorario_estado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAsctrlhorario_estado.setBounds(546, 8, 115, 23);
		panConEsp.add(txtAsctrlhorario_estado);
		
		txtAshorario_tipo = new JTextFieldChanged(100);
		txtAshorario_tipo.setBounds(10, 36, 115, 23);
		panConEsp.add(txtAshorario_tipo);
		txtAshorario_tipo.setHorizontalAlignment(SwingConstants.LEFT);
		txtAshorario_tipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		txtAshorario_nombre = new JTextFieldChanged(100);
		txtAshorario_nombre.setBounds(133, 36, 143, 23);
		panConEsp.add(txtAshorario_nombre);
		txtAshorario_nombre.setHorizontalAlignment(SwingConstants.LEFT);
		txtAshorario_nombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		panelTablaCtrlhorarioTurno = new JPanel();
		panelTablaCtrlhorarioTurno.setBounds(10, 249, 667, 259);
		panelPrincipal.add(panelTablaCtrlhorarioTurno);
		panelTablaCtrlhorarioTurno.setLayout(null);
		panelTablaCtrlhorarioTurno.setBorder(null);
		
		panTabGenCtrlhorarioTurno = new PanelTablaGeneral(panelTablaCtrlhorarioTurno.getWidth(), panelTablaCtrlhorarioTurno.getHeight());
		panTabGenCtrlhorarioTurno.scrtablaBrowse.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent evt) 
			{
				panTabGenTurnoMosuseReleased(evt);
			}
		});
		panelTablaCtrlhorarioTurno.add(panTabGenCtrlhorarioTurno);
//		panTabGenTurno.cmdAgregar.addActionListener(new ActionListener() 
//		{
//			public void actionPerformed(ActionEvent e) 
//			{
//				cmdAgregarTurnoEvento();
//			}
//		});
		panTabGenCtrlhorarioTurno.tablaBrowse.addMouseListener(new java.awt.event.MouseAdapter() 
		{
//			public void mousePressed(java.awt.event.MouseEvent evt) {
//
//				panTabGenTablaBrowseMousePressed(evt);
//			}
			public void mouseReleased(java.awt.event.MouseEvent evt) {

				panTabGenTurnoMosuseReleased(evt);
			}
		});		
		cmdGuardar = new JButton();
		cmdGuardar.setBounds(10, 514, 102, 40);
		panelPrincipal.add(cmdGuardar);
		cmdGuardar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdGuardarEvento();
			}
		});
		cmdGuardar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdGuardar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png"));
		cmdGuardar.setText("Guardar");
		cmdGuardar.setMargin(new Insets(0, 0, 0, 0));
		cmdGuardar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setBounds(575, 516, 102, 40);
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				cmdCerrarEvento();
			}
		});
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setText("Cerrar");
		com.toedter.calendar.JDateChooser dateChooser = new com.toedter.calendar.JDateChooser((com.toedter.calendar.IDateEditor) null);
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the
		 * default look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DlgAsctrlhorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DlgAsctrlhorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DlgAsctrlhorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DlgAsctrlhorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		/*
		 * Create and display the dialog
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				DlgAsctrlhorario dialog = new DlgAsctrlhorario(null);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {

					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {

						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	public static javax.swing.JButton cmdCerrar;
	private javax.swing.JPanel panConEsp;
	private JButton cmdGuardar;
	private List listaSubArea;
	private JTextFieldFormatoHora txtAshorario_horinicio;
	private JTextFieldFormatoHora txtAshorario_horfinal;
	private JTextAreaChanged txaAshorario_descripcion;
	private Browse browseCtlrhorarioTurno;
	private BrowseObject browseObjectCtlrhorarioTurno;
	private List browseListCtlrhorarioTurno;
	private JScrollPane scrTxaAshorario_descripcion;
	private JPanel panelTablaCtrlhorarioTurno;
	private JDateChooser cmbAsctrlhorario_fecinicio;
	private JDateChooser cmbAsctrlhorario_fecfinal;
	private JTextFieldChanged txtAsctrlhorario_estado;
	private JDateChooser cmbAsihorario_fecha;
	private JDateChooser cmbAsihorario_fecejefinal;
	private JDateChooser cmbAsihorario_fechainicio;
	private JTextFieldChanged txtAsihorario_autor;
	private JTextFieldChanged txtAsihorario_aprobador;
	private JTextFieldChanged txtAsihorario_estado;
	private JTextFieldChanged txtAsihorario_tipo;
	private JTextFieldChanged txtAsihorario_modo;
	private JTextFieldChanged txtAshorario_tipo;
	private JTextFieldChanged txtAshorario_nombre;
	

	/******************************************************
	 * FUNCTIONES DE LA INTERFAZ VISTALISTENER
	 ******************************************************/
	@Override
	public void iniciarFormulario() {

		try
		{
			frmAbierto = false;
			aux = Auxiliar.getInstance();
			pagGen = PaginaGeneral.getInstance();
			paginaHijo = this.getClass().getSimpleName();
			pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
			if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR) || pagGen.pagAct.getAcceso().equals(Acceso.ACTUALIZAR))
			{
				llenarFormulario();
				refrescarFormulario();
			}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
			frmAbierto = true;
			if(!this.isVisible()){this.setVisible(true);}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void limpiarFormulario()
	{
		txtAshorario_tipo.setText("");
		txtAshorario_nombre.setText("");
		
		cmbAsctrlhorario_fecinicio.setDate(Util.getInst().getFechaSql());
		cmbAsctrlhorario_fecfinal.setDate(Util.getInst().getFechaSql());
		
		txtAshorario_horinicio.setText("");
		txtAshorario_horfinal.setText("");
		txaAshorario_descripcion.setText("");
		txtAsctrlhorario_estado.setText("");
		
		txtAsihorario_tipo.setText("");
		txtAsihorario_modo.setText("");
		
		cmbAsihorario_fecha.setDate(Util.getInst().getFechaSql());
		cmbAsihorario_fechainicio.setDate(Util.getInst().getFechaSql());
		cmbAsihorario_fecejefinal.setDate(Util.getInst().getFechaSql());
		txtAsihorario_autor.setText("");
		txtAsihorario_aprobador.setText("");
	}

	@Override
	public void llenarFormulario() 
	{
		/*txtAshorario_tipo.setText(aux.asiplanhora.getTipo());
		txtAshorario_nombre.setText(aux.asiplanhora.getNombre());
		
		cmbAsctrlhorario_fecinicio.setDate(aux.asihora.getFecinicio());
		cmbAsctrlhorario_fecfinal.setDate(aux.asihora.getFecfinal());
		
		txtAshorario_horinicio.setText(aux.asiplanhora.getHorinicio().toString());
		txtAshorario_horfinal.setText(aux.asiplanhora.getHorfinal().toString());
		txaAshorario_descripcion.setText(aux.asiplanhora.getDescripcion());
		txtAsctrlhorario_estado.setText(aux.asihora.getEstado());
		
		txtAsihorario_tipo.setText(aux.asihora.getTipo());
		txtAsihorario_modo.setText(aux.asihora.getModo());
		
		cmbAsihorario_fecha.setDate(aux.asihora.getFechaSql());
		cmbAsihorario_fechainicio.setDate(aux.asihora.getFecejeinicio());
		cmbAsihorario_fecejefinal.setDate(aux.asihora.getFecejefinal());
		txtAsihorario_autor.setText(aux.asihora.getAutor().toString());
		txtAsihorario_aprobador.setText(aux.asihora.getAprobador().toString());*/

		generarTablaCtrlhorarioTurno();
	}

	private void generarTablaCtrlhorarioTurno()
	{
		browseCtlrhorarioTurno = (Browse) peticion.get("browseCtrlhorarioTurno");
		browseObjectCtlrhorarioTurno = (BrowseObject) peticion.get("browseObjectCtrlhorarioTurno");
		browseListCtlrhorarioTurno = (List) peticion.get("browseListCtrlhorarioTurno");
		panTabGenCtrlhorarioTurno.crearModeloTabla(browseObjectCtlrhorarioTurno.getBrowseColumns(), this);
		panTabGenCtrlhorarioTurno.crearTablaResultado(browseCtlrhorarioTurno);
		panTabGenCtrlhorarioTurno.llenarRegistrosTabla(browseCtlrhorarioTurno, browseObjectCtlrhorarioTurno.getBrowseColumns(), browseListCtlrhorarioTurno);
		panTabGenCtrlhorarioTurno.refrescarControlPanTabGen(browseObjectCtlrhorarioTurno, pagGen);
	}

	@Override
	public void activarFormulario(boolean activo)
	{
		txtAshorario_tipo.setEnabled(false);
		txtAshorario_nombre.setEnabled(false);
		
		cmbAsctrlhorario_fecinicio.setEnabled(false);
		cmbAsctrlhorario_fecfinal.setEnabled(false);
		
		txtAshorario_horinicio.setEnabled(false);
		txtAshorario_horfinal.setEnabled(false);
		txaAshorario_descripcion.setEnabled(false);
		txtAsctrlhorario_estado.setEnabled(false);
		
		txtAsihorario_tipo.setEnabled(false);
		txtAsihorario_modo.setEnabled(false);
		
		cmbAsihorario_fecha.setEnabled(false);
		cmbAsihorario_fechainicio.setEnabled(false);
		cmbAsihorario_fecejefinal.setEnabled(false);
		txtAsihorario_autor.setEnabled(false);
		txtAsihorario_aprobador.setEnabled(false);
	}

	@Override
	public void refrescarFormulario()
	{

		if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			limpiarFormulario();
			activarFormulario(false);
			llenarFormulario();
			cmdGuardar.setEnabled(false);
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			activarFormulario(true);
			llenarFormulario();
			cmdGuardar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
		else if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
		{
			limpiarFormulario();
			llenarFormulario();
			activarFormulario(true);
			cmdGuardar.setEnabled(true);
			cmdCerrar.setEnabled(true);
		}
	}

	@Override
	public void obtenerDatoFormulario()
	{

		peticion.put("Pagina_paginaHijo", panTabGenCtrlhorarioTurno.paginaHijo);
		peticion.put("Pagina_paginaPadre", panTabGenCtrlhorarioTurno.paginaPadre);
		peticion.put("Pagina_evento", panTabGenCtrlhorarioTurno.paginaEvento);
		peticion.put("Pagina_accion", panTabGenCtrlhorarioTurno.paginaAccion);
		peticion.put("Pagina_valor", panTabGenCtrlhorarioTurno.paginaValor);
		
		peticion.put("Ashorario_tipo", txtAshorario_tipo.getText());
		peticion.put("Ashorario_nombre", txtAshorario_nombre.getText());		
		peticion.put("Ashorario_horinicio", txtAshorario_horinicio.getText());
		peticion.put("Ashorario_horfinal", txtAshorario_horfinal.getText());
		peticion.put("Ashorario_descripcion", txaAshorario_descripcion.getText());
	}

	public void obtenerDatoFormularioAshorario()
	{
		peticion.put("Pagina_paginaHijo", paginaHijo);
		peticion.put("Pagina_paginaPadre", paginaPadre);
		peticion.put("Pagina_evento", paginaEvento);
		peticion.put("Pagina_accion", paginaAccion);
		peticion.put("Pagina_valor", paginaValor);

		peticion.put("Ashorario_tipo", txtAshorario_tipo.getText());
		peticion.put("Ashorario_nombre", txtAshorario_nombre.getText());		
		peticion.put("Ashorario_horinicio", txtAshorario_horinicio.getText());
		peticion.put("Ashorario_horfinal", txtAshorario_horfinal.getText());
		peticion.put("Ashorario_descripcion", txaAshorario_descripcion.getText());
	}

	public void obtenerDatoFormularioAsihorario()
	{
		peticion.put("Ashorario_tipo", txtAshorario_tipo.getText());
		peticion.put("Ashorario_nombre", txtAshorario_nombre.getText());		
		peticion.put("Ashorario_horinicio", txtAshorario_horinicio.getText());
		peticion.put("Ashorario_horfinal", txtAshorario_horfinal.getText());
		peticion.put("Ashorario_descripcion", txaAshorario_descripcion.getText());
	}

	@Override
	public void obtenerDatoBaseDato() {

		// TODO Auto-generated method stub
	}

	@Override
	public boolean validarFormulario() {

		if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
		{
			return true;
		}
		else if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String modoGrabar) {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cerrarFormulario() {

	}

	/*
	 * EVENTOS DE LOS CONTROLES DEL FORMULARIO
	 */
	public void cmdCerrarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			ejecutar("cmdCerrarEvento", Peticion.AJAX);
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	public void cmdGuardarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdGuardarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			refrescarFormulario();
//			this.dispose();
		}
	}

	public void cmdBrowseSeleccionarEvento()
	{

		if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if(validarFormulario())
			{
				obtenerDatoFormularioAshorario();
				ejecutar("cmdBrowseSeleccionarEvento", Peticion.AJAX);
			}
		}
		else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			this.dispose();
		}
	}

	@Override
	public void setPeticion(Map peticion)
	{

		this.peticion = new HashMap();
		this.peticion = peticion;
	}

	@Override
	public JProgressBar obtenerBarraProgreso()
	{

		return null;
	}

	@Override
	public JLabel obtenerEtiAccionProgreso()
	{

		return null;
	}
	
	private void panTabGenTurnoMosuseReleased(java.awt.event.MouseEvent evt) 
	{// GEN-FIRST:event_panArtFotoMouseReleased
		if(evt.getButton() == MouseEvent.BUTTON3) 
		{
			popMenPanTabGenTurno.show(evt.getComponent(), evt.getX(), evt.getY());
			if(panTabGenCtrlhorarioTurno.tablaBrowse.getSelectedRow() > -1)
			{
				menEliminarPanTabGenTurno.setEnabled(true);
				menModificarPanTabGenTurno.setEnabled(true);
			}
			else
			{
				menEliminarPanTabGenTurno.setEnabled(false);
				menModificarPanTabGenTurno.setEnabled(false);
			}
		}// Fin de if (evt.getButton() == MouseEvent.BUTTON3)
	}// GEN-LAST:event_panArtFotoMouseReleased
	
	public void cmdAgregarTurnoEvento()
	{
		if (estadoEvento.equals(Modo.EVENTO_ENESPERA))
		{
			peticion = new HashMap();
			if (validarFormulario())
			{
				obtenerDatoFormulario();
				ejecutar("cmdAgregarTurnoEvento", Peticion.AJAX);
			}
		}
		else if (estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
		{
			estadoEvento = Modo.EVENTO_ENESPERA;
			if(pagGen.pagAct.getPaginaHijo().equals(pagGen.pagNew.getPaginaHijo()))
			{
				refrescarFormulario();
			}
//			this.dispose();
		}
	}
}