package com.asis.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import com.browse.Browse;
import com.browse.BrowseObject;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.Modo;
import com.comun.referencia.Peticion;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoHora;
import com.sis.main.PaginaGeneral;
import com.sis.main.VistaAdatper;
import com.sis.main.VistaListener;
import com.sis.produccion.FrmInicio;

public class DlgAsiplantillaturno extends VistaAdatper implements VistaListener {

  private JPanel panelPrincipal;
  private JButton cmdCerrar;

  public DlgAsiplantillaturno(FrmInicio parent) {

    super(parent);
    setResizable(false);
    initComponents();
    // Conjunto de teclas que queremos que sirvan para pasar el foco
    // al siguiente campo de texto: ENTER y TAB
    // Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
    // teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
    // teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));
    // Se pasa el conjunto de teclas al panel principal
    // super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);
    super.addWindowListener(new WindowAdapter() {

      @Override
      public void windowActivated(WindowEvent we) {

        frmAbierto = true;
      }
    });
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  private void initComponents() {

    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    getContentPane().setLayout(null);
    panelAdapter = new JPanel();
    panelAdapter.setLayout(null);
    panelAdapter.setOpaque(false);
    getContentPane().add(panelAdapter);
    panelPrincipal = new javax.swing.JPanel();
    panelPrincipal.setOpaque(false);
    panelPrincipal.setLayout(null);
    panelAdapter.add(panelPrincipal);
    panelPrincipal.setBounds(0, 0, 541, 322);
    panelAdapter.setBounds(0, 0, panelPrincipal.getWidth(), panelPrincipal.getHeight());
    setSize(panelPrincipal.getWidth(), panelPrincipal.getHeight() + 30);
    setLocationRelativeTo(null);
    panConEsp = new javax.swing.JPanel();
    jLabel2 = new javax.swing.JLabel();
    jLabel2.setForeground(Color.BLACK);
    jLabel2.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_turcodigo = new JTextFieldChanged(11);
    txtAsiturno_turcodigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
    setTitle(this.getClass().getSimpleName() + " - Horarios");
    panConEsp.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    panConEsp.setLayout(null);
    jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel2.setText("Codigo");
    panConEsp.add(jLabel2);
    jLabel2.setBounds(10, 11, 121, 20);
    txtAsiturno_turcodigo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
    txtAsiturno_turcodigo.setText("12345678901234");
    txtAsiturno_turcodigo.addKeyListener(new java.awt.event.KeyAdapter() {

      public void keyReleased(java.awt.event.KeyEvent evt) {

      }
    });
    panConEsp.add(txtAsiturno_turcodigo);
    txtAsiturno_turcodigo.setBounds(10, 35, 121, 23);
    panelPrincipal.add(panConEsp);
    panConEsp.setBounds(10, 72, 520, 185);
    JLabel lblHoraInicio = new JLabel();
    lblHoraInicio.setText("Limit-Ent 1");
    lblHoraInicio.setHorizontalAlignment(SwingConstants.CENTER);
    lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblHoraInicio.setBounds(262, 69, 76, 20);
    panConEsp.add(lblHoraInicio);
    JLabel lblApellido_1 = new JLabel();
    lblApellido_1.setText("Entrada");
    lblApellido_1.setHorizontalAlignment(SwingConstants.CENTER);
    lblApellido_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblApellido_1.setBounds(176, 69, 76, 20);
    panConEsp.add(lblApellido_1);
    
    JLabel lblLimitent = new JLabel();
    lblLimitent.setText("Limit-Ent 2");
    lblLimitent.setHorizontalAlignment(SwingConstants.CENTER);
    lblLimitent.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblLimitent.setBounds(348, 69, 76, 20);
    panConEsp.add(lblLimitent);
    
    JLabel lblLimitent_1 = new JLabel();
    lblLimitent_1.setText("Limit-Ent");
    lblLimitent_1.setHorizontalAlignment(SwingConstants.CENTER);
    lblLimitent_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblLimitent_1.setBounds(434, 69, 76, 20);
    panConEsp.add(lblLimitent_1);
    
    cmbAsiturno_tipo = new JComboBox();
    cmbAsiturno_tipo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmbAsiturno_tipo.setModel(new DefaultComboBoxModel(new String[] {"LABORABLE", "FERIADO"}));
    cmbAsiturno_tipo.setBounds(420, 35, 90, 23);
    panConEsp.add(cmbAsiturno_tipo);
    
    lblTipo = new JLabel();
    lblTipo.setText("Tipo");
    lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
    lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblTipo.setBounds(420, 11, 90, 20);
    panConEsp.add(lblTipo);
    
    JLabel lblLimitsal = new JLabel();
    lblLimitsal.setBounds(171, 131, 76, 20);
    panConEsp.add(lblLimitsal);
    lblLimitsal.setText("Limit-Sal 1");
    lblLimitsal.setHorizontalAlignment(SwingConstants.CENTER);
    lblLimitsal.setFont(new Font("Tahoma", Font.PLAIN, 14));
    
    JLabel lblLimitsal_2 = new JLabel();
    lblLimitsal_2.setBounds(348, 131, 76, 20);
    panConEsp.add(lblLimitsal_2);
    lblLimitsal_2.setText("Limit-Sal");
    lblLimitsal_2.setHorizontalAlignment(SwingConstants.CENTER);
    lblLimitsal_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
    
    JLabel lblLimitsal_1 = new JLabel();
    lblLimitsal_1.setBounds(262, 131, 76, 20);
    panConEsp.add(lblLimitsal_1);
    lblLimitsal_1.setText("Limit-Sal 2");
    lblLimitsal_1.setHorizontalAlignment(SwingConstants.CENTER);
    lblLimitsal_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
    
    JLabel lblSalida = new JLabel();
    lblSalida.setBounds(434, 131, 76, 20);
    panConEsp.add(lblSalida);
    lblSalida.setText("Salida");
    lblSalida.setHorizontalAlignment(SwingConstants.CENTER);
    lblSalida.setFont(new Font("Tahoma", Font.PLAIN, 14));
    
    txtAsiturno_entrada = new JTextFieldFormatoHora();
    txtAsiturno_entrada.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent e) {
        JTextFieldFormatoHora texto = (JTextFieldFormatoHora) e.getSource();
        Time horaEntrada = Util.getInst().fromStrdateToTime(texto.getText());
        Time horaLimEntUno = new Time(horaEntrada.getHours(), horaEntrada.getMinutes() + 5, horaEntrada.getSeconds());
        Time horaLimEntDos = new Time(horaEntrada.getHours(), horaEntrada.getMinutes() + 10, horaEntrada.getSeconds());
        Time horaLimEntrada = new Time(horaEntrada.getHours(), horaEntrada.getMinutes() + 15, horaEntrada.getSeconds());
        txtAsiturno_limentuno.setText(horaLimEntUno.toString());
        txtAsiturno_limentdos.setText(horaLimEntDos.toString());
        txtAsiturno_limentrada.setText(horaLimEntrada.toString());
      }
    });
    txtAsiturno_entrada.setText("05:10:00");
    txtAsiturno_entrada.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsiturno_entrada.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_entrada.setBounds(176, 92, 76, 23);
    panConEsp.add(txtAsiturno_entrada);
    
    txtAsiturno_limentuno = new JTextFieldFormatoHora();
    txtAsiturno_limentuno.setText("05:10:00");
    txtAsiturno_limentuno.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsiturno_limentuno.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_limentuno.setBounds(262, 92, 76, 23);
    panConEsp.add(txtAsiturno_limentuno);
    
    txtAsiturno_limentdos = new JTextFieldFormatoHora();
    txtAsiturno_limentdos.setText("05:10:00");
    txtAsiturno_limentdos.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsiturno_limentdos.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_limentdos.setBounds(348, 92, 76, 23);
    panConEsp.add(txtAsiturno_limentdos);
    
    txtAsiturno_limentrada = new JTextFieldFormatoHora();
    txtAsiturno_limentrada.setText("05:10:00");
    txtAsiturno_limentrada.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsiturno_limentrada.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_limentrada.setBounds(434, 92, 76, 23);
    panConEsp.add(txtAsiturno_limentrada);
    
    txtAsiturno_limsaluno = new JTextFieldFormatoHora();
    txtAsiturno_limsaluno.setText("05:10:00");
    txtAsiturno_limsaluno.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsiturno_limsaluno.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_limsaluno.setBounds(171, 151, 76, 23);
    panConEsp.add(txtAsiturno_limsaluno);
    
    txtAsiturno_limsaldos = new JTextFieldFormatoHora();
    txtAsiturno_limsaldos.setText("05:10:00");
    txtAsiturno_limsaldos.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsiturno_limsaldos.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_limsaldos.setBounds(262, 151, 76, 23);
    panConEsp.add(txtAsiturno_limsaldos);
    
    txtAsiturno_limsalida = new JTextFieldFormatoHora();
    txtAsiturno_limsalida.setText("05:10:00");
    txtAsiturno_limsalida.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsiturno_limsalida.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_limsalida.setBounds(348, 151, 76, 23);
    panConEsp.add(txtAsiturno_limsalida);
    
    txtAsiturno_salida = new JTextFieldFormatoHora();
    txtAsiturno_salida.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent e) {
        JTextFieldFormatoHora texto = (JTextFieldFormatoHora) e.getSource();
        Time horaSalida = Util.getInst().fromStrdateToTime(texto.getText());
        Time horaLimSalUno = new Time(horaSalida.getHours(), horaSalida.getMinutes() - 15, horaSalida.getSeconds());
        Time horaLimSalDos = new Time(horaSalida.getHours(), horaSalida.getMinutes() - 10, horaSalida.getSeconds());
        Time horaLimSalida = new Time(horaSalida.getHours(), horaSalida.getMinutes() - 5, horaSalida.getSeconds());
        txtAsiturno_limsaluno.setText(horaLimSalUno.toString());
        txtAsiturno_limsaldos.setText(horaLimSalDos.toString());
        txtAsiturno_limsalida.setText(horaLimSalida.toString());
      }
    });
    txtAsiturno_salida.setText("05:10:00");
    txtAsiturno_salida.setHorizontalAlignment(SwingConstants.CENTER);
    txtAsiturno_salida.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAsiturno_salida.setBounds(434, 151, 76, 23);
    panConEsp.add(txtAsiturno_salida);
    
    cmbAsiturno_nombre = new JComboBox();
    cmbAsiturno_nombre.setBounds(141, 35, 102, 23);
    panConEsp.add(cmbAsiturno_nombre);
    cmbAsiturno_nombre.setName("turno");
    cmbAsiturno_nombre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmbAsiturno_nombre.setModel(new DefaultComboBoxModel(new String[] {"TURNO-A", "TURNO-B", "TURNO-C"}));
    
    lblDia = new JLabel();
    lblDia.setBounds(141, 11, 97, 20);
    panConEsp.add(lblDia);
    lblDia.setText("Nombre");
    lblDia.setHorizontalAlignment(SwingConstants.CENTER);
    lblDia.setFont(new Font("Tahoma", Font.PLAIN, 14));
    
    cmbAsiturno_dia = new JComboBox();
    cmbAsiturno_dia.setBounds(295, 37, 97, 23);
    panConEsp.add(cmbAsiturno_dia);
    cmbAsiturno_dia.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmbAsiturno_dia.setModel(new DefaultComboBoxModel(new String[] {"DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO"}));
    
    JLabel lblDia_1 = new JLabel();
    lblDia_1.setText("Dia");
    lblDia_1.setHorizontalAlignment(SwingConstants.CENTER);
    lblDia_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblDia_1.setBounds(295, 16, 97, 20);
    panConEsp.add(lblDia_1);
    
    cmbDiaEntrada = new JComboBox();
    cmbDiaEntrada.setFont(new Font("Tahoma", Font.PLAIN, 14));
    cmbDiaEntrada.setModel(new DefaultComboBoxModel(new String[] {"1970-1-1", "1970-1-2"}));
    cmbDiaEntrada.setName("turno");
    cmbDiaEntrada.setBounds(10, 92, 102, 23);
    panConEsp.add(cmbDiaEntrada);
    
    cmbDiaSalida = new JComboBox();
    cmbDiaSalida.setFont(new Font("Tahoma", Font.PLAIN, 14));
    cmbDiaSalida.setModel(new DefaultComboBoxModel(new String[] {"1970-1-1", "1970-1-2"}));
    cmbDiaSalida.setName("turno");
    cmbDiaSalida.setBounds(10, 154, 102, 23);
    panConEsp.add(cmbDiaSalida);
    
    JLabel lblDia_2 = new JLabel();
    lblDia_2.setText("Dia Entrada");
    lblDia_2.setHorizontalAlignment(SwingConstants.CENTER);
    lblDia_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblDia_2.setBounds(10, 69, 97, 20);
    panConEsp.add(lblDia_2);
    
    JLabel lblDia_3 = new JLabel();
    lblDia_3.setText("Dia Salida");
    lblDia_3.setHorizontalAlignment(SwingConstants.CENTER);
    lblDia_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
    lblDia_3.setBounds(10, 131, 97, 20);
    panConEsp.add(lblDia_3);
    cmdGuardar = new JButton();
    cmdGuardar.setBounds(10, 268, 102, 40);
    panelPrincipal.add(cmdGuardar);
    cmdGuardar.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {

        cmdGuardarEvento();
      }
    });
    cmdGuardar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmdGuardar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png"));
    cmdGuardar.setText("Guardar");
    cmdGuardar.setMargin(new Insets(0, 0, 0, 0));
    cmdGuardar.setFont(new Font("Tahoma", Font.PLAIN, 14));
    cmdCerrar = new javax.swing.JButton();
    cmdCerrar.setBounds(428, 270, 102, 40);
    panelPrincipal.add(cmdCerrar);
    cmdCerrar.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {

        cmdCerrarEvento();
      }
    });
    cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
    cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
    cmdCerrar.setText("Cerrar");
    
    txtAshorario_tipo = new JTextFieldChanged(11);
    txtAshorario_tipo.setHorizontalAlignment(SwingConstants.CENTER);
    txtAshorario_tipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAshorario_tipo.setBounds(135, 38, 117, 23);
    panelPrincipal.add(txtAshorario_tipo);
    
    JLabel label = new JLabel();
    label.setText("Codigo");
    label.setHorizontalAlignment(SwingConstants.CENTER);
    label.setForeground(Color.BLACK);
    label.setFont(new Font("Tahoma", Font.PLAIN, 14));
    label.setBounds(10, 14, 121, 20);
    panelPrincipal.add(label);
    
    txtAshorario_horcodigo = new JTextFieldChanged(11);
    txtAshorario_horcodigo.setText("12345678901234");
    txtAshorario_horcodigo.setHorizontalAlignment(SwingConstants.CENTER);
    txtAshorario_horcodigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAshorario_horcodigo.setBounds(10, 38, 121, 23);
    panelPrincipal.add(txtAshorario_horcodigo);
    
    JLabel label_1 = new JLabel();
    label_1.setText("Tipo");
    label_1.setHorizontalAlignment(SwingConstants.CENTER);
    label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
    label_1.setBounds(135, 14, 117, 20);
    panelPrincipal.add(label_1);
    
    JLabel label_2 = new JLabel();
    label_2.setText("Nombre");
    label_2.setHorizontalAlignment(SwingConstants.CENTER);
    label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
    label_2.setBounds(259, 14, 271, 20);
    panelPrincipal.add(label_2);
    
    txtAshorario_nombre = new JTextFieldChanged(11);
    txtAshorario_nombre.setHorizontalAlignment(SwingConstants.CENTER);
    txtAshorario_nombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
    txtAshorario_nombre.setBounds(259, 38, 271, 23);
    panelPrincipal.add(txtAshorario_nombre);
    
    chkAsiturno_dia = new JCheckBox[7];
    
    chkAsiturno_dia[0] = new JCheckBox("DOMINGO");
    chkAsiturno_dia[0].setBounds(119, 262, 68, 23);
    chkAsiturno_dia[0].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    chkAsiturno_dia[0].setName("1DOM");
    panelPrincipal.add(chkAsiturno_dia[0]);
    
    chkAsiturno_dia[1] = new JCheckBox("LUNES");
    chkAsiturno_dia[1].setBounds(119, 285, 68, 23);
    chkAsiturno_dia[1].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    chkAsiturno_dia[1].setName("2LUN");
    panelPrincipal.add(chkAsiturno_dia[1]);
    
    chkAsiturno_dia[2] = new JCheckBox("MARTES");
    chkAsiturno_dia[2].setBounds(189, 264, 83, 23);
    chkAsiturno_dia[2].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    chkAsiturno_dia[2].setName("3MAR");
    panelPrincipal.add(chkAsiturno_dia[2]);
    
    chkAsiturno_dia[3] = new JCheckBox("MIERCOLES");
    chkAsiturno_dia[3].setBounds(189, 285, 83, 23);
    chkAsiturno_dia[3].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    chkAsiturno_dia[3].setName("4MIE");
    panelPrincipal.add(chkAsiturno_dia[3]);
    
    chkAsiturno_dia[4] = new JCheckBox("JUEVES");
    chkAsiturno_dia[4].setBounds(274, 264, 68, 23);
    chkAsiturno_dia[4].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    chkAsiturno_dia[4].setName("5JUE");
    panelPrincipal.add(chkAsiturno_dia[4]);
    
    chkAsiturno_dia[5] = new JCheckBox("VIERNES");
    chkAsiturno_dia[5].setBounds(274, 285, 68, 23);
    chkAsiturno_dia[5].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    chkAsiturno_dia[5].setName("6VIE");
    panelPrincipal.add(chkAsiturno_dia[5]);
    
    chkAsiturno_dia[6] = new JCheckBox("SABADO");
    chkAsiturno_dia[6].setBounds(349, 264, 83, 23);
    chkAsiturno_dia[6].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    chkAsiturno_dia[6].setName("7SAB");
    panelPrincipal.add(chkAsiturno_dia[6]);
    
    chkTodos = new JCheckBox("TODOS");
    chkTodos.setBounds(349, 285, 73, 23);
    chkTodos.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) 
      {
        JCheckBox chkBox = (JCheckBox) e.getSource();
        if(chkBox.isSelected())
        {
          chkAsiturno_dia[0].setSelected(true);
          chkAsiturno_dia[1].setSelected(true);
          chkAsiturno_dia[2].setSelected(true);
          chkAsiturno_dia[3].setSelected(true);
          chkAsiturno_dia[4].setSelected(true);
          chkAsiturno_dia[5].setSelected(true);
          chkAsiturno_dia[6].setSelected(true);
        }
        else
        {
          chkAsiturno_dia[0].setSelected(false);
          chkAsiturno_dia[1].setSelected(false);
          chkAsiturno_dia[2].setSelected(false);
          chkAsiturno_dia[3].setSelected(false);
          chkAsiturno_dia[4].setSelected(false);
          chkAsiturno_dia[5].setSelected(false);
          chkAsiturno_dia[6].setSelected(false);
        }
      }
    });
    panelPrincipal.add(chkTodos);
    com.toedter.calendar.JDateChooser dateChooser = new com.toedter.calendar.JDateChooser((com.toedter.calendar.IDateEditor) null);
  }// </editor-fold>//GEN-END:initComponents

  /**
   * @param args
   *            the command line arguments
   */
  public static void main(String args[]) {

    /*
     * Set the Nimbus look and feel
     */
    // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /*
     * If Nimbus (introduced in Java SE 6) is not available, stay with the
     * default look and feel. For details see
     * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
     */
    try {
      for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(DlgAsiplantillahorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(DlgAsiplantillahorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(DlgAsiplantillahorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(DlgAsiplantillahorario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    // </editor-fold>
    /*
     * Create and display the dialog
     */
    java.awt.EventQueue.invokeLater(new Runnable() {

      public void run() {

        DlgAsiplantillaturno dialog = new DlgAsiplantillaturno(null);
        dialog.addWindowListener(new java.awt.event.WindowAdapter() {

          @Override
          public void windowClosing(java.awt.event.WindowEvent e) {

            System.exit(0);
          }
        });
        dialog.setVisible(true);
      }
    });
  }

  private javax.swing.JLabel jLabel2;
  private javax.swing.JPanel panConEsp;
  public JTextFieldChanged txtAsiturno_turcodigo;
  private JButton cmdGuardar;
  private List listaSubArea;
  private Browse browseTurno;
  private BrowseObject browseObjectTurno;
  private List browseListTurno;
  private JTextFieldChanged txtAshorario_tipo;
  private JTextFieldChanged txtAshorario_nombre;
  private JTextFieldChanged txtAshorario_horcodigo;
  private JComboBox cmbAsiturno_dia;
  private JLabel lblDia;
  private JComboBox cmbAsiturno_tipo;
  private JLabel lblTipo;
  private JComboBox cmbAsiturno_nombre;
  private JTextFieldFormatoHora txtAsiturno_entrada;
  private JTextFieldFormatoHora txtAsiturno_limentuno;
  private JTextFieldFormatoHora txtAsiturno_limentdos;
  private JTextFieldFormatoHora txtAsiturno_limentrada;
  private JTextFieldFormatoHora txtAsiturno_limsaluno;
  private JTextFieldFormatoHora txtAsiturno_limsaldos;
  private JTextFieldFormatoHora txtAsiturno_limsalida;
  private JTextFieldFormatoHora txtAsiturno_salida;
  private JCheckBox[] chkAsiturno_dia;
  private JCheckBox chckbxTurnob;
  private JCheckBox chckbxTurnoc;
  private JCheckBox chkTodos;
  private JComboBox cmbDiaEntrada;
  private JComboBox cmbDiaSalida;
  

  /******************************************************
   * FUNCTIONES DE LA INTERFAZ VISTALISTENER
   ******************************************************/
  @Override
  public void iniciarFormulario() {

    try
    {
      frmAbierto = false;
      aux = Auxiliar.getInstance();
      pagGen = PaginaGeneral.getInstance();
      paginaHijo = this.getClass().getSimpleName();
      pagGen.pagAct.setObjeto(pagGen.obtenerPagina(paginaHijo));
//      if(pagGen.pagAct.getAcceso().equals(Acceso.INICIAR))
//      {
        refrescarFormulario();
//      }// Fin de if(acceso.equals(AccesoPagina.INICIAR))
      frmAbierto = true;
      this.show();
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  @Override
  public void limpiarFormulario()
  {
    txtAshorario_horcodigo.setText("");
    txtAshorario_tipo.setText("");
    txtAshorario_nombre.setText("");
    txtAsiturno_turcodigo.setText("");
    
    cmbAsiturno_nombre.setSelectedIndex(0);
    cmbAsiturno_tipo.setSelectedIndex(0);
    cmbAsiturno_dia.setSelectedIndex(0);
    
    cmbDiaEntrada.setSelectedIndex(0);
    cmbDiaSalida.setSelectedIndex(0);
    
    txtAsiturno_entrada.setText("00:00:00");
    txtAsiturno_limentuno.setText("00:00:00");
    txtAsiturno_limentdos.setText("00:00:00");
    txtAsiturno_limentdos.setText("00:00:00");
    txtAsiturno_limentrada.setText("00:00:00");
    txtAsiturno_limsaluno.setText("00:00:00");
    txtAsiturno_limsaldos.setText("00:00:00");
    txtAsiturno_limsalida.setText("00:00:00");
    txtAsiturno_salida.setText("00:00:00");
  }

  @Override
  public void llenarFormulario() 
  {
    /*txtAshorario_horcodigo.setText(aux.asiplanhora.getHorcodigo().toString());
    txtAshorario_tipo.setText(aux.asiplanhora.getTipo());
    txtAshorario_nombre.setText(aux.asiplanhora.getNombre());
    
    txtAsiturno_turcodigo.setText(aux.asiturn.getTurcodigo().toString());

    cmbAsiturno_nombre.setSelectedItem(aux.asiturn.getNombre());
    cmbAsiturno_tipo.setSelectedItem(aux.asiturn.getTipo());
    cmbAsiturno_dia.setSelectedItem(aux.asiturn.getDia());
    
    cmbDiaEntrada.setSelectedItem((aux.asiturn.getEntrada().getYear()+1900)+"-"+(aux.asiturn.getEntrada().getMonth()+1)+"-"+aux.asiturn.getEntrada().getDate());
    cmbDiaSalida.setSelectedItem((aux.asiturn.getSalida().getYear()+1900)+"-"+(aux.asiturn.getSalida().getMonth()+1)+"-"+aux.asiturn.getSalida().getDate());
    
    txtAsiturno_entrada.setText(Util.getInst().fromTimeToString(aux.asiturn.getEntrada()));
    txtAsiturno_limentuno.setText(Util.getInst().fromTimeToString(aux.asiturn.getLimentuno()));
    txtAsiturno_limentdos.setText(Util.getInst().fromTimeToString(aux.asiturn.getLimentdos()));
    txtAsiturno_limentrada.setText(Util.getInst().fromTimeToString(aux.asiturn.getLimentrada()));
    txtAsiturno_limsaluno.setText(Util.getInst().fromTimeToString(aux.asiturn.getLimsaluno()));
    txtAsiturno_limsaldos.setText(Util.getInst().fromTimeToString(aux.asiturn.getLimsaldos()));
    txtAsiturno_limsalida.setText(Util.getInst().fromTimeToString(aux.asiturn.getLimsalida()));
    txtAsiturno_salida.setText(Util.getInst().fromTimeToString(aux.asiturn.getSalida()));*/
  }

  @Override
  public void activarFormulario(boolean activo)
  {
    txtAshorario_horcodigo.setEnabled(false);
    txtAshorario_tipo.setEnabled(false);
    txtAshorario_nombre.setEnabled(false);
    
    txtAsiturno_turcodigo.setEnabled(false);
    cmbAsiturno_nombre.setEnabled(activo);
    cmbAsiturno_tipo.setEnabled(activo);
    cmbAsiturno_dia.setEnabled(activo);
    
    cmbDiaEntrada.setEnabled(activo);
    cmbDiaSalida.setEnabled(activo);
    
    txtAsiturno_entrada.setEnabled(activo);
    txtAsiturno_limentuno.setEnabled(activo);
    txtAsiturno_limentdos.setEnabled(activo);
    txtAsiturno_limentdos.setEnabled(activo);
    txtAsiturno_limentrada.setEnabled(activo);
    txtAsiturno_limsaluno.setEnabled(activo);
    txtAsiturno_limsaldos.setEnabled(activo);
    txtAsiturno_limsalida.setEnabled(activo);
    txtAsiturno_salida.setEnabled(activo);
    if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
    {
      cmbAsiturno_nombre.setEnabled(true);
      cmbAsiturno_dia.setEnabled(false);
      for(int iteDia = 0; iteDia < chkAsiturno_dia.length; iteDia++)
      {
        chkAsiturno_dia[iteDia].setEnabled(activo);
      }
      chkTodos.setEnabled(activo);
    }
    else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
    {
      cmbAsiturno_nombre.setEnabled(false);
      cmbAsiturno_dia.setEnabled(false);
      for(int iteDia = 0; iteDia < chkAsiturno_dia.length; iteDia++)
      {
        chkAsiturno_dia[iteDia].setSelected(false);
        chkAsiturno_dia[iteDia].setEnabled(false);
      }
      chkTodos.setSelected(false);
      chkTodos.setEnabled(false);
    }
  }

  @Override
  public void refrescarFormulario()
  {

    if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
    {
      limpiarFormulario();
      activarFormulario(false);
      llenarFormulario();
      cmdGuardar.setEnabled(false);
      cmdCerrar.setEnabled(true);
    }
    else if(pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
    {
      activarFormulario(true);
      llenarFormulario();
      txtAsiturno_turcodigo.requestFocus();
      cmdGuardar.setEnabled(true);
      cmdCerrar.setEnabled(true);
    }
    else if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO))
    {
      limpiarFormulario();
      llenarFormulario();
      activarFormulario(true);
      txtAsiturno_turcodigo.requestFocus();
      cmdGuardar.setEnabled(true);
      cmdCerrar.setEnabled(true);
    }
  }

  @Override
  public void obtenerDatoFormulario()
  {

    peticion.put("Pagina_paginaHijo", paginaHijo);
    peticion.put("Pagina_paginaPadre", paginaPadre);
    peticion.put("Pagina_evento", paginaEvento);
    peticion.put("Pagina_accion", paginaAccion);
    peticion.put("Pagina_valor", paginaValor);
    peticion.put("Usuario_usucodigo", txtAsiturno_turcodigo.getText());
    peticion.put("Usuario_orgcodigo", OrganizacionGeneral.getOrgcodigo().toUpperCase());
    // peticion.put("Usuario_fechaingreso", Util.getInst().fromDateToString(Util.getInst().fromDateToDateSql(cmbUsuarioFechaingreso.getDate()), "dd/MM/yyyy"));
    // peticion.put("Usuario_estado", Estado.toClave(txtUsuarioEstado.getText()));
    peticion.put("Asiturno_turcodigo", txtAsiturno_turcodigo.getText());
    peticion.put("Asiturno_horcodigo", aux.asiplanhora.getKyplantillahorario());

    List listaDia = new ArrayList();
    List listaIdentidad = new ArrayList();
    for(int iteChkDia = 0; iteChkDia < chkAsiturno_dia.length; iteChkDia++)
    {
      JCheckBox chkDia = chkAsiturno_dia[iteChkDia];
      if(chkDia.isSelected())
      {
        String identidad = chkAsiturno_dia[iteChkDia].getName() + cmbAsiturno_nombre.getSelectedItem().toString();
        listaDia.add(chkDia.getText());
        listaIdentidad.add(identidad);
      }
    }
    
    String identidad = ( cmbAsiturno_dia.getSelectedIndex() + 1 ) + cmbAsiturno_dia.getSelectedItem().toString().substring(0, 3) + cmbAsiturno_nombre.getSelectedItem().toString();
    peticion.put("Asiturno_identidad", identidad);
    peticion.put("Asiturno_nombre", cmbAsiturno_nombre.getSelectedItem().toString());
    peticion.put("Asiturno_tipo", cmbAsiturno_tipo.getSelectedItem().toString());
    peticion.put("Asiturno_dia", cmbAsiturno_dia.getSelectedItem().toString());
    peticion.put("listaDia", listaDia);
    peticion.put("listaIdentidad", listaIdentidad);
    
    validarFechaHora();
    
    /*peticion.put("Asiturno_entrada", aux.asiturn.getEntrada().toString());
    peticion.put("Asiturno_limentuno", aux.asiturn.getLimentuno().toString());
    peticion.put("Asiturno_limentdos", aux.asiturn.getLimentdos().toString());
    peticion.put("Asiturno_limentrada", aux.asiturn.getLimentrada().toString());
    peticion.put("Asiturno_limsaluno", aux.asiturn.getLimsaluno().toString());
    peticion.put("Asiturno_limsaldos", aux.asiturn.getLimsaldos().toString());
    peticion.put("Asiturno_limsalida", aux.asiturn.getLimsalida().toString());
    peticion.put("Asiturno_salida", aux.asiturn.getSalida().toString());*/
  }

  private void validarFechaHora(){
    
    /*if(cmbDiaEntrada.getSelectedItem().toString().equals("1970-1-1")){
      aux.asiturn.setLimentuno(Util.getInst().stringToTimestamp(txtAsiturno_limentuno.getText(),0));
      aux.asiturn.setLimentdos(Util.getInst().stringToTimestamp(txtAsiturno_limentdos.getText(),0));
      aux.asiturn.setLimentrada(Util.getInst().stringToTimestamp(txtAsiturno_limentrada.getText(),0));
      aux.asiturn.setEntrada(Util.getInst().stringToTimestamp(txtAsiturno_entrada.getText(),0));
    }else{
      aux.asiturn.setLimentuno(Util.getInst().stringToTimestamp(txtAsiturno_limentuno.getText(),1));
      aux.asiturn.setLimentdos(Util.getInst().stringToTimestamp(txtAsiturno_limentdos.getText(),1));
      aux.asiturn.setLimentrada(Util.getInst().stringToTimestamp(txtAsiturno_limentrada.getText(),1));
      aux.asiturn.setEntrada(Util.getInst().stringToTimestamp(txtAsiturno_entrada.getText(),1));
    }
    if(cmbDiaSalida.getSelectedItem().toString().equals("1970-1-1")){
      aux.asiturn.setLimsaluno(Util.getInst().stringToTimestamp(txtAsiturno_limsaluno.getText(),0));
      aux.asiturn.setLimsaldos(Util.getInst().stringToTimestamp(txtAsiturno_limsaldos.getText(),0));
      aux.asiturn.setLimsalida(Util.getInst().stringToTimestamp(txtAsiturno_limsalida.getText(),0));
      aux.asiturn.setSalida(Util.getInst().stringToTimestamp(txtAsiturno_salida.getText(),0));
    }
    else{
      aux.asiturn.setLimsaluno(Util.getInst().stringToTimestamp(txtAsiturno_limsaluno.getText(),1));
      aux.asiturn.setLimsaldos(Util.getInst().stringToTimestamp(txtAsiturno_limsaldos.getText(),1));
      aux.asiturn.setLimsalida(Util.getInst().stringToTimestamp(txtAsiturno_limsalida.getText(),1));
      aux.asiturn.setSalida(Util.getInst().stringToTimestamp(txtAsiturno_salida.getText(),1));
    }*/
    
//    if(aux.asiturn.getEntrada().compareTo(aux.asiturn.getLimentuno())==1) {
//      aux.asiturn.getLimentuno().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimentdos().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimentrada().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsaluno().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsaldos().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getSalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//    }
//    if(aux.asiturn.getLimentuno().compareTo(aux.asiturn.getLimentdos())==1) {
//      aux.asiturn.getLimentdos().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimentrada().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsaluno().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsaldos().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getSalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//    }
//    if(aux.asiturn.getLimentdos().compareTo(aux.asiturn.getLimentrada())==1) {
//      aux.asiturn.getLimentrada().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsaluno().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsaldos().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getSalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//    }
//    if(aux.asiturn.getLimentrada().compareTo(aux.asiturn.getLimsaluno())==1) {
//      aux.asiturn.getLimsaluno().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsaldos().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getSalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//    }
//    if(aux.asiturn.getLimsaluno().compareTo(aux.asiturn.getLimsaldos())==1) {
//      aux.asiturn.getLimsaldos().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getLimsalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getSalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//    }
//    if(aux.asiturn.getLimsaldos().compareTo(aux.asiturn.getLimsalida())==1) {
//      aux.asiturn.getLimsalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//      aux.asiturn.getSalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//    }
//    if(aux.asiturn.getLimsalida().compareTo(aux.asiturn.getSalida())==1) {
//      aux.asiturn.getSalida().setDate(aux.asiturn.getEntrada().getDate()+1);
//    }
  }
  
  @Override
  public void obtenerDatoBaseDato() {

    // TODO Auto-generated method stub
  }

  @Override
  public boolean validarFormulario() {

    if(pagGen.pagAct.getModo().equals(Modo.AGREGANDO) || pagGen.pagAct.getModo().equals(Modo.MODIFICANDO))
    {
      String[] strFecIni = cmbDiaEntrada.getSelectedItem().toString().split("-");
      String[] strFecFin = cmbDiaSalida.getSelectedItem().toString().split("-");
      java.sql.Date fechaInicio = new Date(Integer.parseInt(strFecIni[0])-1900,Integer.parseInt(strFecIni[1])-1,Integer.parseInt(strFecIni[2]));
      java.sql.Date fechaFinal = new Date(Integer.parseInt(strFecFin[0])-1900,Integer.parseInt(strFecFin[1])-1,Integer.parseInt(strFecFin[2]));
      
      if(fechaFinal.getTime()==fechaInicio.getTime() || fechaFinal.getTime()>fechaInicio.getTime()){
        return validarControles("turno");
      }else{
        msgBox("Error de validacion", "Verifique los dias", "successful");
        return false;
      }
    }
    else if(pagGen.pagAct.getModo().equals(Modo.VISUALIZANDO))
    {
      return true;
    }
    return false;
  }

  @Override
  public boolean guardarDatoBaseDato(String modoGrabar) {

    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void cerrarFormulario() {

  }

  /*
   * EVENTOS DE LOS CONTROLES DEL FORMULARIO
   */
  public void cmdCerrarEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      ejecutar("cmdCerrarEvento", Peticion.AJAX);
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      this.dispose();
    }
  }

  public void cmdGuardarEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      if(validarFormulario())
      {
        obtenerDatoFormulario();
        ejecutar("cmdGuardarEvento", Peticion.AJAX);
      }
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      refrescarFormulario();
    }
  }

  public void cmdBrowseSeleccionarEvento()
  {

    if(estadoEvento.equals(Modo.EVENTO_ENESPERA))
    {
      peticion = new HashMap();
      if(validarFormulario())
      {
        obtenerDatoFormulario();
        ejecutar("cmdBrowseSeleccionarEvento", Peticion.AJAX);
      }
    }
    else if(estadoEvento.equals(Modo.FINALIZANDO_EVENTO))
    {
      estadoEvento = Modo.EVENTO_ENESPERA;
      this.dispose();
    }
  }

  @Override
  public void setPeticion(Map peticion)
  {

    this.peticion = new HashMap();
    this.peticion = peticion;
  }

  @Override
  public JProgressBar obtenerBarraProgreso()
  {

    return null;
  }

  @Override
  public JLabel obtenerEtiAccionProgreso()
  {

    return null;
  }
}