package com.asis.accion;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import com.asistencia.accion.EliminarRegistroSinSalida;
import com.asistencia.accion.ExisteRegistroAsistencia;
import com.asistencia.accion.GuardarAsiasistencia;
import com.asistencia.accion.GuardarAsirepositorio;
import com.asistencia.accion.ObtenerAsctrlasistencia;
import com.asistencia.accion.GetListaTurnosByDiaAndDiaAnt;
import com.comun.database.DBConeccion;
import com.comun.entidad.Asictrlasistencia;
import com.comun.entidad.Asiturno;
import com.comun.entidad.Usuario;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;
import com.comun.referencia.Modo;
import com.comun.utilidad.Auxiliar;
import com.comun.utilidad.Util;
import com.usuario.accion.ObtenerUsuarioPorIdentidad;

public class EvaluarAsistenciaTurnos extends Accion {

	public Object ejecutarAccion(Map peticion) throws Exception {

		Object result = null;
		Auxiliar aux = Auxiliar.getInstance();
		DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
		try
		{
			BigDecimal newBigdecimal = new BigDecimal(0);
			Date fecinicio = new Date(aux.asictrlasi.getFechainicio().getYear(), aux.asictrlasi.getFechainicio().getMonth(), aux.asictrlasi.getFechainicio().getDate());
			Date fecfinal = (Date) peticion.get("Asictrlasistencia_fechafinal");
			if(fecfinal.compareTo(fecinicio) == 1 || fecfinal.compareTo(fecinicio) == 0)
			{
				if(fecfinal.compareTo(aux.asictrlasi.getFechafinal()) == 1)
				{
					fecfinal = new Date(aux.asictrlasi.getFechafinal().getYear(), aux.asictrlasi.getFechafinal().getMonth(), aux.asictrlasi.getFechafinal().getDate());
				}
				peticion.put("fechaActual", fecfinal);
				(new EliminarRegistroSinSalida()).ejecutarAccion(peticion);
				// Se suma 2 dias a total de dias por que cuando se resta por ejemplo
				// 20 - 11 = 9 DIAS Pero deben evaluarse 10 dias contanto el ultimo dia a evaluar
				// mostrarMensaje("last dia : " + lastDia + " totdia " + totDias, this.getClass().getName());
				if(fecinicio.getTime() <= fecfinal.getTime())
				{
					Date fecactTmp = new Date(fecinicio.getYear(), fecinicio.getMonth(), fecinicio.getDate());
					int numDias = Util.getInst().diferenciaFechas(fecinicio, fecfinal, "DIAS");
					double sumHoras = 0.00;
					for(int iteDia = 0; iteDia <= numDias; iteDia++)
					{
						fecactTmp.setDate(fecinicio.getDate() + iteDia);
						
						peticion.put("fechaActual", fecactTmp);
						(new GetListaTurnosByDiaAndDiaAnt()).ejecutarAccion(peticion);
            List listaTurno = (List) peticion.get("listaAsiturno");
						
						for(int iteLis = 0; iteLis < listaTurno.size(); iteLis++)
						{
							aux.asiturn = (Asiturno) listaTurno.get(iteLis);
							if(aux.asiturn.getTurnotipo().equals("LABORABLE"))
							{
								peticion.put("turno", aux.asiturn.getTurnonombre());
								boolean existeAsistencia = (Boolean) (new ExisteRegistroAsistencia()).ejecutarAccion(peticion);
								if(!existeAsistencia)
								{
									peticion.put("hora", Util.getInst().getFechaHora());
									peticion.put("estadoRegistro", Modo.SIN_REGISTRO);
									peticion.put("modoRegistro", Modo.FALTA);
									
						            peticion.put("totalHoraTrab", newBigdecimal);
						            peticion.put("totalHoraCons", Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), aux.asiturn.getSalida()));
						            peticion.put("totalHoraCont", newBigdecimal);
						            peticion.put("asisHorExt", newBigdecimal);

									(new GuardarAsiasistencia()).ejecutarAccion(peticion);

									if(aux.asiasis != null && !Util.getInst().isEmpty(aux.asiasis.getKyasistencia())){
										peticion.put("totalHoraIna", Util.getInst().getTotalHoraDecimal(aux.asiturn.getEntrada(), aux.asiturn.getSalida()));
							            peticion.put("totalHoraExt", newBigdecimal);
							            peticion.put("estadoRegistro", Modo.FALTA);
							            
										(new GuardarAsirepositorio()).ejecutarAccion(peticion);
									}
								}
							}// if(asiturno.getTipo().equals("LABORABLE"))
						}// for(int iteLis = 0; iteLis < listaTurno.size(); iteLis++)
					}// if (fecinicio.getTime() < fecfinal.getTime())
				}// Fin de iterar los dias que se han registrado o han registrado solo ingreso
			}// Find e verificar que el ultimo registro es anterior a la fecact actual
			else {
			}
			this.setEstado(Estado.SUCCEEDED);
		} catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
			throw e;
		}
		return result;
	}
	
	public static void main (String[] args)
	{
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() 
			{
				try {
					Map peticion = new HashMap();
					Auxiliar aux = Auxiliar.getInstance();
					
					/*
					 * select row_number()over(order by usuario.identidad asc) as rownum, asictrlasistencia.kyctrlasistencia, asictrlasistencia.periodo, 
						usuario.usucodigo, usuario.nombre, usuario.identidad
						from usuario inner join asictrlasistencia on usuario.usucodigo=asictrlasistencia.usucodigo
							inner join asctrlhorario on asictrlasistencia.kyctrlasistencia=asctrlhorario.kyctrlasistencia
							inner join aspapeleta on aspapeleta.papcodigo=asctrlhorario.papcodigo
							inner join ashorario on ashorario.horcodigo=asctrlhorario.horcodigo
							inner join asiturno on asiturno.horcodigo=ashorario.horcodigo
						group by usuario.identidad, asictrlasistencia.kyctrlasistencia, asictrlasistencia.periodo, usuario.usucodigo, usuario.nombre, usuario.identidad
					 */
					peticion.put("Asictrlasistencia_kyctrlasistencia", "34207811106901");
					peticion.put("Usuario_identidad", "10793205");
					peticion.put("dbConeccion", new DBConeccion("SISEM"));
					
					aux.asictrlasi = (Asictrlasistencia) (new ObtenerAsctrlasistencia()).ejecutarAccion(peticion);
					aux.usu = (Usuario) (new ObtenerUsuarioPorIdentidad()).ejecutarAccion(peticion);
					
					Date fecact = new Date(2014-1900, 07-1, 25);
					peticion.put("Asictrlasistencia_fechafinal", fecact);
					
					(new EvaluarAsistenciaTurnos()).ejecutarAccion(peticion);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
			}
		});
	}
}