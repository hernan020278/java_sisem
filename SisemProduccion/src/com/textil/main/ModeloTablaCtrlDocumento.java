package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaCtrlDocumento extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public ModeloTablaCtrlDocumento() {

		super.addColumn("ctrldoc_cod"); // Col 0 Integer
		super.addColumn("Local");      	// Col 1 String
		super.addColumn("Documento");   // Col 2 String
		super.addColumn("Serie");		// Col 3 Integer
		
	}

	@Override
	public Class getColumnClass(int col) {
		if (col == 0 || col == 3) {
			return Integer.class;
		}
		if (col == 1 || col == 2) {
			return String.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
