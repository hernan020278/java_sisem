package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaSubArea extends DefaultTableModel {

    public ModeloTablaSubArea() {
        
        super.addColumn("Ide");      	//Col 0 String
        super.addColumn("Nombre");      //Col 1 String
        super.addColumn("Descripcion"); //Col 2 String
        super.addColumn("Act");  		//Col 3 Boolean
        super.addColumn("subare_cod");  //Col 4 Integer
        super.addColumn("are_cod");  	//Col 5 Integer

    }

    @Override
    public Class getColumnClass(int columna) {
        if (columna >= 0 && columna <= 2) {
            return String.class;
        }
        if (columna >= 3) {
            return Boolean.class;
        }
        if (columna >= 4 && columna <= 5) {
            return Integer.class;
        }
        return Object.class;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
