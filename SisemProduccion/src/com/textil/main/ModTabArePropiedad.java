package com.textil.main;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JTable;

import com.textil.controlador.AuxiliarTextil.EstadoPagina;

/**
 *
 * @author BASVALGRAFICA
 */
public class ModTabArePropiedad implements PropertyChangeListener {

    private DlgArea dlgAre;
    private JTable tabDetVen;

    public ModTabArePropiedad(DlgArea parDlgAre, JTable parTabVen) {

        this.dlgAre = parDlgAre;
        this.tabDetVen = parTabVen;

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (dlgAre.frmAbierto) {
            if (!dlgAre.estDlgAre.equals(EstadoPagina.SUSPENDIDO)) {
                if (tabDetVen.isEditing()) {

                	dlgAre.activarFormulario(false);
                	dlgAre.desactivarBotonFormulario();
                	dlgAre.tabSubAre.setEnabled(false);
                	dlgAre.tabAre.setEnabled(true);
                }//fin de if (tabDetKar.isEditing())
                else {

//                System.out.println("Dejo de editarse" + this.getClass().getName());
                    dlgAre.refrescarFormulario();

                }//fin de if (tabDetKar.isEditing()) - Else                    
            }//Fin de if (!dlgAre.estDlgDetVen.equals(estadoVentana.SUSPENDIDO))
        }//Fin de if(frmAbierto)
    }//Fin de propertyChange(PropertyChangeEvent evt)
}