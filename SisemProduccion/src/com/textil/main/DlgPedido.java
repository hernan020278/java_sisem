package com.textil.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Agrupacion;
import com.comun.entidad.Area;
import com.comun.entidad.Clase;
import com.comun.entidad.Cliente;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Ficha;
import com.comun.entidad.Grupo;
import com.comun.entidad.Pedido;
import com.comun.entidad.Producto;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.CodigoBarras;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoDecimal;
import com.comun.utilidad.swing.JTextFieldFormatoEntero;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.comun.utilidad.swing.PanelImgFoto;
import com.textil.controlador.AuxiliarTextil;
import com.textil.controlador.AuxiliarTextil.AccesoPagina;
import com.textil.controlador.AuxiliarTextil.EstadoDocumento;
import com.textil.controlador.AuxiliarTextil.EstadoPagina;
import com.textil.controlador.AuxiliarTextil.TipoDocumento;
import com.textil.controlador.ContenedorTextil;
import com.textil.dao.ClaseDAO;
import com.textil.dao.ClienteDAO;
import com.textil.dao.TransactionDelegate;
import com.toedter.calendar.JDateChooser;

/**
 * 
 * @author Hernan
 */
public class DlgPedido extends javax.swing.JDialog implements InterfaceUsuario {

	/**
	 * Variables principales
	 */
	public static EstadoPagina estDlgPed = EstadoPagina.VISUALIZANDO;
	public EstadoPagina estDlgDetPed;
	public AccesoPagina acceso;
	public AuxiliarTextil man;
	public String padre;
	public boolean frmAbierto;
	/**
	 * Variables del formulario
	 */

	private CeldaRenderGeneral celdaRenderGeneral;
	private JTable tabDetTal;
	private ModeloTablaDetProducto modTabDetPrd;
	private TableColumn[] colTabDetPrd;
	private JTextFieldChanged txtCliIde;
	private JTextFieldChanged txtCliDoc;
	private JTextFieldChanged txtCliNum;
	private JTextFieldChanged txtCliNom;
	private JTextFieldChanged txtCliDir;
	private JTextFieldChanged txtPedNumDoc;
	private JDateChooser cmbPedFecReg;
	private JDateChooser cmbPedFecEnt;
	private JTextFieldFormatoEntero txtPedAno;
	private JTextFieldFormatoEntero txtPedTemp;
	private JTextFieldChanged txtPedEst;
	private PanelImgFoto panModFoto;
	private PanelImgFoto panPrdBar;
	private JScrollPane scrTabDetTal;
	private JButton cmdDetPedAgregar;
	private JButton cmdDetPedModificar;
	private JButton cmdDetPedEliminar;
	private JButton cmdDetPedBuscar;
	private JButton cmdPedAgregar;
	private JButton cmdPedModificar;
	private JButton cmdPedBuscar;
	private JButton cmdPedEliminar;
	private JLabel etiPedNomDoc;
	private final JTextEditorFecha txtEditPedFecReg, txtEditPedFecEnt;
	private JButton cmdPedDocumento;
	private JButton cmdCerrar;
	private JButton cmdDetPedImprimir;
	private JTextFieldFormatoDecimal txtPedImpTot;
	private DefaultListModel modLisCliNom;
	private DefaultListModel modLisModIde;
	private DefaultListModel modLisModGau;
	private DefaultListModel modLisModGro;
	private DefaultListModel modLisPrdCol;
	private DefaultListModel modLisModTip;
	private DefaultListModel modLisModMat;
	private JScrollPane scrLisModIde;
	private JScrollPane scrLisModTip;
	private JScrollPane scrLisModMat;
	private JScrollPane scrLisModGau;
	private JScrollPane scrLisModGro;

	private JList lisModIde;
	private JList lisModTip;
	private JList lisModMat;
	private JList lisModGau;
	private boolean busLisCliNom;
	private boolean busLisModIde;
	private boolean busLisModTip;
	private boolean busLisModMat;
	private boolean busLisModGau;
	private boolean busLisModGro;
	private boolean busLisPrdCol;
	private List<String> varLisCliNom;
	private List<String> varLisModIde = new ArrayList<String>();
	private List<String> varLisModTip = new ArrayList<String>();
	private List<String> varLisModMat = new ArrayList<String>();
	private List<String> varLisModGau = new ArrayList<String>();
	private List<String> varLisModGro = new ArrayList<String>();
	private List<String> varLisPrdCol = new ArrayList<String>();
	public static int bookMarkPedCod = 0;
	private int bookMarkDetPedCod;

	private int bookMarkModCod;
	private int bookMarkPrdCod;
	private int bookMarkTalCod;
	private int bookMarkCliCod;
	/**
	 * codigoCompuesto<br>
	 * ===============<br>
	 * Se forma de una consulta en<br>
	 * <b>ProductoDAO.obtenerListaProducto</b><br>
	 * 0 : detped_cod <br>
	 * 1 : ped_cod<br>
	 * 2 : prd_cod<br>
	 * 3 : cla_cod<br>
	 * 4 : gru_cod<br>
	 * 5 : gru_nivel<br>
	 * 6 : prd_ide<br>
	 * 7 : prd_des<br>
	 */
	private JPopupMenu popMenModFoto;
	private JMenuItem menuPegarModFoto;
	private JMenuItem menuBorrarModFoto;
	private JButton cmdFichaControl;
	private JScrollPane scrTabDetPrd;
	private JTable tabDetPrd;
	private ModeloTablaDetTalla modTabDetTal;
	private TableColumn[] colTabDetTal;
	private JTextFieldChanged txtModGau;
	private JTextFieldChanged txtModMat;
	private JTextFieldChanged txtModTip;
	private JTextFieldChanged txtModIde;
	private JScrollPane scrLisCliNom;
	private JList lisCliNom;
	private JTextFieldChanged txtPrdIde;
	private JTextFieldChanged txtPrdDes;
	private JTextFieldChanged txtTalIde;
	private JTextFieldChanged txtTalDes;
	private JTextFieldFormatoEntero txtTalCan;
	private JTextFieldChanged txtModDes;
	private JTextFieldFormatoEntero txtPrdTot;
	private JTextFieldFormatoEntero txtTalTot;
	private JTextFieldChanged txtModGro;
	private JTextFieldChanged txtPrdCol;
	private JList lisPrdCol;
	private JScrollPane scrLisPrdCol;
	private JList lisModGro;
	private String bookMarkNomDoc;
	private JButton cmdPedFichas;

	/**
	 * Creates new form DlgGrupo
	 */
	public DlgPedido(FrmPrincipal parent) {

		super(parent, false);
		setTitle("Administracion del Pedido : "+this.getClass().getSimpleName());
		setResizable(false);
		getContentPane().setFont(new Font("Tahoma", Font.BOLD, 14));
		modLisCliNom = new DefaultListModel();
		modLisModIde = new DefaultListModel();
		modLisModTip = new DefaultListModel();
		modLisModMat = new DefaultListModel();
		modLisModGau = new DefaultListModel();
		modLisModGro = new DefaultListModel();
		modLisPrdCol = new DefaultListModel();
		txtEditPedFecReg = new JTextEditorFecha();
		txtEditPedFecReg.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditPedFecEnt = new JTextEditorFecha();
		txtEditPedFecEnt.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txtEditPedFecReg.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		txtEditPedFecEnt.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());

		initComponents();

		panModFoto.setImagen(null, null, null, "CONSTRAINT");

		popMenModFoto = new JPopupMenu();

		menuPegarModFoto = new JMenuItem("Pegar Imagen");
		menuPegarModFoto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				panModFoto.pegarImagenClipboard("logo.png");
				man.mod.setFilPrd_foto(panModFoto.getFil());
				man.mod.setFisPrd_foto(panModFoto.getFis());
				man.mod.setPrd_foto(panModFoto.getImagen());

			}
		});
		menuBorrarModFoto = new JMenuItem("Borrar Imagen");
		menuBorrarModFoto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				panModFoto.setImagen(null, null, null, "CONSTRAINT");
				man.mod.setFilPrd_foto(panModFoto.getFil());
				man.mod.setFisPrd_foto(panModFoto.getFis());
				man.mod.setPrd_foto(panModFoto.getImagen());

			}
		});
		popMenModFoto.add(menuPegarModFoto);
		popMenModFoto.add(menuBorrarModFoto);

		crearModeloTablas();

	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		scrLisCliNom = new JScrollPane();
		scrLisCliNom.setBounds(110, 112, 210, 242);
		getContentPane().add(scrLisCliNom);

		lisCliNom = new JList(modLisCliNom);
		lisCliNom.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				lisPerNomMousePressed(e);

			}
		});
		lisCliNom.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				lisPerNomKeyReleased(e);

			}
		});
		scrLisCliNom.setViewportView(lisCliNom);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Pedido", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(5, 5, 703, 59);
		getContentPane().add(panel);

		etiPedNomDoc = new JLabel();
		etiPedNomDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiPedNomDoc.setText(TipoDocumento.NOTAPEDIDO.getVal());
		etiPedNomDoc.setHorizontalAlignment(SwingConstants.CENTER);
		etiPedNomDoc.setForeground(new Color(51, 51, 51));
		etiPedNomDoc.setBounds(10, 15, 129, 15);
		panel.add(etiPedNomDoc);

		txtPedNumDoc = new JTextFieldChanged(11);
		txtPedNumDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedNumDoc.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPedNumDocKeyReleased(arg0);

			}
		});
		txtPedNumDoc.setText("001-56467564");
		txtPedNumDoc.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedNumDoc.setBounds(10, 32, 129, 20);
		panel.add(txtPedNumDoc);

		cmbPedFecReg = new JDateChooser(txtEditPedFecReg);
		cmbPedFecReg.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbPedFecReg.setDateFormatString("dd/MM/yyyy");
		cmbPedFecReg.setBounds(168, 32, 115, 20);
		panel.add(cmbPedFecReg);

		JLabel label_1 = new JLabel();
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setText("Fecha Registro");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(51, 51, 51));
		label_1.setBounds(168, 15, 115, 15);
		panel.add(label_1);

		JLabel label_2 = new JLabel();
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setText("Fecha Entrega");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setForeground(new Color(51, 51, 51));
		label_2.setBounds(304, 15, 115, 15);
		panel.add(label_2);

		cmbPedFecEnt = new JDateChooser(txtEditPedFecEnt);
		cmbPedFecEnt.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbPedFecEnt.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbPedFecEnt.setDateFormatString("dd/MM/yyyy");
		cmbPedFecEnt.setBounds(304, 32, 115, 20);
		panel.add(cmbPedFecEnt);

		JLabel label_3 = new JLabel();
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setText("Estado");
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setForeground(new Color(51, 51, 51));
		label_3.setBounds(562, 15, 131, 15);
		panel.add(label_3);

		txtPedEst = new JTextFieldChanged(20);
		txtPedEst.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedEst.setText("12345678901234567890");
		txtPedEst.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedEst.setBounds(562, 32, 131, 20);
		panel.add(txtPedEst);

		txtPedTemp = new JTextFieldFormatoEntero(2);
		txtPedTemp.setText("4");
		txtPedTemp.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedTemp.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedTemp.setBounds(482, 32, 70, 20);
		panel.add(txtPedTemp);

		JLabel label_4 = new JLabel();
		label_4.setText("Temporada");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(482, 15, 70, 15);
		panel.add(label_4);

		JLabel label_5 = new JLabel();
		label_5.setText("A\u00F1o");
		label_5.setHorizontalAlignment(SwingConstants.CENTER);
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(429, 15, 49, 15);
		panel.add(label_5);

		txtPedAno = new JTextFieldFormatoEntero(4);
		txtPedAno.setText("4");
		txtPedAno.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedAno.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedAno.setBounds(429, 32, 49, 20);
		panel.add(txtPedAno);

		JPanel panCliente = new JPanel();
		panCliente.setLayout(null);
		panCliente.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panCliente.setBounds(5, 60, 703, 59);
		getContentPane().add(panCliente);

		JLabel label_6 = new JLabel();
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_6.setText("Nombre");
		label_6.setHorizontalAlignment(SwingConstants.LEFT);
		label_6.setForeground(new Color(51, 51, 51));
		label_6.setBounds(105, 15, 209, 15);
		panCliente.add(label_6);

		txtCliNom = new JTextFieldChanged(50);
		txtCliNom.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliNom.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPerNomKeyReleased(arg0);

			}
		});

		txtCliIde = new JTextFieldChanged(10);
		txtCliIde.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliIde.setText("AYK");
		txtCliIde.setBounds(10, 32, 91, 20);
		panCliente.add(txtCliIde);
		txtCliNom.setText("Hernan Mendoza Ticllahuanaco");
		txtCliNom.setBounds(105, 32, 209, 20);
		panCliente.add(txtCliNom);

		txtCliDoc = new JTextFieldChanged(3);
		txtCliDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliDoc.setBounds(317, 32, 39, 20);
		panCliente.add(txtCliDoc);
		txtCliDoc.setText("RUC");

		txtCliNum = new JTextFieldChanged(11);
		txtCliNum.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliNum.setBounds(358, 32, 114, 20);
		panCliente.add(txtCliNum);
		txtCliNum.setText("12345678901");

		txtCliDir = new JTextFieldChanged(100);
		txtCliDir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliDir.setText("Av. 28 de Julio 785 La Tomilla Cayma");
		txtCliDir.setBounds(475, 32, 218, 20);
		panCliente.add(txtCliDir);

		JLabel label_9 = new JLabel();
		label_9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_9.setText("Direccion");
		label_9.setHorizontalAlignment(SwingConstants.LEFT);
		label_9.setForeground(new Color(51, 51, 51));
		label_9.setBounds(475, 15, 218, 15);
		panCliente.add(label_9);

		JLabel label_10 = new JLabel();
		label_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_10.setText("Codigo");
		label_10.setHorizontalAlignment(SwingConstants.LEFT);
		label_10.setForeground(new Color(51, 51, 51));
		label_10.setBounds(10, 15, 91, 15);
		panCliente.add(label_10);

		JLabel lblDoc = new JLabel();
		lblDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDoc.setBounds(317, 15, 39, 15);
		panCliente.add(lblDoc);
		lblDoc.setText("DOC");
		lblDoc.setHorizontalAlignment(SwingConstants.CENTER);
		lblDoc.setForeground(new Color(51, 51, 51));

		JLabel label_8 = new JLabel();
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_8.setBounds(358, 15, 114, 15);
		panCliente.add(label_8);
		label_8.setText("Numero");
		label_8.setHorizontalAlignment(SwingConstants.LEFT);
		label_8.setForeground(new Color(51, 51, 51));

		JPanel panProducto = new JPanel();
		panProducto.setLayout(null);
		panProducto.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Producto", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panProducto.setBounds(5, 115, 833, 437);
		getContentPane().add(panProducto);

		JLabel lblModelo_1 = new JLabel("Modelo");
		lblModelo_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblModelo_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblModelo_1.setBorder(null);
		lblModelo_1.setBounds(11, 15, 157, 15);
		panProducto.add(lblModelo_1);

		JLabel lblTipo_1 = new JLabel("Tipo");
		lblTipo_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipo_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo_1.setBorder(null);
		lblTipo_1.setBounds(386, 15, 137, 15);
		panProducto.add(lblTipo_1);

		JLabel lblMaterial = new JLabel("Material");
		lblMaterial.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMaterial.setHorizontalAlignment(SwingConstants.CENTER);
		lblMaterial.setBorder(null);
		lblMaterial.setBounds(527, 15, 132, 15);
		panProducto.add(lblMaterial);

		JLabel lblGalga = new JLabel("Galga");
		lblGalga.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGalga.setHorizontalAlignment(SwingConstants.CENTER);
		lblGalga.setBorder(null);
		lblGalga.setBounds(662, 15, 76, 15);
		panProducto.add(lblGalga);

		txtModIde = new JTextFieldChanged(50);
		txtModIde.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModIde.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				txtModIdeKeyReleased(e);

			}
		});
		txtModIde.setText("12345678901234567890");
		txtModIde.setBounds(11, 32, 157, 20);
		panProducto.add(txtModIde);

		txtModTip = new JTextFieldChanged(50);
		txtModTip.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModTip.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				txtModTipKeyReleased(e);

			}
		});
		txtModTip.setText("12345678901234567890");
		txtModTip.setBounds(386, 32, 137, 20);
		panProducto.add(txtModTip);

		txtModMat = new JTextFieldChanged(50);
		txtModMat.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModMat.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				txtModMatKeyReleased(e);

			}
		});
		txtModMat.setText("12345678901234567890");
		txtModMat.setBounds(527, 32, 132, 20);
		panProducto.add(txtModMat);

		txtModGau = new JTextFieldChanged(50);
		txtModGau.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModGau.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				txtModGauKeyReleased(e);

			}
		});
		txtModGau.setText("12345678901234567890");
		txtModGau.setBounds(662, 32, 76, 20);
		panProducto.add(txtModGau);

		scrLisModIde = new JScrollPane();
		scrLisModIde.setBounds(11, 53, 157, 185);
		panProducto.add(scrLisModIde);

		lisModIde = new JList(modLisModIde);
		lisModIde.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				lisModIdeMousePressed(arg0);

			}
		});
		lisModIde.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				lisModIdeKeyReleased(e);
			}
		});
		scrLisModIde.setViewportView(lisModIde);

		scrLisModTip = new JScrollPane();
		scrLisModTip.setBounds(386, 53, 137, 185);
		panProducto.add(scrLisModTip);

		lisModTip = new JList(modLisModTip);
		lisModTip.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				lisModTipMousePressed(e);

			}
		});
		lisModTip.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				lisModTipKeyReleased(e);

			}
		});
		scrLisModTip.setViewportView(lisModTip);

		scrLisModMat = new JScrollPane();
		scrLisModMat.setBounds(527, 53, 132, 185);
		panProducto.add(scrLisModMat);

		lisModMat = new JList(modLisModMat);
		lisModMat.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				lisModMatMousePressed(e);

			}
		});
		lisModMat.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				lisModMatKeyReleased(e);

			}
		});
		scrLisModMat.setViewportView(lisModMat);

		scrLisModGau = new JScrollPane();
		scrLisModGau.setBounds(662, 52, 76, 186);
		panProducto.add(scrLisModGau);

		lisModGau = new JList(modLisModGau);
		lisModGau.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				lisModGauMousePressed(e);

			}
		});
		lisModGau.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				lisModGauKeyReleased(e);

			}
		});
		scrLisModGau.setViewportView(lisModGau);

		scrLisModGro = new JScrollPane();
		scrLisModGro.setBounds(741, 53, 82, 185);
		panProducto.add(scrLisModGro);

		lisModGro = new JList(modLisModGro);
		lisModGro.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				lisModGroMousePressed(arg0);

			}
		});
		lisModGro.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				lisModGroKeyReleased(arg0);

			}
		});
		scrLisModGro.setViewportView(lisModGro);

		scrLisPrdCol = new JScrollPane();
		scrLisPrdCol.setBounds(11, 95, 147, 143);
		panProducto.add(scrLisPrdCol);

		lisPrdCol = new JList(modLisPrdCol);
		lisPrdCol.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				lisPrdColMousePressed(arg0);

			}
		});
		lisPrdCol.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				lisPrdColKeyReleased(arg0);

			}
		});
		scrLisPrdCol.setViewportView(lisPrdCol);

		scrTabDetTal = new JScrollPane();
		scrTabDetTal.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrTabDetTal.setBounds(614, 95, 209, 331);
		panProducto.add(scrTabDetTal);

		tabDetTal = new JTable();
		tabDetTal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tabDetTal.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabDetTal.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				tabDetTalMousePressed(e);

			}
		});
		tabDetTal.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				tabDetTalKeyReleased(e);

			}
		});
		scrTabDetTal.setViewportView(tabDetTal);

		scrTabDetPrd = new JScrollPane();
		scrTabDetPrd.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrTabDetPrd.setBounds(11, 95, 593, 331);
		panProducto.add(scrTabDetPrd);

		tabDetPrd = new JTable();
		tabDetPrd.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tabDetPrd.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabDetPrd.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				tabDetPrdKeyReleased(arg0);

			}
		});
		tabDetPrd.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				tabDetPrdMousePressed(arg0);

			}
		});
		scrTabDetPrd.setViewportView(tabDetPrd);

		JLabel lblProducto = new JLabel("Articulo");
		lblProducto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProducto.setHorizontalAlignment(SwingConstants.CENTER);
		lblProducto.setBorder(null);
		lblProducto.setBounds(167, 55, 167, 15);
		panProducto.add(lblProducto);

		txtPrdIde = new JTextFieldChanged(50);
		txtPrdIde.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPrdIdeKeyReleased(arg0);

			}
		});
		txtPrdIde.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPrdIde.setText("12345678901234567890");
		txtPrdIde.setBounds(168, 73, 166, 20);
		panProducto.add(txtPrdIde);

		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDescripcion.setHorizontalAlignment(SwingConstants.CENTER);
		lblDescripcion.setBorder(null);
		lblDescripcion.setBounds(343, 55, 261, 15);
		panProducto.add(lblDescripcion);

		txtPrdDes = new JTextFieldChanged(50);
		txtPrdDes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPrdDesKeyReleased(arg0);

			}
		});
		txtPrdDes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPrdDes.setText("12345678901234567890");
		txtPrdDes.setBounds(344, 73, 260, 20);
		panProducto.add(txtPrdDes);

		JLabel lblTalla_1 = new JLabel("Talla");
		lblTalla_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTalla_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblTalla_1.setBorder(null);
		lblTalla_1.setBounds(640, 55, 117, 15);
		panProducto.add(lblTalla_1);

		txtTalDes = new JTextFieldChanged(5);
		txtTalDes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtTalDesKeyReleased(arg0);

			}
		});
		txtTalDes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTalDes.setText("12345678901234567890");
		txtTalDes.setBounds(641, 73, 116, 20);
		panProducto.add(txtTalDes);

		JLabel lblColor = new JLabel("Color");
		lblColor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblColor.setHorizontalAlignment(SwingConstants.CENTER);
		lblColor.setBorder(null);
		lblColor.setBounds(11, 56, 147, 15);
		panProducto.add(lblColor);

		txtTalCan = new JTextFieldFormatoEntero(4);
		txtTalCan.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtTalCanKeyReleased(arg0);

			}
		});
		txtTalCan.setText("4");
		txtTalCan.setHorizontalAlignment(SwingConstants.CENTER);
		txtTalCan.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTalCan.setBounds(767, 73, 56, 20);
		panProducto.add(txtTalCan);

		JLabel lblCantidad = new JLabel("Cantidad");
		lblCantidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCantidad.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantidad.setBorder(null);
		lblCantidad.setBounds(767, 55, 56, 15);
		panProducto.add(lblCantidad);

		txtModDes = new JTextFieldChanged(50);
		txtModDes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				txtModDesKeyReleased(e);

			}
		});
		txtModDes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModDes.setText("12345678901234567890");
		txtModDes.setBounds(179, 32, 203, 20);
		panProducto.add(txtModDes);

		JLabel label = new JLabel("Descripcion");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBorder(null);
		label.setBounds(178, 14, 204, 15);
		panProducto.add(label);

		txtModGro = new JTextFieldChanged(50);
		txtModGro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModGro.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtModGroKeyReleased(arg0);

			}
		});
		txtModGro.setText("12345678901234567890");
		txtModGro.setBounds(741, 32, 82, 20);
		panProducto.add(txtModGro);

		JLabel lblGrosor = new JLabel("Grosor");
		lblGrosor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGrosor.setHorizontalAlignment(SwingConstants.CENTER);
		lblGrosor.setBorder(null);
		lblGrosor.setBounds(741, 15, 82, 15);
		panProducto.add(lblGrosor);

		txtPrdCol = new JTextFieldChanged(50);
		txtPrdCol.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPrdCol.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPrdColKeyReleased(arg0);

			}
		});
		txtPrdCol.setBounds(11, 73, 147, 20);
		panProducto.add(txtPrdCol);
		txtPrdCol.setText("12345678901234567890");

		cmdDetPedBuscar = new JButton();
		cmdDetPedBuscar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDetPedBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));
		cmdDetPedBuscar.setText("Buscar");
		cmdDetPedBuscar.setName("Agregar");
		cmdDetPedBuscar.setMargin(new Insets(0, 0, 0, 0));
		cmdDetPedBuscar.setBounds(886, 289, 90, 35);
		getContentPane().add(cmdDetPedBuscar);

		JLabel label_20 = new JLabel();
		label_20.setText("Importe Total");
		label_20.setHorizontalAlignment(SwingConstants.CENTER);
		label_20.setForeground(new Color(51, 51, 51));
		label_20.setBounds(886, 329, 90, 20);
		getContentPane().add(label_20);

		txtPedImpTot = new JTextFieldFormatoDecimal(13, 2);
		txtPedImpTot.setEnabled(false);
		txtPedImpTot.setText("9999999999.99");
		txtPedImpTot.setHorizontalAlignment(SwingConstants.RIGHT);
		txtPedImpTot.setBounds(886, 349, 90, 20);
		getContentPane().add(txtPedImpTot);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 605, 828, 5);
		getContentPane().add(separator);

		cmdPedEliminar = new JButton();
		cmdPedEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdPedEliminarActionPerformed(e);

			}
		});
		cmdPedEliminar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdPedEliminar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPedEliminar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("eliminar.png"));
		cmdPedEliminar.setText("Eliminar");
		cmdPedEliminar.setName("Agregar");
		cmdPedEliminar.setMargin(new Insets(0, 0, 0, 0));
		cmdPedEliminar.setBounds(281, 610, 90, 40);
		getContentPane().add(cmdPedEliminar);

		cmdPedBuscar = new JButton();
		cmdPedBuscar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdPedBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdPedBuscarActionPerformed(arg0);

			}
		});
		cmdPedBuscar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPedBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));
		cmdPedBuscar.setText("Buscar");
		cmdPedBuscar.setName("Agregar");
		cmdPedBuscar.setMargin(new Insets(0, 0, 0, 0));
		cmdPedBuscar.setBounds(196, 610, 85, 40);
		getContentPane().add(cmdPedBuscar);

		cmdPedModificar = new JButton();
		cmdPedModificar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdPedModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdPedModificarActionPerformed(arg0);

			}
		});
		cmdPedModificar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPedModificar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("modificar.png"));
		cmdPedModificar.setText("Modificar");
		cmdPedModificar.setName("Agregar");
		cmdPedModificar.setMargin(new Insets(0, 0, 0, 0));
		cmdPedModificar.setBounds(100, 610, 95, 40);
		getContentPane().add(cmdPedModificar);

		cmdPedAgregar = new JButton();
		cmdPedAgregar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdPedAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdPedAgregarActionPerformed(arg0);

			}
		});
		cmdPedAgregar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPedAgregar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png"));
		cmdPedAgregar.setText("Agregar");
		cmdPedAgregar.setName("Agregar");
		cmdPedAgregar.setMargin(new Insets(0, 0, 0, 0));
		cmdPedAgregar.setBounds(10, 610, 90, 40);
		getContentPane().add(cmdPedAgregar);

		cmdDetPedImprimir = new JButton();
		cmdDetPedImprimir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdDetPedImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdDetPedImprimirActionPerformed(e);

			}
		});
		cmdDetPedImprimir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDetPedImprimir.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		cmdDetPedImprimir.setText("Imprimir Ficha");
		cmdDetPedImprimir.setName("Agregar");
		cmdDetPedImprimir.setMargin(new Insets(0, 0, 0, 0));
		cmdDetPedImprimir.setBounds(350, 563, 142, 40);
		getContentPane().add(cmdDetPedImprimir);

		cmdPedDocumento = new JButton();
		cmdPedDocumento.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdPedDocumento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdPedImprimirActionPerformed(e);

			}
		});
		cmdPedDocumento.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPedDocumento.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		cmdPedDocumento.setText("Pedido");
		cmdPedDocumento.setName("Agregar");
		cmdPedDocumento.setMargin(new Insets(0, 0, 0, 0));
		cmdPedDocumento.setBounds(372, 610, 95, 40);
		getContentPane().add(cmdPedDocumento);

		cmdCerrar = new JButton();
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdCerrarActionPerformed(arg0);

			}
		});
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setName("Agregar");
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setBounds(737, 610, 104, 40);
		getContentPane().add(cmdCerrar);

		cmdFichaControl = new JButton();
		cmdFichaControl.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdFichaControl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdFichaControl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdFichaControlActionPerformed(arg0);

			}
		});
		cmdFichaControl.setIcon(AplicacionGeneral.getInstance().obtenerImagen("ficha.png"));
		cmdFichaControl.setText("Fichas Control");
		cmdFichaControl.setName("Agregar");
		cmdFichaControl.setMargin(new Insets(0, 0, 0, 0));
		cmdFichaControl.setBounds(565, 610, 136, 40);
		getContentPane().add(cmdFichaControl);

		panPrdBar = new PanelImgFoto();
		panPrdBar.setBounds(881, 429, 95, 41);
		getContentPane().add(panPrdBar);
		panPrdBar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GroupLayout gl_panPrdBar = new GroupLayout(panPrdBar);
		gl_panPrdBar.setHorizontalGroup(
				gl_panPrdBar.createParallelGroup(Alignment.LEADING)
						.addGap(0, 145, Short.MAX_VALUE)
						.addGap(0, 133, Short.MAX_VALUE)
				);
		gl_panPrdBar.setVerticalGroup(
				gl_panPrdBar.createParallelGroup(Alignment.LEADING)
						.addGap(0, 65, Short.MAX_VALUE)
						.addGap(0, 53, Short.MAX_VALUE)
				);
		panPrdBar.setLayout(gl_panPrdBar);

		panModFoto = new PanelImgFoto();
		panModFoto.setBounds(718, 5, 120, 105);
		getContentPane().add(panModFoto);
		panModFoto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {

				panModFotoMouseReleased(e);

			}
		});
		panModFoto.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GroupLayout gl_panModFoto = new GroupLayout(panModFoto);
		gl_panModFoto.setHorizontalGroup(
				gl_panModFoto.createParallelGroup(Alignment.LEADING)
						.addGap(0, 98, Short.MAX_VALUE)
						.addGap(0, 86, Short.MAX_VALUE)
				);
		gl_panModFoto.setVerticalGroup(
				gl_panModFoto.createParallelGroup(Alignment.LEADING)
						.addGap(0, 81, Short.MAX_VALUE)
						.addGap(0, 69, Short.MAX_VALUE)
				);
		panModFoto.setLayout(gl_panModFoto);

		JLabel lblElaborando = new JLabel();
		lblElaborando.setBounds(881, 481, 95, 20);
		getContentPane().add(lblElaborando);
		lblElaborando.setForeground(new Color(0, 100, 0));
		lblElaborando.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		lblElaborando.setText("ELABORANDO");
		lblElaborando.setHorizontalAlignment(SwingConstants.CENTER);
		lblElaborando.setFont(new Font("Tahoma", Font.PLAIN, 11));

		cmdDetPedAgregar = new JButton();
		cmdDetPedAgregar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdDetPedAgregar.setBounds(10, 563, 104, 40);
		getContentPane().add(cmdDetPedAgregar);
		cmdDetPedAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdDetPrdAgregarActionPerformed(arg0);

			}
		});
		cmdDetPedAgregar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDetPedAgregar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png"));
		cmdDetPedAgregar.setText("Agregar");
		cmdDetPedAgregar.setName("Agregar");
		cmdDetPedAgregar.setMargin(new Insets(0, 0, 0, 0));

		cmdDetPedModificar = new JButton();
		cmdDetPedModificar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdDetPedModificar.setBounds(124, 563, 104, 40);
		getContentPane().add(cmdDetPedModificar);
		cmdDetPedModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdDetPrdModificarActionPerformed(e);

			}
		});
		cmdDetPedModificar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDetPedModificar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("modificar.png"));
		cmdDetPedModificar.setText("Modificar");
		cmdDetPedModificar.setName("Agregar");
		cmdDetPedModificar.setMargin(new Insets(0, 0, 0, 0));

		cmdDetPedEliminar = new JButton();
		cmdDetPedEliminar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdDetPedEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdDetPrdEliminarActionPerformed(arg0);
			}
		});
		cmdDetPedEliminar.setBounds(238, 563, 104, 40);
		getContentPane().add(cmdDetPedEliminar);
		cmdDetPedEliminar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDetPedEliminar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("eliminar.png"));
		cmdDetPedEliminar.setText("Eliminar");
		cmdDetPedEliminar.setName("Agregar");
		cmdDetPedEliminar.setMargin(new Insets(0, 0, 0, 0));

		JLabel lblTalla = new JLabel("Talla Ide");
		lblTalla.setBounds(886, 380, 90, 15);
		getContentPane().add(lblTalla);
		lblTalla.setHorizontalAlignment(SwingConstants.CENTER);
		lblTalla.setBorder(null);

		txtTalIde = new JTextFieldChanged(50);
		txtTalIde.setEnabled(false);
		txtTalIde.setBounds(887, 398, 89, 20);
		getContentPane().add(txtTalIde);
		txtTalIde.setText("12345678901234567890");

		JLabel lblTotalArticulo = new JLabel();
		lblTotalArticulo.setText("Tot-Art");
		lblTotalArticulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalArticulo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTotalArticulo.setBounds(586, 552, 53, 20);
		getContentPane().add(lblTotalArticulo);

		txtPrdTot = new JTextFieldFormatoEntero(2);
		txtPrdTot.setText("4");
		txtPrdTot.setHorizontalAlignment(SwingConstants.CENTER);
		txtPrdTot.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPrdTot.setBounds(586, 572, 53, 20);
		getContentPane().add(txtPrdTot);

		JLabel lblTottal = new JLabel();
		lblTottal.setText("Tot-Tal");
		lblTottal.setHorizontalAlignment(SwingConstants.CENTER);
		lblTottal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTottal.setBounds(785, 552, 53, 20);
		getContentPane().add(lblTottal);

		txtTalTot = new JTextFieldFormatoEntero(2);
		txtTalTot.setText("4");
		txtTalTot.setHorizontalAlignment(SwingConstants.CENTER);
		txtTalTot.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTalTot.setBounds(785, 572, 53, 20);
		getContentPane().add(txtTalTot);

		cmdPedFichas = new JButton();
		cmdPedFichas.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPedFichas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdPedFichasActionPerformed(arg0);

			}
		});
		cmdPedFichas.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		cmdPedFichas.setText("Fichas");
		cmdPedFichas.setName("Agregar");
		cmdPedFichas.setMargin(new Insets(0, 0, 0, 0));
		cmdPedFichas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdPedFichas.setBounds(469, 610, 95, 40);
		getContentPane().add(cmdPedFichas);

		setSize(new Dimension(851, 683));
		setLocationRelativeTo(null);
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DlgPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DlgPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DlgPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DlgPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/* Create and display the dialog */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				DlgPedido dialog = new DlgPedido(null);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	// End of variables declaration//GEN-END:variables

	/*
	 * METODOS DE LA INTERFAZ PRINCIPAL
	 * 
	 * @see com.vista.InterfaceUsuario#iniciarFormulario()
	 */
	@Override
	public void iniciarFormulario() {

		if (man.validarUsuario()) {

			busLisCliNom = true;
			busLisModIde = true;
			busLisModTip = true;
			busLisModMat = true;
			busLisModGau = true;
			busLisModGro = true;
			busLisPrdCol = true;

			if (acceso.equals(AccesoPagina.INICIAR)) {
				if (estDlgPed.equals(EstadoPagina.BUSCANDO)) {

					// estDlgPed = EstadoPagina.BUSCANDO;
					// estDlgDetPed = EstadoPagina.SUSPENDIDO;
					//
					// iniciarInstancias();
					// limpiarInstancias(AccesoPagina.INICIAR.getVal());
					// limpiarFormulario();
					// llenarFormulario();
					//
					// bookMarkPedCod = man.ped.getPed_cod();
					// buscarPedidoCliente(bookMarkPedCod);

				}// Fin de if (estDlgPed.equals(EstadoPagina.BUSCANDO))
				else {

					accesoIniciarFormulario();

				}
			}// Fin de else if (accion.equals(accesoPagina.INICIAR))
			else if (acceso.equals(AccesoPagina.ACTUALIZAR)) {

				accesoActualizarFormulario();

			}// Fin de if (accion.equals(accesoPagina.ACTUALIZAR))

			this.setVisible(true);
		} else {
			this.setVisible(false);
		}

	}

	@Override
	public void iniciarInstancias() {
		/*
		 * Obtiene una conexion del pool de conexiones de Apache
		 */
		man.transactionDelegate = (TransactionDelegate) ContenedorTextil.getComponent("TransactionDelegate");
		man.testProp = (TestPropiedades) ContenedorTextil.getComponent("TestPropiedades");
		man.funUsu = (FuncionesUsuario) ContenedorTextil.getComponent("FuncionesUsuario");

//		man.transactionDelegate.start();
		/*
		 * Obtiene las entidades del modelo relacional para su manejo en este
		 * formulario
		 */
		man.cli = (Cliente) ContenedorTextil.getComponent("Cliente");
		man.lisCli = (List<Cliente>) ContenedorTextil.getComponent("ListaCliente");

		man.gru = (Grupo) ContenedorTextil.getComponent("Grupo");
		man.gruCol = (Grupo) ContenedorTextil.getComponent("GrupoColor");
		man.gruMod = (Grupo) ContenedorTextil.getComponent("GrupoModelo");
		man.gruPrd = (Grupo) ContenedorTextil.getComponent("GrupoProducto");
		man.gruTal = (Grupo) ContenedorTextil.getComponent("GrupoTalla");

		man.claCol = (Clase) ContenedorTextil.getComponent("ClaseColor");
		man.cla = (Clase) ContenedorTextil.getComponent("Clase");

		man.lisGru = (List<Grupo>) ContenedorTextil.getComponent("ListaGrupo");
		man.lisCla = (List<Clase>) ContenedorTextil.getComponent("ListaClase");

		man.ped = (Pedido) ContenedorTextil.getComponent("Pedido");
		man.detPedTmp = (DetPedido) ContenedorTextil.getComponent("DetPedidoTemporal");
		man.detPed = (DetPedido) ContenedorTextil.getComponent("DetPedido");
		man.lisDetPed = (List<DetPedido>) ContenedorTextil.getComponent("ListaDetPedido");

		man.prdGen = (Producto) ContenedorTextil.getComponent("ProductoGeneral");
		man.prdPad = (Producto) ContenedorTextil.getComponent("ProductoPadre");
		man.mod = (Producto) ContenedorTextil.getComponent("Modelo");
		man.prd = (Producto) ContenedorTextil.getComponent("Producto");
		man.tal = (Producto) ContenedorTextil.getComponent("Talla");

		man.are = (Area) ContenedorTextil.getComponent("Area");
		man.lisAre = (List<Area>) ContenedorTextil.getComponent("ListaArea");

		man.subAre = (SubArea) ContenedorTextil.getComponent("SubArea");
		man.lisSubAre = (List<SubArea>) ContenedorTextil.getComponent("ListaSubArea");

		man.fic = (Ficha) ContenedorTextil.getComponent("Ficha");

		man.lisMod = (List<Producto>) ContenedorTextil.getComponent("ListaModelo");
		man.lisPrd = (List<Producto>) ContenedorTextil.getComponent("ListaProducto");
		man.lisTal = (List<Producto>) ContenedorTextil.getComponent("ListaTalla");
		man.lisPrdGen = (List<Producto>) ContenedorTextil.getComponent("ListaProductoGeneral");

		man.usuSes = (Usuario) ContenedorTextil.getComponent("UsuarioSesion");
		man.agru = (Agrupacion) ContenedorTextil.getComponent("Agrupacion");

	}

	@Override
	public void limpiarInstancias(AccesoPagina parAcceso) {
		if (parAcceso.equals(AccesoPagina.INICIAR) || parAcceso.equals(AccesoPagina.FINALIZAR)) {

			man.ped.limpiarInstancia();
			man.cli.limpiarInstancia();

			man.gru.limpiarInstancia();
			man.cla.limpiarInstancia();

			man.gruMod.limpiarInstancia();
			man.gruPrd.limpiarInstancia();
			man.gruTal.limpiarInstancia();
			man.detPed.limpiarInstancia();
			man.detPedTmp.limpiarInstancia();

			man.prdGen.limpiarInstancia();
			man.prdPad.limpiarInstancia();

			man.mod.limpiarInstancia();
			man.prd.limpiarInstancia();
			man.tal.limpiarInstancia();

			man.are.limpiarInstancia();
			man.subAre.limpiarInstancia();
			man.fic.limpiarInstancia();

		}// Fin de

		man.lisCli.clear();
		man.lisGru.clear();
		man.lisCla.clear();
		man.lisDetPed.clear();
		man.lisMod.clear();
		man.lisPrd.clear();
		man.lisTal.clear();
		man.lisAre.clear();
		man.lisSubAre.clear();
		man.lisPrdGen.clear();

	}

	@Override
	public void limpiarFormulario() {

		limpiarDlgPedido();
		limpiarDlgCliente();
		limpiarDlgModelo();
		limpiarDlgProducto();
		limpiarDlgTalla();

		modTabDetPrd.setRowCount(0);
		modTabDetTal.setRowCount(0);

	}

	@Override
	public void generarNuevoRegistro() {

	}

	@Override
	public void llenarFormulario() {

		man.obtenerListaGrupo();
		man.obtenerListaClase();

		// llenarComboColor();

	}

	@Override
	public void activarFormulario(boolean parAct) {

		activarDlgCliente(parAct);
		activarDlgPedido(parAct);
		activarDlgModelo(parAct);
		activarDlgProducto(parAct);
		activarDlgTalla(parAct);

	}

	@Override
	public void refrescarFormulario() {

		refrescarDlgPedido();
		refrescarDlgDetPedido();

	}

	@Override
	public void obtenerDatoFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {
		return true;
	}

	@Override
	public boolean guardarDatoBaseDato(String parModoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setManejador(AuxiliarTextil parMan) {

		this.man = parMan;

	}

	@Override
	public void cerrar() {
		if (padre.equals("FrmPrincipal")) {
			int resMsg = JOptionPane.showConfirmDialog(this,
					"�Deasea cerrar el sistema?",
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (resMsg == 0) {

				this.dispose();
				bookMarkCliCod = 0;
				bookMarkPedCod = 0;
				bookMarkDetPedCod = 0;
				bookMarkModCod = 0;
				bookMarkPrdCod = 0;
				bookMarkTalCod = 0;
				man.claveValidado = "";

				limpiarInstancias(AccesoPagina.FINALIZAR);
//				man.transactionDelegate.end();
//				try {
//					DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().commit();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
				man.abrirPantalla("FrmPrincipal", "FrmPrincipal", AccesoPagina.ACTUALIZAR);

			}// Fin de verificar que se desea cerrar el formulario
		}// El padre es cualquier formulario
		else {

			this.dispose();
			man.abrirPantalla(padre, "FrmPrincipal", AccesoPagina.ACTUALIZAR);

		}// Fin de Verificar que el padre es cualquier ventana
	}

	/*
	 * METODOS DE USUARIO
	 */

	public void limpiarInstanciasDetPedido() {

		man.gruCol.limpiarInstancia();
		man.claCol.limpiarInstancia();

		man.detPed.limpiarInstancia();
		man.prdGen.limpiarInstancia();
		man.gruTal.limpiarInstancia();
		man.tal.limpiarInstancia();

	}

	public void accesoIniciarFormulario() {

		estDlgPed = EstadoPagina.VISUALIZANDO;
		estDlgDetPed = EstadoPagina.VISUALIZANDO;

		frmAbierto = false;

		iniciarInstancias();
		limpiarInstancias(AccesoPagina.INICIAR);
		llenarFormulario();
		limpiarFormulario();

		bookMarkPedCod = (man.ped.getPed_cod() == 0) ? bookMarkPedCod : man.ped.getPed_cod();
		bookMarkDetPedCod = (man.detPed.getDetped_cod() == 0) ? bookMarkDetPedCod : man.detPed.getDetped_cod();
		bookMarkModCod = (man.mod.getPrd_cod() == 0) ? bookMarkModCod : man.mod.getPrd_cod();
		bookMarkPrdCod = (man.prd.getPrd_cod() == 0) ? bookMarkPrdCod : man.prd.getPrd_cod();
		bookMarkTalCod = (man.tal.getPrd_cod() == 0) ? bookMarkTalCod : man.tal.getPrd_cod();

		if (bookMarkPedCod > 0) {
			if (man.buscarPedidoDetalle(bookMarkPedCod, 1, 0)) {
				if(Util.strServicioWebPHP.compareTo("")==0)
				{
					//Util.strServicioWebPHP = Util.getInst().servicioWebPHP("http://sisprom.freetzi.com/yurak/soap/server.php", "getAcceso", "empresa", "AMPATO");
					Util.strServicioWebPHP = "SI";
				}
				bookMarkCliCod = man.cli.getCli_cod();
				llenarPedidoCliente();

			}
		}

		man.funUsu.ubicarRegistroEnteroTabla(tabDetPrd, bookMarkPrdCod, 12);// Codigo detalle de pedido

		if(Util.strServicioWebPHP.compareTo("NO")==0 && man.ped.getPed_src().compareTo("IMPORTACION")==0)
		{
			estDlgPed = EstadoPagina.SUSPENDIDO;
			estDlgDetPed = EstadoPagina.SUSPENDIDO;
			refrescarFormulario();
			cmdCerrar.setEnabled(true);
		}
		else{seleccionarRegistroProducto();}


		frmAbierto = true;
	}

	public void accesoActualizarFormulario() {

		accesoIniciarFormulario();

	}

	public void generarIdentificacionModelo(String parIde) {

		if ((estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO)) && !txtModIde.getText().equals("")) {

			boolean existe = true;
			String codigo = "";
			while (existe) {
				codigo = KeyGenerator.getInst().obtenerCodigoBarra();
				if (!man.existeCodigoBarra("PRODUCTO", codigo)) {
					man.mod.setPrd_ide(parIde);
					man.mod.setPrd_bar(codigo);
					existe = false;
				}
			}

		}// Fin de if(estDlgSuc.equals(estadoVentana.AGREGANDO) ||estDlgSuc.equals(estadoVentana.AGREGANDO))
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)

	public void generarIdentificacionProducto(String parIde) {

		if (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO)) {
			if (txtPrdCol.getText().trim().length() > 1) {

				boolean existe = true;
				String codigo = "";
				while (existe) {
					codigo = KeyGenerator.getInst().obtenerCodigoBarra();
					if (!man.existeCodigoBarra("PRODUCTO", codigo)) {
						man.prd.setPrd_ide(parIde);
						man.prd.setPrd_bar(codigo);
						existe = false;
					}
				}
			}
		}// Fin de if(estDlgSuc.equals(estadoVentana.AGREGANDO) ||estDlgSuc.equals(estadoVentana.AGREGANDO))
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)

	public void generarIdentificacionTalla(String parIde) {

		if (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO)) {

			boolean existe = true;
			String codigo = "";
			while (existe) {
				codigo = KeyGenerator.getInst().obtenerCodigoBarra();
				if (!man.existeCodigoBarra("PRODUCTO", codigo)) {
					man.tal.setPrd_ide(parIde);
					man.tal.setPrd_bar(codigo);
					existe = false;
				}
			}
		}// Fin de if(estDlgSuc.equals(estadoVentana.AGREGANDO) ||estDlgSuc.equals(estadoVentana.AGREGANDO))
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)

	private void crearModeloTablas() {

		crearModeloTablaDetProducto();
		crearModeloTablaDetTalla();

	}

	private void crearModeloTablaDetProducto() {

		modTabDetPrd = new ModeloTablaDetProducto();
		tabDetPrd.setModel(modTabDetPrd);
		celdaRenderGeneral = new CeldaRenderGeneral();

		try {

			tabDetPrd.setDefaultRenderer(Class.forName("java.lang.String"), celdaRenderGeneral);
			tabDetPrd.setDefaultRenderer(Class.forName("java.lang.Integer"), celdaRenderGeneral);
			tabDetPrd.setDefaultRenderer(Class.forName("java.lang.Double"), celdaRenderGeneral);
			tabDetPrd.setDefaultRenderer(Class.forName("java.sql.Date"), celdaRenderGeneral);

		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DlgPedido.class.getName()).log(Level.SEVERE, null, ex);
		}
		// tabDetPrd.addPropertyChangeListener(new modTabDetPrdPropiedad(this, tabDetPrd));

		tabDetPrd.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tabDetPrd.getTableHeader().setReorderingAllowed(false);
		tabDetPrd.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabDetPrd.setShowGrid(true);

		String[] header = { "Item", "Tipo", "Material", "Gauge", "Color", "Descripcion", "Stk-Act", "Can-Ped", "Can-Fab", "Can-Ent", "detped_cod", "ped_cod", "prd_cod", "gru_cod", "Estado" };
		// String[] tipo = { "String", "String", "String", "String", "String", "Integer", "Double", "Double", "Integer", "Integer", "Integer", "Integer", "Integer", "Integer", "Integer" };
		int[] ancho = { 30, 0, 0, 0, 0, 364, 0, 45, 45, 0, 0, 0, 0, 0, 90 };

		colTabDetPrd = new TableColumn[header.length + 1];
		for (int iteHead = 0; iteHead < header.length; iteHead++) {

			colTabDetPrd[iteHead] = tabDetPrd.getColumn(header[iteHead]);
			colTabDetPrd[iteHead].setResizable(true);
			colTabDetPrd[iteHead].setPreferredWidth(ancho[iteHead]);
			// colTabDetPrd[iteHead].addPropertyChangeListener(new ListenerCambioPropiedad());

			if (ancho[iteHead] == 0) {

				tabDetPrd.getColumnModel().getColumn(iteHead).setMaxWidth(0);
				tabDetPrd.getColumnModel().getColumn(iteHead).setMinWidth(0);
				tabDetPrd.getTableHeader().getColumnModel().getColumn(iteHead).setMaxWidth(0);
				tabDetPrd.getTableHeader().getColumnModel().getColumn(iteHead).setMinWidth(0);

			}// Fin de if (anchoHeader[iteHead] == 0)
				// if (iteHead == 5) {
			// colTabDetPrd[iteHead].setCellEditor(new CellEditEnteroTabDetPed(this));
			// }
		}// Recorrer la tabla de encabezados
	}

	private void crearModeloTablaDetTalla() {

		modTabDetTal = new ModeloTablaDetTalla();
		tabDetTal.setModel(modTabDetTal);
		celdaRenderGeneral = new CeldaRenderGeneral();

		try {

			tabDetTal.setDefaultRenderer(Class.forName("java.lang.String"), celdaRenderGeneral);
			tabDetTal.setDefaultRenderer(Class.forName("java.lang.Integer"), celdaRenderGeneral);
			tabDetTal.setDefaultRenderer(Class.forName("java.lang.Double"), celdaRenderGeneral);
			tabDetTal.setDefaultRenderer(Class.forName("java.sql.Date"), celdaRenderGeneral);

		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DlgPedido.class.getName()).log(Level.SEVERE, null, ex);
		}
		// tabDetTalla.addPropertyChangeListener(new modTabDetTalPropiedad(this, tabDetTalla));

		tabDetTal.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tabDetTal.getTableHeader().setReorderingAllowed(false);
		tabDetTal.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabDetTal.setShowGrid(true);

		String[] header = { "Item", "Tipo", "Material", "Gauge", "Color", "Descripcion", "Stk-Act", "Can-Ped", "Can-Fab", "Can-Ent", "detped_cod", "ped_cod", "prd_cod", "gru_cod", "Estado" };
		// String[] tipo = { "String", "String", "String", "String", "String", "Integer", "Double", "Double", "Integer", "Integer", "Integer", "Integer", "Integer", "Integer", "Integer" };
		int[] ancho = { 30, 0, 0, 0, 0, 115, 0, 43, 0, 0, 0, 0, 0, 0, 0 };

		colTabDetTal = new TableColumn[header.length + 1];
		for (int iteHead = 0; iteHead < header.length; iteHead++) {

			colTabDetTal[iteHead] = tabDetTal.getColumn(header[iteHead]);
			colTabDetTal[iteHead].setResizable(true);
			colTabDetTal[iteHead].setPreferredWidth(ancho[iteHead]);
			// colTabDetTal[iteHead].addPropertyChangeListener(new ListenerCambioPropiedad());

			if (ancho[iteHead] == 0) {

				tabDetTal.getColumnModel().getColumn(iteHead).setMaxWidth(0);
				tabDetTal.getColumnModel().getColumn(iteHead).setMinWidth(0);
				tabDetTal.getTableHeader().getColumnModel().getColumn(iteHead).setMaxWidth(0);
				tabDetTal.getTableHeader().getColumnModel().getColumn(iteHead).setMinWidth(0);

			}// Fin de if (anchoHeader[iteHead] == 0)
				// if (iteHead == 5) {
			// colTabDetTal[iteHead].setCellEditor(new CellEditEnteroTabDetPed(this));
			// }
		}// Recorrer la tabla de encabezados
	}

	private void limpiarDlgCliente() {

		txtCliIde.setText("");
		txtCliDoc.setText("");
		txtCliNum.setText("");
		txtCliNom.setText("");
		txtCliDir.setText("");

		scrLisCliNom.setVisible(false);

	}

	private void limpiarDlgPedido() {

		// etiPedNomDoc.setText("");
		txtPedNumDoc.setText("");
		cmbPedFecEnt.setDate(null);
		cmbPedFecReg.setDate(null);
		txtPedAno.setValue(0);
		txtPedTemp.setValue(0);
		txtPedEst.setText("");
		txtPedImpTot.setValue(0.00);
		lisCliNom.setVisible(false);

	}

	private void limpiarDlgModelo() {

		if (man.mod.getPrd_cod() == 0 || estDlgPed.equals(EstadoPagina.AGREGANDO)) {

			txtModDes.setText("");
			txtModIde.setText("");
			txtModTip.setText("");
			txtModMat.setText("");
			txtModGau.setText("");
			txtModGro.setText("");
			scrLisModIde.setVisible(false);
			scrLisModTip.setVisible(false);
			scrLisModMat.setVisible(false);
			scrLisModGau.setVisible(false);
			scrLisModGro.setVisible(false);

			panModFoto.setImagen(null, null, null, "CONSTRAINT");
			panPrdBar.setImagen(null, null, null, "CONSTRAINT");

		}

	}

	private void limpiarDlgProducto() {

		if (man.prd.getPrd_cod() == 0 || estDlgPed.equals(EstadoPagina.AGREGANDO)) {

			// cmbPrdCol.setSelectedIndex(-1);
			txtPrdCol.setText("");
			txtPrdIde.setText("");
			txtPrdIde.setText("");
			txtPrdDes.setText("");
			txtPrdTot.setValue(0);

			scrLisPrdCol.setVisible(false);

		}

	}

	private void limpiarDlgTalla() {

		txtTalIde.setText("");
		txtTalIde.setText("");
		txtTalDes.setText("");
		txtTalCan.setValue(0);
		txtTalTot.setValue(0);

	}

	// private void llenarComboColor() {
	//
	// varLisModCol.clear();
	// cmbPrdCol.removeAllItems();
	// for (Grupo iteGru : man.lisGru) {
	// if (iteGru.getGru_tipo().equals("DETALLE") && iteGru.getGru_nom().equals("Color")) {
	// for (Clase iteCla : man.lisCla) {
	// if (iteCla.getGru_cod() == iteGru.getGru_cod()) {
	//
	// cmbPrdCol.addItem(iteCla.getCla_nom());
	// varLisModCol.add(iteCla.getCla_cod() + "#" + iteCla.getGru_cod() + "#" + iteCla.getCla_nom() + "#" + iteCla.getCla_des());
	// }
	// }
	// break;
	// }
	// }
	// }

	public void llenarTablaProducto() {

		Object[] record = { 0, "", "", "", "", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, "" };
		modTabDetPrd.setRowCount(0);
		int numItem = 0;
		int totCanPed = 0;

		for (DetPedido iteDetPed : man.lisDetPed) {

			man.prdGen.setObjeto(man.obtenerItemListaProducto(iteDetPed.getPrd_cod()));
			if (man.prdGen.getPrd_nivel() == 2) {

				modTabDetPrd.addRow(record);
				modTabDetPrd.setValueAt(numItem + 1, numItem, 0);// Item
				modTabDetPrd.setValueAt(man.prdGen.getPrd_tip(), numItem, 1);// Tipo
				modTabDetPrd.setValueAt(man.prdGen.getPrd_mat(), numItem, 2);// Material
				modTabDetPrd.setValueAt(man.prdGen.getPrd_gau(), numItem, 3);// Gauge
				modTabDetPrd.setValueAt(man.prdGen.getPrd_col(), numItem, 4);// Color
				modTabDetPrd.setValueAt(man.prdGen.getPrd_des(), numItem, 5);// Descripcion
				modTabDetPrd.setValueAt(man.prdGen.getPrd_des(), numItem, 6);// Stk-Act
				modTabDetPrd.setValueAt(iteDetPed.getDetped_canped(), numItem, 7);// Can-Ped
				modTabDetPrd.setValueAt(iteDetPed.getDetped_canfab(), numItem, 8);// Can-Fab
				modTabDetPrd.setValueAt(iteDetPed.getDetped_canent(), numItem, 9);// Can-Ent
				modTabDetPrd.setValueAt(iteDetPed.getDetped_cod(), numItem, 10);// detped_cod
				modTabDetPrd.setValueAt(man.ped.getPed_cod(), numItem, 11);// ped_cod
				modTabDetPrd.setValueAt(man.prdGen.getPrd_cod(), numItem, 12);// prd_cod
				modTabDetPrd.setValueAt(man.prdGen.getGru_cod(), numItem, 13);// gru_cod
				modTabDetPrd.setValueAt(iteDetPed.getDetped_est(), numItem, 14);// Estado del detalle de pedido

				totCanPed = totCanPed + iteDetPed.getDetped_canped();
				numItem++;

			}// Fin de if(man.prdGen.getPrd_nivel() == 2)
		}// Fin de for(DetPedido iteDetPed : man.lisDetPed)

		// man.ped.setPed_imptot(pedImp);
		txtPrdTot.setValue(totCanPed);

	}// Fin de llenarTablaDetPedido

	public void llenarTablaTalla() {

		Object[] record = { 0, "", "", "", "", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, "" };
		modTabDetTal.setRowCount(0);
		int numItem = 0;
		int totCanPed = 0;

		for (DetPedido iteDetPed : man.lisDetPed) {

			man.prdGen.setObjeto(man.obtenerItemListaTalla(iteDetPed.getPrd_cod()));
			if (man.prdGen.getPrd_nivel() == 3) {

				modTabDetTal.addRow(record);
				modTabDetTal.setValueAt(numItem + 1, numItem, 0);// Item
				modTabDetTal.setValueAt(man.prdGen.getPrd_tip(), numItem, 1);// Tipo
				modTabDetTal.setValueAt(man.prdGen.getPrd_mat(), numItem, 2);// Material
				modTabDetTal.setValueAt(man.prdGen.getPrd_gau(), numItem, 3);// Gauge
				modTabDetTal.setValueAt(man.prdGen.getPrd_col(), numItem, 4);// Color
				modTabDetTal.setValueAt(man.prdGen.getPrd_des(), numItem, 5);// Descripcion
				modTabDetTal.setValueAt(man.prdGen.getPrd_des(), numItem, 6);// Stk-Act
				modTabDetTal.setValueAt(iteDetPed.getDetped_canped(), numItem, 7);// Can-Ped
				modTabDetTal.setValueAt(iteDetPed.getDetped_canfab(), numItem, 8);// Can-Fab
				modTabDetTal.setValueAt(iteDetPed.getDetped_canent(), numItem, 9);// Can-Ent
				modTabDetTal.setValueAt(iteDetPed.getDetped_cod(), numItem, 10);// detped_cod
				modTabDetTal.setValueAt(man.ped.getPed_cod(), numItem, 11);// ped_cod
				modTabDetTal.setValueAt(man.prdGen.getPrd_cod(), numItem, 12);// prd_cod
				modTabDetTal.setValueAt(man.prdGen.getGru_cod(), numItem, 13);// gru_cod
				modTabDetTal.setValueAt(iteDetPed.getDetped_est(), numItem, 14);// Estado del detalle de pedido

				totCanPed = totCanPed + iteDetPed.getDetped_canped();

				numItem++;

			}// Fin de if(man.prdGen.getPrd_nivel() == 2)
		}// Fin de for(DetPedido iteDetPed : man.lisDetPed)

		// man.ped.setPed_imptot(pedImp);
		// txtPedImpTot.setValue(man.ped.getPed_imptot());
		txtTalTot.setValue(totCanPed);

	}// Fin de llenarTablaDetPedido

	private void llenarPedidoCliente() {

		frmAbierto = false;

		llenarDlgCliente();
		llenarDlgPedido();
		llenarDlgModelo();
		llenarTablaProducto();

		frmAbierto = true;
	}// Fin de buscarVenta()

	private void seleccionarRegistroProducto() {
		if (estDlgDetPed.equals(EstadoPagina.VISUALIZANDO)) {

			obtenerRegistroProducto();
			llenarProducto();

			if (!tabDetPrd.isEditing()) {

				refrescarFormulario();

				if (tabDetPrd.getSelectedRow() > -1) {

					man.lisTal.clear();
					man.lisTal.addAll(man.obtenerListaProductoPedido(man.ped.getPed_cod(), 3, man.prd.getPrd_cod()));
					llenarTablaTalla();
					// cmdDetPedModificar.setEnabled(true);
					// cmdDetPedEliminar.setEnabled(true);
					cmdDetPedImprimir.setEnabled(true);

				}
			}// Fin de if(!tabDetVen.isEditing())
		}
	}// Fin de seleccionarRegistroTablaSucursal()

	private void obtenerRegistroProducto() {

		int filTab = tabDetPrd.getSelectedRow();
		if (filTab > -1) {

			bookMarkDetPedCod = (Integer) tabDetPrd.getValueAt(tabDetPrd.getSelectedRow(), 10);// Codigo del detalle del pedido
			man.detPed.setObjeto(man.obtenerItemListaDetPedido(bookMarkDetPedCod));

			man.prd.setObjeto(man.obtenerItemListaProducto(man.detPed.getPrd_cod()));
			man.gruPrd.setObjeto(man.obtenerItemListaGrupo(man.prd.getGru_cod()));

			man.gruTal.limpiarInstancia();
			man.tal.limpiarInstancia();

			bookMarkPrdCod = man.prd.getPrd_cod();
			bookMarkTalCod = 0;

		}// Fin de if(parNumFil > -1)
		else {

			bookMarkDetPedCod = 0;
			bookMarkModCod = 0;
			bookMarkPrdCod = 0;
			bookMarkTalCod = 0;

			man.detPed.limpiarInstancia();

			man.gruPrd.limpiarInstancia();
			man.prd.limpiarInstancia();

			man.gruTal.limpiarInstancia();
			man.tal.limpiarInstancia();
		}
	}// Fin de obtenerRegistroTablaContacto()

	private void seleccionarRegistroTalla() {

		if (estDlgDetPed.equals(EstadoPagina.VISUALIZANDO)) {
			obtenerRegistroTalla();
			llenarTalla();

			if (!tabDetTal.isEditing()) {

				refrescarFormulario();

				if (tabDetTal.getSelectedRow() > -1) {

					cmdDetPedModificar.setEnabled(true);
					cmdDetPedEliminar.setEnabled(true);
					// cmdDetPedImprimir.setEnabled(true);

				}
			}// Fin de if(!tabDetVen.isEditing())

		}
	}// Fin de seleccionarRegistroTablaSucursal()

	private void obtenerRegistroTalla() {

		int filTab = tabDetTal.getSelectedRow();
		if (filTab > -1) {

			bookMarkDetPedCod = (Integer) tabDetTal.getValueAt(tabDetTal.getSelectedRow(), 10);// Codigo del detalle del pedido
			man.detPed.setObjeto(man.obtenerItemListaDetPedido(bookMarkDetPedCod));

			man.tal.setObjeto(man.obtenerItemListaTalla(man.detPed.getPrd_cod()));
			man.gruTal.setObjeto(man.obtenerItemListaGrupo(man.tal.getGru_cod()));

			bookMarkTalCod = man.tal.getPrd_cod();

		}// Fin de if(parNumFil > -1)
		else {

			bookMarkDetPedCod = 0;
			bookMarkTalCod = 0;

			man.detPed.limpiarInstancia();

			man.gruTal.limpiarInstancia();
			man.tal.limpiarInstancia();
		}
	}// Fin de obtenerRegistroTablaContacto()

	private boolean validarDlgPedido() {

		if (cmbPedFecReg.getDate() != null) {
			if (cmbPedFecEnt.getDate() != null) {
				if (Integer.parseInt(txtPedTemp.getText()) > 0) {

					return true;

				} else {

					JOptionPane.showConfirmDialog(this,
							"���Ingrrese una temporada correcta!!!\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

				}
			} else {
				JOptionPane.showConfirmDialog(this,
						"���Ingrese fecha de entrega correcta!!!\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			}
		} else {
			JOptionPane.showConfirmDialog(this,
					"���Ingrese fecha de registro correcta!!!\n" + this.getClass().getName(),
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
		}

		return false;
	}

	private boolean validarDlgDetPedido() {

		if (validarIdentificador(txtModIde.getText())) {
			if (!txtModDes.getText().equals("")) {
				if (!txtModTip.getText().equals("")) {
					if (!txtModMat.getText().equals("")) {
						if (!txtModGau.getText().equals("")) {
							if (!txtModGro.getText().equals("")) {
								if (validarIdentificador(txtPrdCol.getText().trim())) {
									if (validarIdentificador(txtPrdIde.getText())) {
										if (!txtPrdDes.getText().equals("")) {
											if (!txtTalDes.getText().equals("")) {
												if (Integer.parseInt(txtTalCan.getText()) > 0) {

													return true;

												} else {
													JOptionPane.showConfirmDialog(this,
															"���Ingrese cantidad a solicita!!!\n" + this.getClass().getName(),
															man.testProp.getPropiedad("TituloSistema"),
															JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
												}
											} else {
												JOptionPane.showConfirmDialog(this,
														"���Ingrese descripcion de talla !!!\n" + this.getClass().getName(),
														man.testProp.getPropiedad("TituloSistema"),
														JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
											}
										} else {
											JOptionPane.showConfirmDialog(this,
													"���Ingrese descripcion de producto !!!\n" + this.getClass().getName(),
													man.testProp.getPropiedad("TituloSistema"),
													JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
										}
									}
								} else {
									JOptionPane.showConfirmDialog(this,
											"���Ingrese Color de producto !!!\n" + this.getClass().getName(),
											man.testProp.getPropiedad("TituloSistema"),
											JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
								}
							} else {
								JOptionPane.showConfirmDialog(this,
										"���Ingrese grosor!!!\n" + this.getClass().getName(),
										man.testProp.getPropiedad("TituloSistema"),
										JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
							}
						} else {
							JOptionPane.showConfirmDialog(this,
									"���Ingrese Galga !!!\n" + this.getClass().getName(),
									man.testProp.getPropiedad("TituloSistema"),
									JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
						}
					} else {
						JOptionPane.showConfirmDialog(this,
								"���Ingrese material !!!\n" + this.getClass().getName(),
								man.testProp.getPropiedad("TituloSistema"),
								JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
					}
				} else {
					JOptionPane.showConfirmDialog(this,
							"���Ingrese tipo !!!\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
				}
			} else {
				JOptionPane.showConfirmDialog(this,
						"���Ingrese descripcion para modelo!!!\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			}
		}

		return false;

	}

	private boolean validarIdentificador(String parPrdCol) {

		// System.out.println("lengt : " + parPrdCol.length() + " indice : " + parPrdCol.indexOf("-"));
		if (parPrdCol.contains("-")) {
			String numero = parPrdCol.substring(0, parPrdCol.indexOf("-"));
			if (man.funUsu.isInteger(numero)) {
				if (parPrdCol.length() > parPrdCol.indexOf("-") + 1) {

					return true;

				} else {
					JOptionPane.showConfirmDialog(null, "LA SEGUNDA PARTE DEL CODIGO DEBE SER TEXTO", man.testProp.getPropiedad("TituloSistema"), -1, 1);
				}
			} else {
				JOptionPane.showConfirmDialog(null, "LA PRIMERA PARTE DEL CODIGO DEBE SER NUMERO", man.testProp.getPropiedad("TituloSistema"), -1, 1);
			}
		} else {
			JOptionPane.showConfirmDialog(null, "LA ESTRUCTURA DE COLOR DEBE SER 01-IDENTIFICADOR", man.testProp.getPropiedad("TituloSistema"), -1, 1);
		}
		return false;
	}

	public void enviarImpresora(String tipoReporte) {
		File repFactura;
			
		if (tipoReporte.equals("PEDIDO")) {
			repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "ReportePedido.jasper");
		} else if (tipoReporte.equals("MODELO")) {
			repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "FichaControlModelo.jasper");
		} else if (tipoReporte.equals("PRODUCTO")) {
			repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "FichaControlProducto.jasper");
		} else {
			repFactura = null;
		}
		if (repFactura != null) {
			try {
				InputStream inpStrRepFac = new FileInputStream(repFactura);
				HashMap map = new HashMap();
				map.put("PED_COD", man.ped.getPed_cod());
				map.put("PRD_SUPUNO", man.mod.getPrd_cod());
				map.put("PRD_SUPDOS", man.prd.getPrd_cod());
				try {

					JasperPrint print = JasperFillManager.fillReport(inpStrRepFac, map, DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection());

					JasperViewer jv = new JasperViewer(print, false);
					jv.setTitle("Reporte de Pedidos");
					jv.setSize(800, 600);
					jv.setVisible(true);

					// JasperPrintManager.printReport(print, true);

				} catch (JRException ex) {
					JOptionPane.showConfirmDialog(null, "Error de conexion con la impresora", man.testProp.getPropiedad("TituloSistema"), -1, 1);
				}
			} catch (FileNotFoundException ex) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * ACTIVAR CONTROLES
	 */

	private void activarDlgCliente(boolean parAct) {

		txtCliIde.setEnabled((estDlgPed.equals(EstadoPagina.AGREGANDO)) ? true : false);
		txtCliDoc.setEnabled(parAct);
		txtCliNum.setEnabled(parAct);
		txtCliNom.setEnabled(parAct);
		txtCliDir.setEnabled(parAct);

	}

	private void activarDlgPedido(boolean parAct) {

		if (estDlgPed.equals(EstadoPagina.BUSCANDO)) {

			etiPedNomDoc.setText(TipoDocumento.NOTAPEDIDO.getVal());
			txtPedNumDoc.setEnabled(true);

		}// Fin de if(estDlgKar.equals(estadoVentana.BUSCANDO))
		else if (!estDlgPed.equals(EstadoPagina.BUSCANDO)) {

			etiPedNomDoc.setEnabled(true);
			txtPedNumDoc.setEnabled(false);
			cmbPedFecEnt.setEnabled(parAct);
			cmbPedFecReg.setEnabled(parAct);
			txtPedAno.setEnabled(parAct);
			txtPedTemp.setEnabled(parAct);
			txtPedEst.setEnabled(false);
			txtPedImpTot.setEnabled(false);

		}
	}

	private void activarDlgModelo(boolean parAct) {

		txtModIde.setEnabled(parAct);
		txtModDes.setEnabled(parAct);
		txtModTip.setEnabled(parAct);
		txtModMat.setEnabled(parAct);
		txtModGau.setEnabled(parAct);
		txtModGro.setEnabled(parAct);

		panModFoto.setEnabled(parAct);
		if (estDlgPed.equals(EstadoPagina.VISUALIZANDO) || estDlgPed.equals(EstadoPagina.SUSPENDIDO))
			panPrdBar.setVisible(true);
		else {
			panPrdBar.setVisible(false);
		}

	}

	private void activarDlgProducto(boolean parAct) {

		txtPrdCol.setEnabled(parAct);
		txtPrdIde.setEnabled(parAct);
		txtPrdIde.setEnabled(parAct);
		txtPrdDes.setEnabled(parAct);
		txtPrdTot.setEnabled(false);

	}

	private void activarDlgTalla(boolean parAct) {

		if (estDlgDetPed.equals(EstadoPagina.AGREGANDO)) {
			txtPrdCol.setEnabled(true);
			txtPrdDes.setEnabled(true);
		} else {
			txtPrdCol.setEnabled(false);
			txtPrdDes.setEnabled(false);
		}

		txtTalIde.setEnabled(parAct);
		txtTalDes.setEnabled(parAct);
		txtTalCan.setEnabled(parAct);
		txtTalTot.setEnabled(false);

	}

	/*
	 * 
	 * REFRESCAR CONTROLES
	 */
	private void refrescarDlgPedido() {
		switch (estDlgPed) {
		case VISUALIZANDO:
			activarDlgPedido(false);
			activarDlgCliente(false);

			cmdPedAgregar.setText("Agregar");
			cmdPedModificar.setText("Modificar");

			if (man.ped.getPed_cod() > 0) {

				cmdPedAgregar.setEnabled(true);
				cmdPedModificar.setEnabled(true);
				cmdPedBuscar.setEnabled(true);
				cmdPedDocumento.setEnabled(true);
				cmdPedFichas.setEnabled(true);
				cmdFichaControl.setEnabled(true);
				cmdPedEliminar.setEnabled(true);

			} else {

				cmdPedAgregar.setEnabled(true);
				cmdPedModificar.setEnabled(false);
				cmdPedBuscar.setEnabled(true);
				cmdPedDocumento.setEnabled(false);
				cmdPedFichas.setEnabled(false);
				cmdFichaControl.setEnabled(false);
				cmdPedEliminar.setEnabled(false);

			}// Fin de else if (man.vou.getVou_cod().equals(""))
			cmdCerrar.setEnabled(true);
			break;

		case AGREGANDO:

			activarDlgPedido(true);
			activarDlgCliente(true);
			limpiarFormulario();
			limpiarInstancias(AccesoPagina.INICIAR);

			cmdPedAgregar.setText("Guardar");
			cmdPedModificar.setText("Cancelar");

			cmdPedAgregar.setEnabled(true);
			cmdPedModificar.setEnabled(true);
			cmdPedBuscar.setEnabled(false);
			cmdPedDocumento.setEnabled(false);
			cmdPedFichas.setEnabled(false);
			cmdPedEliminar.setEnabled(false);
			cmdFichaControl.setEnabled(false);
			cmdCerrar.setEnabled(false);

			break;

		case MODIFICANDO:

			activarDlgPedido(true);
			activarDlgCliente(true);

			cmdPedAgregar.setText("Guardar");
			cmdPedModificar.setText("Cancelar");

			cmdPedAgregar.setEnabled(true);
			cmdPedModificar.setEnabled(true);
			cmdPedBuscar.setEnabled(false);
			cmdPedDocumento.setEnabled(false);
			cmdPedFichas.setEnabled(false);
			cmdPedEliminar.setEnabled(false);
			cmdFichaControl.setEnabled(false);
			cmdCerrar.setEnabled(false);

			break;

		case SUSPENDIDO:

			activarDlgPedido(false);

			cmdPedAgregar.setText("Guardar");
			cmdPedModificar.setText("Cancelar");

			cmdPedAgregar.setEnabled(false);
			cmdPedModificar.setEnabled(false);
			cmdPedBuscar.setEnabled(false);
			cmdPedDocumento.setEnabled(false);
			cmdPedFichas.setEnabled(false);
			cmdPedEliminar.setEnabled(false);
			cmdFichaControl.setEnabled(false);
			cmdCerrar.setEnabled(false);
			break;

		case BUSCANDO:

			activarDlgPedido(false);
			limpiarFormulario();
			cmdPedModificar.setText("Cancelar");

			cmdPedAgregar.setEnabled(false);
			cmdPedModificar.setEnabled(true);
			cmdPedBuscar.setEnabled(false);
			cmdPedDocumento.setEnabled(false);
			cmdPedFichas.setEnabled(false);
			cmdPedEliminar.setEnabled(false);
			cmdFichaControl.setEnabled(false);
			cmdCerrar.setEnabled(false);
			break;
		}// Fin de switch para estado del formaulario de cliente
	}

	private void refrescarDlgDetPedido() {
		switch (estDlgDetPed) {
		case VISUALIZANDO:

			activarDlgModelo(false);
			activarDlgProducto(false);
			activarDlgTalla(false);

			tabDetPrd.setEnabled(true);
			tabDetTal.setEnabled(true);

			cmdDetPedAgregar.setText("Agregar");
			cmdDetPedModificar.setText("Modificar");

			cmdDetPedAgregar.setEnabled((man.ped.getPed_cod() > 0) ? true : false);
			cmdDetPedModificar.setEnabled((tabDetTal.getSelectedRow() > 0) ? true : false);
			cmdDetPedEliminar.setEnabled((tabDetTal.getSelectedRow() > 0) ? true : false);
			cmdDetPedBuscar.setEnabled(false);
			cmdDetPedImprimir.setEnabled((tabDetTal.getSelectedRow() > 0) ? true : false);

			break;
		case AGREGANDO:

			activarDlgModelo((man.mod.getPrd_cod() == 0) ? true : false);
			activarDlgProducto((man.prd.getPrd_cod() == 0) ? true : false);
			activarDlgTalla(true);

			limpiarDlgModelo();
			limpiarDlgProducto();
			limpiarDlgTalla();
			limpiarInstanciasDetPedido();

			modTabDetPrd.setRowCount(0);
			// modTabDetTal.setRowCount(0);
			tabDetTal.setEnabled(false);

			cmdDetPedAgregar.setText("Guardar");
			cmdDetPedModificar.setText("Cancelar");

			cmdDetPedAgregar.setEnabled(true);
			cmdDetPedModificar.setEnabled(true);
			cmdDetPedBuscar.setEnabled(false);
			cmdDetPedImprimir.setEnabled(false);
			cmdDetPedEliminar.setEnabled(false);
			break;

		case MODIFICANDO:

			activarDlgModelo((man.mod.getPrd_cod() == 0) ? true : false);
			activarDlgProducto((man.prd.getPrd_cod() == 0) ? true : false);
			activarDlgTalla(true);

			modTabDetPrd.setRowCount(0);
			// modTabDetTal.setRowCount(0);
			tabDetTal.setEnabled(false);

			cmdDetPedAgregar.setText("Guardar");
			cmdDetPedModificar.setText("Cancelar");

			cmdDetPedAgregar.setEnabled(true);
			cmdDetPedModificar.setEnabled(true);
			cmdDetPedBuscar.setEnabled(false);
			cmdDetPedImprimir.setEnabled(false);
			cmdDetPedEliminar.setEnabled(false);
			break;

		case SUSPENDIDO:

			activarDlgModelo(false);
			activarDlgProducto(false);
			activarDlgTalla(false);

			tabDetPrd.setEnabled(false);
			tabDetTal.setEnabled(false);

			cmdDetPedAgregar.setText("Agregar");
			cmdDetPedModificar.setText("Guardar");

			cmdDetPedAgregar.setEnabled(false);
			cmdDetPedModificar.setEnabled(false);
			cmdDetPedBuscar.setEnabled(false);
			cmdDetPedImprimir.setEnabled(false);
			cmdDetPedEliminar.setEnabled(false);
			break;

		case BUSCANDO:

			activarDlgModelo(false);
			activarDlgProducto(false);
			activarDlgTalla(false);

			cmdDetPedModificar.setText("Cancelar");

			cmdDetPedAgregar.setEnabled(false);
			cmdDetPedModificar.setEnabled(true);
			cmdDetPedBuscar.setEnabled(false);
			cmdDetPedImprimir.setEnabled(false);
			cmdDetPedEliminar.setEnabled(false);
			break;
		}// Fin de switch para estado del formaulario de cliente
	}

	/*
	 * 
	 * 
	 * 
	 * LLENAR REGISTROS DE FORMULARIO
	 */
	private void llenarDlgCliente() {

		txtCliIde.setText(man.cli.getCli_ide());
		txtCliDoc.setText(man.cli.getCli_doc());
		txtCliNum.setText(man.cli.getCli_num());
		txtCliNom.setText(man.cli.getCli_nom());
		txtCliDir.setText(man.cli.getCli_dir());

	}// Fin de llenarDlgCliente

	private void llenarDlgPedido() {

		etiPedNomDoc.setText(man.ped.getPed_nomdoc());
		txtPedNumDoc.setText(man.ped.getPed_numdoc());
		cmbPedFecReg.setDate(man.ped.getPed_fecreg());
		cmbPedFecEnt.setDate(man.ped.getPed_fecent());
		txtPedAno.setValue(man.ped.getPed_fecreg().getYear() + 1900);
		txtPedTemp.setValue(Integer.parseInt(man.ped.getPed_temp()));
		txtPedEst.setText(man.ped.getPed_est());
		txtPedImpTot.setValue(man.ped.getPed_imptot());

	}// Fin de llenarDlgPedido

	private void llenarDlgModelo() {

		txtModIde.setText(man.mod.getPrd_ide());
		txtModDes.setText(man.mod.getPrd_des());
		txtModTip.setText(man.mod.getPrd_tip());
		txtModMat.setText(man.mod.getPrd_mat());
		txtModGau.setText(man.mod.getPrd_gau());
		txtModGro.setText(man.mod.getPrd_gro());

		CodigoBarras codigoBarras = (CodigoBarras) ContenedorTextil.getComponent("CodigoBarras");
		panModFoto.setImagen(man.mod.getPrd_foto(), null, null, "CONSTRAINT");
		panPrdBar.setImagen(codigoBarras.obtenerImageIcon(man.mod.getPrd_bar()), null, null, "CONSTRAINT");

	}// Fin de llenarProducto

	private void llenarProducto() {

		frmAbierto = false;
		txtModMat.setText(man.prd.getPrd_mat());
		txtPrdCol.setText(man.prd.getPrd_col());
		txtPrdIde.setText(man.prd.getPrd_ide());
		txtPrdDes.setText(man.prd.getPrd_des());
		frmAbierto = true;
	}// Fin de llenarProducto

	private void llenarTalla() {

		txtTalIde.setText(man.tal.getPrd_ide());
		txtTalDes.setText(man.tal.getPrd_des());
		txtTalCan.setValue(man.detPed.getDetped_canped());

	}// Fin de llenarProducto

	private int agregarPedidoCliente() {

		bookMarkCliCod = man.agregarCliente(0, txtCliIde.getText());
		if (bookMarkCliCod == 0) {
			bookMarkCliCod = man.actualizarCliente(man.cli.getCli_cod(), txtCliIde.getText());
		}
		man.cli.setObjeto(man.obtenerCliente(bookMarkCliCod));
		if (bookMarkCliCod != 0) {

			return man.agregarPedido();

		}
		return 0;

	}// Fin de agregar Documento Pedido

	private int actualizarPedidoCliente() {

		bookMarkCliCod = man.actualizarCliente(man.cli.getCli_cod(), txtCliIde.getText());
		man.cli.setObjeto(man.obtenerCliente(bookMarkCliCod));
		if (bookMarkCliCod != 0) {

			return man.actualizarPedido();

		}
		return 0;

	}// Fin de agregar Documento Pedido

	/*
	 * 
	 * 
	 * METODOS PARA OBTENER DATOS DEL FORMULARIO
	 */

	private void obtenerDatoDlgDetPedido(Producto parPrd, Grupo parGru) {

		// man.detPed.setDetped_cod("AUTONUMERICO");
		man.detPed.setPed_cod(man.ped.getPed_cod());
		man.detPed.setPrd_cod(parPrd.getPrd_cod());
		man.detPed.setGru_cod(parGru.getGru_cod());
		man.detPed.setDetped_canped(Integer.parseInt(txtTalCan.getText()));
		// man.detPed.setDetped_canfab(resSql.getInt("detped_canfab"));
		// man.detPed.setDetped_canent(resSql.getInt("detped_canent"));
		// man.detPed.setDetped_pre(resSql.getDouble("detped_pre"));
		// man.detPed.setDetped_imp(resSql.getDouble("detped_imp"));
		man.detPed.setDetped_est(EstadoDocumento.PENDIENTE.getVal());
		// man.detPed.setDetped_ver();

	}

	private void obtenerDatoDlgProductoNivelGeneral() {

		// man.prd.setPrd_cod(0);//Autonumerico
		man.prd.setGru_cod(man.gru.getGru_cod());// obtiene el codigo de grupo
		man.prd.setPrd_ide(txtPrdIde.getText());
		man.prd.setPrd_tip(man.prd.getPrd_tip());
		man.prd.setPrd_mat(man.prd.getPrd_mat());
		man.prd.setPrd_gau(man.prd.getPrd_gau());
		man.prd.setPrd_col(man.prd.getPrd_col());
		man.prd.setPrd_des(txtPrdDes.getText());
		man.prd.setPrd_stkact(0);

		if (man.gru.getGru_nivel() == 1) {

			man.prd.setPrd_supuno(0);
			man.prd.setPrd_supdos(0);
			man.prd.setPrd_suptres(0);
			man.prd.setPrd_supcua(0);

		}// Fin de if(man.gru.getGru_nivel() == 1)
		else if (man.gru.getGru_nivel() == 2) {

			man.prd.setPrd_supuno(man.prdPad.getPrd_cod());
			man.prd.setPrd_supdos(0);
			man.prd.setPrd_suptres(0);
			man.prd.setPrd_supcua(0);

		}// Fin de else if(man.gru.getGru_nivel() == 2)
		else if (man.gru.getGru_nivel() == 3) {

			man.prd.setPrd_supuno(man.prdPad.getPrd_supuno());
			man.prd.setPrd_supdos(man.prdPad.getPrd_cod());
			man.prd.setPrd_suptres(0);
			man.prd.setPrd_supcua(0);

		} else if (man.gru.getGru_nivel() == 4) {

			man.prd.setPrd_supuno(man.prdPad.getPrd_supuno());
			man.prd.setPrd_supdos(man.prdPad.getPrd_supdos());
			man.prd.setPrd_suptres(man.prdPad.getPrd_cod());
			man.prd.setPrd_supcua(0);

		} else if (man.gru.getGru_nivel() == 5) {

			man.prd.setPrd_supuno(man.prdPad.getPrd_supuno());
			man.prd.setPrd_supdos(man.prdPad.getPrd_supdos());
			man.prd.setPrd_suptres(man.prdPad.getPrd_suptres());
			man.prd.setPrd_supcua(man.prdPad.getPrd_cod());

		}

		man.prd.setPrd_nivel(man.gru.getGru_nivel());

	}

	private void obtenerDatoDlgModelo() {

		validarModeloIde();
		man.gruMod.setObjeto(man.obtenerGrupo("Modelo"));
		// man.mod.setPrd_cod(0);//Autonumerico
		man.mod.setGru_cod(man.gruMod.getGru_cod());// obtiene el codigo de grupo
		man.mod.setPrd_ide(txtModIde.getText());
		man.mod.setPrd_tip(txtModTip.getText());
		man.mod.setPrd_mat(txtModMat.getText());
		man.mod.setPrd_gau(txtModGau.getText());
		man.mod.setPrd_gro(txtModGro.getText());
		man.mod.setPrd_col(txtPrdCol.getText());
		man.mod.setPrd_des(txtModDes.getText());
		man.mod.setPrd_stkact(0);
		man.mod.setPrd_supuno(0);
		man.mod.setPrd_supdos(0);
		man.mod.setPrd_suptres(0);
		man.mod.setPrd_supcua(0);
		man.mod.setPrd_nivel(man.gruMod.getGru_nivel());

		// if (man.mod.getPrd_cod() == 0) {
		// generarImagenIdentificacionModelo();
		// }

		// man.mod.setPrd_foto(funUsu.leerBlobmodsSql.getBytes("mod_foto")));
		// man.mod.setPrd_bar(funUsu.leerBlob(resSql.getBytes("mod_bar")));
		// man.mod.setPrd_ver(resSql.getInt("mod_ver"));

	}

	private void obtenerDatoDlgProducto() {

		validarProductoIde();
		man.gruPrd.setObjeto(man.obtenerGrupo("Producto"));
		// man.prd.setPrd_cod(0);//Autonumerico
		man.prd.setGru_cod(man.gruPrd.getGru_cod());// obtiene el codigo de grupo
		man.prd.setPrd_ide(txtPrdIde.getText());
		man.prd.setPrd_tip(man.mod.getPrd_tip());
		man.prd.setPrd_mat(man.mod.getPrd_mat());
		man.prd.setPrd_gro(man.mod.getPrd_gro());
		man.prd.setPrd_gau(man.mod.getPrd_gau());
		man.prd.setPrd_col(txtPrdCol.getText().trim());
		man.prd.setPrd_des(txtPrdDes.getText());
		man.prd.setPrd_stkact(0);
		man.prd.setPrd_supuno(man.mod.getPrd_cod());
		man.prd.setPrd_supdos(0);
		man.prd.setPrd_suptres(0);
		man.prd.setPrd_supcua(0);
		man.prd.setPrd_nivel(man.gruPrd.getGru_nivel());
		// man.prd.setPrd_foto(funUsu.leerBlob(resSql.getBytes("prd_foto")));
		// man.prd.setPrd_bar(funUsu.leerBlob(resSql.getBytes("prd_bar")));
		// man.prd.setPrd_ver(resSql.getInt("prd_ver"));

	}

	private void obtenerDatoDlgTalla() {

		validarTallaIde();

		man.gruTal.setObjeto(man.obtenerGrupo("Talla"));
		// man.tal.setPrd_cod(0);//Autonumerico
		man.tal.setGru_cod(man.gruTal.getGru_cod());// obtiene el codigo de grupo
		man.tal.setPrd_ide(txtTalIde.getText());
		man.tal.setPrd_tip(man.prd.getPrd_tip());
		man.tal.setPrd_mat(man.prd.getPrd_mat());
		man.tal.setPrd_gro(man.prd.getPrd_gro());
		man.tal.setPrd_gau(man.prd.getPrd_gau());
		man.tal.setPrd_col(man.prd.getPrd_col());
		man.tal.setPrd_des(txtTalDes.getText());
		man.tal.setPrd_stkact(0);
		man.tal.setPrd_supuno(man.mod.getPrd_cod());
		man.tal.setPrd_supdos(man.prd.getPrd_cod());
		man.tal.setPrd_suptres(0);
		man.tal.setPrd_supcua(0);
		man.tal.setPrd_nivel(man.gruTal.getGru_nivel());
		// man.tal.setPrd_foto(funUsu.leerBlob(resSql.getBytes("prd_foto")));
		// man.tal.setPrd_bar(funUsu.leerBlob(resSql.getBytes("prd_bar")));
		// man.tal.setPrd_ver(resSql.getInt("prd_ver"));

	}

	public void obtenerDatoDlgPedido() {

		// man.ped.setPed_cod(0);//Autonumerico
		man.ped.setCli_cod(man.cli.getCli_cod());
		man.ped.setPed_nomdoc(etiPedNomDoc.getText());
		man.ped.setPed_numdoc(txtPedNumDoc.getText());
		man.ped.setPed_nom(txtCliNom.getText());
		man.ped.setPed_dir(txtCliDir.getText());
		man.ped.setPed_docide(txtCliDoc.getText());
		man.ped.setPed_numide(txtCliNum.getText());
		man.ped.setPed_temp(txtPedTemp.getText());
		man.ped.setPed_imptot(Double.parseDouble(txtPedImpTot.getText()));
		man.ped.setPed_est(txtPedEst.getText());
		// man.ped.setPed_ver(0);
	}// Fin de obtenerDatoDlgVenta()

	public void obtenerDatoDlgCliente() {

		man.cli.setCli_ide(txtCliIde.getText());
		man.cli.setCli_doc(txtCliDoc.getText());
		man.cli.setCli_num(txtCliNum.getText());
		man.cli.setCli_nom(txtCliNom.getText());
		man.cli.setCli_dir(txtCliDir.getText());
		// man.cli.setCli_tel("");
		// man.cli.setCli_email(resSql.getString("cli_email"));
		// man.cli.setCli_web(resSql.getString("cli_web"));
		// man.cli.setCli_act(resSql.getString("cli_act"));
		// man.cli.setCli_ver(1);

	}// Fin de obtenerDatoDlgVenta()

	public int registrarModelo() {

		registrarClase("Tipo", txtModTip.getText());
		registrarClase("Material", txtModMat.getText());
		registrarClase("Gauge", txtModGau.getText());
		registrarClase("Grosor", txtModGro.getText());
		registrarClase("Color", txtPrdCol.getText());

		obtenerDatoDlgModelo();
		int bookMark = man.actualizarProducto(man.mod);
		if (bookMark == 0) {
			bookMark = man.agregarProducto(man.mod);
		}
		return bookMark;
	}

	private void registrarClase(String parGruNom, String parClaNom) {

		if (!man.existeClase(parClaNom)) {

			man.gru.setObjeto(man.obtenerGrupo(parGruNom));
			man.cla.setGru_cod(man.gru.getGru_cod());
			man.cla.setCla_nom(parClaNom);
			man.cla.setCla_des("");
			man.cla.setCla_ver(1);

			man.agregarClase();
		}

	}

	public int registrarProducto() {

		obtenerDatoDlgProducto();
		int bookMark = man.actualizarProducto(man.prd);
		if (bookMark == 0) {
			bookMark = man.agregarProducto(man.prd);
		}
		return bookMark;
	}

	public int registrarTalla() {

		obtenerDatoDlgTalla();
		int bookMark = man.actualizarProducto(man.tal);
		if (bookMark == 0) {
			bookMark = man.agregarProducto(man.tal);
		}
		return bookMark;
	}

	public int registrarDetPedido(Producto parPrd, Grupo parGru) {

		obtenerDatoDlgDetPedido(parPrd, parGru);
		int bookMark = man.actualizarDetPedido(parPrd, parGru);
		if (bookMark == 0) {
			bookMark = man.agregarDetPedido(parPrd, parGru);
		}

		return bookMark;

	}

	private void panModFotoMouseReleased(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_panArtFotoMouseReleased
		if (evt.getButton() == MouseEvent.BUTTON3) {
			if (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO)) {

				popMenModFoto.show(evt.getComponent(), evt.getX(), evt.getY());

			}// Fin de if (estDlgArt.equals(estadoVentana.AGREGANDO) || estDlgArt.equals(estadoVentana.MODIFICANDO))
		}// Fin de if (evt.getButton() == MouseEvent.BUTTON3)

	}// GEN-LAST:event_panArtFotoMouseReleased

	private void cmdPedAgregarActionPerformed(ActionEvent arg0) {
		if (cmdPedAgregar.getText().equals("Agregar")) {
			bookMarkNomDoc = seleccionarDocumento();
			if (!bookMarkNomDoc.equals("")) {

				estDlgPed = EstadoPagina.AGREGANDO;
				estDlgDetPed = EstadoPagina.SUSPENDIDO;

				bookMarkPedCod = man.ped.getPed_cod();
				bookMarkDetPedCod = man.detPed.getDetped_cod();
				bookMarkModCod = man.mod.getPrd_cod();
				bookMarkPrdCod = man.prd.getPrd_cod();
				bookMarkTalCod = man.tal.getPrd_cod();

				limpiarFormulario();
				refrescarFormulario();

				man.nuevoNumeroDocumento(bookMarkNomDoc);
				man.nuevoRegistroPedido();
				llenarDlgPedido();

				txtEditPedFecReg.requestFocus();

			}// Fin de if(!bookMarkNomDoc.equals(""))
		}// Fin de if(accion.equals(accionVentana.INICIAR))
		else if (cmdPedAgregar.getText().equals("Guardar")) {
			int resMsg = JOptionPane.showConfirmDialog(this,
					"���Desea guardar el pedido para produccion!!!",
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (resMsg == JOptionPane.YES_OPTION) {
				if (validarDlgPedido()) {

					obtenerDatoDlgCliente();
					obtenerDatoDlgPedido();

					switch (estDlgPed) {

					case AGREGANDO:

						bookMarkPedCod = agregarPedidoCliente();
						break;

					case MODIFICANDO:

						bookMarkPedCod = actualizarPedidoCliente();
						break;

					default:
						break;

					}// Fin de switch (estDlgSuc)

					if (bookMarkPedCod != 0) {

//						man.transactionDelegate.commit();
						man.ped.setObjeto(man.obtenerPedido(bookMarkPedCod));

						acceso = AccesoPagina.ACTUALIZAR;
						estDlgPed = EstadoPagina.VISUALIZANDO;
						iniciarFormulario();

					}// Fin de if(bookMarkDevCod != 0)
					else {

//						man.transactionDelegate.rollback();

						JOptionPane.showConfirmDialog(this,
								"���Error de transaction!!!\n" + this.getClass().getName(),
								man.testProp.getPropiedad("TituloSistema"),
								JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

					}
				}// fin de if (validarDlgContacto())
			}// Fin de if (resMsg == JOptionPane.YES_OPTION)
		}// Fin de if(accion.equals(accionVentana.ACTUALIZAR)) }
	}

	private void cmdDetPrdAgregarActionPerformed(ActionEvent arg0) {
		if (cmdDetPedAgregar.getText().equals("Agregar")) {
			if (validarDlgPedido()) {
				if (man.ped.getPed_est().equals(EstadoDocumento.PENDIENTE.getVal())) {

					estDlgPed = EstadoPagina.SUSPENDIDO;
					estDlgDetPed = EstadoPagina.AGREGANDO;

					bookMarkPedCod = man.ped.getPed_cod();
					bookMarkDetPedCod = man.detPed.getDetped_cod();
					bookMarkModCod = man.mod.getPrd_cod();
					bookMarkPrdCod = man.prd.getPrd_cod();
					bookMarkTalCod = man.tal.getPrd_cod();

					refrescarFormulario();
					if (man.mod.getPrd_cod() == 0) {
						txtModIde.setText(man.ped.getPed_temp() + "-");
					}
					txtPrdDes.requestFocus();

				} else {
					JOptionPane.showConfirmDialog(this,
							"���EL PEDIDO DEBE DE ESTAR EN MODO PENDIENTE PARA MODIFICARLO!!!"
									+ "\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}// Fin de if(accion.equals(accionVentana.INICIAR))
		else if (cmdDetPedAgregar.getText().equals("Guardar")) {
			int resMsg = JOptionPane.showConfirmDialog(this,
					"���Desea guardar el detalle de pedido!!!",
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (resMsg == JOptionPane.YES_OPTION) {
				if (validarDlgDetPedido()) {

					switch (estDlgDetPed) {

					case AGREGANDO:
					case MODIFICANDO:

						bookMarkModCod = registrarModelo();
						man.mod.setObjeto(man.obtenerProducto(bookMarkModCod));
						bookMarkDetPedCod = registrarDetPedido(man.mod, man.gruMod);

						bookMarkPrdCod = registrarProducto();
						man.prd.setObjeto(man.obtenerProducto(bookMarkPrdCod));
						bookMarkDetPedCod = registrarDetPedido(man.prd, man.gruPrd);

						bookMarkTalCod = registrarTalla();
						man.tal.setObjeto(man.obtenerProducto(bookMarkTalCod));
						bookMarkDetPedCod = registrarDetPedido(man.tal, man.gruTal);
						break;

					}// Fin de switch (estDlgSuc)

					if (bookMarkModCod != 0 && bookMarkPrdCod != 0 && bookMarkTalCod != 0 && bookMarkDetPedCod != 0) {

						man.detPed.setObjeto(man.obtenerDetPedido(bookMarkDetPedCod));

						man.actualizarTotalDetPedido(man.ped.getPed_cod(), man.tal.getPrd_nivel(), man.prd.getPrd_cod());
						man.actualizarTotalDetPedido(man.ped.getPed_cod(), man.prd.getPrd_nivel(), man.mod.getPrd_cod());

//						man.transactionDelegate.commit();

						acceso = AccesoPagina.ACTUALIZAR;
						estDlgPed = EstadoPagina.VISUALIZANDO;
						iniciarFormulario();
						cmdDetPedAgregar.requestFocus();

					}// Fin de if(bookMarkDevCod != 0)
					else {

//						man.transactionDelegate.rollback();

						JOptionPane.showConfirmDialog(this,
								"���Error de transaction!!!\n" + this.getClass().getName(),
								man.testProp.getPropiedad("TituloSistema"),
								JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

					}
				}// fin de if (validarDlgContacto())
			}// Fin de if (resMsg == JOptionPane.YES_OPTION)
		}// Fin de if(accion.equals(accionVentana.ACTUALIZAR)) }
	}

	private void cmdDetPrdEliminarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdAreEliminarActionPerformed

		if (man.ped.getPed_est().equals(EstadoDocumento.PENDIENTE.getVal())) {
			if (man.tal.getPrd_cod() != 0) {
				int resMsg = JOptionPane.showConfirmDialog(this,
						"Desea Eliminar el Registro",
						"Sistema de Administracion",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (resMsg == JOptionPane.YES_OPTION) {

					boolean exito = man.eliminarDetPedido(man.detPed.getDetped_cod());

					if (exito) {

//						man.transactionDelegate.commit();

						man.actualizarTotalDetPedido(man.ped.getPed_cod(), man.tal.getPrd_nivel(), man.prd.getPrd_cod());
						man.actualizarTotalDetPedido(man.ped.getPed_cod(), man.prd.getPrd_nivel(), man.mod.getPrd_cod());

						acceso = AccesoPagina.ACTUALIZAR;
						iniciarFormulario();

					}// Fin de eliminarDocuemtnoKardex
					else {

//						man.transactionDelegate.rollback();

						JOptionPane.showConfirmDialog(this,
								"���SE CANCELO LA TRANSACCION EN LA ELIMINACION DE FICHAS DE CONTROL!!!"
										+ "\n" + this.getClass().getName(),
								man.testProp.getPropiedad("TituloSistema"),
								JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

					}

				}// Fin de if(resMsg == JOptionPane.YES_OPTION)
			} else {
				JOptionPane.showConfirmDialog(this,
						"���DEBE SELECCIONAR UNA TALLA PARA ELIMINAR EL REGISTRO!!!"
								+ "\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			}
		} else {
			JOptionPane.showConfirmDialog(this,
					"���EL PEDIDO DEBE ESTAR EN ESTADO DE PENDIENTE PARA ELIMINAR EL REGISTRO!!!"
							+ "\n" + this.getClass().getName(),
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

		}
	}// GEN-LAST:event_cmdAreEliminarActionPerformed

	private void cmdPedModificarActionPerformed(ActionEvent arg0) {
		if (cmdPedModificar.getText().equals("Modificar")) {
			if (man.ped.getPed_est().equals(EstadoDocumento.PENDIENTE.getVal())) {

				estDlgPed = EstadoPagina.MODIFICANDO;
				estDlgDetPed = EstadoPagina.SUSPENDIDO;

				bookMarkPedCod = man.ped.getPed_cod();
				bookMarkDetPedCod = man.detPed.getDetped_cod();
				bookMarkModCod = man.mod.getPrd_cod();
				bookMarkPrdCod = man.prd.getPrd_cod();
				bookMarkTalCod = man.tal.getPrd_cod();

				refrescarFormulario();
				txtEditPedFecReg.requestFocus();

			} else {
				JOptionPane.showConfirmDialog(this,
						"���EL PEDIDO DEBE DE ESTAR EN MODO PENDIENTE PARA MODIFICARLO!!!"
								+ "\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			}

		} else if (cmdPedModificar.getText().equals("Cancelar")) {

			acceso = AccesoPagina.ACTUALIZAR;
			estDlgPed = EstadoPagina.VISUALIZANDO;
			iniciarFormulario();

		}// FIN DE EVALUAR QUE VA HA HACER EL BOTON
	}

	private void cmdDetPrdModificarActionPerformed(ActionEvent e) {
		if (cmdDetPedModificar.getText().equals("Modificar")) {
			if (man.mod.getPrd_cod() > 0) {
				if (man.prd.getPrd_cod() > 0) {
					if (man.tal.getPrd_cod() > 0) {
						if (man.ped.getPed_est().equals(EstadoDocumento.PENDIENTE.getVal())) {

							estDlgPed = EstadoPagina.SUSPENDIDO;
							estDlgDetPed = EstadoPagina.MODIFICANDO;

							bookMarkPedCod = man.ped.getPed_cod();
							bookMarkDetPedCod = man.detPed.getDetped_cod();
							bookMarkModCod = man.mod.getPrd_cod();
							bookMarkPrdCod = man.prd.getPrd_cod();
							bookMarkTalCod = man.tal.getPrd_cod();

							refrescarFormulario();
							txtTalDes.requestFocus();

						} else {
							JOptionPane.showConfirmDialog(this,
									"���EL PEDIDO DEBE DE ESTAR EN MODO PENDIENTE PARA MODIFICARLO!!!"
											+ "\n" + this.getClass().getName(),
									man.testProp.getPropiedad("TituloSistema"),
									JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
						}

					} else {
						JOptionPane.showConfirmDialog(this,
								"���Seleccione una talla!!!\n" + this.getClass().getName(),
								man.testProp.getPropiedad("TituloSistema"),
								JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
					}
				} else {
					JOptionPane.showConfirmDialog(this,
							"���Seleccione un producto!!!\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
				}
			} else {
				JOptionPane.showConfirmDialog(this,
						"���Seleccione un modelo!!!\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			}
		} else if (cmdDetPedModificar.getText().equals("Cancelar")) {

			acceso = AccesoPagina.ACTUALIZAR;
			estDlgPed = EstadoPagina.VISUALIZANDO;
			iniciarFormulario();

		}// FIN DE EVALUAR QUE VA HA HACER EL BOTON
	}// Fin de private void cmdDetPrdModificarActionPerformed(ActionEvent e)

	private void cmdCerrarActionPerformed(ActionEvent arg0) {

		cerrar();

	}

	/**
	 * 
	 * 
	 * Configuracion de la lista desplegable de cliente
	 * 
	 * 
	 */
	private void txtPerNomKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtCliNom.getText().trim().length() == 0) {

			scrLisCliNom.setVisible(false);
			lisCliNom.setVisible(false);
			busLisCliNom = true;

		} else if (txtCliNom.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisCliNom = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisCliNom.size() > 0) {

					scrLisCliNom.setVisible(true);
					lisCliNom.setVisible(true);
					lisCliNom.requestFocus();
					lisCliNom.setSelectedIndex(0);
					busLisCliNom = true;

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisPerNomMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisCliNom == true) && (estDlgPed.equals(EstadoPagina.AGREGANDO) || estDlgPed.equals(EstadoPagina.MODIFICANDO))) {

				modLisCliNom.removeAllElements();
				varLisCliNom = man.obtenerListaNombreCliente(txtCliNom.getText());
				for (int iteLis = 0; iteLis < varLisCliNom.size(); iteLis++) {
					modLisCliNom.addElement(varLisCliNom.get(iteLis));
				}

				varLisCliNom = man.obtenerListaNombreCliente(txtCliNom.getText().toUpperCase());
				for (String iteLisCliNom : varLisCliNom) {
					if (!modLisCliNom.contains(iteLisCliNom)) {
						modLisCliNom.addElement(iteLisCliNom);
					}
				}
				if (varLisCliNom.size() > 0) {

					scrLisCliNom.setVisible(true);
					lisCliNom.setVisible(true);
					busLisCliNom = true;

				} else {

					scrLisCliNom.setVisible(false);
					lisCliNom.setVisible(false);
					busLisCliNom = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisPerNomMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisCliNom.setVisible(false);
		lisCliNom.setVisible(false);
		if (lisCliNom.getSelectedIndex() > -1) {
			txtCliNom.setText((String) lisCliNom.getSelectedValue());
		}

		ClienteDAO cliDAO = (ClienteDAO) ContenedorTextil.getComponent("ClienteDAO");
		man.cli.setObjeto(cliDAO.leerPersonaNombre(txtCliNom.getText().trim()));

		llenarDlgCliente();

	}// GEN-LAST:event_lisPerNomMousePressed

	private void lisPerNomKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			lisCliNom.setVisible(false);
			txtCliNom.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisPerNomMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	/*
	 * 
	 * fin de configuracionde lista de cliente
	 */

	private void lisModMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisModIde.setVisible(false);
		lisModIde.setVisible(false);
		txtModIde.setText((String) lisModIde.getSelectedValue());

		ClaseDAO claDAO = (ClaseDAO) ContenedorTextil.getComponent("ClaseDAO");
		man.cla.setObjeto(claDAO.obtenerClase(txtModIde.getText().trim()));

	}// GEN-LAST:event_lisPerNomMousePressed

	private void txtModDesKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			txtModTip.requestFocus();
		}
	}

	private void txtPrdIdeKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			txtPrdDes.requestFocus();
		}
	}

	private void txtPrdDesKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			txtTalDes.requestFocus();
		}
	}

	private void txtTalDesKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			txtTalCan.requestFocus();
		}
	}

	/**
	 * 
	 * @param Configuracion
	 *            de modelo de identificador
	 */
	private void lisModIdeKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			scrLisModIde.setVisible(false);
			lisModIde.setVisible(false);
			txtModIde.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisModIdeMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	private void txtModIdeKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtModIde.getText().trim().length() == 0) {

			scrLisModIde.setVisible(false);
			lisModIde.setVisible(false);
			busLisModIde = true;

		} else if (txtModIde.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisModIde = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisModIde.size() > 0) {

					scrLisModIde.setVisible(true);
					lisModIde.setVisible(true);
					busLisModIde = false;
					lisModIde.requestFocus();
					lisModIde.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisModIdeMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisModIde == true) && (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO))) {

				modLisModIde.removeAllElements();
				varLisModIde = man.obtenerListaModeloIde(txtModIde.getText());
				for (int iteLis = 0; iteLis < varLisModIde.size(); iteLis++) {
					modLisModIde.addElement(varLisModIde.get(iteLis));
				}

				varLisModIde = man.obtenerListaModeloIde(txtModIde.getText());
				for (int iteLis = 0; iteLis < varLisModIde.size(); iteLis++) {
					if (!modLisModIde.contains(varLisModIde.get(iteLis))) {
						modLisModIde.addElement(varLisModIde.get(iteLis));
					}
				}
				if (varLisModIde.size() > 0) {

					scrLisModIde.setVisible(true);
					lisModIde.setVisible(true);
					busLisModIde = true;

				} else {

					scrLisModIde.setVisible(false);
					lisModIde.setVisible(false);
					busLisModIde = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
		txtModDes.setText(txtModIde.getText());
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisModIdeMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisModIde.setVisible(false);
		lisModIde.setVisible(false);
		if (lisModIde.getSelectedIndex() > -1) {

			txtModIde.setText((String) lisModIde.getSelectedValue());

		}
		validarModeloIde();
		txtModDes.requestFocus();

	}// GEN-LAST:event_lisPerNomMousePressed

	public boolean validarModeloIde() {

		if (txtModIde.getText().length() > 1) {
			if (validarIdentificador(txtModIde.getText())) {
				if (man.existeProducto(0, txtModIde.getText())) {

					man.mod.setObjeto(man.obtenerProductoIde(txtModIde.getText()));
					llenarDlgModelo();

				} else {
					man.mod.limpiarInstancia();
					generarIdentificacionModelo(txtModIde.getText().trim());

				}
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * Configuracion del Tipo de Modelo
	 * 
	 * @param evt
	 */
	private void lisModTipKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			scrLisModTip.setVisible(false);
			txtModTip.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisModTipMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	private void txtModTipKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtModTip.getText().trim().length() == 0) {

			scrLisModTip.setVisible(false);
			lisModTip.setVisible(false);
			busLisModTip = true;

		} else if (txtModTip.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisModTip = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisModTip.size() > 0) {

					scrLisModTip.setVisible(true);
					lisModTip.setVisible(true);
					busLisModTip = false;
					lisModTip.requestFocus();
					lisModTip.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisModTipMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisModTip == true) && (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO))) {

				modLisModTip.removeAllElements();
				varLisModTip = man.obtenerListaClase("Tipo", txtModTip.getText());
				for (int iteLis = 0; iteLis < varLisModTip.size(); iteLis++) {
					String[] record = varLisModTip.get(iteLis).split("#");
					modLisModTip.addElement(record[2]);
				}

				varLisModTip = man.obtenerListaClase("Tipo", txtModTip.getText());
				for (int iteLis = 0; iteLis < varLisModTip.size(); iteLis++) {
					String[] record = varLisModTip.get(iteLis).split("#");
					if (!modLisModTip.contains(record[2])) {
						modLisModTip.addElement(record[2]);
					}
				}
				if (varLisModTip.size() > 0) {

					scrLisModTip.setVisible(true);
					lisModTip.setVisible(true);
					busLisModTip = true;

				} else {

					scrLisModTip.setVisible(false);
					lisModTip.setVisible(false);
					busLisModTip = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisModTipMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisModTip.setVisible(false);
		lisModTip.setVisible(false);
		if (lisModTip.getSelectedIndex() > -1) {

			txtModTip.setText((String) lisModTip.getSelectedValue());

		}
		txtModMat.requestFocus();

	}// GEN-LAST:event_lisPerNomMousePressed

	/*
	 * 
	 * 
	 * 
	 * CONFIGURACION DE PRODUCTO MATERIAL
	 */
	private void lisModMatKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			scrLisModMat.setVisible(false);
			txtModMat.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisModMatMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	private void txtModMatKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtModMat.getText().trim().length() == 0) {

			scrLisModMat.setVisible(false);
			lisModMat.setVisible(false);
			busLisModMat = true;

		} else if (txtModMat.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisModMat = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisModMat.size() > 0) {

					scrLisModMat.setVisible(true);
					lisModMat.setVisible(true);
					busLisModMat = false;
					lisModMat.requestFocus();
					lisModMat.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisModMatMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisModMat == true) && (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO))) {

				modLisModMat.removeAllElements();
				varLisModMat = man.obtenerListaClase("Material", txtModMat.getText());
				for (int iteLis = 0; iteLis < varLisModMat.size(); iteLis++) {
					String[] record = varLisModMat.get(iteLis).split("#");
					modLisModMat.addElement(record[2]);
				}

				varLisModMat = man.obtenerListaClase("Material", txtModMat.getText());
				for (int iteLis = 0; iteLis < varLisModMat.size(); iteLis++) {
					String[] record = varLisModMat.get(iteLis).split("#");
					if (!modLisModMat.contains(record[2])) {
						modLisModMat.addElement(record[2]);
					}
				}

				if (varLisModMat.size() > 0) {

					scrLisModMat.setVisible(true);
					lisModMat.setVisible(true);
					busLisModMat = true;

				} else {

					scrLisModMat.setVisible(false);
					lisModMat.setVisible(false);
					busLisModMat = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisModMatMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisModMat.setVisible(false);
		lisModMat.setVisible(false);
		if (lisModMat.getSelectedIndex() > -1) {
			txtModMat.setText((String) lisModMat.getSelectedValue());
		}
		txtModGau.requestFocus();
	}// GEN-LAST:event_lisPerNomMousePressed

	/*
	 * 
	 * 
	 * 
	 * CONFIGURACION DE PRODUCTO GAUGE
	 */
	private void lisModGauKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			scrLisModGau.setVisible(false);
			txtModGau.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisModGauMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	private void txtModGauKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtModGau.getText().trim().length() == 0) {

			scrLisModGau.setVisible(false);
			lisModGau.setVisible(false);
			busLisModGau = true;

		} else if (txtModGau.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisModGau = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisModGau.size() > 0) {

					scrLisModGau.setVisible(true);
					lisModGau.setVisible(true);
					busLisModGau = false;
					lisModGau.requestFocus();
					lisModGau.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisModGauMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisModGau == true) && (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO))) {

				modLisModGau.removeAllElements();
				varLisModGau = man.obtenerListaClase("Gauge", txtModGau.getText());
				for (int iteLis = 0; iteLis < varLisModGau.size(); iteLis++) {
					String[] record = varLisModGau.get(iteLis).split("#");
					modLisModGau.addElement(record[2]);
				}

				varLisModGau = man.obtenerListaClase("Gauge", txtModGau.getText());
				for (int iteLis = 0; iteLis < varLisModGau.size(); iteLis++) {
					String[] record = varLisModGau.get(iteLis).split("#");
					if (!modLisModGau.contains(record[2])) {
						modLisModGau.addElement(record[2]);
					}
				}
				if (varLisModGau.size() > 0) {

					scrLisModGau.setVisible(true);
					lisModGau.setVisible(true);
					busLisModGau = true;

				} else {

					scrLisModGau.setVisible(false);
					lisModGau.setVisible(false);
					busLisModGau = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisModGauMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisModGau.setVisible(false);
		lisModGau.setVisible(false);
		if (lisModGau.getSelectedIndex() > -1) {
			txtModGau.setText((String) lisModGau.getSelectedValue());
		}

		txtModGro.requestFocus();

	}// GEN-LAST:event_lisPerNomMousePressed

	/*
	 * 
	 * 
	 * 
	 * CONFIGURACION DE GROSOR
	 */

	private void lisModGroKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			scrLisModGro.setVisible(false);
			txtModGro.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisModGroMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	private void txtModGroKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtModGro.getText().trim().length() == 0) {

			scrLisModGro.setVisible(false);
			lisModGro.setVisible(false);
			busLisModGro = true;

		} else if (txtModGro.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisModGro = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisModGro.size() > 0) {

					scrLisModGro.setVisible(true);
					lisModGro.setVisible(true);
					busLisModGro = false;
					lisModGro.requestFocus();
					lisModGro.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisModGroMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisModGro == true) && (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO))) {

				modLisModGro.removeAllElements();
				varLisModGro = man.obtenerListaClase("Grosor", txtModGro.getText());
				for (int iteLis = 0; iteLis < varLisModGro.size(); iteLis++) {
					String[] record = varLisModGro.get(iteLis).split("#");
					modLisModGro.addElement(record[2]);
				}

				varLisModGro = man.obtenerListaClase("Grosor", txtModGro.getText());
				for (int iteLis = 0; iteLis < varLisModGro.size(); iteLis++) {
					String[] record = varLisModGro.get(iteLis).split("#");
					if (!modLisModGro.contains(record[2])) {
						modLisModGro.addElement(record[2]);
					}
				}
				if (varLisModGro.size() > 0) {

					scrLisModGro.setVisible(true);
					lisModGro.setVisible(true);
					busLisModGro = true;

				} else {

					scrLisModGro.setVisible(false);
					lisModGro.setVisible(false);
					busLisModGro = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisModGroMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisModGro.setVisible(false);
		lisModGro.setVisible(false);
		if (lisModGro.getSelectedIndex() > -1) {
			txtModGro.setText((String) lisModGro.getSelectedValue());
		}
		txtPrdCol.requestFocus();
	}// GEN-LAST:event_lisPerNomMousePressed

	/**
	 * Configuracio del color del producto
	 * 
	 * @param evt
	 */
	private void lisPrdColKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			scrLisPrdCol.setVisible(false);
			txtPrdCol.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisPrdColMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	private void txtPrdColKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtPrdCol.getText().trim().length() == 0) {

			scrLisPrdCol.setVisible(false);
			lisPrdCol.setVisible(false);
			busLisPrdCol = true;

		} else if (txtPrdCol.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisPrdCol = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisPrdCol.size() > 0) {

					scrLisPrdCol.setVisible(true);
					lisPrdCol.setVisible(true);
					busLisPrdCol = false;
					lisPrdCol.requestFocus();
					lisPrdCol.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisPrdColMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisPrdCol == true) && (estDlgDetPed.equals(EstadoPagina.AGREGANDO) || estDlgDetPed.equals(EstadoPagina.MODIFICANDO))) {

				modLisPrdCol.removeAllElements();
				varLisPrdCol = man.obtenerListaClase("Color", txtPrdCol.getText());
				for (int iteLis = 0; iteLis < varLisPrdCol.size(); iteLis++) {
					String[] record = varLisPrdCol.get(iteLis).split("#");
					modLisPrdCol.addElement(record[2]);
				}

				varLisPrdCol = man.obtenerListaClase("Color", txtPrdCol.getText());
				for (int iteLis = 0; iteLis < varLisPrdCol.size(); iteLis++) {
					String[] record = varLisPrdCol.get(iteLis).split("#");
					if (!modLisPrdCol.contains(record[2])) {
						modLisPrdCol.addElement(record[2]);
					}
				}
				if (varLisPrdCol.size() > 0) {

					scrLisPrdCol.setVisible(true);
					lisPrdCol.setVisible(true);
					busLisPrdCol = true;

				} else {

					scrLisPrdCol.setVisible(false);
					lisPrdCol.setVisible(false);
					busLisPrdCol = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisPrdColMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisPrdCol.setVisible(false);
		lisPrdCol.setVisible(false);
		if (lisPrdCol.getSelectedIndex() > -1) {

			txtPrdCol.setText((String) lisPrdCol.getSelectedValue());

		}
		if (validarProductoIde()) {
			txtPrdDes.requestFocus();
		}
	}// GEN-LAST:event_lisPerNomMousePressed

	public boolean validarProductoIde() {
		if (txtPrdCol.getText().length() > 1) {
			if (validarIdentificador(txtPrdCol.getText())) {
				txtPrdIde.setText(txtModIde.getText() + "-" + txtCliIde.getText() + "-" + txtPrdCol.getText().substring(0, txtPrdCol.getText().indexOf("-")));
				if (man.existeProducto(0, txtPrdIde.getText())) {
					man.prd.setObjeto(man.obtenerProductoIde(txtPrdIde.getText()));
					txtPrdDes.setText(man.prd.getPrd_des());
				} else {
					man.prd.limpiarInstancia();
					generarIdentificacionProducto(txtPrdIde.getText());
					txtPrdDes.setText(txtModMat.getText() + " " + txtPrdCol.getText().substring(txtPrdCol.getText().indexOf("-"), txtPrdCol.getText().length()));
				}
				return true;
			}
		}
		return false;
	}

	public boolean validarTallaIde() {
		if (txtTalDes.getText().length() > 1) {
			txtTalIde.setText(txtPrdIde.getText() + "-" + txtTalDes.getText());
			if (man.existeProducto(0, txtTalIde.getText())) {
				man.tal.setObjeto(man.obtenerProductoIde(txtTalIde.getText()));
				txtTalDes.setText(man.tal.getPrd_des());
			} else {

				man.tal.limpiarInstancia();
				generarIdentificacionTalla(txtTalIde.getText());
				// txtTalDes.setText(txtModMat.getText() + " " + txtPrdCol.getText().substring(0, txtPrdCol.getText().indexOf("-")));
			}
			return true;
		}
		return false;
	}

	/*
	 * 
	 * 
	 * 
	 * FIN DE EVENTO DE CONTROL DE TEXTO DESPLEGABLE
	 */
	private void cmdPedBuscarActionPerformed(ActionEvent arg0) {

		bookMarkNomDoc = seleccionarDocumento();
		if (!bookMarkNomDoc.equals("")) {

			estDlgPed = EstadoPagina.BUSCANDO;
			estDlgDetPed = EstadoPagina.SUSPENDIDO;

			bookMarkPedCod = man.ped.getPed_cod();
			bookMarkDetPedCod = man.detPed.getDetped_cod();
			bookMarkModCod = man.mod.getPrd_cod();
			bookMarkPrdCod = man.prd.getPrd_cod();
			bookMarkTalCod = man.tal.getPrd_cod();

			refrescarFormulario();
			etiPedNomDoc.setText(bookMarkNomDoc);
			txtPedNumDoc.requestFocus();

		}
	}

	private void txtPedNumDocKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER && estDlgPed.equals(EstadoPagina.BUSCANDO)) {

			man.ped.setObjeto(man.obtenerPedido(etiPedNomDoc.getText(), txtPedNumDoc.getText()));
			bookMarkPedCod = man.ped.getPed_cod();
			cmdPedModificarActionPerformed(null);

		}// fin de if(estDlgPed.equals(estadoVentana.BUSCANDO))
	}// Fin de private void txtPedNumDocKeyReleased(KeyEvent arg0)

	private void tabDetPrdKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_UP
				|| evt.getKeyCode() == KeyEvent.VK_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_PAGE_UP
				|| evt.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_ENTER
				|| evt.getKeyCode() == KeyEvent.VK_F2
				|| evt.getKeyCode() == KeyEvent.VK_TAB) {

			tabDetPrdMousePressed(null);

		}// Fin de if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN)
	}

	private void tabDetPrdMousePressed(MouseEvent e) {
		if (tabDetPrd.getSelectedRow() > -1
				&& (estDlgDetPed.equals(EstadoPagina.VISUALIZANDO)
				|| estDlgDetPed.equals(EstadoPagina.BUSCANDO))) {

			seleccionarRegistroProducto();

		}// FIN DE VERIFICAR QUE EL REGISTRO SELECCIONADO ESTE EN LA TABLA
	}

	private void tabDetTalKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_UP
				|| evt.getKeyCode() == KeyEvent.VK_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_PAGE_UP
				|| evt.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_ENTER
				|| evt.getKeyCode() == KeyEvent.VK_F2
				|| evt.getKeyCode() == KeyEvent.VK_TAB) {

			tabDetTalMousePressed(null);

		}// Fin de if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN)
	}

	private void tabDetTalMousePressed(MouseEvent e) {
		if (tabDetTal.getSelectedRow() > -1
				&& (estDlgDetPed.equals(EstadoPagina.VISUALIZANDO)
				|| estDlgDetPed.equals(EstadoPagina.BUSCANDO))) {

			seleccionarRegistroTalla();

		}// FIN DE VERIFICAR QUE EL REGISTRO SELECCIONADO ESTE EN LA TABLA
	}

	public void cmdFichaControlActionPerformed(ActionEvent evt) {

		if (man.ped.getPed_est().equals(EstadoDocumento.PENDIENTE.getVal())) {
			int resMsg = JOptionPane.showConfirmDialog(this,
					"�Desea Generar Fichas de Control?",
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (resMsg == 0) {

				if (man.existeRegistroZona("AREA")) {
					if (man.existeRegistroZona("SUBAREA")) {

						man.agregarFichasPedido(man.ped.getPed_cod(), 3);
//						man.transactionDelegate.commit();
						acceso = AccesoPagina.ACTUALIZAR;
						estDlgPed = EstadoPagina.VISUALIZANDO;
						iniciarFormulario();

					} else {
						JOptionPane.showConfirmDialog(this,
								"���NO EXISTE SUBAREAS REGISTRADAS!!!\n" + this.getClass().getName(),
								man.testProp.getPropiedad("TituloSistema"),
								JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

					}
				} else {
					JOptionPane.showConfirmDialog(this,
							"���NO EXISTE AREAS REGISTRADAS!!!\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

				}
			}// Fin de verificar que se desea cerrar el formulario
		} else {

			this.setVisible(false);
			DlgFicha.estDlgFic = EstadoPagina.BUSCANDO;
			man.abrirPantalla("DlgFicha", "DlgPedido", AccesoPagina.INICIAR);

		}
	}

	public void cmdPedImprimirActionPerformed(ActionEvent evt) {

		enviarImpresora("PEDIDO");
	}

	private void cmdDetPedImprimirActionPerformed(ActionEvent e) {

		enviarImpresora("PRODUCTO");

	}

	private void cmdPedFichasActionPerformed(ActionEvent e) {

		enviarImpresora("MODELO");

	}

	private String seleccionarDocumento() {

		String[] lisDoc = { TipoDocumento.NOTAPEDIDO.getVal(), TipoDocumento.NOTAPRUEBA.getVal() };

		String tipoDocumento = (String) JOptionPane.showInputDialog(this, "�Tipo de Documento?", man.testProp.getPropiedad("TituloSistema"), JOptionPane.QUESTION_MESSAGE, null, lisDoc, lisDoc[0]);
		if (tipoDocumento == null) {
			tipoDocumento = "";
		}

		return tipoDocumento;
	}// Fin de seleccionarDocumento()

	private void cmdPedEliminarActionPerformed(ActionEvent e) {
		int resMsg = JOptionPane.showConfirmDialog(this,
				"�DESEA ELIMINAR LA " + man.ped.getPed_nomdoc() + " : " + man.ped.getPed_numdoc() + "?",
				man.testProp.getPropiedad("TituloSistema"),
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (resMsg == JOptionPane.YES_OPTION) {

			man.eliminarListaFicha(man.ped.getPed_cod());
			man.eliminarListaDetPedido(man.ped.getPed_cod());
			boolean exito = man.eliminarPedido(man.ped.getPed_cod());

			if (exito) {

//				man.transactionDelegate.commit();
				acceso = AccesoPagina.INICIAR;
				iniciarFormulario();

			}// Fin de eliminarDocuemtnoKardex
			else {

//				man.transactionDelegate.rollback();
				JOptionPane.showConfirmDialog(this,
						"���SE CANCELO LA TRANSACCION EN LA ELIMINACION DE FICHAS DE CONTROL!!!"
								+ "\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			}

		}// Fin de if(resMsg == JOptionPane.YES_OPTION)
	}

	private void txtTalCanKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			EstadoPagina estDlgDetPedTmp = estDlgDetPed;

			cmdDetPrdAgregarActionPerformed(null);

			if (estDlgDetPedTmp.equals(EstadoPagina.AGREGANDO)) {

				cmdDetPrdAgregarActionPerformed(null);

			}
		}
	}
}// Fin de Clase Principal