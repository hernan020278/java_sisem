/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textil.main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.sql.Date;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class CeldaRenderArticulo extends DefaultTableCellRenderer {

    private Color azul = new Color(0, 51, 255);
    private Color verde = new Color(0, 153, 51);
    private Color rojo = new Color(204, 0, 0);
    private Color naranja = new Color(255, 153, 0);
    private Color marron = new Color(153, 102, 0);
    private Color crema = new Color(255, 255, 153);
    private Color amarillo = new Color(255, 255, 0);
    private Color negro = new Color(0, 0, 0);
    private Color blanco = new Color(255, 255, 255);
    private Color violeta = new Color(153,0,153);

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

        Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
        if (col == 2) {

            super.setHorizontalAlignment(SwingConstants.LEFT);

        } else if (col == 0 || col == 1 || col == 3 || col == 4 || col == 5) {

            super.setHorizontalAlignment(SwingConstants.CENTER);

        } else if (col == 6 || col == 7 || col == 8 || col == 9 || col == 10) {

            super.setHorizontalAlignment(SwingConstants.RIGHT);

        }

        cell.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 11));

        if (isSelected) {
            if (hasFocus) {

                cell.setBackground(marron);
                if (value instanceof Integer) {
                    Integer importe = (Integer) value;
                    if (importe <= 0) {

                        cell.setForeground(marron);

                    } else if (importe > 0) {

                        cell.setForeground(crema);

                    }

                } else if (value instanceof Double) {
                    Double importe = (Double) value;
                    if (importe <= 0.00) {

                        cell.setForeground(marron);

                    } else if (importe > 0) {

                        cell.setForeground(crema);
                    }
                } else {
                    cell.setForeground(crema);
                }

            } else {

                cell.setBackground(crema);
                if (value instanceof Integer) {
                    Integer importe = (Integer) value;
                    if (importe <= 0) {

                        cell.setForeground(crema);

                    } else if (importe > 0) {

                        cell.setForeground(marron);

                    }

                } else if (value instanceof Double) {
                    Double importe = (Double) value;
                    if (importe <= 0.00) {

                        cell.setForeground(crema);

                    } else if (importe > 0) {

                        cell.setForeground(marron);
                    }
                } else {
                    cell.setForeground(marron);
                }
            }
        } else {

            cell.setBackground(blanco);

            if (value instanceof String) {
                String categoria;
                if (table.getValueAt(row, 3) instanceof String) {

                    categoria = (String) table.getValueAt(row, 3);
                } else {

                    categoria = "TODOS";
                }

                if (categoria.equals("Calzado")) {

                    cell.setForeground(azul);

                } else if (categoria.equals("Textil")) {

                    cell.setForeground(verde);

                } else if (categoria.equals("Accesorio")) {

                    cell.setForeground(violeta);

                } else {
                    cell.setForeground(marron);
                }
            }//Fin de if (value instanceof String)
            else if (value instanceof Integer) {

                Integer importe = (Integer) value;
                if (importe < 0.00) {

                    cell.setForeground(rojo);

                } else if (importe == 0.00) {

                    cell.setForeground(blanco);

                } else if (importe > 0.00) {

                    cell.setForeground(verde);

                }
            } else if (value instanceof Double) {
                Double importe = (Double) value;
                if (importe < 0.00) {

                    cell.setForeground(rojo);

                } else if (importe == 0.00) {

                    cell.setForeground(blanco);

                } else if (importe > 0.00) {

                    cell.setForeground(azul);

                }
            } else if (value instanceof String) {

                cell.setForeground(marron);

            } else if (value instanceof Date) {

                cell.setForeground(naranja);
            } else if (value instanceof JComboBox) {

                cell.setForeground(naranja);

            }
        }
        return cell;
    }
}
