package com.textil.main;

import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.naming.NamingException;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.comun.utilidad.EvaluarExtensionArchivo;
import com.comun.utilidad.MyArchivoFiltro;
import com.textil.dao.PoolConexionSqlServer;


@SuppressWarnings("deprecation") 

public class FuncionesUsuario implements Serializable {

	private static final long serialVersionUID = 1L;
    public final String TIPOCONEXION = "APP";
    public Connection conexionDataBase;
    public EvaluarExtensionArchivo evaluarArchivo;
	private ImageIcon imagen;
	public final String[] nivel = {"supuno" , "supdos" , "suptres", "supcua" };

    public FuncionesUsuario() {
    }

    public void setEvaluarArchivo(EvaluarExtensionArchivo evaluarArchivo) {
        this.evaluarArchivo = evaluarArchivo;
    }

    public void setConexionDataBase(Connection conexionDataBase) {
        this.conexionDataBase = conexionDataBase;
    }

    public Connection getConexionDataBase() {

        if (TIPOCONEXION.equals("WEB")) {

            conexionDataBase = coneccionWeb();

        }//Fin de if (TIPOCONEXION.equals("WEB"))
        else if (TIPOCONEXION.equals("APP")) {

            conexionDataBase = coneccionApp();

        }

        return conexionDataBase;

    }//Fin de getConexionDataBase

    public void abrirConexion() {
        if (TIPOCONEXION.equals("WEB")) {

            setConexionDataBase(PoolConexionSqlServer.crearConexionApp());

        }//Fin de if(TIPOCONEXION.equals("WEB"))
        else if (TIPOCONEXION.equals("APP")) {

            setConexionDataBase(PoolConexionSqlServer.crearConexionApp());
        }
    }//Fin de abrirConexion

    public void cerrarConexion() {
        try {

            this.conexionDataBase.close();

        } catch (SQLException ex) {
            Logger.getLogger(FuncionesUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public java.sql.Date from_UtilDate_To_SqlDate(java.util.Date parFecha) {

        java.sql.Date varFecha = new java.sql.Date(parFecha.getYear(), parFecha.getMonth(), parFecha.getDate());

        return varFecha;
    }
    public java.util.Date from_SqlDate_To_UtilDate(java.sql.Date parFecha) {

        java.util.Date varFecha = new java.util.Date(parFecha.getYear(), parFecha.getMonth(), parFecha.getDate());

        return varFecha;
    }

    public String getCodTab(String nomTab, int numReg, int canCar) {
        numReg = numReg + 1;

        String nueNumReg = String.valueOf(numReg);
        int numCero = canCar - (nomTab.length() + nueNumReg.length());

        for (int i = 1; i <= numCero; i++) {
            nueNumReg = "0" + nueNumReg;
        }
        nueNumReg = nomTab + nueNumReg;

        return nueNumReg;
    }//Fin de fuencion para sacar el codigo de una tabla

    public String getMonedaToString(double parValEva) {

        int numInt = ((int) parValEva);
        int numDec = (int) Math.round((parValEva - numInt) * 100);

        /*
         * System.out.println("entero : " + numInt); System.out.println("decimal
         * : " + numDec);
         */
        String resNumConv = numeroLetra(numInt) + " con " + ((numDec < 10) ? "0" + numDec : numDec) + "/100 nuevos soles";

        return resNumConv.trim();
    }

    public static String numeroLetra(int numero) {
        String cadena = new String();
        // Aqui identifico si lleva millones
        if ((numero / 1000000) > 0) {
            if ((numero / 1000000) == 1) {
                cadena = " Un Millon " + numeroLetra(numero % 1000000);
            } else {
                cadena = numeroLetra(numero / 1000000) + " Millones " + numeroLetra(numero % 1000000);
            }
        } else {
            // Aqui identifico si lleva Miles
            if ((numero / 1000) > 0) {

                if ((numero / 1000) == 1) {
                    cadena = " Mil " + numeroLetra(numero % 1000);
                } else {
                    cadena = numeroLetra(numero / 1000) + " Mil " + numeroLetra(numero % 1000);
                }
            } else {
                // Aqui identifico si lleva cientos
                if ((numero / 100) > 0) {
                    if ((numero / 100) == 1) {
                        if ((numero % 100) == 0) {
                            cadena = " Cien ";
                        } else {
                            cadena = " Ciento " + numeroLetra(numero % 100);
                        }
                    } else {
                        if ((numero / 100) == 5) {
                            cadena = " Quinientos " + numeroLetra(numero % 100);
                        } else {
                            if ((numero / 100) == 9) {
                                cadena = " Novecientos " + numeroLetra(numero % 100);
                            } else {
                                cadena = numeroLetra(numero / 100) + "cientos" + numeroLetra(numero % 100);
                            }
                        }
                    }
                } // Aqui se identifican las Decenas
                else {
                    if ((numero / 10) > 0) {
                        switch (numero / 10) {
                            case 1:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Diez ";
                                        break;
                                    case 1:
                                        cadena = " Once ";
                                        break;
                                    case 2:
                                        cadena = " Doce ";
                                        break;
                                    case 3:
                                        cadena = " Trece ";
                                        break;
                                    case 4:
                                        cadena = " Catorce ";
                                        break;
                                    case 5:
                                        cadena = " Quince ";
                                        break;
                                    default:
                                        cadena = " Diez y " + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                            case 2:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Veinte ";
                                        break;
                                    default:
                                        cadena = " Veinti" + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                            case 3:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Treinta ";
                                        break;
                                    default:
                                        cadena = " Treinta y " + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                            case 4:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Cuarenta ";
                                        break;
                                    default:
                                        cadena = " Cuarenta y " + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                            case 5:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Cincuenta ";
                                        break;
                                    default:
                                        cadena = " Cincuenta y " + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                            case 6:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Sesenta ";
                                        break;
                                    default:
                                        cadena = " Sesenta y " + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                            case 7:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Setenta ";
                                        break;
                                    default:
                                        cadena = " Setenta y " + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                            case 8:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Ochenta ";
                                        break;
                                    default:
                                        cadena = " Ochenta y " + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                            case 9:
                                switch (numero % 10) {
                                    case 0:
                                        cadena = " Noventa ";
                                        break;
                                    default:
                                        cadena = " Noventa y " + numeroLetra(numero % 10);
                                        break;
                                }
                                break;
                        }
                    } else {
                        switch (numero) {
                            case 1:
                                cadena = "Uno";
                                break;
                            case 2:
                                cadena = "Dos";
                                break;
                            case 3:
                                cadena = "Tres";
                                break;
                            case 4:
                                cadena = "Cuatro";
                                break;
                            case 5:
                                cadena = "Cinco";
                                break;
                            case 6:
                                cadena = "Seis";
                                break;
                            case 7:
                                cadena = "Siete";
                                break;
                            case 8:
                                cadena = "Ocho";
                                break;
                            case 9:
                                cadena = "Nueve";
                                break;
                        }
                    }

                }
            }

        }
        return cadena;
    }//FIN DE FUNCION PARA CONVERTIR NUMERO A TEXTO

    public String getFillZero(int numReg, int canCar) {

        String nueNumReg = String.valueOf(numReg);
        int numCero = canCar - nueNumReg.length();

        for (int i = 1; i <= numCero; i++) {
            nueNumReg = "0" + nueNumReg;
        }

        return nueNumReg;
    }//Fin de fuencion para sacar el codigo de una tabla

    public boolean isDouble(String strNumOpe) {

        try {
            Double.parseDouble(strNumOpe);
            return true;
        } catch (NumberFormatException e) {
            System.err.println("no es un double ");
            return false;
        }
    }

    public boolean isInteger(String strNumOpe) {

        try {
            Integer.parseInt(strNumOpe);
            return true;
        } catch (NumberFormatException e) {
            System.err.println("no es un Entero ");
            return false;
        }
    }

    public boolean isDate(String strFecha) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

//        Date miFecha = null;
        try {
            sdf.parse(strFecha);
            return true;
        } catch (ParseException ex) {

            return false;

        }
    }

    public boolean isDateFormato(String strFecha, String parFormato) {

        SimpleDateFormat sdf = new SimpleDateFormat(parFormato);

//        Date miFecha = null;
        try {
            sdf.parse(strFecha);
            return true;
        } catch (ParseException ex) {

            return false;

        }
    }
    
    public boolean isTime(String strHora) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        try {

            sdf.parse(strHora);
            return true;

        } catch (Exception ex) {

            return false;

        }
    }

    public Date fromStrdateToDate(String strFecha) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date miFecha;
        try {

            miFecha = sdf.parse(strFecha);

        } catch (ParseException ex) {
            System.err.println("Fecha no valida");
            miFecha = null;

        }

        return miFecha;
    }//Fin de metodo de DateToString()

    public Date fromStrdateToDateFormato(String strFecha, String parFormato) {

        SimpleDateFormat sdf = new SimpleDateFormat(parFormato);
        Date miFecha;
        try {

            miFecha = sdf.parse(strFecha);

        } catch (ParseException ex) {
            System.err.println("Fecha no valida");
            miFecha = null;

        }

        return miFecha;
    }//Fin de metodo de DateToString()

    public java.sql.Date fromStrdateToDateSql(String strFecha) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaInicio;
        try {

            fechaInicio = sdf.parse(strFecha);
            return from_UtilDate_To_SqlDate(fechaInicio);

        } catch (ParseException ex) {
            System.err.println("Fecha no valida");

        }

        return null;
    }//Fin de metodo de DateToString()

    public String dateToString(java.sql.Date strFecha, String parFormato) {

        if (strFecha != null) {
            if (parFormato.equals("yyyy-MM-dd")
                    || parFormato.equals("dd/MM/yyyy")
                    || parFormato.equals("dd-MM-yyyy")
                    || parFormato.equals("yyyy/MM/dd")) {

                SimpleDateFormat formatoDelTexto = new SimpleDateFormat(parFormato);
                String fecha = formatoDelTexto.format(strFecha);
                return fecha;

            }//Fin de condicion de formato
        }//Fin de if (strFecha != null) 

        return "";

    }//Fin de dateToString(java.sql.Date strFecha, String parFormato)

    public String stringDateToString(String parFecha, String parFormato) {

        String[] matrizFecha;
        String ano = "00", mes = "00", dia = "00";
        String signo;
        if(parFormato.equals("dd/MM/yyyy")){

                signo = "/";
                matrizFecha = parFecha.split(signo);
                if (isInteger(matrizFecha[0]) && isInteger(matrizFecha[1]) && isInteger(matrizFecha[2])) {

                    dia = matrizFecha[0];
                    mes = matrizFecha[1];
                    ano = matrizFecha[2];

                }
        }else if(parFormato.equals("dd-MM-yyyy")){
                signo = "-";
                matrizFecha = parFecha.split(signo);
                if (isInteger(matrizFecha[0]) && isInteger(matrizFecha[1]) && isInteger(matrizFecha[2])) {

                    dia = matrizFecha[0];
                    mes = matrizFecha[1];
                    ano = matrizFecha[2];

                }//Fin de if(isInteger(matrizFecha[0]) && isInteger(matrizFecha[1])  &&isInteger(matrizFecha[2]) )
        }else if(parFormato.equals("yyyy/MM/dd")){

                signo = "/";
                matrizFecha = parFecha.split(signo);
                if (isInteger(matrizFecha[0]) && isInteger(matrizFecha[1]) && isInteger(matrizFecha[2])) {

                    ano = matrizFecha[0];
                    mes = matrizFecha[1];
                    dia = matrizFecha[2];

                }//Fin de if(isInteger(matrizFecha[0]) && isInteger(matrizFecha[1])  &&isInteger(matrizFecha[2]) )
        }else if(parFormato.equals("yyyy-MM-dd")){

                signo = "-";
                matrizFecha = parFecha.split(signo);
                if (isInteger(matrizFecha[0]) && isInteger(matrizFecha[1]) && isInteger(matrizFecha[2])) {

                    ano = matrizFecha[0];
                    mes = matrizFecha[1];
                    dia = matrizFecha[2];

                }//Fin de if(isInteger(matrizFecha[0]) && isInteger(matrizFecha[1])  &&isInteger(matrizFecha[2]) )
        }
        return dia + "/" + mes + "/" + ano;

    }//Fin de 

    public Double redondearNumero(Double numOpe, Integer numDec) {

        int cifras = (int) Math.pow(10, numDec);
        return Math.rint(numOpe * cifras) / cifras;

    }//Fin de redondearNumero(Double numOpe, Integer numDec)

    public String quitarCaracter(String cadOpe, String caracter) {
        String nueCad = "";
        for (int i = 0; i < cadOpe.length(); i++) {
            if (cadOpe.substring(i, i + 1).equals(caracter) != true) {

                nueCad = nueCad + cadOpe.substring(i, i + 1);

            }
        }
        return nueCad;
    }//Fin de metodo quitar coma

    public String getCodTab(String nomTab, long numReg, int canCar) {
        numReg = numReg + 1;

        String nueNumReg = String.valueOf(numReg);
        int numCero = canCar - (nomTab.length() + nueNumReg.length());

        for (int i = 1; i <= numCero; i++) {
            nueNumReg = "0" + nueNumReg;
        }
        nueNumReg = nomTab + nueNumReg;

        return nueNumReg;
    }//Fin de fuencion para sacar el codigo de una tabla

    public String timeToString(Time parTime) {


        String hor = String.valueOf(parTime.getHours());
        hor = getFillZero(Integer.parseInt(hor), 2);
        String min = String.valueOf(parTime.getMinutes());
        min = getFillZero(Integer.parseInt(min), 2);
        String sec = String.valueOf(parTime.getSeconds());
        sec = getFillZero(Integer.parseInt(sec), 2);

        String myTime = hor + ":" + min + ":" + sec;

        return myTime;
    }

    public double getCantHorDec(Time parHorUno, Time parHorDos) {

        long resHor = parHorDos.getTime() - parHorUno.getTime();
        double varMin = (resHor / 1000.00) / 60.00;

        return ((varMin * 1.00) / 60);

    }

    public Time stringToTime(String parStrTime) {

        Time hora;
        if (isTime(parStrTime)) {

            String varField;
            String[] varCampo;

            varField = parStrTime;//Hora de Registro
            varCampo = varField.split(":");
            hora = new Time(Integer.parseInt(varCampo[0]), Integer.parseInt(varCampo[1]), Integer.parseInt(varCampo[2]));

        }//Fin de if(isTime(parStrTime))
        else {

            hora = new Time(0, 0, 0);

        }
        return hora;
    }//Fin de stringoTime

    public void mostrarMensaje(String parMsg, String parNomClass) {

        JOptionPane.showConfirmDialog(null,
                parMsg + "\nClase : " + parNomClass,
                "Mensaje del Sistema",
                JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);


    }//Fin de Mostrar Mensaje

    public String obtenerMacAdress() {

        try {
            //Obtenemos la interface en la cual estamos conectados.
            NetworkInterface netInt = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
            //Obtenemos su MAC Address, pero nos devuelve un array de bytes
            //Por lo que hay que convertirlos a Hexadecimal

            String macAdress = "";
            byte[] byteMacAdress = netInt.getHardwareAddress();

            for (int ite = 0; ite < byteMacAdress.length; ite++) {
                //Tratamos los valores que devuelven < 0 normalmente son el "3 y 5 par" 
                if (byteMacAdress[ite] < 0) {

                    //Convertimos el byte a Hexadecimal con la clase Integer
                    String tmpHex = Integer.toHexString(byteMacAdress[ite]);
                    /*
                     * Los numeros que son menores a cero al momento de
                     * convertirlo a string nos devuelven una cadena de este
                     * tipo ffffffAA por lo que unicamente tomamos los ultimos 2
                     * caracteres que son lo que buscamos. y obtenemos esos
                     * ultimos caracteres con substring
                     */
                    if ((ite + 1) == byteMacAdress.length) {
                        macAdress = macAdress + tmpHex.substring(tmpHex.length() - 2).toUpperCase();
                    } else {
                        macAdress = macAdress + tmpHex.substring(tmpHex.length() - 2).toUpperCase() + "-";
                    }
                    continue;
                }
                //Aqui imprimimos directamente los bytes que son mayores a cero.
                if ((ite + 1) == byteMacAdress.length) {
                    macAdress = macAdress + Integer.toHexString(byteMacAdress[ite]);
                } else {
                    macAdress = macAdress + Integer.toHexString(byteMacAdress[ite]) + "-";
                }
            }//Fin de for (int ite = 0; ite < byteMacAdress.length; ite++) {

            return validarMacAdress(macAdress.toUpperCase());

        } catch (SocketException ex) {
            Logger.getLogger(FuncionesUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(FuncionesUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }//Fin de metodo para Obtener la Mac Adrress del una pc

    private String validarMacAdress(String parMacAdress) {

        String newMacAdress = "";
        String[] parseMacAdress = parMacAdress.split("-");

        for (int iteMac = 0; iteMac < parseMacAdress.length; iteMac++) {
            if (parseMacAdress[iteMac].length() == 1) {
                if ((iteMac + 1) == parseMacAdress.length) {

                    newMacAdress = newMacAdress + "0" + parseMacAdress[iteMac];

                } else {

                    newMacAdress = newMacAdress + "0" + parseMacAdress[iteMac] + "-";

                }
            }//Fin de if(parseMacAdress[iteMac].equals("0")){
            else {
                if ((iteMac + 1) == parseMacAdress.length) {

                    newMacAdress = newMacAdress + parseMacAdress[iteMac];

                } else {

                    newMacAdress = newMacAdress + parseMacAdress[iteMac] + "-";

                }
            }//Fin de si el valor hexadecimmal es 0
        }//Fin de for(int iteMac = 0; iteMac< parseMacAdress.length; iteMac++){
        return newMacAdress;
    }//Fin de metodo para validarMacAdress

	public void ubicarCeldaTabla(JTable parTab, int parFil, int parCol) {

		if (!parTab.isRowSelected(parFil) || !parTab.isColumnSelected(parCol)) {

			parTab.changeSelection(parFil, parCol, true, false);

		}// Fin de if(!parTab.isRowSelected(parCol))
	}
    
    public void ubicarRegistroTabla(JTable parTab, String parValor, int parCol) {

        String varValor;

        for (int iteTab = 0; iteTab < parTab.getRowCount(); iteTab++) {
            varValor = (String) parTab.getValueAt(iteTab, parCol);//Codigo del Articulo
            if (varValor.equals(parValor)) {
                if (!parTab.isRowSelected(iteTab)) {

                    parTab.changeSelection(iteTab, 0, true, false);
                    break;

                }//Fin de if(!parTab.isRowSelected(parCol))
            }//Fin de if (varArtCod.equals(parArtCod))
        }//Fin de for (int iteTab = 0; iteTab < parTab.getRowCount(); iteTab++)

    }

    public void ubicarRegistroEnteroTabla(JTable parTab, int parValor, int parCol) {

        int varValor;

        for (int iteTab = 0; iteTab < parTab.getRowCount(); iteTab++) {
            varValor = (Integer) parTab.getValueAt(iteTab, parCol);//Codigo del Articulo
            if (varValor == parValor) {
                if (!parTab.isRowSelected(iteTab)) {

                    parTab.changeSelection(iteTab, 0, true, false);
                    break;

                }//Fin de if (!parTab.isRowSelected(parCol))
            }//Fin de if (varArtCod.equals(parArtCod))
        }//Fin de for (int iteTab = 0; iteTab < parTab.getRowCount(); iteTab++)

    }

    public void setSizeTexto(KeyEvent e, int parNumDig) {

        if (e.getSource() instanceof JTextArea) {

            JTextArea miText = (JTextArea) e.getSource();
            if (miText.getText().trim().length() > parNumDig) {

                miText.setText(miText.getText().substring(0, parNumDig));

            }//Fin de 
        }//Fin de if (e.getSource() instanceof JTextArea)
        else if (e.getSource() instanceof JTextField) {

            JTextField miText = (JTextField) e.getSource();
            if (miText.getText().trim().length() > parNumDig) {

                miText.setText(miText.getText().substring(0, parNumDig));

            }//Fin de if (miText.getText().trim().length() > parNumDig)
        }//Fin de else if (e.getSource() instanceof JTextField) 
    }//Fin de Metodo de SetSizeTexto

    public Connection coneccionWeb() {
        try {
            if (conexionDataBase == null) {
                try {

                    setConexionDataBase(PoolConexionSqlServer.crearConexionWeb());

                } catch (NamingException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }

            }//Fin de if(funUsu.conexionDataBase == null)
            else if (conexionDataBase.isClosed()) {
                try {

                    setConexionDataBase(PoolConexionSqlServer.crearConexionWeb());

                } catch (NamingException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }//Fin de 
        } //Fin de coneccion
        catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return conexionDataBase;

    }//Fin de coneccion    

    public Connection coneccionApp() {
        try {
            if (conexionDataBase == null) {

                setConexionDataBase(PoolConexionSqlServer.crearConexionApp());


            }//Fin de if(funUsu.conexionDataBase == null)
            else if (conexionDataBase.isClosed()) {


                setConexionDataBase(PoolConexionSqlServer.crearConexionApp());

            }//Fin de 
        } //Fin de coneccion
        catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return conexionDataBase;

    }//Fin de coneccion    

    public boolean verificarCarpetaArchivoWeb(String parRutaCarpeta, String parNomFile, String parRutaWeb) {

        File carpeta = new File(parRutaCarpeta);
        if (!carpeta.exists()) {

            carpeta.mkdirs();

        }//Fin de if (!carpeta.exists())
        if (carpeta.exists()) {

            return crearArchivoUrlWeb(parRutaCarpeta, parNomFile, parRutaWeb);

        } else {
            return false;
        }
    }//Fin de metodo para crear reporte

    public boolean crearArchivoUrlWeb(String parRutaCarpeta, String parNomFile, String parRutaWeb) {

        File archivo = new File(parRutaCarpeta + parNomFile);
        if (!archivo.exists()) {
            try {

                URL urlFileRep = new URL(parRutaWeb + parNomFile);

                try {

                    BufferedInputStream bis = new BufferedInputStream(urlFileRep.openStream());
                    FileOutputStream fos = new FileOutputStream(parRutaCarpeta + parNomFile);
                    BufferedOutputStream bos = new BufferedOutputStream(fos);

                    boolean eof = false;
                    int flujoByte;
                    while (!eof) {

                        flujoByte = bis.read();
                        if (flujoByte != -1) {

                            bos.write(flujoByte);

                        }//[IF] Leyendo byte de archivo
                        else {

                            eof = true;
                        }//[ELSE] Fin de archivo
                    }//Fin de bucle para verificar el final del archivo

                    bis.close();
                    bos.close();
                    fos.close();
                    return true;

                } catch (Exception e) {
                    System.err.println(e);
                    return false;
                }
            } catch (IOException ex) {
                Logger.getLogger(FuncionesUsuario.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return true;
    }

    public boolean verificarCarpeta(String parCarpeta) {

        File carpeta = new File(parCarpeta);
        if (!carpeta.exists()) {

            carpeta.mkdirs();
            return true;

        } else {

            return true;

        }

    }//Fin de metodo para crear reporte

    public boolean verificarCarpetaArchivo(File fileOrigen, String parCarpeta, String parArchivo) {

        File carpeta = new File(parCarpeta);
        if (!carpeta.exists()) {

            carpeta.mkdirs();

        }//Fin de if (!carpeta.exists())

        File fileDestino = new File(parCarpeta + parArchivo);
        return verificarArchivo(fileOrigen, fileDestino);

    }//Fin de metodo para crear reporte

    public boolean verificarArchivo(File fileOrigen, File fileDestino) {

        if (!fileDestino.exists()) {
            try {

                FileInputStream fis = new FileInputStream(fileOrigen);
                FileOutputStream fos = new FileOutputStream(fileDestino);

                int dato;
                while ((dato = fis.read()) != -1) {

                    fos.write(dato);

                }

                fis.close();
                fos.close();

                return true;

            } catch (IOException e) {
                System.err.println("Hubo un error de entrada/salida!!!");
            }
        }//Fin de if (!fileDestino.exists())

        return true;

    }//Fin de verificarArchivo(File fileOrigen, File fileDestino)

    public BufferedImage obtenerBufferedImageArchivo(String parRutaCarpeta) {

        try {
            URL urlFile;
            File file;

            file = new File(parRutaCarpeta);
            if (!file.exists()) {

                file = new File(parRutaCarpeta + "imagen.jpg");

            }

            urlFile = file.toURL();
            BufferedImage bufImg = null;

            try {
                bufImg = ImageIO.read(urlFile);
            } catch (IOException ex) {
            }

            return bufImg;
        } catch (MalformedURLException ex) {

            Logger.getLogger(FuncionesUsuario.class.getName()).log(Level.SEVERE, null, ex);
            return null;

        }
    }

    public void abrirArchivoDesktop(String parPathFile, String parNomFile) {

        File file = new File(parPathFile + parNomFile);
        if (file.exists()) {
            try {

                Desktop.getDesktop().open(file);

            } catch (Exception e) {

                e.printStackTrace();
            }
        }//Fin de if(file.exists())
        else {

            JOptionPane.showConfirmDialog(null,
                    "ARCHIVO NO EXISTE O FUE BORRADO!!!"
                    + "\n" + this.getClass().getName(),
                    "Sistema",
                    JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

        }
    }//Fin de abrirArchivoDesktop()

    public String obtenerCarpetaArchivo(String[] parLisExt, String parCarpeta) {

        JFileChooser escogerFile = new JFileChooser();
//        escogerFile.setCurrentDirectory(new File(System.getProperty("user.dir")));//El Directorio Actual
        escogerFile.setCurrentDirectory(new File(parCarpeta));
        escogerFile.setFileSelectionMode(JFileChooser.FILES_ONLY);
        MyArchivoFiltro filtrado = new MyArchivoFiltro(parLisExt);
        escogerFile.addChoosableFileFilter(filtrado);

        int rpta = escogerFile.showDialog(null, "Seleccione un Archivo");

        //rpta = escogerFile.showOpenDialog(null);
        if (rpta == JFileChooser.APPROVE_OPTION) {
            if (filtrado.accept(escogerFile.getSelectedFile()) == true) {

                File fileOrigen = escogerFile.getSelectedFile();
                if (fileOrigen.getName().length() <= 100) {
                    if (verificarCarpetaArchivo(fileOrigen, parCarpeta, fileOrigen.getName())) {

                        return fileOrigen.getName();

                    } else {
                        JOptionPane.showConfirmDialog(null,
                                "NO SE PUDO CREAR EL ARCHIVO!!!"
                                + "\n" + this.getClass().getName(),
                                "Sistema",
                                JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
                    }
                }//Fin de if(fileOrigen.getName().length() <= 100)
                else {
                    JOptionPane.showConfirmDialog(null,
                            "El Nombre de Archivo no debe de exceder los 100 Caracteres!!!"
                            + "\n" + this.getClass().getName(),
                            "Sistema",
                            JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                JOptionPane.showConfirmDialog(null,
                        "EXTENSION DE ARCHIVO INVALIDO!!!",
                        "Systema de Cobranza",
                        JOptionPane.PLAIN_MESSAGE,
                        JOptionPane.INFORMATION_MESSAGE);
            }
        }//Fin de Block True if (resVal == JFileChooser.APPROVE_OPTION)  

        return "SIN ARCHIVO";

    }//Fin de crearObtenerNombreArchivo()       

    public void borrarArchivo(String parCarpeta, String parArchivo) {

        File file = new File(parCarpeta + parArchivo);
        file.delete();

    }//Fin de borrarArchivo(String parCarpeta, String parArchivo)

    public int obtenerMeses(Date parInicio, Date parFinal) {

        Date fechaInicio = new java.sql.Date(parInicio.getYear(), parInicio.getMonth(), 1);
        Date fechaFinal = new java.sql.Date(parFinal.getYear(), parFinal.getMonth(), 1);

        int numMeses = 0;
        while (fechaFinal.compareTo(fechaInicio) == 1
                || fechaFinal.compareTo(fechaInicio) == 0) {

            fechaInicio.setMonth(fechaInicio.getMonth() + 1);
            numMeses++;

        }//Fin de while (fechaFinal.compareTo(fechaInicial) == 1) 

        return numMeses;

    }//Fin de obtenerMeses(Date fechaInicial, Date fechaFinal)

    public String obtenerPeriodo(Date parFecha) {

        int ano = parFecha.getYear() + 1900;
        int mes = parFecha.getMonth() + 1;

        return mes + "-" + ano;

    }//Fin de obtenerMeses(Date fechaInicial, Date fechaFinal)

    public boolean escribirImagenIconDisk(ImageIcon parImgIco, String parCarpeta, String parNomFile) {
        try {

            if (parImgIco == null) {
                parImgIco = new ImageIcon(parCarpeta + "logo.gif");
            }

            Image image = parImgIco.getImage();
            RenderedImage rendered;
            if (image instanceof RenderedImage) {

                rendered = (RenderedImage) image;

            } else {

                BufferedImage buffered = new BufferedImage(parImgIco.getIconWidth(), parImgIco.getIconHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D g = buffered.createGraphics();
                g.drawImage(image, 0, 0, null);
                g.dispose();
                rendered = buffered;

            }

            ImageIO.write(rendered, "GIF", new File(parCarpeta + parNomFile));

            return true;

        } catch (IOException ex) {
            Logger.getLogger(FuncionesUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public java.sql.Date ofStrToDate(String parStrDate) {

        if (isDate(parStrDate)) {

            String varField;
            String[] varCampo;

            varField = parStrDate;//Fecha de Registro
            varCampo = varField.split("-");
            java.sql.Date fecha = new java.sql.Date(Integer.parseInt(varCampo[2]) - 1900, Integer.parseInt(varCampo[1]) - 1, Integer.parseInt(varCampo[0]));
            return fecha;

        } else {
            return null;
        }

    }

	public List<Integer> obtenerListaPagina(String parTexto) {
		String letra;
		String item = "";
		boolean agregar = false;
		List<Integer> lista = new ArrayList<Integer>();
		for (int ite = 0; ite < parTexto.length(); ite++) {

			letra = parTexto.substring(ite, ite + 1);
			if (isInteger(letra)) {

				item = item + letra;
				if (isInteger(item) && !lista.contains(item) && (ite + 1 == parTexto.length())) {
					lista.add(Integer.parseInt(item));
				}

			} else {

				if ((isInteger(item) && !lista.contains(Integer.parseInt(item)))) {
					lista.add(Integer.parseInt(item));
					item = "";
				} else if (isInteger(item) && lista.contains(Integer.parseInt(item))) {
					lista.clear();
					break;
				}
			}
		}// Fin de for (int ite = 0; ite < parTexto.length(); ite++)
		return lista;
	}// Fin de obtenerListaPagina(String parTexto)
    
	public List<String> obtenerListaSigno(String parTexto) {
		String letra;
		String item = "";
		boolean copiar = false;
		List<String> lista = new ArrayList<String>();
		for (int ite = 0; ite < parTexto.length(); ite++) {

			letra = parTexto.substring(ite, ite + 1);
			if (!isInteger(letra) && copiar && (letra.equals(",") || letra.equals("-"))) {

				lista.add(letra);
				copiar = false;

			} else {
				if (isInteger(letra)) {

					copiar = true;

				}
			}
		}// Fin de for (int ite = 0; ite < parTexto.length(); ite++)
		return lista;
	}// Fin de obtenerListaPagina(String parTexto)
	
	public boolean verificarOrdenAsc(List<Integer> parLista) {

		boolean exito = true;
		for (int idxUno = 0; idxUno < parLista.size(); idxUno++) {
			for (int idxDos = idxUno; idxDos < parLista.size(); idxDos++) {

				if (parLista.get(idxUno) > parLista.get(idxDos)) {

					exito = false;
					break;

				}
			}
		}
		return exito;
	}

	public List<Integer> obtenerListaPaginas(List<Integer> lisPag, List<String> lisSig) {

		List<Integer> listaPagina = new ArrayList<Integer>();
		for (int ite = 0; ite < lisPag.size(); ite++) {
			if (lisSig.size() > 0 && ite < lisSig.size() && lisSig.get(ite).equals("-")) {
				for (int iteLis = lisPag.get(ite); iteLis <= lisPag.get(ite + 1); iteLis++) {

					listaPagina.add(iteLis);

				}
				ite = ite + 1;
			} else {
				listaPagina.add(lisPag.get(ite));
			}
		}
		return listaPagina;
	}
	
    
    public boolean verificarCarpetaArchivo(String parRutaCarpeta, String parNomFile, String parRutaWeb) {

        File carpeta = new File(parRutaCarpeta);
        if (!carpeta.exists()) {

            carpeta.mkdirs();

        }//Fin de if (!carpeta.exists())
        if (carpeta.exists()) {

            return crearCarpetaArchivo(parRutaCarpeta, parNomFile, parRutaWeb);

        } else {
            return false;
        }
    }//Fin de metodo para crear reporte

    public boolean crearCarpetaArchivo(String parRutaCarpeta, String parNomFile, String parRutaWeb) {

        File archivo = new File(parRutaCarpeta + parNomFile);
        if (!archivo.exists()) {
            try {

                URL urlFileRep = new URL(parRutaWeb + parNomFile);

                try {

                    BufferedInputStream bis = new BufferedInputStream(urlFileRep.openStream());

                    FileOutputStream fos = new FileOutputStream(parRutaCarpeta + parNomFile);
                    BufferedOutputStream bos = new BufferedOutputStream(fos);

                    boolean eof = false;
                    int flujoByte;
                    while (!eof) {

                        flujoByte = bis.read();
                        if (flujoByte != -1) {

                            bos.write(flujoByte);

                        }//[IF] Leyendo byte de archivo
                        else {

                            eof = true;
                        }//[ELSE] Fin de archivo
                    }//Fin de bucle para verificar el final del archivo

                    bis.close();
                    bos.close();
                    fos.close();
                    return true;

                } catch (Exception e) {
                    System.err.println(e);
                    return false;
                }
            } catch (IOException ex) {
                Logger.getLogger(FuncionesUsuario.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return true;
    }
    public String dateToString(java.sql.Date strFecha) {

        if (strFecha != null) {

            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
            String fecha = formatoDelTexto.format(strFecha);
            return fecha;

        } else {

            return "";
        }

    }

	public boolean existeItemJCombo(JComboBox parJCombo, Object parItem){
		
		boolean existe = false;
		
		for(int ite = 0; ite < parJCombo.getItemCount (); ite++){
			
			Object valor = parJCombo.getItemAt (ite);
			if(valor.equals (parItem)) {
				
				existe = true;
				
			}
		}
		
		return existe;
		
	}//Fin de existeItemJCombo(JComboBox parJCombo, Object parItem) 
    
    public ImageIcon leerBlob(byte[] parResSql) {

        if (parResSql != null) {
            imagen = new ImageIcon(parResSql);
        } else {
            imagen = null;
        }
        return imagen;
    }
    
	public Object obtenerValueFieldClass(Object parObj) {

		// no paramater
		Class noparams[] = {};
		Object result = null;
		try
		{
			if(parObj != null)
			{
				Class objClass = parObj.getClass();
				Field[] listaVariables = objClass.getFields();
				for(Field variable : listaVariables){

					Method method = objClass.getMethod(variable.getName(), String.class);
					
				}
			}
		} catch (Exception exception)
		{
			exception.printStackTrace();
		}
		return result;
	}
    
    
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                FuncionesUsuario funUsu = new FuncionesUsuario();
                System.out.println("fecha : " + funUsu.isDate("22-10-2012"));

            }
        });
    }
}//Finde clase principal