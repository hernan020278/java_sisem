package com.textil.main;

import com.textil.controlador.AuxiliarTextil;
import com.textil.controlador.AuxiliarTextil.AccesoPagina;

public interface InterfaceUsuario {

    public void iniciarFormulario();

    public void iniciarInstancias();

    public void limpiarInstancias(AccesoPagina parAcceso);

    public void limpiarFormulario();
    
    public void generarNuevoRegistro();

    public void llenarFormulario();

    public void activarFormulario(boolean parAct);

    public void refrescarFormulario();

    public void obtenerDatoFormulario();

    public void obtenerDatoBaseDato();

    public boolean validarFormulario();

    public boolean guardarDatoBaseDato(String parModoGrabar);

    public void setManejador(AuxiliarTextil parMan);

    public void cerrar();
}
