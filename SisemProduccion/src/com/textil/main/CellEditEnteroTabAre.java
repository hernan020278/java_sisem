package com.textil.main;

import java.awt.Component;
import java.awt.event.KeyEvent;

import javax.swing.AbstractCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.comun.utilidad.swing.JTextFieldFormatoEntero;

public class CellEditEnteroTabAre extends AbstractCellEditor implements TableCellEditor {

	private DlgArea dlgAre;
	private JFormattedTextField txtCelda;
	private int fil, col;
	private int numOrd;
	private int areCod;
	private boolean evaluar=false;

	public CellEditEnteroTabAre(DlgArea parDlgAre) {

		this.dlgAre = parDlgAre;
		txtCelda = new JTextFieldFormatoEntero(2);
		txtCelda.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
					
					stopCellEditing();

					if(evaluar){
						dlgAre.man.are.setAre_ord(numOrd);
						dlgAre.bookMarkAreCod = dlgAre.man.actualizarArea();

						if (dlgAre.bookMarkAreCod != 0) {

//							dlgAre.man.transactionDelegate.commit();
							dlgAre.man.are.setObjeto(dlgAre.man.obtenerArea(dlgAre.bookMarkAreCod));

							dlgAre.man.obtenerListaArea(dlgAre.bookMarkAreNom);
							dlgAre.llenarTablaArea();

						}// Fin de if(!dlgAre.ingresarSubDetKardexCantidad(cantidad))
						else {
//							dlgAre.man.transactionDelegate.rollback();
						}
					}
				}
			}
		});

	}// TabDetKarIngEditorJTextEntero(DlgArea parDlgArea)

	@Override
	public boolean stopCellEditing() {

		numOrd = (Integer) getCellEditorValue();
		areCod = (Integer) dlgAre.tabAre.getValueAt(fil, 5);// Coitgo de la area

		if (dlgAre.frmAbierto) {
			if (dlgAre.tabAre.isEditing()) {
				if (col == 0) {// COL 1 : Cantidad
					evaluar = true;
				}// Fin de if(col == 1)
				else{
					evaluar = false;
					cancelCellEditing();
				}
			}// fin de if (tabDetKar.isEditing())
			else{
				evaluar = false;
				cancelCellEditing();
			}
		}// Fin de if(dlgAre.frmAbierto)
		else{
			evaluar = false;
			cancelCellEditing();
		}
		return super.stopCellEditing();

	}// Fin stopCellEditing()

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

		fil = row;
		col = column;
		txtCelda.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		txtCelda.setValue(value);

		return txtCelda;

	}

	@Override
	public Object getCellEditorValue() {

		return txtCelda.getValue();

	}
}