package com.textil.main;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

import com.textil.controlador.AuxiliarTextil.AccesoPagina;
import com.textil.controlador.AuxiliarTextil.EstadoDocumento;
import com.textil.controlador.AuxiliarTextil.EstadoPagina;

public class CellEditBooleanTabFic extends AbstractCellEditor implements TableCellEditor {

	private DlgFicha dlgFic;
	private JCheckBox chkCelda;
	private int fil, col;
	private boolean evaluar = false;
	private boolean activar = false;

	public CellEditBooleanTabFic(DlgFicha parDlgFic) {

		this.dlgFic = parDlgFic;
		chkCelda = new JCheckBox();
		chkCelda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				stopCellEditing();

				if (evaluar) {
					
					dlgFic.revisarSeleccionTablaFicha();
					
				}
			}
		});

	}// TabDetKarIngEditorJTextEntero(DlgFicha parDlgFicha)

	@Override
	public boolean stopCellEditing() {

		activar = (Boolean) getCellEditorValue();
		String estado = (String) dlgFic.tabFic.getValueAt(fil, 14);// Columna de estado
		String codigoBarra = (String) dlgFic.tabFic.getValueAt(fil, 2);// Codigo de codigoBarra de la ficha
		if (dlgFic.frmAbierto) {
			if (dlgFic.tabFic.isEditing()) {
				if (col == 1){
					if(!dlgFic.modSeleccion.contains(codigoBarra + "#" + estado)) {// COL 1 : Columna de Seleccion

						dlgFic.modSeleccion.add(codigoBarra + "#" + estado );
						evaluar = true;
						
					}else if(activar== false && !dlgFic.modSeleccion.contains(codigoBarra)) {// COL 1 : Columna de Seleccion
						
						dlgFic.modSeleccion.remove(codigoBarra + "#" + estado );
						evaluar = true;
						
					}else if(activar== true && !dlgFic.modSeleccion.contains(codigoBarra)) {// COL 1 : Columna de Seleccion
						
						evaluar = false;
						cancelCellEditing();
						
					}
				}// Fin de if(col == 1)
				else {
					evaluar = false;
					cancelCellEditing();
				}
			}// fin de if (tabDetKar.isEditing())
			else {
				evaluar = false;
				cancelCellEditing();
			}
		}// Fin de if(dlgFic.frmAbierto)

		return super.stopCellEditing();

	}// Fin stopCellEditing()

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

		fil = row;
		col = column;
		chkCelda.setHorizontalAlignment(SwingConstants.CENTER);
		chkCelda.setBackground(Color.white);
		chkCelda.setSelected((Boolean) value);

		return chkCelda;

	}

	@Override
	public Object getCellEditorValue() {

		return chkCelda.isSelected();

	}
}