package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaFicha extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public ModeloTablaFicha() {

		super.addColumn("Item");  		// Col 0 Integer
		super.addColumn("Sel"); 		// Col 1 Boolean
		super.addColumn("Cod-Bar"); 	// Col 2 String
		super.addColumn("Area");   		// Col 3 String
		super.addColumn("SubArea");   	// Col 4 String
		super.addColumn("Modelo"); 		// Col 5 String
		super.addColumn("Articulo"); 	// Col 6 String
		super.addColumn("Talla"); 		// Col 7 String
		super.addColumn("Ficha");		// Col 8 String Identificador de la ficha (1/1 1/2 1/3 ) que podra ser como codigo de barra
		super.addColumn("Can-Ped");		// Col 9 Integer
		super.addColumn("Can-Fic");		// Col 10 Integer
		super.addColumn("Can-Reg");		// Col 11 Integer
		super.addColumn("Fec-Reg");     // Col 12 String Date
//		super.addColumn("Hor-Reg");     // Col 12 String Date
		super.addColumn("Observacion"); // Col 13 String
		super.addColumn("Estado");  	// Col 14 String
		super.addColumn("fic_cod");		// Col 15 Integer
		super.addColumn("subare_cod");	// Col 16 Integer
		super.addColumn("are_cod");		// Col 17 Integer
		super.addColumn("detped_cod");	// Col 18 Integer
		super.addColumn("ped_cod");		// Col 19 Integer
		super.addColumn("prd_cod");		// Col 20 Integer
		super.addColumn("gru_cod");		// Col 21 Integer

	}

	@Override
	public Class getColumnClass(int columna) {
		if ((columna >= 2 && columna <= 8) || (columna >= 11 && columna <= 14)) {
			return String.class;
		}
		if (columna == 0 || (columna >= 2 && columna <= 6) || (columna >= 9 && columna <= 12)) {
			return Integer.class;
		}
		if (columna == 1) {
			return Boolean.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		if (column == 1) {
			return true;
		}
		return false;
	}
}
