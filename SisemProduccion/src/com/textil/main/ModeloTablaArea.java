package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaArea extends DefaultTableModel {

	public ModeloTablaArea() {

		super.addColumn("Orden");      	// Col 0 Integer
		super.addColumn("Ide");      	// Col 1 String
		super.addColumn("Nombre");      // Col 2 String
		super.addColumn("Descripcion"); // Col 3 String
		super.addColumn("Act"); 		// Col 4 Boolean
		super.addColumn("are_cod");     // Col 5 Integer

	}

	@Override
	public Class getColumnClass(int columna) {
		if (columna >= 1 && columna <= 3) {
			return String.class;
		}
		if (columna == 4) {
			return Boolean.class;
		}
		if (columna == 0 || columna == 5) {
			return Integer.class;
		}

		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int column) {

		if (column == 4 || column==0) {
			return true;
		}
		return false;

	}// Fin de isCellEditable(int row, int column)
}// Fin de Clase principal
