package com.textil.main;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;

public class ItemListaRender extends DefaultListCellRenderer {
	
	private static final long serialVersionUID = 1L;
    private Color azul = new Color(0, 51, 255);
    private Color verde = new Color(0, 153, 51);
    private Color rojo = new Color(204, 0, 0);
    private Color naranja = new Color(255, 153, 0);
    private Color marron = new Color(153, 102, 0);
    private Color crema = new Color(255, 255, 153);
    private Color amarillo = new Color(255, 255, 0);
    private Color negro = new Color(0, 0, 0);
    private Color blanco = new Color(255, 255, 255);

	public Component getListCellRendererComponent (JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

		JLabel etiItemJCombo = (JLabel) super.getListCellRendererComponent (list, value, index, isSelected, cellHasFocus);
		
		if(value != null && value instanceof String){
			
			String valor = (String)value;
			
			etiItemJCombo.setText (valor);
			etiItemJCombo.setOpaque (true);
			etiItemJCombo.setHorizontalAlignment(SwingConstants.CENTER);
			
			if(valor.equals ("CONTADO") || valor.equals ("MAYOR") || valor.equals ("BOLETA")){
				
				etiItemJCombo.setForeground (azul);
				
			}//Fin de if(valor.equals ("CONTADO")){
			else{
				
				etiItemJCombo.setForeground (verde);
				
			}
			
		}//Fin if(value != null)

		return etiItemJCombo;
		
	}
}
