package com.textil.main;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Area;
import com.comun.entidad.Clase;
import com.comun.entidad.Cliente;
import com.comun.entidad.Ficha;
import com.comun.entidad.Grupo;
import com.comun.entidad.Producto;
import com.comun.entidad.SubArea;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.SwingWorker;
import com.comun.utilidad.Util;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.textil.controlador.AuxiliarTextil;
import com.textil.controlador.AuxiliarTextil.AccesoPagina;
import com.textil.controlador.AuxiliarTextil.EstadoPagina;
import com.textil.controlador.ContenedorTextil;
import com.textil.dao.TransactionDelegate;
import com.toedter.calendar.JDateChooser;
import javax.swing.JProgressBar;

public class DlgControlPedido extends JDialog implements InterfaceUsuario {

	/**
	 * Variables principales
	 */
	public static EstadoPagina estDlgPed = EstadoPagina.VISUALIZANDO;
	public AccesoPagina acceso;
	public AuxiliarTextil man;
	public String padre;
	public boolean frmAbierto;
	String archivo;
	/**
	 * 	
	 */
	private CeldaRenderGeneral modTabCellRender;
	private TableColumn[] colTabPed;
	private JComboBox cmbCliIde;
	private JButton cmdCerrar;
	private JButton cmdVenImprimir;
	private ButtonGroup grupoOpcPedEst;
	private ButtonGroup grupoOpcEntidad;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JRadioButton opcPedEstTer;
	private JRadioButton opcPedEstPen;
	private JRadioButton opcPedEstPro;
	private JPanel panKarDes;
	private JPanel panKarTip;
	private JScrollPane scrTabVen;
	private JLabel lblMaterial;
	private JComboBox cmbModMat;
	private JLabel lblGauge;
	private JComboBox cmbModGau;
	private JComboBox cmbModIde;
	private JComboBox cmbModTip;
	private final JTextEditorFecha txtEditFecIni, txtEditFecFin;
	private JDateChooser cmbFecIni;
	private JDateChooser cmbFecFin;
	public int varPedCod, varCliCod;
	private JRadioButton opcPedEstTod;
	private JComboBox cmbTipDoc;
	private HashMap mapReporte = new HashMap();
	private JComboBox cmbTipoImpresion;
	private JSeparator separator_1;
	private JRadioButton opcArea;
	private JRadioButton opcCliente;
	private JComboBox cmbArea;
	private PanelBrowseTablaPedido panBrwTabPed;
	private JButton cmdVenImportar;
	private JProgressBar barraProgreso;

	public DlgControlPedido(FrmPrincipal parent) {
		super(parent);
		setTitle("Control del Busqueda del Pedido : "+this.getClass().getSimpleName());
		setResizable(false);

		txtEditFecIni = new JTextEditorFecha();
		txtEditFecIni.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditFecFin = new JTextEditorFecha();
		txtEditFecFin.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txtEditFecIni.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		txtEditFecFin.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());

		initComponents();
		crearModeloTablas();

		super.addWindowListener(new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				DlgControlPedido.this.frmAbierto = true;
			}
		});
	}

	private void initComponents() {
		this.grupoOpcPedEst = new ButtonGroup();
		this.grupoOpcEntidad = new ButtonGroup();
		this.scrTabVen = new JScrollPane();
		this.panKarDes = new JPanel();
		this.cmbCliIde = new JComboBox();
		this.cmdCerrar = new JButton();
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.cmdVenImprimir = new JButton();
		cmdVenImprimir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdVenImprimir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		cmdVenImportar = new JButton();
		cmdVenImportar.setText("Importar");
		cmdVenImportar.setMargin(new Insets(0, 0, 0, 0));
		cmdVenImportar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdVenImportar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		this.panKarDes.setBorder(BorderFactory.createTitledBorder("Lista de Locales"));
		this.panKarDes.setLayout(null);

		this.cmbCliIde.setFont(new Font("Tahoma", 0, 13));
		this.cmbCliIde.setModel(new DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
		this.cmbCliIde.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		this.panKarDes.add(this.cmbCliIde);
		this.cmbCliIde.setBounds(535, 19, 250, 25);

		getContentPane().add(this.panKarDes);
		this.panKarDes.setBounds(10, 11, 795, 184);

		cmbModIde = new JComboBox();
		cmbModIde.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbModIde.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbModIde.setBounds(10, 83, 189, 25);
		panKarDes.add(cmbModIde);

		JLabel lblModelo = new JLabel();
		lblModelo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblModelo.setText("Modelo");
		lblModelo.setHorizontalAlignment(SwingConstants.CENTER);
		lblModelo.setBounds(10, 61, 189, 20);
		panKarDes.add(lblModelo);
		this.jLabel3 = new JLabel();
		jLabel3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		jLabel3.setBounds(491, 120, 131, 20);
		panKarDes.add(jLabel3);

		this.jLabel3.setHorizontalAlignment(0);
		this.jLabel3.setText("Fecha de Inicio");

		cmbFecIni = new JDateChooser(txtEditFecIni);
		cmbFecIni.setDate(new Date());
		cmbFecIni.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbFecIni.setBounds(491, 142, 131, 25);
		panKarDes.add(cmbFecIni);
		cmbFecIni.setDateFormatString("dd/MM/yyyy");

		cmbFecFin = new JDateChooser(txtEditFecFin);
		cmbFecFin.setDate(new Date());
		cmbFecFin.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbFecFin.setBounds(654, 142, 131, 25);
		panKarDes.add(cmbFecFin);
		cmbFecFin.setDateFormatString("dd/MM/yyyy");
		this.jLabel4 = new JLabel();
		jLabel4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		jLabel4.setBounds(654, 120, 131, 20);
		panKarDes.add(jLabel4);

		this.jLabel4.setHorizontalAlignment(0);
		this.jLabel4.setText("Fecha de Final");
		this.panKarTip = new JPanel();
		panKarTip.setBounds(10, 110, 210, 69);
		panKarDes.add(panKarTip);
		this.opcPedEstPen = new JRadioButton();
		opcPedEstPen.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.opcPedEstPro = new JRadioButton();
		opcPedEstPro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.opcPedEstTer = new JRadioButton();
		opcPedEstTer.setFont(new Font("Tahoma", Font.PLAIN, 14));

		this.panKarTip.setBorder(new TitledBorder(null, "Estado del Pedido", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.panKarTip.setLayout(null);

		this.grupoOpcPedEst.add(this.opcPedEstPen);
		this.opcPedEstPen.setText("Pendiente");
		this.opcPedEstPen.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		this.opcPedEstPen.setName("INGRESO");
		this.panKarTip.add(this.opcPedEstPen);
		this.opcPedEstPen.setBounds(6, 15, 91, 25);

		this.grupoOpcPedEst.add(this.opcPedEstPro);
		this.opcPedEstPro.setText("Produccion");
		this.opcPedEstPro.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		this.opcPedEstPro.setName("SALIDA");
		this.panKarTip.add(this.opcPedEstPro);
		this.opcPedEstPro.setBounds(99, 15, 95, 25);

		this.grupoOpcPedEst.add(this.opcPedEstTer);
		this.opcPedEstTer.setText("Terminado");
		this.opcPedEstTer.setToolTipText("");
		this.opcPedEstTer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		this.opcPedEstTer.setName("AMBOS");
		this.panKarTip.add(this.opcPedEstTer);
		this.opcPedEstTer.setBounds(6, 40, 91, 25);

		opcPedEstTod = new JRadioButton();
		opcPedEstTod.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.grupoOpcPedEst.add(this.opcPedEstTod);
		opcPedEstTod.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcPedEstTod.setToolTipText("");
		opcPedEstTod.setText("Todos");
		opcPedEstTod.setName("AMBOS");
		opcPedEstTod.setBounds(99, 40, 69, 25);
		panKarTip.add(opcPedEstTod);

		JLabel lblTipo = new JLabel();
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipo.setText("Tipo");
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo.setBounds(227, 61, 174, 20);
		panKarDes.add(lblTipo);

		cmbModTip = new JComboBox();
		cmbModTip.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbModTip.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbModTip.setBounds(227, 83, 174, 25);
		panKarDes.add(cmbModTip);

		lblMaterial = new JLabel();
		lblMaterial.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMaterial.setText("Material");
		lblMaterial.setHorizontalAlignment(SwingConstants.CENTER);
		lblMaterial.setBounds(411, 61, 223, 20);
		panKarDes.add(lblMaterial);

		cmbModMat = new JComboBox();
		cmbModMat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbModMat.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbModMat.setBounds(411, 83, 223, 25);
		panKarDes.add(cmbModMat);

		lblGauge = new JLabel();
		lblGauge.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGauge.setText("Gauge");
		lblGauge.setHorizontalAlignment(SwingConstants.CENTER);
		lblGauge.setBounds(644, 61, 141, 20);
		panKarDes.add(lblGauge);

		cmbModGau = new JComboBox();
		cmbModGau.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbModGau.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbModGau.setBounds(644, 83, 141, 25);
		panKarDes.add(cmbModGau);

		JButton cmdBuscar = new JButton("Buscar");
		cmdBuscar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdBuscar.setMargin(new Insets(0, 0, 0, 0));
		cmdBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdBuscarActionPerformed(e);

			}
		});
		cmdBuscar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));
		cmdBuscar.setBounds(384, 112, 94, 52);
		panKarDes.add(cmdBuscar);

		JLabel lblDocumento = new JLabel();
		lblDocumento.setText("Tipo Documento");
		lblDocumento.setHorizontalAlignment(SwingConstants.CENTER);
		lblDocumento.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDocumento.setBounds(227, 120, 147, 20);
		panKarDes.add(lblDocumento);

		cmbTipDoc = new JComboBox();
		cmbTipDoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbTipDoc.setModel(new DefaultComboBoxModel(new String[] { "NOTA DE PEDIDO", "NOTA DE PRUEBA" }));
		cmbTipDoc.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbTipDoc.setBounds(227, 142, 147, 25);
		panKarDes.add(cmbTipDoc);

		opcCliente = new JRadioButton("Clientes");
		opcCliente.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcCliente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		opcCliente.setBounds(446, 19, 83, 23);
		this.grupoOpcEntidad.add(this.opcCliente);
		panKarDes.add(opcCliente);

		opcArea = new JRadioButton("Areas");
		opcArea.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		opcArea.setFont(new Font("Tahoma", Font.PLAIN, 14));
		opcArea.setBounds(28, 21, 76, 23);
		this.grupoOpcEntidad.add(this.opcArea);
		panKarDes.add(opcArea);

		cmbArea = new JComboBox();
		cmbArea.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbArea.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbArea.setBounds(110, 19, 210, 25);
		panKarDes.add(cmbArea);

		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(370, 11, 10, 43);
		panKarDes.add(separator);

		separator_1 = new JSeparator();
		separator_1.setBounds(10, 55, 775, 11);
		panKarDes.add(separator_1);

		this.cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		this.cmdCerrar.setText("Cerrar");
		this.cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		this.cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		this.cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DlgControlPedido.this.cmdCerrarActionPerformed(evt);
			}
		});
		getContentPane().add(this.cmdCerrar);
		this.cmdCerrar.setBounds(691, 620, 114, 40);

		this.cmdVenImprimir.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		this.cmdVenImprimir.setText("Imprimir");
		this.cmdVenImprimir.setMargin(new Insets(0, 0, 0, 0));
		this.cmdVenImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DlgControlPedido.this.cmdVenImprimirActionPerformed(evt);
			}
		});
		getContentPane().add(this.cmdVenImprimir);
		this.cmdVenImprimir.setBounds(10, 620, 141, 40);

		this.cmdVenImportar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		this.cmdVenImportar.setMargin(new Insets(0, 0, 0, 0));
		this.cmdVenImportar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DlgControlPedido.this.cmdVenImportarActionPerformed(evt);
			}
		});
		getContentPane().add(this.cmdVenImportar);
		cmdVenImportar.setBounds(338, 620, 141, 40);
		
		cmbTipoImpresion = new JComboBox();
		cmbTipoImpresion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmbTipoImpresionActionPerformed();

			}
		});
		cmbTipoImpresion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbTipoImpresion.setModel(new DefaultComboBoxModel(new String[] { "TOTAL", "FALTANTES" }));
		cmbTipoImpresion.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbTipoImpresion.setBounds(161, 640, 167, 20);
		getContentPane().add(cmbTipoImpresion);

		JLabel label = new JLabel();
		label.setText("Tipo Impresion");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		label.setBounds(161, 618, 167, 20);
		getContentPane().add(label);

		// Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		// setBounds((screenSize.width - 934) / 2, (screenSize.height - 556) / 2, 669, 507);
		setSize(new Dimension(822, 698));
		setLocationRelativeTo(null);
	}

	private void cmdCerrarActionPerformed(ActionEvent evt) {
		cerrar();
	}

	private void cmdVenImprimirActionPerformed(ActionEvent evt) {
		
		if (panBrwTabPed.tabPed.getRowCount() > 0) {
			enviarImpresora();
		} else {
			JOptionPane.showConfirmDialog(null, "���NO EXISTE REGISTROS A IMPRIMIR!!\n" + getClass().getName(), man.testProp.getPropiedad("TituloSistema"), -1, 1);
		}
	}

	private void cmdVenImportarActionPerformed(java.awt.event.ActionEvent evt) 
	{
		
//		if (Util.getInst().obtenerFechaInternet().compareTo(new Date(2019-1900, 3-1, 25))==-1)
		Util.strServicioWebPHP = Util.getInst().servicioWebPHP("http://www.imperialknits.com/yurak//soap/server.php", "getAcceso", "empresa", "AMPATO");
		if(Util.strServicioWebPHP.compareTo("SI")==0)
		{
			String[] parLisExt = {"xlsx"};
			archivo = Util.getInst().obtenerCarpetaArchivo(parLisExt, "D:\\");

			if(archivo != "SIN ARCHIVO")
			{
				final SwingWorker worker = new SwingWorker() {
					public Object construct() {

						barraProgreso.setIndeterminate(true);
						Map rqst = new LinkedHashMap();
						rqst.put("archivo", archivo);
						
						//List worksheet = Util.getInst().leerArchivoExcel(archivo);
						rqst = Util.getInst().getWorkBook(rqst);

						if( rqst.get("libroExcelXLSX") != null || rqst.get("libroExcelXLS") != null )
						{
							int numCol = 30;
							int rowIni = 3;
							int colIni = 1;
							int numFil = 1000;
							boolean exito = true;
							
							/******************************
							 * Validar registros de excel *
							 ******************************/
							
							rqst.put("rowIni", rowIni);
							rqst.put("colIni", colIni);
							rqst.put("numCol", numCol);
							rqst.put("numFil", numFil);
							
							rqst = man.validarLibroExcel(rqst, barraProgreso);
							
							if(!((String) rqst.get("errorCampo")).isEmpty())
							{
								String errorCampo = (String) rqst.get("errorCampo");
								JOptionPane.showConfirmDialog(null,
										"NO HAY VALOR "+((errorCampo.startsWith("MODELO")) ? errorCampo : "CAMPO [ " + errorCampo + " ]")+" - CELDA" + ((String) rqst.get("errorCelda")) + "\n" + this.getClass().getName(),
										man.testProp.getPropiedad("TituloSistema"),
										JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);								
							}
							else
							{
								dispose();
								man.abrirPantalla("DlgPedidoImportacion", "DlgControlPedido", AccesoPagina.INICIAR);								
							}
						}//if(worksheet!=null)								
						return null;
					}
					public void finished(){						 
					}
				};
				worker.start();
			}//if(archivo != null)
		}//if (cmbOpeNom.getSelectedItem().equals("COMPRA"))
		else {
			JOptionPane.showConfirmDialog(null, "���Se termino el periodo de prueba!!!" + "\n" + this.getClass().getName(), "Sistema Chentty",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
		}//if (cmbOpeNom.getSelectedItem().equals("COMPRA"))
	}// GEN-LAST:event_cmdDetKarAgregarActionPerformed
	
	private void cmbTipoImpresionActionPerformed() {

		cmdVenImprimir.setEnabled(false);

	}

	public static void main(String[] args) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DlgControlPedido.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(DlgControlPedido.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(DlgControlPedido.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(DlgControlPedido.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				DlgControlPedido dialog = new DlgControlPedido(null);
				dialog.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	public void iniciarFormulario() {

		man.obtenerFechaHoraActual();

		if (this.acceso.equals(AccesoPagina.INICIAR)) {
			this.estDlgPed = EstadoPagina.VISUALIZANDO;

			this.frmAbierto = false;

			iniciarInstancias();
			limpiarInstancias(acceso.INICIAR);
			llenarFormulario();
			limpiarFormulario();
			activarFormulario(false);

		} else if (this.acceso.equals(AccesoPagina.ACTUALIZAR)) {

			limpiarFormulario();
			llenarTablaPedido(elaborarSqlCtrlPedido());

		}
		this.opcPedEstTod.setSelected(true);
		setVisible(true);
	}

	public void iniciarInstancias() {
		/*
		 * Obtiene una conexion del pool de conexiones de Apache
		 */
		man.transactionDelegate = (TransactionDelegate) ContenedorTextil.getComponent("TransactionDelegate");
		man.testProp = (TestPropiedades) ContenedorTextil.getComponent("TestPropiedades");
		man.funUsu = (FuncionesUsuario) ContenedorTextil.getComponent("FuncionesUsuario");
//		man.transactionDelegate.start();
		/*
		 * Obtiene las entidades del modelo relacional para su manejo en este
		 * formulario
		 */
		man.gru = (Grupo) ContenedorTextil.getComponent("Grupo");
		man.cla = (Clase) ContenedorTextil.getComponent("Clase");
		
		man.gruCol = (Grupo) ContenedorTextil.getComponent("GrupoColor");		
		man.gruMod = (Grupo) ContenedorTextil.getComponent("GrupoModelo");
		man.gruPrd = (Grupo) ContenedorTextil.getComponent("GrupoProducto");
		man.gruTal = (Grupo) ContenedorTextil.getComponent("GrupoTalla");

		man.claCol = (Clase) ContenedorTextil.getComponent("ClaseColor");
				
		man.mod = (Producto) ContenedorTextil.getComponent("Modelo");
		man.prd = (Producto) ContenedorTextil.getComponent("Producto");
		man.tal = (Producto) ContenedorTextil.getComponent("Talla");

		man.are = (Area) ContenedorTextil.getComponent("Area");
		man.subAre = (SubArea) ContenedorTextil.getComponent("SubArea");
		man.fic = (Ficha) ContenedorTextil.getComponent("Ficha");
		
		man.lisAre = (List<Area>) ContenedorTextil.getComponent("ListaArea");
		man.lisCli = (List<Cliente>) ContenedorTextil.getComponent("ListaCliente");
		man.lisGru = (List<Grupo>) ContenedorTextil.getComponent("ListaGrupo");
		man.lisCla = (List<Clase>) ContenedorTextil.getComponent("ListaClase");
		man.lisPrdGen = (List<Producto>) ContenedorTextil.getComponent("ListaProductoGeneral");
		man.lisMod = (List<Producto>) ContenedorTextil.getComponent("ListaModelo");
		
	}

	public void limpiarInstancias(AccesoPagina parAcceso) {
		if (parAcceso.equals(AccesoPagina.INICIAR) || parAcceso.equals(AccesoPagina.FINALIZAR)) {

			man.gru.limpiarInstancia();
			man.cla.limpiarInstancia();

			man.gruMod.limpiarInstancia();
			man.gruPrd.limpiarInstancia();
			man.gruTal.limpiarInstancia();
			
			man.claCol.limpiarInstancia();

			man.mod.limpiarInstancia();
			man.prd.limpiarInstancia();
			man.tal.limpiarInstancia();

			man.are.limpiarInstancia();
			man.subAre.limpiarInstancia();
			man.fic.limpiarInstancia();
		}// Fin de

		man.lisAre.clear();
		man.lisCli.clear();
		man.lisGru.clear();
		man.lisCla.clear();
		man.lisPrdGen.clear();
		man.lisMod.clear();
	}

	public void limpiarFormulario() {

		cmbArea.setSelectedIndex(0);
		cmbCliIde.setSelectedIndex(0);
		cmbModIde.setSelectedIndex(0);
		cmbModTip.setSelectedIndex(0);
		cmbModMat.setSelectedIndex(0);
		cmbModGau.setSelectedIndex(0);
		// cmbFecIni.setDate(man.fechaActual);
		// cmbFecFin.setDate(man.fechaActual);
		barraProgreso.setValue(0);
		this.grupoOpcPedEst.clearSelection();
		panBrwTabPed.modTabPed.setRowCount(0);
	}

	public void generarNuevoRegistro() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void llenarFormulario() {

		man.obtenerListaGrupo();
		man.obtenerListaClase();

		llenarComboArea();
		llenarComboCliente();
		llenarComboModelo();
		llenarCombo(cmbModTip, "Tipo");
		llenarCombo(cmbModMat, "Material");
		llenarCombo(cmbModGau, "Gauge");

	}

	public void activarFormulario(boolean parAct) {

		cmdVenImprimir.setEnabled(parAct);

	}

	public void refrescarFormulario() {
	}

	public void obtenerDatoFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void obtenerDatoBaseDato() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public boolean validarFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public boolean guardarDatoBaseDato(String parModoGrabar) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void setManejador(AuxiliarTextil parMan) {
		this.man = parMan;
	}

	public void cerrar() {
		if (this.padre.equals("FrmPrincipal")) {
			int resMsg = JOptionPane.showConfirmDialog(this, "�Desea cerrar formulario?", "Sistema de Cobranza", 0, 3);

			if (resMsg == 0) {
				dispose();
				limpiarInstancias(AccesoPagina.FINALIZAR);
				DlgPedido.bookMarkPedCod = 0;

//				man.transactionDelegate.end();
//				try {
//					DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().commit();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
				man.abrirPantalla("FrmPrincipal", "FrmPrincipal", AccesoPagina.ACTUALIZAR);
			}

		} else {
			dispose();
			this.man.abrirPantalla(this.padre, "FrmPrincipal", AccesoPagina.ACTUALIZAR);
		}
	}

	private String elaborarSqlCtrlPedido() {

		String sql = "";
		if (opcArea.isSelected()) {
			sql = "select are_nom,cli_cod,ped_cod,ped_nomdoc,ped_numdoc,ped_nom,ped_temp,ped_fecreg,ped_fecent,ped_est, prd_supuno, dbo.nombreProducto(prd_supuno) as prd_ide, prd_gau " +
					"from viewFichaProducto where 1=1 and are_act = 1 ";
		} else {
			sql = "select * from viewPedidoCliente where 1=1 ";
		}

		mapReporte.clear();

		man.obtenerFechaHoraActual();

		if (opcArea.isSelected()) {
			if (this.cmbArea.getSelectedIndex() > -1) {
				if (!this.cmbArea.getSelectedItem().toString().equals("TODOS")) {

					sql = sql + " and are_nom like '%" + this.cmbArea.getSelectedItem().toString() + "%' ";
					mapReporte.put("ARE_NOM", "%" + this.cmbArea.getSelectedItem().toString() + "%");

				} else {
					sql = sql + " and are_nom like '%%' ";
					mapReporte.put("ARE_NOM", "%%");
				}
			}
		} else {
			if (this.cmbCliIde.getSelectedIndex() > -1) {
				if (!this.cmbCliIde.getSelectedItem().toString().equals("TODOS")) {

					sql = sql + " and cli_nom like '%" + this.cmbCliIde.getSelectedItem().toString() + "%' ";
					mapReporte.put("CLI_NOM", "%" + this.cmbCliIde.getSelectedItem().toString() + "%");

				} else {
					sql = sql + " and cli_nom like '%%' ";
					mapReporte.put("CLI_NOM", "%%");
				}
			}
		}
		if (this.cmbModIde.getSelectedIndex() > -1) {
			if (!this.cmbModIde.getSelectedItem().toString().equals("TODOS")) {

				sql = sql + " and prd_ide like '%" + this.cmbModIde.getSelectedItem().toString() + "%' ";
				mapReporte.put("PRD_IDE", "%" + this.cmbModIde.getSelectedItem().toString() + "%");

			} else {

				sql = sql + " and prd_ide like '%%' ";
				mapReporte.put("PRD_IDE", "%%");

			}
		}
		if (this.cmbModTip.getSelectedIndex() > -1) {
			if (!this.cmbModTip.getSelectedItem().toString().equals("TODOS")) {

				sql = sql + " and prd_tip like '%" + this.cmbModTip.getSelectedItem().toString() + "%' ";
				mapReporte.put("PRD_TIP", "%" + this.cmbModTip.getSelectedItem().toString() + "%");

			} else {
				sql = sql + " and prd_tip like  '%%' ";
				mapReporte.put("PRD_TIP", "%%");

			}
		}
		if (this.cmbModMat.getSelectedIndex() > -1) {
			if (!this.cmbModMat.getSelectedItem().toString().equals("TODOS")) {

				sql = sql + " and prd_mat like '%" + this.cmbModMat.getSelectedItem().toString() + "%' ";
				mapReporte.put("PRD_MAT", "%" + this.cmbModMat.getSelectedItem().toString() + "%");
			} else {
				sql = sql + " and prd_mat like '%%' ";
				mapReporte.put("PRD_MAT", "%%");

			}
		}
		if (this.cmbModGau.getSelectedIndex() > -1) {
			if (!this.cmbModGau.getSelectedItem().toString().equals("TODOS")) {

				sql = sql + " and prd_gau like '%" + this.cmbModGau.getSelectedItem().toString() + "%' ";
				mapReporte.put("PRD_GAU", "%" + this.cmbModGau.getSelectedItem().toString() + "%");

			} else {

				sql = sql + " and prd_gau like '%%' ";
				mapReporte.put("PRD_GAU", "%%");

			}
		}

		if (!opcPedEstTod.isSelected()) {
			if (opcPedEstPen.isSelected() || opcPedEstPro.isSelected() || opcPedEstTer.isSelected()) {
				String estado = (opcPedEstPen.isSelected() ? "PENDIENTE" : (opcPedEstPro.isSelected()) ? "PRODUCCION" : "TERMINADO");
				sql = sql + " and ped_est like '%" + estado + "%' ";
				mapReporte.put("PED_EST", "%" + estado + "%");
			} else {
				sql = sql + " and ped_est like '%%' ";
				mapReporte.put("PED_EST", "%%");
			}
		} else {
			sql = sql + " and ped_est like '%%' ";
			mapReporte.put("PED_EST", "%%");
		}
		if (this.cmbTipDoc.getSelectedIndex() > -1) {
			if (!this.cmbTipDoc.getSelectedItem().toString().equals("TODOS")) {

				sql = sql + " and ped_nomdoc like '%" + this.cmbTipDoc.getSelectedItem().toString() + "%' ";
				mapReporte.put("PED_NOMDOC", "%" + this.cmbTipDoc.getSelectedItem().toString() + "%");

			} else {
				sql = sql + " and ped_nomdoc like '%%' ";
				mapReporte.put("PED_NOMDOC", "%%");

			}
		}

		if (cmbFecIni.getDate() != null && cmbFecFin.getDate() != null) {
			if (cmbFecIni.getDate().compareTo(cmbFecFin.getDate()) == 1) {

				JOptionPane.showConfirmDialog(null, "�LA FECHA DE INICIO DEBE SER MENOR QUE LA FECHA FINAL!!!\n" + getClass().getName(), man.testProp.getPropiedad("TituloSistema"), -1, 1);

				cmbFecIni.setDate(man.fechaActual);
				cmbFecFin.setDate(man.fechaActual);

			}
		} else {
			cmbFecIni.setDate(man.fechaActual);
			cmbFecFin.setDate(man.fechaActual);
		}

		String fecIni = (this.cmbFecIni.getDate().getYear() + 1900) + "-" + (this.cmbFecIni.getDate().getMonth() + 1) + "-" + this.cmbFecIni.getDate().getDate();
		String fecFin = (this.cmbFecFin.getDate().getYear() + 1900) + "-" + (this.cmbFecFin.getDate().getMonth() + 1) + "-" + this.cmbFecFin.getDate().getDate();

		sql = sql + " and (ped_fecreg >= '" + fecIni + "' and ped_fecreg <= '" + fecFin + "') ";
		mapReporte.put("FEC_INI", cmbFecIni.getDate());
		mapReporte.put("FEC_FIN", cmbFecFin.getDate());
//		mapReporte.put("FEC_ACT", man.funUsu.from_SqlDate_To_UtilDate(man.fechaActual));

		if (this.cmbTipoImpresion.getSelectedIndex() > -1) {

			mapReporte.put("DETPED_EST", this.cmbTipoImpresion.getSelectedItem().toString());

		}

		if (opcArea.isSelected()) {
			sql = sql + " group by are_nom,cli_cod,ped_cod,ped_nomdoc,ped_numdoc,ped_nom,ped_temp,ped_fecreg,ped_fecent,ped_est,prd_supuno, prd_gau ";
		} else {
			sql = sql + " and prd_nivel=1 ";
		}

		return sql;

	}

	private String elaborarSqlCtrlPedido(String parPedCod) {
		String sql = "select * from viewRepCtrlDetKardex where kar_cod'" + parPedCod + "'";
		return sql;
	}

	private void llenarComboArea() {

		this.cmbArea.removeAllItems();
		this.cmbArea.addItem("TODOS");

		man.obtenerListaArea();
		for (Area iteAre : man.lisAre) {

			this.cmbArea.addItem(iteAre.getAre_nom());
		}

	}

	private void llenarComboCliente() {

		this.cmbCliIde.removeAllItems();
		this.cmbCliIde.addItem("TODOS");

		man.lisCli.clear();
		man.lisCli.addAll(man.obtenerListaCliente(""));
		for (Cliente iteCli : man.lisCli) {

			this.cmbCliIde.addItem(iteCli.getCli_nom());
		}

	}

	private void llenarComboModelo() {

		this.cmbModIde.removeAllItems();
		this.cmbModIde.addItem("TODOS");
		List<String> lisMod = man.obtenerListaModeloIde("*");

		for (String iteMod : lisMod) {

			this.cmbModIde.addItem(iteMod);
		}

	}

	private void llenarCombo(JComboBox parCombo, String parTipo) {

		parCombo.removeAllItems();
		parCombo.addItem("TODOS");
		for (Grupo iteGru : man.lisGru) {
			if (iteGru.getGru_tipo().equals("DETALLE") && iteGru.getGru_nom().equals(parTipo)) {
				for (Clase iteCla : man.lisCla) {
					if (iteCla.getGru_cod() == iteGru.getGru_cod()) {

						parCombo.addItem(iteCla.getCla_nom());

					}
				}
				break;
			}
		}
	}

	// private void crearModeloTablaPedido() {
	// this.modTabPed = new ModeloTablaPedido();
	// this.tabPed.setModel(this.modTabPed);
	// this.modTabCellRender = new CeldaRenderGeneral();
	// try {
	// this.tabPed.setDefaultRenderer(Class.forName("java.lang.String"), this.modTabCellRender);
	// this.tabPed.setDefaultRenderer(Class.forName("java.lang.Integer"), this.modTabCellRender);
	// this.tabPed.setDefaultRenderer(Class.forName("java.lang.Double"), this.modTabCellRender);
	// } catch (ClassNotFoundException ex) {
	// Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
	// }
	//
	// this.tabPed.setSelectionMode(1);
	// this.tabPed.getTableHeader().setReorderingAllowed(false);
	// this.tabPed.setAutoResizeMode(0);
	// this.tabPed.setShowGrid(true);
	//
	// String[] header = { "Documento", "Numero", "Nombre", "Modelo", "Fec-Reg", "Fec-Ent", "Temp", "Estado", "ped_cod", "cli_cod" };
	// // String[] tipo = { "Date", "String", "String", "String", "String", "String", "String", "String", "Double", "String" };
	//
	// int[] ancho = { 120, 100, 200, 70, 70, 70, 30, 110, 0, 0 };
	//
	// this.colTabPed = new TableColumn[header.length];
	//
	// for (int iteHead = 0; iteHead < header.length; iteHead++) {
	// this.colTabPed[iteHead] = this.tabPed.getColumn(header[iteHead]);
	// this.colTabPed[iteHead].setResizable(true);
	// this.colTabPed[iteHead].setPreferredWidth(ancho[iteHead]);
	// // colTabPed[iteHead].addPropertyChangeListener(new ListenerCambioPropiedad());
	//
	// if (ancho[iteHead] == 0) {
	// this.tabPed.getColumnModel().getColumn(iteHead).setMaxWidth(0);
	// this.tabPed.getColumnModel().getColumn(iteHead).setMinWidth(0);
	// this.tabPed.getTableHeader().getColumnModel().getColumn(iteHead).setMaxWidth(0);
	// this.tabPed.getTableHeader().getColumnModel().getColumn(iteHead).setMinWidth(0);
	// }
	// }
	// }

	private void llenarTablaPedido(String parSql) {

		List<String> lisPed = man.obtenerListaPedido(parSql);
		panBrwTabPed.crearTablaResultado(lisPed);

	}

	private void crearModeloTablas() {

		// crearModeloTablaPedido();
		panBrwTabPed = new PanelBrowseTablaPedido(this, 10, 200, 795, 391);
		getContentPane().add(this.panBrwTabPed);
		
		barraProgreso = new JProgressBar();
		barraProgreso.setBounds(484, 635, 197, 25);
		getContentPane().add(barraProgreso);
		
		panBrwTabPed.getTabPed().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				tabPedMousePressed(evt);
			}

			public void mouseClicked(MouseEvent evt) {
				tabPedMouseClicked(evt);
			}
		});
		panBrwTabPed.getTabPed().addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent evt) {
				tabPedKeyReleased(evt);
			}
		});
	}

	private void tabPedMousePressed(MouseEvent evt) {
		if (panBrwTabPed.getTabPed().getSelectedRow() > -1) {

			varPedCod = ((Integer) panBrwTabPed.getModTabPed().getValueAt(panBrwTabPed.getTabPed().getSelectedRow(), 8));// Codigo del pedido
			varCliCod = ((Integer) panBrwTabPed.getModTabPed().getValueAt(panBrwTabPed.getTabPed().getSelectedRow(), 9));// Codigo del cliente

			if (!panBrwTabPed.getTabPed().isEditing()) {
				// this.cmdVenImprimir.setEnabled(true);
			}
		}
	}

	private void tabPedKeyReleased(KeyEvent evt) {
		if ((evt.getKeyCode() == 38) || (evt.getKeyCode() == 40) || (evt.getKeyCode() == 33) || (evt.getKeyCode() == 34) || (evt.getKeyCode() == 10) || (evt.getKeyCode() == 113)
				|| (evt.getKeyCode() == 9)) {
			tabPedMousePressed(null);
		}
	}

	private void tabPedMouseClicked(MouseEvent evt) {
		if ((evt.getClickCount() == 2) && (panBrwTabPed.getTabPed().getSelectedRow() > -1)) {

			DlgPedido.estDlgPed = EstadoPagina.VISUALIZANDO;
			DlgPedido.bookMarkPedCod = (Integer) panBrwTabPed.getTabPed().getValueAt(panBrwTabPed.getTabPed().getSelectedRow(), 8);// Codigo del pedido

			dispose();
			man.abrirPantalla("DlgPedido", "DlgControlPedido", AccesoPagina.INICIAR);
		}
	}

	public void enviarImpresora() {
		File repFactura;
		if (opcArea.isSelected()) {
			repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "rep_areped.jasper");
		} else {
			repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "rep_cliped.jasper");
		}

		if (repFactura != null) {
			try {
				InputStream inpStrRepFac = new FileInputStream(repFactura);
				try {
					JasperPrint print = JasperFillManager.fillReport(inpStrRepFac, mapReporte, DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection());

					JasperViewer jv = new JasperViewer(print, false);
					jv.setTitle("Reporte de Pedidos");
					jv.setSize(800, 600);
					jv.setVisible(true);

					// JasperPrintManager.printReport(print, true);
				} catch (JRException ex) {
					JOptionPane.showConfirmDialog(null, "Error de conexion con la impresora", "Sistema de Produccion", -1, 1);
				}
			} catch (FileNotFoundException ex) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	private void cmdBuscarActionPerformed(ActionEvent e) {

		if (opcArea.isSelected() || opcCliente.isSelected()) {

			llenarTablaPedido(elaborarSqlCtrlPedido());
			cmdVenImprimir.setEnabled(true);

		} else {

			JOptionPane.showConfirmDialog(this, "Seleccion Area o Cliente", "Sistema de Produccion", -1, 1);

		}
	}

	private void registrarClase(String parGruNom, String parClaNom) {

		if (!man.existeClase(parClaNom)) {

			man.gru.setObjeto(man.obtenerGrupo(parGruNom));
			man.cla.setGru_cod(man.gru.getGru_cod());
			man.cla.setCla_nom(parClaNom);
			man.cla.setCla_des("");
			man.cla.setCla_ver(1);

			man.agregarClase();
		}

	}
}