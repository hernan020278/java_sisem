/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textil.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

/**
 *
 * @author BASVALGRAFICA
 */
public class TestPropiedades {

    private Properties props;

    public TestPropiedades() {

        props = new Properties();

    }//Fin de metodo constructor

    public Properties getPropiedades() {
        try {
            //se leen el archivo .properties
            props.load(getClass().getResourceAsStream("Propiedades.properties"));
            //si el archivo de props NO esta vacio retornan las propiedes leidas
            if (!props.isEmpty()) {
                return props;
            } else {//sino  retornara NULL
                return null;
            }
        } catch (IOException ex) {
            return null;
        }
    }//Fin de metodo para obtener la lista de props

    public String getPropiedad(String parKey) {
        try {
            
            //se leen el archivo .properties
        	props.load(getClass().getResourceAsStream("Propiedades.properties"));
            String valor = props.getProperty(parKey);
            if(valor != null){
            	if(valor.equals("")){
            		valor=parKey;
            	}
            }else{
            	valor=parKey;
            }
            return valor;

        } catch (IOException ex) {
            Logger.getLogger(TestPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public void setPropiedades(String parClave, String parValor) {
        try {
            //se leen el archivo .properties
            URL urlClassPath = getClass().getResource("Propiedades.properties");
            File file = new File(urlClassPath.toURI());
            for (Enumeration enuProp = props.propertyNames(); enuProp.hasMoreElements();) {
                // Obtenemos el objeto
                Object objPropNom = enuProp.nextElement();
                props.setProperty(objPropNom.toString(), props.getProperty(objPropNom.toString()));

            }
            props.setProperty(parClave, parValor);
            //Actualiza el archivo
            props.store(new FileOutputStream(file), null);

        } catch (IOException ex) {
            Logger.getLogger(TestPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(TestPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void eliminarPropieda(String parClave) {
        try {
            //se leen el archivo .properties
            URL urlClassPath = getClass().getResource("Propiedades.properties");
            File file = new File(urlClassPath.toURI());
            //Actualiza el archivo
            props.remove(parClave);
            props.store(new FileOutputStream(file), null);

        } catch (IOException ex) {
            Logger.getLogger(TestPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(TestPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void refrescarClave(List<String> parLis) {

        try {
            //se leen el archivo .properties
            URL urlClassPath = getClass().getResource("Propiedades.properties");
            File file = new File(urlClassPath.toURI());
            String[] regLis;
            props.clear();
            for (int ite = 0; ite < parLis.size(); ite++) {

                regLis = parLis.get(ite).split("#");
                props.setProperty("FRMPAG" + (ite + 1), regLis[1]);
                System.out.println("FRMPAG" + (ite + 1) + " Val : " + regLis[1]);

            }
            //Actualiza el archivo
            props.store(new FileOutputStream(file), null);

        } catch (IOException ex) {
            Logger.getLogger(TestPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(TestPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listarPropiedades() {
        try {
            //se leen el archivo .properties
            props.load(getClass().getResourceAsStream("Propiedades.properties"));
            for (Enumeration enuProp = props.propertyNames(); enuProp.hasMoreElements();) {
                // Obtenemos el objeto
                Object objPropNom = enuProp.nextElement();
                System.out.println("clave : " + objPropNom.toString() + " valor : " + props.getProperty(objPropNom.toString()));
            }
        } catch (IOException ex) {
            Logger.getLogger(TestPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                TestPropiedades testProp = new TestPropiedades();
                testProp.getPropiedad("SistemaTitulo");
            }
        });
    }
}
