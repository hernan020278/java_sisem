package com.textil.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.TableColumn;

import com.comun.entidad.Agrupacion;
import com.comun.entidad.Area;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Ficha;
import com.comun.entidad.Pedido;
import com.comun.entidad.Producto;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.PropiedadesExterna;
import com.comun.utilidad.Util;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.comun.utilidad.swing.PanelImgFoto;
import com.textil.controlador.AuxiliarTextil;
import com.textil.controlador.AuxiliarTextil.AccesoPagina;
import com.textil.controlador.AuxiliarTextil.EstadoPagina;
import com.textil.controlador.ContenedorTextil;
import com.textil.dao.TransactionDelegate;
import com.toedter.calendar.JDateChooser;
import javax.swing.JCheckBox;
import java.awt.Cursor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DlgCodigoProducto extends JDialog implements InterfaceUsuario, PropertyChangeListener {

	/**
	 * Variables principales
	 */
	public EstadoPagina estDlgDetPed;
	public AccesoPagina acceso;
	public AuxiliarTextil man;
	public String padre;
	public boolean frmAbierto;
	public boolean exit;
	/**
	 * 	
	 */
	private CeldaRenderGeneral modTabCellRender;
	private TableColumn[] colTabPed;
	private ButtonGroup grupoOpcPedEst;
	private final JTextEditorFecha txtEditFecIni, txtEditFecFin;
	private JDateChooser cmbPedFecIni;
	private JDateChooser cmbPedFecFin;
	private int varPedCod, varCliCod;
	private JLabel etiSubAreNom;
	private JLabel etiAreNom;
	private JLabel etiPrdIde;
	private JTextFieldChanged txtPrdBar;
	private PanelImgFoto panRegistro;
	private PanelImgFoto panError;
	private PanelImgFoto panCorrecto;
	private PanelImgFoto panAdvertencia;
	private JLabel etiCodigo;
	private JCheckBox chkAlterarIngreso;
	private JLabel etiPedido;
	private JLabel etiRegistro;

	public DlgCodigoProducto(FrmPrincipal parent) {
		super(parent);
		setModal(true);
		setTitle("Control de Producto : "+this.getClass().getSimpleName());
		setResizable(false);

		txtEditFecIni = new JTextEditorFecha();
		txtEditFecFin = new JTextEditorFecha();

		txtEditFecIni.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		txtEditFecFin.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());

		initComponents();

		super.addWindowListener(new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				DlgCodigoProducto.this.frmAbierto = true;
			}

			public void windowClosing(WindowEvent e) {
				cerrar();
			}
		});
	}

	private void initComponents() {
		this.grupoOpcPedEst = new ButtonGroup();

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		etiCodigo = new JLabel("Ingrese Codigo : 1");
		etiCodigo.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiCodigo.setHorizontalAlignment(SwingConstants.CENTER);
		etiCodigo.setFont(new Font("Dialog", Font.PLAIN, 45));
		etiCodigo.setBounds(540, 10, 414, 86);
		getContentPane().add(etiCodigo);

		txtPrdBar = new JTextFieldChanged(13);
		txtPrdBar.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {

				txtPrdBarFocusLost(arg0);

			}
		});
		txtPrdBar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPrdBarKeyReleased(arg0);

			}

		});
		txtPrdBar.setHorizontalAlignment(SwingConstants.CENTER);
		txtPrdBar.setFont(new Font("Dialog", Font.PLAIN, 50));
		txtPrdBar.setText("123456789124");
		txtPrdBar.setBounds(448, 107, 506, 64);
		getContentPane().add(txtPrdBar);

		etiPrdIde = new JLabel("Registro de Producto");
		etiPrdIde.setEnabled(false);
		etiPrdIde.setHorizontalAlignment(SwingConstants.CENTER);
		etiPrdIde.setBorder(new LineBorder(new Color(0, 0, 0)));
		etiPrdIde.setFont(new Font("Dialog", Font.PLAIN, 40));
		etiPrdIde.setBounds(448, 176, 506, 54);
		getContentPane().add(etiPrdIde);

		panRegistro = new PanelImgFoto();
		panRegistro.setImagen(AplicacionGeneral.getInstance().obtenerImagen("registro.png"), null, null, "CONSTRAINT");
		panRegistro.setBounds(10, 10, 428, 420);
		getContentPane().add(panRegistro);
		GroupLayout gl_panRegistro = new GroupLayout(panRegistro);
		gl_panRegistro.setHorizontalGroup(
				gl_panRegistro.createParallelGroup(Alignment.LEADING)
						.addGap(0, 120, Short.MAX_VALUE)
						.addGap(0, 108, Short.MAX_VALUE)
				);
		gl_panRegistro.setVerticalGroup(
				gl_panRegistro.createParallelGroup(Alignment.LEADING)
						.addGap(0, 105, Short.MAX_VALUE)
						.addGap(0, 93, Short.MAX_VALUE)
				);
		panRegistro.setLayout(gl_panRegistro);

		etiAreNom = new JLabel("Registro de Area");
		etiAreNom.setEnabled(false);
		etiAreNom.setBorder(new LineBorder(new Color(0, 0, 0)));
		etiAreNom.setHorizontalAlignment(SwingConstants.CENTER);
		etiAreNom.setFont(new Font("Dialog", Font.PLAIN, 40));
		etiAreNom.setBounds(448, 241, 506, 54);
		getContentPane().add(etiAreNom);

		etiSubAreNom = new JLabel("Registro de SubArea");
		etiSubAreNom.setEnabled(false);
		etiSubAreNom.setHorizontalAlignment(SwingConstants.CENTER);
		etiSubAreNom.setFont(new Font("Dialog", Font.PLAIN, 40));
		etiSubAreNom.setBorder(new LineBorder(new Color(0, 0, 0)));
		etiSubAreNom.setBounds(448, 306, 506, 54);
		getContentPane().add(etiSubAreNom);

		panError = new PanelImgFoto();
		panError.setImagen(AplicacionGeneral.getInstance().obtenerImagen("error.png"), null, null, "CONSTRAINT");
		panError.setBounds(10, 10, 428, 420);
		getContentPane().add(panError);
		GroupLayout gl_panError = new GroupLayout(panError);
		gl_panError.setHorizontalGroup(
				gl_panError.createParallelGroup(Alignment.LEADING)
						.addGap(0, 120, Short.MAX_VALUE)
						.addGap(0, 108, Short.MAX_VALUE)
				);
		gl_panError.setVerticalGroup(
				gl_panError.createParallelGroup(Alignment.LEADING)
						.addGap(0, 105, Short.MAX_VALUE)
						.addGap(0, 93, Short.MAX_VALUE)
				);
		panError.setLayout(gl_panError);

		panCorrecto = new PanelImgFoto();
		panCorrecto.setImagen(AplicacionGeneral.getInstance().obtenerImagen("correcto.png"), null, null, "CONSTRAINT");
		panCorrecto.setBounds(10, 10, 428, 420);
		getContentPane().add(panCorrecto);
		GroupLayout gl_panCorrecto = new GroupLayout(panCorrecto);
		gl_panCorrecto.setHorizontalGroup(
				gl_panCorrecto.createParallelGroup(Alignment.LEADING)
						.addGap(0, 120, Short.MAX_VALUE)
						.addGap(0, 120, Short.MAX_VALUE)
						.addGap(0, 108, Short.MAX_VALUE)
				);
		gl_panCorrecto.setVerticalGroup(
				gl_panCorrecto.createParallelGroup(Alignment.LEADING)
						.addGap(0, 105, Short.MAX_VALUE)
						.addGap(0, 105, Short.MAX_VALUE)
						.addGap(0, 93, Short.MAX_VALUE)
				);
		panCorrecto.setLayout(gl_panCorrecto);

		panAdvertencia = new PanelImgFoto();
		panAdvertencia.setImagen(AplicacionGeneral.getInstance().obtenerImagen("advertencia.png"), null, null, "CONSTRAINT");
		panAdvertencia.setBounds(10, 10, 428, 420);
		getContentPane().add(panAdvertencia);
		GroupLayout gl_panAdvertencia = new GroupLayout(panAdvertencia);
		gl_panAdvertencia.setHorizontalGroup(
				gl_panAdvertencia.createParallelGroup(Alignment.LEADING)
						.addGap(0, 120, Short.MAX_VALUE)
						.addGap(0, 120, Short.MAX_VALUE)
						.addGap(0, 108, Short.MAX_VALUE)
				);
		gl_panAdvertencia.setVerticalGroup(
				gl_panAdvertencia.createParallelGroup(Alignment.LEADING)
						.addGap(0, 105, Short.MAX_VALUE)
						.addGap(0, 105, Short.MAX_VALUE)
						.addGap(0, 93, Short.MAX_VALUE)
				);
		panAdvertencia.setLayout(gl_panAdvertencia);
		
		chkAlterarIngreso = new JCheckBox("");
		chkAlterarIngreso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PropiedadesExterna testProp = new PropiedadesExterna("config");
				String valor = "";
				if(chkAlterarIngreso.isSelected()){
					while (Util.isEmpty(valor)) {
						valor = (String) JOptionPane.showInputDialog(null, "Ingrese Clave?", "Sistema", JOptionPane.QUESTION_MESSAGE);
						chkAlterarIngreso.setSelected(false);
						String s = testProp.getServidor("clave-ficha");
						if(!Util.isEmpty(valor) && valor.equals(testProp.getServidor("clave-ficha"))){
							chkAlterarIngreso.setSelected(true);
							break;
						}
						if(valor==null){
							valor="SINCLAVE";
						}
					}				
				}
			}
		});
		chkAlterarIngreso.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chkAlterarIngreso.setIcon(AplicacionGeneral.getInstance().obtenerImagen("unchecked.png"));
		chkAlterarIngreso.setSelectedIcon(AplicacionGeneral.getInstance().obtenerImagen("checked.png"));
		chkAlterarIngreso.setMinimumSize(new Dimension(50, 50));
		chkAlterarIngreso.setMaximumSize(new Dimension(50, 50));
		chkAlterarIngreso.setPreferredSize(new Dimension(50, 50));
		chkAlterarIngreso.setFont(new Font("Tahoma", Font.PLAIN, 20));
		chkAlterarIngreso.setBounds(448, 10, 86, 86);
		getContentPane().add(chkAlterarIngreso);
		
		etiPedido = new JLabel("Pedido : 10");
		etiPedido.setHorizontalAlignment(SwingConstants.CENTER);
		etiPedido.setFont(new Font("Dialog", Font.PLAIN, 35));
		etiPedido.setEnabled(false);
		etiPedido.setBorder(new LineBorder(new Color(0, 0, 0)));
		etiPedido.setBounds(448, 371, 249, 54);
		getContentPane().add(etiPedido);
		
		etiRegistro = new JLabel("Registrado : 5");
		etiRegistro.setHorizontalAlignment(SwingConstants.CENTER);
		etiRegistro.setFont(new Font("Dialog", Font.PLAIN, 35));
		etiRegistro.setEnabled(false);
		etiRegistro.setBorder(new LineBorder(new Color(0, 0, 0)));
		etiRegistro.setBounds(707, 371, 247, 54);
		getContentPane().add(etiRegistro);

		// Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		// setBounds((screenSize.width - 934) / 2, (screenSize.height - 556) / 2, 669, 507);
		setSize(new Dimension(969, 464));
		setLocationRelativeTo(null);
	}

	public static void main(String[] args) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DlgCodigoProducto.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(DlgCodigoProducto.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(DlgCodigoProducto.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(DlgCodigoProducto.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				DlgCodigoProducto dialog = new DlgCodigoProducto(null);
				dialog.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	@Override
	public void iniciarFormulario() {

		man.bookMarkBarraFicha = "";
		man.bookMarkBarraSubArea = "";
		man.contadorMarcador = 1;
		exit = false;

		if (this.acceso.equals(AccesoPagina.INICIAR)) {

			this.frmAbierto = false;

			iniciarInstancias();
			limpiarInstancias(acceso.INICIAR);
			limpiarFormulario();
			activarFormulario(false);

		}

		txtPrdBar.requestFocus();
		setVisible(true);
	}

	@Override
	public void iniciarInstancias() {
		/*
		 * Obtiene una conexion del pool de conexiones de Apache
		 */
		man.transactionDelegate = (TransactionDelegate) ContenedorTextil.getComponent("TransactionDelegate");
		man.testProp = (TestPropiedades) ContenedorTextil.getComponent("TestPropiedades");
		man.funUsu = (FuncionesUsuario) ContenedorTextil.getComponent("FuncionesUsuario");

//		man.transactionDelegate.start();

		/*
		 * Obtiene las entidades del modelo relacional para su manejo en este
		 * formulario
		 */

		man.are = (Area) ContenedorTextil.getComponent("Area");
		man.subAre = (SubArea) ContenedorTextil.getComponent("SubArea");

		man.ped = (Pedido) ContenedorTextil.getComponent("Pedido");
		man.detPed = (DetPedido) ContenedorTextil.getComponent("DetPedido");
		man.prd = (Producto) ContenedorTextil.getComponent("Producto");
		man.fic = (Ficha) ContenedorTextil.getComponent("Ficha");
		man.usuSes = (Usuario) ContenedorTextil.getComponent("UsuarioSesion");
		man.agru = (Agrupacion) ContenedorTextil.getComponent("Agrupacion");

	}

	@Override
	public void limpiarInstancias(AccesoPagina parAcceso) {
		if (parAcceso.equals(AccesoPagina.INICIAR) || parAcceso.equals(AccesoPagina.FINALIZAR)) {

			man.are.limpiarInstancia();
			man.subAre.limpiarInstancia();

			man.ped.limpiarInstancia();
			man.detPed.limpiarInstancia();
			man.prd.limpiarInstancia();
			man.fic.limpiarInstancia();

		}// Fin de
	}

	@Override
	public void limpiarFormulario() {

		etiCodigo.setText("Ingrese Codigo");
		ponerImagenRegistro("REGISTRO");

		txtPrdBar.setText("");
		etiPrdIde.setText("Registro de Producto");
		etiAreNom.setText("Registro de Area");
		etiSubAreNom.setText("Registro de SubArea");

		etiPrdIde.setEnabled(false);
		etiAreNom.setEnabled(false);
		etiSubAreNom.setEnabled(false);

		// etiPrdIde.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		etiPrdIde.setBorder(new LineBorder(new Color(0, 0, 0)));
		etiAreNom.setBorder(new LineBorder(new Color(0, 0, 0)));
		etiSubAreNom.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		etiPedido.setText("Pedido");
		etiRegistro.setText("Registro");
		chkAlterarIngreso.setSelected(false);
	}

	private void ponerImagenRegistro(String parImgReg) {
		if (parImgReg.equals("REGISTRO")) {

			panRegistro.setVisible(true);
			panError.setVisible(false);
			panAdvertencia.setVisible(false);
			panCorrecto.setVisible(false);

		} else if (parImgReg.equals("CORRECTO")) {

			panRegistro.setVisible(false);
			panError.setVisible(false);
			panAdvertencia.setVisible(false);
			panCorrecto.setVisible(true);

		} else if (parImgReg.equals("ADVERTENCIA")) {

			panRegistro.setVisible(false);
			panError.setVisible(false);
			panAdvertencia.setVisible(true);
			panCorrecto.setVisible(false);

		} else if (parImgReg.equals("ERROR")) {

			panRegistro.setVisible(false);
			panError.setVisible(true);
			panAdvertencia.setVisible(false);
			panCorrecto.setVisible(false);

		}
	}

	@Override
	public void generarNuevoRegistro() {
		// TODO Auto-generated method stub

	}

	@Override
	public void llenarFormulario() {
		etiAreNom.setText("Registro de Area");
		etiSubAreNom.setText("Registro de Sub Area");
		etiPedido.setText("Pedido");
		etiRegistro.setText("Registro");
		etiAreNom.setBorder(null);
		etiSubAreNom.setBorder(null);
		etiSubAreNom.setEnabled(false);
		etiAreNom.setEnabled(false);

		if (!man.bookMarkBarraFicha.equals("")) {

			etiPrdIde.setText(man.prd.getPrd_ide());
			etiPrdIde.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
			etiPrdIde.setEnabled(true);

		} else {
			etiPrdIde.setText("Registro de Producto");
			etiPrdIde.setBorder(null);
			etiPrdIde.setEnabled(false);
		}
		if (!man.bookMarkBarraSubArea.equals("")) {

			etiAreNom.setText(man.are.getAre_nom());
			etiSubAreNom.setText(man.subAre.getSubare_nom());
			etiAreNom.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
			etiSubAreNom.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
			etiSubAreNom.setEnabled(true);
			etiAreNom.setEnabled(true);
		}
		etiPedido.setText("Pedido : " + String.valueOf(man.fic.getFic_canped()));
		etiRegistro.setText("Registro : " + String.valueOf(man.fic.getFic_canreg()));
		etiCodigo.setText("Ingrese Codigo : " + man.contadorMarcador);

	}

	@Override
	public void activarFormulario(boolean parAct) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refrescarFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean guardarDatoBaseDato(String parModoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setManejador(AuxiliarTextil parMan) {

		this.man = parMan;

	}

	@Override
	public void cerrar() {
		if (this.padre.equals("FrmPrincipal")) {
			int resMsg = JOptionPane.showConfirmDialog(this, "�Desea cerrar formulario?", "Sistema de Cobranza", 0, 3);

			if (resMsg == 0) {
				dispose();
				limpiarInstancias(AccesoPagina.FINALIZAR);
//				man.transactionDelegate.end();
//				try {
//					DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().commit();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
				man.abrirPantalla("FrmPrincipal", "FrmPrincipal", AccesoPagina.ACTUALIZAR);
			}

		} else {
			dispose();
			this.man.abrirPantalla(this.padre, "FrmPrincipal", AccesoPagina.ACTUALIZAR);
		}
	}

	private void txtPrdBarKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
			exit = true;
			man.bookMarkBarraFicha = "";
			man.bookMarkBarraSubArea = "";
			man.contadorMarcador = 1;
			ponerImagenRegistro("REGISTRO");
			cerrar();
		} else {

			txtPrdBar.setText(txtPrdBar.getText().trim());
			if (txtPrdBar.getText().trim().length() == 13) {

				txtPrdBar.setText(txtPrdBar.getText().trim().substring(0, 12));
				txtPrdBar.setEditable(false);
				boolean reproducirSonido = false;
				if (man.contadorMarcador == 2) {
					reproducirSonido = true;
				}
				final String respuesta = man.registrarControlFicha(txtPrdBar.getText().trim(), chkAlterarIngreso.isSelected());
				llenarFormulario();
				ponerImagenRegistro(respuesta);
				txtPrdBar.setText("");
				if (respuesta.equals("CORRECTO")) {
//						man.transactionDelegate.commit();
				} else {
//						man.transactionDelegate.rollback();
				}

				if (reproducirSonido) {

					SwingUtilities.invokeLater(new Runnable()
					{
						@Override
						public void run()
						{
							Map requestParameters = new HashMap();
							requestParameters.put("manejador", man);
							requestParameters.put("accion", "reproducirSonido");
							requestParameters.put("sonido", respuesta);
							man.reproducirSonido(requestParameters);
						}
					});
				}

				txtPrdBar.setEditable(true);					
			}
			exit = false;
		}
	}

	public void propertyChange(PropertyChangeEvent evt) {

		System.out.println("progreso : " + evt.getPropertyName());

		if ("progress".equals(evt.getPropertyName())) {

			System.out.println("Termino el Hilo");

		}

	}

	private void txtPrdBarFocusLost(FocusEvent arg0) {
		// if (!exit) {
		// txtPrdBar.requestFocus();
		// }
	}
}