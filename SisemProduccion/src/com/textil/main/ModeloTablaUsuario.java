package com.textil.main;

import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author Romulo
 */
public class ModeloTablaUsuario extends DefaultTableModel {

	public ModeloTablaUsuario() {

		super.addColumn("Ide");      // Col 0
		super.addColumn("Mail");     // Col 1
		super.addColumn("Nombre");   // Col 2
		super.addColumn("Estado");   // Col 3
		super.addColumn("Area");     // Col 4
		super.addColumn("Clave");    // Col 5
		super.addColumn("version");  // Col 6

	}

	@Override
	public Class getColumnClass(int columna) {
		if (columna >= 0 && columna <= 5) {
			return String.class;
		}
		if (columna == 6) {
			return Integer.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
