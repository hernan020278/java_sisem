/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textil.main;

import java.awt.Color;
import java.awt.Component;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class CeldaRenderGeneral extends DefaultTableCellRenderer {

	private Color azul = new Color(0, 51, 255);
	private Color verde = new Color(0, 153, 51);
	private Color rojo = new Color(204, 0, 0);
	private Color naranja = new Color(255, 153, 0);
	private Color marron = new Color(153, 102, 0);
	private Color crema = new Color(255, 255, 153);
	private Color amarillo = new Color(255, 255, 0);
	private Color negro = new Color(0, 0, 0);
	private Color blanco = new Color(255, 255, 255);

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

		Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		if (value instanceof Boolean) {
			
			super.setHorizontalAlignment(SwingConstants.CENTER);
			
		} else if (value instanceof String) {
			if (isDate(String.valueOf(value))) {

				super.setHorizontalAlignment(SwingConstants.CENTER);

			}// Fin de if(isDate(String.valueOf(value))
			else {

				super.setHorizontalAlignment(SwingConstants.LEFT);

			}
		} else if (value instanceof Date || value instanceof Integer) {

			super.setHorizontalAlignment(SwingConstants.CENTER);

		} else if (value instanceof Double) {

			super.setHorizontalAlignment(SwingConstants.RIGHT);

		}

		if (isSelected) {

			setOpaque(true);

			if (hasFocus) {

				cell.setBackground(marron);
				if (value instanceof Integer) {
					Integer importe = (Integer) value;
					if (importe <= 0) {

						cell.setForeground(marron);

					} else if (importe > 0) {

						cell.setForeground(crema);

					}

				} else if (value instanceof Double) {
					Double importe = (Double) value;
					if (importe <= 0.00) {

						cell.setForeground(marron);

					} else if (importe > 0) {

						cell.setForeground(crema);
					}
				} else {
					cell.setForeground(crema);
				}

			}// Fin de if (hasFocus)
			else {

				cell.setBackground(crema);
				if (value instanceof Integer) {
					Integer importe = (Integer) value;
					if (importe <= 0) {

						cell.setForeground(crema);

					} else if (importe > 0) {

						cell.setForeground(marron);

					}

				} else if (value instanceof Double) {
					Double importe = (Double) value;
					if (importe <= 0.00) {

						cell.setForeground(crema);

					} else if (importe > 0) {

						cell.setForeground(marron);
					}
				} else {
					cell.setForeground(marron);
				}
			}// Fin de false if (hasFocus)
		}// Fin de if (isSelected)
		else {

			setOpaque(false);
			cell.setBackground(blanco);

			if (value instanceof Integer) {

				Integer importe = (Integer) value;

				if (importe <= 0) {

					cell.setForeground(blanco);

				} else if (importe > 0) {

					cell.setForeground(verde);

				}

			} else if (value instanceof Double) {
				Double importe = (Double) value;

				if (importe <= 0.00) {

					cell.setForeground(blanco);

				} else if (importe > 0) {

					cell.setForeground(azul);
				}
			} else if (value instanceof String) {
				if (isDate(String.valueOf(value))) {

					cell.setForeground(naranja);

				}// Fin de if (isDate(String.valueOf(value)))
				else {

					cell.setForeground(marron);

				}
			} else if (value instanceof Date) {

				cell.setForeground(naranja);

			} else if (value instanceof JComboBox) {

				cell.setForeground(naranja);

			}
		}// Fin de false de if (isSelected)
		return cell;
	}

	public boolean isDate(String strFecha) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		java.util.Date miFecha = null;
		try {
			miFecha = sdf.parse(strFecha);
			return true;
		} catch (ParseException ex) {

			return false;

		}
	}
}
