package com.textil.main;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

public class CellEditBooleanTabAre extends AbstractCellEditor implements TableCellEditor {

	private DlgArea dlgAre;
	private JCheckBox chkCelda;
	private int fil, col;
	private boolean evaluar = false;
	private boolean activar=false;

	public CellEditBooleanTabAre(DlgArea parDlgAre) {

		this.dlgAre = parDlgAre;
		chkCelda = new JCheckBox();
		chkCelda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				stopCellEditing();

				if (evaluar) {
					dlgAre.man.are.setAre_act(activar);
					dlgAre.bookMarkAreCod = dlgAre.man.actualizarArea();

					if (dlgAre.bookMarkAreCod != 0) {

//						dlgAre.man.transactionDelegate.commit();
						dlgAre.man.are.setObjeto(dlgAre.man.obtenerArea(dlgAre.bookMarkAreCod));

						dlgAre.man.obtenerListaArea(dlgAre.bookMarkAreNom);
						dlgAre.llenarTablaArea();
						
						dlgAre.man.funUsu.ubicarRegistroEnteroTabla(dlgAre.tabAre, dlgAre.bookMarkAreCod, 5);// Codigo de Area
						dlgAre.seleccionarRegistroTablaArea();


					}// Fin de if(!dlgAre.ingresarSubDetKardexCantidad(cantidad))
					else {
//						dlgAre.man.transactionDelegate.rollback();
					}
				}
			}
		});

	}// TabDetKarIngEditorJTextEntero(DlgArea parDlgArea)

	@Override
	public boolean stopCellEditing() {

		activar = (Boolean) getCellEditorValue();
		if (dlgAre.frmAbierto) {
			if (dlgAre.tabAre.isEditing()) {
				if (col == 4) {// COL 1 : Activar o Desactivar Area
					int resMsg = JOptionPane.showConfirmDialog(dlgAre,
							"�DESEA ACTIVAR EL AREA " + dlgAre.man.are.getAre_nom() + "?",
							dlgAre.man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					if (resMsg == JOptionPane.YES_OPTION) {

						evaluar = true;

					} else {
						evaluar = false;
						cancelCellEditing();
					}
				}// Fin de if(col == 1)
				else {
					evaluar = false;
					cancelCellEditing();
				}
			}// fin de if (tabDetKar.isEditing())
			else {
				evaluar = false;
				cancelCellEditing();
			}
		}// Fin de if(dlgAre.frmAbierto)

		return super.stopCellEditing();

	}// Fin stopCellEditing()

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

		fil = row;
		col = column;
		chkCelda.setHorizontalAlignment(SwingConstants.CENTER);
		chkCelda.setBackground(Color.white);
		chkCelda.setSelected((Boolean) value);

		return chkCelda;

	}

	@Override
	public Object getCellEditorValue() {

		return chkCelda.isSelected();

	}
}