package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaCtrlProducto extends DefaultTableModel {
	private static final long serialVersionUID = 1L;

	public ModeloTablaCtrlProducto() {

		super.addColumn("Area");    	// 0 String
		super.addColumn("SubArea"); 	// 1 String
		super.addColumn("Cliente"); 	// 2 String
		super.addColumn("Tipo");    	// 3 String
		super.addColumn("Articulo");  	// 4 String
		super.addColumn("Color");   	// 5 String
		super.addColumn("Talla");   	// 6 String
		super.addColumn("Ficha");   	// 7 String
		super.addColumn("Fecha");		// 8 String
		super.addColumn("Estado");  	// 9 String
		super.addColumn("Reg");  		// 10 String
		super.addColumn("Pcs");     	// 11 Integer
		super.addColumn("fic_cod");   	// 12 Integer
		super.addColumn("subare_cod");	// 13 Integer
		super.addColumn("are_cod");   	// 14 Integer
		super.addColumn("detped_cod");	// 15 Integer
		super.addColumn("ped_cod");   	// 16 Integer
		super.addColumn("prd_cod");   	// 17 Integer
		super.addColumn("gru_cod");   	// 18 Integer

	}

	@Override
	public Class getColumnClass(int columna) {
		if (columna >= 0 && columna <= 9) {
			return String.class;
		}
		if ((columna >= 10 && columna <= 18)) {
			return Integer.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		// if (col == 5) {
		// return true;
		// }
		return false;
	}
}
