package com.textil.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Area;
import com.comun.entidad.Clase;
import com.comun.entidad.Cliente;
import com.comun.entidad.Ficha;
import com.comun.entidad.Grupo;
import com.comun.entidad.Pedido;
import com.comun.entidad.Producto;
import com.comun.entidad.SubArea;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoEntero;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.textil.controlador.AuxiliarTextil;
import com.textil.controlador.AuxiliarTextil.AccesoPagina;
import com.textil.controlador.AuxiliarTextil.EstadoDocumento;
import com.textil.controlador.AuxiliarTextil.EstadoPagina;
import com.textil.controlador.ContenedorTextil;
import com.textil.dao.ClienteDAO;
import com.textil.dao.TransactionDelegate;
import com.toedter.calendar.JDateChooser;

public class DlgControlProducto extends JDialog implements InterfaceUsuario, PropertyChangeListener {

	/**
	 * Variables principales
	 */
	public static EstadoPagina estDlgPed = EstadoPagina.VISUALIZANDO;
	public EstadoPagina estDlgDetPed;
	public AccesoPagina acceso;
	public AuxiliarTextil man;
	public String padre;
	public boolean frmAbierto;
	private DefaultListModel modFiltro;
	HashMap mapReporte = new HashMap();
	protected int totalPiezas = 0;
	String sqlVista = "";
	/**
	 * 	
	 */
	private static int LR_PAGE_SIZE = 5;

	private DefaultListModel modLisCliNom;
	private DefaultListModel modLisModIde;
	private DefaultListModel modLisTalDes;
	private DefaultListModel modLisPrdCol;
	private JScrollPane scrLisCliNom;
	private JScrollPane scrLisModIde;
	private JScrollPane scrLisTalDes;
	private JScrollPane scrLisPrdCol;

	private JList lisCliNom;
	private JList lisModIde;
	private JList lisTalDes;
	private JList lisPrdCol;
	private boolean busLisCliNom;
	private boolean busLisModIde;
	private boolean busLisTalDes;
	private boolean busLisPrdCol;
	private List<String> varLisCliNom = new ArrayList<String>();
	private List<String> varLisModIde = new ArrayList<String>();
	private List<String> varLisTalDes = new ArrayList<String>();
	private List<String> varLisPrdCol = new ArrayList<String>();

	/*
	 * 
	 * 
	 * 
	 */
	private CeldaRenderGeneral modTabCellRender;
	private TableColumn[] colTabCtrlPrd;
	private JButton cmdCerrar;
	private JButton cmdPedImprimir;
	private ButtonGroup grupoOpcZona;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JPanel panFiltro;
	// private JTable tabCtrlPrd;
	// private ModeloTablaCtrlProducto modTabCtrlPrd;
	private JLabel lblGauge;
	private final JTextEditorFecha txtEditFecIni, txtEditFecFin;
	private JDateChooser cmbFecIni;
	private JDateChooser cmbFecFin;
	private int varPedCod, varCliCod;
	private JTextFieldChanged txtCliNom;
	private JTextFieldChanged txtTalDes;
	private JTextFieldChanged txtFicBar;
	private JTextFieldChanged txtPrdCol;
	private JLabel lblDocumento;
	private JComboBox cmbTipDoc;
	private JComboBox cmbAreNom;
	private JComboBox cmbSubAreNom;
	private JLabel lblEstado;
	private JComboBox cmbFicEst;
	private JButton cmdBuscar;
	private ButtonGroup grupoOpcProducto;
	protected JTextFieldFormatoEntero txtTotalRegistro;
	protected JCheckBox chkUltimo;
	private JTextFieldChanged txtModIde;
	private JComboBox cmbTipoImpresion;
	private PanelBrowseTablaProducto panBrwTabPrd;
	protected JTextFieldFormatoEntero txtPiezasPagina;
	public JTextFieldFormatoEntero txtTotalPiezas;

	public DlgControlProducto(FrmPrincipal parent) {
		super(parent);
		setResizable(false);
		setTitle("Control de Busquedad del Producto : "+this.getClass().getSimpleName());

		modLisCliNom = new DefaultListModel();
		modLisModIde = new DefaultListModel();
		modLisTalDes = new DefaultListModel();
		modLisPrdCol = new DefaultListModel();
		modFiltro = new DefaultListModel();

		txtEditFecIni = new JTextEditorFecha();
		txtEditFecIni.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditFecFin = new JTextEditorFecha();
		txtEditFecFin.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txtEditFecIni.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		txtEditFecFin.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());

		initComponents();

		crearModeloTablas();

		super.addWindowListener(new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				DlgControlProducto.this.frmAbierto = true;
			}
		});
	}

	private void initComponents() {
		this.grupoOpcZona = new ButtonGroup();
		this.grupoOpcProducto = new ButtonGroup();
		this.panFiltro = new JPanel();
		this.cmdCerrar = new JButton();
		this.cmdPedImprimir = new JButton();
		cmdPedImprimir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		setDefaultCloseOperation(2);
		getContentPane().setLayout(null);

		scrLisCliNom = new JScrollPane();
		scrLisCliNom.setBounds(15, 55, 178, 155);
		getContentPane().add(scrLisCliNom);

		lisCliNom = new JList(modLisCliNom);
		lisCliNom.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				lisCliNomMousePressed(arg0);

			}
		});
		lisCliNom.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				lisCliNomKeyReleased(arg0);

			}
		});
		scrLisCliNom.setViewportView(lisCliNom);

		scrLisModIde = new JScrollPane();
		scrLisModIde.setBounds(195, 55, 130, 155);
		getContentPane().add(scrLisModIde);

		lisModIde = new JList(modLisModIde);
		lisModIde.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				lisModIdeKeyReleased(arg0);

			}
		});
		lisModIde.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				lisModIdeMousePressed(arg0);

			}
		});
		scrLisModIde.setViewportView(lisModIde);

		scrLisPrdCol = new JScrollPane();
		scrLisPrdCol.setBounds(457, 55, 104, 155);
		getContentPane().add(scrLisPrdCol);

		lisPrdCol = new JList(modLisPrdCol);
		lisPrdCol.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				lisPrdColMousePressed(arg0);

			}
		});
		lisPrdCol.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				lisPrdColKeyReleased(arg0);

			}
		});
		scrLisPrdCol.setViewportView(lisPrdCol);

		scrLisTalDes = new JScrollPane();
		scrLisTalDes.setBounds(327, 55, 127, 155);
		getContentPane().add(scrLisTalDes);

		lisTalDes = new JList(modLisTalDes);
		lisTalDes.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				lisTalDesMousePressed(arg0);

			}
		});
		lisTalDes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				lisTalDesKeyReleased(arg0);

			}
		});
		scrLisTalDes.setViewportView(lisTalDes);

		this.panFiltro.setBorder(new TitledBorder(null, "Parametros de Busquedad", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.panFiltro.setLayout(null);

		getContentPane().add(this.panFiltro);
		this.panFiltro.setBounds(5, 5, 872, 109);

		JLabel lblClientes = new JLabel();
		lblClientes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblClientes.setText("Clientes");
		lblClientes.setHorizontalAlignment(SwingConstants.CENTER);
		lblClientes.setBounds(10, 15, 179, 15);
		panFiltro.add(lblClientes);

		JLabel lblTipo = new JLabel();
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipo.setText("Talla");
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo.setBounds(322, 15, 126, 15);
		panFiltro.add(lblTipo);

		lblGauge = new JLabel();
		lblGauge.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGauge.setText("Color");
		lblGauge.setHorizontalAlignment(SwingConstants.CENTER);
		lblGauge.setBounds(452, 15, 105, 15);
		panFiltro.add(lblGauge);

		txtCliNom = new JTextFieldChanged(50);
		txtCliNom.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliNom.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtCliNomKeyReleased(arg0);

			}
		});
		txtCliNom.setText("Hernan Mendoza Ticllahuanaco");
		txtCliNom.setBounds(10, 30, 179, 20);
		panFiltro.add(txtCliNom);

		txtTalDes = new JTextFieldChanged(50);
		txtTalDes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTalDes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtTalDesKeyReleased(arg0);

			}
		});
		txtTalDes.setText("Hernan Mendoza Ticllahuanaco");
		txtTalDes.setBounds(322, 30, 126, 20);
		panFiltro.add(txtTalDes);

		txtFicBar = new JTextFieldChanged(13);
		txtFicBar.setHorizontalAlignment(SwingConstants.CENTER);
		txtFicBar.setFont(new Font("Tahoma", Font.PLAIN, 25));
		txtFicBar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtFicBarKeyReleased(arg0);

			}
		});
		txtFicBar.setText("123456789123");
		txtFicBar.setBounds(554, 59, 198, 44);
		panFiltro.add(txtFicBar);

		txtPrdCol = new JTextFieldChanged(50);
		txtPrdCol.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPrdCol.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPrdColKeyReleased(arg0);

			}
		});
		txtPrdCol.setText("Hernan Mendoza Ticllahuanaco");
		txtPrdCol.setBounds(452, 30, 105, 20);
		panFiltro.add(txtPrdCol);

		lblDocumento = new JLabel();
		lblDocumento.setBounds(584, 10, 134, 20);
		panFiltro.add(lblDocumento);
		lblDocumento.setText("Documento");
		lblDocumento.setHorizontalAlignment(SwingConstants.CENTER);
		lblDocumento.setFont(new Font("Tahoma", Font.PLAIN, 14));

		cmbTipDoc = new JComboBox();
		cmbTipDoc.setBounds(584, 30, 134, 25);
		panFiltro.add(cmbTipDoc);
		cmbTipDoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbTipDoc.setModel(new DefaultComboBoxModel(new String[] { "TODOS", "NOTA DE PEDIDO", "NOTA DE PRUEBA" }));
		cmbTipDoc.setFont(new Font("Tahoma", Font.PLAIN, 13));

		lblEstado = new JLabel();
		lblEstado.setText("Estado Ficha");
		lblEstado.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEstado.setBounds(728, 10, 134, 20);
		panFiltro.add(lblEstado);

		cmbFicEst = new JComboBox();
		cmbFicEst.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbFicEst.setModel(new DefaultComboBoxModel(new String[] { "TODOS", "PENDIENTE", "PRODUCCION", "TERMINADO" }));
		cmbFicEst.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbFicEst.setBounds(728, 30, 134, 25);
		panFiltro.add(cmbFicEst);

		cmdBuscar = new JButton("Buscar");
		cmdBuscar.setForeground(new Color(0, 100, 0));
		cmdBuscar.setBounds(758, 58, 104, 45);
		panFiltro.add(cmdBuscar);
		cmdBuscar.setFont(new Font("Tahoma", Font.BOLD, 14));
		cmdBuscar.setMargin(new Insets(0, 0, 0, 0));
		cmdBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdBuscarActionPerformed(e);

			}
		});
		cmdBuscar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));
		this.jLabel4 = new JLabel();
		jLabel4.setBounds(255, 83, 95, 20);
		panFiltro.add(jLabel4);
		jLabel4.setFont(new Font("Tahoma", Font.PLAIN, 14));

		this.jLabel4.setHorizontalAlignment(0);
		this.jLabel4.setText("Fecha de Final");

		cmbSubAreNom = new JComboBox();
		cmbSubAreNom.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {

				cmbSubAreNomItemChanged(arg0);

			}
		});
		cmbSubAreNom.setBounds(78, 83, 141, 20);
		panFiltro.add(cmbSubAreNom);
		cmbSubAreNom.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbSubAreNom.setFont(new Font("Tahoma", Font.PLAIN, 13));

		cmbAreNom = new JComboBox();
		cmbAreNom.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {

				cmbAreNomItemChanged(arg0);

			}
		});
		cmbAreNom.setBounds(78, 55, 141, 20);
		panFiltro.add(cmbAreNom);
		cmbAreNom.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbAreNom.setFont(new Font("Tahoma", Font.PLAIN, 13));
		this.jLabel3 = new JLabel();
		jLabel3.setBounds(255, 55, 95, 20);
		panFiltro.add(jLabel3);
		jLabel3.setFont(new Font("Tahoma", Font.PLAIN, 14));

		this.jLabel3.setHorizontalAlignment(0);
		this.jLabel3.setText("Fecha de Inicio");

		cmbFecFin = new JDateChooser(txtEditFecFin);
		cmbFecFin.setDate(new Date());
		cmbFecFin.setBounds(369, 83, 109, 20);
		panFiltro.add(cmbFecFin);
		cmbFecFin.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbFecFin.setDateFormatString("dd/MM/yyyy");

		cmbFecIni = new JDateChooser(txtEditFecIni);
		cmbFecIni.setDate(new Date());
		cmbFecIni.setBounds(369, 55, 109, 20);
		panFiltro.add(cmbFecIni);
		cmbFecIni.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbFecIni.setDateFormatString("dd/MM/yyyy");

		JLabel lblArea = new JLabel();
		lblArea.setText("Area");
		lblArea.setHorizontalAlignment(SwingConstants.CENTER);
		lblArea.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblArea.setBounds(10, 55, 62, 20);
		panFiltro.add(lblArea);

		JLabel lblSubare = new JLabel();
		lblSubare.setText("SubArea");
		lblSubare.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubare.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSubare.setBounds(10, 83, 62, 20);
		panFiltro.add(lblSubare);

		txtModIde = new JTextFieldChanged(50);
		txtModIde.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtModIdeKeyReleased(arg0);

			}
		});
		txtModIde.setText("Hernan Mendoza Ticllahuanaco");
		txtModIde.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtModIde.setBounds(191, 30, 126, 20);
		panFiltro.add(txtModIde);

		JLabel lblModelo = new JLabel();
		lblModelo.setText("Modelo");
		lblModelo.setHorizontalAlignment(SwingConstants.CENTER);
		lblModelo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblModelo.setBounds(191, 15, 126, 15);
		panFiltro.add(lblModelo);

		this.cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		this.cmdCerrar.setText("Cerrar");
		this.cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		this.cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		this.cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DlgControlProducto.this.cmdCerrarActionPerformed(evt);
			}
		});
		getContentPane().add(this.cmdCerrar);
		this.cmdCerrar.setBounds(767, 574, 104, 40);

		this.cmdPedImprimir.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		this.cmdPedImprimir.setText("Imprimir");
		this.cmdPedImprimir.setMargin(new Insets(0, 0, 0, 0));
		this.cmdPedImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DlgControlProducto.this.cmdPedImprimirActionPerformed(evt);
			}
		});
		getContentPane().add(this.cmdPedImprimir);
		this.cmdPedImprimir.setBounds(7, 574, 117, 45);

		JLabel lblTotalPiezas = new JLabel();
		lblTotalPiezas.setForeground(new Color(139, 69, 19));
		lblTotalPiezas.setText("Total Registro");
		lblTotalPiezas.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalPiezas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTotalPiezas.setBounds(650, 572, 105, 23);
		getContentPane().add(lblTotalPiezas);

		txtTotalRegistro = new JTextFieldFormatoEntero(4);
		txtTotalRegistro.setForeground(new Color(139, 69, 19));
		txtTotalRegistro.setText("4");
		txtTotalRegistro.setHorizontalAlignment(SwingConstants.CENTER);
		txtTotalRegistro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTotalRegistro.setBounds(650, 597, 105, 22);
		getContentPane().add(txtTotalRegistro);

		chkUltimo = new JCheckBox("Ultimo Registro");
		chkUltimo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chkUltimo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		chkUltimo.setBounds(313, 574, 117, 40);
		getContentPane().add(chkUltimo);

		cmbTipoImpresion = new JComboBox();
		cmbTipoImpresion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbTipoImpresion.setModel(new DefaultComboBoxModel(new String[] { "Fichas General", "Fichas Area" }));
		cmbTipoImpresion.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cmbTipoImpresion.setBounds(126, 596, 167, 22);
		getContentPane().add(cmbTipoImpresion);

		JLabel lblTipoImpresion = new JLabel();
		lblTipoImpresion.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		lblTipoImpresion.setText("Tipo Impresion");
		lblTipoImpresion.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipoImpresion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipoImpresion.setBounds(126, 572, 167, 23);
		getContentPane().add(lblTipoImpresion);

		// Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		// setBounds((screenSize.width - 934) / 2, (screenSize.height - 556) / 2, 669, 507);
		setSize(new Dimension(886, 650));
		setLocationRelativeTo(null);
	}

	private void tabCtrlPrdMousePressed(MouseEvent evt) {
		if (panBrwTabPrd.tabCtrlPrd.getSelectedRow() > -1) {

			varPedCod = ((Integer) panBrwTabPrd.modTabCtrlPrd.getValueAt(panBrwTabPrd.tabCtrlPrd.getSelectedRow(), 16));// Codigo del pedido

			if (!panBrwTabPrd.tabCtrlPrd.isEditing()) {
				// this.cmdPedImprimir.setEnabled(true);
			}
		}
	}

	private void cmdCerrarActionPerformed(ActionEvent evt) {
		cerrar();
	}

	private void tabCtrlPrdKeyReleased(KeyEvent evt) {
		if ((evt.getKeyCode() == 38) || (evt.getKeyCode() == 40) || (evt.getKeyCode() == 33) || (evt.getKeyCode() == 34) || (evt.getKeyCode() == 10) || (evt.getKeyCode() == 113)
				|| (evt.getKeyCode() == 9)) {
			tabCtrlPrdMousePressed(null);
		}
	}

	private void tabCtrlPrdMouseClicked(MouseEvent evt) {
		if ((evt.getClickCount() == 2) && (panBrwTabPrd.tabCtrlPrd.getSelectedRow() > -1)) {

			DlgPedido.estDlgPed = EstadoPagina.VISUALIZANDO;
			DlgPedido.bookMarkPedCod = (Integer) panBrwTabPrd.tabCtrlPrd.getValueAt(panBrwTabPrd.tabCtrlPrd.getSelectedRow(), 16);// Codigo del pedido

			dispose();
			this.man.abrirPantalla("DlgPedido", "DlgControlProducto", AccesoPagina.INICIAR);
		}
	}

	private void cmdPedImprimirActionPerformed(ActionEvent evt) {
		if (panBrwTabPrd.tabCtrlPrd.getRowCount() > 0) {
			enviarImpresora();
		} else {
			JOptionPane.showConfirmDialog(null, "���NO EXISTE NINGUN REGISTRO A IMPRIMIR!!\n" + getClass().getName(), man.testProp.getPropiedad("TituloSistema"), -1, 1);
		}
	}

	public static void main(String[] args) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DlgControlProducto.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(DlgControlProducto.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(DlgControlProducto.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(DlgControlProducto.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				DlgControlProducto dialog = new DlgControlProducto(null);
				dialog.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	public void iniciarFormulario() {

		busLisCliNom = true;
		busLisModIde = true;
		busLisTalDes = true;
		busLisPrdCol = true;

		man.obtenerFechaHoraActual();

		if (this.acceso.equals(AccesoPagina.INICIAR)) {
			this.estDlgPed = EstadoPagina.VISUALIZANDO;

			this.frmAbierto = false;

			iniciarInstancias();
			limpiarInstancias(acceso.INICIAR);
			llenarFormulario();
			limpiarFormulario();
			activarFormulario(false);

		} else if (this.acceso.equals(AccesoPagina.ACTUALIZAR)) {

			llenarFormulario();
			limpiarFormulario();
			llenarTablaCtrlProducto(elaborarSqlCtrlProducto());

		}

		setVisible(true);
	}

	public void iniciarInstancias() {
		/*
		 * Obtiene una conexion del pool de conexiones de Apache
		 */
		man.transactionDelegate = (TransactionDelegate) ContenedorTextil.getComponent("TransactionDelegate");
		man.testProp = (TestPropiedades) ContenedorTextil.getComponent("TestPropiedades");
		man.funUsu = (FuncionesUsuario) ContenedorTextil.getComponent("FuncionesUsuario");

//		man.transactionDelegate.start();

		/*
		 * Obtiene las entidades del modelo relacional para su manejo en este
		 * formulario
		 */
		man.ped = (Pedido) ContenedorTextil.getComponent("Pedido");

		man.cli = (Cliente) ContenedorTextil.getComponent("Cliente");
		man.lisCli = (List<Cliente>) ContenedorTextil.getComponent("ListaCliente");

		man.gru = (Grupo) ContenedorTextil.getComponent("Grupo");
		man.cla = (Clase) ContenedorTextil.getComponent("Clase");

		man.lisGru = (List<Grupo>) ContenedorTextil.getComponent("ListaGrupo");
		man.lisCla = (List<Clase>) ContenedorTextil.getComponent("ListaClase");

		man.are = (Area) ContenedorTextil.getComponent("Area");
		man.subAre = (SubArea) ContenedorTextil.getComponent("SubArea");

		man.ped = (Pedido) ContenedorTextil.getComponent("Pedido");

		man.mod = (Producto) ContenedorTextil.getComponent("Modelo");
		man.prd = (Producto) ContenedorTextil.getComponent("Producto");
		man.tal = (Producto) ContenedorTextil.getComponent("Talla");
		man.fic = (Ficha) ContenedorTextil.getComponent("Ficha");

		man.lisMod = (List<Producto>) ContenedorTextil.getComponent("ListaModelo");
		man.lisTal = (List<Producto>) ContenedorTextil.getComponent("ListaTalla");

		man.prdGen = (Producto) ContenedorTextil.getComponent("ProductoGeneral");
		man.lisPrdGen = (List<Producto>) ContenedorTextil.getComponent("ListaProductoGeneral");

		man.lisAre = (List<Area>) ContenedorTextil.getComponent("ListaArea");
		man.lisSubAre = (List<SubArea>) ContenedorTextil.getComponent("ListaSubArea");

	}

	public void limpiarInstancias(AccesoPagina parAcceso) {
		if (parAcceso.equals(AccesoPagina.INICIAR) || parAcceso.equals(AccesoPagina.FINALIZAR)) {

			man.ped.limpiarInstancia();
			man.cli.limpiarInstancia();

			man.gru.limpiarInstancia();
			man.cla.limpiarInstancia();

			man.are.limpiarInstancia();
			man.subAre.limpiarInstancia();

			man.mod.limpiarInstancia();
			man.prd.limpiarInstancia();
			man.tal.limpiarInstancia();
			man.fic.limpiarInstancia();

			man.prdGen.limpiarInstancia();

		}// Fin de

		man.lisCli.clear();
		man.lisGru.clear();
		man.lisCla.clear();
		man.lisMod.clear();
		man.lisTal.clear();
		man.lisPrdGen.clear();
		man.lisAre.clear();
		man.lisSubAre.clear();
	}

	public void limpiarFormulario() {

		man.obtenerFechaActual();
		txtCliNom.setText("");
		txtModIde.setText("");
		txtTalDes.setText("");
		txtFicBar.setText("");
		txtPrdCol.setText("");
		txtTotalPiezas.setValue(0);
		txtTotalRegistro.setValue(0);
		// cmbFecIni.setDate(man.funUsu.from_SqlDate_To_UtilDate(man.fechaActual));
		// cmbFecFin.setDate(man.funUsu.from_SqlDate_To_UtilDate(man.fechaActual));

		this.grupoOpcProducto.clearSelection();
		panBrwTabPrd.modTabCtrlPrd.setRowCount(0);
	}

	public void generarNuevoRegistro() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void llenarFormulario() {

		man.obtenerListaProductoGeneral("TODOS");
		man.obtenerListaGrupo();
		man.obtenerListaClase();

		llenarComboArea();
		llenarComboSubArea();
	}

	public void activarFormulario(boolean parAct) {

		scrLisCliNom.setVisible(false);
		scrLisModIde.setVisible(false);
		scrLisTalDes.setVisible(false);
		scrLisPrdCol.setVisible(false);
		txtPiezasPagina.setEnabled(false);
		txtTotalPiezas.setEnabled(false);
		txtTotalRegistro.setEnabled(false);
		// cmdPedImprimir.setEnabled(parAct);

	}

	public void refrescarFormulario() {
	}

	public void obtenerDatoFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void obtenerDatoBaseDato() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public boolean validarFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public boolean guardarDatoBaseDato(String parModoGrabar) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void setManejador(AuxiliarTextil parMan) {
		this.man = parMan;
	}

	public void cerrar() {
		if (this.padre.equals("FrmPrincipal")) {
			int resMsg = JOptionPane.showConfirmDialog(this, "�Desea cerrar formulario?", "Sistema de Cobranza", 0, 3);

			if (resMsg == 0) {
				dispose();
				limpiarInstancias(AccesoPagina.FINALIZAR);
				DlgPedido.bookMarkPedCod = 0;

//				man.transactionDelegate.end();
//				try {
//					DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().commit();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
				man.abrirPantalla("FrmPrincipal", "FrmPrincipal", AccesoPagina.ACTUALIZAR);
			}

		} else {
			dispose();
			this.man.abrirPantalla(this.padre, "FrmPrincipal", AccesoPagina.ACTUALIZAR);
		}
	}

	public String elaborarSqlCtrlProducto() {

		String sql = "";
		int modeloCodigo = 0;

		mapReporte.clear();

		if (!txtFicBar.getText().equals("")) {

			man.fic.setObjeto(man.obtenerFichaBarra(txtFicBar.getText().trim()));
			man.tal.setObjeto(man.obtenerProducto(man.fic.getPrd_cod()));
			man.prd.setObjeto(man.obtenerProducto(man.tal.getPrd_supdos()));
			man.mod.setObjeto(man.obtenerProducto(man.tal.getPrd_supuno()));

		} else {

			man.fic.limpiarInstancia();
			man.tal.limpiarInstancia();
			man.prd.limpiarInstancia();
			man.mod.limpiarInstancia();

		}

		if (!txtModIde.getText().equals("")) {
			if (!txtModIde.getText().equals("*")) {

				modeloCodigo = man.obtenerModeoloCodigo(txtModIde.getText());

			}
		}
		if (this.cmbFicEst.getSelectedIndex() > -1) {
			if (this.cmbFicEst.getSelectedItem().toString().equals(EstadoDocumento.TERMINADO.getVal())) {
				sqlVista = "viewFichaProducto";
			} else {
				sqlVista = "viewFichaPedido";
			}
		}

		sql = "select * from " + sqlVista + " where 1=1 ";

		sql = sql + " and ped_nom like '%" + this.txtCliNom.getText() + "%' ";
		mapReporte.put("PED_NOM", "%" + this.txtCliNom.getText() + "%");

		if (this.cmbTipDoc.getSelectedIndex() > -1) {
			if (!this.cmbTipDoc.getSelectedItem().toString().equals("TODOS")) {

				sql = sql + " and ped_nomdoc like '%" + this.cmbTipDoc.getSelectedItem().toString() + "%' ";
				mapReporte.put("PED_NOMDOC", "%" + this.cmbTipDoc.getSelectedItem().toString() + "%");

			} else {
				sql = sql + " and ped_nomdoc like '%%' ";
				mapReporte.put("PED_NOMDOC", "%%");
			}
		}
		if (this.cmbAreNom.getSelectedIndex() > -1) {
			if (!this.cmbAreNom.getSelectedItem().toString().equals("TODOS")) {

				man.are.setObjeto(man.obtenerArea(cmbAreNom.getSelectedItem().toString()));
				sql = sql + " and are_cod like '" + man.are.getAre_cod() + "' ";
				mapReporte.put("ARE_COD", String.valueOf(man.are.getAre_cod()));

			} else {
				sql = sql + " and are_cod like '%%' ";
				mapReporte.put("ARE_COD", "%%");
			}
		}
		if (this.cmbSubAreNom.getSelectedIndex() > -1) {
			if (!this.cmbSubAreNom.getSelectedItem().toString().equals("TODOS")) {

				man.subAre.setObjeto(man.obtenerSubArea(cmbSubAreNom.getSelectedItem().toString()));
				sql = sql + " and subare_cod like '" + man.subAre.getSubare_cod() + "' ";
				mapReporte.put("SUBARE_COD", String.valueOf(man.subAre.getSubare_cod()));

			} else {
				sql = sql + " and subare_cod like '%%' ";
				mapReporte.put("SUBARE_COD", "%%");
			}
		}

		sql = sql + " and prd_supuno like '" + ((modeloCodigo > 0) ? modeloCodigo : "%%") + "' ";
		mapReporte.put("PRD_SUPUNO", ((modeloCodigo > 0) ? String.valueOf(modeloCodigo) : "%%"));

		sql = sql + " and prd_des like '%" + this.txtTalDes.getText() + "%' ";
		mapReporte.put("PRD_DES", "%" + this.txtTalDes.getText() + "%");

		sql = sql + " and prd_col like '%" + this.txtPrdCol.getText() + "%' ";
		mapReporte.put("PRD_COL", "%" + this.txtPrdCol.getText() + "%");

		sql = sql + " and fic_bar like '%" + this.txtFicBar.getText() + "%' ";
		mapReporte.put("FIC_BAR", "%" + this.txtFicBar.getText() + "%");

		if (this.cmbFicEst.getSelectedIndex() > -1) {
			if (!this.cmbFicEst.getSelectedItem().toString().equals("TODOS")) {

				sql = sql + " and fic_estado like '%" + cmbFicEst.getSelectedItem().toString() + "%' ";
				mapReporte.put("FIC_ESTADO", "%" + cmbFicEst.getSelectedItem().toString() + "%");

			} else {
				sql = sql + " and fic_estado like '%%' ";
				mapReporte.put("FIC_ESTADO", "%%");
			}

		}
		if (cmbFecIni.getDate() != null && cmbFecFin.getDate() != null) {
			if (cmbFecIni.getDate().compareTo(cmbFecFin.getDate()) == 1) {
				cmbFecIni.setDate(man.fechaActual);
				cmbFecFin.setDate(man.fechaActual);
			}
		} else {
			cmbFecIni.setDate(man.fechaActual);
			cmbFecFin.setDate(man.fechaActual);
		}

		String fecIni = (this.cmbFecIni.getDate().getYear() + 1900) + "-" + (this.cmbFecIni.getDate().getMonth() + 1) + "-" + this.cmbFecIni.getDate().getDate();
		String fecFin = (this.cmbFecFin.getDate().getYear() + 1900) + "-" + (this.cmbFecFin.getDate().getMonth() + 1) + "-" + this.cmbFecFin.getDate().getDate();

		sql = sql + " and (fic_fecing >= '" + fecIni + "' and fic_fecing <= '" + fecFin + "') ";
		mapReporte.put("FEC_INI", cmbFecIni.getDate());
		mapReporte.put("FEC_FIN", cmbFecFin.getDate());
//		mapReporte.put("FEC_ACT", man.funUsu.from_SqlDate_To_UtilDate(man.fechaActual));

		sql = sql + " order by prd_col asc, dbo.funGetFillZero(prd_des,5) asc, fic_ide asc";

		return sql;
	}

	// private void crearModeloTablaCtrlProducto() {
	// this.modTabCtrlPrd = new ModeloTablaCtrlProducto();
	// this.tabCtrlPrd.setModel(this.modTabCtrlPrd);
	// this.modTabCellRender = new CeldaRenderGeneral();
	// try {
	// this.tabCtrlPrd.setDefaultRenderer(Class.forName("java.lang.String"), this.modTabCellRender);
	// this.tabCtrlPrd.setDefaultRenderer(Class.forName("java.lang.Integer"), this.modTabCellRender);
	// this.tabCtrlPrd.setDefaultRenderer(Class.forName("java.lang.Double"), this.modTabCellRender);
	// } catch (ClassNotFoundException ex) {
	// Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
	// }
	//
	// this.tabCtrlPrd.setSelectionMode(1);
	// this.tabCtrlPrd.getTableHeader().setReorderingAllowed(false);
	// this.tabCtrlPrd.setAutoResizeMode(0);
	// this.tabCtrlPrd.setShowGrid(true);
	//
	// String[] header = { "Area", "SubArea", "Cliente", "Modelo", "Tipo", "Articulo", "Color", "Talla", "Ficha", "Fecha", "Estado", "Pcs", "fic_cod", "subare_cod", "are_cod", "detped_cod", "ped_cod", "prd_cod", "gru_cod" };
	// // String[] tipo = { "Date", "String", "String", "String", "String", "String", "String", "String", "Double", "String" };
	//
	// int[] ancho = { 90, 110, 180, 85, 95, 160, 90, 50, 45, 80, 100, 40, 0, 0, 0, 0, 0, 0, 0 };
	//
	// this.colTabCtrlPrd = new TableColumn[header.length];
	//
	// for (int iteHead = 0; iteHead < header.length; iteHead++) {
	// this.colTabCtrlPrd[iteHead] = this.tabCtrlPrd.getColumn(header[iteHead]);
	// this.colTabCtrlPrd[iteHead].setResizable(true);
	// this.colTabCtrlPrd[iteHead].setPreferredWidth(ancho[iteHead]);
	// // colTabCtrlPrd[iteHead].addPropertyChangeListener(new ListenerCambioPropiedad());
	//
	// if (ancho[iteHead] == 0) {
	// this.tabCtrlPrd.getColumnModel().getColumn(iteHead).setMaxWidth(0);
	// this.tabCtrlPrd.getColumnModel().getColumn(iteHead).setMinWidth(0);
	// this.tabCtrlPrd.getTableHeader().getColumnModel().getColumn(iteHead).setMaxWidth(0);
	// this.tabCtrlPrd.getTableHeader().getColumnModel().getColumn(iteHead).setMinWidth(0);
	// }
	// }
	// }

	public void llenarTablaCtrlProducto(String parSql) {

		modFiltro.removeAllElements();
		// this.cmdPedImprimir.setEnabled(false);
		List listaVista = this.man.obtenerListaCtrlProducto(parSql);
		panBrwTabPrd.crearTablaResultado(listaVista);

		// Object[] record = { "", "", "", "", "", "", "", "", "", "", "", 0, 0, 0, 0, 0, 0, 0, 0 };
		//
		// this.modTabCtrlPrd.setRowCount(0);
		// // this.modTabCtrlPrd.setRowCount(listaVista.size());
		// int numIte = 0;
		// int total = 0;
		// boolean agregar = true;
		// // if (listaVista.size() <= 10000) {
		// if (true) {
		// for (int ite = 0; ite < listaVista.size(); ite++) {
		//
		// String[] regTab = ((String) listaVista.get(ite)).split("#");
		// man.tal.setObjeto(man.obtenerItemListaProductoGeneral(Integer.parseInt(regTab[17])));
		// man.mod.setObjeto(man.obtenerItemListaProductoGeneral(man.tal.getPrd_supuno()));
		// man.prd.setObjeto(man.obtenerItemListaProductoGeneral(man.tal.getPrd_supdos()));
		// man.are.setObjeto(man.obtenerArea(Integer.parseInt(regTab[0])));
		// man.subAre.setObjeto(man.obtenerSubArea(Integer.parseInt(regTab[1])));
		//
		// if (chkUltimo.isSelected() && (ite + 1) < listaVista.size()) {
		// agregar = false;
		// } else {
		// agregar = true;
		// }
		// if (agregar) {
		//
		// this.modTabCtrlPrd.addRow(record);
		// this.modTabCtrlPrd.setValueAt(man.are.getAre_nom(), numIte, 0); // String
		// this.modTabCtrlPrd.setValueAt(man.subAre.getSubare_nom(), numIte, 1); // String
		// this.modTabCtrlPrd.setValueAt(regTab[2], numIte, 2); // String
		// this.modTabCtrlPrd.setValueAt(man.mod.getPrd_ide(), numIte, 3); // String
		// this.modTabCtrlPrd.setValueAt(regTab[4], numIte, 4); // String
		// this.modTabCtrlPrd.setValueAt(man.prd.getPrd_ide(), numIte, 5); // String
		// this.modTabCtrlPrd.setValueAt(regTab[6], numIte, 6); // String
		// this.modTabCtrlPrd.setValueAt(man.tal.getPrd_des(), numIte, 7); // String
		// this.modTabCtrlPrd.setValueAt(regTab[8], numIte, 8); // String
		// this.modTabCtrlPrd.setValueAt(man.funUsu.fromDateToString(man.funUsu.fromStrdateToDateSql(regTab[9]), "dd/MM/yyyy"), numIte, 9); // String Fecha de Ingreso
		// this.modTabCtrlPrd.setValueAt(regTab[10], numIte, 10); // String
		// this.modTabCtrlPrd.setValueAt(Integer.parseInt(regTab[11]), numIte, 11); // Integer
		// this.modTabCtrlPrd.setValueAt(Integer.parseInt(regTab[12]), numIte, 12); // Integer
		// this.modTabCtrlPrd.setValueAt(Integer.parseInt(regTab[13]), numIte, 13); // Integer
		// this.modTabCtrlPrd.setValueAt(Integer.parseInt(regTab[14]), numIte, 14); // Integer
		// this.modTabCtrlPrd.setValueAt(Integer.parseInt(regTab[15]), numIte, 15); // Integer
		// this.modTabCtrlPrd.setValueAt(Integer.parseInt(regTab[16]), numIte, 16); // Integer
		// this.modTabCtrlPrd.setValueAt(Integer.parseInt(regTab[17]), numIte, 17); // Integer
		// this.modTabCtrlPrd.setValueAt(Integer.parseInt(regTab[18]), numIte, 18); // Integer
		//
		// total = total + Integer.parseInt(regTab[11]);
		// numIte++;
		// }
		// }
		// txtTotal.setValue(total);
		// }// if(listaVista.size() <= 10000)
		// else {
		// JOptionPane.showConfirmDialog(null, "El numero de registros excede el limite permitido 10,000 registros\n Aplique filtros para poder visualizar todos los registros", "Sistema de Produccion", -1, 1);
		// }

	}

	private void crearModeloTablas() {

		// crearModeloTablaCtrlProducto();
		panBrwTabPrd = new PanelBrowseTablaProducto(this, 7, 112, 867, 457);
		getContentPane().add(this.panBrwTabPrd);

		JLabel lblTotalPiezasX = new JLabel();
		lblTotalPiezasX.setText("Piezas x Pagina");
		lblTotalPiezasX.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalPiezasX.setForeground(new Color(139, 69, 19));
		lblTotalPiezasX.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTotalPiezasX.setBounds(436, 573, 105, 23);
		getContentPane().add(lblTotalPiezasX);

		txtPiezasPagina = new JTextFieldFormatoEntero(4);
		txtPiezasPagina.setText("4");
		txtPiezasPagina.setHorizontalAlignment(SwingConstants.CENTER);
		txtPiezasPagina.setForeground(new Color(139, 69, 19));
		txtPiezasPagina.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPiezasPagina.setBounds(436, 597, 105, 22);
		getContentPane().add(txtPiezasPagina);
		
		JLabel label = new JLabel();
		label.setText("Total Piezas");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(new Color(139, 69, 19));
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(543, 573, 105, 23);
		getContentPane().add(label);
		
		txtTotalPiezas = new JTextFieldFormatoEntero(4);
		txtTotalPiezas.setText("4");
		txtTotalPiezas.setHorizontalAlignment(SwingConstants.CENTER);
		txtTotalPiezas.setForeground(new Color(139, 69, 19));
		txtTotalPiezas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTotalPiezas.setBounds(543, 597, 105, 22);
		getContentPane().add(txtTotalPiezas);

		panBrwTabPrd.getTabCtrlPrd().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				DlgControlProducto.this.tabCtrlPrdMousePressed(evt);
			}

			public void mouseClicked(MouseEvent evt) {
				DlgControlProducto.this.tabCtrlPrdMouseClicked(evt);
			}
		});
		panBrwTabPrd.getTabCtrlPrd().addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent evt) {
				DlgControlProducto.this.tabCtrlPrdKeyReleased(evt);
			}
		});

	}

	public void enviarImpresora() {
		File repFactura = null;

		if (cmbTipoImpresion.getSelectedItem().toString().equals("Fichas General")) {
			if (sqlVista.equals("viewFichaProducto")) {
				repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "ReporteFichaProducto.jasper");
			}
			else if (sqlVista.equals("viewFichaPedido")) {
				repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "ReporteFichaPedido.jasper");
			}
		} else if (cmbTipoImpresion.getSelectedItem().toString().equals("Fichas Area")) {
			if (sqlVista.equals("viewFichaProducto")) {
				repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "rep_fic_prod_area.jasper");
			}
			else if (sqlVista.equals("viewFichaPedido")) {
				repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "rep_fic_ped_area.jasper");
			}
		}
		if (repFactura != null) {
			try {
				InputStream inpStrRepFac = new FileInputStream(repFactura);

				try {
					JasperPrint print = JasperFillManager.fillReport(inpStrRepFac, mapReporte, DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection());

					JasperViewer jv = new JasperViewer(print, false);
					jv.setTitle("Reporte de Pedidos");
					jv.setSize(800, 600);
					jv.setVisible(true);

					// JasperPrintManager.printReport(print, true);
				} catch (JRException ex) {
					JOptionPane.showConfirmDialog(null, "Error de conexion con la impresora", "Sistema de Facturacion", -1, 1);
				}
			} catch (FileNotFoundException ex) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	private void cmdBuscarActionPerformed(ActionEvent e) {

		// Map requestParameters = new HashMap();
		// requestParameters.put("manejador", man);
		// requestParameters.put("formulario", "DlgControlProducto");
		// requestParameters.put("accion", "llenarTabla");

		// SwingWorkerJava swiWorkJava = new SwingWorkerJava(requestParameters);
		// swiWorkJava.addPropertyChangeListener(this);
		// swiWorkJava.execute();

		man.requestParameters.put("formulario", "DlgControlProducto");
		man.requestParameters.put("accion", "llenarTabla");

		man.ejecutarProceso();

		// llenarTablaCtrlProducto(elaborarSqlCtrlProducto());

	}

	public void llenarTabla() {

		llenarTablaCtrlProducto(elaborarSqlCtrlProducto());

	}

	public void propertyChange(PropertyChangeEvent evt) {

		System.out.println("progreso : " + evt.getPropertyName());

		if ("progress".equals(evt.getPropertyName())) {

			System.out.println("Termino el Hilo");

		}
	}

	/**
	 * Configuracion de la lista desplegable de cliente
	 */

	private void txtCliNomKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtCliNom.getText().trim().length() == 0) {

			scrLisCliNom.setVisible(false);
			lisCliNom.setVisible(false);
			busLisCliNom = true;

		} else if (txtCliNom.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisCliNom = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisCliNom.size() > 0) {

					scrLisCliNom.setVisible(true);
					lisCliNom.setVisible(true);
					lisCliNom.requestFocus();
					lisCliNom.setSelectedIndex(0);
					busLisCliNom = true;

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisCliNomMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisCliNom == true)) {

				modLisCliNom.removeAllElements();
				varLisCliNom = man.obtenerListaNombreCliente(txtCliNom.getText());
				for (int iteLis = 0; iteLis < varLisCliNom.size(); iteLis++) {
					modLisCliNom.addElement(varLisCliNom.get(iteLis));
				}

				varLisCliNom = man.obtenerListaNombreCliente(txtCliNom.getText().toUpperCase());
				for (String iteLisCliNom : varLisCliNom) {
					if (!modLisCliNom.contains(iteLisCliNom)) {
						modLisCliNom.addElement(iteLisCliNom);
					}
				}
				if (varLisCliNom.size() > 0) {

					scrLisCliNom.setVisible(true);
					lisCliNom.setVisible(true);
					busLisCliNom = true;

				} else {

					scrLisCliNom.setVisible(false);
					lisCliNom.setVisible(false);
					busLisCliNom = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisCliNomMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisCliNom.setVisible(false);
		lisCliNom.setVisible(false);
		txtCliNom.setText((String) lisCliNom.getSelectedValue());

		ClienteDAO cliDAO = (ClienteDAO) ContenedorTextil.getComponent("ClienteDAO");
		man.cli.setObjeto(cliDAO.leerPersonaNombre(txtCliNom.getText().trim()));

		// llenarDlgCliente();
		//
	}// GEN-LAST:event_lisPerNomMousePressed

	private void lisCliNomKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			lisCliNom.setVisible(false);
			txtCliNom.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisCliNomMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	/*
	 * 
	 * fin de configuracionde lista de cliente
	 */

	/**
	 * 
	 * @param Configuracion
	 *            de modelo de identificador
	 */
	private void txtModIdeKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtModIde.getText().trim().length() == 0) {

			scrLisModIde.setVisible(false);
			lisModIde.setVisible(false);
			busLisModIde = true;

		} else if (txtModIde.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisModIde = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisModIde.size() > 0) {

					scrLisModIde.setVisible(true);
					lisModIde.setVisible(true);
					busLisModIde = false;
					lisModIde.requestFocus();
					lisModIde.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisModIdeMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisModIde == true)) {

				modLisModIde.removeAllElements();
				varLisModIde = man.obtenerListaModeloIde(txtModIde.getText());
				for (int iteLis = 0; iteLis < varLisModIde.size(); iteLis++) {
					modLisModIde.addElement(varLisModIde.get(iteLis));
				}

				varLisModIde = man.obtenerListaModeloIde(txtModIde.getText());
				for (int iteLis = 0; iteLis < varLisModIde.size(); iteLis++) {
					if (!modLisModIde.contains(varLisModIde.get(iteLis))) {
						modLisModIde.addElement(varLisModIde.get(iteLis));
					}
				}
				if (varLisModIde.size() > 0) {

					scrLisModIde.setVisible(true);
					lisModIde.setVisible(true);
					busLisModIde = true;

				} else {

					scrLisModIde.setVisible(false);
					lisModIde.setVisible(false);
					busLisModIde = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisModIdeMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisModIde.setVisible(false);
		lisModIde.setVisible(false);
		if (lisModIde.getSelectedIndex() > -1) {

			txtModIde.setText((String) lisModIde.getSelectedValue());

		}
	}// GEN-LAST:event_lisPerNomMousePressed

	private void lisModIdeKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			lisModIde.setVisible(false);
			txtModIde.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisModIdeMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	/*
	 * 
	 * Fin de configuracion de modelo identificador
	 */

	/**
	 * 
	 * Configuracion del Tipo de Modelo
	 * 
	 * @param evt
	 */
	private void txtTalDesKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtTalDes.getText().trim().length() == 0) {

			scrLisTalDes.setVisible(false);
			lisTalDes.setVisible(false);
			busLisTalDes = true;

		} else if (txtTalDes.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisTalDes = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisTalDes.size() > 0) {

					scrLisTalDes.setVisible(true);
					lisTalDes.setVisible(true);
					busLisTalDes = false;
					lisTalDes.requestFocus();
					lisTalDes.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisTalDesMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisTalDes == true)) {

				modLisTalDes.removeAllElements();
				varLisTalDes = man.obtenerListaTallaDes(txtTalDes.getText());
				for (String iteLis : varLisTalDes) {
					modLisTalDes.addElement(iteLis);
				}

				varLisTalDes = man.obtenerListaTallaDes(txtTalDes.getText());
				for (String iteLis : varLisTalDes) {
					if (!modLisTalDes.contains(iteLis)) {
						modLisTalDes.addElement(iteLis);
					}
				}
				if (varLisTalDes.size() > 0) {

					scrLisTalDes.setVisible(true);
					lisTalDes.setVisible(true);
					busLisTalDes = true;

				} else {

					scrLisTalDes.setVisible(false);
					lisTalDes.setVisible(false);
					busLisTalDes = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisTalDesMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisTalDes.setVisible(false);
		lisTalDes.setVisible(false);
		if (lisTalDes.getSelectedIndex() > -1) {

			txtTalDes.setText((String) lisTalDes.getSelectedValue());

		}
	}// GEN-LAST:event_lisPerNomMousePressed

	private void lisTalDesKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			lisTalDes.setVisible(false);
			txtTalDes.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisTalDesMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	/**
	 * Configuracio del color del producto
	 * 
	 * @param evt
	 */
	private void lisPrdColKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			scrLisPrdCol.setVisible(false);
			txtPrdCol.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisPrdColMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	private void txtPrdColKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtPrdCol.getText().trim().length() == 0) {

			scrLisPrdCol.setVisible(false);
			lisPrdCol.setVisible(false);
			busLisPrdCol = true;

		} else if (txtPrdCol.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisPrdCol = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisPrdCol.size() > 0) {

					scrLisPrdCol.setVisible(true);
					lisPrdCol.setVisible(true);
					busLisPrdCol = false;
					lisPrdCol.requestFocus();
					lisPrdCol.setSelectedIndex(0);

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisPrdColMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisPrdCol == true)) {

				modLisPrdCol.removeAllElements();
				varLisPrdCol = man.obtenerListaClase("Color", txtPrdCol.getText());
				for (int iteLis = 0; iteLis < varLisPrdCol.size(); iteLis++) {
					String[] record = varLisPrdCol.get(iteLis).split("#");
					modLisPrdCol.addElement(record[2]);
				}

				varLisPrdCol = man.obtenerListaClase("Color", txtPrdCol.getText());
				for (int iteLis = 0; iteLis < varLisPrdCol.size(); iteLis++) {
					String[] record = varLisPrdCol.get(iteLis).split("#");
					if (!modLisPrdCol.contains(record[2])) {
						modLisPrdCol.addElement(record[2]);
					}
				}
				if (varLisPrdCol.size() > 0) {

					scrLisPrdCol.setVisible(true);
					lisPrdCol.setVisible(true);
					busLisPrdCol = true;

				} else {

					scrLisPrdCol.setVisible(false);
					lisPrdCol.setVisible(false);
					busLisPrdCol = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisPrdColMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisPrdCol.setVisible(false);
		lisPrdCol.setVisible(false);
		if (lisPrdCol.getSelectedIndex() > -1) {

			txtPrdCol.setText((String) lisPrdCol.getSelectedValue());

		}
	}// GEN-LAST:event_lisPerNomMousePressed

	private void txtFicBarKeyReleased(KeyEvent evt) {
		txtFicBar.setText(txtFicBar.getText().trim());
		if (txtFicBar.getText().trim().length() == 13) {

			txtFicBar.setEditable(false);

			txtFicBar.setText(txtFicBar.getText().trim().substring(0, 12));
			llenarTablaCtrlProducto(elaborarSqlCtrlProducto());
			txtFicBar.setText("");

			txtFicBar.setEditable(true);

		}
	}

	private void opcModeloActionPerformed(ActionEvent arg0) {
		// cmbAreNom.setEnabled(true);
		// cmbSubAreNom.setEnabled(true);
		// txtFicBar.setEnabled(true);
		// txtFicBar.requestFocus();
	}

	private void cmbAreNomItemChanged(ItemEvent evt) {
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			if (frmAbierto) {
				if (cmbAreNom.getSelectedIndex() > -1) {

					frmAbierto = false;
					cmbSubAreNom.setSelectedIndex(0);
					frmAbierto = true;

				}// /Fin de if (cmbKarCnx.getSelectedIndex() > -1)
			}// Fin de if(frmAbierto){
		}// Fin de if (evt.getStateChange() == ItemEvent.SELECTED)
	}

	private void cmbSubAreNomItemChanged(ItemEvent evt) {
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			if (frmAbierto) {
				if (cmbSubAreNom.getSelectedIndex() > -1) {

					frmAbierto = false;
					cmbAreNom.setSelectedIndex(0);
					frmAbierto = true;

				}// /Fin de if (cmbKarCnx.getSelectedIndex() > -1)
			}// Fin de if(frmAbierto){
		}// Fin de if (evt.getStateChange() == ItemEvent.SELECTED)
	}

	private void llenarComboArea() {

		this.cmbAreNom.removeAllItems();
		this.cmbAreNom.addItem("TODOS");

		man.obtenerListaAreaControl();
		for (Area iteAre : man.lisAre) {

			this.cmbAreNom.addItem(iteAre.getAre_nom());
		}

	}

	private void llenarComboSubArea() {

		this.cmbSubAreNom.removeAllItems();
		this.cmbSubAreNom.addItem("TODOS");

		man.obtenerListaSubArea();
		for (SubArea iteSubAre : man.lisSubAre) {

			this.cmbSubAreNom.addItem(iteSubAre.getSubare_nom());
		}
	}
}
