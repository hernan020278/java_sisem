package com.textil.main;


import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.comun.entidad.Cliente;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.MoverVentanaJComponent;
import com.comun.utilidad.VentanaImagen;
import com.sis.main.SisemAsistenciaListener;
import com.sis.main.SisemContextListener;
import com.textil.controlador.AuxiliarTextil;
import com.textil.controlador.AuxiliarTextil.AccesoPagina;
import com.textil.controlador.AuxiliarTextil.EstadoPagina;
import com.textil.controlador.ContenedorTextil;
import com.textil.dao.TransactionDelegate;

@SuppressWarnings("serial")
public class FrmPrincipal extends javax.swing.JFrame implements InterfaceUsuario {

	public String padre;
	public AccesoPagina acceso;
	private AuxiliarTextil man;
	private EstadoPagina estFrmPrin;
	/*
	 * Variables de formaulario
	 */
	private JComponent contenedor;
	private JPanel panelPrincipal;

	// Variable del formulario Principal
	public FrmPrincipal() {

		contenedor = new VentanaImagen(AplicacionGeneral.getInstance().obtenerImagen("fondo_principal.png"));

		this.setUndecorated(true);
		this.setContentPane(contenedor);
//		AWTUtilities.setWindowOpaque(this, false);
        this.setOpacity(1.0f);
		this.getRootPane().setOpaque(false);
		MoverVentanaJComponent moverVentana = new MoverVentanaJComponent(contenedor);
		this.addMouseListener(moverVentana);
		this.addMouseMotionListener(moverVentana);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		this.getContentPane().setLayout(new BorderLayout());
		this.setSize(new java.awt.Dimension(810, 597));
		this.setLocationRelativeTo(null);

		initComponents();

	}

	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {
		cmdCerrar = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

		panelPrincipal = new JPanel();
		panelPrincipal.setLayout(null);
		panelPrincipal.setOpaque(false);
		getContentPane().add(panelPrincipal);

		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonSalirDown.png")); // NOI18N
		cmdCerrar.setBorderPainted(false);
		cmdCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdCerrar.setOpaque(false);
		cmdCerrar.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonSalirDown.png")); // NOI18N
		cmdCerrar.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonSalirUp.png")); // NOI18N
		cmdCerrar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdCerrarActionPerformed(evt);
			}
		});

		cmdInicio = new javax.swing.JButton();
		cmdInicio.setBounds(10, 490, 100, 100);
		panelPrincipal.add(cmdInicio);

		cmdInicio.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonInicioDown.png")); // NOI18N
		cmdInicio.setBorderPainted(false);
		cmdInicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdInicio.setOpaque(false);
		cmdInicio.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonInicioDown.png")); // NOI18N
		cmdInicio.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonInicioUp.png")); // NOI18N
		cmdInicio.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				cmdInicioActionPerformed(evt);

			}
		});
		panelPrincipal.add(cmdCerrar);
		cmdCerrar.setBounds(700, 490, 100, 100);
		panMenuInicio = new javax.swing.JPanel();
		panMenuInicio.setOpaque(false);
		cmdOperaciones = new javax.swing.JButton();
		cmdAdministracion = new javax.swing.JButton();
		panMenuInicio.setLayout(null);

		cmdOperaciones.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
		cmdOperaciones.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonArchivoDown.png")); // NOI18N
		cmdOperaciones.setText("Operaciones");
		cmdOperaciones.setBorderPainted(false);
		cmdOperaciones.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdOperaciones.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		cmdOperaciones.setOpaque(false);
		cmdOperaciones.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonOperacionDown.png")); // NOI18N
		cmdOperaciones.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonArchivoUp.png")); // NOI18N
		cmdOperaciones.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
		cmdOperaciones.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		cmdOperaciones.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdOperacionesActionPerformed(evt);
			}
		});
		panMenuInicio.add(cmdOperaciones);
		cmdOperaciones.setBounds(600, 254, 140, 130);

		cmdAdministracion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
		cmdAdministracion.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonAdministracionDown.png")); // NOI18N
		cmdAdministracion.setText("Administracion");
		cmdAdministracion.setBorderPainted(false);
		cmdAdministracion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdAdministracion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		cmdAdministracion.setOpaque(false);
		cmdAdministracion.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonAdministracionDown.png")); // NOI18N
		cmdAdministracion.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonAdministracionUp.png")); // NOI18N
		cmdAdministracion.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
		cmdAdministracion.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		cmdAdministracion.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdAdministracionActionPerformed(evt);
			}
		});
		panMenuAdministracion = new javax.swing.JPanel();
		panMenuAdministracion.setOpaque(false);
		cmdAreas = new javax.swing.JButton();
		cmdPedido = new javax.swing.JButton();
		panMenuAdministracion.setLayout(null);

		cmdAreas.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
		cmdAreas.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonReporteDown.png")); // NOI18N
		cmdAreas.setText("Area/SubArea");
		cmdAreas.setToolTipText("");
		cmdAreas.setBorderPainted(false);
		cmdAreas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdAreas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		cmdAreas.setOpaque(false);
		cmdAreas.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonReporteDown.png")); // NOI18N
		cmdAreas.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonReporteUp.png")); // NOI18N
		cmdAreas.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
		cmdAreas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		cmdAreas.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdReporteActionPerformed(evt);
			}
		});
		panMenuAdministracion.add(cmdAreas);
		cmdAreas.setBounds(575, 253, 139, 130);

		cmdPedido.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
		cmdPedido.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonAtencionDown.png")); // NOI18N
		cmdPedido.setText("Pedidos");
		cmdPedido.setToolTipText("");
		cmdPedido.setBorderPainted(false);
		cmdPedido.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdPedido.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		cmdPedido.setOpaque(false);
		cmdPedido.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonAtencionDown.png")); // NOI18N
		cmdPedido.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonAtencionUp.png")); // NOI18N
		cmdPedido.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
		cmdPedido.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		cmdPedido.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdPedidoActionPerformed(evt);
			}
		});
		
		logo = new JLabel();
		logo.setBounds(336, 249, 189, 164);
		panelPrincipal.add(logo);
		logo.setIcon(AplicacionGeneral.getInstance().obtenerImagen("logo_principal.png"));
		panMenuAdministracion.add(cmdPedido);
		cmdPedido.setBounds(55, 266, 130, 130);

		panelPrincipal.add(panMenuAdministracion);
		panMenuAdministracion.setBounds(0, 0, 810, 597);

		cmdControlProduccion = new JButton();
		cmdControlProduccion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdControlProduccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdControlProduccionActionPerformed(e);
			}
		});
		cmdControlProduccion.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaDown.png"));
		cmdControlProduccion.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaUp.png"));
		cmdControlProduccion.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaDown.png"));
		cmdControlProduccion.setMargin(new Insets(0, 0, 0, 0));
		cmdControlProduccion.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdControlProduccion.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdControlProduccion.setToolTipText("");
		cmdControlProduccion.setText("Control Produccion");
		cmdControlProduccion.setOpaque(false);
		cmdControlProduccion.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdControlProduccion.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdControlProduccion.setBorderPainted(false);
		cmdControlProduccion.setBounds(55, 81, 167, 130);
		panMenuAdministracion.add(cmdControlProduccion);
		
		cmdAsistencia = new JButton();
		cmdAsistencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdAsistenciaActionPerformed(e);
			}
		});
		cmdAsistencia.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdAsistencia.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdAsistencia.setToolTipText("");
		cmdAsistencia.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAsistencia.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonArchivoDown.png"));
		cmdAsistencia.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonArchivoUp.png"));
		cmdAsistencia.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonArchivoDown.png"));
		
		cmdAsistencia.setText("Asistencia");
		cmdAsistencia.setOpaque(false);
		cmdAsistencia.setMargin(new Insets(0, 0, 0, 0));
		cmdAsistencia.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdAsistencia.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdAsistencia.setBorderPainted(false);
		cmdAsistencia.setBounds(550, 81, 167, 130);
		panMenuAdministracion.add(cmdAsistencia);
		panMenuInicio.add(cmdAdministracion);
		cmdAdministracion.setBounds(40, 270, 150, 130);

		panelPrincipal.add(panMenuInicio);
		panMenuInicio.setBounds(0, 0, 810, 597);

		cmdControl = new JButton();
		cmdControl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdControl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdControlActionPerformed(arg0);

			}
		});
		cmdControl.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("MarcarDown.png"));
		cmdControl.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("MarcarUp.png"));
		cmdControl.setIcon(AplicacionGeneral.getInstance().obtenerImagen("MarcarDown.png"));
		cmdControl.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdControl.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdControl.setText("Control");
		cmdControl.setOpaque(false);
		cmdControl.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdControl.setFont(new Font("Tahoma", Font.BOLD, 14));
		cmdControl.setBorderPainted(false);
		cmdControl.setBounds(345, 85, 140, 130);
		panMenuInicio.add(cmdControl);

		JButton cmdUsuario = new JButton();
		cmdUsuario.setBounds(381, 456, 130, 130);
		panMenuInicio.add(cmdUsuario);
		cmdUsuario.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdUsuarioActionPerformed(e);

			}

		});
		cmdUsuario.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonCerrarSesionDown.png"));
		cmdUsuario.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonCerraSesionUp.png"));
		cmdUsuario.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonCerrarSesionDown.png"));
		cmdUsuario.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdUsuario.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdUsuario.setToolTipText("");
		cmdUsuario.setText("Usuarios");
		cmdUsuario.setOpaque(false);
		cmdUsuario.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdUsuario.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdUsuario.setBorderPainted(false);

		panMenuOperaciones = new JPanel();
		panMenuOperaciones.setOpaque(false);
		panMenuOperaciones.setLayout(null);
		panMenuOperaciones.setBounds(0, 0, 810, 597);
		panelPrincipal.add(panMenuOperaciones);
		cmdProduccion = new javax.swing.JButton();
		cmdProduccion.setBounds(593, 228, 130, 130);
		panMenuOperaciones.add(cmdProduccion);

		cmdProduccion.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
		cmdProduccion.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonProcesoDown.png")); // NOI18N
		cmdProduccion.setText("Productos");
		cmdProduccion.setBorderPainted(false);
		cmdProduccion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdProduccion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		cmdProduccion.setOpaque(false);
		cmdProduccion.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonProcesoDown.png")); // NOI18N
		cmdProduccion.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonProcesoUp.png")); // NOI18N
		cmdProduccion.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
		cmdProduccion.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		cmdProduccion.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdAcuentaActionPerformed(evt);
			}
		});
		cmdEntregas = new javax.swing.JButton();
		cmdEntregas.setBounds(94, 228, 149, 130);
		panMenuOperaciones.add(cmdEntregas);

		cmdEntregas.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
		cmdEntregas.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaDown.png")); // NOI18N
		cmdEntregas.setText("Control Pedidos");
		cmdEntregas.setBorderPainted(false);
		cmdEntregas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		cmdEntregas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		cmdEntregas.setOpaque(false);
		cmdEntregas.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaDown.png")); // NOI18N
		cmdEntregas.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonEntregaUp.png")); // NOI18N
		cmdEntregas.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
		cmdEntregas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

		cmdReporteProduccion = new JButton();
		cmdReporteProduccion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdReporteProduccion.setPressedIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonArchivoDown.png"));
		cmdReporteProduccion.setRolloverIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonArchivoUp.png"));
		cmdReporteProduccion.setIcon(AplicacionGeneral.getInstance().obtenerImagen("BotonArchivoDown.png"));
		cmdReporteProduccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdReporteProduccionActionPerformed(arg0);

			}

		});
		cmdReporteProduccion.setVerticalTextPosition(SwingConstants.BOTTOM);
		cmdReporteProduccion.setVerticalAlignment(SwingConstants.BOTTOM);
		cmdReporteProduccion.setText("Reporte Produccion");
		cmdReporteProduccion.setOpaque(false);
		cmdReporteProduccion.setHorizontalTextPosition(SwingConstants.CENTER);
		cmdReporteProduccion.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmdReporteProduccion.setBorderPainted(false);
		cmdReporteProduccion.setBounds(321, 86, 186, 130);
		panMenuOperaciones.add(cmdReporteProduccion);
		cmdEntregas.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdEntregasActionPerformed(evt);
			}
		});

		setSize(new java.awt.Dimension(810, 597));
		setLocationRelativeTo(null);
	}// </editor-fold>//GEN-END:initComponents

	private void cmdCerrarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdCerrarActionPerformed
		int resMsg = JOptionPane.showConfirmDialog(this, "���DESEA SALIR DEL DEL SISTEMA !!!", "Sistema de ", JOptionPane.YES_NO_OPTION);

		if (resMsg == 0) {

//			man.transactionDelegate.end();
			System.exit(0);
//	    	this.dispose();
//	    	new SisemListener().ejecutarModulo(OrganizacionGeneral.getOrgcodigo());
		}// Fin de if (resMsg == 0)
	}// GEN-LAST:event_cmdCerrarActionPerformed

	private void cmdAdministracionActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdAdministracionActionPerformed

		panMenuInicio.setVisible(false);
		panMenuAdministracion.setVisible(true);
		panMenuOperaciones.setVisible(false);

	}// GEN-LAST:event_cmdAdministracionActionPerformed

	private void cmdOperacionesActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdOperacionesActionPerformed

		panMenuInicio.setVisible(false);
		panMenuAdministracion.setVisible(false);
		panMenuOperaciones.setVisible(true);

	}// GEN-LAST:event_cmdOperacionesActionPerformed

	private void cmdInicioActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdInicioActionPerformed

		panMenuInicio.setVisible(true);
		panMenuAdministracion.setVisible(false);
		panMenuOperaciones.setVisible(false);
	}// GEN-LAST:event_cmdInicioActionPerformed

	private void cmdEntregasActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdEntregasActionPerformed

		man.abrirPantalla("DlgControlPedido", "FrmPrincipal", AuxiliarTextil.AccesoPagina.INICIAR);
	}// GEN-LAST:event_cmdEntregasActionPerformed

	private void cmdUsuarioActionPerformed(ActionEvent e) {

		this.dispose();
		man.abrirPantalla("DlgAcceso", "FrmPrincipal", AuxiliarTextil.AccesoPagina.INICIAR);

	}

	private void cmdPedidoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdPedidoActionPerformed
		man.abrirPantalla("DlgPedido", "FrmPrincipal", AuxiliarTextil.AccesoPagina.INICIAR);
	}// GEN-LAST:event_cmdPedidoActionPerformed

	private void cmdControlProduccionActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdPedidoActionPerformed
//		Util.getInstance().abrirArchivoDesktop("C:\\sisem\\bin\\produccion.bat");
//		man.transactionDelegate.end();
//		System.exit(0);
//		try {
//			DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().commit();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		this.dispose();
		new SisemContextListener().ejecutarModulo(OrganizacionGeneral.getOrgcodigo());
		
	}// GEN-LAST:event_cmdPedidoActionPerformed
	private void cmdAsistenciaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdPedidoActionPerformed
//		Util.getInstance().abrirArchivoDesktop("C:\\sisem\\bin\\produccion.bat");
//		man.transactionDelegate.end();
//		System.exit(0);
//		try {
//			DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().commit();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		if (man.validarUsuario()) {
			this.dispose();
			new SisemAsistenciaListener().ejecutarModulo(OrganizacionGeneral.getOrgcodigo());
		}
		
		
	}// GEN-LAST:event_cmdPedidoActionPerformed
	private void cmdAcuentaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdAcuentaActionPerformed

		man.abrirPantalla("DlgProducto", "FrmPrincipal", AuxiliarTextil.AccesoPagina.INICIAR);
	}// GEN-LAST:event_cmdAcuentaActionPerformed

	private void cmdReporteActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdReporteActionPerformed

		man.abrirPantalla("DlgArea", "FrmPrincipal", AccesoPagina.INICIAR);

	}// GEN-LAST:event_cmdReporteActionPerformed

	private void cmdReporteProduccionActionPerformed(ActionEvent arg0) {

		man.abrirPantalla("DlgControlProducto", "FrmPrincipal", AuxiliarTextil.AccesoPagina.INICIAR);

	}

	private void cmdControlActionPerformed(ActionEvent arg0) {

		man.abrirPantalla("DlgCodigoProducto", "FrmPrincipal", AuxiliarTextil.AccesoPagina.INICIAR);

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting cFrmPrincipal">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;

				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/*
		 * Create and display the form
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				new FrmPrincipal().setVisible(true);

			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton cmdProduccion;
	private javax.swing.JButton cmdAdministracion;
	private javax.swing.JButton cmdCerrar;
	private javax.swing.JButton cmdEntregas;
	private javax.swing.JButton cmdInicio;
	private javax.swing.JButton cmdOperaciones;
	private javax.swing.JButton cmdPedido;
	private javax.swing.JButton cmdAreas;
	private javax.swing.JPanel panMenuInicio;
	private javax.swing.JPanel panMenuAdministracion;
	private JPanel panMenuOperaciones;
	private JButton cmdReporteProduccion;
	private JButton cmdControl;
	private JButton cmdControlProduccion;
	private JLabel logo;
	private JButton cmdAsistencia;

	// End of variables declaration//GEN-END:variables

	@Override
	public void iniciarFormulario() {
		man.claveValidado = "";
		if (acceso.equals(AccesoPagina.INICIAR)) {

			panMenuInicio.setVisible(true);
			panMenuAdministracion.setVisible(false);
			panMenuOperaciones.setVisible(false);

			llenarFormulario();
			this.setVisible(true);

		}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
		else if (acceso.equals(AccesoPagina.ACTUALIZAR)) {

			llenarFormulario();

		}
	}// Fin de iniciarFormulario()

	@Override
	public void iniciarInstancias() {
		/*
		 * Obtiene una conexion del pool de conexiones de Apache
		 */
		man.transactionDelegate = (TransactionDelegate) ContenedorTextil.getComponent("TransactionDelegate");
		man.testProp = (TestPropiedades) ContenedorTextil.getComponent("TestPropiedades");
		man.funUsu = (FuncionesUsuario) ContenedorTextil.getComponent("FuncionesUsuario");

//		man.transactionDelegate.start();
		/*
		 * Obtiene las entidades del modelo relacional para su manejo en este formulario
		 */
		man.cli = (Cliente) ContenedorTextil.getComponent("Persona");

	}// Fin de iniciarInstancias()

	@Override
	public void limpiarInstancias(AccesoPagina parAcceso) {

		man.cli.limpiarInstancia();

	}// Fin de limpiarInstancias()

	@Override
	public void limpiarFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void generarNuevoRegistro() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void llenarFormulario() {

		if (acceso.equals(AccesoPagina.INICIAR)) {

			estFrmPrin = EstadoPagina.VISUALIZANDO;
			iniciarInstancias();

		}// Fin de if(acceso.equals(AccesoPagina.INICIAR))
		else if (acceso.equals(AccesoPagina.ACTUALIZAR)) {

			estFrmPrin = EstadoPagina.VISUALIZANDO;
			iniciarInstancias();

		}// Fin de if(acceso.equals(AccesoPagina.INICIAR))

//		etiEmpresa.setText(man.testProp.getPropiedad("AMPATO INVEST S.A.C"));
//		etiLocal.setText(man.testProp.getPropiedad(""));
//		etiRepresentante.setText("");

	}// Fin de llenarFormulario

	@Override
	public void activarFormulario(boolean parAct) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void refrescarFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void obtenerDatoFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void obtenerDatoBaseDato() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean validarFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}// Fin de validarFormulario

	@Override
	public boolean guardarDatoBaseDato(String parModoGrabar) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void setManejador(AuxiliarTextil parMan) {

		man = parMan;

	}

	@Override
	public void cerrar() {
		if (padre.equals("FrmPrincipal")) {
			int resMsg = JOptionPane.showConfirmDialog(this, "¿Desea cerrar formulario?", "Sistema de Cobranza", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

			if (resMsg == 0) {

				this.dispose();

			}// Fin de verificar que se desea cerrar el formulario
		}// El padre es cualquier formulario
		else {

			this.dispose();
			man.abrirPantalla(padre, "FrmPrincipal", AuxiliarTextil.AccesoPagina.ACTUALIZAR);

		}// Fin de Verificar que el padre es cualquier ventana
	}// cerrar
}// Find e clase principal

