package com.textil.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Agrupacion;
import com.comun.entidad.Area;
import com.comun.entidad.Clase;
import com.comun.entidad.Cliente;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Ficha;
import com.comun.entidad.Grupo;
import com.comun.entidad.Pedido;
import com.comun.entidad.Producto;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.SwingWorker;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.formato.FormatoFecha;
import com.comun.utilidad.swing.JTextEditorFecha;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.JTextFieldFormatoDecimal;
import com.comun.utilidad.swing.JTextFieldFormatoEntero;
import com.comun.utilidad.swing.JTextFieldFormatoFecha;
import com.comun.utilidad.swing.PanelImgFoto;
import com.textil.controlador.AuxiliarTextil;
import com.textil.controlador.AuxiliarTextil.AccesoPagina;
import com.textil.controlador.AuxiliarTextil.EstadoDocumento;
import com.textil.controlador.AuxiliarTextil.EstadoPagina;
import com.textil.controlador.AuxiliarTextil.TipoDocumento;
import com.textil.controlador.ContenedorTextil;
import com.textil.dao.ClienteDAO;
import com.textil.dao.TransactionDelegate;
import com.toedter.calendar.JDateChooser;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 * 
 * @author Hernan
 */
public class DlgPedidoImportacion extends javax.swing.JDialog implements InterfaceUsuario {

	/**
	 * Variables principales
	 */
	public static EstadoPagina estDlgPed = EstadoPagina.VISUALIZANDO;
	public AccesoPagina acceso;
	public AuxiliarTextil man;
	public String padre;
	public boolean frmAbierto;
	/**
	 * Variables del formulario
	 */

	private CeldaRenderGeneral celdaRenderGeneral;
	private TableColumn[] colTabDetPrd;
	private JTextFieldChanged txtCliIde;
	private JTextFieldChanged txtCliDoc;
	private JTextFieldChanged txtCliNum;
	private JTextFieldChanged txtCliNom;
	private JTextFieldChanged txtCliDir;
	private JTextFieldChanged txtPedNumDoc;
	private JDateChooser cmbPedFecReg;
	private JDateChooser cmbPedFecEnt;
	private JTextFieldFormatoEntero txtPedAno;
	private JTextFieldFormatoEntero txtPedTemp;
	private JTextFieldChanged txtPedEst;
	private PanelImgFoto panPrdBar;
	private JButton cmdDetPedBuscar;
	private JLabel etiPedNomDoc;
	private final JTextEditorFecha txtEditPedFecReg, txtEditPedFecEnt;
	private JButton cmdCerrar;
	private JTextFieldFormatoDecimal txtPedImpTot;
	private DefaultListModel modLisCliNom;
	private boolean busLisCliNom;
	private List<String> varLisCliNom;
	public static int bookMarkPedCod = 0;
	private int bookMarkDetPedCod;

	private int bookMarkCliCod;
	/**
	 * codigoCompuesto<br>
	 * ===============<br>
	 * Se forma de una consulta en<br>
	 * <b>ProductoDAO.obtenerListaProducto</b><br>
	 * 0 : detped_cod <br>
	 * 1 : ped_cod<br>
	 * 2 : prd_cod<br>
	 * 3 : cla_cod<br>
	 * 4 : gru_cod<br>
	 * 5 : gru_nivel<br>
	 * 6 : prd_ide<br>
	 * 7 : prd_des<br>
	 */
	private JPopupMenu popMenModFoto;
	private JMenuItem menuPegarModFoto;
	private JMenuItem menuBorrarModFoto;
	private TableColumn[] colTabDetTal;
	private JScrollPane scrLisCliNom;
	private JList lisCliNom;
	private JTextFieldChanged txtTalIde;
	private String bookMarkNomDoc;
	private JButton cmdPedAgregar;
	private JProgressBar barraProgreso;

	/**
	 * Creates new form DlgGrupo
	 */
	public DlgPedidoImportacion(FrmPrincipal parent) {

		super(parent, false);
		setTitle("Administracion del Pedido : "+this.getClass().getSimpleName());
		setResizable(false);
		getContentPane().setFont(new Font("Tahoma", Font.BOLD, 14));
		modLisCliNom = new DefaultListModel();
		txtEditPedFecReg = new JTextEditorFecha();
		txtEditPedFecReg.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEditPedFecEnt = new JTextEditorFecha();
		txtEditPedFecEnt.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txtEditPedFecReg.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());
		txtEditPedFecEnt.setFormatterFactory(new JTextFieldFormatoFecha(new FormatoFecha()).getFormatterFactory());

		initComponents();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		scrLisCliNom = new JScrollPane();
		scrLisCliNom.setBounds(110, 114, 210, 82);
		getContentPane().add(scrLisCliNom);

		lisCliNom = new JList(modLisCliNom);
		lisCliNom.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				lisPerNomMousePressed(e);

			}
		});
		lisCliNom.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				lisPerNomKeyReleased(e);

			}
		});
		scrLisCliNom.setViewportView(lisCliNom);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Pedido", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(5, 5, 703, 59);
		getContentPane().add(panel);

		etiPedNomDoc = new JLabel();
		etiPedNomDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiPedNomDoc.setText(TipoDocumento.NOTAPEDIDO.getVal());
		etiPedNomDoc.setHorizontalAlignment(SwingConstants.CENTER);
		etiPedNomDoc.setForeground(new Color(51, 51, 51));
		etiPedNomDoc.setBounds(10, 15, 129, 15);
		panel.add(etiPedNomDoc);

		txtPedNumDoc = new JTextFieldChanged(11);
		txtPedNumDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedNumDoc.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPedNumDocKeyReleased(arg0);

			}
		});
		txtPedNumDoc.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedNumDoc.setBounds(10, 32, 129, 20);
		panel.add(txtPedNumDoc);

		cmbPedFecReg = new JDateChooser(txtEditPedFecReg);
		cmbPedFecReg.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbPedFecReg.setDateFormatString("dd/MM/yyyy");
		cmbPedFecReg.setBounds(168, 32, 115, 20);
		panel.add(cmbPedFecReg);

		JLabel label_1 = new JLabel();
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setText("Fecha Registro");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(51, 51, 51));
		label_1.setBounds(168, 15, 115, 15);
		panel.add(label_1);

		JLabel label_2 = new JLabel();
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setText("Fecha Entrega");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setForeground(new Color(51, 51, 51));
		label_2.setBounds(304, 15, 115, 15);
		panel.add(label_2);

		cmbPedFecEnt = new JDateChooser(txtEditPedFecEnt);
		cmbPedFecEnt.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmbPedFecEnt.getCalendarButton().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmbPedFecEnt.setDateFormatString("dd/MM/yyyy");
		cmbPedFecEnt.setBounds(304, 32, 115, 20);
		panel.add(cmbPedFecEnt);

		JLabel label_3 = new JLabel();
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setText("Estado");
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setForeground(new Color(51, 51, 51));
		label_3.setBounds(562, 15, 131, 15);
		panel.add(label_3);

		txtPedEst = new JTextFieldChanged(20);
		txtPedEst.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedEst.setText("12345678901234567890");
		txtPedEst.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedEst.setBounds(562, 32, 131, 20);
		panel.add(txtPedEst);

		txtPedTemp = new JTextFieldFormatoEntero(2);
		txtPedTemp.setText("4");
		txtPedTemp.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedTemp.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedTemp.setBounds(482, 32, 70, 20);
		panel.add(txtPedTemp);

		JLabel label_4 = new JLabel();
		label_4.setText("Temporada");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(482, 15, 70, 15);
		panel.add(label_4);

		JLabel label_5 = new JLabel();
		label_5.setText("A\u00F1o");
		label_5.setHorizontalAlignment(SwingConstants.CENTER);
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(429, 15, 49, 15);
		panel.add(label_5);

		txtPedAno = new JTextFieldFormatoEntero(4);
		txtPedAno.setText("4");
		txtPedAno.setHorizontalAlignment(SwingConstants.CENTER);
		txtPedAno.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPedAno.setBounds(429, 32, 49, 20);
		panel.add(txtPedAno);

		JPanel panCliente = new JPanel();
		panCliente.setLayout(null);
		panCliente.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Datos del Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panCliente.setBounds(5, 60, 703, 59);
		getContentPane().add(panCliente);

		JLabel label_6 = new JLabel();
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_6.setText("Nombre");
		label_6.setHorizontalAlignment(SwingConstants.LEFT);
		label_6.setForeground(new Color(51, 51, 51));
		label_6.setBounds(105, 15, 209, 15);
		panCliente.add(label_6);

		txtCliNom = new JTextFieldChanged(50);
		txtCliNom.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliNom.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtPerNomKeyReleased(arg0);

			}
		});

		txtCliIde = new JTextFieldChanged(10);
		txtCliIde.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliIde.setText("AYK");
		txtCliIde.setBounds(10, 32, 91, 20);
		panCliente.add(txtCliIde);
		txtCliNom.setText("Hernan Mendoza Ticllahuanaco");
		txtCliNom.setBounds(105, 32, 209, 20);
		panCliente.add(txtCliNom);

		txtCliDoc = new JTextFieldChanged(3);
		txtCliDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliDoc.setBounds(317, 32, 39, 20);
		panCliente.add(txtCliDoc);
		txtCliDoc.setText("RUC");

		txtCliNum = new JTextFieldChanged(11);
		txtCliNum.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliNum.setBounds(358, 32, 114, 20);
		panCliente.add(txtCliNum);
		txtCliNum.setText("12345678901");

		txtCliDir = new JTextFieldChanged(100);
		txtCliDir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCliDir.setText("Av. 28 de Julio 785 La Tomilla Cayma");
		txtCliDir.setBounds(475, 32, 218, 20);
		panCliente.add(txtCliDir);

		JLabel label_9 = new JLabel();
		label_9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_9.setText("Direccion");
		label_9.setHorizontalAlignment(SwingConstants.LEFT);
		label_9.setForeground(new Color(51, 51, 51));
		label_9.setBounds(475, 15, 218, 15);
		panCliente.add(label_9);

		JLabel label_10 = new JLabel();
		label_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_10.setText("Codigo");
		label_10.setHorizontalAlignment(SwingConstants.LEFT);
		label_10.setForeground(new Color(51, 51, 51));
		label_10.setBounds(10, 15, 91, 15);
		panCliente.add(label_10);

		JLabel lblDoc = new JLabel();
		lblDoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDoc.setBounds(317, 15, 39, 15);
		panCliente.add(lblDoc);
		lblDoc.setText("DOC");
		lblDoc.setHorizontalAlignment(SwingConstants.CENTER);
		lblDoc.setForeground(new Color(51, 51, 51));

		JLabel label_8 = new JLabel();
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_8.setBounds(358, 15, 114, 15);
		panCliente.add(label_8);
		label_8.setText("Numero");
		label_8.setHorizontalAlignment(SwingConstants.LEFT);
		label_8.setForeground(new Color(51, 51, 51));

		cmdDetPedBuscar = new JButton();
		cmdDetPedBuscar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdDetPedBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));
		cmdDetPedBuscar.setText("Buscar");
		cmdDetPedBuscar.setName("Agregar");
		cmdDetPedBuscar.setMargin(new Insets(0, 0, 0, 0));
		cmdDetPedBuscar.setBounds(886, 289, 90, 35);
		getContentPane().add(cmdDetPedBuscar);

		JLabel label_20 = new JLabel();
		label_20.setText("Importe Total");
		label_20.setHorizontalAlignment(SwingConstants.CENTER);
		label_20.setForeground(new Color(51, 51, 51));
		label_20.setBounds(886, 329, 90, 20);
		getContentPane().add(label_20);

		txtPedImpTot = new JTextFieldFormatoDecimal(13, 2);
		txtPedImpTot.setEnabled(false);
		txtPedImpTot.setText("9999999999.99");
		txtPedImpTot.setHorizontalAlignment(SwingConstants.RIGHT);
		txtPedImpTot.setBounds(886, 349, 90, 20);
		getContentPane().add(txtPedImpTot);

		JSeparator separator = new JSeparator();
		separator.setBounds(5, 121, 703, 2);
		getContentPane().add(separator);

		cmdCerrar = new JButton();
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cmdCerrarActionPerformed(arg0);

			}
		});
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png"));
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setName("Agregar");
		cmdCerrar.setMargin(new Insets(0, 0, 0, 0));
		cmdCerrar.setBounds(602, 130, 104, 40);
		getContentPane().add(cmdCerrar);

		panPrdBar = new PanelImgFoto();
		panPrdBar.setBounds(881, 429, 95, 41);
		getContentPane().add(panPrdBar);
		panPrdBar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GroupLayout gl_panPrdBar = new GroupLayout(panPrdBar);
		gl_panPrdBar.setHorizontalGroup(
				gl_panPrdBar.createParallelGroup(Alignment.LEADING)
						.addGap(0, 145, Short.MAX_VALUE)
						.addGap(0, 133, Short.MAX_VALUE)
				);
		gl_panPrdBar.setVerticalGroup(
				gl_panPrdBar.createParallelGroup(Alignment.LEADING)
						.addGap(0, 65, Short.MAX_VALUE)
						.addGap(0, 53, Short.MAX_VALUE)
				);
		panPrdBar.setLayout(gl_panPrdBar);

		JLabel lblElaborando = new JLabel();
		lblElaborando.setBounds(881, 481, 95, 20);
		getContentPane().add(lblElaborando);
		lblElaborando.setForeground(new Color(0, 100, 0));
		lblElaborando.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		lblElaborando.setText("ELABORANDO");
		lblElaborando.setHorizontalAlignment(SwingConstants.CENTER);
		lblElaborando.setFont(new Font("Tahoma", Font.PLAIN, 11));

		JLabel lblTalla = new JLabel("Talla Ide");
		lblTalla.setBounds(886, 380, 90, 15);
		getContentPane().add(lblTalla);
		lblTalla.setHorizontalAlignment(SwingConstants.CENTER);
		lblTalla.setBorder(null);

		txtTalIde = new JTextFieldChanged(50);
		txtTalIde.setEnabled(false);
		txtTalIde.setBounds(887, 398, 89, 20);
		getContentPane().add(txtTalIde);
		txtTalIde.setText("12345678901234567890");
		
		cmdPedAgregar = new JButton();
		cmdPedAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdPedAgregarActionPerformed(e);
			}
		});
		cmdPedAgregar.setText("Agregar");
		cmdPedAgregar.setName("Agregar");
		cmdPedAgregar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdPedAgregar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png"));		
		cmdPedAgregar.setMargin(new Insets(0, 0, 0, 0));
		cmdPedAgregar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdPedAgregar.setBounds(5, 130, 90, 40);
		getContentPane().add(cmdPedAgregar);
		
		barraProgreso = new JProgressBar();
		barraProgreso.setBounds(105, 145, 487, 25);
		getContentPane().add(barraProgreso);

		setSize(new Dimension(715, 221));
		setLocationRelativeTo(null);
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DlgPedidoImportacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DlgPedidoImportacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DlgPedidoImportacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DlgPedidoImportacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/* Create and display the dialog */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				DlgPedidoImportacion dialog = new DlgPedidoImportacion(null);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	// End of variables declaration//GEN-END:variables

	/*
	 * METODOS DE LA INTERFAZ PRINCIPAL
	 * 
	 * @see com.vista.InterfaceUsuario#iniciarFormulario()
	 */
	@Override
	public void iniciarFormulario() {

		if (man.validarUsuario()) {

			busLisCliNom = true;

			accesoIniciarFormulario();

			this.setVisible(true);
		} else {
			this.setVisible(false);
		}

	}

	@Override
	public void iniciarInstancias() {
		/*
		 * Obtiene una conexion del pool de conexiones de Apache
		 */
		man.transactionDelegate = (TransactionDelegate) ContenedorTextil.getComponent("TransactionDelegate");
		man.testProp = (TestPropiedades) ContenedorTextil.getComponent("TestPropiedades");
		man.funUsu = (FuncionesUsuario) ContenedorTextil.getComponent("FuncionesUsuario");

//		man.transactionDelegate.start();
		/*
		 * Obtiene las entidades del modelo relacional para su manejo en este
		 * formulario
		 */
		man.cli = (Cliente) ContenedorTextil.getComponent("Cliente");

		man.gru = (Grupo) ContenedorTextil.getComponent("Grupo");
		man.gruCol = (Grupo) ContenedorTextil.getComponent("GrupoColor");
		man.gruMod = (Grupo) ContenedorTextil.getComponent("GrupoModelo");
		man.gruPrd = (Grupo) ContenedorTextil.getComponent("GrupoProducto");
		man.gruTal = (Grupo) ContenedorTextil.getComponent("GrupoTalla");

		man.claCol = (Clase) ContenedorTextil.getComponent("ClaseColor");
		man.cla = (Clase) ContenedorTextil.getComponent("Clase");

		man.cli = (Cliente) ContenedorTextil.getComponent("Cliente");
		man.ped = (Pedido) ContenedorTextil.getComponent("Pedido");
		man.detPed = (DetPedido) ContenedorTextil.getComponent("DetPedido");
		
		man.prdGen = (Producto) ContenedorTextil.getComponent("ProductoGeneral");
		man.prdPad = (Producto) ContenedorTextil.getComponent("ProductoPadre");
		man.mod = (Producto) ContenedorTextil.getComponent("Modelo");
		man.prd = (Producto) ContenedorTextil.getComponent("Producto");
		man.tal = (Producto) ContenedorTextil.getComponent("Talla");

		man.are = (Area) ContenedorTextil.getComponent("Area");
		man.subAre = (SubArea) ContenedorTextil.getComponent("SubArea");
		man.fic = (Ficha) ContenedorTextil.getComponent("Ficha");

		man.usuSes = (Usuario) ContenedorTextil.getComponent("UsuarioSesion");
		man.agru = (Agrupacion) ContenedorTextil.getComponent("Agrupacion");

	}

	@Override
	public void limpiarInstancias(AccesoPagina parAcceso) {
		if (parAcceso.equals(AccesoPagina.INICIAR) || parAcceso.equals(AccesoPagina.FINALIZAR)) {

			man.ped.limpiarInstancia();
			man.cli.limpiarInstancia();

			man.are.limpiarInstancia();
			man.subAre.limpiarInstancia();
			man.fic.limpiarInstancia();
		}// Fin de
//		man.lisCli.clear();
	}

	@Override
	public void limpiarFormulario() {

		limpiarDlgPedido();
		limpiarDlgCliente();
		barraProgreso.setValue(0);
	}

	@Override
	public void generarNuevoRegistro() {

	}

	@Override
	public void llenarFormulario() {

		man.obtenerListaGrupo();
		man.obtenerListaClase();

		// llenarComboColor();

	}

	@Override
	public void activarFormulario(boolean parAct) {

		activarDlgCliente(parAct);
		activarDlgPedido(parAct);
	}

	@Override
	public void refrescarFormulario() {
		refrescarDlgPedido();
	}

	@Override
	public void obtenerDatoFormulario() {
		// TODO Auto-generated method stub

	}

	@Override
	public void obtenerDatoBaseDato() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validarFormulario() {
		return true;
	}

	@Override
	public boolean guardarDatoBaseDato(String parModoGrabar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setManejador(AuxiliarTextil parMan) {

		this.man = parMan;

	}

	@Override
	public void cerrar() {
		if (padre.equals("FrmPrincipal")) {
			int resMsg = JOptionPane.showConfirmDialog(this,
					"�Deasea cerrar el sistema?",
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (resMsg == 0) {

				this.dispose();
				bookMarkCliCod = 0;
				bookMarkPedCod = 0;
				bookMarkDetPedCod = 0;
				man.claveValidado = "";

				limpiarInstancias(AccesoPagina.FINALIZAR);
//				man.transactionDelegate.end();
//				try {
//					DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().commit();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
				man.abrirPantalla("FrmPrincipal", "FrmPrincipal", AccesoPagina.ACTUALIZAR);

			}// Fin de verificar que se desea cerrar el formulario
		}// El padre es cualquier formulario
		else {

			this.dispose();
			man.abrirPantalla(padre, "FrmPrincipal", AccesoPagina.ACTUALIZAR);

		}// Fin de Verificar que el padre es cualquier ventana
	}

	/*
	 * METODOS DE USUARIO
	 */

	public void limpiarInstanciasDetPedido() {

		man.gruCol.limpiarInstancia();
		man.claCol.limpiarInstancia();

		man.detPed.limpiarInstancia();
		man.prdGen.limpiarInstancia();
		man.gruTal.limpiarInstancia();
		man.tal.limpiarInstancia();

	}

	public void accesoIniciarFormulario() {

		estDlgPed = EstadoPagina.AGREGANDO;

		frmAbierto = false;

		iniciarInstancias();
		limpiarInstancias(AccesoPagina.INICIAR);
		llenarFormulario();
		limpiarFormulario();

		bookMarkPedCod = (man.ped.getPed_cod() == 0) ? bookMarkPedCod : man.ped.getPed_cod();
		bookMarkDetPedCod = (man.detPed.getDetped_cod() == 0) ? bookMarkDetPedCod : man.detPed.getDetped_cod();

		cmdPedAgregarActionPerformed(null);
		
		frmAbierto = true;
	}

	public void cerrarFormulario() {

		estDlgPed = EstadoPagina.VISUALIZANDO;

		frmAbierto = false;

		iniciarInstancias();
		limpiarInstancias(AccesoPagina.INICIAR);
		llenarFormulario();
		limpiarFormulario();
		
		refrescarFormulario();
		
		frmAbierto = true;
	}

	public void accesoActualizarFormulario() {

		accesoIniciarFormulario();

	}

	private void limpiarDlgCliente() {

		txtCliIde.setText("");
		txtCliDoc.setText("");
		txtCliNum.setText("");
		txtCliNom.setText("");
		txtCliDir.setText("");

		scrLisCliNom.setVisible(false);

	}

	private void limpiarDlgPedido() {

		// etiPedNomDoc.setText("");
		txtPedNumDoc.setText("");
		cmbPedFecEnt.setDate(null);
		cmbPedFecReg.setDate(null);
		txtPedAno.setValue(0);
		txtPedTemp.setValue(0);
		txtPedEst.setText("");
		txtPedImpTot.setValue(0.00);
		lisCliNom.setVisible(false);

	}

	private void llenarPedidoCliente() {

		frmAbierto = false;

		llenarDlgCliente();
		llenarDlgPedido();

		frmAbierto = true;
	}// Fin de buscarVenta()

	private boolean validarDlgPedido() {

		if (cmbPedFecReg.getDate() != null) {
			if (cmbPedFecEnt.getDate() != null) {
				if (Integer.parseInt(txtPedTemp.getText()) > 0) {
					if (txtCliIde.getText().trim().length() > 0) {

						return true;

					} else {

						JOptionPane.showConfirmDialog(this,
								"Ingrrese un CLIENTE\n" + this.getClass().getName(),
								man.testProp.getPropiedad("TituloSistema"),
								JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
						txtCliNom.requestFocus();

					}
				} else {

					JOptionPane.showConfirmDialog(this,
							"Ingrrese una TEMPORADA correcta\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
					txtPedTemp.requestFocus();

				}
			} else {
				JOptionPane.showConfirmDialog(this,
						"���Ingrese FECHA DE ENTREGA correcta!!!\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
				txtEditPedFecEnt.requestFocus();
			}
		} else {
			JOptionPane.showConfirmDialog(this,
					"���Ingrese FECHA DE REGISTRO correcta!!!\n" + this.getClass().getName(),
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
			txtEditPedFecReg.requestFocus();
		}

		return false;
	}

	private boolean validarIdentificador(String parPrdCol) {

		// System.out.println("lengt : " + parPrdCol.length() + " indice : " + parPrdCol.indexOf("-"));
		if (parPrdCol.contains("-")) {
			String numero = parPrdCol.substring(0, parPrdCol.indexOf("-"));
			if (man.funUsu.isInteger(numero)) {
				if (parPrdCol.length() > parPrdCol.indexOf("-") + 1) {

					return true;

				} else {
					JOptionPane.showConfirmDialog(null, "LA SEGUNDA PARTE DEL CODIGO DEBE SER TEXTO", man.testProp.getPropiedad("TituloSistema"), -1, 1);
				}
			} else {
				JOptionPane.showConfirmDialog(null, "LA PRIMERA PARTE DEL CODIGO DEBE SER NUMERO", man.testProp.getPropiedad("TituloSistema"), -1, 1);
			}
		} else {
			JOptionPane.showConfirmDialog(null, "LA ESTRUCTURA DE COLOR DEBE SER 01-IDENTIFICADOR", man.testProp.getPropiedad("TituloSistema"), -1, 1);
		}
		return false;
	}

	public void enviarImpresora(String tipoReporte) {
		File repFactura;
			
		if (tipoReporte.equals("PEDIDO")) {
			repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "ReportePedido.jasper");
		} else if (tipoReporte.equals("MODELO")) {
			repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "FichaControlModelo.jasper");
		} else if (tipoReporte.equals("PRODUCTO")) {
			repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "FichaControlProducto.jasper");
		} else {
			repFactura = null;
		}
		if (repFactura != null) {
			try {
				InputStream inpStrRepFac = new FileInputStream(repFactura);
				HashMap map = new HashMap();
				map.put("PED_COD", man.ped.getPed_cod());
				map.put("PRD_SUPUNO", man.mod.getPrd_cod());
				map.put("PRD_SUPDOS", man.prd.getPrd_cod());
				try {

					JasperPrint print = JasperFillManager.fillReport(inpStrRepFac, map, DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection());

					JasperViewer jv = new JasperViewer(print, false);
					jv.setTitle("Reporte de Pedidos");
					jv.setSize(800, 600);
					jv.setVisible(true);

					// JasperPrintManager.printReport(print, true);

				} catch (JRException ex) {
					JOptionPane.showConfirmDialog(null, "Error de conexion con la impresora", man.testProp.getPropiedad("TituloSistema"), -1, 1);
				}
			} catch (FileNotFoundException ex) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * ACTIVAR CONTROLES
	 */

	private void activarDlgCliente(boolean parAct) {

		txtCliIde.setEnabled(parAct);
		txtCliDoc.setEnabled(parAct);
		txtCliNum.setEnabled(parAct);
		txtCliNom.setEnabled(parAct);
		txtCliDir.setEnabled(parAct);

	}

	private void activarDlgPedido(boolean parAct) {

		if (estDlgPed.equals(EstadoPagina.BUSCANDO)) {

			etiPedNomDoc.setText(TipoDocumento.NOTAPEDIDO.getVal());
			txtPedNumDoc.setEnabled(true);

		}// Fin de if(estDlgKar.equals(estadoVentana.BUSCANDO))
		else if (!estDlgPed.equals(EstadoPagina.BUSCANDO)) {

			etiPedNomDoc.setEnabled(true);
			txtPedNumDoc.setEnabled(false);
			cmbPedFecEnt.setEnabled(parAct);
			cmbPedFecReg.setEnabled(parAct);
			txtPedAno.setEnabled(parAct);
			txtPedTemp.setEnabled(parAct);
			txtPedEst.setEnabled(false);
			txtPedImpTot.setEnabled(false);

		}
	}

	/*
	 * 
	 * REFRESCAR CONTROLES
	 */
	private void refrescarDlgPedido() {
		switch (estDlgPed) {
		case VISUALIZANDO:
			activarDlgPedido(false);
			activarDlgCliente(false);

			cmdPedAgregar.setText("Agregar");

			if (man.ped.getPed_cod() > 0) 
			{
				cmdPedAgregar.setEnabled(true);
			} else {

				cmdPedAgregar.setEnabled(true);
			}// Fin de else if (man.vou.getVou_cod().equals(""))
			cmdCerrar.setEnabled(true);
			break;

		case AGREGANDO:

			activarDlgPedido(true);
			activarDlgCliente(true);
			limpiarFormulario();
			limpiarInstancias(AccesoPagina.INICIAR);

			cmdPedAgregar.setText("Guardar");

			cmdPedAgregar.setEnabled(true);
			cmdCerrar.setEnabled(true);

			break;

		case MODIFICANDO:

			activarDlgPedido(true);
			activarDlgCliente(true);

			cmdPedAgregar.setText("Guardar");

			cmdPedAgregar.setEnabled(true);
			cmdCerrar.setEnabled(false);

			break;

		case SUSPENDIDO:

			activarDlgPedido(false);

			cmdPedAgregar.setText("Guardar");

			cmdPedAgregar.setEnabled(false);
			cmdCerrar.setEnabled(false);
			break;

		case BUSCANDO:

			activarDlgPedido(false);
			limpiarFormulario();

			cmdPedAgregar.setEnabled(false);
			cmdCerrar.setEnabled(false);
			break;
		}// Fin de switch para estado del formaulario de cliente
	}
	/*
	 * 
	 * 
	 * 
	 * LLENAR REGISTROS DE FORMULARIO
	 */
	private void llenarDlgCliente() {

		txtCliIde.setText(man.cli.getCli_ide());
		txtCliDoc.setText(man.cli.getCli_doc());
		txtCliNum.setText(man.cli.getCli_num());
		txtCliNom.setText(man.cli.getCli_nom());
		txtCliDir.setText(man.cli.getCli_dir());

	}// Fin de llenarDlgCliente

	private void llenarDlgPedido() {

		etiPedNomDoc.setText(man.ped.getPed_nomdoc());
		txtPedNumDoc.setText(man.ped.getPed_numdoc());
		cmbPedFecReg.setDate(man.ped.getPed_fecreg());
		cmbPedFecEnt.setDate(man.ped.getPed_fecent());
		txtPedAno.setValue(man.ped.getPed_fecreg().getYear() + 1900);
		txtPedTemp.setText(man.ped.getPed_temp());
		txtPedEst.setText(man.ped.getPed_est());
		txtPedImpTot.setValue(man.ped.getPed_imptot());

	}// Fin de llenarDlgPedido

	private int agregarPedidoCliente() {

		bookMarkCliCod = man.agregarCliente(0, txtCliIde.getText());
		if (bookMarkCliCod == 0) {
			bookMarkCliCod = man.actualizarCliente(man.cli.getCli_cod(), txtCliIde.getText());
		}
		man.cli.setObjeto(man.obtenerCliente(bookMarkCliCod));
		if (bookMarkCliCod != 0) {

			return man.agregarPedido();

		}
		return 0;

	}// Fin de agregar Documento Pedido

	private int actualizarPedidoCliente() {

		bookMarkCliCod = man.actualizarCliente(man.cli.getCli_cod(), txtCliIde.getText());
		man.cli.setObjeto(man.obtenerCliente(bookMarkCliCod));
		if (bookMarkCliCod != 0) {

			return man.actualizarPedido();

		}
		return 0;

	}// Fin de agregar Documento Pedido

	/*
	 * 
	 * 
	 * METODOS PARA OBTENER DATOS DEL FORMULARIO
	 */

	public void obtenerDatoDlgPedido() {

		// man.ped.setPed_cod(0);//Autonumerico
		man.ped.setCli_cod(man.cli.getCli_cod());
		man.ped.setPed_nomdoc(etiPedNomDoc.getText());
		man.ped.setPed_numdoc(txtPedNumDoc.getText());
		man.ped.setPed_nom(txtCliNom.getText());
		man.ped.setPed_dir(txtCliDir.getText());
		man.ped.setPed_docide(txtCliDoc.getText());
		man.ped.setPed_numide(txtCliNum.getText());
		man.ped.setPed_temp(txtPedTemp.getText());
		man.ped.setPed_imptot(Double.parseDouble(txtPedImpTot.getText()));
		man.ped.setPed_est(txtPedEst.getText());
		// man.ped.setPed_ver(0);
	}// Fin de obtenerDatoDlgVenta()

	public void obtenerDatoDlgCliente() {

		man.cli.setCli_ide(txtCliIde.getText());
		man.cli.setCli_doc(txtCliDoc.getText());
		man.cli.setCli_num(txtCliNum.getText());
		man.cli.setCli_nom(txtCliNom.getText());
		man.cli.setCli_dir(txtCliDir.getText());
		// man.cli.setCli_tel("");
		// man.cli.setCli_email(resSql.getString("cli_email"));
		// man.cli.setCli_web(resSql.getString("cli_web"));
		// man.cli.setCli_act(resSql.getString("cli_act"));
		// man.cli.setCli_ver(1);

	}// Fin de obtenerDatoDlgVenta()

	private void registrarClase(String parGruNom, String parClaNom) {

		if (!man.existeClase(parClaNom)) {

			man.gru.setObjeto(man.obtenerGrupo(parGruNom));
			man.cla.setGru_cod(man.gru.getGru_cod());
			man.cla.setCla_nom(parClaNom);
			man.cla.setCla_des("");
			man.cla.setCla_ver(1);

			man.agregarClase();
		}

	}

	private void cmdCerrarActionPerformed(ActionEvent arg0) {

		cerrarFormulario();
		cerrar();

	}

	/**
	 * 
	 * 
	 * Configuracion de la lista desplegable de cliente
	 * 
	 * 
	 */
	private void txtPerNomKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtPerNomKeyReleased

		if (txtCliNom.getText().trim().length() == 0) {

			scrLisCliNom.setVisible(false);
			lisCliNom.setVisible(false);
			busLisCliNom = true;

		} else if (txtCliNom.getText().trim().length() > 0) {
			if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {

				busLisCliNom = true;

			}
			if (evt.getKeyCode() == KeyEvent.VK_DOWN) {

				if (varLisCliNom.size() > 0) {

					scrLisCliNom.setVisible(true);
					lisCliNom.setVisible(true);
					lisCliNom.requestFocus();
					lisCliNom.setSelectedIndex(0);
					busLisCliNom = true;

				}
			}
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

				lisPerNomMousePressed(null);

			} else if ((evt.getKeyCode() != KeyEvent.VK_DOWN) && (busLisCliNom == true) && (estDlgPed.equals(EstadoPagina.AGREGANDO) || estDlgPed.equals(EstadoPagina.MODIFICANDO))) {

				modLisCliNom.removeAllElements();
				varLisCliNom = man.obtenerListaNombreCliente(txtCliNom.getText());
				for (int iteLis = 0; iteLis < varLisCliNom.size(); iteLis++) {
					modLisCliNom.addElement(varLisCliNom.get(iteLis));
				}

				varLisCliNom = man.obtenerListaNombreCliente(txtCliNom.getText().toUpperCase());
				for (String iteLisCliNom : varLisCliNom) {
					if (!modLisCliNom.contains(iteLisCliNom)) {
						modLisCliNom.addElement(iteLisCliNom);
					}
				}
				if (varLisCliNom.size() > 0) {

					scrLisCliNom.setVisible(true);
					lisCliNom.setVisible(true);
					busLisCliNom = true;

				} else {

					scrLisCliNom.setVisible(false);
					lisCliNom.setVisible(false);
					busLisCliNom = false;

				}// VISUALIZAR LA LISTA SI HAY DATOS QUE MOSTRAR
			}
		}// FIN DE EVALUAR QUE HAY ALGUN TEXTO EN NOMBRE
	}// GEN-LAST:event_txtPerNomKeyReleased

	private void lisPerNomMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_lisPerNomMousePressed

		scrLisCliNom.setVisible(false);
		lisCliNom.setVisible(false);
		if (lisCliNom.getSelectedIndex() > -1) {
			txtCliNom.setText((String) lisCliNom.getSelectedValue());
		}

		ClienteDAO cliDAO = (ClienteDAO) ContenedorTextil.getComponent("ClienteDAO");
		man.cli.setObjeto(cliDAO.leerPersonaNombre(txtCliNom.getText().trim()));

		llenarDlgCliente();

	}// GEN-LAST:event_lisPerNomMousePressed

	private void lisPerNomKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lisPerNomKeyReleased

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {

			lisCliNom.setVisible(false);
			txtCliNom.requestFocus();

		}
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			lisPerNomMousePressed(null);

		}// FIN DE EVENTO TECLA ENTER
	}// GEN-LAST:event_lisPerNomKeyReleased

	/*
	 * 
	 * fin de configuracionde lista de cliente
	 */

	private void txtPedNumDocKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER && estDlgPed.equals(EstadoPagina.BUSCANDO)) {

			man.ped.setObjeto(man.obtenerPedido(etiPedNomDoc.getText(), txtPedNumDoc.getText()));
			bookMarkPedCod = man.ped.getPed_cod();

		}// fin de if(estDlgPed.equals(estadoVentana.BUSCANDO))
	}// Fin de private void txtPedNumDocKeyReleased(KeyEvent arg0)

	private void cmdPedAgregarActionPerformed(ActionEvent arg0) {
		if (cmdPedAgregar.getText().equals("Agregar")) {
			bookMarkNomDoc = TipoDocumento.NOTAPEDIDO.getVal();
			if (!bookMarkNomDoc.equals("")) {

				estDlgPed = EstadoPagina.AGREGANDO;

				bookMarkPedCod = man.ped.getPed_cod();
				bookMarkDetPedCod = man.detPed.getDetped_cod();

				limpiarFormulario();
				refrescarFormulario();

				man.nuevoNumeroDocumento(bookMarkNomDoc);
				man.nuevoRegistroPedido();
				llenarDlgPedido();

				txtEditPedFecReg.requestFocus();

			}// Fin de if(!bookMarkNomDoc.equals(""))
		}// Fin de if(accion.equals(accionVentana.INICIAR))
		else if (cmdPedAgregar.getText().equals("Guardar")) {
			int resMsg = JOptionPane.showConfirmDialog(this,
					"���Desea guardar el pedido para produccion!!!",
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (resMsg == JOptionPane.YES_OPTION) {				
				if (validarDlgPedido()) {
					activarFormulario(false);
					cmdPedAgregar.setEnabled(false);
					cmdCerrar.setEnabled(false);
					final SwingWorker worker = new SwingWorker() {
				          public Object construct() {
				            
								/** **********************************************************
								 * GUARDAR MODELO, PRODUCTO, TALLA IMPORTADOS DESDE EL EXCEL *
								 *************************************************************/
								obtenerDatoDlgCliente();
								obtenerDatoDlgPedido();

								bookMarkCliCod = man.agregarCliente(0, txtCliIde.getText());
								if (bookMarkCliCod == 0) 
								{
									bookMarkCliCod = man.actualizarCliente(man.cli.getCli_cod(), txtCliIde.getText());
								}
								barraProgreso.setMaximum(man.lisModImp.size());
								for (int iteModTmp = 0; iteModTmp < man.lisModImp.size(); iteModTmp++) 
								{
									Map rstModTmp = (Map) man.lisModImp.get(iteModTmp);

									/** *************************************
									 * GUARDAR CLIENTE Y PEDIDO DEL CLIENTE *
									 ****************************************/
									man.nuevoNumeroDocumento(bookMarkNomDoc);
									etiPedNomDoc.setText(man.ped.getPed_nomdoc());
									txtPedNumDoc.setText(man.ped.getPed_numdoc());
									man.ped.setPed_src("IMPORTACION");
									
									bookMarkPedCod = man.agregarPedido();
									man.ped.setPed_cod(bookMarkPedCod);
									
									if (!guardarLibroExcel(rstModTmp))
									{
										man.eliminarListaFicha(man.ped.getPed_cod());
										man.eliminarListaDetPedido(man.ped.getPed_cod());
										man.eliminarPedido(man.ped.getPed_cod());
										man.ped.limpiarInstancia();
										man.detPed.limpiarInstancia();
										break;
									}
									barraProgreso.setValue(iteModTmp+1);
								}// for(int iteModTmp = 0; iteModTmp <
								
								cmdCerrarActionPerformed(null);				        	  
				            return null;
				          }
				          //una vez terminada la tarea, se
				          //elimina el dialogo
				          public void finished(){
								cmdPedAgregar.setEnabled(true);
								cmdCerrar.setEnabled(true);
				          }
				        };
				        worker.start();
				}// fin de if (validarDlgContacto())
			}// Fin de if (resMsg == JOptionPane.YES_OPTION)
		}// Fin de if(accion.equals(accionVentana.ACTUALIZAR)) }
	}
	public boolean guardarLibroExcel(Map parRstMod)
	{
		int bookMarkDetPedCod = 0;
		boolean exito = true;
		
		Producto tmpMod = new Producto();
				
		tmpMod.setPrd_ide((String) parRstMod.get("prd_ide"));
		tmpMod.setPrd_tip((String) parRstMod.get("prd_tip"));
		tmpMod.setPrd_mat((String) parRstMod.get("prd_mat"));
		tmpMod.setPrd_gau((String) parRstMod.get("prd_gau"));
		tmpMod.setPrd_gro("");
		tmpMod.setPrd_col("");
		tmpMod.setPrd_des((String) parRstMod.get("prd_des"));
		
		if(registrarModelo(tmpMod) != 0)
		{
			bookMarkDetPedCod = registrarDetPedido(man.mod, man.gruMod, 0);
			List lisPrdTmp = (List) parRstMod.get("listaProducto");
			for(int itePrdTmp = 0; itePrdTmp < lisPrdTmp.size(); itePrdTmp++)
			{
				Map rstPrdTmp = (Map) lisPrdTmp.get(itePrdTmp);
			
				Producto tmpPrd = new Producto();
				
				tmpPrd.setPrd_ide((String) rstPrdTmp.get("prd_ide"));
				tmpPrd.setPrd_tip(tmpMod.getPrd_tip());
				tmpPrd.setPrd_mat((String) rstPrdTmp.get("prd_mat"));
				tmpPrd.setPrd_gau(tmpMod.getPrd_gau());
				tmpPrd.setPrd_gro(tmpMod.getPrd_gro());
				tmpPrd.setPrd_col((String) rstPrdTmp.get("prd_col"));
				tmpPrd.setPrd_des((String) rstPrdTmp.get("prd_des"));
				
				if(registrarProducto(tmpPrd) != 0)
				{
					bookMarkDetPedCod = registrarDetPedido(man.prd, man.gruPrd, 0);
					Map rstLisTal = (Map) rstPrdTmp.get("listaTalla");
					Iterator iteTal = rstLisTal.entrySet().iterator();
					while (iteTal.hasNext()) {
						Map.Entry entTal = (Map.Entry) iteTal.next();
					
						Producto tmpTal = new Producto();
						
						tmpTal.setPrd_ide(tmpPrd.getPrd_ide()+"-"+(String) entTal.getKey());
						tmpTal.setPrd_tip(tmpMod.getPrd_tip());
						tmpTal.setPrd_mat(tmpMod.getPrd_mat());
						tmpTal.setPrd_gau(tmpMod.getPrd_gau());
						tmpTal.setPrd_gro(tmpMod.getPrd_gro());
						tmpTal.setPrd_col(tmpMod.getPrd_col());
						tmpTal.setPrd_des((String) entTal.getKey());
						tmpTal.setPrd_stkact(0);
														
						if(registrarTalla(tmpTal) != 0)
						{
							bookMarkDetPedCod = registrarDetPedido(man.tal, man.gruTal, Integer.parseInt(entTal.getValue().toString()));
						}
						else
						{
							JOptionPane.showConfirmDialog(this,
									"Este TALLA " + tmpTal.getPrd_ide() + " ya fue ingresado en pedido\n" + this.getClass().getName(),
									man.testProp.getPropiedad("TituloSistema"),
									JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);
							exito = false;
							break;
						}
					}//while (iteTal.hasNext()) {
					
					/** ****************************************************
					 * ACTUALILZAR TOTAL DE TALLAS POR PRODUCTO INGRESADO  *
					 *******************************************************/
					if (exito)
					{
						man.detPed.setObjeto(man.obtenerDetPedido(bookMarkDetPedCod));
						man.actualizarTotalDetPedido(man.ped.getPed_cod(), man.tal.getPrd_nivel(), man.prd.getPrd_cod());						
					}
					else{break;}
				}//if(registrarProducto(tmpPrd) != 0)
				else
				{
					JOptionPane.showConfirmDialog(this,
							"Este PRODUCTO " + tmpPrd.getPrd_ide() + " ya fue ingresado en pedido\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);
					exito = false;
					break;
				}
			}//for(int itePrdTmp = 0; itePrdTmp < lisPrdTmp.size(); itePrdTmp++)
			/** *****************************************************
			 * ACTUALILZAR TOTAL DE PRODUCTOS POR MODELO INGRESADO  *
			 ********************************************************/
			if (exito)
			{
				man.detPed.setObjeto(man.obtenerDetPedido(bookMarkDetPedCod));
				man.actualizarTotalDetPedido(man.ped.getPed_cod(), man.prd.getPrd_nivel(), man.mod.getPrd_cod());
			}
		}//if(registrarModelo(tmpMod) != 0)
		else
		{
			JOptionPane.showConfirmDialog(this,
					"El MODELO " + tmpMod.getPrd_ide() + " ya fue ingresado en pedido\n" + this.getClass().getName(),
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);
			exito = false;			
		}
		return exito;
	}//public void guardarLibroExcel(Map parMod, Map parPrd)
	public int registrarModelo(Producto parObj)
	{
		int bookMark = 0;
		
		if(!parObj.getPrd_tip().isEmpty()){registrarClase("Tipo", parObj.getPrd_tip());}
		if(!parObj.getPrd_gau().isEmpty()){registrarClase("Gauge", parObj.getPrd_gau());}
		if(!parObj.getPrd_gro().isEmpty()){registrarClase("Grosor", parObj.getPrd_gro());}
		
		man.gruMod.setObjeto(man.obtenerGrupo("Modelo"));
		man.mod.setObjeto(man.obtenerProductoIde(parObj.getPrd_ide()));
		bookMark = man.mod.getPrd_cod();
		if (bookMark == 0)
		{
			man.mod.limpiarInstancia();
			generarIdentificacionModelo(parObj.getPrd_ide());
			// man.mod.setPrd_cod(0);//Autonumerico
			man.mod.setGru_cod(man.gruMod.getGru_cod());// obtiene el codigo de grupo
			man.mod.setPrd_ide(parObj.getPrd_ide());
			man.mod.setPrd_tip(parObj.getPrd_tip());
			man.mod.setPrd_mat(parObj.getPrd_mat());
			man.mod.setPrd_gau(parObj.getPrd_gau());
			man.mod.setPrd_gro(parObj.getPrd_gro());
			man.mod.setPrd_col(parObj.getPrd_col());
			man.mod.setPrd_des(parObj.getPrd_des());
			man.mod.setPrd_stkact(0);
			man.mod.setPrd_supuno(0);
			man.mod.setPrd_supdos(0);
			man.mod.setPrd_suptres(0);
			man.mod.setPrd_supcua(0);
			man.mod.setPrd_nivel(man.gruMod.getGru_nivel());
			bookMark = man.agregarProductoSinValidar(man.mod);
			man.mod.setPrd_cod(bookMark);
		}
		return bookMark;
	}//public void registrarModelo(String modelo, String tipo, String material, String gauge, String grosor, String color)
	public int registrarProducto(Producto parObj)
	{
		int bookMark;

		if(!parObj.getPrd_mat().isEmpty()){registrarClase("Material", parObj.getPrd_mat());}
		if(!parObj.getPrd_col().isEmpty()){registrarClase("Color", parObj.getPrd_col());}
		
		man.gruPrd.setObjeto(man.obtenerGrupo("Producto"));
		man.prd.setObjeto(man.obtenerProductoIde(parObj.getPrd_ide()));
		bookMark = man.prd.getPrd_cod();
		if (bookMark == 0)
		{
			man.prd.limpiarInstancia();
			generarIdentificacionProducto(parObj.getPrd_ide());
			// man.mod.setPrd_cod(0);//Autonumerico
			man.prd.setGru_cod(man.gruPrd.getGru_cod());// obtiene el codigo de grupo
			man.prd.setPrd_ide(parObj.getPrd_ide());
			man.prd.setPrd_tip(parObj.getPrd_tip());
			man.prd.setPrd_mat(parObj.getPrd_mat());
			man.prd.setPrd_gau(parObj.getPrd_gau());
			man.prd.setPrd_gro(parObj.getPrd_gro());
			man.prd.setPrd_col(parObj.getPrd_col());
			man.prd.setPrd_des(parObj.getPrd_des());
			man.prd.setPrd_stkact(0);			
			man.prd.setPrd_supuno(man.mod.getPrd_cod());
			man.prd.setPrd_supdos(0);
			man.prd.setPrd_suptres(0);
			man.prd.setPrd_supcua(0);
			man.prd.setPrd_nivel(man.gruPrd.getGru_nivel());
			bookMark = man.agregarProductoSinValidar(man.prd);
			man.prd.setPrd_cod(bookMark);
		}
		return bookMark;
	}//public void registrarProducto
	
	public int registrarTalla(Producto parObj)
	{
		int bookMark;
		man.gruTal.setObjeto(man.obtenerGrupo("Talla"));		
		man.tal.setObjeto(man.obtenerProductoIde(parObj.getPrd_ide()));
		bookMark = man.tal.getPrd_cod();
		if (bookMark == 0)
		{
			man.tal.limpiarInstancia();
			generarIdentificacionTalla(parObj.getPrd_ide());
			// man.mod.setPrd_cod(0);//Autonumerico
			man.tal.setGru_cod(man.gruTal.getGru_cod());// obtiene el codigo de grupo
			man.tal.setPrd_ide(parObj.getPrd_ide());
			man.tal.setPrd_tip(parObj.getPrd_tip());
			man.tal.setPrd_mat(parObj.getPrd_mat());
			man.tal.setPrd_gau(parObj.getPrd_gau());
			man.tal.setPrd_gro(parObj.getPrd_gro());
			man.tal.setPrd_col(parObj.getPrd_col());
			man.tal.setPrd_des(parObj.getPrd_des());
			man.tal.setPrd_stkact(0);			
			man.tal.setPrd_supuno(man.mod.getPrd_cod());
			man.tal.setPrd_supdos(man.prd.getPrd_cod());
			man.tal.setPrd_suptres(0);
			man.tal.setPrd_supcua(0);
			man.tal.setPrd_nivel(man.gruTal.getGru_nivel());
			bookMark = man.agregarProductoSinValidar(man.tal);
			man.tal.setPrd_cod(bookMark);
		}
		
		return bookMark;
	}//public void registrarTall	
	public int registrarDetPedido(Producto parPrd, Grupo parGru, int cantidad) 
	{
		int bookMark = 0;
		obtenerDatoDlgDetPedido(parPrd, parGru, cantidad);
		if (man.existeProductoDetPedido(man.ped.getPed_cod(), parPrd.getPrd_cod(), parPrd.getPrd_ide())) 
		{
			bookMark = man.updateDetPedido(parPrd, parGru);
		}
		else
		{	
			bookMark = man.addDetPedido(parPrd, parGru);
		}
		man.detPed.setDetped_cod(bookMark);
		return bookMark;

	}
	private void obtenerDatoDlgDetPedido(Producto parPrd, Grupo parGru, int cantidad) {

		// man.detPed.setDetped_cod("AUTONUMERICO");
		man.detPed.setPed_cod(man.ped.getPed_cod());
		man.detPed.setPrd_cod(parPrd.getPrd_cod());
		man.detPed.setGru_cod(parGru.getGru_cod());
		man.detPed.setDetped_canped(cantidad);
		// man.detPed.setDetped_canfab(resSql.getInt("detped_canfab"));
		// man.detPed.setDetped_canent(resSql.getInt("detped_canent"));
		// man.detPed.setDetped_pre(resSql.getDouble("detped_pre"));
		// man.detPed.setDetped_imp(resSql.getDouble("detped_imp"));
		man.detPed.setDetped_est(EstadoDocumento.PENDIENTE.getVal());
		// man.detPed.setDetped_ver();
	}
	public void generarIdentificacionModelo(String parIde) 
	{
		boolean existe = true;
		String codigo = "";
		while (existe) {
			codigo = KeyGenerator.getInst().obtenerCodigoBarra();
			if (!man.existeCodigoBarra("PRODUCTO", codigo)) {
				man.mod.setPrd_ide(parIde);
				man.mod.setPrd_bar(codigo);
				existe = false;
			}
		}
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)
	public void generarIdentificacionProducto(String parIde) 
	{
		boolean existe = true;
		String codigo = "";
		while (existe) {
			codigo = KeyGenerator.getInst().obtenerCodigoBarra();
			if (!man.existeCodigoBarra("PRODUCTO", codigo)) {
				man.prd.setPrd_ide(parIde);
				man.prd.setPrd_bar(codigo);
				existe = false;
			}
		}
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)
	public void generarIdentificacionTalla(String parIde) 
	{
		boolean existe = true;
		String codigo = "";
		while (existe) {
			codigo = KeyGenerator.getInst().obtenerCodigoBarra();
			if (!man.existeCodigoBarra("PRODUCTO", codigo)) {
				man.tal.setPrd_ide(parIde);
				man.tal.setPrd_bar(codigo);
				existe = false;
			}
		}
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)	
}// Fin de Clase Principal