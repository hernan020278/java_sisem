package com.textil.main;

import java.awt.GridLayout;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainClass {

  public static void main(String argv[]) {

    java.net.URL u = null;
    try {
      u = new java.net.URL("http://www.java2s.com/");
    } catch (java.net.MalformedURLException ignored) {
    }

    JFormattedTextField ftf1 = new JFormattedTextField(u);
    JFormattedTextField ftf2 = new JFormattedTextField(u);

    ftf2.setInputVerifier(new InputVerifier() {
      public boolean verify(JComponent input) {
        if (!(input instanceof JFormattedTextField))
          return true;
        return ((JFormattedTextField) input).isEditValid();
      }
    });

    JPanel p = new JPanel(new GridLayout(0, 2, 3, 8));
    p.add(new JLabel("plain JFormattedTextField:"));
    p.add(ftf1);
    p.add(new JLabel("FTF with InputVerifier:"));
    p.add(ftf2);
    p.add(new JLabel("plain JTextField:"));
    p.add(new JTextField(u.toString()));

    JFrame f = new JFrame("MainClass");
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.getContentPane().add(new JLabel("Try to delete the colon in each field."), "North");
    f.getContentPane().add(p, "Center");
    f.pack();
    f.setVisible(true);
  }
}
