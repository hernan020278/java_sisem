package com.textil.main;

import java.awt.AWTKeyStroke;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Area;
import com.comun.entidad.SubArea;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.CodigoBarras;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;
import com.comun.utilidad.swing.JTextFieldChanged;
import com.comun.utilidad.swing.PanelImgFoto;
import com.textil.controlador.AuxiliarTextil;
import com.textil.controlador.AuxiliarTextil.AccesoPagina;
import com.textil.controlador.AuxiliarTextil.EstadoPagina;
import com.textil.controlador.ContenedorTextil;
import com.textil.dao.TransactionDelegate;

/**
 * 
 * @author Hernan
 */
public class DlgArea extends javax.swing.JDialog implements InterfaceUsuario {

	/**
	 * Variables principales
	 */
	public EstadoPagina estDlgAre, estDlgSubAre;
	public AccesoPagina acceso;
	public AuxiliarTextil man;
	public String padre;
	public boolean frmAbierto;
	/**
     *
     */
	public String bookMarkAreNom, bookmarString;
	public int bookMarkAreCod, bookMarkSubAreCod;
	/**
	 * 
	 */
	private ModeloTablaArea modTabAre;
	private ModeloTablaSubArea modTabSubAre;
	private TableColumn[] colTabAre;
	private TableColumn[] colTabSubAre;
	private CeldaRenderGeneral modTabCellRender;

	/**
	 * Creates new form DlgArea
	 */
	public DlgArea(FrmPrincipal parent) {

		super(parent, false);
		setTitle("Administracion de las Areas : "+this.getClass().getSimpleName());
		setResizable(false);

		initComponents();

		// Conjunto de teclas que queremos que sirvan para pasar el foco
		// al siguiente campo de texto: ENTER y TAB
		Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
		teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		// teclas.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0));

		// Se pasa el conjunto de teclas al panel principal
		super.getContentPane().setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);

		cmdSubAreImprimir = new JButton();
		cmdSubAreImprimir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdSubAreImprimir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdSubAreImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdSubAreImprimirActionPerformed(e);

			}

		});
		cmdSubAreImprimir.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		cmdSubAreImprimir.setText("Imprimir");
		cmdSubAreImprimir.setMargin(new Insets(0, 0, 0, 0));
		cmdSubAreImprimir.setBounds(5, 440, 113, 40);
		getContentPane().add(cmdSubAreImprimir);

		cmdAreImprimir = new JButton();
		cmdAreImprimir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdAreImprimir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAreImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cmdAreImprimirActionPerformed(e);

			}

		});
		cmdAreImprimir.setIcon(AplicacionGeneral.getInstance().obtenerImagen("imprimir.png"));
		cmdAreImprimir.setText("Imprimir");
		cmdAreImprimir.setMargin(new Insets(0, 0, 0, 0));
		cmdAreImprimir.setBounds(726, 126, 102, 40);
		getContentPane().add(cmdAreImprimir);

		txtAreBar = new JTextFieldChanged(50);
		txtAreBar.setEnabled(false);
		txtAreBar.setText("aplications web solutions SRL");
		txtAreBar.setBounds(920, 107, 92, 20);
		getContentPane().add(txtAreBar);

		txtSubAreBar = new JTextFieldChanged(50);
		txtSubAreBar.setEnabled(false);
		txtSubAreBar.setText("aplications web solutions SRL");
		txtSubAreBar.setBounds(918, 135, 92, 20);
		getContentPane().add(txtSubAreBar);
		panSubAreBar.setBounds(153, 17, 192, 71);
		panSubArea.add(panSubAreBar);
		panSubAreBar.setImagen(null, null, null, "CONSTRAINT");

		crearModeloTablas();

		super.addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {

				frmAbierto = true;

			}
		});

	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		panArea = new javax.swing.JPanel();
		etiCliNom3 = new javax.swing.JLabel();
		etiCliNom3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiCliNom3.setBorder(null);
		etiCliNom = new javax.swing.JLabel();
		etiCliNom.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiCliNom.setBorder(null);
		etiCliNom2 = new javax.swing.JLabel();
		etiCliNom2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiCliNom2.setBorder(null);
		scrTabAre = new javax.swing.JScrollPane();
		scrTabAre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabAre = new javax.swing.JTable();
		tabAre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAreIde = new JTextFieldChanged(11);
		txtAreIde.setHorizontalAlignment(SwingConstants.CENTER);
		txtAreIde.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAreNom = new JTextFieldChanged(50);
		txtAreNom.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAreDes = new JTextFieldChanged(80);
		txtAreDes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				txtAreDesKeyRealeased(e);

			}
		});
		txtAreDes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panSubArea = new javax.swing.JPanel();
		scrTabSubAre = new javax.swing.JScrollPane();
		scrTabSubAre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabSubAre = new javax.swing.JTable();
		tabSubAre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdAreAgregar = new javax.swing.JButton();
		cmdAreAgregar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdAreModificar = new javax.swing.JButton();
		cmdAreModificar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdAreBuscar = new javax.swing.JButton();
		cmdAreBuscar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdCerrar = new javax.swing.JButton();
		cmdCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdAreEliminar = new javax.swing.JButton();
		cmdAreEliminar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdSubAreAgregar = new javax.swing.JButton();
		cmdSubAreAgregar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdSubAreModificar = new javax.swing.JButton();
		cmdSubAreModificar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdSubAreEliminar = new javax.swing.JButton();
		cmdSubAreEliminar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cmdSubAreBuscar = new javax.swing.JButton();
		cmdSubAreBuscar.setMargin(new Insets(0, 0, 0, 0));
		cmdSubAreBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png"));

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);

		panArea.setBorder(new TitledBorder(null, "Adminstracion de las Area de la Empresa", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panArea.setLayout(null);

		etiCliNom3.setHorizontalAlignment(SwingConstants.CENTER);
		etiCliNom3.setText("Codigo Barra");
		panArea.add(etiCliNom3);
		etiCliNom3.setBounds(10, 15, 261, 20);

		etiCliNom.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		etiCliNom.setText("Nombre");
		panArea.add(etiCliNom);
		etiCliNom.setBounds(10, 68, 261, 20);

		etiCliNom2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		etiCliNom2.setText("Descripcion");
		panArea.add(etiCliNom2);
		etiCliNom2.setBounds(281, 68, 123, 20);

		scrTabAre.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		tabAre.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] {
						{},
						{},
						{},
						{}
				},
				new String[] {

				}
				));
		tabAre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabAre.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				tabAreMousePressed(evt);
			}
		});
		tabAre.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				tabAreKeyReleased(evt);
			}
		});
		scrTabAre.setViewportView(tabAre);

		panArea.add(scrTabAre);
		scrTabAre.setBounds(10, 120, 691, 128);

		txtAreIde.setText("14438399310");
		panArea.add(txtAreIde);
		txtAreIde.setBounds(10, 35, 261, 31);

		txtAreNom.setText("aplications web solutions SRL");
		txtAreNom.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				txtAreNomKeyReleased(evt);
			}
		});
		panArea.add(txtAreNom);
		txtAreNom.setBounds(10, 89, 261, 20);

		txtAreDes.setText("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		panArea.add(txtAreDes);
		txtAreDes.setBounds(282, 89, 419, 20);

		panArea.add(panAreBar);
		panAreBar.setBounds(407, 11, 294, 77);

		getContentPane().add(panArea);
		panArea.setBounds(5, 5, 711, 259);

		panSubArea.setBorder(new TitledBorder(null, "Administrar las SubAreas de la Empresa", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panSubArea.setLayout(null);

		scrTabSubAre.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		tabSubAre.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] {
						{},
						{}
				},
				new String[] {

				}
				));
		tabSubAre.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
		tabSubAre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tabSubAre.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				tabSubAreMousePressed(evt);
			}
		});
		tabSubAre.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				tabSubAreKeyReleased(evt);
			}
		});
		scrTabSubAre.setViewportView(tabSubAre);

		panSubArea.add(scrTabSubAre);
		scrTabSubAre.setBounds(10, 120, 680, 212);

		getContentPane().add(panSubArea);
		panSubArea.setBounds(128, 271, 700, 343);

		JLabel label = new JLabel();
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setText("Codigo Barra");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBorder(null);
		label.setBounds(10, 15, 137, 20);
		panSubArea.add(label);

		txtSubAreIde = new JTextFieldChanged(11);
		txtSubAreIde.setHorizontalAlignment(SwingConstants.CENTER);
		txtSubAreIde.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtSubAreIde.setText("14438399310");
		txtSubAreIde.setBounds(10, 35, 137, 31);
		panSubArea.add(txtSubAreIde);

		JLabel label_1 = new JLabel();
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setText("Nombre");
		label_1.setHorizontalAlignment(SwingConstants.LEFT);
		label_1.setBorder(null);
		label_1.setBounds(10, 68, 137, 20);
		panSubArea.add(label_1);

		txtSubAreNom = new JTextFieldChanged(50);
		txtSubAreNom.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtSubAreNom.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				txtSubAreNomKeyReleased(arg0);

			}

		});
		txtSubAreNom.setText("aplications web solutions SRL");
		txtSubAreNom.setBounds(10, 89, 190, 20);
		panSubArea.add(txtSubAreNom);

		txtSubAreDes = new JTextFieldChanged(80);
		txtSubAreDes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				txtSubAreDesKeyReleased(e);

			}
		});
		txtSubAreDes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtSubAreDes.setText("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		txtSubAreDes.setBounds(355, 37, 335, 20);
		panSubArea.add(txtSubAreDes);

		JLabel label_2 = new JLabel();
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setText("Descripcion");
		label_2.setHorizontalAlignment(SwingConstants.LEFT);
		label_2.setBorder(null);
		label_2.setBounds(354, 16, 336, 20);
		panSubArea.add(label_2);

		JLabel lblIntegrantes = new JLabel();
		lblIntegrantes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIntegrantes.setText("Integrantes");
		lblIntegrantes.setHorizontalAlignment(SwingConstants.LEFT);
		lblIntegrantes.setBorder(null);
		lblIntegrantes.setBounds(355, 68, 335, 20);
		panSubArea.add(lblIntegrantes);

		txtSubAreInt = new JTextFieldChanged(150);
		txtSubAreInt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				txtSubAreIntKeyReleased(e);

			}
		});
		txtSubAreInt.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtSubAreInt.setText("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		txtSubAreInt.setBounds(207, 89, 483, 20);
		panSubArea.add(txtSubAreInt);

		cmdAreAgregar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png")); // NOI18N
		cmdAreAgregar.setText("Agregar");
		cmdAreAgregar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAreAgregar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdAreAgregar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdAreAgregarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdAreAgregar);
		cmdAreAgregar.setBounds(726, 5, 102, 40);

		cmdAreModificar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("modificar.png")); // NOI18N
		cmdAreModificar.setText("Modificar");
		cmdAreModificar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAreModificar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdAreModificar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdAreModificarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdAreModificar);
		cmdAreModificar.setBounds(726, 45, 102, 40);

		cmdAreBuscar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("buscar.png")); // NOI18N
		cmdAreBuscar.setText("Buscar");
		cmdAreBuscar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAreBuscar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdAreBuscar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdAreBuscarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdAreBuscar);
		cmdAreBuscar.setBounds(726, 173, 102, 40);

		cmdCerrar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("cancelar.png")); // NOI18N
		cmdCerrar.setText("Cerrar");
		cmdCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdCerrar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdCerrar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdCerrarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdCerrar);
		cmdCerrar.setBounds(726, 224, 102, 40);

		cmdAreEliminar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("eliminar.png")); // NOI18N
		cmdAreEliminar.setText("Eliminar");
		cmdAreEliminar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdAreEliminar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdAreEliminar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdAreEliminarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdAreEliminar);
		cmdAreEliminar.setBounds(726, 85, 102, 40);

		cmdSubAreAgregar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("agregar.png")); // NOI18N
		cmdSubAreAgregar.setText("Agregar");
		cmdSubAreAgregar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdSubAreAgregar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdSubAreAgregar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdSubAreAgregarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdSubAreAgregar);
		cmdSubAreAgregar.setBounds(5, 287, 113, 40);

		cmdSubAreModificar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("modificar.png")); // NOI18N
		cmdSubAreModificar.setText("Modificar");
		cmdSubAreModificar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdSubAreModificar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdSubAreModificar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdSubAreModificarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdSubAreModificar);
		cmdSubAreModificar.setBounds(5, 338, 113, 40);

		cmdSubAreEliminar.setIcon(AplicacionGeneral.getInstance().obtenerImagen("eliminar.png")); // NOI18N
		cmdSubAreEliminar.setText("Eliminar");
		cmdSubAreEliminar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cmdSubAreEliminar.setMargin(new java.awt.Insets(0, 0, 0, 0));
		cmdSubAreEliminar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdSubAreEliminarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdSubAreEliminar);
		cmdSubAreEliminar.setBounds(5, 389, 113, 40);

		cmdSubAreBuscar.setText("Buscar");
		cmdSubAreBuscar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmdSubAreBuscarActionPerformed(evt);
			}
		});
		getContentPane().add(cmdSubAreBuscar);
		cmdSubAreBuscar.setBounds(920, 10, 90, 35);

		setSize(new Dimension(838, 650));
		setLocationRelativeTo(null);
	}// </editor-fold>//GEN-END:initComponents

	private void tabAreMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_tabAreMousePressed
		if (tabAre.getSelectedRow() > -1
				&& (estDlgAre.equals(EstadoPagina.VISUALIZANDO)
				|| estDlgAre.equals(EstadoPagina.BUSCANDO))) {

			estDlgAre = EstadoPagina.VISUALIZANDO;
			estDlgSubAre = EstadoPagina.VISUALIZANDO;

			seleccionarRegistroTablaArea();

		}// FIN DE VERIFICAR QUE EL REGISTRO SELECCIONADO ESTE EN LA TABLA
	}// GEN-LAST:event_tabAreMousePressed

	private void tabAreKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_tabAreKeyReleased
		if (evt.getKeyCode() == KeyEvent.VK_UP
				|| evt.getKeyCode() == KeyEvent.VK_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_PAGE_UP
				|| evt.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_ENTER
				|| evt.getKeyCode() == KeyEvent.VK_F2
				|| evt.getKeyCode() == KeyEvent.VK_TAB) {

			tabAreMousePressed(null);

		}// Fin de if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN)
	}// GEN-LAST:event_tabAreKeyReleased

	private void txtAreNomKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtAreNomKeyReleased
		if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

			txtAreDes.requestFocus();

		} else {
			if (estDlgAre.equals(EstadoPagina.AGREGANDO) || estDlgAre.equals(EstadoPagina.MODIFICANDO)) {

				man.are.setAre_nom(txtAreNom.getText().trim());
				if (txtAreNom.getText().trim().length() == 3) {

					generarIdentificacion("AREA", txtAreNom.getText());

				}
			}// Fin de if (txtPerNom.getText().trim().length() > 0 &&
				// estDlgPer.equals(EstadoPagina.BUSCANDO))
			else if (txtAreNom.getText().trim().length() > 0 && estDlgAre.equals(EstadoPagina.BUSCANDO)) {

				bookMarkAreNom = txtAreNom.getText().trim();
				man.obtenerListaArea(bookMarkAreNom);
				llenarTablaArea();

			}// Verificar que se esta buscando un dato
			else if (txtAreNom.getText().trim().length() == 0 && estDlgAre.equals(EstadoPagina.BUSCANDO)) {

				bookMarkAreNom = "TODOS";
				man.obtenerListaArea(bookMarkAreNom);
				llenarTablaArea();

			}// Verificar que se esta buscando un dato
		}
	}// GEN-LAST:event_txtAreNomKeyReleased

	private void txtSubAreNomKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

			txtSubAreDes.requestFocus();

		} else {
			if (estDlgSubAre.equals(EstadoPagina.AGREGANDO) || estDlgSubAre.equals(EstadoPagina.MODIFICANDO)) {

				man.subAre.setSubare_nom(txtAreNom.getText().trim());
				generarIdentificacion("SUBAREA", txtSubAreNom.getText());

			}// Fin de if (txtPerNom.getText().trim().length() > 0 &&
		}
	}

	private void txtSubAreDesKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

			txtSubAreInt.requestFocus();

		}
	}

	private void txtSubAreIntKeyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

			txtSubAreNom.requestFocus();

		}
	}

	private void txtAreDesKeyRealeased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB) {

			txtAreNom.requestFocus();

		}
	}

	private void tabSubAreMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_tabSubAreMousePressed
		if (tabSubAre.getSelectedRow() > -1
				&& (estDlgSubAre.equals(EstadoPagina.VISUALIZANDO)
				|| estDlgSubAre.equals(EstadoPagina.BUSCANDO))) {

			estDlgAre = EstadoPagina.VISUALIZANDO;
			estDlgSubAre = EstadoPagina.VISUALIZANDO;

			seleccionarRegistroTablaSubArea();

		}// FIN DE VERIFICAR QUE EL REGISTRO SELECCIONADO ESTE EN LA TABLA
	}// GEN-LAST:event_tabSubAreMousePressed

	private void tabSubAreKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_tabSubAreKeyReleased
		if (evt.getKeyCode() == KeyEvent.VK_UP
				|| evt.getKeyCode() == KeyEvent.VK_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_PAGE_UP
				|| evt.getKeyCode() == KeyEvent.VK_PAGE_DOWN
				|| evt.getKeyCode() == KeyEvent.VK_ENTER
				|| evt.getKeyCode() == KeyEvent.VK_F2
				|| evt.getKeyCode() == KeyEvent.VK_TAB) {

			tabSubAreMousePressed(null);

		}// Fin de if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN)
	}// GEN-LAST:event_tabSubAreKeyReleased

	private void cmdAreAgregarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdAreAgregarActionPerformed
		if (cmdAreAgregar.getText().equals("Agregar")) {

			estDlgAre = EstadoPagina.AGREGANDO;
			estDlgSubAre = EstadoPagina.SUSPENDIDO;

			refrescarFormulario();
			// generarIdentificacion("AREA",txtAreNom.getText());
			txtAreNom.requestFocus();

		} else if (cmdAreAgregar.getText().equals("Guardar")) {
			if (validarDlgArea()) {

				obtenerDatoDlgArea();

				switch (estDlgAre) {

				case AGREGANDO:

					bookMarkAreCod = man.agregarArea();
					break;

				case MODIFICANDO:

					bookMarkAreCod = man.actualizarArea();
					break;

				default:
					break;

				}// Fin de switch (estDlgSubAre)

				if (bookMarkAreCod != 0) {

					man.are.setObjeto(man.obtenerArea(bookMarkAreCod));
					acceso = AccesoPagina.INICIAR;
					estDlgAre = EstadoPagina.VISUALIZANDO;
					iniciarFormulario();

				}// Fin de if(bookMarkDevCod != 0)
				else {

//					man.transactionDelegate.rollback();

					JOptionPane.showConfirmDialog(this,
							"���SE CANCELO LA TRANSACCION!!!"
									+ "\n" + this.getClass().getName(),
							"Sistema",
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

				}
			}// fin de if (validarDlgContacto())
		}// Fin de } else if (cmdContAgregar.getText().equals("Guardar"))
	}// GEN-LAST:event_cmdAreAgregarActionPerformed

	private void cmdAreModificarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdAreModificarActionPerformed
		if (cmdAreModificar.getText().equals("Modificar")) {

			estDlgAre = EstadoPagina.MODIFICANDO;
			estDlgSubAre = EstadoPagina.SUSPENDIDO;

			refrescarFormulario();
			txtAreNom.requestFocus();

		} else if (cmdAreModificar.getText().equals("Cancelar")) {

			acceso = AccesoPagina.INICIAR;
			iniciarFormulario();

		}// FIN DE EVALUAR QUE VA HA HACER EL BOTON
	}// GEN-LAST:event_cmdAreModificarActionPerformed

	private void cmdAreBuscarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdAreBuscarActionPerformed

		estDlgAre = EstadoPagina.BUSCANDO;
		estDlgSubAre = EstadoPagina.SUSPENDIDO;

		refrescarFormulario();
		txtAreNom.requestFocus();

	}// GEN-LAST:event_cmdAreBuscarActionPerformed

	private void cmdCerrarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdCerrarActionPerformed

		cerrar();

	}// GEN-LAST:event_cmdCerrarActionPerformed

	private void cmdAreEliminarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdAreEliminarActionPerformed
		int resMsg = JOptionPane.showConfirmDialog(this,
				"�DESEA ELIMINAR EL AREA " + man.are.getAre_nom() + "?",
				man.testProp.getPropiedad("TituloSistema"),
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (resMsg == JOptionPane.YES_OPTION) {

			boolean exito = man.eliminarArea(man.are.getAre_cod());

			if (exito) {

//				man.transactionDelegate.commit();
				acceso = AccesoPagina.INICIAR;
				iniciarFormulario();

			}// Fin de eliminarDocuemtnoKardex
			else {

//				man.transactionDelegate.rollback();
				JOptionPane.showConfirmDialog(this,
						"���SE CANCELO LA TRANSACCION EN LA ELIMINACION DE FICHAS DE CONTROL!!!"
								+ "\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			}

		}// Fin de if(resMsg == JOptionPane.YES_OPTION)
	}// GEN-LAST:event_cmdAreEliminarActionPerformed

	private void cmdSubAreAgregarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdSubAreAgregarActionPerformed
		if (cmdSubAreAgregar.getText().equals("Agregar")) {

			estDlgAre = EstadoPagina.SUSPENDIDO;
			estDlgSubAre = EstadoPagina.AGREGANDO;

			refrescarFormulario();
			// generarIdentificacion("SUBAREA",txtSubAreNom.getText());
			txtSubAreNom.requestFocus();

		} else if (cmdSubAreAgregar.getText().equals("Guardar")) {
			if (validarDlgSubArea()) {

				obtenerDatoDlgSubArea();

				switch (estDlgSubAre) {

				case AGREGANDO:

					bookMarkSubAreCod = man.agregarSubArea();
					break;

				case MODIFICANDO:

					bookMarkSubAreCod = man.actualizarSubArea();
					break;

				}// Fin de switch (estDlgCont)

				if (bookMarkSubAreCod != 0) {

//					man.transactionDelegate.commit();
					man.subAre.setObjeto(man.obtenerSubArea(bookMarkSubAreCod));
					acceso = AccesoPagina.INICIAR;
					estDlgAre = EstadoPagina.VISUALIZANDO;
					iniciarFormulario();

				}// Fin de if(bookMarkDevCod != 0)
				else {

//					man.transactionDelegate.rollback();
					JOptionPane.showConfirmDialog(this,
							"���SE CANCELO LA TRANSACCION!!!"
									+ "\n" + this.getClass().getName(),
							"Sistema",
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

				}
			}// fin de if (validarDlgContacto())
		}// Fin de } else if (cmdContAgregar.getText().equals("Guardar"))
	}// GEN-LAST:event_cmdSubAreAgregarActionPerformed

	private void cmdSubAreModificarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdSubAreModificarActionPerformed
		if (cmdSubAreModificar.getText().equals("Modificar")) {
			if (tabSubAre.getSelectedRow() > -1) {

				estDlgAre = EstadoPagina.SUSPENDIDO;
				estDlgSubAre = EstadoPagina.MODIFICANDO;

				refrescarFormulario();
				txtSubAreNom.requestFocus();

			}// Fin de if (tabPer.getSelectedRow() > -1)
		} else if (cmdSubAreModificar.getText().equals("Cancelar")) {

			acceso = AccesoPagina.INICIAR;
			iniciarFormulario();

		}// FIN DE EVALUAR QUE VA HA HACER EL BOTON
	}// GEN-LAST:event_cmdSubAreModificarActionPerformed

	private void cmdSubAreEliminarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdSubAreEliminarActionPerformed
		int resMsg = JOptionPane.showConfirmDialog(this,
				"�DESEA ELIMINAR EL SUBAREA " + man.are.getAre_nom() + "?",
				man.testProp.getPropiedad("TituloSistema"),
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (resMsg == JOptionPane.YES_OPTION) {

			boolean exito = man.eliminarSubArea(man.subAre.getSubare_cod());

			if (exito) {

//				man.transactionDelegate.commit();
				acceso = AccesoPagina.INICIAR;
				iniciarFormulario();

			}// Fin de eliminarDocuemtnoKardex
			else {

//				man.transactionDelegate.rollback();
				JOptionPane.showConfirmDialog(this,
						"���SE CANCELO LA TRANSACCION EN LA ELIMINACION DE FICHAS DE CONTROL!!!"
								+ "\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			}

		}// Fin de if(resMsg == JOptionPane.YES_OPTION)
	}// GEN-LAST:event_cmdSubAreEliminarActionPerformed

	private void cmdSubAreBuscarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmdSubAreBuscarActionPerformed

		estDlgAre = EstadoPagina.SUSPENDIDO;
		estDlgSubAre = EstadoPagina.BUSCANDO;

		refrescarFormulario();
		txtSubAreNom.requestFocus();

	}// GEN-LAST:event_cmdSubAreBuscarActionPerformed

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		/*
		 * Set the Nimbus look and feel
		 */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the
		 * default look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DlgArea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DlgArea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DlgArea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DlgArea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/*
		 * Create and display the dialog
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				DlgArea dialog = new DlgArea(null);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	private javax.swing.JButton cmdCerrar;
	private javax.swing.JButton cmdAreAgregar;
	private javax.swing.JButton cmdAreBuscar;
	private javax.swing.JButton cmdAreEliminar;
	private javax.swing.JButton cmdAreModificar;
	private javax.swing.JButton cmdSubAreAgregar;
	private javax.swing.JButton cmdSubAreBuscar;
	private javax.swing.JButton cmdSubAreEliminar;
	private javax.swing.JButton cmdSubAreModificar;
	private javax.swing.JLabel etiCliNom;
	private javax.swing.JLabel etiCliNom2;
	private javax.swing.JLabel etiCliNom3;
	private PanelImgFoto panAreBar = new PanelImgFoto();
	private PanelImgFoto panSubAreBar = new PanelImgFoto();
	private javax.swing.JPanel panArea;
	private javax.swing.JPanel panSubArea;
	private javax.swing.JScrollPane scrTabAre;
	private javax.swing.JScrollPane scrTabSubAre;
	public javax.swing.JTable tabAre;
	public javax.swing.JTable tabSubAre;
	private javax.swing.JTextField txtAreDes;
	private javax.swing.JTextField txtAreNom;
	private javax.swing.JTextField txtAreIde;
	private JTextFieldChanged txtSubAreIde;
	private JTextFieldChanged txtSubAreNom;
	private JTextFieldChanged txtSubAreDes;
	private JTextFieldChanged txtSubAreInt;
	private JButton cmdAreImprimir;
	private JButton cmdSubAreImprimir;
	private JTextFieldChanged txtAreBar;
	private JTextFieldChanged txtSubAreBar;

	// End of variables declaration//GEN-END:variables

	/*
	 * 
	 * METODOS DEFINIDOS POR LA INTERFAZ
	 */
	@Override
	public void iniciarFormulario() {
		if (man.validarUsuario()) {
			if (acceso.equals(AccesoPagina.INICIAR)) {

				estDlgAre = EstadoPagina.VISUALIZANDO;
				estDlgSubAre = EstadoPagina.VISUALIZANDO;
				frmAbierto = false;

				iniciarInstancias();
				limpiarInstancias(AccesoPagina.INICIAR);
				limpiarFormulario();
				activarFormulario(false);

				bookMarkAreNom = "TODOS";

				bookMarkAreCod = (man.are.getAre_cod() == 0) ? bookMarkAreCod : man.are.getAre_cod();
				bookMarkSubAreCod = (man.subAre.getSubare_cod() == 0) ? bookMarkSubAreCod : man.subAre.getSubare_cod();

				man.are.setObjeto(man.obtenerArea(bookMarkAreCod));
				man.subAre.setObjeto(man.obtenerSubArea(bookMarkSubAreCod));

				man.obtenerListaArea(bookMarkAreNom);
				llenarTablaArea();

				man.funUsu.ubicarRegistroEnteroTabla(tabAre, bookMarkAreCod, 5);// Codigo de Area
				seleccionarRegistroTablaArea();

				man.funUsu.ubicarRegistroEnteroTabla(tabSubAre, bookMarkSubAreCod, 4);// Codigo de SubArea
				seleccionarRegistroTablaSubArea();

				frmAbierto = true;

			}// Fin de else if (acceso.equals(AccesoPagina.INICIAR))
			else if (acceso.equals(AccesoPagina.ACTUALIZAR)) {
			}// Fin de if (acceso.equals(AccesoPagina.ACTUALIZAR))
			this.setVisible(true);
		} else {
			this.setVisible(false);
		}
	}// Fin de iniicarFormulario

	@SuppressWarnings("unchecked")
	@Override
	public void iniciarInstancias() {
		/*
		 * Obtiene una conexion del pool de conexiones de Apache
		 */
		man.transactionDelegate = (TransactionDelegate) ContenedorTextil.getComponent("TransactionDelegate");
		man.testProp = (TestPropiedades) ContenedorTextil.getComponent("TestPropiedades");
		man.funUsu = (FuncionesUsuario) ContenedorTextil.getComponent("FuncionesUsuario");

//		man.transactionDelegate.start();

		/*
		 * Obtiene las entidades del modelo relacional para su manejo en este
		 * formulario
		 */
		man.are = (Area) ContenedorTextil.getComponent("Area");
		man.lisAre = (List<Area>) ContenedorTextil.getComponent("ListaArea");

		man.subAre = (SubArea) ContenedorTextil.getComponent("SubArea");
		man.lisSubAre = (List<SubArea>) ContenedorTextil.getComponent("ListaSubArea");

	}// Fin de iniciarInstancias

	@Override
	public void limpiarInstancias(AccesoPagina parAcceso) {

		if (parAcceso.equals(AccesoPagina.INICIAR) || parAcceso.equals(AccesoPagina.FINALIZAR)) {

			man.are.limpiarInstancia();
			man.subAre.limpiarInstancia();

		}// Fin de

		man.lisAre.clear();
		man.lisSubAre.clear();

	}// fin de limpiarInstancias

	@Override
	public void limpiarFormulario() {

		limpiarDlgArea();
		limpiarDlgSubArea();

	}// Fin de limpiarFormulario

	@Override
	public void generarNuevoRegistro() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void llenarFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}// Fin de llenarFormulario

	@Override
	public void activarFormulario(boolean parAct) {

		activarArea(parAct);
		activarSubArea(parAct);

	}

	@Override
	public void refrescarFormulario() {

		refrescarDlgArea();
		refrescarDlgSubArea();

	}// Fin de refrescarFormulario

	@Override
	public void obtenerDatoFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void obtenerDatoBaseDato() {
		throw new UnsupportedOperationException("Not supported yet.");
	}// Fin de obtenerDatoBaseDato

	@Override
	public boolean validarFormulario() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean guardarDatoBaseDato(String parModoGrabar) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void setManejador(AuxiliarTextil parMan) {
		man = parMan;
	}

	@Override
	public void cerrar() {
		if (padre.equals("FrmPrincipal")) {
			int resMsg = JOptionPane.showConfirmDialog(this,
					"�Desea cerrar el formulario?",
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (resMsg == 0) {

				this.dispose();
				man.claveValidado = "";

				limpiarInstancias(AccesoPagina.FINALIZAR);
//				man.transactionDelegate.end();
//				try {
//					DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().commit();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
				man.abrirPantalla("FrmPrincipal", "FrmPrincipal", AccesoPagina.ACTUALIZAR);

			}// Fin de verificar que se desea cerrar el formulario
		}// El padre es cualquier formulario
		else {

			this.dispose();
			man.abrirPantalla(padre, "FrmPrincipal", AccesoPagina.ACTUALIZAR);

		}// Fin de Verificar que el padre es cualquier ventana
	}// Fin de cerrar()

	private void crearModeloTablas() {

		crearModeloTablaArea();
		crearModeloTablaSubArea();

	}// Fin de crearModeloTablas

	private void crearModeloTablaArea() {

		modTabAre = new ModeloTablaArea();
		tabAre.setModel(modTabAre);
		modTabCellRender = new CeldaRenderGeneral();
		try {

			tabAre.setDefaultRenderer(Class.forName("java.lang.String"), modTabCellRender);
			tabAre.setDefaultRenderer(Class.forName("java.lang.Integer"), modTabCellRender);
//			tabAre.setDefaultRenderer(Class.forName("javax.swing.JCheckBox"), modTabCellRender);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}

		tabAre.addPropertyChangeListener(new ModTabArePropiedad(this, tabAre));
		// tabAre.addPropertyChangeListener(new ModTabDetKarPropiedad(this, tabDetKar));

		tabAre.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tabAre.getTableHeader().setReorderingAllowed(false);
		tabAre.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		String[] header = { "Orden", "Ide", "Nombre", "Descripcion", "Act", "are_cod" };
		// String[] tipo = { "String", "String", "String", "Integer" };
		int[] ancho = { 35, 150, 190, 265, 30, 0 };

		colTabAre = new TableColumn[header.length + 1];
		for (int iteHead = 0; iteHead < header.length; iteHead++) {

			colTabAre[iteHead] = tabAre.getColumn(header[iteHead]);
			colTabAre[iteHead].setResizable(true);
			colTabAre[iteHead].setPreferredWidth(ancho[iteHead]);
			// colTabAre[iteHead].addPropertyChangeListener(new ListenerCambioPropiedad());

			if (ancho[iteHead] == 0) {
				tabAre.getColumnModel().getColumn(iteHead).setMaxWidth(0);
				tabAre.getColumnModel().getColumn(iteHead).setMinWidth(0);
				tabAre.getTableHeader().getColumnModel().getColumn(iteHead).setMaxWidth(0);
				tabAre.getTableHeader().getColumnModel().getColumn(iteHead).setMinWidth(0);

			}// Fin de if (anchoHeader[iteHead] == 0)
			if (iteHead == 0) {
				colTabAre[iteHead].setCellEditor(new CellEditEnteroTabAre(this));
			}
			if (iteHead == 4) {
				colTabAre[iteHead].setCellEditor(new CellEditBooleanTabAre(this));
			}
		}// Recorrer la tabla de encabezados fdsfsdf
	}// Fin de metodo crearModelo

	private void crearModeloTablaSubArea() {

		modTabSubAre = new ModeloTablaSubArea();
		tabSubAre.setModel(modTabSubAre);

		tabSubAre.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tabSubAre.getTableHeader().setReorderingAllowed(false);
		tabSubAre.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		modTabCellRender = new CeldaRenderGeneral();
		try {

			tabSubAre.setDefaultRenderer(Class.forName("java.lang.String"), modTabCellRender);
			tabSubAre.setDefaultRenderer(Class.forName("java.lang.Integer"), modTabCellRender);
			// tabSubAre.setDefaultRenderer(Class.forName("java.lang.Boolean"), modTabCellRender);
			// tabAre.setDefaultRenderer(Class.forName("javax.swing.JCheckBox"), modTabCellRender);

		} catch (ClassNotFoundException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
		// tabAre.addPropertyChangeListener(new ModTabDetKarPropiedad(this, tabDetKar));

		String[] header = { "Ide", "Nombre", "Descripcion", "Act", "subare_cod", "are_cod" };
		String[] tipo = { "String", "String", "String", "Integer" };
		int[] ancho = { 140, 150, 341, 30, 0, 0 };

		colTabSubAre = new TableColumn[header.length + 1];
		for (int iteHead = 0; iteHead < header.length; iteHead++) {

			colTabSubAre[iteHead] = tabSubAre.getColumn(header[iteHead]);
			colTabSubAre[iteHead].setResizable(true);
			colTabSubAre[iteHead].setPreferredWidth(ancho[iteHead]);
			// colTabSubAre[iteHead].addPropertyChangeListener(new ListenerCambioPropiedad());

			if (ancho[iteHead] == 0) {

				tabSubAre.getColumnModel().getColumn(iteHead).setMaxWidth(0);
				tabSubAre.getColumnModel().getColumn(iteHead).setMinWidth(0);
				tabSubAre.getTableHeader().getColumnModel().getColumn(iteHead).setMaxWidth(0);
				tabSubAre.getTableHeader().getColumnModel().getColumn(iteHead).setMinWidth(0);

			}// Fin de if (anchoHeader[iteHead] == 0)
				// if (tipo[iteHead].equals("Integer")) {
				// colTabDetKar[iteHead].setCellEditor(new CeldaEditorJTextEntero(this));
				// }
		}// Recorrer la tabla de encabezados
	}// Fin de metodo crea r modelo cotnacto

	private void limpiarDlgArea() {

		txtAreIde.setText("");
		txtAreNom.setText("");
		txtAreDes.setText("");
		panAreBar.setImagen(null, null, null, "CONSTRAINT");
		panSubAreBar.setImagen(null, null, null, "CONSTRAINT");

	}// limpiarDlgArea

	private void limpiarDlgSubArea() {

		txtSubAreIde.setText("");
		txtSubAreNom.setText("");
		txtSubAreDes.setText("");
		txtSubAreInt.setText("");

	}// Fin de limpiarDlgSubArea

	private void refrescarDlgArea() {

		switch (estDlgAre) {
		case VISUALIZANDO:

			activarArea(false);
			tabAre.setEnabled(true);

			cmdAreAgregar.setText("Agregar");
			cmdAreModificar.setText("Modificar");

			cmdAreAgregar.setEnabled(true);
			cmdAreModificar.setEnabled((tabAre.getSelectedRow() > -1) ? true : false);
			cmdAreEliminar.setEnabled((tabAre.getSelectedRow() > -1) ? true : false);
			cmdAreImprimir.setEnabled((tabAre.getSelectedRow() > -1) ? true : false);
			cmdAreBuscar.setEnabled((tabAre.getSelectedRow() > -1) ? true : false);
			cmdCerrar.setEnabled(true);

			break;

		case MODIFICANDO:

			activarArea(true);
			modTabAre.setRowCount(0);

			cmdAreAgregar.setText("Guardar");
			cmdAreModificar.setText("Cancelar");

			cmdAreAgregar.setEnabled(true);
			cmdAreModificar.setEnabled(true);
			cmdAreBuscar.setEnabled(false);
			cmdAreEliminar.setEnabled(false);
			cmdAreImprimir.setEnabled(false);
			cmdCerrar.setEnabled(false);
			break;

		case AGREGANDO:

			limpiarFormulario();
			activarArea(true);
			modTabAre.setRowCount(0);

			cmdAreAgregar.setText("Guardar");
			cmdAreModificar.setText("Cancelar");

			cmdAreAgregar.setEnabled(true);
			cmdAreModificar.setEnabled(true);
			cmdAreBuscar.setEnabled(false);
			cmdAreEliminar.setEnabled(false);
			cmdAreImprimir.setEnabled(false);
			cmdCerrar.setEnabled(false);
			break;

		case BUSCANDO:

			limpiarFormulario();
			activarArea(true);
			modTabAre.setRowCount(0);

			cmdAreModificar.setText("Cancelar");

			cmdAreAgregar.setEnabled(false);
			cmdAreModificar.setEnabled(true);
			cmdAreBuscar.setEnabled(false);
			cmdAreEliminar.setEnabled(false);
			cmdAreImprimir.setEnabled(false);
			cmdCerrar.setEnabled(false);
			break;

		case SUSPENDIDO:

			activarArea(false);
			tabAre.setEnabled(false);

			cmdAreAgregar.setEnabled(false);
			cmdAreModificar.setEnabled(false);
			cmdAreBuscar.setEnabled(false);
			cmdAreEliminar.setEnabled(false);
			cmdAreImprimir.setEnabled(false);
			cmdCerrar.setEnabled(false);
			break;

		}// Fin de switch para estado del formaulario de cliente
	}// Fin de refrescarCliente

	private void refrescarDlgSubArea() {
		switch (estDlgSubAre) {
		case VISUALIZANDO:

			activarSubArea(false);
			tabSubAre.setEnabled(true);

			cmdSubAreAgregar.setText("Agregar");
			cmdSubAreModificar.setText("Modificar");

			cmdSubAreAgregar.setEnabled((man.are.getAre_cod() > 0) ? true : false);
			cmdSubAreModificar.setEnabled((tabSubAre.getSelectedRow() > -1) ? true : false);
			cmdSubAreEliminar.setEnabled((tabSubAre.getSelectedRow() > -1) ? true : false);
			cmdSubAreImprimir.setEnabled((tabSubAre.getSelectedRow() > -1) ? true : false);
			cmdSubAreBuscar.setEnabled(false);
			cmdCerrar.setEnabled(true);

			break;

		case MODIFICANDO:

			activarSubArea(true);
			modTabSubAre.setRowCount(0);

			cmdSubAreAgregar.setText("Guardar");
			cmdSubAreModificar.setText("Cancelar");

			cmdSubAreAgregar.setEnabled(true);
			cmdSubAreModificar.setEnabled(true);
			cmdSubAreBuscar.setEnabled(false);
			cmdSubAreEliminar.setEnabled(false);
			cmdSubAreImprimir.setEnabled(false);
			break;

		case AGREGANDO:

			limpiarDlgSubArea();
			activarSubArea(true);
			modTabSubAre.setRowCount(0);

			cmdSubAreAgregar.setText("Guardar");
			cmdSubAreModificar.setText("Cancelar");

			cmdSubAreAgregar.setEnabled(true);
			cmdSubAreModificar.setEnabled(true);
			cmdSubAreBuscar.setEnabled(false);
			cmdSubAreEliminar.setEnabled(false);
			cmdSubAreImprimir.setEnabled(false);
			break;

		case BUSCANDO:

			limpiarDlgSubArea();
			activarSubArea(true);
			modTabSubAre.setRowCount(0);

			cmdSubAreModificar.setText("Cancelar");

			cmdSubAreAgregar.setEnabled(false);
			cmdSubAreModificar.setEnabled(true);
			cmdSubAreBuscar.setEnabled(false);
			cmdSubAreEliminar.setEnabled(false);
			cmdSubAreImprimir.setEnabled(false);
			break;

		case SUSPENDIDO:

			activarSubArea(false);
			tabSubAre.setEnabled(false);

			cmdSubAreAgregar.setEnabled(false);
			cmdSubAreModificar.setEnabled(false);
			cmdSubAreBuscar.setEnabled(false);
			cmdSubAreEliminar.setEnabled(false);
			cmdSubAreImprimir.setEnabled(false);

			break;

		}// Fin de switch para estado del formaulario de cliente
	}// Fin de refrescarCliente

	private void activarArea(boolean parAct) {
		if (estDlgAre.equals(EstadoPagina.BUSCANDO)) {

			txtAreNom.setEnabled(parAct);

		}// Fin de if(estDlgAre.equals(EstadoPagina.BUSCANDO))
		else if (!estDlgAre.equals(EstadoPagina.BUSCANDO)) {

			txtAreIde.setEnabled(false);
			txtAreNom.setEnabled(parAct);
			txtAreDes.setEnabled(parAct);
			panAreBar.setEnabled(false);

		}// Fin de else if(!estDlgAre.equals(EstadoPagina.BUSCANDO))
	}// Fin de activarArea

	private void activarSubArea(boolean parAct) {
		if (estDlgSubAre.equals(EstadoPagina.BUSCANDO)) {

			txtSubAreNom.setEnabled(parAct);

		}// Fin de if(estDlgAre.equals(EstadoPagina.BUSCANDO))
		else if (!estDlgSubAre.equals(EstadoPagina.BUSCANDO)) {

			txtSubAreIde.setEnabled(false);
			txtSubAreNom.setEnabled(parAct);
			txtSubAreDes.setEnabled(parAct);
			txtSubAreInt.setEnabled(parAct);
			panSubAreBar.setEnabled(false);

		}// Fin de else if(!estDlgAre.equals(EstadoPagina.BUSCANDO))
	}// Fin de activarArea

	private boolean validarDlgArea() {

		if (!txtAreIde.getText().trim().equals("")) {
			if (txtAreNom.getText().trim().length() >= 3) {
				if (!txtAreDes.getText().trim().equals("")) {

					return true;

				}// fin de if(!txtContNom.getText().trim().equals(""))
				else {

					JOptionPane.showConfirmDialog(this,
							"Ingresa Descripcion  de are"
									+ "\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

				}
			}// fin de if(!txtContNom.getText().trim().equals(""))
			else {

				JOptionPane.showConfirmDialog(this,
						"El nombre del are debe de tener minimo 3 caracteres"
								+ "\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			}
		}// fin de if(!txtContNom.getText().trim().equals(""))
		else {

			JOptionPane.showConfirmDialog(this,
					"Ingresa codigo de barras"
							+ "\n" + this.getClass().getName(),
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

		}
		return false;
	}// fin de validarDlgPersona()

	private boolean validarDlgSubArea() {

		if (txtSubAreNom.getText().trim().length() >= 3) {
			if (!txtSubAreIde.getText().trim().equals("")) {
				if (!txtSubAreDes.getText().trim().equals("")) {

					return true;

				}// fin de if(!txtContNom.getText().trim().equals(""))
				else {

					JOptionPane.showConfirmDialog(this,
							"Ingresa descripcion  de subarea"
									+ "\n" + this.getClass().getName(),
							man.testProp.getPropiedad("TituloSistema"),
							JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

				}
			}// fin de if(!txtContNom.getText().trim().equals(""))
			else {

				JOptionPane.showConfirmDialog(this,
						"Ingresa codigo de barras de subarea"
								+ "\n" + this.getClass().getName(),
						man.testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			}
		}// fin de if(!txtContNom.getText().trim().equals(""))
		else {

			JOptionPane.showConfirmDialog(this,
					"El nombre de la subarea debe tenere minimo 3 caracteres"
							+ "\n" + this.getClass().getName(),
					man.testProp.getPropiedad("TituloSistema"),
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

		}
		return false;
	}// fin de validarDlgPersona()

	private void obtenerDatoDlgArea() {

		validarAreaCodigoBarra();
		man.are.setAre_ord(0);
		man.are.setAre_ide(txtAreIde.getText().trim());
		man.are.setAre_nom(txtAreNom.getText().trim());
		man.are.setAre_des(txtAreDes.getText().trim());
		man.are.setAre_act(true);
		// man.are.setAre_bar();
		// man.are.setAre_ver(0)

	}// Fin de obtenerDatoPersona()

	public boolean validarAreaCodigoBarra() {

		if (txtAreNom.getText().length() >= 3) {
			boolean existe = true;
			String codigo = "";
			while (existe) {
				codigo = KeyGenerator.getInst().obtenerCodigoBarra();
				if (!man.existeCodigoBarra("AREA", codigo)) {
					man.are.setAre_bar(KeyGenerator.getInst().obtenerCodigoBarra());
					existe = false;
				}
			}
			return true;
		}
		return false;
	}

	public boolean validarSubAreaCodigoBarra() {

		if (txtSubAreNom.getText().length() >= 3) {
			boolean existe = true;
			String codigo = "";
			while (existe) {
				codigo = KeyGenerator.getInst().obtenerCodigoBarra();
				if (!man.existeCodigoBarra("SUBAREA", codigo)) {
					man.subAre.setSubare_bar(KeyGenerator.getInst().obtenerCodigoBarra());
					existe = false;
				}
			}
			return true;
		}
		return false;
	}

	private void seleccionarRegistroTablaSubArea() {

		obtenerRegistroTablaSubArea();
		llenarDlgSubArea();

		if (!tabSubAre.isEditing()) {

			refrescarDlgSubArea();

			if (tabSubAre.getSelectedRow() > -1) {

				cmdSubAreModificar.setEnabled(true);
				cmdSubAreEliminar.setEnabled(true);

			}// Fin de if(tabSubAre.getSelectedRow() >-1)
		}// Fin de if(!tabDetVen.isEditing())

	}// Fin de seleccionarRegistroTablaSubArea()

	private void obtenerDatoDlgSubArea() {

		validarSubAreaCodigoBarra();
		// man.subAre.setSubare_cod();
		man.subAre.setAre_cod(man.are.getAre_cod());
		man.subAre.setSubare_ide(txtSubAreIde.getText().trim());
		man.subAre.setSubare_nom(txtSubAreNom.getText().trim());
		man.subAre.setSubare_des(txtSubAreDes.getText().trim());
		man.subAre.setSubare_des(txtSubAreDes.getText().trim());
		man.subAre.setSubare_int(txtSubAreInt.getText().trim());
		man.subAre.setSubare_act(man.are.isAre_act());
		man.subAre.setSubare_ord(0);
		// man.subAre.setSubare_bar();
		// man.subAre.setSubare_ver();

	}// Fin de obtenerDatoPersona()

	public void seleccionarRegistroTablaArea() {

		obtenerRegistroTablaArea();
		llenarDlgArea();

		man.obtenerListaSubArea(bookMarkAreCod);
		llenarTablaSubArea();

		if (!tabAre.isEditing()) {

			refrescarFormulario();

			if (tabAre.getSelectedRow() > -1) {

				cmdAreModificar.setEnabled(true);
				cmdAreEliminar.setEnabled(true);

			}// Fin dde if (tabAre.getSelectedRow() > -1)
		}// Fin de if(!tabDetVen.isEditing())
	}// Fin de seleccionarRegistroTablaArea()

	private void obtenerRegistroTablaArea() {

		int filTab = tabAre.getSelectedRow();
		if (filTab > -1) {

			bookMarkAreCod = (Integer) modTabAre.getValueAt(filTab, 5);// Codigo del Area
			man.are.setObjeto(man.obtenerItemListaArea(bookMarkAreCod));

		}// Fin de if(parNumFil > -1)
		else {

			bookMarkAreCod = 0;
			man.are.limpiarInstancia();

		}
	}// Fin de obtenerRegistroTablaPersona()

	private void obtenerRegistroTablaSubArea() {

		int filTab = tabSubAre.getSelectedRow();
		if (filTab > -1) {

			bookMarkSubAreCod = (Integer) modTabSubAre.getValueAt(filTab, 4);// Codigo de Subarea
			man.subAre.setObjeto(man.obtenerItemListaSubArea(bookMarkSubAreCod));

		}// Fin de if(parNumFil > -1)
		else {

			bookMarkSubAreCod = 0;
			man.subAre.limpiarInstancia();

		}
	}// Fin de obtenerRegistroTablaContacto()

	private void llenarDlgArea() {

		txtAreIde.setText(man.are.getAre_ide());
		txtAreNom.setText(man.are.getAre_nom());
		txtAreDes.setText(man.are.getAre_des());
		CodigoBarras codigoBarras = (CodigoBarras) ContenedorTextil.getComponent("CodigoBarras");
		panAreBar.setImagen(codigoBarras.obtenerImageIcon(man.are.getAre_bar()), null, null, "CONSTRAINT");

	}// (Fin de llenarDialogoPersona

	private void llenarDlgSubArea() {

		txtSubAreIde.setText(man.subAre.getSubare_ide());
		txtSubAreNom.setText(man.subAre.getSubare_nom());
		txtSubAreDes.setText(man.subAre.getSubare_des());
		txtSubAreInt.setText(man.subAre.getSubare_int());
		CodigoBarras codigoBarras = (CodigoBarras) ContenedorTextil.getComponent("CodigoBarras");
		panSubAreBar.setImagen(codigoBarras.obtenerImageIcon(man.subAre.getSubare_bar()), null, null, "CONSTRAINT");

	}// (Fin de llenarDialogoPersona

	public void llenarTablaArea() {

		modTabAre.setRowCount(0);
		modTabAre.setRowCount(man.lisAre.size());
		int numItem = 0;

		for (Area iteAre : man.lisAre) {

			modTabAre.setValueAt(iteAre.getAre_ord(), numItem, 0);// Orden
			modTabAre.setValueAt(iteAre.getAre_ide(), numItem, 1);// Ide
			modTabAre.setValueAt(iteAre.getAre_nom(), numItem, 2);// Nombre
			modTabAre.setValueAt(iteAre.getAre_des(), numItem, 3);// Descripcion
			modTabAre.setValueAt(iteAre.isAre_act(), numItem, 4);// Activo
			modTabAre.setValueAt(iteAre.getAre_cod(), numItem, 5);// Codigo

			numItem++;

		}
	}// llenarTablaArea

	private void llenarTablaSubArea() {

		modTabSubAre.setRowCount(0);
		modTabSubAre.setRowCount(man.lisSubAre.size());
		int numItem = 0;

		for (SubArea iteSubAre : man.lisSubAre) {

			modTabSubAre.setValueAt(iteSubAre.getSubare_ide(), numItem, 0);// Ide
			modTabSubAre.setValueAt(iteSubAre.getSubare_nom(), numItem, 1);// Nombre
			modTabSubAre.setValueAt(iteSubAre.getSubare_des(), numItem, 2);// Descripcion
			modTabSubAre.setValueAt(iteSubAre.isSubare_act(), numItem, 3);// Activo
			modTabSubAre.setValueAt(iteSubAre.getSubare_cod(), numItem, 4);// Codigo Area
			modTabSubAre.setValueAt(iteSubAre.getAre_cod(), numItem, 5);// Codigo SubArea

			numItem++;

		}
	}// llenarTablaArea

	public void generarIdentificacion(String parTipZona, String parNomZona) {

		if (estDlgAre.equals(EstadoPagina.AGREGANDO) || estDlgAre.equals(EstadoPagina.MODIFICANDO)
				|| estDlgSubAre.equals(EstadoPagina.AGREGANDO) || estDlgSubAre.equals(EstadoPagina.MODIFICANDO)) {

			man.nuevoNumeroIde(parTipZona, parNomZona);
			if (parTipZona.equals("AREA") && !man.are.getAre_ide().equals("")) {

				txtAreIde.setText(man.are.getAre_ide());

			} else if (parTipZona.equals("SUBAREA") && !man.subAre.getSubare_ide().equals("")) {

				txtSubAreIde.setText(man.subAre.getSubare_ide());

			}
		}// Fin de if(estDlgSubAre.equals(estadoVentana.AGREGANDO) ||estDlgSubAre.equals(estadoVentana.AGREGANDO))
	}// Fin de public void generarIdentificacionProducto(int ano, int tAre, String mod, String cli, String col)

	private void cmdSubAreImprimirActionPerformed(ActionEvent e) {

		enviarImpresora(man.subAre.getSubare_cod());

	}

	private void cmdAreImprimirActionPerformed(ActionEvent e) {
		for (SubArea iteSubAre : man.lisSubAre) {

			enviarImpresora(iteSubAre.getSubare_cod());

		}
	}

	public void enviarImpresora(int parSubAreCod) {
		File repFactura;
		repFactura = new File(AplicacionGeneral.getInstance().obtenerRutaReporteJasper() + "ReporteSubArea.jasper");

		if (repFactura != null) {
			try {
				InputStream inpStrRepFac = new FileInputStream(repFactura);
				HashMap map = new HashMap();
				map.put("SUBARE_COD", parSubAreCod);

				try {

					JasperPrint print = JasperFillManager.fillReport(inpStrRepFac, map, DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection());
					String ruta = "Y:\\reportes\\out\\";
					String fileName = KeyGenerator.getInst().getUniqueKey().toString();
					JasperExportManager.exportReportToPdfFile(print, ruta + fileName + ".pdf");
					Util.getInst().abrirArchivoDesktop(ruta + fileName + ".pdf");
					
//					JasperViewer jv = new JasperViewer(print, false);
//					jv.setTitle("Reporte de Pedidos");
//					jv.setSize(800, 600);
//					jv.setVisible(true);

					// JasperPrintManager.printReport(print, true);

				} catch (JRException ex) {
					JOptionPane.showConfirmDialog(null, "Error de conexion con la impresora", man.testProp.getPropiedad("TituloSistema"), -1, 1);
				}
			} catch (FileNotFoundException ex) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public void desactivarBotonFormulario() {

		cmdAreAgregar.setEnabled(false);
		cmdAreModificar.setEnabled(false);
		cmdAreEliminar.setEnabled(false);
		cmdAreImprimir.setEnabled(false);
		cmdAreBuscar.setEnabled(false);
		cmdCerrar.setEnabled(false);

		cmdSubAreAgregar.setEnabled(false);
		cmdSubAreModificar.setEnabled(false);
		cmdSubAreEliminar.setEnabled(false);
		cmdSubAreImprimir.setEnabled(false);
		cmdSubAreBuscar.setEnabled(false);

	}

}// Fin de Clase principal