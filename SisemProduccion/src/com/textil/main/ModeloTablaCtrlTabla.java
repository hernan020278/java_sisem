package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaCtrlTabla extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public ModeloTablaCtrlTabla() {

		super.addColumn("ctrl_cod"); 	// Col 0 String
		super.addColumn("Descripcion"); // Col 1 String
		super.addColumn("Serie");      	// Col 2 Integer
		super.addColumn("Registro");    // Col 3 Integer
		super.addColumn("Valor");    	// Col 4 Double
		
	}

	@Override
	public Class getColumnClass(int col) {
		if (col == 1) {
			return String.class;
		}
		if (col == 0 || col == 2 || col == 3) {
			return Integer.class;
		}
		if (col == 4) {
			return Double.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
