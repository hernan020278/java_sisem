package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaProducto extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public ModeloTablaProducto() {

		super.addColumn("Modelo"); 			// Col 0 String
		super.addColumn("Tipo");   			// Col 1 String
		super.addColumn("Material");   		// Col 2 String
		super.addColumn("Grosor");   		// Col 3 String
		super.addColumn("Gauge");   		// Col 4 String
		super.addColumn("Articulo");   		// Col 5 String
		super.addColumn("Color");     		// Col 6 String
		super.addColumn("Talla");     		// Col 7 String
		super.addColumn("Stk-Act");   		// Col 8 Integer
		super.addColumn("prd_cod");  		// Col 9 Integer
		super.addColumn("gru_cod");		   	// Col 10 Integer
	}

	@Override
	public Class getColumnClass(int columna) {
		if (columna == 8 || columna == 9 || columna == 10) {
			return Integer.class;
		}
		if ((columna >= 0 && columna <= 7)) {
			return String.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
