package com.textil.main;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class JComboBoxRender extends JFrame {
	
	private static final long serialVersionUID = 1L;

	JComboBox cbo = new JComboBox (new String[] { "CONTADO", "Madrid", "New York", "Rome", "Sydney", "Washington" });

	public JComboBoxRender () {
		setSize (150, 75);
		setLocation (400, 300);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
		cbo.setRenderer (new ItemListaRender ());
		JPanel jp = new JPanel ();
		jp.add (cbo);
		getContentPane ().add (jp);
	}

	public static void main (String args[]) {
		new JComboBoxRender ().setVisible (true);
	}
}// Fin de Clase Principal

