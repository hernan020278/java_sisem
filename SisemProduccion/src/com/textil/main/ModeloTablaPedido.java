package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaPedido extends DefaultTableModel {
    private static final long serialVersionUID = 1L;

    public ModeloTablaPedido() {

		super.addColumn("Documento");   // 0 String
		super.addColumn("Numero");    	// 1 String
		super.addColumn("Nombre");    	// 2 String
		super.addColumn("Modelo");    	// 3 String
		super.addColumn("Fec-Reg");     // 4 String
		super.addColumn("Fec-Ent");	  	// 5 String
		super.addColumn("Temp"); 		// 6 String
		super.addColumn("Estado");    	// 7 String
		super.addColumn("ped_cod");    	// 8 Integer
		super.addColumn("cli_cod");    	// 9 Integer
		
	}

	@Override
	public Class getColumnClass(int columna) {
		if ((columna >= 0 && columna <= 7)) {
			return String.class;
		}
		if ((columna >= 8 && columna <= 9)) {
			return Integer.class;
		}
		return Object.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
//        if (col == 5) {
//            return true;
//        }
        return false;
    }
}
