package com.textil.main;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaDetProducto extends DefaultTableModel {
	private static final long serialVersionUID = 1L;

	public ModeloTablaDetProducto() {

		super.addColumn("Item");      	// 0 Integer
		super.addColumn("Tipo");    	// 1 String
		super.addColumn("Material");    // 2 String
		super.addColumn("Gauge");     	// 3 String
		super.addColumn("Color");	  	// 4 String
		super.addColumn("Descripcion"); // 5 String
		super.addColumn("Stk-Act");    	// 6 Integer
		super.addColumn("Can-Ped");    	// 7 Integer
		super.addColumn("Can-Fab");    	// 8 Integer
		super.addColumn("Can-Ent");    	// 9 Integer
		super.addColumn("detped_cod");  // 10 Integer
		super.addColumn("ped_cod");  	// 11 Integer
		super.addColumn("prd_cod");  	// 12 Integer
		super.addColumn("gru_cod");  	// 13 Integer
		super.addColumn("Estado");    	// 14 String
	}

	@Override
	public Class getColumnClass(int columna) {
		if ((columna >= 1 && columna <= 5) || columna == 14) {
			return String.class;
		}
		if (columna == 0 || (columna >= 6 && columna <= 13)) {
			return Integer.class;
		}
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		// if (col == 5) {
		// return true;
		// }
		return false;
	}
}
