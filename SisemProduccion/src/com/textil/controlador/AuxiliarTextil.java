package com.textil.controlador;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Agrupacion;
import com.comun.entidad.Area;
import com.comun.entidad.Clase;
import com.comun.entidad.Cliente;
import com.comun.entidad.CtrlTabla;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Ficha;
import com.comun.entidad.Grupo;
import com.comun.entidad.Organizacion;
import com.comun.entidad.Pedido;
import com.comun.entidad.PerfilSeguridad;
import com.comun.entidad.Producto;
import com.comun.entidad.Seguridad;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.DriverJdbc;
import com.comun.utilidad.AplicacionGeneral;
import com.comun.utilidad.SwingWorker;
import com.comun.utilidad.KeyGenerator;
import com.comun.utilidad.Util;
import com.textil.dao.AreaDAO;
import com.textil.dao.ClaseDAO;
import com.textil.dao.ClienteDAO;
import com.textil.dao.CtrlTablaDAO;
import com.textil.dao.DetPedidoDAO;
import com.textil.dao.FichaDAO;
import com.textil.dao.GrupoDAO;
import com.textil.dao.PedidoDAO;
import com.textil.dao.ProductoDAO;
import com.textil.dao.SubAreaDAO;
import com.textil.dao.TransactionDelegate;
import com.textil.dao.UsuarioDAO;
import com.textil.main.DlgAcceso;
import com.textil.main.DlgArea;
import com.textil.main.DlgCodigoProducto;
import com.textil.main.DlgControlPedido;
import com.textil.main.DlgControlProceso;
import com.textil.main.DlgControlProducto;
import com.textil.main.DlgFicha;
import com.textil.main.DlgPedido;
import com.textil.main.DlgPedidoImportacion;
import com.textil.main.DlgProducto;
import com.textil.main.FrmPrincipal;
import com.textil.main.FuncionesUsuario;
import com.textil.main.TestPropiedades;

/**
 * @author Paolo
 */
public abstract class AuxiliarTextil {

	public String bookMarkBarraFicha = "";
	public String bookMarkBarraSubArea = "";
	public int contadorMarcador = 1;
	public SwingWorker swnWork;
	public Map requestParameters = new HashMap();
	/*
	 * Variables del controlador
	 */

	public AuxiliarTextil manPadre;
	/*
	 * Formularios
	 */
	public FrmPrincipal frmPri;
	public DlgProducto dlgPrd;
	public DlgPedido dlgPed;
	public DlgPedidoImportacion dlgPedImp;
	public DlgControlPedido dlgCtrlPed;
	public DlgControlProducto dlgCtrlPrd;
	public DlgCodigoProducto dlgCodPrd;
	public DlgArea dlgAre;
	public DlgFicha dlgFic;
	public DlgAcceso dlgAcc;
	public DlgControlProceso dlgCtrlProc;
	public Date varFecha;
	/*
	 * FUNCIONES UTILITARIOS
	 */
	public FuncionesUsuario funUsu;
	public TestPropiedades testProp;
	public TransactionDelegate transactionDelegate;

	/*
	 * OBJETOS DEL MODEL RELACIONAL
	 */
	public CtrlTabla ctrlTab;
	public Cliente cli;
	public Pedido ped;
	public DetPedido detPed;
	public DetPedido detPedTmp;
	public Producto prdGen;
	public Producto prd;
	public Producto mod;
	public Producto tal;
	public Producto prdPad;
	public Grupo gru;
	public Clase cla;
	public Clase claCol;
	public Grupo gruCol;
	public Grupo gruMod;
	public Grupo gruPrd;
	public Grupo gruTal;
	public Area are;
	public SubArea subAre;
	public Ficha fic;
	public Usuario usu;
	public Usuario usuSes;
	public Organizacion org;
	public Seguridad sec;
	public PerfilSeguridad perfSec;
	public Agrupacion agru;
	/*
	 * Listas de Clases de Modelo
	 */
	public List lisModImp;
	public List<Cliente> lisCli;
	public List<Producto> lisMod;
	public List<Producto> lisPrd;
	public List<Producto> lisTal;
	public List<Producto> lisPrdGen;
	public List<Pedido> lisPed;
	public List<DetPedido> lisDetPed;
	public List<CtrlTabla> lisCtrlTab;
	public List<Grupo> lisGru;
	public List<Clase> lisCla;
	public List<Area> lisAre;
	public List<SubArea> lisSubAre;
	public List<Ficha> lisFic;
	public List<Usuario> lisUsu;
	public List<Organizacion> lisOrg;
	public List<Seguridad> lisSec;
	public List<PerfilSeguridad> lisPerfSec;

	/*
	 * VARIABLES TEMPORALES DEL SISTEMA
	 */
	public String valorPrueba;
	public int itemDetVenta, itemSubDetVenta, itemDetKardex, itemSubDetKardex;
	public Date fechaActual;
	public Time horaActual;
	public Calendar now;
	public String claveValidado;

	public static enum EstadoDocumento {

		PENDIENTE(0, "PENDIENTE"), PRODUCCION(1, "PRODUCCION"), TERMINADO(2, "TERMINADO"), ANULADO(3, "ANULADO");
		private final int varKey;
		private final String varValue;

		EstadoDocumento(int parKey, String parValue) {
			this.varKey = parKey;
			this.varValue = parValue;
		}

		public int getKey() {
			return this.varKey;
		}

		public String getVal() {
			return this.varValue;
		}// FIN DE METODOS
	}// FIN DE DECLARACION DE ENUM

	public static enum TipoDocumento {

		NOTAPEDIDO(0, "NOTA DE PEDIDO"), FICHACONTROL(1, "FICHA DE CONTROL"), NOTAPRUEBA(2, "NOTA DE PRUEBA");
		private final int varKey;
		private final String varValue;

		TipoDocumento(int parKey, String parValue) {
			this.varKey = parKey;
			this.varValue = parValue;
		}

		public int getKey() {
			return this.varKey;
		}

		public String getVal() {
			return this.varValue;
		}// FIN DE METODOS
	}// FIN DE DECLARACION DE ENUM

	public static enum AccesoPagina {
		ACTUALIZAR(0, "ACTUALIZAR"), INICIAR(1, "INICIAR"), FINALIZAR(2, "FINALIZAR");

		private final int varKey;
		private final String varValue;

		AccesoPagina(int parKey, String parValue) {

			this.varKey = parKey;
			this.varValue = parValue;
		}

		public int getKey() {

			return this.varKey;
		}

		public String getVal() {

			return this.varValue;
		}// FIN DE METODOS
	}// FIN DE DECLARACION DE ENUM

	public static enum EstadoPagina {
		VISUALIZANDO(0, "VISUALIZANDO"), AGREGANDO(1, "AGREGANDO"), MODIFICANDO(2, "MODIFICANDO"), BUSCANDO(3, "BUSCANDO"), SUSPENDIDO(4, "SUSPENDIDO");

		private final int varKey;
		private final String varValue;

		EstadoPagina(int parKey, String parValue) {

			this.varKey = parKey;
			this.varValue = parValue;
		}

		public int getKey() {

			return this.varKey;
		}

		public String getVal() {

			return this.varValue;
		}// FIN DE METODOS
	}// FIN DE DECLARACION DE ENUM

	public static final String[] strDia = { "DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO" };
	public static final String[] strMes = { "0", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" };

	public AuxiliarTextil() {
	}

	public AuxiliarTextil(AuxiliarTextil m) {
		manPadre = m;
	}

	public Date obtenerFechaActual() {

		int varDia, varMes, varAno;
		fechaActual = (Date) ContenedorTextil.getComponent("FechaActual");

		Calendar now = Calendar.getInstance();

		varAno = now.get(Calendar.YEAR);
		varMes = (now.get(Calendar.MONTH) + 1);
		varDia = now.get(Calendar.DATE);

		fechaActual.setYear(varAno - 1900);
		fechaActual.setMonth(varMes - 1);
		fechaActual.setDate(varDia);

		return fechaActual;

	}

	public void obtenerFechaHoraActual() {

		obtenerFechaHoraServidor();

	}

	public void obtenerFechaHoraCliente() {
		fechaActual = (Date) ContenedorTextil.getComponent("FechaActual");
		horaActual = (Time) ContenedorTextil.getComponent("HoraActual");

		int varAno, varMes, varDia, varHor, varMin, varSeg;
		now = Calendar.getInstance();

		varAno = now.get(Calendar.YEAR);
		varMes = (now.get(Calendar.MONTH) + 1);
		varDia = now.get(Calendar.DATE);

		int varConHor;
		if (now.get(Calendar.AM_PM) == 1) {// VERIFICA SI LA HORA ES AM O PM
			varConHor = now.get(Calendar.HOUR) + 12;
		} else {
			varConHor = now.get(Calendar.HOUR);
		}

		horaActual.setHours(varConHor);
		horaActual.setMinutes(now.get(Calendar.MINUTE));
		horaActual.setSeconds(now.get(Calendar.SECOND));

		fechaActual.setYear(varAno - 1900);
		fechaActual.setMonth(varMes - 1);
		fechaActual.setDate(varDia);

	}

	public void obtenerFechaHoraServidor() {

		Connection conecction;
		ResultSet resSql;
		fechaActual = (Date) ContenedorTextil.getComponent("FechaActual");
		horaActual = (Time) ContenedorTextil.getComponent("HoraActual");

		try {

			conecction = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection();
			if (DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getDriverJdbc().equals(DriverJdbc.SQLSERVER))
			{
				resSql = conecction.prepareStatement("select getdate() as fechaHora").executeQuery();	
			} else
			{
				resSql = conecction.prepareStatement("select curdate() as fechaHora").executeQuery();
			}
			
			

			if (resSql.next()) {

				horaActual.setHours(resSql.getTime("fechaHora").getHours());
				horaActual.setMinutes(resSql.getTime("fechaHora").getMinutes());
				horaActual.setSeconds(resSql.getTime("fechaHora").getSeconds());

				fechaActual.setYear(resSql.getDate("fechaHora").getYear());
				fechaActual.setMonth(resSql.getDate("fechaHora").getMonth());
				fechaActual.setDate(resSql.getDate("fechaHora").getDate());
			}

			resSql.close();
			conecction.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public void abrirPantalla(String parHijo, String parPadre, AccesoPagina paracceso) {

		if (parHijo.equals("FrmPrincipal")) {

			if (frmPri == null) {

				frmPri = (FrmPrincipal) ContenedorTextil.getComponent(parHijo);
				frmPri.setManejador(this);

			}// Fin de if(frmPri == null)

			frmPri.padre = parPadre;
			frmPri.acceso = paracceso;
			frmPri.iniciarFormulario();

		}// Fin de abrir pantalla DlgEmpresa
		else if (parHijo.equals("DlgProducto")) {

			if (dlgPrd == null) {

				dlgPrd = (DlgProducto) ContenedorTextil.getComponent(parHijo);
				dlgPrd.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgPrd.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgPrd.padre);
			dlgPrd.acceso = paracceso;
			dlgPrd.iniciarFormulario();

		}// Fin de abrir pantalla DlgListaCotizacion
		else if (parHijo.equals("DlgPedido")) {

			if (dlgPed == null) {

				dlgPed = (DlgPedido) ContenedorTextil.getComponent(parHijo);
				dlgPed.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgPed.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgPed.padre);
			dlgPed.acceso = paracceso;
			dlgPed.iniciarFormulario();

		}else if (parHijo.equals("DlgPedidoImportacion")) {

				if (dlgPedImp == null) {

					dlgPedImp = (DlgPedidoImportacion) ContenedorTextil.getComponent(parHijo);
					dlgPedImp.setManejador(this);

				}// Verificar si existe la instancia de la interfaz
				dlgPedImp.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgPedImp.padre);
				dlgPedImp.acceso = paracceso;
				dlgPedImp.iniciarFormulario();

		} else if (parHijo.equals("DlgControlPedido")) {

			if (dlgCtrlPed == null) {

				dlgCtrlPed = (DlgControlPedido) ContenedorTextil.getComponent(parHijo);
				dlgCtrlPed.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgCtrlPed.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgCtrlPed.padre);
			dlgCtrlPed.acceso = paracceso;
			dlgCtrlPed.iniciarFormulario();

		} else if (parHijo.equals("DlgControlProducto")) {

			if (dlgCtrlPrd == null) {

				dlgCtrlPrd = (DlgControlProducto) ContenedorTextil.getComponent(parHijo);
				dlgCtrlPrd.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgCtrlPrd.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgCtrlPrd.padre);
			dlgCtrlPrd.acceso = paracceso;
			dlgCtrlPrd.iniciarFormulario();

		} else if (parHijo.equals("DlgCodigoProducto")) {

			if (dlgCodPrd == null) {

				dlgCodPrd = (DlgCodigoProducto) ContenedorTextil.getComponent(parHijo);
				dlgCodPrd.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgCodPrd.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgCodPrd.padre);
			dlgCodPrd.acceso = paracceso;
			dlgCodPrd.iniciarFormulario();

		} else if (parHijo.equals("DlgArea")) {

			if (dlgAre == null) {

				dlgAre = (DlgArea) ContenedorTextil.getComponent(parHijo);
				dlgAre.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgAre.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgAre.padre);
			dlgAre.acceso = paracceso;
			dlgAre.iniciarFormulario();

		} else if (parHijo.equals("DlgFicha")) {

			if (dlgFic == null) {

				dlgFic = (DlgFicha) ContenedorTextil.getComponent(parHijo);
				dlgFic.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgFic.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgFic.padre);
			dlgFic.acceso = paracceso;
			dlgFic.iniciarFormulario();

		} else if (parHijo.equals("DlgAcceso")) {

			if (dlgAcc == null) {

				dlgAcc = (DlgAcceso) ContenedorTextil.getComponent(parHijo);
				dlgAcc.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgAcc.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgAcc.padre);
			dlgAcc.acceso = paracceso;
			dlgAcc.iniciarFormulario();

		} else if (parHijo.equals("DlgControlProceso")) {
			if (dlgCtrlProc == null) {

				dlgCtrlProc = (DlgControlProceso) ContenedorTextil.getComponent(parHijo);
				dlgCtrlProc.setManejador(this);

			}// Verificar si existe la instancia de la interfaz
			dlgCtrlProc.padre = (paracceso.equals(AccesoPagina.INICIAR) ? parPadre : dlgCtrlProc.padre);
			dlgCtrlProc.acceso = paracceso;
			dlgCtrlProc.iniciarFormulario();

		}// Fin de abrir pantalla DlgListaCotizacion
	}// Fin de abrir pantalla

	/*
	 * 
	 * 
	 * 
	 * AGREGAR REGISTROS A LA BASE DE DATOS
	 */
	public int agregarProducto(Producto parPrd) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		if (!existeProducto(parPrd.getPrd_cod(), parPrd.getPrd_ide())) {

			parPrd.setPrd_ver(1);
			return prdDAO.agregar(parPrd);

		}
		return 0;
	}// Fin de agregarVoucher()

	public int agregarProductoSinValidar(Producto parPrd) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		parPrd.setPrd_ver(1);
		return prdDAO.agregar(parPrd);
	}// Fin de agregarVoucher()

	public int agregarCliente(int parCliCod, String parCliIde) {

		ClienteDAO cliDAO = (ClienteDAO) ContenedorTextil.getComponent("ClienteDAO");
		if (validarDatoCliente()) {
			if (!existeCliente(parCliCod, parCliIde)) {

				nuevoRegistroCliente();
				return cliDAO.agregar(cli);

			}
		}
		return 0;
	}// Fin de agregarVoucher()

	public int agregarPedido() {

		PedidoDAO pedDAO = (PedidoDAO) ContenedorTextil.getComponent("PedidoDAO");
		ped.setCli_cod(cli.getCli_cod());
		return pedDAO.agregar(ped);

	}// Fin de agregarKardex()

	public int addDetPedido(Producto parPrd, Grupo parGru) 
	{
		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");

		detPed.setPed_cod(ped.getPed_cod());
		detPed.setPrd_cod(parPrd.getPrd_cod());
		detPed.setGru_cod(parGru.getGru_cod());
		detPed.setDetped_est(EstadoDocumento.PENDIENTE.getVal());
		detPed.setDetped_ver(1);
		return detPedDAO.agregar(detPed);
	}// Fin de agregarKardex()
	
	public int agregarDetPedido(Producto parPrd, Grupo parGru) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");

		if (!existeProductoDetPedido(ped.getPed_cod(), parPrd.getPrd_cod(), parPrd.getPrd_ide())) {

			detPed.setPed_cod(ped.getPed_cod());
			detPed.setPrd_cod(parPrd.getPrd_cod());
			detPed.setGru_cod(parGru.getGru_cod());
			detPed.setDetped_est(EstadoDocumento.PENDIENTE.getVal());
			detPed.setDetped_ver(1);
			return detPedDAO.agregar(detPed);
		}
		return 0;
	}// Fin de agregarKardex()

	public int agregarArea() {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		are.setAre_ver(1);

		return areDAO.agregar(are);

	}// Fin de agregarVoucher()

	public int agregarSubArea() {

		SubAreaDAO subAreDAO = (SubAreaDAO) ContenedorTextil.getComponent("SubAreaDAO");
		subAre.setSubare_ver(1);
		return subAreDAO.agregar(subAre);

	}// Fin de agregarVoucher()

	public int agregarClase() {

		ClaseDAO claDAO = (ClaseDAO) ContenedorTextil.getComponent("ClaseDAO");
		cla.setCla_ver(1);
		return claDAO.agregar(cla);

	}// Fin de agregarVoucher()

	public int agregarFichasPedido(int parPedCod, int parPrdNiv) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		lisDetPed.clear();
		lisDetPed.addAll(detPedDAO.obtenerListaDetPedidoFicha(parPedCod, parPrdNiv));

		int recFic = 0;

		obtenerFechaHoraActual();

		for (DetPedido iteDetPed : lisDetPed) {
			recFic = agregarFichaDetPedido(iteDetPed);
			if (recFic == 0) {
				break;
			}
		}

		// ped.setPed_est(EstadoDocumento.PRODUCCION.getVal());
		// actualizarPedido();

		return recFic;

	}// Fin de agregarVoucher()

	public int agregarFichaDetPedido(DetPedido parDetPed) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");

		int recFic = 0;
		int totFic = parDetPed.getDetped_canped() / 10;
		int resto = parDetPed.getDetped_canped() % 10;
		totFic = (resto > 0) ? (totFic + 1) : totFic;

		for (int ite = 1; ite <= totFic; ite++) {

			fic.setSubare_cod(0);
			fic.setAre_cod(0);
			fic.setDetped_cod(parDetPed.getDetped_cod());
			fic.setPed_cod(parDetPed.getPed_cod());
			fic.setPrd_cod(parDetPed.getPrd_cod());
			fic.setGru_cod(parDetPed.getGru_cod());
			nuevoNumeroIde("FICHA", ite + "-" + totFic);
			// fic.setFic_ide(GENERADO POR NUEVONUMEROIDE);
			fic.setFic_fecing(fechaActual);
			fic.setFic_horing(horaActual);
			fic.setFic_fecsal(fechaActual);
			fic.setFic_horsal(horaActual);
			fic.setFic_canped((ite == totFic && resto > 0) ? resto : 10);
			fic.setFic_estado(EstadoDocumento.PENDIENTE.getVal());// Gestionado por el trigger de sqlserver
			fic.setFic_bar(KeyGenerator.getInst().obtenerCodigoBarra());
			fic.setFic_ver(1);
			recFic = ficDAO.agregar(fic);

		}

		return recFic;

	}// Fin de agregarVoucher()

	public int agregarFichaAreasDetPedido(DetPedido parDetPed) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");

		int recFic = 0;
		int totFic = parDetPed.getDetped_canped() / 10;
		int resto = parDetPed.getDetped_canped() % 10;
		totFic = (resto > 0) ? (totFic + 1) : totFic;

		for (int ite = 1; ite <= totFic; ite++) {

			fic.setSubare_cod(0);
			fic.setAre_cod(0);
			fic.setDetped_cod(parDetPed.getDetped_cod());
			fic.setPed_cod(parDetPed.getPed_cod());
			fic.setPrd_cod(parDetPed.getPrd_cod());
			fic.setGru_cod(parDetPed.getGru_cod());
			nuevoNumeroIde("FICHA", ite + "-" + totFic);
			// fic.setFic_ide(GENERADO POR NUEVONUMEROIDE);
			fic.setFic_fecing(fechaActual);
			fic.setFic_horing(horaActual);
			fic.setFic_fecsal(fechaActual);
			fic.setFic_horsal(horaActual);
			fic.setFic_canped((ite == totFic && resto > 0) ? resto : 10);
			fic.setFic_estado(EstadoDocumento.PENDIENTE.getVal());// Gestionado por el trigger de sqlserver
			fic.setFic_bar(KeyGenerator.getInst().obtenerCodigoBarra());
			fic.setFic_ver(1);
			recFic = ficDAO.agregar(fic);

		}

		return recFic;

	}// Fin de agregarVoucher()

	public int agregarFichaBlanca() {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		int recFic = 0;

		obtenerFechaHoraActual();

		fic.setSubare_cod(0);
		fic.setAre_cod(0);
		// fic.setFic_ide(GENERADO POR NUEVONUMEROIDE);
		fic.setFic_fecing(fechaActual);
		fic.setFic_horing(horaActual);
		fic.setFic_fecsal(fechaActual);
		fic.setFic_horsal(horaActual);
		fic.setFic_estado(EstadoDocumento.PENDIENTE.getVal());// Gestionado por trigger sqlserver
		fic.setFic_ver(1);
		recFic = ficDAO.agregar(fic);

		// ped.setPed_est(EstadoDocumento.PRODUCCION.getVal());
		// actualizarPedido();

		return recFic;

	}// Fin de agregarVoucher()

	public int actualizarFicha() {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.actualizar(fic);

	}// Fin de agregarVoucher()

	public int actualizarFichaBarraSubArea() {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.actualizarBarraSubArea(fic);

	}// Fin de agregarVoucher()

	public int actualizarArea() {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		return areDAO.actualizar(are);

	}// Fin de agregarVoucher()

	public int actualizarSubArea() {

		SubAreaDAO subAreDAO = (SubAreaDAO) ContenedorTextil.getComponent("SubAreaDAO");
		return subAreDAO.actualizar(subAre);

	}// Fin de agregarVoucher()

	public int actualizarDetPedido(Producto parPrd, Grupo parGru) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		if (!existeProductoDetPedido(ped.getPed_cod(), parPrd.getPrd_cod(), parPrd.getPrd_ide())) {
			detPedTmp.setObjeto(obtenerDetPedido(ped.getPed_cod(), parPrd.getPrd_cod()));
			detPedTmp.setDetped_canped(detPed.getDetped_canped());
			return detPedDAO.actualizar(detPedTmp);
		}

		return 0;

	}// Fin de agregarKardex()

	public int updateDetPedido(Producto parPrd, Grupo parGru) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		detPed.setObjeto(obtenerDetPedido(ped.getPed_cod(), parPrd.getPrd_cod()));
		detPed.setDetped_canped(detPed.getDetped_canped());
		return detPedDAO.actualizar(detPedTmp);
	}// Fin de agregarKardex()

	public void actualizarTotalDetPedido(int parPedCod, int parPrdNiv, int parPrdCod) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		detPedDAO.actualizarTotalDetPedito(parPedCod, parPrdNiv, parPrdCod);

	}// Fin de agregarKardex()

	public void agregarListaDetPedido() {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");

		for (DetPedido iteDetPed : lisDetPed) {

			iteDetPed.setPed_cod(ped.getPed_cod());
			detPedDAO.agregar(iteDetPed);

		}// Fin de for(DetPedido iteDetPed : lisDetPed)
	}// Fin de guardarDetKardexDB

	/*
	 * 
	 * 
	 * 
	 * ACTUALIZAR REGISTROS A LA BASE DE DATOS
	 */

	/*
	 * 
	 * 
	 * 
	 * BUSQUEDAS A LA BASE DE DATOS
	 */
	public int actualizarCliente(int parCliCod, String parCliIde) {

		ClienteDAO cliDAO = (ClienteDAO) ContenedorTextil.getComponent("ClienteDAO");
		if (validarDatoCliente()) {
			if (!existeCliente(parCliCod, parCliIde)) {
				return cliDAO.actualizar(cli);
			}
		}
		return 0;
	}// Fin de agregarVoucher()

	public int actualizarProductoSinValidar(Producto parPrd) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.actualizar(parPrd);
	}// Fin de agregarVoucher()

	public int actualizarProducto(Producto parPrd) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		if (!existeProducto(parPrd.getPrd_cod(), parPrd.getPrd_ide())) {
			return prdDAO.actualizar(parPrd);
		}
		return 0;
	}// Fin de agregarVoucher()

	public int actualizarPedido() {

		PedidoDAO pedDAO = (PedidoDAO) ContenedorTextil.getComponent("PedidoDAO");
		return pedDAO.actualizar(ped);

	}// Fin de agregarKardex()

	public boolean existeCliente(int parCliCod, String parCliIde) {

		ClienteDAO cliDAO = (ClienteDAO) ContenedorTextil.getComponent("ClienteDAO");
		return cliDAO.existeCliente(parCliCod, parCliIde);

	}

	public boolean existeClase(String parClaNom) {

		ClaseDAO claDAO = (ClaseDAO) ContenedorTextil.getComponent("ClaseDAO");
		return claDAO.existeClase(parClaNom);

	}

	public boolean existeRegistroZona(String parTipoZona) {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		String sql = "";
		if (parTipoZona.equals("AREA")) {

			sql = "select * from area";
			return areDAO.existeRegistroZona(sql);

		} else if (parTipoZona.equals("SUBAREA")) {

			sql = "select * from subarea";
			return areDAO.existeRegistroZona(sql);
		}
		return false;
	}

	public boolean existeCodigoBarra(String parTabla, String parCodBar) {

		String sql = "";
		if (parTabla.equals("AREA")) {
			sql = "select * from area where are_bar='" + parCodBar + "'";
		} else if (parTabla.equals("SUBAREA")) {
			sql = "select * from subarea where subare_bar='" + parCodBar + "'";
		} else if (parTabla.equals("PRODUCTO")) {
			sql = "select * from producto where prd_bar='" + parCodBar + "'";
		}

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.existeCodigoBarra(sql);

	}

	public boolean existeFichaSubArea(Ficha parFic, SubArea parSubAre) {

		String sql = "";
		sql = "select * from ficha where subare_cod=" + parSubAre.getSubare_cod() + " and are_cod=" + parSubAre.getAre_cod() + " and fic_bar='" + parFic.getFic_bar() + "'";
		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.existeFicha(sql);

	}

	public boolean existeFichaArea(Ficha parFic, Area parAre) {

		String sql = "";
		sql = "select * from ficha where are_cod=" + parAre.getAre_cod() + " and fic_bar='" + parFic.getFic_bar() + "'";
		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.existeFicha(sql);

	}

	public int numeroOrdenUltimaAreaRegistrada(Ficha parFic, SubArea parSubAre) {

		String sql = "select top 1 are_ord from viewFichaProducto where ped_cod=" + parFic.getPed_cod() + " and " +
				"detped_cod=" + parFic.getDetped_cod() + " and fic_bar='" + parFic.getFic_bar() + "' order by fic_fecing desc";
		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.numeroOrdenUltimaAreaRegistrada(sql);

	}

	public boolean existeProducto(int parPrdCod, String parPrdIde) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.existeProducto(parPrdCod, parPrdIde);

	}

	public boolean existeProductoDetPedido(int parPedCod, int parPrdCod, String parPrdIde) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		return detPedDAO.existeProductoDetPedido(parPedCod, parPrdCod, parPrdIde);

	}

	public boolean existeProductoPadrePedido(int parPedCod, int parPrdNiv, int parPrdCod) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.existeProductoPadrePedido(parPedCod, parPrdNiv, parPrdCod);

	}

	public boolean existeProductoPadre(int parPrdNiv, int parPrdCod) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.existeProductoPadre(parPrdNiv, parPrdCod);

	}

	public boolean buscarPedidoDetalle(int parPedCod, int parPrdNiv, int parPrdCod) {

		ped.setObjeto(obtenerPedido(parPedCod));
		if (ped.getPed_cod() != 0) {

			cli.setObjeto(obtenerCliente(ped.getCli_cod()));
			if (cli.getCli_cod() != 0) {

				obtenerListaDetallePedido();

				lisMod.addAll(obtenerListaProductoPedido(parPedCod, 1, 0));
				if (lisMod != null && !lisMod.isEmpty()) {

					mod.setObjeto(lisMod.get(0));
					gruMod.setObjeto(obtenerItemListaGrupo(mod.getGru_cod()));

					lisPrd.clear();
					lisPrd.addAll(obtenerListaProductoPedido(parPedCod, 2, mod.getPrd_cod()));

				}

				return true;

				// }// fin de if(llenarListaCronogramaDetDocumento())
			}// Fin de if (per.getPer_cod() != 0)
		}// Fin de if (ped.getPed_cod() != 0)

		return false;

	}// fIN DE buscarDocumentoVenta()

	public boolean buscarPedidoFicha(int parPedCod) {

		ped.setObjeto(obtenerPedido(parPedCod));
		if (ped.getPed_cod() != 0) {

			cli.setObjeto(obtenerCliente(ped.getCli_cod()));
			if (cli.getCli_cod() != 0) {

				obtenerListaDetallePedido();
				obtenerListaProductoGeneral("TODOS");

				FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
				lisFic.clear();
				lisFic.addAll(ficDAO.obtenerListaFicha(ped.getPed_cod()));

				return true;

				// }// fin de if(llenarListaCronogramaDetDocumento())
			}// Fin de if (per.getPer_cod() != 0)
		}// Fin de if (ped.getPed_cod() != 0)

		return false;

	}// fIN DE buscarDocumentoVenta()

	/*
	 * 
	 * 
	 * 
	 * 
	 * OBTENER REGISTRO DE LA BASE DE DATOS
	 */

	public Producto obtenerProductoPadre(Producto parPrdHij) {

		// if (parPrdHij.getPrd_nivel() == 1) {} else Nivel demodelo no se obtiene nada y se evalua en la condicion ELSE
		if (parPrdHij.getPrd_nivel() == 2) {
			return obtenerProducto(parPrdHij.getPrd_supuno());
		} else if (parPrdHij.getPrd_nivel() == 3) {
			return obtenerProducto(parPrdHij.getPrd_supdos());
		} else if (parPrdHij.getPrd_nivel() == 4) {
			return obtenerProducto(parPrdHij.getPrd_suptres());
		} else if (parPrdHij.getPrd_nivel() == 5) {
			return obtenerProducto(parPrdHij.getPrd_supcua());
		} else {
			return obtenerProducto(0);
		}
	}

	public Pedido obtenerPedido(int parPedCod) {

		PedidoDAO pedDAO = (PedidoDAO) ContenedorTextil.getComponent("PedidoDAO");
		return pedDAO.obtenerPedido(parPedCod);

	}

	public List<Integer> obtenerPedidoDetPedidoProducto(int parPrdCod) {

		PedidoDAO pedDAO = (PedidoDAO) ContenedorTextil.getComponent("PedidoDAO");
		return pedDAO.obtenerPedidoDetPedidoProducto(parPrdCod);

	}

	public Pedido obtenerPedido(String parPedNomDoc, String parPedNumDoc) {

		PedidoDAO pedDAO = (PedidoDAO) ContenedorTextil.getComponent("PedidoDAO");
		return pedDAO.obtenerPedido(parPedNomDoc, parPedNumDoc);

	}

	public Cliente obtenerCliente(int parCliCod) {

		ClienteDAO cliDAO = (ClienteDAO) ContenedorTextil.getComponent("ClienteDAO");
		return cliDAO.obtenerCliente(parCliCod);

	}

	public Ficha obtenerFicha(int parFicCod) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.obtenerFicha(parFicCod);

	}

	public Ficha obtenerFicha(int parSubAreCod, int parAreCod, int parPrdCod) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		String sql = "select * from ficha where subare_cod=" + parSubAreCod + " and are_cod=" + parAreCod + " and prd_cod=" + parPrdCod;

		return ficDAO.obtenerEntidad(sql);

	}

	public Grupo obtenerGrupo(String parGruNom) {

		GrupoDAO gruDAO = (GrupoDAO) ContenedorTextil.getComponent("GrupoDAO");
		return gruDAO.obtenerGrupo(parGruNom);

	}

	public Area obtenerArea(int parAreCod) {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		return areDAO.obtenerArea(parAreCod);

	}

	public SubArea obtenerSubArea(int parSubAreCod) {

		SubAreaDAO subAreDAO = (SubAreaDAO) ContenedorTextil.getComponent("SubAreaDAO");
		return subAreDAO.obtenerSubArea(parSubAreCod);

	}

	public Area obtenerArea(String parAreNom) {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		return areDAO.obtenerArea(parAreNom);

	}

	public SubArea obtenerSubArea(String parSubAreNom) {

		SubAreaDAO subAreDAO = (SubAreaDAO) ContenedorTextil.getComponent("SubAreaDAO");
		return subAreDAO.obtenerSubArea(parSubAreNom);

	}

	public SubArea obtenerSubAreaBarra(String parSubAreBar) {

		SubAreaDAO subAreDAO = (SubAreaDAO) ContenedorTextil.getComponent("SubAreaDAO");
		String sql = "select * from subarea where subare_bar='" + parSubAreBar + "' and subare_act=1";

		return subAreDAO.obtenerEntidad(sql);

	}

	public Ficha obtenerFichaBarra(String parFicBar) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		String sql = "select * from ficha where fic_bar='" + parFicBar + "'";

		return ficDAO.obtenerEntidad(sql);

	}

	public int obtenerCantidadRestanteFichaSubarea(String fic_bar, int subare_cod) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		String sql = "select sum(fic_canreg) tot_canreg from ficha where fic_bar=" + fic_bar + " and subare_cod="+subare_cod;
		ResultSet resSql;
		try {
			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(sql);
			if (resSql.next()) {
				return resSql.getInt("tot_canreg");
			}// Fin de

			resSql.close();
		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
		return 0;
	}

	public Ficha obtenerFichaBarraPendiente(String parFicBar) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		String sql = "select * from ficha where fic_bar='" + parFicBar + "' and fic_estado='PENDIENTE'";

		return ficDAO.obtenerEntidad(sql);

	}

	public Producto obtenerProducto(int parPrdCod) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.obtenerProducto(parPrdCod);

	}

	public Producto obtenerProductoBarra(String parPrdBar) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		String sql = "select * from producto where prd_bar='" + parPrdBar + "'";
		return prdDAO.obtenerEntidad(sql);

	}

	public DetPedido obtenerDetPedido(int parPedCod, int parPrdCod) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		return detPedDAO.obtenerDetPedido(parPedCod, parPrdCod);

	}

	public DetPedido obtenerDetPedido(int parDetPedCod) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		return detPedDAO.obtenerDetPedido(parDetPedCod);

	}

	public Producto obtenerProductoIde(String parPrdIde) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.obtenerProductoIde(parPrdIde);

	}

	public int obtenerModeoloCodigo(String parPrdIde) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.obtenerModeloCodigo(parPrdIde);

	}

	public Usuario obtenerUsuario(String parUsuIde) {

		UsuarioDAO usuDAO = (UsuarioDAO) ContenedorTextil.getComponent("UsuarioDAO");
		return usuDAO.obtenerUsuario(parUsuIde);

	}

	/*
	 * 
	 * 
	 * OBTENER ITEM DE LISTAS
	 */
	public int indiceListaGrupo(int parGruCod) {
		int idx = -1;
		boolean existe = false;

		for (Grupo iteGru : lisGru) {

			idx++;
			if (iteGru.getGru_cod() == parGruCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaClase(int parClaCod) {
		int idx = -1;
		boolean existe = false;

		for (Clase iteCla : lisCla) {

			idx++;
			if (iteCla.getCla_cod() == parClaCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaFicha(int parFicCod) {
		int idx = -1;
		boolean existe = false;

		for (Ficha iteFic : lisFic) {

			idx++;
			if (iteFic.getFic_cod() == parFicCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaArea(int parAreCod) {
		int idx = -1;
		boolean existe = false;

		for (Area iteAre : lisAre) {

			idx++;
			if (iteAre.getAre_cod() == parAreCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaSubArea(int parSubAreCod) {
		int idx = -1;
		boolean existe = false;

		for (SubArea iteSubAre : lisSubAre) {

			idx++;
			if (iteSubAre.getSubare_cod() == parSubAreCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaUsuario(String parUsuIde) {
		int idx = -1;
		boolean existe = false;

		for (Usuario iteUsu : lisUsu) {

			idx++;
			if (iteUsu.getIdentidad() == parUsuIde) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaProducto(int parPrdCod) {
		int idx = -1;
		boolean existe = false;

		for (Producto itePrdGen : lisPrd) {

			idx++;
			if (itePrdGen.getPrd_cod() == parPrdCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaProductoGeneral(int parPrdCod) {
		int idx = -1;
		boolean existe = false;

		for (Producto itePrdGen : lisPrdGen) {

			idx++;
			if (itePrdGen.getPrd_cod() == parPrdCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaTalla(int parTalCod) {
		int idx = -1;
		boolean existe = false;

		for (Producto iteTal : lisTal) {

			idx++;
			if (iteTal.getPrd_cod() == parTalCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	public int indiceListaDetPedido(int parDetPedCod) {
		int idx = -1;
		boolean existe = false;

		for (DetPedido iteDetPed : lisDetPed) {

			idx++;
			if (iteDetPed.getDetped_cod() == parDetPedCod) {
				existe = true;
				break;
			}
		}
		return (existe == false) ? -1 : idx;
	}// Fin de indiceListaEmpresa(int parEmpCod)

	/*
	 * 
	 * OBTENER REGISTRO DE LISTAS
	 */

	public Usuario obtenerItemListaUsuario(String parUsuIde) {
		// System.out.println("Obtenemos detkardex de  lista Detkardex" +
		// this.getClass().getName());
		int indice = indiceListaUsuario(parUsuIde);
		if (indice != -1) {

			return lisUsu.get(indice);

		} else {

			Util.getInst().limpiarEntidad(usu, true);
			return usu;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public Producto obtenerItemListaProducto(int parPrdCod) {
		// System.out.println("Obtenemos detkardex de  lista Detkardex" +
		// this.getClass().getName());
		int indice = indiceListaProducto(parPrdCod);
		if (indice != -1) {

			return lisPrd.get(indice);

		} else {

			prdGen.limpiarInstancia();
			return prdGen;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public Producto obtenerItemListaProductoGeneral(int parPrdCod) {
		// System.out.println("Obtenemos detkardex de  lista Detkardex" +
		// this.getClass().getName());
		int indice = indiceListaProductoGeneral(parPrdCod);
		if (indice != -1) {

			return lisPrdGen.get(indice);

		} else {

			prdGen.limpiarInstancia();
			return prdGen;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public Producto obtenerItemListaTalla(int parPrdCod) {
		// System.out.println("Obtenemos detkardex de  lista Detkardex" +
		// this.getClass().getName());
		int indice = indiceListaTalla(parPrdCod);
		if (indice != -1) {

			return lisTal.get(indice);

		} else {

			prdGen.limpiarInstancia();
			return prdGen;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public DetPedido obtenerItemListaDetPedido(int parDetPed) {
		// System.out.println("Obtenemos detkardex de  lista Detkardex" +
		// this.getClass().getName());
		int indice = indiceListaDetPedido(parDetPed);
		if (indice != -1) {

			return lisDetPed.get(indice);

		} else {

			DetPedido varDetPed = new DetPedido();
			varDetPed.limpiarInstancia();
			return varDetPed;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public Grupo obtenerItemListaGrupo(int parGruCod) {
		int indice = indiceListaGrupo(parGruCod);
		if (indice != -1) {

			return lisGru.get(indice);

		} else {

			Grupo varGru = new Grupo();
			varGru.limpiarInstancia();
			return varGru;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public Clase obtenerItemListaClase(int parClaCod) {
		int indice = indiceListaClase(parClaCod);
		if (indice != -1) {

			return lisCla.get(indice);

		} else {

			Clase varCla = new Clase();
			varCla.limpiarInstancia();
			return varCla;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public Ficha obtenerItemListaFicha(int parFicCod) {
		int indice = indiceListaFicha(parFicCod);
		if (indice != -1) {

			return lisFic.get(indice);

		} else {

			Ficha varFic = new Ficha();
			varFic.limpiarInstancia();
			return varFic;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public Area obtenerItemListaArea(int parAreCod) {
		int indice = indiceListaArea(parAreCod);
		if (indice != -1) {

			return lisAre.get(indice);

		} else {

			Area varAre = new Area();
			varAre.limpiarInstancia();
			return varAre;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	public SubArea obtenerItemListaSubArea(int parSubAreCod) {
		int indice = indiceListaSubArea(parSubAreCod);
		if (indice != -1) {

			return lisSubAre.get(indice);

		} else {

			SubArea varSubAre = new SubArea();
			varSubAre.limpiarInstancia();
			return varSubAre;

		}
	}// Fin de obtenerItemListaEmpresa(int parEmpCod)

	/*
	 * OBTENER LISTA DE REGISTROS
	 */

	public void obtenerListaCtrlTabla() {

		CtrlTablaDAO ctrlTabDAO = (CtrlTablaDAO) ContenedorTextil.getComponent("CtrlTablaDAO");
		lisCtrlTab.clear();
		lisCtrlTab.addAll(ctrlTabDAO.obtenerListaCtrlTabla());

	}// Fin de obtenerDatoDBArticulo

	public void obtenerListaUsuario(String parUsuNom, String parUsuEst) {

		UsuarioDAO usuDAO = (UsuarioDAO) ContenedorTextil.getComponent("UsuarioDAO");

		lisUsu.clear();
		lisUsu.addAll(usuDAO.obtenerListaUsuario(parUsuNom, parUsuEst));

	}// Fin de public void obtenerDBListaUsuario(String parEmlNom)

	public void obtenerListaFicha(String parSql) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		lisFic.clear();
		lisFic.addAll(ficDAO.obtenerLista(parSql));
	}

	public void obtenerListaGroupFicha(String parSql) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		lisFic.clear();
		lisFic.addAll(ficDAO.obtenerListaGroupFicha(parSql));
	}

	public List<String> obtenerListaGrupoPedido(int parPedCod) {

		GrupoDAO gruDAO = (GrupoDAO) ContenedorTextil.getComponent("GrupoDAO");
		return gruDAO.obtenerListaGrupoPedido(parPedCod);

	}// Fin de public List<Grupo> obtenerListaGrupo()

	public void obtenerListaGrupo() {
		try {
			if (lisGru.isEmpty()) {
				GrupoDAO gruDAO = (GrupoDAO) ContenedorTextil.getComponent("GrupoDAO");
				lisGru.addAll(gruDAO.obtenerListaGrupo());

			}// Fin de if(lisGru.isEmpty())
		} catch (Exception e) {

			e.printStackTrace();

		}
	}// Fin de public List<Grupo> obtenerListaGrupo()

	public void obtenerListaClase() {
		try {
			if (lisCla.isEmpty()) {

				ClaseDAO claDAO = (ClaseDAO) ContenedorTextil.getComponent("ClaseDAO");
				lisCla.addAll(claDAO.obtenerListaClase());

			}// Fin de if(lisGru.isEmpty())
		} catch (Exception e) {

			e.printStackTrace();

		}
	}// Fin de public List<Grupo> obtenerListaGrupo()

	public void obtenerListaDetallePedido() {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");

		lisDetPed.clear();
		lisDetPed.addAll(detPedDAO.obtenerListaDetPedido(ped.getPed_cod(), 0, 0));

	}// Fin de public List<Grupo> obtenerListaGrupo()

	public List<String> obtenerListaPedido(String parSql) {

		PedidoDAO pedDAO = (PedidoDAO) ContenedorTextil.getComponent("PedidoDAO");
		return pedDAO.obtenerListaPedido(parSql);

	}// Fin de public List<Grupo> obtenerListaGrupo()

	public List<String> obtenerListaCtrlProducto(String parSql) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.obtenerListaCtrlProducto(parSql);

	}// Fin de public List<Grupo> obtenerListaGrupo()

	public List<Cliente> obtenerListaCliente(String parPerNom) {

		ClienteDAO cliDAO = (ClienteDAO) ContenedorTextil.getComponent("ClienteDAO");
		return cliDAO.getListaCliente(parPerNom);

	}

	public List<String> obtenerListaNombreCliente(String parPerNom) {

		List<String> lisCliNom = new ArrayList<String>();
		lisCli.clear();
		lisCli.addAll(obtenerListaCliente(parPerNom));
		for (Cliente iteCli : lisCli) {

			lisCliNom.add(iteCli.getCli_nom());

		}
		return lisCliNom;

	}

	public List<String> obtenerListaClase(String parGruNom, String parClaNom) {

		ClaseDAO claDAO = (ClaseDAO) ContenedorTextil.getComponent("ClaseDAO");
		return claDAO.obtenerListaClase(parGruNom, parClaNom);

	}

	public List<String> obtenerListaProductoPadre(int parPrdNiv) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.obtenerListaProductoPadre(parPrdNiv);

	}

	public void obtenerListaArea(String parAreNom) {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		lisAre.clear();
		lisAre.addAll(areDAO.obtenerListaArea(parAreNom));

	}

	public void obtenerListaArea() {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		lisAre.clear();
		lisAre.addAll(areDAO.obtenerListaArea());

	}
	
	public void obtenerListaAreaControl() {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		lisAre.clear();
		lisAre.addAll(areDAO.obtenerListaAreaControl());

	}

	public void obtenerListaSubArea(int parAreCod) {

		SubAreaDAO subAreDAO = (SubAreaDAO) ContenedorTextil.getComponent("SubAreaDAO");
		lisSubAre.clear();
		lisSubAre.addAll(subAreDAO.obtenerListaSubArea(parAreCod));

	}

	public void obtenerListaSubArea() {

		SubAreaDAO subAreDAO = (SubAreaDAO) ContenedorTextil.getComponent("SubAreaDAO");
		lisSubAre.clear();
		lisSubAre.addAll(subAreDAO.obtenerListaSubArea());

	}

	public List<Producto> obtenerListaProductoPedido(int parPedCod, int parPrdNivSup, int parPrdCod) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.obtenerListaProductoPedido(parPedCod, parPrdNivSup, parPrdCod);

	}// Fin de public List<Grupo> obtenerListaGrupo()

	public void obtenerListaProducto(String parPrdNom) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		lisPrd.clear();
		lisPrd.addAll(prdDAO.obtenerListaProducto(parPrdNom));

	}

	public void obtenerListaProductoGeneral(String parPrdNom) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		lisPrdGen.clear();
		lisPrdGen.addAll(prdDAO.obtenerListaProducto(parPrdNom));

	}

	public List<Producto> obtenerListaProducto(int parPrdNiv, int parPrdCod, String parPrdIde) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.obtenerListaProducto(parPrdNiv, parPrdCod, parPrdIde);

	}

	public List<String> obtenerGrupoProducto(int parPrdNiv, String parPrdIde) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.obtenerGrupoProducto(parPrdNiv, parPrdIde);

	}

	public List<Producto> obtenerListaProducto(int parPrdPed, int parPrdNiv, int parPrdCod) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.obtenerListaProducto(parPrdPed, parPrdNiv, parPrdCod);

	}

	public List<String> obtenerListaModeloIde(String parModIde) {

		lisMod.clear();
		lisMod.addAll(obtenerListaProducto(1, 0, parModIde));
		List<String> lisModIde = new ArrayList<String>();

		for (Producto iteLisMod : lisMod) {

			lisModIde.add(iteLisMod.getPrd_ide());

		}
		return lisModIde;
	}

	public List<String> obtenerListaTallaDes(String parTalIde) {

		lisTal.clear();
		lisTal.addAll(obtenerListaProducto(3, 0, parTalIde));
		List<String> lisTalDes = new ArrayList<String>();

		for (Producto iteLisTal : lisTal) {
			if (!lisTalDes.contains(iteLisTal.getPrd_des())) {
				lisTalDes.add(iteLisTal.getPrd_des());
			}
		}
		return lisTalDes;
	}

	public List<String> obtenerListaProductoPedido(int parPedCod) {

		lisPrdGen.clear();
		lisPrdGen.addAll(obtenerListaProductoPedido(parPedCod, 2, 0));
		List<String> lisMod = new ArrayList<String>();

		for (Producto itePrdGen : lisPrdGen) {

			lisMod.add(itePrdGen.getPrd_ide());

		}
		return lisMod;
	}

	public List<String> obtenerListaTallaPedido(int parPedCod) {

		lisPrdGen.clear();
		lisPrdGen.addAll(obtenerListaProductoPedido(parPedCod, 3, 0));
		List<String> lisMod = new ArrayList<String>();

		for (Producto itePrdGen : lisPrdGen) {

			lisMod.add(itePrdGen.getPrd_ide());

		}
		return lisMod;
	}

	// public List<String> obtenerListaTalla(String parPrdIde) {
	//
	// return obtenerGrupoProducto(3, parPrdIde);
	// }

	/*
	 * 
	 * METODOS POR NUEVOS REGISTROS
	 */

	public String registrarControlFicha(String parBarCod, boolean alterarIngreso) {

		if (contadorMarcador == 1) {
			bookMarkBarraFicha = "";
			bookMarkBarraSubArea = "";
		}
		String respuesta = "";
		boolean marcar = false;
		
		fic.setObjeto(obtenerFichaBarraPendiente((bookMarkBarraFicha.equals("")) ? parBarCod : bookMarkBarraFicha));
		prd.setObjeto(obtenerProducto(fic.getPrd_cod()));
		subAre.setObjeto(obtenerSubAreaBarra((bookMarkBarraSubArea.equals("")) ? parBarCod : bookMarkBarraSubArea));
		are.setObjeto(obtenerArea(subAre.getAre_cod()));

		bookMarkBarraFicha = fic.getFic_bar();
		bookMarkBarraSubArea = subAre.getSubare_bar();
		
		int cantidad = 0;
		marcar=true;
		
		if (!bookMarkBarraFicha.equals("") && !bookMarkBarraSubArea.equals("")) 
		{
			int acumulado = obtenerCantidadRestanteFichaSubarea(fic.getFic_bar(), subAre.getSubare_cod());
			int restante = fic.getFic_canped() - acumulado;
			
			if( ( alterarIngreso && restante > 0) || ( restante > 0 && restante < fic.getFic_canped() ) )
			{
				String valor = "";
				while (!Util.isInteger(valor) && ( restante + cantidad )  <= fic.getFic_canped()) {
					valor = (String) JOptionPane.showInputDialog(null, "�Ingrese cantidad menor a "+restante+"?", testProp.getPropiedad("TituloSistema"), JOptionPane.QUESTION_MESSAGE);
					if(Util.isInteger(valor)){
						cantidad=Integer.parseInt(valor);
						marcar=true;
						break;
					}
					if(valor==null){
						fic.limpiarInstancia();
						prd.limpiarInstancia();
						subAre.limpiarInstancia();
						are.limpiarInstancia();
						marcar=false;
						break;
					}
				}
			}//if(alterarIngreso){
			else{
				cantidad = fic.getFic_canped();
			}

			if(marcar){
				if ( fic.getFic_cod() > 0 && fic.getFic_estado().equals(EstadoDocumento.PENDIENTE.getVal()) ) {
					if (!existeFichaArea(fic, are) || ( existeFichaArea(fic, are) && restante > 0 )) {
						if ((are.getAre_ord() > numeroOrdenUltimaAreaRegistrada(fic, subAre)) || agru.getSegcodigo().equals("ADMIN")) {

							obtenerFechaHoraActual();

							fic.setSubare_cod(subAre.getSubare_cod());
							fic.setAre_cod(are.getAre_cod());
							fic.setFic_fecing(fechaActual);
							fic.setFic_horing(horaActual);
							fic.setFic_fecsal(fechaActual);
							fic.setFic_horsal(horaActual);
							fic.setFic_estado(EstadoDocumento.PRODUCCION.getVal());
							fic.setFic_canreg( cantidad );

							if (actualizarFicha() > 0) {
								fic.setFic_canreg( 0 );
								if (agregarFichaBlanca() > 0) {
									fic.setFic_canreg( acumulado + cantidad);
									respuesta = "CORRECTO";

								} else {
									respuesta = "ERROR";
								}
							} else {
								respuesta = "ERROR";
							}
						} else {
							respuesta = "ERROR";
						}
					} else {
						fic.setFic_canreg(cantidad);
						respuesta = "ERROR";
					}
				} else if (fic.getFic_estado().equals(EstadoDocumento.TERMINADO.getVal()) 
						|| fic.getFic_estado().equals(EstadoDocumento.PRODUCCION.getVal())
						|| fic.getFic_canreg() == fic.getFic_canped()
						) {
					respuesta = "ADVERTENCIA";
				}
			}//if(marcar){
		} else if (bookMarkBarraFicha.equals("") && bookMarkBarraSubArea.equals("") && (contadorMarcador == 1 || contadorMarcador == 2)) {
			respuesta = "ERROR";
		} else if (bookMarkBarraFicha.equals("") && !bookMarkBarraSubArea.equals("") && contadorMarcador == 2) {
			respuesta = "ERROR";
		} else if (!bookMarkBarraFicha.equals("") && bookMarkBarraSubArea.equals("") && contadorMarcador == 2) {
			respuesta = "ERROR";
		} else {
			respuesta = "CORRECTO";
		}
		contadorMarcador = (contadorMarcador == 2) ? 1 : contadorMarcador + 1;					

		return respuesta;

	}// Fin de nuevoRegistroContacto()

	public void nuevoRegistroCliente() {

		cli.setCli_ver(1);

	}// Fin de nuevoRegistroContacto()

	public void nuevoRegistroPedido() {

		// ped.setPed_cod(0);
		ped.setCli_cod(0);
		// ped.setPed_nomdoc("YA GENERADO EN OTRO METODO");
		// ped.setPed_numdoc("YA GENERADO EN OTRO METODO");
		// ped.setPed_nom("");
		// ped.setPed_dir("");
		// ped.setPed_docide(");
		// ped.setPed_numide("");
		ped.setPed_fecreg(obtenerFechaActual());
		ped.setPed_fecent(obtenerFechaActual());
		ped.setPed_imptot(0.00);
		ped.setPed_est(EstadoDocumento.PENDIENTE.getVal());
		ped.setPed_ver(1);

	}// Fin de nuevoRegistroContacto()

	public void nuevoNumeroDocumento(String parDoc) {

		CtrlTablaDAO ctrlTabDAO = (CtrlTablaDAO) ContenedorTextil.getComponent("CtrlTablaDAO");
		// DOC001 : BOLETA
		// DOC001 : FACTURA
		// DOC007 : GUIA DE REMISION

		if (parDoc.equals("AREA")) {

			ctrlTab = ctrlTabDAO.getNumRegTab(parDoc);
			String codigoArea = "ARE-" + (obtenerFechaActual().getYear() - 100) + "-" + (obtenerFechaActual().getMonth() + 1);
			are.setAre_ide(funUsu.getCodTab(codigoArea + "-", ctrlTab.getCtrl_numreg(), 15));

		} else if (parDoc.equals("SUBAREA")) {

			ctrlTab = ctrlTabDAO.getNumRegTab(parDoc);
			String codigoSubArea = "SUB-" + (obtenerFechaActual().getYear() - 100) + "-" + (obtenerFechaActual().getMonth() + 1);
			subAre.setSubare_ide(funUsu.getCodTab(codigoSubArea + "-", ctrlTab.getCtrl_numreg(), 15));

		} else if (parDoc.equals("TALLA")) {

			ctrlTab = ctrlTabDAO.getNumRegTab(parDoc);
			String codigoTalla = "TALLA-" + (obtenerFechaActual().getYear() - 100) + "-" + (obtenerFechaActual().getMonth() + 1);
			tal.setPrd_ide(funUsu.getCodTab(codigoTalla + "-", ctrlTab.getCtrl_numreg(), 20));
			tal.setPrd_bar(KeyGenerator.getInst().obtenerCodigoBarra());

		} else {

			ctrlTab = ctrlTabDAO.getNumRegTab(parDoc);
			String numeroSerie = funUsu.getFillZero(ctrlTab.getCtrl_numser(), 3);
			ped.setPed_nomdoc(parDoc);
			ped.setPed_numdoc(funUsu.getCodTab(numeroSerie + "-", ctrlTab.getCtrl_numreg(), 11));

		}

	}// Fin de generarNuevaNumeroDocumento()

	public void nuevoNumeroIde(String parTipZona, String parNomZona) {

		CtrlTablaDAO ctrlTabDAO = (CtrlTablaDAO) ContenedorTextil.getComponent("CtrlTablaDAO");
		if (parTipZona.equals("AREA") && parNomZona.length() == 3) {
			ctrlTab = ctrlTabDAO.getNumRegTab("AREA");
			String codigoArea = parNomZona.substring(0, 3) + "-" + (obtenerFechaActual().getYear() - 100) + "-" + (obtenerFechaActual().getMonth() + 1);
			are.setAre_ide(funUsu.getCodTab(codigoArea + "-", ctrlTab.getCtrl_numreg(), 15));

		} else if (parTipZona.equals("SUBAREA") && parNomZona.length() > 3) {

			// ctrlTab = ctrlTabDAO.getNumRegTab("SUBAREA");
			String codigoSubArea = are.getAre_nom().substring(0, 3) + "-" + parNomZona.substring(0, 3) + "-" + (parNomZona.substring(parNomZona.length() - 1, parNomZona.length()));
			subAre.setSubare_ide(codigoSubArea);

		} else if (parTipZona.equals("FICHA")) {

			ctrlTab = ctrlTabDAO.getNumRegTab("FICHA");
			fic.setFic_ide(parNomZona);
			fic.setFic_bar(KeyGenerator.getInst().obtenerCodigoBarra());

		}

	}// Fin de generarNuevaNumeroDocumento()

	/*
	 * 
	 * 
	 * METODOS DE VALIDACION
	 */
	public boolean validarDatosPedido() {

		if (!ped.getPed_nomdoc().equals("")) {
			return true;
		}// Fin de if(!ped.getPed_nomdoc().equals(""))
		else {
			JOptionPane.showConfirmDialog(null,
					"���NO EXISTE NUMERO DE DOCUMENTO!!!"
							+ "\n" + this.getClass().getName(),
					"Sistema Control de Produccion",
					JOptionPane.PLAIN_MESSAGE,
					JOptionPane.INFORMATION_MESSAGE);

		}
		return false;
	}

	public boolean validarDatosDetPedido() {

		if (!ped.getPed_nomdoc().equals("")) {
			return true;
		}// Fin de if(!ped.getPed_nomdoc().equals(""))
		else {
			JOptionPane.showConfirmDialog(null,
					"���NO EXISTE NUMERO DE DOCUMENTO!!!"
							+ "\n" + this.getClass().getName(),
					"Sistema Control de Produccion",
					JOptionPane.PLAIN_MESSAGE,
					JOptionPane.INFORMATION_MESSAGE);

		}
		return false;
	}

	/*
	 * 
	 * 
	 * GENERAR QUERY EN LA BASE DE DATOS
	 */

	/*
	 * 
	 * 
	 * 
	 * 
	 * METODOS DE VALIDACION DE DATOS
	 */

	public boolean validarDatoCliente() {

		if (!cli.getCli_ide().equals("")) {
			if (!cli.getCli_nom().equals("")) {
				return true;
			} else {
				JOptionPane.showConfirmDialog(null,
						"Ingrese un nombre para el cliente",
						testProp.getPropiedad("TituloSistema"),
						JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);

			}
		} else {
			JOptionPane.showConfirmDialog(null,
					"Ingrese un Codigo de Identificacion para el cliente",
					testProp.getPropiedad("TituloSistema"),
					JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);

		}

		return false;
	}

	/*
	 * 
	 * 
	 * METODOS PARA LLENAR LISTAS
	 */
	/*
	 * 
	 * 
	 * 
	 * METODOS PARA ELIMINAR REGISTROS
	 */
	public boolean reiniciarClase(String parTipoClase) {

		ClaseDAO claDAO = (ClaseDAO) ContenedorTextil.getComponent("ClaseDAO");
		return claDAO.reiniciarClase(parTipoClase);
	}

	public boolean eliminarArea(int parAreCod) {

		AreaDAO areDAO = (AreaDAO) ContenedorTextil.getComponent("AreaDAO");
		return areDAO.eliminar(parAreCod);
	}

	public boolean eliminarSubArea(int parSubAreCod) {

		SubAreaDAO subAreDAO = (SubAreaDAO) ContenedorTextil.getComponent("SubAreaDAO");
		return subAreDAO.eliminar(parSubAreCod);
	}

	public int eliminarByBarra(String parFicbar, String estado, int subare_cod) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.eliminarByBarra(parFicbar, estado, subare_cod);
	}

	public int eliminarFichaPendiente(int parPedCod, String parFicBar) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.eliminarFichaPendiente(parPedCod, parFicBar);
	}

	public int eliminarFichaTerminadoPendienteProduccion(int parPedCod, String parFicBar) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.eliminarFichaTerminadoPendienteProduccion(parPedCod, parFicBar);
	}

	public boolean eliminarDetPedido(int parDetPedCod) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		return detPedDAO.eliminar(parDetPedCod);
	}
	public boolean eliminarListaDetPedido(int pedCod) {

		DetPedidoDAO detPedDAO = (DetPedidoDAO) ContenedorTextil.getComponent("DetPedidoDAO");
		return detPedDAO.eliminarLista(pedCod);
	}
	public int eliminarListaFicha(int pedCod) {

		FichaDAO ficDAO = (FichaDAO) ContenedorTextil.getComponent("FichaDAO");
		return ficDAO.eliminarLista(pedCod);
	}

	public boolean eliminarPedido(int parPedCod) {

		PedidoDAO pedDAO = (PedidoDAO) ContenedorTextil.getComponent("PedidoDAO");
		return pedDAO.eliminar(parPedCod);
	}

	public boolean eliminarProducto(int parPrdCod) {

		ProductoDAO prdDAO = (ProductoDAO) ContenedorTextil.getComponent("ProductoDAO");
		return prdDAO.eliminar(parPrdCod);
	}

	public boolean validarUsuario() {

		if (agru.getSegcodigo().equals("ADMIN")) {
			return true;
		} else {
			JOptionPane.showConfirmDialog(null,
					"���NO TIENE PERMISOS PARA ACCEDER A ESTE FORMULARIO!!!"
							+ "\n" + this.getClass().getName(),
					"Sistema Control de Produccion",
					JOptionPane.PLAIN_MESSAGE,
					JOptionPane.INFORMATION_MESSAGE);
		}
		return false;

		// JPasswordField pField = new JPasswordField(5);
		// JPanel pPanel = new JPanel();
		// pPanel.add(new JLabel("Please Enter Password: "));
		// pPanel.add(pField);
		//
		// if (claveValidado.equals("")) {
		// int result = JOptionPane.showConfirmDialog(null, pPanel, "Sistema Administracion", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		// if (result == JOptionPane.OK_OPTION) {
		// claveValidado = String.valueOf(pField.getPassword());
		// if (claveValidado == null) {
		// claveValidado = "";
		// }
		// if (clavePropiedad == null) {
		// clavePropiedad = "ADMIN";
		// }
		// if (claveValidado.toUpperCase().equals(clavePropiedad.toUpperCase())) {
		// return true;
		// } else {
		// claveValidado = "";
		// return false;
		// }
		//
		// } else {
		// return false;
		// }
		// }// Fin de if(!claveValidado.equals(""))
		// else {
		// return true;
		// }
	}// Fin de seleccionarDocumento()

	public void reproducirSonido(Object object) {

		Map requestParameters = (Map) object;
		String tipoSonido = (String) requestParameters.get("sonido");

        try 
        {
        	AudioClip ac;
			URL url = (new File(AplicacionGeneral.getInstance().obtenerRutaSonidos() + tipoSonido.toLowerCase() + ".wav")).toURI().toURL();
			if (url != null) {

				ac = Applet.newAudioClip(url);
				ac.play();

			} else {
				url = (new File(AplicacionGeneral.getInstance().obtenerRutaSonidos() + "defecto.wav")).toURI().toURL();
				ac = Applet.newAudioClip(url);
				ac.play();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void iniciarAccion(Object object) {

		Map incommingRequest = (Map) object;

		Object result = null;
		Class[] argTypes = new Class[] { Object.class };
		String accion = (String) incommingRequest.get("accion");
		try {

			Method method = this.getClass().getMethod(accion, argTypes);
			result = method.invoke(this, incommingRequest);
			incommingRequest = (Map) result;

		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void llenarTabla(Object object) {

		dlgCtrlPrd.llenarTabla();

	}

	public void ejecutarProceso() {

		if (requestParameters.containsKey("accion") && !requestParameters.get("accion").equals("")) {

			swnWork = new SwingWorker() {

				@Override
				public Object construct() {

					// setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

					// numProc = 1;
					// valPrgBar = 0;
					// totProc = 1;
					// iteProc = 0;
					//
					// progBarra.setEnabled(true);
					// progBarra.setVisible(true);
					// progBarra.setIndeterminate(true);
					// etiProgBarra.setVisible(true);
					// etiProgBarra.setText("Iniciando Proceso..");
					//
					// actualizarProceso("Procesando Marcado...");
					System.out.println("Iniciamos el proceso");
					abrirPantalla("DlgControlProceso", "FrmPrincipal", AccesoPagina.INICIAR);
					iniciarAccion(requestParameters);

					return "Finalizo el proceso";
				}

				@Override
				public void finished() {

					// activarFormulario(true);
					// setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					// progBarra.setEnabled(false);
					// progBarra.setVisible(false);
					// etiProgBarra.setVisible(false);
					// cmdRegistrar.requestFocus();
					System.out.println("Termino el proceso");
					dlgCtrlProc.cerrar();
				}
			};
			swnWork.start();

		}
	}
	public Map validarLibroExcel(Map rqst, JProgressBar barraProgreso)
	{
		try
		{
			String[] matColumnaExcel = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ"};
			Map msgBox = new LinkedHashMap();
			List lisBrwCol = new ArrayList();
			
			msgBox.put("type","success");
			msgBox.put("text","Inicio operacion!!!");
			rqst.put("msg",msgBox);
			rqst.put("exito",true);
			
			int numFil = (Integer) rqst.get("numFil");
			int numCol = (Integer) rqst.get("numCol");
			int rowIni = (Integer) rqst.get("rowIni");
			int colIni = (Integer) rqst.get("colIni");
			int maxRow = 1000;
			
			String value = "";
			
			Map wrkLisUni = new LinkedHashMap();
			
			String campo="", valor="";
			boolean llenarModelo = true;
			boolean llenarProducto = false;
			boolean llenarTalla = false;
			boolean llenarValores = false;
			
			boolean agrMod = true;
			boolean agrEsp = false;
			boolean agrDes = false;
			boolean agrGau = false;
			
			boolean agrPrd = false;
			boolean agrNom = false;
			boolean agrTal = false;
			
			String errorCampo = "";
			int colPrd = 0, colError = 0, filError = 0;
			int colNom = 0;
			int colTal = 0;
			
			lisModImp = new ArrayList();
			List lisPrdTmp = null;
			
			Map rstMod = new LinkedHashMap();
			Map rstPrd = null;
			Map rstTal = null;
			Map rstTalCol = null;
			Map rstTalColVal = null;
			barraProgreso.setIndeterminate(false);
			barraProgreso.setMaximum(numFil);
			for (int fil = rowIni; fil <= numFil; fil++)
			{
				if(!errorCampo.isEmpty() || campo.toUpperCase().contains("TOTAL")){break;}
				/**
				 * OBTENEMOS EL ENCABEZADO DEL PEDIDO
				 */
				if (llenarModelo == true)
				{
					for(int col = colIni; col <= numCol; col++)
					{
						campo = Util.getInst().getValorExcel(rqst, fil, col);
						if( agrMod == true && campo.toUpperCase().startsWith("MODELO") )
						{
							errorCampo = campo;colError = col;filError = fil;
							for(int iteVal = col + 1; iteVal <= numCol; iteVal++)
							{
								valor = Util.getInst().getValorExcel(rqst, fil, iteVal);
								if(!Util.getInst().isEmpty(valor) && !Util.getInst().validarPalabraClave(valor))
								{
									if(valor.substring(0, 1).compareTo("-")==0){valor = valor.substring(1,valor.length());}
									if(valor.substring(valor.length()-1, valor.length()).compareTo("-") == 0){valor = valor.substring(0,valor.length()-1);}
									rstMod.put("prd_ide", valor);
									agrMod = false;
									agrEsp = true;
									agrDes = false;
									agrGau = false;
									col = iteVal + 1;
									errorCampo = "";
									break;
								}//if(!isEmpty(talNom) && !isEmpty(talCan))
							}//for(int tal = 14; tal < 50; tal+=2)
						}//if( campo.toUpperCase().contains("MODELO") )
						campo = Util.getInst().getValorExcel(rqst, fil, col);
						if( agrEsp == true && campo.toUpperCase().startsWith("ESPECIFICACION") )
						{
							errorCampo = campo;colError = col;filError = fil;
							for(int iteVal = col + 1; iteVal <= numCol; iteVal++)
							{
								valor = Util.getInst().getValorExcel(rqst, fil, iteVal);
								if(!Util.getInst().isEmpty(valor) && !Util.getInst().validarPalabraClave(valor))
								{
									if(valor.substring(0, 1).compareTo("-")==0){valor = valor.substring(1,valor.length());}
									if(valor.substring(valor.length()-1, valor.length()).compareTo("-") == 0){valor = valor.substring(0,valor.length()-1);}

									rstMod.put("prd_mat", valor);
									agrMod = false;
									agrEsp = false;
									agrDes = true;
									agrGau = false;
									col = iteVal + 1;
									errorCampo = "";
									break;
								}//if(!isEmpty(talNom) && !isEmpty(talCan))
							}//for(int tal = 14; tal < 50; tal+=2)
						}
						campo = Util.getInst().getValorExcel(rqst, fil, col);
						if( agrDes == true && campo.toUpperCase().startsWith("DESCRIPCION") )
						{
							errorCampo = campo;colError = col;filError = fil;
							for(int iteVal = col + 1; iteVal <= numCol; iteVal++)
							{
								valor = Util.getInst().getValorExcel(rqst, fil, iteVal);
								if(!Util.getInst().isEmpty(valor) && !Util.getInst().validarPalabraClave(valor))
								{
									rstMod.put("prd_des", valor);
									rstMod.put("prd_tip", valor.replace("-", " ").trim().replace(((String)rstMod.get("prd_ide")).replace("-", " "), "").trim());
									
									agrMod = false;
									agrEsp = false;
									agrDes = false;
									agrGau = true;
									col = iteVal + 1;
									errorCampo = "";
									break;
								}//if(!isEmpty(talNom) && !isEmpty(talCan))
							}//for(int tal = 14; tal < 50; tal+=2)
						}
						campo = Util.getInst().getValorExcel(rqst, fil, col);
						if( agrGau == true && campo.toUpperCase().startsWith("GAUGE") )
						{
							errorCampo = campo;colError = col;filError = fil;
							for(int iteVal = col + 1; iteVal <= numCol; iteVal++)								
							{
								valor = Util.getInst().getValorExcel(rqst, fil, iteVal);
								if(!Util.getInst().isEmpty(valor) && !Util.getInst().validarPalabraClave(valor))
								{
									rstMod.put("prd_gau", valor);
									agrMod = false;
									agrEsp = false;
									agrDes = false;
									agrGau = false;
									
									agrPrd = true;
									agrNom = false;
									agrTal = false;
									
									errorCampo = "";
									
									col = numCol;
									fil++;
									
									llenarModelo = false;
									llenarProducto = true;
									llenarTalla = false;
									llenarValores = false;							
									
									break;
								}//if(!isEmpty(talNom) && !isEmpty(talCan))
							}//for(int tal = 14; tal < 50; tal+=2)
						}
					}//for($col = 1; $col < 24; $col++)					
				}//if (llenarModelo == true)
				
				/**
				 * OBTENEMOS EL ENCABEZADO Producto
				 */
				if (llenarProducto == true)
				{
					for(int col = colIni; col <= numCol; col++)
					{
						campo = Util.getInst().getValorExcel(rqst, fil, col);
						if( agrPrd == true && campo.toUpperCase().startsWith("ARTICULO") )
						{
							agrPrd = false;
							agrNom = true;
							agrTal = false;
							colPrd = col;
							col++;
						}//if( campo.toUpperCase().contains("MODELO") )
						campo = Util.getInst().getValorExcel(rqst, fil, col);
						if( agrNom == true && campo.toUpperCase().startsWith("DESC") )
						{
							agrPrd = false;
							agrNom = false;
							agrTal = true;
							colNom = col;
							col++;
						}//if( campo.toUpperCase().contains("MODELO") )
						/** ********************************
						 * OBTENEMOS EL DETALLE DEL PEDIDO *
						 ***********************************/						
						campo = Util.getInst().getValorExcel(rqst, fil, col);
						if( agrTal == true && campo.toUpperCase().startsWith("TALLA") )
						{
							agrPrd = false;
							agrNom = false;
							agrTal = false;
							colTal = col;
							col = numCol;
							
							llenarModelo = false;
							llenarProducto = false;
							llenarTalla = true;
							llenarValores = false;
							break;
						}//if( campo.toUpperCase().contains("MODELO") )
					}//for(int col = colIni; col < (numCol+1); col++)					
				}//if (llenarDetalle == true)		
				/** *******************************************************************
				 * OBTENEMOS LAS TALLAS Y LAS COLUMNAS DONDE OBTENEMOS LAS CANTIDADES *
				 **********************************************************************/
				if (llenarTalla == true)
				{
					rstTalCol = new LinkedHashMap();
					rstTalColVal = new LinkedHashMap();
					for(int col = (colTal+1); col <= numCol; col++)
					{
						valor = Util.getInst().getValorExcel(rqst, fil, col);
						if( agrPrd == false && agrNom == false && agrTal == false && !Util.getInst().isEmpty(valor) )
						{
							String valorTemporal = valor.trim().replace(".", "").toUpperCase();
							if(valorTemporal.compareTo("CANT")!=0)
							{
								rstTalCol.put(valor, col);
								rstTalColVal.put(valor, 0);
							}
						}//if( campo.toUpperCase().contains("MODELO") )
					}//for(int col = colIni; col < (numCol+1); col++)
					fil++;
					lisPrdTmp = new ArrayList();
					
					llenarModelo = false;
					llenarProducto = false;
					llenarTalla = false;
					llenarValores = true;
				}//if (llenarTalla == true)
				/** *******************************************************************
				 * OBTENEMOS LOS VALORES DE LOS PRODUCTOS Y CANTIDADES DE LAS TALLAS  *
				 **********************************************************************/
				if (llenarValores == true)
				{
					valor = Util.getInst().getValorExcel(rqst, fil, colPrd);
					if(!Util.getInst().isEmpty(valor) && !Util.getInst().validarPalabraClave(valor))
					{
						rstPrd = new LinkedHashMap();
						rstPrd.put("prd_ide",Util.getInst().getValorExcel(rqst, fil, colPrd));
						rstPrd.put("prd_des",Util.getInst().getValorExcel(rqst, fil, colNom));
						
						String prdIdeCodCol = (String) rstPrd.get("prd_ide");
						prdIdeCodCol = prdIdeCodCol.substring(prdIdeCodCol.lastIndexOf("-")+1, prdIdeCodCol.length());
						
						String valorDescripcion = Util.getInst().getValorExcel(rqst, fil, colNom);
						int posUltima = (valorDescripcion).trim().replace("-", " ").lastIndexOf(" ");
						rstPrd.put("prd_mat", valorDescripcion.substring(0, posUltima));
						rstPrd.put("prd_col", prdIdeCodCol + "-" + valorDescripcion.substring(posUltima + 1, valorDescripcion.length()));
						
						if(!rstMod.containsKey("prd_fil_enc"))
						{
							rstMod.put("prd_fil_enc", fil - 1);
						}
					
						rstTal = new LinkedHashMap();
						Iterator iteTalCol = rstTalCol.entrySet().iterator();
						while (iteTalCol.hasNext()) {
							Map.Entry ent = (Map.Entry) iteTalCol.next();
							int colTalVal = (Integer) ent.getValue();
							String valorTalla = Util.getInst().getValorExcel(rqst, fil, colTalVal);
							if(!valorTalla.isEmpty() && Util.getInst().isInteger(valorTalla) && Integer.parseInt(valorTalla) > 0)
							{
								rstTal.put(ent.getKey(), valorTalla);
								int sumCan = ((Integer) rstTalColVal.get(ent.getKey())) + ( (valorTalla.isEmpty()) ? 0 : Integer.parseInt(valorTalla));
								rstTalColVal.put(ent.getKey(), sumCan);								
							}
						}
						
						rstPrd.put("listaTalla", rstTal);
						lisPrdTmp.add(rstPrd);
					}//if(isEmpty(valor) || validarPalabraClave(valor))
					else
					{
						Iterator iteColVal = rstTalColVal.entrySet().iterator();
						while (iteColVal.hasNext()) {
							Map.Entry entTalVal = (Map.Entry) iteColVal.next();
							int colTalVal = (Integer) rstTalCol.get(entTalVal.getKey());
							int filPrdEnc = (Integer) rstMod.get("prd_fil_enc");
							errorCampo = "";
							if(((Integer) entTalVal.getValue()) == 0)
							{
								errorCampo = "MODELO [ " + (String) rstMod.get("prd_ide") + " ] - " + " TALLA [ " + (String) entTalVal.getKey() + " ]";colError = colTalVal;filError = filPrdEnc;
								break;
							}//if(((Integer) entTalVal.getValue()) == 0)
						}//while (ite.hasNext()) {
						
						rstMod.put("listaProducto", lisPrdTmp);
						lisModImp.add(rstMod);
						rstMod = new LinkedHashMap();
						
						agrMod = true;
						agrEsp = false;
						agrDes = false;
						agrGau = false;
						
						llenarModelo = true;
						llenarProducto = false;
						llenarTalla = false;
						llenarValores = false;
						fil = fil - 1;
					}//if(isEmpty(valor) || valor.equals("MODELO"))
				}//if (llenarTalla == true)
				barraProgreso.setValue(fil);
			}//for (fil = 6; fil <= $numFil; fil++)
			
			/**
			 * IMPRIMIR VALORES ENCONTRADOS
			 */
/*			System.out.println("ENCABEZADO");
			System.out.println("==========");
			for(int iteModTmp = 0; iteModTmp < lisModImp.size(); iteModTmp++)
			{
				Map rstModTmp = (Map) lisModImp.get(iteModTmp);
				Iterator iteMod = rstModTmp.entrySet().iterator();
				while (iteMod.hasNext()) {
					Map.Entry entMod = (Map.Entry) iteMod.next();
					if(entMod.getKey().equals("listaProducto"))
					{
						List lisTmp = (List) entMod.getValue();
						for(int iteTmp = 0; iteTmp < lisTmp.size(); iteTmp++)
						{
							System.out.print("    ");
							Map rstPrdTmp = (Map) lisTmp.get(iteTmp);
							Iterator itePrd = rstPrdTmp.entrySet().iterator();
							while (itePrd.hasNext()) {
								Map.Entry entPrd = (Map.Entry) itePrd.next();
								System.out.print("\t"+entPrd.getValue());
							}
							System.out.println("");
						}
					}
					else
					{
						System.out.println(entMod.getKey() + " : " + entMod.getValue());
					}
				}//while (iteMod.hasNext()) {				
			}//for(int iteModTmp = 0; iteModTmp < lisMod.size(); iteModTmp++)
*/			
			rqst.put("errorCampo", errorCampo);
			rqst.put("errorCelda", (filError == 0) ? "[ " + matColumnaExcel[colError] + " ]" : "[ " + matColumnaExcel[colError] + " : " + (filError + 1) + " ]");
			rqst.put("lisModImp", lisModImp);
			rqst.put("numFil", numFil);
			rqst.put("msg", msgBox);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return rqst;
	}//public Map validarLibroExcel(Map rqst)
	
	public void generarIdentificacionModelo(String parIde) 
	{
		boolean existe = true;
		String codigo = "";
		while (existe) {
			codigo = KeyGenerator.getInst().obtenerCodigoBarra();
			if (!existeCodigoBarra("PRODUCTO", codigo)) {
				mod.setPrd_ide(parIde);
				mod.setPrd_bar(codigo);
				existe = false;
			}
		}
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)

	public void generarIdentificacionProducto(String parIde) 
	{
		boolean existe = true;
		String codigo = "";
		while (existe) {
			codigo = KeyGenerator.getInst().obtenerCodigoBarra();
			if (!existeCodigoBarra("PRODUCTO", codigo)) {
				prd.setPrd_ide(parIde);
				prd.setPrd_bar(codigo);
				existe = false;
			}
		}
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)

	public void generarIdentificacionTalla(String parIde) {

		boolean existe = true;
		String codigo = "";
		while (existe) {
			codigo = KeyGenerator.getInst().obtenerCodigoBarra();
			if (!existeCodigoBarra("PRODUCTO", codigo)) {
				tal.setPrd_ide(parIde);
				tal.setPrd_bar(codigo);
				existe = false;
			}
		}
	}// Fin de public void generarIdentificacionProducto(int ano, int temp, String mod, String cli, String col)	
}// Fin de Clase principal


