package com.textil.controlador;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.comun.entidad.Agrupacion;
import com.comun.entidad.Area;
import com.comun.entidad.Clase;
import com.comun.entidad.Cliente;
import com.comun.entidad.CtrlTabla;
import com.comun.entidad.DetPedido;
import com.comun.entidad.Ficha;
import com.comun.entidad.Grupo;
import com.comun.entidad.Organizacion;
import com.comun.entidad.Pedido;
import com.comun.entidad.PerfilSeguridad;
import com.comun.entidad.Producto;
import com.comun.entidad.Seguridad;
import com.comun.entidad.SubArea;
import com.comun.entidad.Usuario;
import com.comun.utilidad.CodigoBarras;
import com.comun.utilidad.EvaluarExtensionArchivo;
import com.textil.dao.AgrupacionDAO;
import com.textil.dao.AreaDAO;
import com.textil.dao.ClaseDAO;
import com.textil.dao.ClienteDAO;
import com.textil.dao.CtrlTablaDAO;
import com.textil.dao.DetPedidoDAO;
import com.textil.dao.FichaDAO;
import com.textil.dao.GrupoDAO;
import com.textil.dao.OrganizacionDAO;
import com.textil.dao.PedidoDAO;
import com.textil.dao.PerfilSeguridadDAO;
import com.textil.dao.ProductoDAO;
import com.textil.dao.SeguridadDAO;
import com.textil.dao.SubAreaDAO;
import com.textil.dao.TransactionDelegate;
import com.textil.dao.TransactionFactory;
import com.textil.dao.UsuarioDAO;
import com.textil.main.DlgAcceso;
import com.textil.main.DlgArea;
import com.textil.main.DlgCodigoProducto;
import com.textil.main.DlgControlPedido;
import com.textil.main.DlgControlProceso;
import com.textil.main.DlgControlProducto;
import com.textil.main.DlgFicha;
import com.textil.main.DlgPedido;
import com.textil.main.DlgPedidoImportacion;
import com.textil.main.DlgProducto;
import com.textil.main.FrmPrincipal;
import com.textil.main.FuncionesUsuario;
import com.textil.main.TestPropiedades;

public class ContenedorTextil {

	static HashMap<String, Object> componentes = initMap();

	private static HashMap<String, Object> initMap() {

		componentes = new HashMap<String, Object>();

		// ---------------------------------------------
		// -
		// = : POR REFERENCIA -
		// set : POR VALOR -
		// CONFIGURACION DE LAS PROPIEDADES -
		// -
		// -
		// -
		// ---------------------------------------------

		TestPropiedades testProp = new TestPropiedades();
		componentes.put("TestPropiedades", testProp);

		TransactionFactory transactionFactory = new TransactionFactory();
		componentes.put("TransactionFactory", transactionFactory);

		TransactionDelegate transactionDelegate = new TransactionDelegate(transactionFactory);
		componentes.put("TransactionDelegate", transactionDelegate);
		// ---------------------------------------------
		// -
		// -
		// -
		// CONEXION A LA BASE DE DATOS -
		// -
		// -
		// -
		// ---------------------------------------------

		EvaluarExtensionArchivo evaluarExtensionArchivo = new EvaluarExtensionArchivo();
		FuncionesUsuario funcionesUsuario = new FuncionesUsuario();
		funcionesUsuario.setEvaluarArchivo(evaluarExtensionArchivo);
		componentes.put("FuncionesUsuario", funcionesUsuario);
		// ---------------------------------------------
		// -
		// -
		// -
		// MODELO ORM -
		// -
		// -
		// -
		// ---------------------------------------------

		Area area = new Area();
		componentes.put("Area", area);

		AreaDAO areaDAO = new AreaDAO(area);
		componentes.put("AreaDAO", areaDAO);

		List<Area> listaArea = new ArrayList<Area>();
		componentes.put("ListaArea", listaArea);

		SubArea subArea = new SubArea();
		componentes.put("SubArea", subArea);

		SubAreaDAO subAreaDAO = new SubAreaDAO(subArea);
		componentes.put("SubAreaDAO", subAreaDAO);

		List<SubArea> listaSubArea = new ArrayList<SubArea>();
		componentes.put("ListaSubArea", listaSubArea);

		Pedido pedido = new Pedido();
		componentes.put("Pedido", pedido);

		PedidoDAO pedidoDAO = new PedidoDAO(pedido);
		componentes.put("PedidoDAO", pedidoDAO);

		DetPedido detPedido = new DetPedido();
		componentes.put("DetPedido", detPedido);

		DetPedido detPedidoTemporal = new DetPedido();
		componentes.put("DetPedidoTemporal", detPedidoTemporal);

		DetPedidoDAO detPedidoDAO = new DetPedidoDAO(detPedidoTemporal);
		componentes.put("DetPedidoDAO", detPedidoDAO);

		List<DetPedido> listaDetPedido = new ArrayList<DetPedido>();
		componentes.put("ListaDetPedido", listaDetPedido);

		Producto productoPadre = new Producto();
		componentes.put("ProductoPadre", productoPadre);

		Producto productoGeneral = new Producto();
		componentes.put("ProductoGeneral", productoGeneral);

		Producto modelo = new Producto();
		componentes.put("Modelo", modelo);

		Producto producto = new Producto();
		componentes.put("Producto", producto);

		Producto talla = new Producto();
		componentes.put("Talla", talla);

		ProductoDAO productoDAO = new ProductoDAO(productoGeneral);
		componentes.put("ProductoDAO", productoDAO);

		List<Producto> listaModelo = new ArrayList<Producto>();
		componentes.put("ListaModelo", listaModelo);

		List<Producto> listaProducto = new ArrayList<Producto>();
		componentes.put("ListaProducto", listaProducto);

		List<Producto> listaTalla = new ArrayList<Producto>();
		componentes.put("ListaTalla", listaTalla);

		List<Producto> listaProductoGeneral = new ArrayList<Producto>();
		componentes.put("ListaProductoGeneral", listaProductoGeneral);

		Cliente cliente = new Cliente();
		componentes.put("Cliente", cliente);

		ClienteDAO clienteDAO = new ClienteDAO(cliente);
		componentes.put("ClienteDAO", clienteDAO);

		List<Cliente> listaCliente = new ArrayList<Cliente>();
		componentes.put("ListaCliente", listaCliente);

		CtrlTabla ctrlTabla = new CtrlTabla();
		componentes.put("CtrlTabla", ctrlTabla);

		CtrlTablaDAO ctrlTabDAO = new CtrlTablaDAO(ctrlTabla);
		componentes.put("CtrlTablaDAO", ctrlTabDAO);

		List<CtrlTabla> listaCtrlTabla = new ArrayList<CtrlTabla>();
		componentes.put("ListaCtrlTabla", listaCtrlTabla);

		Grupo grupo = new Grupo();
		componentes.put("Grupo", grupo);

		Grupo grupoColor = new Grupo();
		componentes.put("GrupoColor", grupoColor);

		Grupo grupoModelo = new Grupo();
		componentes.put("GrupoModelo", grupoModelo);

		Grupo grupoProducto = new Grupo();
		componentes.put("GrupoProducto", grupoProducto);

		Grupo grupoTalla = new Grupo();
		componentes.put("GrupoTalla", grupoTalla);

		GrupoDAO grupoDAO = new GrupoDAO(grupo);
		componentes.put("GrupoDAO", grupoDAO);

		List<Grupo> listaGrupo = new ArrayList<Grupo>();
		componentes.put("ListaGrupo", listaGrupo);

		Clase clase = new Clase();
		componentes.put("Clase", clase);

		Clase claseColor = new Clase();
		componentes.put("ClaseColor", claseColor);

		ClaseDAO claseDAO = new ClaseDAO(clase);
		componentes.put("ClaseDAO", claseDAO);

		List<Clase> listaClase = new ArrayList<Clase>();
		componentes.put("ListaClase", listaClase);

		Ficha ficha = new Ficha();
		componentes.put("Ficha", ficha);

		FichaDAO fichaDAO = new FichaDAO(ficha);
		componentes.put("FichaDAO", fichaDAO);

		List<Ficha> listaFicha = new ArrayList<Ficha>();
		componentes.put("ListaFicha", listaFicha);

		Usuario usuarioSesion = new Usuario();
		componentes.put("UsuarioSesion", usuarioSesion);

		UsuarioDAO usuarioSesionDAO = new UsuarioDAO(usuarioSesion);
		componentes.put("UsuarioSesionDAO", usuarioSesionDAO);

		Usuario usuario = new Usuario();
		componentes.put("Usuario", usuario);

		UsuarioDAO usuarioDAO = new UsuarioDAO(usuario);
		componentes.put("UsuarioDAO", usuarioDAO);

		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		componentes.put("ListaUsuario", listaUsuario);

		Organizacion organizacion = new Organizacion();
		componentes.put("Organizacion", organizacion);

		OrganizacionDAO organizacionDAO = new OrganizacionDAO(organizacion);
		componentes.put("OrganizacionDAO", organizacionDAO);

		List<Organizacion> listaOrganizacion = new ArrayList<Organizacion>();
		componentes.put("ListaOrganizacion", listaOrganizacion);

		Seguridad seguridad = new Seguridad();
		componentes.put("Seguridad", seguridad);

		SeguridadDAO seguridadDAO = new SeguridadDAO(seguridad);
		componentes.put("SeguridadDAO", seguridadDAO);

		List<Seguridad> listaSeguridad = new ArrayList<Seguridad>();
		componentes.put("ListaSeguridad", listaSeguridad);

		PerfilSeguridad perfilSeguridad = new PerfilSeguridad();
		componentes.put("PerfilSeguridad", perfilSeguridad);

		PerfilSeguridadDAO perfilSeguridadDAO = new PerfilSeguridadDAO(perfilSeguridad);
		componentes.put("PerfilSeguridadDAO", perfilSeguridadDAO);

		List<PerfilSeguridad> listaPerfilSeguridad = new ArrayList<PerfilSeguridad>();
		componentes.put("ListaPerfilSeguridad", listaPerfilSeguridad);

		Agrupacion agrupacion = new Agrupacion();
		componentes.put("Agrupacion", agrupacion);

		AgrupacionDAO agrupacionDAO = new AgrupacionDAO(agrupacion);
		componentes.put("AgrupacionDAO", agrupacionDAO);

		// ---------------------------------------------
		// -
		// -
		// -
		// INSTANCIA PARA CONSTRUIR LA RED BAYESIANA -
		// -
		// -
		// -
		// ---------------------------------------------
		FrmPrincipal frmPrincipal = new FrmPrincipal();
		componentes.put("FrmPrincipal", frmPrincipal);

		DlgProducto dlgProducto = new DlgProducto(frmPrincipal);
		componentes.put("DlgProducto", dlgProducto);

		DlgPedido dlgPedido = new DlgPedido(frmPrincipal);
		componentes.put("DlgPedido", dlgPedido);

		DlgControlPedido dlgControlPedido = new DlgControlPedido(frmPrincipal);
		componentes.put("DlgControlPedido", dlgControlPedido);

		DlgControlProducto dlgControlProducto = new DlgControlProducto(frmPrincipal);
		componentes.put("DlgControlProducto", dlgControlProducto);

		DlgCodigoProducto dlgCodigoProducto = new DlgCodigoProducto(frmPrincipal);
		componentes.put("DlgCodigoProducto", dlgCodigoProducto);

		DlgArea dlgArea = new DlgArea(frmPrincipal);
		componentes.put("DlgArea", dlgArea);

		DlgFicha dlgFicha = new DlgFicha(frmPrincipal);
		componentes.put("DlgFicha", dlgFicha);

		DlgAcceso dlgAcceso = new DlgAcceso(frmPrincipal);
		componentes.put("DlgAcceso", dlgAcceso);

		DlgControlProceso dlgControlProceso = new DlgControlProceso(frmPrincipal);
		componentes.put("DlgControlProceso", dlgControlProceso);

		DlgPedidoImportacion dlgPedidoImportacion = new DlgPedidoImportacion(frmPrincipal);
		componentes.put("DlgPedidoImportacion", dlgPedidoImportacion);

		// ---------------------------------------------
		// -
		// -
		// -
		// VARIABLES PARA LA MANIPULACION DEL SISTEMA-
		// -
		// -
		// -
		// ---------------------------------------------

		CodigoBarras codigoBarras = new CodigoBarras();
		componentes.put("CodigoBarras", codigoBarras);

		Date fechaActual = new Date(2011, 1, 1);
		componentes.put("FechaActual", fechaActual);

		Time horaActual = new Time(0, 0, 0);
		componentes.put("HoraActual", horaActual);

		return componentes;

	}

	public static Object getComponent(String parCompName) {

		return componentes.get(parCompName);

	}// Fin de getComponent()
}// Fin de clase principal
