package com.textil.controlador;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.AplicacionGeneral;

/*
 * @author Paolo
 */
public class ManejadorTextil extends AuxiliarTextil {

	public ManejadorTextil() {
		// manUsu = new ManejadorUsuario(this);
		// manPed = new ManejadorPedido(this);
		// manCob = new ManejadorCobranza(this);
		// manCom = new ManejadorComprobante(this);
	}// Fin de constructor de la clase

	private static ServerSocket SERVER_SOCKET;

	public void ejecutarModulo(String orgIde)
	{
		OrganizacionGeneral.getInstance(orgIde);
		abrirPantalla("FrmPrincipal", "FrmPrincipal", AccesoPagina.INICIAR);
	}	
	public static void main(final String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() 
			{
				try 
				{
					OrganizacionGeneral.getInstance(args[0]);
				} 
				catch (ArrayIndexOutOfBoundsException ex)
				{
					OrganizacionGeneral.getInstance("SISEM");
				}

				try {
					
					//Util.getInst().ejecutarArchivoDesktop("cmd.exe /K start "+AplicacionGeneral.getInstance().obtenerRutaSistema()+"bin\\ejecutarTimerBackup.bat");
					
					String socket = AplicacionGeneral.getInstance().obtenerConfig("server-socket", "50000");
					SERVER_SOCKET = new ServerSocket(Integer.parseInt(socket));

					UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");

					ManejadorTextil manPadre = new ManejadorTextil();
					// manPadre.reproducirSonido();
					manPadre.abrirPantalla("DlgAcceso", "FrmPrincipal", AccesoPagina.INICIAR);

				} 
				catch (ArrayIndexOutOfBoundsException ex){
					Logger.getLogger(ManejadorTextil.class.getName()).log(Level.SEVERE, null, ex);
				}
				catch (IOException e) {

					JOptionPane.showConfirmDialog(null, "Cierre la aplicacion anterior" + "\n" + this.getClass().getName(), "Control de Acceso", JOptionPane.PLAIN_MESSAGE,
							JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);

				}
				catch (ClassNotFoundException ex) {
					Logger.getLogger(ManejadorTextil.class.getName()).log(Level.SEVERE, null, ex);
				} catch (InstantiationException ex) {
					Logger.getLogger(ManejadorTextil.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IllegalAccessException ex) {
					Logger.getLogger(ManejadorTextil.class.getName()).log(Level.SEVERE, null, ex);
				} catch (UnsupportedLookAndFeelException ex) {
					Logger.getLogger(ManejadorTextil.class.getName()).log(Level.SEVERE, null, ex);
				}

			}// Fin de metodo run()
		});
	}// Fin de metodo main
}// Fin de clase principal Manejador Padre
