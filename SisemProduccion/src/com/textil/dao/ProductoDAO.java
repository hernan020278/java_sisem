package com.textil.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Producto;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.DriverJdbc;
import com.comun.utilidad.Util;

public class ProductoDAO {

	private Producto prdTmp;
	private ImageIcon art_foto;
	private FileInputStream fis = null;

	public ProductoDAO(Producto parPrd)
	{
		this.prdTmp = parPrd;
	}// Fin de metodo constructor

	public int agregar(Producto parPrd) {

		String conSql;
		int resCod = 0;
		PreparedStatement pstm;
		ResultSet resSql;

		try {

			if (parPrd.getFilPrd_foto() != null) {
				if (DBConfiguracion.getInstance().getDriverJdbc().equals(DriverJdbc.MYSQL))
				{
					conSql = "insert into producto values(0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				}
				else
				{
					conSql = "insert into producto values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				}
			} else {

				conSql = "insert into producto(gru_cod,prd_ide,prd_tip,prd_mat,prd_gro,prd_gau,prd_col,prd_des,prd_par," +
						"prd_stkact,prd_supuno,prd_supdos,prd_suptres,prd_supcua,prd_nivel,prd_bar,prd_ver) "
						+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			}

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql, Statement.RETURN_GENERATED_KEYS);

			pstm.setInt(1, parPrd.getGru_cod());
			pstm.setString(2, parPrd.getPrd_ide());
			pstm.setString(3, parPrd.getPrd_tip());
			pstm.setString(4, parPrd.getPrd_mat());
			pstm.setString(5, parPrd.getPrd_gro());
			pstm.setString(6, parPrd.getPrd_gau());
			pstm.setString(7, parPrd.getPrd_col());
			pstm.setString(8, parPrd.getPrd_des());
			pstm.setString(9, parPrd.getPrd_par());
			pstm.setInt(10, parPrd.getPrd_stkact());
			pstm.setInt(11, parPrd.getPrd_supuno());
			pstm.setInt(12, parPrd.getPrd_supdos());
			pstm.setInt(13, parPrd.getPrd_suptres());
			pstm.setInt(14, parPrd.getPrd_supcua());
			pstm.setInt(15, parPrd.getPrd_nivel());
			pstm.setString(16, parPrd.getPrd_bar());

			if (parPrd.getFilPrd_foto() != null) {
				pstm.setBinaryStream(17, obtenerFileInputStream(parPrd.getFilPrd_foto()), (int) parPrd.getFilPrd_foto().length());
				pstm.setInt(18, parPrd.getPrd_ver());
			} else {
				pstm.setInt(17, parPrd.getPrd_ver());
			}

			if (pstm.executeUpdate() > 0) {
				resSql = pstm.getGeneratedKeys();
				while (resSql.next()) {

					resCod = resSql.getInt(1);

				}
				resSql.close();
			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return resCod;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de agregar()

	public FileInputStream obtenerFileInputStream(File parFile) {
		try {
			fis = new FileInputStream(parFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fis;
	}

	public int actualizar(Producto parPrd) {

		String conSql;
		int resCod = 0;
		PreparedStatement pstm;

		try {

			if (parPrd.getFilPrd_foto() != null) {
				conSql = "update producto set gru_cod=?,prd_ide=?,prd_tip=?,prd_mat=?,prd_gro=?,prd_gau=?,prd_col=?," +
						"prd_des=?,prd_par=?,prd_stkact=?,prd_supuno=?,prd_supdos=?,prd_suptres=?,prd_supcua=?,prd_nivel=?,prd_bar=?," +
						"prd_foto=?,prd_ver=? " +
						"where prd_cod=? and prd_ver=?";
			} else {
				conSql = "update producto set gru_cod=?,prd_ide=?,prd_tip=?,prd_mat=?,prd_gro=?,prd_gau=?,prd_col=?," +
						"prd_des=?,prd_par=?,prd_stkact=?,prd_supuno=?,prd_supdos=?,prd_suptres=?,prd_supcua=?,prd_nivel=?,prd_bar=?," +
						"prd_ver=? " +
						"where prd_cod=? and prd_ver=?";
			}

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			pstm.setInt(1, parPrd.getGru_cod());
			pstm.setString(2, parPrd.getPrd_ide());
			pstm.setString(3, parPrd.getPrd_tip());
			pstm.setString(4, parPrd.getPrd_mat());
			pstm.setString(5, parPrd.getPrd_gro());
			pstm.setString(6, parPrd.getPrd_gau());
			pstm.setString(7, parPrd.getPrd_col());
			pstm.setString(8, parPrd.getPrd_des());
			pstm.setString(9, parPrd.getPrd_par());
			pstm.setInt(10, parPrd.getPrd_stkact());
			pstm.setInt(11, parPrd.getPrd_supuno());
			pstm.setInt(12, parPrd.getPrd_supdos());
			pstm.setInt(13, parPrd.getPrd_suptres());
			pstm.setInt(14, parPrd.getPrd_supcua());
			pstm.setInt(15, parPrd.getPrd_nivel());
			pstm.setString(16, parPrd.getPrd_bar());

			if (parPrd.getFilPrd_foto() != null) {

				pstm.setBinaryStream(17, obtenerFileInputStream(parPrd.getFilPrd_foto()), (int) parPrd.getFilPrd_foto().length());
				pstm.setInt(18, parPrd.getPrd_ver() + 1);
				pstm.setInt(19, parPrd.getPrd_cod());
				pstm.setInt(20, parPrd.getPrd_ver());

			} else {

				pstm.setInt(17, parPrd.getPrd_ver());
				pstm.setInt(18, parPrd.getPrd_cod());
				pstm.setInt(19, parPrd.getPrd_ver());

			}

			if (pstm.executeUpdate() == 1) {

				resCod = parPrd.getPrd_cod();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return resCod;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de actualizar()

	public boolean eliminar(int parPrdCod) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from producto where prd_cod=" + parPrdCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			int afectadas = pstm.executeUpdate();
			pstm.close();

			return (afectadas == 1) ? true : false;
		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo eliminar contacto()", this.getClass().getName());
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar ORContacto

	public boolean existeProducto(int parPrdCod, String parPrdIde) {

		String conSql;
		ResultSet resSql;
		try {

			if (parPrdCod == 0) {

				conSql = "select * from producto where prd_ide = '" + parPrdIde + "'";

			}// Fin de if(parVenCod.equals(""))
			else {

				conSql = "select * from producto where prd_cod <> " + parPrdCod + " and prd_ide = '" + parPrdIde + "'";

			}

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql).executeQuery();

			if (resSql.next()) {

				return true;

			}

			return false;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error

	}// Fin de metodo para leer un registro de producto

	public boolean existeCodigoBarra(String parSql) {

		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parSql).executeQuery();

			if (resSql.next()) {

				return true;

			}

			return false;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error

	}// Fin de metodo para leer un registro de producto

	public List<String> obtenerListaProductoPedido(int parPrdNivSup) {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from view "
					+ "where producto.prd_nivel=" + parPrdNivSup;

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<String> lisDetPedPrd = new ArrayList<String>();

			while (resSql.next()) {

				lisDetPedPrd.add(resSql.getInt("detped_cod") + "#" +
						resSql.getInt("ped_cod") + "#" +
						resSql.getInt("prd_cod") + "#" +
						resSql.getInt("gru_cod") + "#" +
						resSql.getInt("gru_nivel") + "#" +
						resSql.getString("prd_ide") + "#" +
						resSql.getString("prd_des"));

			}// Fin de

			resSql.close();

			return lisDetPedPrd;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<Producto> obtenerListaProducto(int parPrdNiv, int parPrdCod, String parPrdIde) {

		String conSql;

		conSql = "select * from viewGrupoProducto";

		if (parPrdNiv == 1) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 2 && parPrdCod > 0) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv + " and prd_supuno=" + parPrdCod;
		} else if (parPrdNiv == 3 && parPrdCod > 0) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv + " and prd_supdos=" + parPrdCod;
		} else if (parPrdNiv == 4 && parPrdCod > 0) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv + " and prd_suptres=" + parPrdCod;
		} else if (parPrdNiv == 5 && parPrdCod > 0) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv + " and prd_supcua=" + parPrdCod;
		} else if (parPrdNiv == 2) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 3) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 4) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 5) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		}

		if (!parPrdIde.equals("*")) {
			conSql = conSql + " and (prd_ide like '" + parPrdIde + "%' or prd_ide like '%" + parPrdIde + "%')";
		}

		if (parPrdNiv == 3) {
			conSql = conSql + " order by prd_des";
		} else {
			conSql = conSql + " order by prd_ide";
		}
		return obtenerLista(conSql);

	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<String> obtenerGrupoProducto(int parPrdNiv, String parPrdIde) {

		String conSql;
		ResultSet resSql;

		conSql = "select prd_des from viewGrupoProducto";

		if (parPrdNiv == 1) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 2) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 3) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 4) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 5) {
			conSql = conSql + " where prd_nivel=" + parPrdNiv;
		}

		if (!parPrdIde.equals("*")) {
			conSql = conSql + " and (prd_des like '" + parPrdIde + "%' or prd_des like '%" + parPrdIde + "%')";
		}

		conSql = conSql + " group by prd_des";

		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<String> lisGruPrd = new ArrayList<String>();

			while (resSql.next()) {

				lisGruPrd.add(resSql.getString("prd_des"));

			}// Fin de

			return lisGruPrd;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<Producto> obtenerListaProductoPedido(int parPedCod, int parPrdNiv, int parPrdCod) {

		String conSql;
		conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod;

		if (parPrdNiv == 1) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 2 && parPrdCod > 0) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv + " and prd_supuno=" + parPrdCod;
		} else if (parPrdNiv == 3 && parPrdCod > 0) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv + " and prd_supdos=" + parPrdCod;
		} else if (parPrdNiv == 4 && parPrdCod > 0) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv + " and prd_suptres=" + parPrdCod;
		} else if (parPrdNiv == 5 && parPrdCod > 0) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv + " and prd_supcua=" + parPrdCod;
		} else if (parPrdNiv == 2 && parPrdCod == 0) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 3 && parPrdCod == 0) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 4 && parPrdCod == 0) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 5 && parPrdCod == 0) {
			conSql = conSql + " and prd_nivel=" + parPrdNiv;
		}
		conSql = conSql + " order by prd_des";
		return obtenerLista(conSql);

	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<String> obtenerListaProductoPadre(int parPrdNivSup) {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from viewGrupoProducto where prd_nivel=" + parPrdNivSup;

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<String> lisDetPedPrd = new ArrayList<String>();

			while (resSql.next()) {

				lisDetPedPrd.add(resSql.getInt("prd_cod") + "#" +
						resSql.getInt("gru_cod") + "#" +
						resSql.getInt("prd_nivel") + "#" +
						resSql.getString("prd_ide") + "#" +
						resSql.getString("prd_des"));

			}// Fin de

			resSql.close();

			return lisDetPedPrd;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<Producto> obtenerListaProducto(int parPedCod, int parPrdNiv, int parPrdCod) {

		String conSql = "";
		if (parPrdNiv == 1) {
			conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv;
		} else if (parPrdNiv == 2) {
			conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supuno=" + parPrdCod;
		} else if (parPrdNiv == 3) {
			conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supdos=" + parPrdCod;
		} else if (parPrdNiv == 4) {
			conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_tres=" + parPrdCod;
		} else if (parPrdNiv == 5) {
			conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_cua=" + parPrdCod;
		} else {
			conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod;
		}

		return obtenerLista(conSql);

	}

	public List<Producto> obtenerListaProducto() {

		String conSql = "select * from producto order by prd_ide";
		return obtenerLista(conSql);

	}

	public List<Producto> obtenerLista(String parConSql) {

		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parConSql).executeQuery();
			List<Producto> lisPrd = new ArrayList<Producto>();
			Producto varPrd;
			while (resSql.next()) {

				varPrd = new Producto();

				varPrd.setPrd_cod(resSql.getInt("prd_cod"));
				varPrd.setGru_cod(resSql.getInt("gru_cod"));
				varPrd.setPrd_ide(resSql.getString("prd_ide"));
				varPrd.setPrd_tip(resSql.getString("prd_tip"));
				varPrd.setPrd_mat(resSql.getString("prd_mat"));
				varPrd.setPrd_gro(resSql.getString("prd_gro"));
				varPrd.setPrd_gau(resSql.getString("prd_gau"));
				varPrd.setPrd_col(resSql.getString("prd_col"));
				varPrd.setPrd_des(resSql.getString("prd_des"));
				varPrd.setPrd_par(resSql.getString("prd_par"));
				varPrd.setPrd_stkact(resSql.getInt("prd_stkact"));
				varPrd.setPrd_supuno(resSql.getInt("prd_supuno"));
				varPrd.setPrd_supdos(resSql.getInt("prd_supdos"));
				varPrd.setPrd_suptres(resSql.getInt("prd_suptres"));
				varPrd.setPrd_supcua(resSql.getInt("prd_supcua"));
				varPrd.setPrd_nivel(resSql.getInt("prd_nivel"));
				varPrd.setPrd_bar(resSql.getString("prd_bar"));
				varPrd.setPrd_foto(Util.getInst().leerBlob(resSql.getBytes("prd_foto")));
				varPrd.setPrd_ver(resSql.getInt("prd_ver"));

				lisPrd.add(varPrd);

			}// Fin de
			resSql.close();
			return lisPrd;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo getListaPrdiculo(String parPrdDes)

	public List<Producto> obtenerListaProducto(String parPrdDes) {

		String conSql;
		if (parPrdDes.equals("TODOS")) {

			conSql = "select * from producto order by prd_des";

		}// Fin de if (parEmpNom.equals("TODOS"))
		else {

			conSql = "select * from producto where "
					+ "(prd_des like '" + parPrdDes + "%' or "
					+ "prd_des like '%" + parPrdDes + "%') order by prd_des";

		}

		return obtenerLista(conSql);

	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public boolean existeProductoPadrePedido(int parPedCod, int parPrdNiv, int parPrdCod) {

		String conSql;
		ResultSet resSql;
		try {

			if (parPrdNiv == 1 || parPrdCod == 0) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv;
			} else if (parPrdNiv == 2) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supuno=" + parPrdCod;
			} else if (parPrdNiv == 3) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supdos=" + parPrdCod;
			} else if (parPrdNiv == 4) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_suptres=" + parPrdCod;
			} else if (parPrdNiv == 5) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supcua=" + parPrdCod;
			} else {
				conSql = "select * from viewPedidoProducto where ped_cod=0";
			}
			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);

			if (resSql.next()) {
				resSql.close();
				return true;

			}// Fin de
			resSql.close();
			return false;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public boolean existeProductoPadre(int parPrdNiv, int parPrdCod) {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from viewGrupoProducto";

			if (parPrdNiv == 1 || parPrdCod == 0) {
				conSql = conSql + " where prd_nivel=" + parPrdNiv;
			} else if (parPrdNiv == 2) {
				conSql = conSql + " where prd_nivel=" + parPrdNiv + " and prd_supuno=" + parPrdCod;
			} else if (parPrdNiv == 3) {
				conSql = conSql + " where prd_nivel=" + parPrdNiv + " and prd_supdos=" + parPrdCod;
			} else if (parPrdNiv == 4) {
				conSql = conSql + " where prd_nivel=" + parPrdNiv + " and prd_suptres=" + parPrdCod;
			} else if (parPrdNiv == 5) {
				conSql = conSql + " where prd_nivel=" + parPrdNiv + " and prd_supcua=" + parPrdCod;
			}
			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);

			if (resSql.next()) {
				resSql.close();
				return true;

			}// Fin de
			resSql.close();
			return false;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public Producto obtenerProducto(int parPrdCod) {

		String conSql = "select * from producto where prd_cod=" + parPrdCod;
		return obtenerEntidad(conSql);

	}

	public int obtenerModeloCodigo(String parModIde) {

		String conSql = "select * from producto where prd_ide='" + parModIde + "'";
		PreparedStatement pstm;
		int codigo = 0;
		try {

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			ResultSet resSql = pstm.executeQuery();

			if (resSql.next()) {
				codigo = resSql.getInt("prd_cod");
				resSql.close();
			}

			return codigo;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}

	public Producto obtenerProductoIde(String parPrdIde) {

		String conSql = "select * from producto where prd_ide='" + parPrdIde + "'";
		return obtenerEntidad(conSql);

	}

	public Producto obtenerEntidad(String parSql) {
		PreparedStatement pstm;
		try {

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parSql);

			ResultSet resSql = pstm.executeQuery();
			prdTmp.limpiarInstancia();

			if (resSql.next()) {

				prdTmp.setPrd_cod(resSql.getInt("prd_cod"));
				prdTmp.setGru_cod(resSql.getInt("gru_cod"));
				prdTmp.setPrd_ide(resSql.getString("prd_ide"));
				prdTmp.setPrd_tip(resSql.getString("prd_tip"));
				prdTmp.setPrd_mat(resSql.getString("prd_mat"));
				prdTmp.setPrd_gro(resSql.getString("prd_gro"));
				prdTmp.setPrd_gau(resSql.getString("prd_gau"));
				prdTmp.setPrd_col(resSql.getString("prd_col"));
				prdTmp.setPrd_des(resSql.getString("prd_des"));
				prdTmp.setPrd_par(resSql.getString("prd_par"));
				prdTmp.setPrd_stkact(resSql.getInt("prd_stkact"));
				prdTmp.setPrd_stkact(resSql.getInt("prd_stkact"));
				prdTmp.setPrd_supuno(resSql.getInt("prd_supuno"));
				prdTmp.setPrd_supdos(resSql.getInt("prd_supdos"));
				prdTmp.setPrd_suptres(resSql.getInt("prd_suptres"));
				prdTmp.setPrd_supcua(resSql.getInt("prd_supcua"));
				prdTmp.setPrd_nivel(resSql.getInt("prd_nivel"));
				prdTmp.setPrd_bar(resSql.getString("prd_bar"));
				prdTmp.setPrd_foto(Util.getInst().leerBlob(resSql.getBytes("prd_foto")));
				prdTmp.setPrd_ver(resSql.getInt("prd_ver"));

			}
			pstm.close();
			resSql.close();
			return prdTmp;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}

	public boolean existeProductoCodCompuesto(String parPrdCod, String parCodInt) {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from producto where art_cod='" + parPrdCod + "' and art_codint='" + parCodInt + "'";
			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql).executeQuery();
			if (resSql.next()) {
				resSql.close();
				return true;

			}
			resSql.close();
			return false;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo getListaPrdiculo(String parPrdDes)

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}
		});
	}
}// Fin de clasr principal