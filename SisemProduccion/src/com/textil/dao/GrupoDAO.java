package com.textil.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Grupo;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.DriverJdbc;

public class GrupoDAO {

	private Grupo gru;

	public GrupoDAO(Grupo parGru)
	{
		this.gru = parGru;
	}

	public int agregar(Grupo parGru) {

		String conSql;
		int codIns = 0;
		PreparedStatement pstm;
		ResultSet resSql;
		try {
			if (DBConfiguracion.getInstance().getDriverJdbc().equals(DriverJdbc.MYSQL))
			{
				conSql = "insert into grupo values(0,?,?,?,?,?,?,?,?,?,?,?)";
			}
			else
			{
				conSql = "insert into grupo values(?,?,?,?,?,?,?,?,?,?,?)";
			}
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql, Statement.RETURN_GENERATED_KEYS);

			// pstm.setInt(1, parGru.getCla_cod());
			// pstm.setInt(2, parGru.getUnd_cod());
			// pstm.setInt(3, parGru.getPrd_cod());
			// pstm.setInt(4, parGru.getPed_cod());
			// pstm.setInt(5, parGru.getDetped_cod());
			// pstm.setInt(6, parGru.getDetped_canped());
			// pstm.setInt(7, parGru.getDetped_canfab());
			// pstm.setInt(8, parGru.getDetped_canent());
			// pstm.setDouble(9, parGru.getDetped_pre());
			// pstm.setDouble(10, parGru.getDetped_imp());
			// pstm.setDouble(11, parGru.getDetped_ver());

			if (pstm.executeUpdate() > 0) {
				resSql = pstm.getGeneratedKeys();
				while (resSql.next()) {

					codIns = resSql.getInt(1);

				}
				resSql.close();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return codIns;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public List<Grupo> obtenerListaGrupo() {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from grupo";
			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<Grupo> lisGru = new ArrayList<Grupo>();

			Grupo varGru;

			while (resSql.next()) {

				varGru = new Grupo();

				varGru.setGru_cod(resSql.getInt("gru_cod"));
				varGru.setGru_tipo(resSql.getString("gru_tipo"));
				varGru.setGru_nom(resSql.getString("gru_nom"));
				varGru.setGru_des(resSql.getString("gru_des"));
				varGru.setGru_supuno(resSql.getInt("gru_supuno"));
				varGru.setGru_supdos(resSql.getInt("gru_supdos"));
				varGru.setGru_suptres(resSql.getInt("gru_suptres"));
				varGru.setGru_supcua(resSql.getInt("gru_supcua"));
				varGru.setGru_nivel(resSql.getInt("gru_nivel"));
				varGru.setGru_ver(resSql.getInt("gru_ver"));

				lisGru.add(varGru);

			}// Fin de

			resSql.close();
			return lisGru;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<String> obtenerListaGrupoPedido(int parPedCod) {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select grupo.gru_cod,grupo.gru_tipo,grupo.gru_nom,grupo.gru_nivel "
					+ "from grupo inner join producto on grupo.gru_cod=producto.gru_cod "
					+ "inner join detpedido on producto.prd_cod=detpedido.prd_cod "
					+ "inner join pedido on pedido.ped_cod=detpedido.ped_cod "
					+ "where pedido.ped_cod=" + parPedCod + " "
					+ "group by grupo.gru_cod,grupo.gru_tipo,grupo.gru_nom,grupo.gru_nivel";

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<String> lisGru = new ArrayList<String>();

			while (resSql.next()) {

				lisGru.add(resSql.getInt("gru_cod") + "#" +
						resSql.getString("gru_tipo") + "#" +
						resSql.getString("gru_nom") + "#" +
						resSql.getString("gru_nivel"));

			}// Fin de

			resSql.close();
			return lisGru;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public Grupo obtenerGrupo(String parGruNom) {

		String conSql = "select * from grupo where gru_nom='" + parGruNom + "'";
		return ejecutarConsulta(conSql);

	}

	public Grupo ejecutarConsulta(String parSql) {
		PreparedStatement pstm;
		try {

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parSql);

			ResultSet resSql = pstm.executeQuery();
			gru.limpiarInstancia();

			if (resSql.next()) {

				gru.setGru_cod(resSql.getInt("gru_cod"));
				gru.setGru_tipo(resSql.getString("gru_tipo"));
				gru.setGru_nom(resSql.getString("gru_nom"));
				gru.setGru_des(resSql.getString("gru_des"));
				gru.setGru_supuno(resSql.getInt("gru_supuno"));
				gru.setGru_supdos(resSql.getInt("gru_supdos"));
				gru.setGru_suptres(resSql.getInt("gru_suptres"));
				gru.setGru_supcua(resSql.getInt("gru_supcua"));
				gru.setGru_nivel(resSql.getInt("gru_nivel"));
				gru.setGru_ver(resSql.getInt("gru_ver"));

			}
			pstm.close();
			resSql.close();
			return gru;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}

	public boolean eliminar(int parDetPedCod) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from grupo where detped_cod=" + parDetPedCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			int afectadas = pstm.executeUpdate();
			pstm.close();
			return (afectadas == 1) ? true : false;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}// Fin de public void run()
		});
	}// Fin de metod main
}// in de clase prinicipal