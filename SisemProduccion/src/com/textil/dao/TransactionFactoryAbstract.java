package com.textil.dao;

import java.util.Properties;
import javax.sql.DataSource;

/**
 *
 * Fabrica para crear implementaciones de TransactionFactoryListener.
 *
 * @author jonathansanchez
 *
 *
 *
 */
public class TransactionFactoryAbstract {

    public final static String JDBC = "jdbc";
    public final static String IMPLEMENTATION_TYPE_PROPERTY_KEY = "implementation";

//    public TransactionFactoryListener createFactory(Properties factoryProperties) {
//
//        TransactionFactoryListener factory = null;
//        if (JDBC.equalsIgnoreCase(factoryProperties.getProperty(IMPLEMENTATION_TYPE_PROPERTY_KEY))) {
//
//            factory = this.createFactory(factoryProperties);
//
//        }
//
//        return factory;
//
//    } // createFactory.

} 