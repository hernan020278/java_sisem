package com.textil.dao;

import java.sql.*;

/**
 * @author HERNAN
 */
public class ConeccionDAO {

    private static TransactionFactory transactionFactory = null;
    protected TransactionDelegate transactionDelegate = null;
    private boolean isTransactional = false;

    /**
     * Constructor por defecto.
     */
    public ConeccionDAO() {

        super();

    } // BaseDAO.

    /**
     * Constructor por defecto.
     */
    public ConeccionDAO(TransactionDelegate transactionDelegate) {

        super();
        this.transactionDelegate = transactionDelegate;
        this.isTransactional = true;

    } // BaseDAO.

    protected Connection obtenerConeccion() {

        if (transactionDelegate != null) {

            return this.transactionDelegate.obtenerConeccion();

        }//Fin de if(transactionDelegate != null)
        else if (transactionDelegate == null) {

            System.out.println("No Existe un delegado " + this.getClass().getName());
        }
        
        return null;

    } // obtenerConeccion.

    protected void closeQuiet(Connection conn) {
        if (null != conn) {
            try {
                if (!this.isTransactional) {

                    conn.close();

                }
            } catch (SQLException e) {

                e.printStackTrace();

            }
        }//Fin de if (null != conn)
    } // closeQuiet.

    public static void setTransactionFactory(TransactionFactory transactionFactory) {

        ConeccionDAO.transactionFactory = transactionFactory;

    } // setTransactionFactory.

    protected void closeQuiet(PreparedStatement ps) {
        if (null != ps) {
            try {

                ps.close();

            } catch (SQLException e) {

                e.printStackTrace();

            }
        }
    } // closeQuiet.

    protected void closeQuiet(ResultSet rs) {
        if (null != rs) {
            try {

                rs.close();

            } catch (SQLException e) {

                e.printStackTrace();

            }
        }
    } // closeQuiet.

    protected void closeQuiet(Connection conn, PreparedStatement ps, ResultSet rs) {

        this.closeQuiet(rs);
        this.closeQuiet(ps);
        this.closeQuiet(conn);

    } // closeQuiet.
}//Fin de clase principal ConeccionDAO
