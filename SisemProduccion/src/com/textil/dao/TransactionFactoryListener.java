package com.textil.dao;

import java.sql.Connection;

/**
 *
 * Define el funcionamiento basico de una fabrica de transacciones.
 *
 *
 */
public interface TransactionFactoryListener {

    /**
     *
     * Obtiene una transaccion.
     *
     * @return TransactionDelegate.
     *
     */
    public abstract Connection crearNuevaConeccion(String parTipoConeccion);
    
} 