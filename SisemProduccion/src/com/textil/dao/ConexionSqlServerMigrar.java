package com.textil.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.comun.utilidad.PropiedadesExterna;

public class ConexionSqlServerMigrar {

	public static Connection crearConexion()
	{
		PropiedadesExterna prop = new PropiedadesExterna();
		String servidor_database = prop.getServidor("servidor_database");
		String usuario = prop.getServidor("usuario");
		String password = prop.getServidor("password");

		try {

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conectarBd = DriverManager.getConnection("jdbc:sqlserver://" + servidor_database + "\\SQLEXPRESS:1433;databaseName=dbsistemachentty_v5", usuario, password);

			return conectarBd;

		} catch (ClassNotFoundException e) {

			JOptionPane.showConfirmDialog(null,
					"No se encontro el controlador de SQLServer!!!",
					"Sistema de Hernan",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			throw new RuntimeException(e);

		} catch (SQLException e) {

			JOptionPane.showConfirmDialog(null,
					"Error de Sentencia SQL - 6!!!",
					"Sistema de Hernan",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			throw new RuntimeException(e);

		}
	}

	public static void main(String[] args) throws SQLException {

		Connection c = ConexionSqlServerMigrar.crearConexion();
		System.out.println("Cerrada : " + c.isClosed());
		c.close();
	}
}
