package com.textil.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.CtrlTabla;
import com.comun.organizacion.OrganizacionGeneral;

public class CtrlTablaDAO {

	private CtrlTabla varCtrlTab;

	public CtrlTablaDAO(CtrlTabla parCtrlTab)
	{
		this.varCtrlTab = parCtrlTab;
	}// Fin de constructor

	public CtrlTabla getNumRegTab(String parTabNom) {

		ResultSet resSql;
		PreparedStatement pstmtm;
		try {

			String conSql = "select * from ctrltabla where ctrl_nomtab = ?";
			pstmtm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);
			pstmtm.setString(1, parTabNom);

			resSql = pstmtm.executeQuery();

			varCtrlTab.limpiarInstancia();

			if (resSql.next()) {

				varCtrlTab.setCtrl_nomtab(resSql.getString("ctrl_nomtab"));
				varCtrlTab.setCtrl_numreg(resSql.getInt("ctrl_numreg"));
				varCtrlTab.setCtrl_numser(resSql.getInt("ctrl_numser"));
				varCtrlTab.setCtrl_valor(resSql.getInt("ctrl_valor"));

			}// Fin de Bucle While para explorar los datos

			pstmtm.close();
			resSql.close();
			return varCtrlTab;

		} catch (SQLException e) {

			throw new RuntimeException(e);

		}
	}// Fin de Metodo Buscar Ide del Daoente

	public boolean actualizar(CtrlTabla parCtrlTab) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "update CtrlTabla set ctrl_numreg=?, ctrl_numser=?, ctrl_valor=? where ctrl_nomtab=?";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			pstm.setInt(1, parCtrlTab.getCtrl_numreg());
			pstm.setInt(2, parCtrlTab.getCtrl_numser());
			pstm.setDouble(3, parCtrlTab.getCtrl_valor());
			pstm.setString(4, parCtrlTab.getCtrl_nomtab());

			int afectadas = pstm.executeUpdate();
			pstm.close();
			return (afectadas == 1) ? true : false;

		} catch (SQLException e) {

			throw new RuntimeException(e);

		}
	}// Fin de Metodo Actualizar de TipleadoDao

	public List<CtrlTabla> obtenerListaCtrlTabla() {

		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "select * from ctrltabla "
					+ "where ctrl_nomtab <> 'NOTA DE INGRESO' and "
					+ "ctrl_nomtab <> 'NOTA DE SALIDA' and "
					+ "ctrl_nomtab <> 'NOTA DE VENTA' order by ctrl_nomtab, ctrl_numser";

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);
			ResultSet resSql = pstm.executeQuery();
			List<CtrlTabla> lisCtrlTabla = new ArrayList<CtrlTabla>();

			CtrlTabla ctrlTab;
			while (resSql.next()) {

				ctrlTab = new CtrlTabla();

				ctrlTab.setCtrl_nomtab(resSql.getString("ctrl_nomtab"));
				ctrlTab.setCtrl_numreg(resSql.getInt("ctrl_numreg"));
				ctrlTab.setCtrl_numser(resSql.getInt("ctrl_numser"));
				ctrlTab.setCtrl_valor(resSql.getDouble("ctrl_valor"));

				lisCtrlTabla.add(ctrlTab);

			}// Fin de metodo
			pstm.close();
			resSql.close();

			return lisCtrlTabla;

		} catch (SQLException e) {
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo para obtener detalle

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}
		});
	}

}// Fin de Clase Principal
