package com.textil.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Organizacion;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.Util;

public class OrganizacionDAO {

	private Organizacion varOrg;

	public OrganizacionDAO(Organizacion parOrg)
	{
		this.varOrg = parOrg;
	}// Fin de constructor

	public Organizacion obtenerOrganizacion(String parOrgCod) {

		String conSql;
		PreparedStatement pstm;
		ResultSet resSql;

		try {

			conSql = "select * from Organizacion where orgcodigo='" + parOrgCod + "'";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);
			resSql = pstm.executeQuery();

			Util.getInst().limpiarEntidad(varOrg, true);
			if (resSql.next())
			{
				varOrg.setOrgcodigo(resSql.getString("orgcodigo"));
				varOrg.setFechafinal(resSql.getDate("fechafinal"));
				varOrg.setNombre(resSql.getString("nombre"));
				varOrg.setEstado(resSql.getString("estado"));
				varOrg.setVersion(resSql.getInt("version"));
			}
			pstm.close();
			resSql.close();
		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
		return varOrg;

	}

	public boolean existeOrganizacion(String parOrgMail, String parOrgClave) {

		String conSql;
		PreparedStatement pstm;
		ResultSet resSql;

		try {

			conSql = "select * from Organizacion where mail = ? and clave = ?";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			pstm.setString(1, parOrgMail);
			pstm.setString(2, parOrgClave);
			resSql = pstm.executeQuery();

			if (resSql.next()) {
				pstm.close();
				resSql.close();
				return true;

			}

			pstm.close();
			resSql.close();
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Buscar Nombre de ORPersonal

	public ImageIcon leerBlob(byte[] parResSql) {

		ImageIcon emp_logo;

		if (parResSql != null) {
			emp_logo = new ImageIcon(parResSql);
		} else {
			emp_logo = null;
		}
		return emp_logo;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String conSql;
				PreparedStatement pstm;

				// MigrarOrganizacion empMig;
				// OrganizacionDAO empDAO = new OrganizacionDAO(null, null);
				//
				// List<MigrarOrganizacion> lisEmp = empDAO.getListMigrarOrganizacion();
				// Iterator ite = lisEmp.iterator();
				//
				// while (ite.hasNext()) {
				//
				// empMig = (MigrarOrganizacion) ite.next();
				//
				// try {
				// Connection conexSQLServer = ConexionSqlServer.crearConexion();
				//
				// conSql = "insert into Organizacion values(?,?,?,?,?)";
				// pstm = conexSQLServer.prepareStatement(conSql);
				//
				// pstm.setString(1, empMig.getEmp_cod());//Codigo
				// pstm.setString(2, empMig.getEmp_ruc());
				// pstm.setString(3, empMig.getEmp_nombre());
				// pstm.setString(4, empMig.getEmp_descripcion());
				// pstm.setString(5, empMig.getEmp_contacto());
				//
				//
				// pstm.execute();
				//
				// conexSQLServer.close();
				//
				// } catch (SQLException e) {
				// throw new RuntimeException(e);
				// }
				//
				// System.out.println("Codigo : " + empMig.getEmp_cod());
				// }
			}
		});
	}
}