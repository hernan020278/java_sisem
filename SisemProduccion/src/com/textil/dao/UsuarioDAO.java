package com.textil.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Usuario;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.Util;

public class UsuarioDAO {

	private Usuario varUsu;

	public UsuarioDAO(Usuario parUsu)
	{
		this.varUsu = parUsu;
	}// Fin de constructor

	public Usuario obtenerUsuario(String parUsuIde, String parUsuClave)
	{
		String conSql;
		conSql = "select * from usuario where usuario='" + parUsuIde + "' and clave='" + parUsuClave + "'";
		return obtenerEntidad(conSql);
	}

	public Usuario obtenerUsuario(String parUsuIde) {

		String conSql;
		conSql = "select * from usuario where usuario='" + parUsuIde + "'";

		return obtenerEntidad(conSql);

	}

	public Usuario obtenerEntidad(String parSql) {

		PreparedStatement pstm;
		ResultSet resSql;

		try {

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parSql);
			resSql = pstm.executeQuery();

			Util.getInst().limpiarEntidad(varUsu,true);
			if (resSql.next()) {

				varUsu.setUsucodigo(resSql.getBigDecimal("usucodigo"));
				varUsu.setIdentidad(resSql.getString("identidad"));
				varUsu.setOrgcodigo(resSql.getString("orgcodigo"));
				varUsu.setMail(resSql.getString("mail"));
				varUsu.setNombre(resSql.getString("nombre"));
				varUsu.setApellido(resSql.getString("apellido"));
				varUsu.setCargo(resSql.getString("cargo"));
				varUsu.setTelefono(resSql.getString("telefono"));
				varUsu.setEstado(resSql.getString("estado"));
				varUsu.setPropietario(resSql.getString("propietario"));
				varUsu.setFechaingreso(resSql.getDate("fechaingreso"));
				varUsu.setFechasalida(resSql.getDate("fechasalida"));
				varUsu.setArecodigo(resSql.getBigDecimal("arecodigo"));
				varUsu.setUsuario(resSql.getString("usuario"));
				varUsu.setClave(resSql.getString("clave"));
				varUsu.setBloqueado(resSql.getString("bloqueado"));
				varUsu.setPregseguridad(resSql.getString("pregseguridad"));
				varUsu.setResseguridad(resSql.getString("resseguridad"));
				varUsu.setMailactivo(resSql.getString("mailactivo"));
				varUsu.setVersion(resSql.getBigDecimal("version"));

			}
			pstm.close();
			resSql.close();
			
		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
		return varUsu;

	}

	public boolean existeUsuario(String parUsuMail, String parUsuClave) {

		String conSql;
		PreparedStatement pstm;
		ResultSet resSql;

		try {

			conSql = "select * from usuario where mail = ? and clave = ?";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			pstm.setString(1, parUsuMail);
			pstm.setString(2, parUsuClave);
			resSql = pstm.executeQuery();

			if (resSql.next()) {
				pstm.close();
				resSql.close();
				return true;

			}

			pstm.close();
			resSql.close();
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Buscar Nombre de ORPersonal

	public boolean existeUsuario(String parUsuUsu, String parUsuPas, int parUsuCod) {

		String conSql;
		PreparedStatement pstm;
		ResultSet resSql;

		try {

			conSql = "select * from usuario where eml_usu = ? and eml_pas = ? and eml_cod <> ?";
			String otro = "select * from usuario where eml_usu = '" + parUsuUsu + "' and eml_pas = '" + parUsuPas + "' and eml_cod <> '" + parUsuCod + "'";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			pstm.setString(1, parUsuUsu);
			pstm.setString(2, parUsuPas);
			pstm.setInt(3, parUsuCod);
			resSql = pstm.executeQuery();

			if (resSql.next()) {

				pstm.close();
				resSql.close();
				return true;

			}

			pstm.close();
			resSql.close();
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Buscar Nombre de ORPersonal

	public List<Usuario> obtenerListaUsuario(String parUsuNom, String parUsuEst) {

		String conSql;
		try {

			if (parUsuNom.equals("TODOS")) {

				conSql = "select * from usuario where estado='" + parUsuEst + "' order by nombre";

			} else {

				conSql = "select * from usuario where "
						+ "(nombre like '" + parUsuNom + "%' or "
						+ "nombre like '%" + parUsuNom + "%') and estado='" + parUsuEst + "' order by nombre";

			}
			ResultSet resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<Usuario> lisPer = new ArrayList<Usuario>();

			Usuario usu;
			while (resSql.next()) {

				usu = new Usuario();

				usu.setUsucodigo(resSql.getBigDecimal("usucodigo"));
				usu.setIdentidad(resSql.getString("identidad"));
				usu.setOrgcodigo(resSql.getString("orgcodigo"));
				usu.setMail(resSql.getString("mail"));
				usu.setNombre(resSql.getString("nombre"));
				usu.setApellido(resSql.getString("apellido"));
				usu.setCargo(resSql.getString("cargo"));
				usu.setTelefono(resSql.getString("telefono"));
				usu.setEstado(resSql.getString("estado"));
				usu.setPropietario(resSql.getString("propietario"));
				usu.setFechaingreso(resSql.getDate("fechaingreso"));
				usu.setFechasalida(resSql.getDate("fechasalida"));
				usu.setArecodigo(resSql.getBigDecimal("arecodigo"));
				usu.setClave(resSql.getString("clave"));
				usu.setBloqueado(resSql.getString("bloqueado"));
				usu.setPregseguridad(resSql.getString("pregseguridad"));
				usu.setResseguridad(resSql.getString("resseguridad"));
				usu.setMailactivo(resSql.getString("mailactivo"));
				usu.setVersion(resSql.getBigDecimal("version"));

				lisPer.add(usu);

			}// Fin de metodo
			resSql.close();
			return lisPer;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListausuario(String parUsuNom)

	public ImageIcon leerBlob(byte[] parResSql) {

		ImageIcon emp_logo;

		if (parResSql != null) {
			emp_logo = new ImageIcon(parResSql);
		} else {
			emp_logo = null;
		}
		return emp_logo;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String conSql;
				PreparedStatement pstm;

				// Migrarusuario empMig;
				// usuarioDAO empDAO = new usuarioDAO(null, null);
				//
				// List<Migrarusuario> lisEmp = empDAO.getListMigrarusuario();
				// Iterator ite = lisEmp.iterator();
				//
				// while (ite.hasNext()) {
				//
				// empMig = (Migrarusuario) ite.next();
				//
				// try {
				// Connection conexSQLServer = ConexionSqlServer.crearConexion();
				//
				// conSql = "insert into usuario values(?,?,?,?,?)";
				// pstm = conexSQLServer.prepareStatement(conSql);
				//
				// pstm.setString(1, empMig.getEmp_cod());//Codigo
				// pstm.setString(2, empMig.getEmp_ruc());
				// pstm.setString(3, empMig.getEmp_nombre());
				// pstm.setString(4, empMig.getEmp_descripcion());
				// pstm.setString(5, empMig.getEmp_contacto());
				//
				//
				// pstm.execute();
				//
				// conexSQLServer.close();
				//
				// } catch (SQLException e) {
				// throw new RuntimeException(e);
				// }
				//
				// System.out.println("Codigo : " + empMig.getEmp_cod());
				// }
			}
		});
	}
}