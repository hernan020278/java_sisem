package com.textil.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.comun.utilidad.PropiedadesExterna;

public class ConexionSqlServer {


	public static Connection crearConexion() {
		
		PropiedadesExterna prop = new PropiedadesExterna();
		String servidor_database = prop.getServidor("servidor_database");
		String usuario = prop.getServidor("usuario");
		String password = prop.getServidor("password");
		
		Connection conectarBd;
		try {

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			conectarBd = DriverManager.getConnection("jdbc:sqlserver://" + servidor_database + ":1433;databaseName=dbsistematextil", usuario, password);

			return conectarBd;

		} catch (ClassNotFoundException e) {

			JOptionPane.showConfirmDialog(null,
					"No se encontro el controlador de SQLServer!!!",
					"Sistema de Asistencia",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			throw new RuntimeException(e);
		} catch (SQLException e) {

			JOptionPane.showConfirmDialog(null,
					"Error de Sentencia SQL - 1!!!",
					"Sistema de Asistencia",
					JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

			throw new RuntimeException(e);

		}
	}

	public static void main(String[] args) throws SQLException {

		Connection c = ConexionSqlServer.crearConexion();

		// String conSql;
		// PreparedStatement ps;
		// try {
		//
		// conSql = "backup database scm to disk = 'D:\\prueba.bak'";
		//
		// ps = c.prepareStatement(conSql);
		// ps.execute();
		//
		// } catch (SQLException e) {
		// // funUsu.mostrarMensaje("¡¡¡Error en metodo agregar()",
		// // this.getClass().getName());
		// throw new RuntimeException(e);
		// }

		System.out.println("Cerrada : " + c.isClosed());
		c.close();

	}
}
