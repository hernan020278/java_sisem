package com.textil.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.swing.SwingUtilities;

import org.apache.commons.dbcp.BasicDataSource;

import com.comun.utilidad.PropiedadesExterna;

public class PoolConexionSqlServer {

	private static DataSource dataSource;
	private static Connection cnxDB = null;

	public static Connection crearConexionWeb() throws NamingException {

		try {

			// Obtenemos el Pool de Conexiones
			Context initCtx = new InitialContext();
			dataSource = (DataSource) initCtx.lookup("JDBCPoolSQLServerDBChentty");

			cnxDB = dataSource.getConnection();

			return cnxDB;

		} catch (SQLException ex) {
			Logger.getLogger(PoolConexionSqlServer.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;

	}

	public static Connection crearConexionApp() 
	{
		PropiedadesExterna prop = new PropiedadesExterna();
		String servidor_database = prop.getServidor("servidor_database");
		String usuario = prop.getServidor("usuario");
		String password = prop.getServidor("password");
		
		try 
		{
			BasicDataSource basicDataSource = new BasicDataSource();

			basicDataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			basicDataSource.setUsername(usuario);
			basicDataSource.setPassword(password);

			basicDataSource.setUrl("jdbc:sqlserver://" + servidor_database + ":1433;databaseName=dbsistematextil");

			// Opcional. Sentencia SQL que le puede servir a BasicDataSource
			// para comprobar que la conexion es correcta.
			basicDataSource.setValidationQuery("select 1");

			dataSource = basicDataSource;

			return dataSource.getConnection();

		} catch (SQLException ex) {
			Logger.getLogger(PoolConexionSqlServer.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				Connection cnxDB = null;
				try {

					cnxDB = PoolConexionSqlServer.crearConexionApp();
					System.out.println("Cerrada : " + cnxDB.isClosed());
					cnxDB.close();

				} catch (SQLException ex) {
					Logger.getLogger(PoolConexionSqlServer.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
	}
}
