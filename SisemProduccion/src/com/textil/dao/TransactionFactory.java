package com.textil.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.swing.SwingUtilities;

import org.apache.commons.dbcp.BasicDataSource;

import com.comun.organizacion.OrganizacionGeneral;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.utilidad.PropiedadesExterna;


public class TransactionFactory implements TransactionFactoryListener {

	private static DataSource dataSource;
	private static Connection cnxDB = null;

	private static String  servidor_database = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("servidor-database", "");
	private static String  usuario = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("usuario-database", "");
	private static String  password = DiccionarioGeneral.getInstance("config", OrganizacionGeneral.getOrgcodigo()).getPropiedad("clave-database", "");
	
	public static Connection crearConexionWeb() {

		try {

			// Obtenemos el Pool de Conexiones
			Context initCtx = new InitialContext();
			dataSource = (DataSource) initCtx.lookup("JDBCPoolSQLServerDBChentty");

			cnxDB = dataSource.getConnection();

			return cnxDB;

		} catch (NamingException ex) {
			Logger.getLogger(TransactionFactory.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SQLException ex) {
			Logger.getLogger(PoolConexionSqlServer.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;

	}

	public static Connection crearConexionApp() {
		try {

			BasicDataSource basicDataSource = new BasicDataSource();

			basicDataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			basicDataSource.setUsername(usuario);
			basicDataSource.setPassword(password);
			basicDataSource.setUrl("jdbc:sqlserver://" + servidor_database + ":1433;databaseName=dbsistematextil");

			// Opcional. Sentencia SQL que le puede servir a BasicDataSource
			// para comprobar que la conexion es correcta.
			basicDataSource.setValidationQuery("select 1");

			dataSource = basicDataSource;

			return dataSource.getConnection();

		} catch (SQLException ex) {
			Logger.getLogger(PoolConexionSqlServer.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	@Override
	public Connection crearNuevaConeccion(String parTipoConeccion) {

		if (parTipoConeccion.equals("WEB")) {

			return crearConexionWeb();

		} else {

			return crearConexionApp();

		}

	} // createNewTransaction

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				Connection cnxDB = null;
				try {

					cnxDB = TransactionFactory.crearConexionApp();
					System.out.println("Cerrada : " + cnxDB.isClosed());
					cnxDB.close();

				} catch (SQLException ex) {
					Logger.getLogger(PoolConexionSqlServer.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
	}

}// Fin de clase principal TransactionFactory