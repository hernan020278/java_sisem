package com.textil.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Agrupacion;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.Util;

public class AgrupacionDAO {

	private Agrupacion varAgru;

	public AgrupacionDAO(Agrupacion parAgru) {

		this.varAgru = parAgru;

	}// Fin de constructor

	public Agrupacion obtenerAgrupacion(String parUsuIde) {

		String conSql;
		PreparedStatement pstm;
		ResultSet resSql;

		try {

			conSql = "select * from agrupacion where usuario='" + parUsuIde + "'";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);
			resSql = pstm.executeQuery();

			Util.getInst().limpiarEntidad(varAgru, true);
			if (resSql.next()) {

				varAgru.setSegcodigo(resSql.getString("segcodigo"));
				varAgru.setUsuario(resSql.getString("usuario"));
				varAgru.setVersion(resSql.getInt("version"));

			}
			pstm.close();
			resSql.close();
		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
		return varAgru;

	}

	public ImageIcon leerBlob(byte[] parResSql) {

		ImageIcon emp_logo;

		if (parResSql != null) {
			emp_logo = new ImageIcon(parResSql);
		} else {
			emp_logo = null;
		}
		return emp_logo;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String conSql;
				PreparedStatement pstm;

				// Migrarusuario empMig;
				// usuarioDAO empDAO = new usuarioDAO(null, null);
				//
				// List<Migrarusuario> lisEmp = empDAO.getListMigrarusuario();
				// Iterator ite = lisEmp.iterator();
				//
				// while (ite.hasNext()) {
				//
				// empMig = (Migrarusuario) ite.next();
				//
				// try {
				// Connection conexSQLServer = ConexionSqlServer.crearConexion();
				//
				// conSql = "insert into  values(?,?,?,?,?)";
				// pstm = conexSQLServer.prepareStatement(conSql);
				//
				// pstm.setString(1, empMig.getEmp_cod());//Codigo
				// pstm.setString(2, empMig.getEmp_ruc());
				// pstm.setString(3, empMig.getEmp_nombre());
				// pstm.setString(4, empMig.getEmp_descripcion());
				// pstm.setString(5, empMig.getEmp_contacto());
				//
				//
				// pstm.execute();
				//
				// conexSQLServer.close();
				//
				// } catch (SQLException e) {
				// throw new RuntimeException(e);
				// }
				//
				// System.out.println("Codigo : " + empMig.getEmp_cod());
				// }
			}
		});
	}
}