package com.textil.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Clase;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.DriverJdbc;

public class ClaseDAO {

	private Clase cla;

	public ClaseDAO(Clase parCla)
	{
		this.cla = parCla;
	}

	public int agregar(Clase parCla) {

		String conSql;
		int codIns = 0;
		PreparedStatement pstm;
		ResultSet resSql;
		try {
			if (DBConfiguracion.getInstance().getDriverJdbc().equals(DriverJdbc.MYSQL))
			{
				conSql = "insert into clase values(0,?,?,?,?)";
			}
			else
			{
				conSql = "insert into clase values(?,?,?,?)";
			}
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql, Statement.RETURN_GENERATED_KEYS);

			// pstm.setInt(1, parCla.getCla_cod());
			pstm.setInt(1, parCla.getGru_cod());
			pstm.setString(2, parCla.getCla_nom());
			pstm.setString(3, parCla.getCla_des());
			pstm.setInt(4, parCla.getCla_ver());

			if (pstm.executeUpdate() > 0) {
				resSql = pstm.getGeneratedKeys();
				while (resSql.next()) {

					codIns = resSql.getInt(1);

				}
				resSql.close();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return codIns;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public Clase obtenerClase(String parClaNom) {

		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "select * from clase where cla_nom='" + parClaNom + "'";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);
			ResultSet resSql = pstm.executeQuery();

			cla.limpiarInstancia();

			while (resSql.next()) {

				cla.setCla_cod(resSql.getInt("cla_cod"));
				cla.setGru_cod(resSql.getInt("gru_cod"));
				cla.setCla_nom(resSql.getString("cla_nom"));
				cla.setCla_des(resSql.getString("cla_des"));
				cla.setCla_ver(resSql.getInt("cla_ver"));

			}
			pstm.close();
			resSql.close();
			
			return cla;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error

	}

	public List<Clase> obtenerListaClase() {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from clase order by cla_nom";
			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<Clase> lisDetVen = new ArrayList<Clase>();

			Clase varCla;

			while (resSql.next()) {

				varCla = new Clase();

				varCla.setCla_cod(resSql.getInt("cla_cod"));
				varCla.setGru_cod(resSql.getInt("gru_cod"));
				varCla.setCla_nom(resSql.getString("cla_nom"));
				varCla.setCla_des(resSql.getString("cla_des"));
				varCla.setCla_ver(resSql.getInt("cla_ver"));

				lisDetVen.add(varCla);

			}// Fin de

			resSql.close();
			return lisDetVen;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<String> obtenerListaClase(String parGruNom, String parClaNom) {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select clase.gru_cod,clase.cla_cod,clase.cla_nom,clase.cla_des "
					+ "from grupo inner join clase on grupo.gru_cod=clase.gru_cod ";
			if (parClaNom.equals("*")) {
				conSql = conSql + "where grupo.gru_nom='" + parGruNom + "' order by clase.cla_nom ";
			} else {
				conSql = conSql + "where grupo.gru_nom='" + parGruNom + "' and (clase.cla_nom like '" + parClaNom + "%' or cla_nom like '%" + parClaNom + "%') order by clase.cla_nom ";
			}

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<String> lisCla = new ArrayList<String>();

			while (resSql.next()) {

				lisCla.add(resSql.getInt("gru_cod") + "#" +
						resSql.getInt("cla_cod") + "#" +
						resSql.getString("cla_nom") + "#" +
						resSql.getString("cla_des"));

			}// Fin de

			resSql.close();
			return lisCla;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public boolean existeClase(String parClaNom) {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from clase where cla_nom = '" + parClaNom + "'";

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql).executeQuery();

			if (resSql.next()) {
				resSql.close();
				return true;

			}
			resSql.close();
			return false;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error

	}// Fin de metodo para leer un registro de producto

	public boolean eliminar(int parGruCod) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from clase where gru_cod=" + parGruCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			int afectadas = pstm.executeUpdate();
			pstm.close();
			return (afectadas == 1) ? true : false;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public boolean reiniciarClase(String parTipoClase) {
		String conSql = "";
		CallableStatement cstm;
		try {

			if (parTipoClase.equals("TIPO")) {
				conSql = "{call proc_reiniciarTipo()}";
			}
			if (parTipoClase.equals("MATERIAL")) {
				conSql = "{call proc_reiniciarMaterial()}";
			}
			if (parTipoClase.equals("GAUGE")) {
				conSql = "{call proc_reiniciarGauge()}";
			}
			if (parTipoClase.equals("COLOR")) {
				conSql = "{call proc_reiniciarColor()}";
			}
			if (parTipoClase.equals("GROSOR")) {
				conSql = "{call proc_reiniciarGrosor()}";
			}
			if (!conSql.equals("")) {

				cstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareCall(conSql);
				cstm.execute();
				cstm.close();
				return true;

			}

			return false;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}// Fin de public void run()
		});
	}// Fin de metod main
}// in de clase prinicipal