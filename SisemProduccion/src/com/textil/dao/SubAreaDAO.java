package com.textil.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.SubArea;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.DriverJdbc;

public class SubAreaDAO {

	private SubArea subAre;
	private FileInputStream fis;

	public SubAreaDAO(SubArea parSubAre)
	{
		this.subAre = parSubAre;
	}

	public int agregar(SubArea parSubAre) {

		String conSql;
		int codIns = 0;
		PreparedStatement pstm;
		ResultSet resSql;
		try {
			if (DBConfiguracion.getInstance().getDriverJdbc().equals(DriverJdbc.MYSQL))
			{
				conSql = "insert into subarea values(0,?,?,?,?,?,?,?,?,?)";
			}
			else
			{
				conSql = "insert into subarea values(?,?,?,?,?,?,?,?,?)";
			}
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql, Statement.RETURN_GENERATED_KEYS);

			pstm.setInt(1, parSubAre.getAre_cod());
			pstm.setString(2, parSubAre.getSubare_ide());
			pstm.setString(3, parSubAre.getSubare_nom());
			pstm.setString(4, parSubAre.getSubare_des());
			pstm.setString(5, parSubAre.getSubare_int());
			pstm.setString(6, parSubAre.getSubare_bar());
			pstm.setBoolean(7, parSubAre.isSubare_act());
			pstm.setInt(8, parSubAre.getSubare_ord());
			pstm.setInt(9, parSubAre.getSubare_ver());

			if (pstm.executeUpdate() > 0) {
				resSql = pstm.getGeneratedKeys();
				while (resSql.next()) {

					codIns = resSql.getInt(1);

				}
				resSql.close();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return codIns;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public FileInputStream obtenerFileInputStream(File parFile) {
		try {
			fis = new FileInputStream(parFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fis;
	}

	public int actualizar(SubArea parSubAre) {

		String conSql;
		int resCod = 0;
		PreparedStatement pstm;
		try {

			conSql = "update subarea set are_cod=?,subare_ide=?,subare_nom=?,subare_des=?,subare_int=?,subare_bar=?,subare_act=?,subare_ord=?,subare_ver=? " +
					"where subare_cod=? and subare_ver=?";

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql, Statement.RETURN_GENERATED_KEYS);

			pstm.setInt(1, parSubAre.getAre_cod());
			pstm.setString(2, parSubAre.getSubare_ide());
			pstm.setString(3, parSubAre.getSubare_nom());
			pstm.setString(4, parSubAre.getSubare_des());
			pstm.setString(5, parSubAre.getSubare_int());
			pstm.setString(6, parSubAre.getSubare_bar());
			pstm.setBoolean(7, parSubAre.isSubare_act());
			pstm.setInt(8, parSubAre.getSubare_ord());
			pstm.setInt(9, parSubAre.getSubare_ver() + 1);
			pstm.setInt(10, parSubAre.getSubare_cod());
			pstm.setInt(11, parSubAre.getSubare_ver());

			if (pstm.executeUpdate() == 1) {

				resCod = parSubAre.getSubare_cod();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return resCod;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public SubArea obtenerSubArea(int parSubAreCod) {
		String conSql;
		conSql = "select * from subarea where subare_cod=" + parSubAreCod;
		return obtenerEntidad(conSql);
	}

	public SubArea obtenerSubArea(String parSubAreNom) {
		String conSql;
		conSql = "select * from subarea where subare_nom='" + parSubAreNom + "'";
		return obtenerEntidad(conSql);
	}

	public SubArea obtenerEntidad(String parConSql) {

		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parConSql).executeQuery();

			subAre.limpiarInstancia();

			if (resSql.next()) {

				subAre.setSubare_cod(resSql.getInt("subare_cod"));
				subAre.setAre_cod(resSql.getInt("are_cod"));
				subAre.setSubare_ide(resSql.getString("subare_ide"));
				subAre.setSubare_nom(resSql.getString("subare_nom"));
				subAre.setSubare_des(resSql.getString("subare_des"));
				subAre.setSubare_int(resSql.getString("subare_int"));
				subAre.setSubare_bar(resSql.getString("subare_bar"));
				subAre.setSubare_act(resSql.getBoolean("subare_act"));
				subAre.setSubare_ord(resSql.getInt("subare_ord"));
				subAre.setSubare_ver(resSql.getInt("subare_ver"));

			}// Fin de
			resSql.close();
			return subAre;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo getListaPrdiculo(String parPrdDes)

	public List<SubArea> obtenerListaSubArea(int parAreCod) {

		String conSql = "select * from subarea where are_cod=" + parAreCod;
		return obtenerListaEntidad(conSql);

	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<SubArea> obtenerListaSubArea() {

		String conSql = "select * from subarea order by subare_nom asc";
		return obtenerListaEntidad(conSql);

	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<SubArea> obtenerListaEntidad(String parSql) {

		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(parSql);
			List<SubArea> lisAre = new ArrayList<SubArea>();

			SubArea varSubAre;

			while (resSql.next()) {

				varSubAre = new SubArea();

				varSubAre.setSubare_cod(resSql.getInt("subare_cod"));
				varSubAre.setAre_cod(resSql.getInt("are_cod"));
				varSubAre.setSubare_ide(resSql.getString("subare_ide"));
				varSubAre.setSubare_nom(resSql.getString("subare_nom"));
				varSubAre.setSubare_des(resSql.getString("subare_des"));
				varSubAre.setSubare_int(resSql.getString("subare_int"));
				varSubAre.setSubare_bar(resSql.getString("subare_bar"));
				varSubAre.setSubare_act(resSql.getBoolean("subare_act"));
				varSubAre.setSubare_ord(resSql.getInt("subare_ord"));
				varSubAre.setSubare_ver(resSql.getInt("subare_ver"));

				lisAre.add(varSubAre);

			}// Fin de

			resSql.close();
			return lisAre;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public boolean eliminar(int parAreCod) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from subarea where subare_cod=" + parAreCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			int afectadas = pstm.executeUpdate();
			pstm.close();
			return (afectadas == 1) ? true : false;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}// Fin de public void run()
		});
	}// Fin de metod main
}// in de clase prinicipal