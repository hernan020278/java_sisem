package com.textil.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.DetPedido;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.DriverJdbc;

public class DetPedidoDAO {

	private DetPedido detPed;

	public DetPedidoDAO(DetPedido parDetPed)
	{
		this.detPed = parDetPed;
	}

	public int agregar(DetPedido parDetPed) {

		String conSql;
		int codIns = 0;
		PreparedStatement pstm;
		ResultSet resSql;
		try {
			if (DBConfiguracion.getInstance().getDriverJdbc().equals(DriverJdbc.MYSQL))
			{
				conSql = "insert into detpedido values(0,?,?,?,?,?,?,?,?,?,?)";
			}
			else
			{
				conSql = "insert into detpedido values(?,?,?,?,?,?,?,?,?,?)";
			}
			
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql, Statement.RETURN_GENERATED_KEYS);

			// pstm.setInt(0, parDetPed.getDetped_cod());
			pstm.setInt(1, parDetPed.getPed_cod());
			pstm.setInt(2, parDetPed.getPrd_cod());
			pstm.setInt(3, parDetPed.getGru_cod());
			pstm.setInt(4, parDetPed.getDetped_canped());
			pstm.setInt(5, parDetPed.getDetped_canfab());
			pstm.setInt(6, parDetPed.getDetped_canent());
			pstm.setDouble(7, parDetPed.getDetped_pre());
			pstm.setDouble(8, parDetPed.getDetped_imp());
			pstm.setString(9, parDetPed.getDetped_est());
			pstm.setDouble(10, parDetPed.getDetped_ver());

			if (pstm.executeUpdate() > 0) {
				resSql = pstm.getGeneratedKeys();
				while (resSql.next()) {

					codIns = resSql.getInt(1);
				}
				resSql.close();
			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return codIns;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public int actualizar(DetPedido parDetPed) {

		String conSql;
		int resCod = 0;
		PreparedStatement pstm;

		try {

			conSql = "update detpedido set ped_cod=?,prd_cod=?,gru_cod=?,detped_canped=?," +
					"detped_canfab=?,detped_canent=?,detped_pre=?, detped_imp=?, detped_est=?, detped_ver=? " +
					"where detped_cod=? and detped_ver=?";

			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			pstm.setInt(1, parDetPed.getPed_cod());
			pstm.setInt(2, parDetPed.getPrd_cod());
			pstm.setInt(3, parDetPed.getGru_cod());
			pstm.setInt(4, parDetPed.getDetped_canped());
			pstm.setInt(5, parDetPed.getDetped_canfab());
			pstm.setInt(6, parDetPed.getDetped_canent());
			pstm.setDouble(7, parDetPed.getDetped_pre());
			pstm.setDouble(8, parDetPed.getDetped_imp());
			pstm.setString(9, parDetPed.getDetped_est());
			pstm.setDouble(10, parDetPed.getDetped_ver() + 1);
			pstm.setDouble(11, parDetPed.getDetped_cod());
			pstm.setDouble(12, parDetPed.getDetped_ver());

			if (pstm.executeUpdate() == 1) {

				resCod = parDetPed.getPrd_cod();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return resCod;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de actualizar()

	public int obtenerTotalDetPedido(int parPedCod, int parPrdNiv, int parPrdCod) {

		String conSql;
		ResultSet resSql;
		int res=0;
		try {

			if (parPrdNiv == 1) {
				conSql = "select SUM(detped_canped) as detped_canped from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv;
			} else if (parPrdNiv == 2) {
				conSql = "select SUM(detped_canped) as detped_canped from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supuno=" + parPrdCod;
			} else if (parPrdNiv == 3) {
				conSql = "select SUM(detped_canped) as detped_canped from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supdos=" + parPrdCod;
			} else if (parPrdNiv == 4) {
				conSql = "select SUM(detped_canped) as detped_canped from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_tres=" + parPrdCod;
			} else if (parPrdNiv == 5) {
				conSql = "select SUM(detped_canped) as detped_canped from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_cua=" + parPrdCod;
			} else {
				conSql = "select SUM(detped_canped) as detped_canped from viewPedidoProducto where ped_cod=" + parPedCod;
			}

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql).executeQuery();
			if (resSql.next()) {
				res= resSql.getInt("detped_canped");
				
			}
			resSql.close();
			return res;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public int actualizarTotalDetPedito(int parPedCod, int parPrdNiv, int parPrdCod) {

		String conSql;
		PreparedStatement pstm = null;

		try {

			int detped_canped = obtenerTotalDetPedido(parPedCod, parPrdNiv, parPrdCod);
			conSql = "update detpedido set detped_canped=" + detped_canped + " where ped_cod=" + parPedCod + " and prd_cod=" + parPrdCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			int result = pstm.executeUpdate();

			pstm.close();

			return result;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de actualizar()

	public List<DetPedido> obtenerListaDetPedido(int parPedCod, int parPrdNiv, int parPrdCod) {

		String conSql;
		ResultSet resSql;
		try {

			if (parPrdNiv == 1) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv;
			} else if (parPrdNiv == 2) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supuno=" + parPrdCod;
			} else if (parPrdNiv == 3) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_supdos=" + parPrdCod;
			} else if (parPrdNiv == 4) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_tres=" + parPrdCod;
			} else if (parPrdNiv == 5) {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv + " and prd_cua=" + parPrdCod;
			} else {
				conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod;
			}
			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<DetPedido> lisDetPed = new ArrayList<DetPedido>();

			DetPedido varDetPed;

			while (resSql.next()) {

				varDetPed = new DetPedido();

				varDetPed.setDetped_cod(resSql.getInt("detped_cod"));
				varDetPed.setPed_cod(resSql.getInt("ped_cod"));
				varDetPed.setPrd_cod(resSql.getInt("prd_cod"));
				varDetPed.setGru_cod(resSql.getInt("gru_cod"));
				varDetPed.setDetped_canped(resSql.getInt("detped_canped"));
				varDetPed.setDetped_canfab(resSql.getInt("detped_canfab"));
				varDetPed.setDetped_canent(resSql.getInt("detped_canent"));
				varDetPed.setDetped_pre(resSql.getDouble("detped_pre"));
				varDetPed.setDetped_imp(resSql.getDouble("detped_imp"));
				varDetPed.setDetped_est(resSql.getString("detped_est"));
				varDetPed.setDetped_ver(resSql.getInt("detped_ver"));

				lisDetPed.add(varDetPed);

			}// Fin de

			resSql.close();
			return lisDetPed;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<DetPedido> obtenerListaDetPedidoFicha(int parPedCod, int parPrdNiv) {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_nivel=" + parPrdNiv;

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<DetPedido> lisDetPed = new ArrayList<DetPedido>();

			DetPedido varDetPed;

			while (resSql.next()) {

				varDetPed = new DetPedido();

				varDetPed.setDetped_cod(resSql.getInt("detped_cod"));
				varDetPed.setPed_cod(resSql.getInt("ped_cod"));
				varDetPed.setPrd_cod(resSql.getInt("prd_cod"));
				varDetPed.setGru_cod(resSql.getInt("gru_cod"));
				varDetPed.setDetped_canped(resSql.getInt("detped_canped"));
				varDetPed.setDetped_canfab(resSql.getInt("detped_canfab"));
				varDetPed.setDetped_canent(resSql.getInt("detped_canent"));
				varDetPed.setDetped_pre(resSql.getDouble("detped_pre"));
				varDetPed.setDetped_imp(resSql.getDouble("detped_imp"));
				varDetPed.setDetped_est(resSql.getString("detped_est"));
				varDetPed.setDetped_ver(resSql.getInt("detped_ver"));

				lisDetPed.add(varDetPed);

			}// Fin de

			resSql.close();
			return lisDetPed;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public DetPedido obtenerDetPedido(int parPedCod, int parPrdCod) {

		String conSql = "select * from viewPedidoProducto where ped_cod=" + parPedCod + " and prd_cod=" + parPrdCod;
		return obtenerEntidad(conSql);

	}

	public DetPedido obtenerDetPedido(int parDetPedCod) {

		String conSql = "select * from viewPedidoProducto where detped_cod=" + parDetPedCod;
		return obtenerEntidad(conSql);

	}

	public DetPedido obtenerEntidad(String parConSql) {

		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(parConSql);

			detPed.limpiarInstancia();

			if (resSql.next()) {

				detPed.setDetped_cod(resSql.getInt("detped_cod"));
				detPed.setPed_cod(resSql.getInt("ped_cod"));
				detPed.setPrd_cod(resSql.getInt("prd_cod"));
				detPed.setGru_cod(resSql.getInt("gru_cod"));
				detPed.setDetped_canped(resSql.getInt("detped_canped"));
				detPed.setDetped_canfab(resSql.getInt("detped_canfab"));
				detPed.setDetped_canent(resSql.getInt("detped_canent"));
				detPed.setDetped_pre(resSql.getDouble("detped_pre"));
				detPed.setDetped_imp(resSql.getDouble("detped_imp"));
				detPed.setDetped_est(resSql.getString("detped_est"));
				detPed.setDetped_ver(resSql.getInt("detped_ver"));

			}// Fin de

			resSql.close();
			return detPed;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error

	}

	public boolean eliminar(int parDetPedCod) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from detpedido where detped_cod=" + parDetPedCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			int afectadas = pstm.executeUpdate();
			pstm.close();
			return (afectadas == 1) ? true : false;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public boolean eliminarLista(int parPedCod) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from detpedido where ped_cod=" + parPedCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			int afectadas = pstm.executeUpdate();
			pstm.close();
			return (afectadas == 1) ? true : false;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente
	
	public boolean existeProductoDetPedido(int parPedCod, int parPrdCod, String parPrdIde) {

		String conSql;
		ResultSet resSql;
		try {

			if (parPrdCod == 0) {

				conSql = "select * from viewPedidoProducto where ped_cod='" + parPedCod + "' and prd_ide = '" + parPrdIde + "'";

			}// Fin de if(parVenCod.equals(""))
			else {

				conSql = "select * from viewPedidoProducto where ped_cod='" + parPedCod + "' and prd_cod <> " + parPrdCod + " and prd_ide = '" + parPrdIde + "'";

			}

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql).executeQuery();

			if (resSql.next()) {
				resSql.close();
				return true;

			}
			resSql.close();
			return false;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error

	}// Fin de metodo para leer un registro de producto

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}// Fin de public void run()
		});
	}// Fin de metod main
}// in de clase prinicipal