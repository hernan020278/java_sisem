package com.textil.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.PerfilSeguridad;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.utilidad.Util;

public class PerfilSeguridadDAO {

	private PerfilSeguridad varPerfSeg;

	public PerfilSeguridadDAO(PerfilSeguridad parPerfSeg)
	{
		this.varPerfSeg = parPerfSeg;
	}// Fin de constructor

	public PerfilSeguridad obtenerSeguridad(String parSecCod) {

		String conSql;
		PreparedStatement pstm;
		ResultSet resSql;

		try {

			conSql = "select * from  where segcodigo=" + parSecCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);
			resSql = pstm.executeQuery();

			Util.getInst().limpiarEntidad(varPerfSeg, true);

			if (resSql.next())
			{
				varPerfSeg.setTipo(resSql.getString("tipo"));
				varPerfSeg.setNombre(resSql.getString("nombre"));
				varPerfSeg.setSegcodigo(resSql.getString("segcodigo"));
				varPerfSeg.setNivel(resSql.getString("nivel"));
				varPerfSeg.setAcceso(resSql.getInt("acceso"));
				varPerfSeg.setVersion(resSql.getInt("version"));
			}
		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo agregar()", this.getClass().getName());
			throw new RuntimeException(e);
		}
		return varPerfSeg;

	}

	public List<PerfilSeguridad> obtenerListaSeguridad() {

		String conSql;
		try {

			conSql = "select * from PerfilSeguridad";

			ResultSet resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<PerfilSeguridad> lisPer = new ArrayList<PerfilSeguridad>();

			PerfilSeguridad sec;
			while (resSql.next()) {

				sec = new PerfilSeguridad();

				sec.setTipo(resSql.getString("tipo"));
				sec.setNombre(resSql.getString("nombre"));
				sec.setSegcodigo(resSql.getString("segcodigo"));
				sec.setNivel(resSql.getString("nivel"));
				sec.setAcceso(resSql.getInt("acceso"));
				sec.setVersion(resSql.getInt("version"));

				lisPer.add(sec);

			}// Fin de metodo
			resSql.close();
			return lisPer;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListausuario(String parUsuNom)

	public ImageIcon leerBlob(byte[] parResSql) {

		ImageIcon emp_logo;

		if (parResSql != null) {
			emp_logo = new ImageIcon(parResSql);
		} else {
			emp_logo = null;
		}
		return emp_logo;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String conSql;
				PreparedStatement pstm;

				// Migrarusuario empMig;
				// usuarioDAO empDAO = new usuarioDAO(null, null);
				//
				// List<Migrarusuario> lisEmp = empDAO.getListMigrarusuario();
				// Iterator ite = lisEmp.iterator();
				//
				// while (ite.hasNext()) {
				//
				// empMig = (Migrarusuario) ite.next();
				//
				// try {
				// Connection conexSQLServer = ConexionSqlServer.crearConexion();
				//
				// conSql = "insert into  values(?,?,?,?,?)";
				// pstm = conexSQLServer.prepareStatement(conSql);
				//
				// pstm.setString(1, empMig.getEmp_cod());//Codigo
				// pstm.setString(2, empMig.getEmp_ruc());
				// pstm.setString(3, empMig.getEmp_nombre());
				// pstm.setString(4, empMig.getEmp_descripcion());
				// pstm.setString(5, empMig.getEmp_contacto());
				//
				//
				// pstm.execute();
				//
				// conexSQLServer.close();
				//
				// } catch (SQLException e) {
				// throw new RuntimeException(e);
				// }
				//
				// System.out.println("Codigo : " + empMig.getEmp_cod());
				// }
			}
		});
	}
}