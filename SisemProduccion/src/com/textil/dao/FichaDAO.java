package com.textil.dao;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Ficha;
import com.comun.organizacion.OrganizacionGeneral;
import com.comun.referencia.DriverJdbc;

public class FichaDAO {

	private Ficha fic;

	public FichaDAO(Ficha parFic)
	{
		this.fic = parFic;
	}

	public int agregar(Ficha parFic) {

		String conSql;
		int codIns = 0;
		PreparedStatement pstm;
		ResultSet resSql;
		try {
			if (DBConfiguracion.getInstance().getDriverJdbc().equals(DriverJdbc.MYSQL))
			{
				conSql = "insert into Ficha values(0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			}
			else
			{
				conSql = "insert into Ficha values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			}
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql, Statement.RETURN_GENERATED_KEYS);

			// pstm.setInt(1, parFic.getfic_cod());
			pstm.setInt(1, parFic.getSubare_cod());
			pstm.setInt(2, parFic.getAre_cod());
			pstm.setInt(3, parFic.getDetped_cod());
			pstm.setInt(4, parFic.getPed_cod());
			pstm.setInt(5, parFic.getPrd_cod());
			pstm.setInt(6, parFic.getGru_cod());
			pstm.setString(7, parFic.getFic_ide());
			pstm.setInt(8, parFic.getFic_canped());
			pstm.setInt(9, parFic.getFic_canreg());
			pstm.setDate(10, parFic.getFic_fecing());
			pstm.setTime(11, parFic.getFic_horing());
			pstm.setDate(12, parFic.getFic_fecsal());
			pstm.setTime(13, parFic.getFic_horsal());
			pstm.setInt(14, parFic.getFic_horcon());
			pstm.setString(15, parFic.getFic_obs());
			pstm.setString(16, parFic.getFic_estado());
			pstm.setString(17, parFic.getFic_bar());
			pstm.setInt(18, parFic.getFic_ver());

			if (pstm.executeUpdate() > 0) {
				resSql = pstm.getGeneratedKeys();
				while (resSql.next()) {

					codIns = resSql.getInt(1);

				}
				resSql.close();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return codIns;
	}// Fin de Metodo Agregar Docente

	public int actualizar(Ficha parFic) {

		String conSql;
		int resCod = 0;
		PreparedStatement pstm;
		try {
			conSql = "update Ficha set subare_cod=?,are_cod=?,detped_cod=?,ped_cod=?,prd_cod=?," +
					"gru_cod=?,fic_ide=?,fic_canped=?,fic_canreg=?,fic_fecing=?,fic_horing=?,fic_fecsal=?," +
					"fic_horsal=?,fic_horcon=?,fic_obs=?,fic_estado=?,fic_bar=?,fic_ver=? " +
					"where fic_cod=? and fic_ver=?";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			pstm.setInt(1, parFic.getSubare_cod());
			pstm.setInt(2, parFic.getAre_cod());
			pstm.setInt(3, parFic.getDetped_cod());
			pstm.setInt(4, parFic.getPed_cod());
			pstm.setInt(5, parFic.getPrd_cod());
			pstm.setInt(6, parFic.getGru_cod());
			pstm.setString(7, parFic.getFic_ide());
			pstm.setInt(8, parFic.getFic_canped());
			pstm.setInt(9, parFic.getFic_canreg());
			pstm.setDate(10, parFic.getFic_fecing());
			pstm.setTime(11, parFic.getFic_horing());
			pstm.setDate(12, parFic.getFic_fecsal());
			pstm.setTime(13, parFic.getFic_horsal());
			pstm.setInt(14, parFic.getFic_horcon());
			pstm.setString(15, parFic.getFic_obs());
			pstm.setString(16, parFic.getFic_estado());
			pstm.setString(17, parFic.getFic_bar());
			pstm.setInt(18, parFic.getFic_ver() + 1);
			pstm.setInt(19, parFic.getFic_cod());
			pstm.setInt(20, parFic.getFic_ver());

			if (pstm.executeUpdate() == 1) {

				resCod = parFic.getFic_cod();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return resCod;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public int actualizarBarraSubArea(Ficha parFic) {

		String conSql;
		int resCod = 0;
		PreparedStatement pstm;
		try {
			conSql = "update Ficha set fic_cod=?,are_cod=?,detped_cod=?,ped_cod=?,prd_cod=?," +
					"gru_cod=?,fic_ide=?,fic_canped=?,fic_canreg=?,fic_fecing=?,fic_horing=?,fic_fecsal=?," +
					"fic_horsal=?,fic_horcon=?,fic_obs=?,fic_estado=?,fic_ver=? " +
					"where subare_cod=? and fic_bar=?, and fic_ver=?";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			pstm.setInt(1, parFic.getFic_cod());
			pstm.setInt(2, parFic.getAre_cod());
			pstm.setInt(3, parFic.getDetped_cod());
			pstm.setInt(4, parFic.getPed_cod());
			pstm.setInt(5, parFic.getPrd_cod());
			pstm.setInt(6, parFic.getGru_cod());
			pstm.setString(7, parFic.getFic_ide());
			pstm.setInt(8, parFic.getFic_canped());
			pstm.setInt(9, parFic.getFic_canreg());
			pstm.setDate(10, parFic.getFic_fecing());
			pstm.setTime(11, parFic.getFic_horing());
			pstm.setDate(12, parFic.getFic_fecsal());
			pstm.setTime(13, parFic.getFic_horsal());
			pstm.setInt(14, parFic.getFic_horcon());
			pstm.setString(15, parFic.getFic_obs());
			pstm.setString(16, parFic.getFic_estado());
			pstm.setInt(17, parFic.getFic_ver() + 1);
			pstm.setInt(18, parFic.getSubare_cod());
			pstm.setString(19, parFic.getFic_bar());
			pstm.setInt(20, parFic.getFic_ver());

			if (pstm.executeUpdate() == 1) {

				resCod = parFic.getFic_cod();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return resCod;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public int obtenerTotal(String parSql) {
		String conSql;
		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parSql).executeQuery();

			if (resSql.next()) {
				resSql.close();
				return resSql.getInt("total");

			}// Fin de if(pstm.executeUpdate() == 1)
			resSql.close();
			return 0;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	// public int actualizarEstado(Ficha parFic) {
	//
	// String sql;
	// int resCod = 0;
	// int totFicTer = 0,totFicPrd = 0,totFicPen = 0;
	// PreparedStatement pstm;
	// try {
	//
	// totFicTer = obtenerTotal("select COUNT(ficha.fic_cod) as total from ficha where ficha.ped_cod="+parFic.getPed_cod()+" and fic_estado='TERMINADO'");
	// totFicPrd = obtenerTotal("select COUNT(ficha.fic_cod) as total from ficha where ficha.ped_cod="+parFic.getPed_cod()+" and fic_estado='PRODUCCION'");
	// totFicPen = obtenerTotal("select COUNT(ficha.fic_cod) as total from ficha where ficha.ped_cod="+parFic.getPed_cod()+" and fic_estado='PENDIENTE'");
	//
	// if(totFicTer > 0 && (totFicPrd == 0 && totFicPen == 0)){
	// DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement("update pedido set ped_est = 'TERMINADO' where ped_cod = "+parFic.getPed_cod()).executeUpdate();
	// }
	//
	// totFicTer = obtenerTotal("select COUNT(ficha.fic_cod) as total from ficha where ficha.ped_cod="+parFic.getPed_cod()+" and fic_estado='TERMINADO' and detped_cod=" + parFic.getDetped_cod());
	// totFicPrd = obtenerTotal("select COUNT(ficha.fic_cod) as total from ficha where ficha.ped_cod="+parFic.getPed_cod()+" and fic_estado='PRODUCCION' and detped_cod=" + parFic.getDetped_cod());
	// totFicPen = obtenerTotal("select COUNT(ficha.fic_cod) as total from ficha where ficha.ped_cod="+parFic.getPed_cod()+" and fic_estado='PENDIENTE' and detped_cod=" + parFic.getDetped_cod());
	//
	// if(totFicTer> 0 && (totFicPrd == 0 && totFicPen == 0)) {
	// sql="update detpedido set detped_est = 'TERMINADO' where ped_cod = "+parFic.getPed_cod() + " and detped_cod="+ parFic.getDetped_cod();
	// DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(sql).executeUpdate();
	// }
	// else if(totFicPrd > 0 || totFicPen > 0) {
	//
	// sql="update pedido set ped_est = 'PRODUCCION' where ped_cod = "+parFic.getPed_cod();
	// DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(sql).executeUpdate();
	//
	// sql="update detpedido set detped_est = 'PRODUCCION' where ped_cod = "+parFic.getPed_cod() + " and detped_cod="+ parFic.getDetped_cod();
	//
	// /*Actualizamos la cantidad de fabricacion del producto superior DOS PRODUCTO*/
	// select @prd_supdos=prd_supdos from producto where prd_cod=@prd_cod;
	// select @detped_cod=detped_cod from detpedido where ped_cod=@ped_cod and prd_cod=@prd_supdos
	// update detpedido set detped_est = 'PRODUCCION' where detped_cod=@detped_cod
	//
	// /*Actualizamos la cantidad de fabricacion del producto superior UNO MODELO*/
	// select @prd_supuno=prd_supuno from producto where prd_cod=@prd_cod;
	// select @detped_cod=detped_cod from detpedido where ped_cod=@ped_cod and prd_cod=@prd_supuno
	// update detpedido set detped_est = 'PRODUCCION' where detped_cod=@detped_cod
	//
	// end
	// else if(totFicTer=0 and totFicPrd = 0 and totFicPen = 0) or
	// (totFicTer is null and totFicPrd is null and totFicPen is null) begin
	//
	// update pedido set ped_est = 'PENDIENTE' where ped_cod = @ped_cod
	// update detpedido set detped_est = 'PENDIENTE' where ped_cod = @ped_cod and detped_cod=@detped_cod
	//
	// /*Actualizamos la cantidad de fabricacion del producto superior DOS PRODUCTO*/
	// select @prd_supdos=prd_supdos from producto where prd_cod=@prd_cod;
	// select @detped_cod=detped_cod from detpedido where ped_cod=@ped_cod and prd_cod=@prd_supdos
	// update detpedido set detped_est = 'PRODUCCION' where detped_cod=@detped_cod
	//
	// /*Actualizamos la cantidad de fabricacion del producto superior UNO MODELO*/
	// select @prd_supuno=prd_supuno from producto where prd_cod=@prd_cod;
	// select @detped_cod=detped_cod from detpedido where ped_cod=@ped_cod and prd_cod=@prd_supuno
	// update detpedido set detped_est = 'PRODUCCION' where detped_cod=@detped_cod
	//
	// end
	//
	// insert into prueba values('tot_fic_detped_ter : ' + convert(varchar(3),totFicTer) + ' : ' +
	// 'tot_fic_detped_prd : ' + convert(varchar(3),totFicPrd) + ' : ' +
	// 'tot_fic_detped_pen : ' + convert(varchar(3),totFicPen))
	//
	//
	// conSql = "update Ficha set fic_cod=?,are_cod=?,detped_cod=?,ped_cod=?,prd_cod=?," +
	// "gru_cod=?,fic_ide=?,fic_canped=?,fic_fecing=?,fic_horing=?,fic_fecsal=?," +
	// "fic_horsal=?,fic_horcon=?,fic_obs=?,fic_estado=?,fic_ver=? " +
	// "where subare_cod=? and fic_bar=?, and fic_ver=?";
	// pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);
	//
	//
	// if (pstm.executeUpdate() == 1) {
	//
	// resCod = parFic.getFic_cod();
	//
	// }// Fin de if(pstm.executeUpdate() == 1)
	//
	// pstm.close();
	// return resCod;
	//
	// } catch (SQLException e) {
	// throw new RuntimeException(e);
	// }
	// }// Fin de Metodo Agregar Docente

	public Ficha obtenerFicha(int parFicCod) {

		String sql = "select * from Ficha where fic_cod=" + parFicCod;
		return obtenerEntidad(sql);

	}

	public Ficha obtenerEntidad(String parSql) {
		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(parSql);
			fic.limpiarInstancia();

			if (resSql.next()) {

				fic.setFic_cod(resSql.getInt("fic_cod"));
				fic.setSubare_cod(resSql.getInt("subare_cod"));
				fic.setAre_cod(resSql.getInt("are_cod"));
				fic.setDetped_cod(resSql.getInt("detped_cod"));
				fic.setPed_cod(resSql.getInt("ped_cod"));
				fic.setPrd_cod(resSql.getInt("prd_cod"));
				fic.setGru_cod(resSql.getInt("gru_cod"));
				fic.setFic_ide(resSql.getString("fic_ide"));
				fic.setFic_canped(resSql.getInt("fic_canped"));
				fic.setFic_canreg(resSql.getInt("fic_canreg"));
				fic.setFic_fecing(resSql.getDate("fic_fecing"));
				fic.setFic_horing(resSql.getTime("fic_horing"));
				fic.setFic_fecsal(resSql.getDate("fic_fecsal"));
				fic.setFic_horsal(resSql.getTime("fic_horsal"));
				fic.setFic_horcon(resSql.getInt("fic_horcon"));
				fic.setFic_obs(resSql.getString("fic_obs"));
				fic.setFic_estado(resSql.getString("fic_estado"));
				fic.setFic_bar(resSql.getString("fic_bar"));
				fic.setFic_ver(resSql.getInt("fic_ver"));

			}// Fin de

			resSql.close();
			return fic;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}

	public List<String> obtenerListaCtrlProducto(String parSql) {

		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(parSql);
			List<String> lisView = new ArrayList<String>();

			while (resSql.next()) {
				lisView.add(resSql.getString("are_cod") + "#"
						+ resSql.getString("subare_cod") + "#"
						+ resSql.getString("ped_nom") + "#"
						+ resSql.getString("prd_supuno") + "#"
						+ resSql.getString("prd_tip") + "#"
						+ resSql.getString("prd_supdos") + "#"
						+ resSql.getString("prd_col") + "#"
						+ resSql.getString("prd_des") + "#"
						+ resSql.getString("fic_ide") + "#"
						+ resSql.getDate("fic_fecing").getDate() + "/"
						+ (resSql.getDate("fic_fecing").getMonth() + 1) + "/"
						+ (resSql.getDate("fic_fecing").getYear() + 1900) + "#"
						+ resSql.getString("fic_estado") + "#"
						+ resSql.getString("fic_canped") + "#"
						+ resSql.getString("fic_cod") + "#"
						+ resSql.getString("subare_cod") + "#"
						+ resSql.getString("are_cod") + "#"
						+ resSql.getString("detped_cod") + "#"
						+ resSql.getString("ped_cod") + "#"
						+ resSql.getString("prd_cod") + "#"
						+ resSql.getString("gru_cod") + "#"
						+ resSql.getString("fic_canreg"));
			}

			resSql.close();
			return lisView;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<Ficha> obtenerListaFicha(int parPedCod) {

		String sql = "select * from Ficha where ped_cod=" + parPedCod + " order by prd_cod,fic_ide, fic_fecing";
		return obtenerLista(sql);

	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public List<Ficha> obtenerListaGroupFicha(String parSql) {

		String conSql;
		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(parSql);
			List<Ficha> lisFic = new ArrayList<Ficha>();

			Ficha varFic=new Ficha();
			varFic.limpiarInstancia();
			while (resSql.next()) {

				varFic = new Ficha();

//				varFic.setFic_cod(resSql.getInt("fic_cod"));
				varFic.setSubare_cod(resSql.getInt("subare_cod"));
				varFic.setAre_cod(resSql.getInt("are_cod"));
				varFic.setDetped_cod(resSql.getInt("detped_cod"));
				varFic.setPed_cod(resSql.getInt("ped_cod"));
				varFic.setPrd_cod(resSql.getInt("prd_cod"));
				varFic.setGru_cod(resSql.getInt("gru_cod"));
				varFic.setFic_bar(resSql.getString("fic_bar"));
				varFic.setFic_ide(resSql.getString("fic_ide"));
				varFic.setFic_canped(resSql.getInt("fic_canped"));
//				varFic.setFic_canreg(resSql.getInt("fic_canreg"));
//				varFic.setFic_fecing(resSql.getDate("fic_fecing"));
//				varFic.setFic_horing(resSql.getTime("fic_horing"));
//				varFic.setFic_fecsal(resSql.getDate("fic_fecsal"));
//				varFic.setFic_horsal(resSql.getTime("fic_horsal"));
//				varFic.setFic_horcon(resSql.getInt("fic_horcon"));
//				varFic.setFic_obs(resSql.getString("fic_obs"));
				varFic.setFic_estado(resSql.getString("fic_estado"));
//				varFic.setFic_ver(resSql.getInt("fic_ver"));

				lisFic.add(varFic);

			}// Fin de

			resSql.close();
			return lisFic;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)
	
	public List<Ficha> obtenerLista(String parSql) {

		String conSql;
		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(parSql);
			List<Ficha> lisFic = new ArrayList<Ficha>();

			Ficha varFic;

			while (resSql.next()) {

				varFic = new Ficha();

				varFic.setFic_cod(resSql.getInt("fic_cod"));
				varFic.setSubare_cod(resSql.getInt("subare_cod"));
				varFic.setAre_cod(resSql.getInt("are_cod"));
				varFic.setDetped_cod(resSql.getInt("detped_cod"));
				varFic.setPed_cod(resSql.getInt("ped_cod"));
				varFic.setPrd_cod(resSql.getInt("prd_cod"));
				varFic.setGru_cod(resSql.getInt("gru_cod"));
				varFic.setFic_ide(resSql.getString("fic_ide"));
				varFic.setFic_canped(resSql.getInt("fic_canped"));
				varFic.setFic_canreg(resSql.getInt("fic_canreg"));
				varFic.setFic_fecing(resSql.getDate("fic_fecing"));
				varFic.setFic_horing(resSql.getTime("fic_horing"));
				varFic.setFic_fecsal(resSql.getDate("fic_fecsal"));
				varFic.setFic_horsal(resSql.getTime("fic_horsal"));
				varFic.setFic_horcon(resSql.getInt("fic_horcon"));
				varFic.setFic_obs(resSql.getString("fic_obs"));
				varFic.setFic_estado(resSql.getString("fic_estado"));
				varFic.setFic_bar(resSql.getString("fic_bar"));
				varFic.setFic_ver(resSql.getInt("fic_ver"));

				lisFic.add(varFic);

			}// Fin de

			resSql.close();
			return lisFic;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public int eliminar(int parFicCod) {
		String conSql;
		PreparedStatement pstm;
		try {
			conSql = "delete from ficha where fic_cod=" + parFicCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			return pstm.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public int eliminarByBarra(String parFicBar, String estado, int subare_cod) {
		int can = 0;
		try {
			
			String sql = "select * from ficha where fic_bar=" + parFicBar + " and fic_estado='"+estado+"' and subare_cod="+subare_cod;
			List<Ficha> lisFic = obtenerLista(sql);
			 for(int ite=0; ite<lisFic.size(); ite++){
				 Ficha fic = (Ficha) lisFic.get(ite);
				 can = eliminar(fic.getFic_cod());
				 if(can==0){return can;}
			 }
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return can;
	}// Fin de Metodo Agregar Docente
	
	public int eliminarLista(int pedCod) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from ficha where ped_cod=" + pedCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			return pstm.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public int eliminarFichaPendiente(int parPedCod, String parFicBar) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from ficha where ped_cod=" + parPedCod + " and fic_estado='PENDIENTE' and fic_bar='" + parFicBar + "'";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			return pstm.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public int eliminarFichaTerminadoPendienteProduccion(int parPedCod, String parFicBar) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from ficha where ped_cod=" + parPedCod + " and (fic_estado='TERMINADO' or fic_estado='PENDIENTE' or fic_estado='PRODUCCION') and fic_bar='" + parFicBar + "'";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			return pstm.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public boolean existeFicha(String parSql) {

		ResultSet resSql;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parSql).executeQuery();

			if (resSql.next()) {

				return true;

			}

			return false;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error

	}// Fin de metodo para leer un registro de producto

	public int numeroOrdenUltimaAreaRegistrada(String parSql) {

		ResultSet resSql;
		int numeroOrden = 0;
		try {

			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(parSql).executeQuery();

			if (resSql.next()) {

				numeroOrden = resSql.getInt("are_ord");

			}

			return numeroOrden;

		} catch (SQLException e) {

			e.printStackTrace();
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error

	}// Fin de metodo para leer un registro de producto

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}// Fin de public void run()
		});
	}// Fin de metod main
}// in de clase prinicipal