package com.textil.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import com.comun.database.DBConfiguracion;
import com.comun.entidad.Unidad;
import com.comun.organizacion.OrganizacionGeneral;

public class UnidadDAO {

	private Unidad und;

	public UnidadDAO(Unidad parUnd)
	{
		this.und = parUnd;
	}

	public int agregar(Unidad parUnd) {

		String conSql;
		int codIns = 0;
		PreparedStatement pstm;
		ResultSet resSql;
		try {

			conSql = "insert into unidad values(?,?,?,?,?,?,?,?,?,?,?)";
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql, Statement.RETURN_GENERATED_KEYS);

			// pstm.setInt(1, parUnd.getCla_cod());
			// pstm.setInt(2, parUnd.getUnd_cod());
			// pstm.setInt(3, parUnd.getPrd_cod());
			// pstm.setInt(4, parUnd.getPed_cod());
			// pstm.setInt(5, parUnd.getDetped_cod());
			// pstm.setInt(6, parUnd.getDetped_canped());
			// pstm.setInt(7, parUnd.getDetped_canfab());
			// pstm.setInt(8, parUnd.getDetped_canent());
			// pstm.setDouble(9, parUnd.getDetped_pre());
			// pstm.setDouble(10, parUnd.getDetped_imp());
			// pstm.setDouble(11, parUnd.getDetped_ver());

			if (pstm.executeUpdate() > 0) {
				resSql = pstm.getGeneratedKeys();
				while (resSql.next()) {

					codIns = resSql.getInt(1);

				}
				resSql.close();

			}// Fin de if(pstm.executeUpdate() == 1)

			pstm.close();
			return codIns;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public List<Unidad> obtenerListaUnidad() {

		String conSql;
		ResultSet resSql;
		try {

			conSql = "select * from unidad ";
			resSql = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().createStatement().executeQuery(conSql);
			List<Unidad> lisUnd = new ArrayList<Unidad>();

			Unidad varUnd;

			while (resSql.next()) {

				varUnd = new Unidad();

				varUnd.setUnd_cod(resSql.getInt("und_cod"));
				varUnd.setUnd_alias(resSql.getString("und_alias"));
				varUnd.setUnd_nombre(resSql.getString("und_nombre"));
				varUnd.setUnd_pakcan(resSql.getInt("und_pakcan"));
				varUnd.setUnd_supuno(resSql.getInt("und_supuno"));
				varUnd.setUnd_supdos(resSql.getInt("und_supdos"));
				varUnd.setUnd_suptres(resSql.getInt("und_suptres"));
				varUnd.setUnd_supcua(resSql.getInt("und_supcua"));
				varUnd.setUnd_nivel(resSql.getInt("und_nivel"));
				varUnd.setUnd_orden(resSql.getInt("und_orden"));
				varUnd.setUnd_ver(resSql.getInt("Und_ver"));

				lisUnd.add(varUnd);

			}// Fin de

			resSql.close();
			return lisUnd;

		} catch (SQLException e) {
			// funUsu.mostrarMensaje("¡¡¡Error en metodo getListaContacto()", this.getClass().getName());
			throw new RuntimeException(e);

		}// Fin Catch en caso de que haya un error
	}// Fin de metodo obtenerListaDetPedido(int parPedCod)

	public boolean eliminar(int parDetPedCod) {
		String conSql;
		PreparedStatement pstm;
		try {

			conSql = "delete from unidad where detped_cod=" + parDetPedCod;
			pstm = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection().prepareStatement(conSql);

			int afectadas = pstm.executeUpdate();
			pstm.close();
			return (afectadas == 1) ? true : false;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// Fin de Metodo Agregar Docente

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}// Fin de public void run()
		});
	}// Fin de metod main
}// in de clase prinicipal