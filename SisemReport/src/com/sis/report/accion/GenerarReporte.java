/*
 * Created on Mar 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sis.report.accion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.browse.Browse;
import com.browse.BrowseObject;
import com.browse.accion.GenerateReportObject;
import com.comun.motor.Accion;
import com.comun.referencia.Estado;

/**
 * @author renzo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class GenerarReporte extends Accion
{

    public Object executeTask(Map peticion) throws Exception
    {
    	Object result = null;
        try
        {
			BrowseObject browseObject = null;
			Browse browse = null;
        	
            String browseName = (String)peticion.get("browseName");
            List xmls = new ArrayList();
            if(browseName.equals("none"))
            {
                xmls.add("project-tracking");
                xmls.add("project-tracking2");
            }
            else
            {
                xmls.add(browseName);
            }

            List partialDs = new ArrayList();
            
            
			browseObject = (BrowseObject) (new GenerateReportObject()).ejecutarAccion(peticion);
			peticion.put("browseObject", browseObject);

            
            
            
//            PuridiomProcessLoader processLoader = new PuridiomProcessLoader();
//            for(int i = 0; i < xmls.size(); i++)
//            {
//                PuridiomProcess process = processLoader.loadProcess("report-execute.xml");
//                peticion.put("browseName", xmls.get(i));
//                process.executeProcess(peticion);
//                if(process.getStatus() == Status.SUCCEEDED)
//                {
//                   partialDs.addAll((List)peticion.get("datasource"));
//                }
//                else
//                {
//                    throw new TsaException("An error ocurred generating " + (String)xmls.get(i) + " Report");
//                }
//            }
//            peticion.put("datasource", partialDs);
//            ret = peticion.get("browseObject");
//
//            this.setStatus(Status.SUCCEEDED);
        }
        catch (Exception e)
        {
            this.setEstado(Estado.FAILED);
        }
        return result;
    }
}
