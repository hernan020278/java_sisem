package com.sis.report.accion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.browse.BrowseObject;
import com.comun.database.DBConeccion;
import com.comun.motor.Accion;
import com.comun.propiedad.DiccionarioGeneral;
import com.comun.propiedad.PropiedadGeneral;
import com.comun.referencia.Estado;
import com.comun.utilidad.Log;
import com.sis.jasperreport.JasperReportsHelper;

public class SystemReportExecute extends Accion
{
	public Object executeTask(Map peticion) throws Exception
	{
		Object ret = "";
		try
		{
			String organizacionIde = (String) peticion.get("organizacionIde");
			String userId = (String) peticion.get("userId");
			String path = DiccionarioGeneral.getInstance("host", organizacionIde).getPropiedad("reportsPath", "");
			DBConeccion dbc = (DBConeccion) peticion.get("dbConeccion");
			BrowseObject reportObject = (BrowseObject) peticion.get("browseObject");
			Map parameters = new HashMap();
			PropiedadGeneral propiedadGeneral = PropiedadGeneral.getInstance(organizacionIde);
			String removePercent = propiedadGeneral.getProperty("REPORT OPTIONS", "REMOVEPERCENTFROMCRITERIA", "N");
			parameters.put("webreport", (String) peticion.get("webreport"));
			parameters.put("isReport", "Y");
			parameters.put("reportPath", path);
			String reportName = (String) peticion.get("reportName");
			parameters.put("filePath", path + reportName);
			parameters.put("organizacionIde", organizacionIde);
			parameters.put("reportTitle", reportObject.getTitle());

			Map reportParameters = new HashMap();
			reportParameters.put("dbConeccion", dbc);
			reportParameters.put("organizacionIde", organizacionIde);
			reportParameters.put("oid", organizacionIde);
			reportParameters.put("userId", userId);
			reportParameters.put("reportTitle", reportObject.getTitle());

			if (reportName.equals("r-pos-s-reg-sup") || reportName.equals("r-pos-d-reg-sup"))
			{
				reportParameters.put("reportCriteria", peticion.get("reportCriteria"));
			}
			else
			{
				// String reportCriteria = (String) reportQueue.getUserWhereClause(organizacionIde);
				// if (removePercent.compareTo("Y") == 0)
				// {
				// reportCriteria = reportCriteria.replace("%", "");
				// }
				reportParameters.put("reportCriteria", reportObject.getSqlWhere());
			}
			parameters.put("reportParameters", reportParameters);
			parameters.put("dbConeccion", dbc);
			List data = (List) peticion.get("datasource");
			int dataSize = data.size();
			int restricted = 0;
			List subData = new ArrayList();
			int topSize = Integer.parseInt(PropiedadGeneral.getInstance(organizacionIde).getProperty("REPORTS OPTIONS", "TOPSIZE", "25"));
			if (reportName.toLowerCase().indexOf("top") > -1 && topSize > 0) {
				if (dataSize > topSize) {
					subData = data.subList(0, topSize);
					restricted = 10;
				}
				else {
					subData = data;
					restricted = dataSize;
				}
				parameters.put("datasource", subData);
			}
			else
			{
				Log.error(this, reportName + " Returned: " + data.size());
				peticion.put("tooMuchData", "N");
				parameters.put("datasource", peticion.get("datasource"));
				restricted = dataSize;
			}
			parameters.put("dataSize", new java.math.BigDecimal(dataSize));
			parameters.put("dataSizeRestricted", new java.math.BigDecimal(restricted));

			List dataList = (List) peticion.get("datasource");
			if (dataList.size() < 1)
			{
				parameters.put("reportName", "nodata");
				parameters.put("errorType", "nodata");
			}
			else
			{
				parameters.put("reportName", (String) peticion.get("reportName"));
			}

			parameters.put("reportObject", reportObject);
			String noParams = (String) peticion.get("noParams");
			if (noParams == null) {
				noParams = "N";
			}
			if (noParams.equalsIgnoreCase("Y"))
			{
				parameters.put("isDatasource", new Boolean(false));
			}
			else
			{
				parameters.put("isDatasource", new Boolean(true));
				// parameters.put("filters", reportObject.getQueryFilter());
			}

			String format = (String) peticion.get("format");
			if (format == null)
			{
				if (noParams.equalsIgnoreCase("Y"))
				{
					format = "html";
				}
				else
				{
					format = "pdf";
				}
			}
			parameters.put("format", format);
			long start = System.currentTimeMillis();
			ret = JasperReportsHelper.runReport(parameters);
			long end = System.currentTimeMillis();
			Log.debug(this, "It took " + ((end - start) / 1000) + " seconds to execute the report" + (String) peticion.get("reportName"));

			this.setEstado(Estado.SUCCEEDED);
		}
		catch (Exception e)
		{
			this.setEstado(Estado.FAILED);
		}
		return ret;
	}
}
