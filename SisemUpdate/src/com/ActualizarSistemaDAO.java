package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.comun.database.DBConfiguracion;
import com.comun.organizacion.OrganizacionGeneral;

public class ActualizarSistemaDAO {

	public void ejecutarSentencias(String archivo) {

		Statement stm = null;
		Connection conecction;

		List<String> listSql = leerArchivo(archivo);
		for (String iteSql : listSql) {

			conecction = DBConfiguracion.getInstance(OrganizacionGeneral.getOrgcodigo()).getConnection();

			try {

				stm = conecction.createStatement();
				stm.execute(iteSql);
				conecction.close();

			} catch (SQLException e) {
				throw new RuntimeException(e);
			}

		}
	}

	public String leerFuente() {

		String fuente = "";
		try {

			FileReader file = new FileReader("src/ManejoCaracter/LeerFuente.java");
			BufferedReader buff = new BufferedReader(file);
			boolean eof = false;
			while (!eof) {
				String line = buff.readLine();
				if (line == null)
					eof = true;
				else
					// System.out.println(line);
					fuente = fuente + line + "\n";
			}

			buff.close();

		} catch (IOException e) {
			System.out.println("Error -- " + e.toString());
		}

		return fuente;

	}

	public List<String> leerArchivo(String archivo) {

		String archivoSql = new String();
		List<String> listSql = new ArrayList<String>();

		try {

			File file = new File(archivo);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
			String linea = "";
			try {
				while (bufferedReader.ready()) {

					linea = bufferedReader.readLine();
					if (linea.trim().toUpperCase().equals("GO")) {

						listSql.add(archivoSql);
						archivoSql = "";

					} else {
						archivoSql = archivoSql + linea + "\n";
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Ejecutando sql... " + archivo);
		return listSql;
	}

}
