package com;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.comun.organizacion.OrganizacionGeneral;

public class EjecutarActualizacion {

	public static void main(final String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				try
				{
					System.out.println("organizacion : " + args[0]);
					OrganizacionGeneral.getInstance(args[0]);
					ActualizarLibrerias actLib = new ActualizarLibrerias();
					System.out.println("Copiando librerias dll...");
					actLib.copiarArchivosDll("C:\\sisem\\dll\\jsmile.dll", "C:\\Windows\\System32\\jsmile.dll");
					System.out.println("Finalizacion con exito...");
					System.out.println("");	
					System.out.println("Creando acceso Directo el sistema...");
					actLib.copiarArchivosDll("C:\\sisem\\bin\\asesor\\asesor.bat", "C:\\Users\\"+ System.getProperty("user.name") +"\\Desktop\\asesor.bat");
					System.out.println("Finalizacion con exito...");
				}
				catch (ArrayIndexOutOfBoundsException ex)
				{
					JOptionPane.showConfirmDialog(null, "El sistema debe ser ejecutado con el archivo bat respectivo..." + "\n" + this.getClass().getName(), "Sistema Chentty", JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
	}
}
